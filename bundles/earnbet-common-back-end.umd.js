(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('mysql'), require('earnbet-common'), require('bs58'), require('bip39'), require('bip32'), require('bitcoinjs-lib'), require('eosjs-revised'), require('eosjs-ecc'), require('big.js'), require('https'), require('http')) :
    typeof define === 'function' && define.amd ? define('earnbet-common-back-end', ['exports', 'mysql', 'earnbet-common', 'bs58', 'bip39', 'bip32', 'bitcoinjs-lib', 'eosjs-revised', 'eosjs-ecc', 'big.js', 'https', 'http'], factory) :
    (global = global || self, factory(global['earnbet-common-back-end'] = {}, global.mysql, global.earnbetCommon, global.bs58, global.bip39, global.bip32, global.bitcoinjsLib, global.Eos, global.eos_ecc, global.big_js, global.https, global.http));
}(this, (function (exports, mysql, earnbetCommon, bs58, bip39, bip32, bitcoinjsLib, Eos, eos_ecc, big_js, https, http) { 'use strict';

    Eos = Eos && Eos.hasOwnProperty('default') ? Eos['default'] : Eos;
    eos_ecc = eos_ecc && eos_ecc.hasOwnProperty('default') ? eos_ecc['default'] : eos_ecc;

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/database/engine/mysql/interfaces.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IMySQLConfig() { }
    if (false) {
        /** @type {?} */
        IMySQLConfig.prototype.host;
        /** @type {?|undefined} */
        IMySQLConfig.prototype.port;
        /** @type {?} */
        IMySQLConfig.prototype.user;
        /** @type {?} */
        IMySQLConfig.prototype.password;
        /** @type {?} */
        IMySQLConfig.prototype.database;
        /** @type {?} */
        IMySQLConfig.prototype.charset;
    }
    /**
     * @record
     */
    function IMySQLRepository() { }
    if (false) {
        /** @type {?} */
        IMySQLRepository.prototype.tableName;
        /** @type {?} */
        IMySQLRepository.prototype.primaryKey;
        /** @type {?} */
        IMySQLRepository.prototype.isPrimaryKeyAString;
    }
    /**
     * @record
     */
    function IMySQLQueryResult() { }
    if (false) {
        /** @type {?} */
        IMySQLQueryResult.prototype.fieldCount;
        /** @type {?} */
        IMySQLQueryResult.prototype.affectedRows;
        /** @type {?} */
        IMySQLQueryResult.prototype.insertId;
        /** @type {?} */
        IMySQLQueryResult.prototype.serverStatus;
        /** @type {?} */
        IMySQLQueryResult.prototype.warningCount;
        /** @type {?} */
        IMySQLQueryResult.prototype.message;
        /** @type {?} */
        IMySQLQueryResult.prototype.changedRows;
    }
    /**
     * @record
     */
    function IMySQLDatabase() { }
    if (false) {
        /** @type {?} */
        IMySQLDatabase.prototype.builder;
        /** @type {?} */
        IMySQLDatabase.prototype.transactionManager;
    }
    /**
     * @record
     * @template T
     */
    function IQueryParams() { }
    if (false) {
        /** @type {?|undefined} */
        IQueryParams.prototype.whereMap;
        /** @type {?|undefined} */
        IQueryParams.prototype.whereAndMap;
        /** @type {?|undefined} */
        IQueryParams.prototype.whereOrMap;
        /** @type {?|undefined} */
        IQueryParams.prototype.whereClause;
        /** @type {?|undefined} */
        IQueryParams.prototype.wherePrimaryKeys;
        /** @type {?|undefined} */
        IQueryParams.prototype.whereGroup;
        /** @type {?|undefined} */
        IQueryParams.prototype.orderBy;
        /** @type {?|undefined} */
        IQueryParams.prototype.limit;
    }
    /**
     * @record
     * @template T
     */
    function ISelectQueryParams() { }
    if (false) {
        /** @type {?|undefined} */
        ISelectQueryParams.prototype.fields;
    }
    /**
     * @record
     * @template T
     */
    function IQueryBuilderParams() { }
    if (false) {
        /** @type {?} */
        IQueryBuilderParams.prototype.repository;
    }
    /**
     * @record
     * @template T
     */
    function ISelectQueryBuilderParams() { }
    if (false) {
        /** @type {?} */
        ISelectQueryBuilderParams.prototype.fields;
    }
    /**
     * @record
     * @template T
     */
    function IUpdateQueryParams() { }
    if (false) {
        /** @type {?|undefined} */
        IUpdateQueryParams.prototype.singleStatement;
        /** @type {?|undefined} */
        IUpdateQueryParams.prototype.statements;
    }
    /**
     * @record
     * @template T
     */
    function IUpdateQueryBuilderParams() { }
    /**
     * @record
     */
    function IQueryData() { }
    if (false) {
        /** @type {?} */
        IQueryData.prototype.sql;
        /** @type {?} */
        IQueryData.prototype.argumentsToPrepare;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/database/engine/mysql/mysql-connection-manager.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var logSqlQueries = process.env.LOG_SQL_QUERIES === 'true';
    var MySQLConnectionManager = /** @class */ (function () {
        function MySQLConnectionManager(config) {
            this.config = config;
            this.isConnecting = false;
            this.connection = undefined;
            this.connect();
        }
        /**
         * @return {?}
         */
        MySQLConnectionManager.prototype.getConnection = /**
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.reconnect()];
                        case 1:
                            _a.sent();
                            return [2 /*return*/, this.connection];
                    }
                });
            });
        };
        /**
         * @private
         * @return {?}
         */
        MySQLConnectionManager.prototype.reconnect = /**
         * @private
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var isConnected;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!!this.isConnected) return [3 /*break*/, 3];
                            earnbetCommon.logWithTime('NOT CONNECTED:', this.connection.threadId);
                            this.printConnectionState();
                            return [4 /*yield*/, this.connect()];
                        case 1:
                            isConnected = _a.sent();
                            if (isConnected) {
                                return [2 /*return*/];
                            }
                            return [4 /*yield*/, earnbetCommon.sleep(1000)];
                        case 2:
                            _a.sent();
                            return [3 /*break*/, 0];
                        case 3: return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * @private
         * @return {?}
         */
        MySQLConnectionManager.prototype.connect = /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            return new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            function (resolve) {
                if (_this.isConnected) {
                    resolve(true);
                    return;
                }
                if (_this.isConnecting) {
                    resolve(false);
                    return;
                }
                _this.isConnecting = true;
                // end existing connection
                if (_this.connection) {
                    _this.connection.end();
                }
                // connection parameters
                _this.connection = mysql.createConnection(_this.config);
                // logWithTime('MySQLConnection State:',this.connection.state);
                // establish connection
                _this.connection.connect((/**
                 * @param {?} err
                 * @return {?}
                 */
                function (err) {
                    earnbetCommon.logWithTime(_this.config);
                    _this.isConnecting = false;
                    if (err) {
                        console.error('error connecting: ' + err.stack);
                        resolve(false);
                    }
                    else {
                        earnbetCommon.logWithTime('connected as id ' + _this.connection.threadId);
                        earnbetCommon.logWithTime('MySQLConnection State:', _this.connection.state);
                        // set query format
                        _this.connection.config.queryFormat = (/**
                         * @param {?} query
                         * @param {?} values
                         * @return {?}
                         */
                        function (query, values) {
                            if (!values) {
                                return query;
                            }
                            /** @type {?} */
                            var escapedQuery = query.replace(/\:(\w+)/g, (/**
                             * @param {?} txt
                             * @param {?} key
                             * @return {?}
                             */
                            function (txt, key) {
                                if (values.hasOwnProperty(key)) {
                                    return this.escape(values[key]);
                                }
                                return txt;
                            }).bind(this));
                            if (logSqlQueries) {
                                console.log({ escapedQuery: escapedQuery });
                            }
                            return escapedQuery;
                        });
                        resolve(true);
                    }
                }));
                _this.connection.on('error', (/**
                 * @param {?} err
                 * @return {?}
                 */
                function (err) {
                    earnbetCommon.logWithTime('******************* MySQL Connection onError EVENT: *******************');
                    console.error(err);
                    _this.printConnectionState();
                }));
            }));
        };
        /**
         * @private
         * @return {?}
         */
        MySQLConnectionManager.prototype.printConnectionState = /**
         * @private
         * @return {?}
         */
        function () {
            earnbetCommon.logWithTime('MySQLConnection State:', this.connection.state);
        };
        Object.defineProperty(MySQLConnectionManager.prototype, "isConnected", {
            /*
            private get isDisconnected():boolean {
                return this.connection == undefined ||
                        this.connection.state === 'disconnected';
            }
            */
            get: /*
                private get isDisconnected():boolean {
                    return this.connection == undefined ||
                            this.connection.state === 'disconnected';
                }
                */
            /**
             * @private
             * @return {?}
             */
            function () {
                return this.connection !== undefined && (this.connection.state === 'connected' ||
                    this.connection.state === 'authenticated');
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        MySQLConnectionManager.prototype.endConnection = /**
         * @return {?}
         */
        function () {
            this.connection.end();
        };
        return MySQLConnectionManager;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        MySQLConnectionManager.prototype.isConnecting;
        /**
         * @type {?}
         * @private
         */
        MySQLConnectionManager.prototype.connection;
        /**
         * @type {?}
         * @private
         */
        MySQLConnectionManager.prototype.config;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/database/engine/mysql/mysql-statements.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {number} */
    var MySQLValueType = {
        EXPRESSION: 0,
        STRING: 1,
    };
    MySQLValueType[MySQLValueType.EXPRESSION] = 'EXPRESSION';
    MySQLValueType[MySQLValueType.STRING] = 'STRING';
    /** @enum {string} */
    var ExpressionOperator = {
        EQUAL: "=",
        NOT_EQUAL: "!=",
        LESS_THAN: "<",
        LESS_THAN_OR_EQUAL: "<=",
        GREATER_THAN: ">",
        GREATER_THAN_OR_EQUAL: ">=",
        IS: "IS",
        IS_NOT: "IS NOT",
    };
    /** @enum {string} */
    var ExpressionGroupJoiner = {
        AND: "AND",
        OR: "OR",
        COMMA: ",",
    };
    /**
     * @record
     */
    function IMySQLValue() { }
    if (false) {
        /** @type {?} */
        IMySQLValue.prototype.data;
        /** @type {?} */
        IMySQLValue.prototype.isString;
    }
    var MySQLValue = /** @class */ (function () {
        function MySQLValue(data, type) {
            this.data = data;
            this.type = type;
            this.isString =
                type == MySQLValueType.STRING;
        }
        return MySQLValue;
    }());
    if (false) {
        /** @type {?} */
        MySQLValue.prototype.isString;
        /** @type {?} */
        MySQLValue.prototype.data;
        /** @type {?} */
        MySQLValue.prototype.type;
    }
    var MySQLNumberValue = /** @class */ (function (_super) {
        __extends(MySQLNumberValue, _super);
        function MySQLNumberValue(data) {
            return _super.call(this, data, MySQLValueType.EXPRESSION) || this;
        }
        return MySQLNumberValue;
    }(MySQLValue));
    var MySQLStringValue = /** @class */ (function (_super) {
        __extends(MySQLStringValue, _super);
        function MySQLStringValue(data) {
            return _super.call(this, data, MySQLValueType.STRING) || this;
        }
        return MySQLStringValue;
    }(MySQLValue));
    var MySQLStringExpressionValue = /** @class */ (function (_super) {
        __extends(MySQLStringExpressionValue, _super);
        function MySQLStringExpressionValue(data) {
            return _super.call(this, data, MySQLValueType.EXPRESSION) || this;
        }
        return MySQLStringExpressionValue;
    }(MySQLValue));
    /** @enum {string} */
    var MySQLExpression = {
        NOW: "NOW()",
        NULL: "NULL",
    };
    /** @type {?} */
    var nowValue = new MySQLStringExpressionValue(MySQLExpression.NOW);
    /** @type {?} */
    var nullValue = new MySQLStringExpressionValue(MySQLExpression.NULL);
    /*
    class MySQLValue
    {
        static booleanAsInt(bool:boolean):number
        {
            return bool ? 1 : 0;
        }
    }
    */
    /**
     * @abstract
     */
    var   /*
    class MySQLValue
    {
        static booleanAsInt(bool:boolean):number
        {
            return bool ? 1 : 0;
        }
    }
    */
    /**
     * @abstract
     */
    Expression = /** @class */ (function () {
        function Expression(leftSide, operator, rightSide) {
            this.leftSide = leftSide;
            this.operator = operator;
            this.rightSide = rightSide;
        }
        return Expression;
    }());
    if (false) {
        /** @type {?} */
        Expression.prototype.leftSide;
        /** @type {?} */
        Expression.prototype.operator;
        /** @type {?} */
        Expression.prototype.rightSide;
    }
    var ExpressionGroup = /** @class */ (function () {
        function ExpressionGroup(expressions, joiner, surroundExpressionWithBrackets) {
            if (surroundExpressionWithBrackets === void 0) { surroundExpressionWithBrackets = true; }
            this.expressions = expressions;
            this.joiner = joiner;
            this.surroundExpressionWithBrackets = surroundExpressionWithBrackets;
        }
        return ExpressionGroup;
    }());
    if (false) {
        /** @type {?} */
        ExpressionGroup.prototype.expressions;
        /** @type {?} */
        ExpressionGroup.prototype.joiner;
        /** @type {?} */
        ExpressionGroup.prototype.surroundExpressionWithBrackets;
    }
    /**
     * @param {?} map
     * @param {?} joiner
     * @return {?}
     */
    function createExpressionGroup(map, joiner) {
        /** @type {?} */
        var expressions = [];
        // should we use Object.keys()?
        for (var fieldName in map) {
            /** @type {?} */
            var fieldValue = map[fieldName];
            expressions.push(typeof fieldValue == 'string' ?
                new StringStatement(fieldName, (/** @type {?} */ (fieldValue))) :
                new NumberStatement(fieldName, fieldValue));
        }
        return new ExpressionGroup(expressions, joiner);
    }
    var SingleExpression = /** @class */ (function (_super) {
        __extends(SingleExpression, _super);
        function SingleExpression(expression) {
            return _super.call(this, [expression], undefined) || this;
        }
        return SingleExpression;
    }(ExpressionGroup));
    var AndExpressionGroup = /** @class */ (function (_super) {
        __extends(AndExpressionGroup, _super);
        function AndExpressionGroup(expressions) {
            return _super.call(this, expressions, ExpressionGroupJoiner.AND) || this;
        }
        return AndExpressionGroup;
    }(ExpressionGroup));
    var OrExpressionGroup = /** @class */ (function (_super) {
        __extends(OrExpressionGroup, _super);
        function OrExpressionGroup(expressions) {
            return _super.call(this, expressions, ExpressionGroupJoiner.OR) || this;
        }
        return OrExpressionGroup;
    }(ExpressionGroup));
    var UpdateStatements = /** @class */ (function (_super) {
        __extends(UpdateStatements, _super);
        function UpdateStatements(expressions) {
            return _super.call(this, expressions, ExpressionGroupJoiner.COMMA, false) || this;
        }
        return UpdateStatements;
    }(ExpressionGroup));
    /**
     * @abstract
     */
    var   /**
     * @abstract
     */
    FieldStatement = /** @class */ (function (_super) {
        __extends(FieldStatement, _super);
        function FieldStatement(fieldName, value, operator) {
            return _super.call(this, fieldName, operator, value) || this;
        }
        return FieldStatement;
    }(Expression));
    var NumberStatement = /** @class */ (function (_super) {
        __extends(NumberStatement, _super);
        function NumberStatement(fieldName, data, operator) {
            if (operator === void 0) { operator = ExpressionOperator.EQUAL; }
            return _super.call(this, fieldName, new MySQLNumberValue(data), operator) || this;
        }
        return NumberStatement;
    }(FieldStatement));
    var StringStatement = /** @class */ (function (_super) {
        __extends(StringStatement, _super);
        function StringStatement(fieldName, data, operator) {
            if (operator === void 0) { operator = ExpressionOperator.EQUAL; }
            return _super.call(this, fieldName, new MySQLStringValue(data), operator) || this;
        }
        return StringStatement;
    }(FieldStatement));
    var StringExpressionStatement = /** @class */ (function (_super) {
        __extends(StringExpressionStatement, _super);
        function StringExpressionStatement(fieldName, data, operator) {
            if (operator === void 0) { operator = ExpressionOperator.EQUAL; }
            return _super.call(this, fieldName, new MySQLStringExpressionValue(data), operator) || this;
        }
        return StringExpressionStatement;
    }(FieldStatement));
    var NullUpdateStatement = /** @class */ (function (_super) {
        __extends(NullUpdateStatement, _super);
        function NullUpdateStatement(fieldName) {
            return _super.call(this, fieldName, nullValue, ExpressionOperator.EQUAL) || this;
        }
        return NullUpdateStatement;
    }(FieldStatement));
    var NowUpdateStatement = /** @class */ (function (_super) {
        __extends(NowUpdateStatement, _super);
        function NowUpdateStatement(fieldName) {
            return _super.call(this, fieldName, nowValue, ExpressionOperator.EQUAL) || this;
        }
        return NowUpdateStatement;
    }(FieldStatement));
    var FieldIsNullStatement = /** @class */ (function (_super) {
        __extends(FieldIsNullStatement, _super);
        function FieldIsNullStatement(fieldName) {
            return _super.call(this, fieldName, nullValue, ExpressionOperator.IS) || this;
        }
        return FieldIsNullStatement;
    }(FieldStatement));
    var FieldIsNotNullStatement = /** @class */ (function (_super) {
        __extends(FieldIsNotNullStatement, _super);
        function FieldIsNotNullStatement(fieldName) {
            return _super.call(this, fieldName, nullValue, ExpressionOperator.IS_NOT) || this;
        }
        return FieldIsNotNullStatement;
    }(FieldStatement));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/database/engine/mysql/mysql-query.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var logSqlQueries$1 = process.env.LOG_SQL_QUERIES === 'true';
    /**
     * @template T
     */
    var   /**
     * @template T
     */
    MySQLQuery = /** @class */ (function () {
        function MySQLQuery(sql, connectionManager, argumentsToPrepare) {
            this.sql = sql;
            this.connectionManager = connectionManager;
            this.argumentsToPrepare = argumentsToPrepare;
        }
        /**
         * @return {?}
         */
        MySQLQuery.prototype.execute = /**
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var connection;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            // When execute is called directly,
                            // by default, it will use the connectionManager used to construct the query...
                            return [4 /*yield*/, this.connectionManager.getConnection()];
                        case 1:
                            connection = _a.sent();
                            return [2 /*return*/, executeQuery(this.sql, connection, this.argumentsToPrepare)];
                    }
                });
            });
        };
        return MySQLQuery;
    }());
    if (false) {
        /** @type {?} */
        MySQLQuery.prototype.sql;
        /**
         * @type {?}
         * @private
         */
        MySQLQuery.prototype.connectionManager;
        /** @type {?} */
        MySQLQuery.prototype.argumentsToPrepare;
    }
    /**
     * @template T
     * @param {?} sql
     * @param {?} connection
     * @param {?} argumentsToPrepare
     * @return {?}
     */
    function executeQuery(sql, connection, argumentsToPrepare) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            if (logSqlQueries$1) {
                earnbetCommon.logWithTime(sql, argumentsToPrepare);
            }
            connection.query(sql, argumentsToPrepare, (/**
             * @param {?} error
             * @param {?} result
             * @param {?} fields
             * @return {?}
             */
            function (error, result, fields) {
                if (error) {
                    earnbetCommon.logWithTime('*** MySQL Error while executing Query: ', sql, argumentsToPrepare);
                    console.error(error);
                    reject(error);
                }
                else {
                    resolve(result);
                }
            }));
        }));
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/database/engine/mysql/mysql-query-builder.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MySQLQueryBuilder = /** @class */ (function () {
        function MySQLQueryBuilder(config) {
            this.connectionManager = new MySQLConnectionManager(config);
        }
        /**
         * @template T
         * @param {?} repository
         * @param {?} entities
         * @return {?}
         */
        MySQLQueryBuilder.prototype.insert = /**
         * @template T
         * @param {?} repository
         * @param {?} entities
         * @return {?}
         */
        function (repository, entities) {
            return __awaiter(this, void 0, void 0, function () {
                var query;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            // construct query
                            return [4 /*yield*/, this.constructInsertQuery(repository, entities)];
                        case 1:
                            query = _a.sent();
                            return [2 /*return*/, new MySQLQuery(query, this.connectionManager)];
                    }
                });
            });
        };
        /**
         * @private
         * @param {?} repository
         * @param {?} entities
         * @return {?}
         */
        MySQLQueryBuilder.prototype.constructInsertQuery = /**
         * @private
         * @param {?} repository
         * @param {?} entities
         * @return {?}
         */
        function (repository, entities) {
            return __awaiter(this, void 0, void 0, function () {
                var fields, fieldName, rows, entities_1, entities_1_1, entity, _a, _b, e_1_1;
                var e_1, _c;
                return __generator(this, function (_d) {
                    switch (_d.label) {
                        case 0:
                            fields = [];
                            for (fieldName in entities[0]) {
                                fields.push(fieldName);
                            }
                            rows = [];
                            _d.label = 1;
                        case 1:
                            _d.trys.push([1, 6, 7, 8]);
                            entities_1 = __values(entities), entities_1_1 = entities_1.next();
                            _d.label = 2;
                        case 2:
                            if (!!entities_1_1.done) return [3 /*break*/, 5];
                            entity = entities_1_1.value;
                            _b = (_a = rows).push;
                            return [4 /*yield*/, this.constructRowForInsert(entity)];
                        case 3:
                            _b.apply(_a, [_d.sent()]);
                            _d.label = 4;
                        case 4:
                            entities_1_1 = entities_1.next();
                            return [3 /*break*/, 2];
                        case 5: return [3 /*break*/, 8];
                        case 6:
                            e_1_1 = _d.sent();
                            e_1 = { error: e_1_1 };
                            return [3 /*break*/, 8];
                        case 7:
                            try {
                                if (entities_1_1 && !entities_1_1.done && (_c = entities_1.return)) _c.call(entities_1);
                            }
                            finally { if (e_1) throw e_1.error; }
                            return [7 /*endfinally*/];
                        case 8: return [2 /*return*/, 'INSERT INTO ' + repository.tableName +
                                ' (' + this.prepareFieldList(fields) + ')' +
                                ' VALUES ' + rows.join(', ') + ';'];
                    }
                });
            });
        };
        /**
         * @private
         * @param {?} entity
         * @return {?}
         */
        MySQLQueryBuilder.prototype.constructRowForInsert = /**
         * @private
         * @param {?} entity
         * @return {?}
         */
        function (entity) {
            return __awaiter(this, void 0, void 0, function () {
                var values, _a, _b, _i, propertyName, rawValue, preparedValue;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            values = [];
                            _a = [];
                            for (_b in entity)
                                _a.push(_b);
                            _i = 0;
                            _c.label = 1;
                        case 1:
                            if (!(_i < _a.length)) return [3 /*break*/, 4];
                            propertyName = _a[_i];
                            rawValue = entity[propertyName];
                            return [4 /*yield*/, this.prepareValueForInsert(rawValue)];
                        case 2:
                            preparedValue = _c.sent();
                            values.push(preparedValue);
                            _c.label = 3;
                        case 3:
                            _i++;
                            return [3 /*break*/, 1];
                        case 4: return [2 /*return*/, '(' + values.join(',') + ')'];
                    }
                });
            });
        };
        /**
         * @private
         * @param {?} value
         * @return {?}
         */
        MySQLQueryBuilder.prototype.prepareValueForInsert = /**
         * @private
         * @param {?} value
         * @return {?}
         */
        function (value) {
            return __awaiter(this, void 0, void 0, function () {
                var _a, _b;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            _a = typeof value;
                            switch (_a) {
                                case 'string': return [3 /*break*/, 1];
                            }
                            return [3 /*break*/, 6];
                        case 1:
                            _b = value;
                            switch (_b) {
                                case MySQLExpression.NULL: return [3 /*break*/, 2];
                                case MySQLExpression.NOW: return [3 /*break*/, 2];
                            }
                            return [3 /*break*/, 3];
                        case 2: return [3 /*break*/, 5];
                        case 3: return [4 /*yield*/, this.escape(value)];
                        case 4:
                            value = _c.sent();
                            return [3 /*break*/, 5];
                        case 5: return [3 /*break*/, 8];
                        case 6: return [4 /*yield*/, this.escape(value)];
                        case 7:
                            value = _c.sent();
                            return [3 /*break*/, 8];
                        case 8: return [2 /*return*/, '' + value];
                    }
                });
            });
        };
        /**
         * @template T
         * @param {?} repository
         * @param {?} fields
         * @param {?} primaryKeyValues
         * @return {?}
         */
        MySQLQueryBuilder.prototype.selectByPrimaryKeys = /**
         * @template T
         * @param {?} repository
         * @param {?} fields
         * @param {?} primaryKeyValues
         * @return {?}
         */
        function (repository, fields, primaryKeyValues) {
            /** @type {?} */
            var query = this.prepareFieldsForSelect(fields, repository.tableName) +
                constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
            return new MySQLQuery(query, this.connectionManager);
        };
        /**
         * @template T
         * @param {?} repository
         * @param {?} fields
         * @return {?}
         */
        MySQLQueryBuilder.prototype.selectAll = /**
         * @template T
         * @param {?} repository
         * @param {?} fields
         * @return {?}
         */
        function (repository, fields) {
            /** @type {?} */
            var query = this.prepareFieldsForSelect(fields, repository.tableName) + ';';
            return new MySQLQuery(query, this.connectionManager);
        };
        /**
         * @template T
         * @param {?} params
         * @return {?}
         */
        MySQLQueryBuilder.prototype.select = /**
         * @template T
         * @param {?} params
         * @return {?}
         */
        function (params) {
            return __awaiter(this, void 0, void 0, function () {
                var query;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.prepareSelectStatement(params)];
                        case 1:
                            query = _a.sent();
                            return [2 /*return*/, new MySQLQuery(query + ";", this.connectionManager)];
                    }
                });
            });
        };
        /**
         * @private
         * @template T
         * @param {?} params
         * @return {?}
         */
        MySQLQueryBuilder.prototype.prepareSelectStatement = /**
         * @private
         * @template T
         * @param {?} params
         * @return {?}
         */
        function (params) {
            return __awaiter(this, void 0, void 0, function () {
                var fields, repository, query, _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            fields = params.fields, repository = params.repository;
                            query = this.prepareFieldsForSelect(fields, repository.tableName);
                            _a = query;
                            return [4 /*yield*/, this.prepareWhereOrderAndLimit(params)];
                        case 1:
                            query = _a + _b.sent();
                            return [2 /*return*/, query];
                    }
                });
            });
        };
        /**
         * @private
         * @param {?} fields
         * @param {?} tableName
         * @return {?}
         */
        MySQLQueryBuilder.prototype.prepareFieldsForSelect = /**
         * @private
         * @param {?} fields
         * @param {?} tableName
         * @return {?}
         */
        function (fields, tableName) {
            /** @type {?} */
            var fieldsList = this.prepareFieldList(fields);
            return "SELECT " + fieldsList + " FROM " + tableName;
        };
        /**
         * @private
         * @param {?} fields
         * @return {?}
         */
        MySQLQueryBuilder.prototype.prepareFieldList = /**
         * @private
         * @param {?} fields
         * @return {?}
         */
        function (fields) {
            var e_2, _a;
            /** @type {?} */
            var prepared = [];
            try {
                for (var fields_1 = __values(fields), fields_1_1 = fields_1.next(); !fields_1_1.done; fields_1_1 = fields_1.next()) {
                    var field = fields_1_1.value;
                    /** @type {?} */
                    var isAlias = field.indexOf(' AS ') > -1;
                    prepared.push(isAlias ?
                        field :
                        '`' + field + '`');
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (fields_1_1 && !fields_1_1.done && (_a = fields_1.return)) _a.call(fields_1);
                }
                finally { if (e_2) throw e_2.error; }
            }
            return prepared.join(',');
        };
        /**
         * @template T
         * @param {?} params
         * @return {?}
         */
        MySQLQueryBuilder.prototype.update = /**
         * @template T
         * @param {?} params
         * @return {?}
         */
        function (params) {
            return __awaiter(this, void 0, void 0, function () {
                var statements, repository, updateStatements, remainingQuery, query;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (params.singleStatement) {
                                params.statements = [
                                    params.singleStatement
                                ];
                            }
                            statements = params.statements, repository = params.repository;
                            if (!statements ||
                                statements.length < 1) {
                                throw new Error('there must be at least 1 update statement!');
                            }
                            return [4 /*yield*/, this.prepareExpressionGroup(new UpdateStatements(statements))];
                        case 1:
                            updateStatements = _a.sent();
                            return [4 /*yield*/, this.prepareWhereOrderAndLimit(params)];
                        case 2:
                            remainingQuery = _a.sent();
                            query = 'UPDATE ' + repository.tableName +
                                ' SET ' + updateStatements +
                                remainingQuery + ';';
                            return [2 /*return*/, new MySQLQuery(query, this.connectionManager)];
                    }
                });
            });
        };
        /**
         * @private
         * @template T
         * @param {?} params
         * @return {?}
         */
        MySQLQueryBuilder.prototype.prepareWhereOrderAndLimit = /**
         * @private
         * @template T
         * @param {?} params
         * @return {?}
         */
        function (params) {
            return __awaiter(this, void 0, void 0, function () {
                var expressions, whereGroup, orderBy, limit, query, whereClause, _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (params.whereMap) {
                                params.whereAndMap = params.whereMap;
                            }
                            if (params.whereAndMap ||
                                params.whereOrMap) {
                                params.whereGroup = createExpressionGroup(params.whereAndMap ?
                                    params.whereAndMap :
                                    params.whereOrMap, params.whereAndMap ?
                                    ExpressionGroupJoiner.AND :
                                    ExpressionGroupJoiner.OR);
                            }
                            else if (params.wherePrimaryKeys) {
                                expressions = params.wherePrimaryKeys.map((/**
                                 * @param {?} primaryKeyValue
                                 * @return {?}
                                 */
                                function (primaryKeyValue) { return new StringStatement(params.repository.primaryKey, '' + primaryKeyValue); }));
                                params.whereGroup = new OrExpressionGroup(expressions);
                            }
                            else if (params.whereClause) {
                                params.whereGroup = new SingleExpression(params.whereClause);
                            }
                            whereGroup = params.whereGroup, orderBy = params.orderBy, limit = params.limit;
                            query = '';
                            _a = whereGroup;
                            if (!_a) return [3 /*break*/, 2];
                            return [4 /*yield*/, this.prepareExpressionGroup(whereGroup)];
                        case 1:
                            _a = (_b.sent());
                            _b.label = 2;
                        case 2:
                            whereClause = _a;
                            if (whereClause) {
                                query += "\n                WHERE " + whereClause;
                            }
                            if (orderBy) {
                                query += "\n                ORDER BY " + orderBy;
                            }
                            if (limit) {
                                query += "\n                LIMIT " + limit;
                            }
                            return [2 /*return*/, query];
                    }
                });
            });
        };
        /**
         * @private
         * @param {?} group
         * @return {?}
         */
        MySQLQueryBuilder.prototype.prepareExpressionGroup = /**
         * @private
         * @param {?} group
         * @return {?}
         */
        function (group) {
            return __awaiter(this, void 0, void 0, function () {
                var statements, useBrackets, _a, _b, expression, leftSide, operator, rightSide, rightSideValue, _c, _d, e_3_1;
                var e_3, _e;
                return __generator(this, function (_f) {
                    switch (_f.label) {
                        case 0:
                            statements = [];
                            useBrackets = group.surroundExpressionWithBrackets;
                            _f.label = 1;
                        case 1:
                            _f.trys.push([1, 8, 9, 10]);
                            _a = __values(group.expressions), _b = _a.next();
                            _f.label = 2;
                        case 2:
                            if (!!_b.done) return [3 /*break*/, 7];
                            expression = _b.value;
                            leftSide = expression.leftSide, operator = expression.operator, rightSide = expression.rightSide;
                            if (!rightSide.isString) return [3 /*break*/, 4];
                            _d = "";
                            return [4 /*yield*/, this.escape((/** @type {?} */ (rightSide.data)))];
                        case 3:
                            _c = _d + (_f.sent());
                            return [3 /*break*/, 5];
                        case 4:
                            _c = rightSide.data;
                            _f.label = 5;
                        case 5:
                            rightSideValue = _c;
                            statements.push((useBrackets ? '(' : '') + " `" + leftSide + "` " + operator + " " + rightSideValue + " " + (useBrackets ? ')' : ''));
                            _f.label = 6;
                        case 6:
                            _b = _a.next();
                            return [3 /*break*/, 2];
                        case 7: return [3 /*break*/, 10];
                        case 8:
                            e_3_1 = _f.sent();
                            e_3 = { error: e_3_1 };
                            return [3 /*break*/, 10];
                        case 9:
                            try {
                                if (_b && !_b.done && (_e = _a.return)) _e.call(_a);
                            }
                            finally { if (e_3) throw e_3.error; }
                            return [7 /*endfinally*/];
                        case 10: return [2 /*return*/, statements.join(" " + group.joiner + " ")];
                    }
                });
            });
        };
        /**
         * @private
         * @param {?} value
         * @return {?}
         */
        MySQLQueryBuilder.prototype.escape = /**
         * @private
         * @param {?} value
         * @return {?}
         */
        function (value) {
            return __awaiter(this, void 0, void 0, function () {
                var connection;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.connectionManager.getConnection()];
                        case 1:
                            connection = _a.sent();
                            return [2 /*return*/, connection.escape(value)];
                    }
                });
            });
        };
        /**
         * @param {?} repository
         * @param {?} primaryKeyValues
         * @return {?}
         */
        MySQLQueryBuilder.prototype.delete = /**
         * @param {?} repository
         * @param {?} primaryKeyValues
         * @return {?}
         */
        function (repository, primaryKeyValues) {
            if (primaryKeyValues.length < 1) {
                throw new Error('Please specify which records to delete!');
            }
            /** @type {?} */
            var query = 'DELETE FROM ' + repository.tableName +
                constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
            return new MySQLQuery(query, this.connectionManager);
        };
        /**
         * @template T
         * @param {?} sql
         * @param {?=} argumentsToPrepare
         * @return {?}
         */
        MySQLQueryBuilder.prototype.rawQuery = /**
         * @template T
         * @param {?} sql
         * @param {?=} argumentsToPrepare
         * @return {?}
         */
        function (sql, argumentsToPrepare) {
            return new MySQLQuery(sql, this.connectionManager, argumentsToPrepare);
        };
        return MySQLQueryBuilder;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        MySQLQueryBuilder.prototype.connectionManager;
    }
    /**
     * @param {?} repository
     * @param {?} primaryKeyValues
     * @return {?}
     */
    function constructWhereWithPrimaryKeys(repository, primaryKeyValues) {
        var e_4, _a;
        /** @type {?} */
        var conditions = [];
        try {
            for (var primaryKeyValues_1 = __values(primaryKeyValues), primaryKeyValues_1_1 = primaryKeyValues_1.next(); !primaryKeyValues_1_1.done; primaryKeyValues_1_1 = primaryKeyValues_1.next()) {
                var primaryKeyValue = primaryKeyValues_1_1.value;
                if (repository.isPrimaryKeyAString) {
                    primaryKeyValue = '\'' + primaryKeyValue + '\'';
                }
                /** @type {?} */
                var condition = repository.primaryKey + ' = ' + primaryKeyValue;
                conditions.push(condition);
            }
        }
        catch (e_4_1) { e_4 = { error: e_4_1 }; }
        finally {
            try {
                if (primaryKeyValues_1_1 && !primaryKeyValues_1_1.done && (_a = primaryKeyValues_1.return)) _a.call(primaryKeyValues_1);
            }
            finally { if (e_4) throw e_4.error; }
        }
        return ' WHERE ' + conditions.join(' OR ');
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/database/engine/mysql/mysql-transaction-manager.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MySQLTransactionManager = /** @class */ (function () {
        function MySQLTransactionManager(config) {
            var _this = this;
            this.queue = [];
            this.isExecutingTransaction = false;
            /**
             * we need to manage a queue of transactions
             * to be executed in order
             * once the transaction either is successful or fails
             * then move on to the next
             */
            this.performNextTransaction = (/**
             * @return {?}
             */
            function () {
                if (_this.isExecutingTransaction ||
                    _this.queue.length < 1) {
                    return;
                }
                earnbetCommon.logWithTime('perform next transaction!');
                _this.isExecutingTransaction = true;
                // remove first element from queue and return it
                /** @type {?} */
                var executor = _this.queue.shift();
                executor.execute();
            });
            this.onTransactionCompleted = (/**
             * @return {?}
             */
            function () {
                earnbetCommon.logWithTime('onTransactionCompleted');
                _this.isExecutingTransaction = false;
                _this.performNextTransaction();
            });
            this.connectionManager = new MySQLConnectionManager(config);
        }
        /**
         * @param {?} queries
         * @return {?}
         */
        MySQLTransactionManager.prototype.performTransaction = /**
         * @param {?} queries
         * @return {?}
         */
        function (queries) {
            return this.performPreparedTransaction(queries.map((/**
             * @param {?} sql
             * @return {?}
             */
            function (sql) { return ({ sql: sql, argumentsToPrepare: undefined }); })));
        };
        /**
         * @param {?} data
         * @return {?}
         */
        MySQLTransactionManager.prototype.performPreparedTransaction = /**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            /*
                        we need to promise the result of each query executed
                        so that they can be inspected by consumer for Ids, etc
                        */
            /** @type {?} */
            var executor = new MySQLTransactionExecuter(data, this.performNextTransaction, this.onTransactionCompleted, this.connectionManager);
            // add executor to queue
            this.queue.push(executor);
            return new Promise(executor.init);
        };
        return MySQLTransactionManager;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        MySQLTransactionManager.prototype.queue;
        /**
         * @type {?}
         * @private
         */
        MySQLTransactionManager.prototype.isExecutingTransaction;
        /**
         * @type {?}
         * @private
         */
        MySQLTransactionManager.prototype.connectionManager;
        /**
         * we need to manage a queue of transactions
         * to be executed in order
         * once the transaction either is successful or fails
         * then move on to the next
         * @type {?}
         * @private
         */
        MySQLTransactionManager.prototype.performNextTransaction;
        /**
         * @type {?}
         * @private
         */
        MySQLTransactionManager.prototype.onTransactionCompleted;
    }
    var MySQLTransactionExecuter = /** @class */ (function () {
        function MySQLTransactionExecuter(queries, onInitHandler, onCompleteHandler, connectionManager) {
            var _this = this;
            this.queries = queries;
            this.onInitHandler = onInitHandler;
            this.onCompleteHandler = onCompleteHandler;
            this.connectionManager = connectionManager;
            this.init = (/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                _this.resolve = resolve;
                _this.reject = reject;
                _this.onInitHandler();
            });
        }
        /**
         * @return {?}
         */
        MySQLTransactionExecuter.prototype.execute = /**
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                var _this = this;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            // only need to get connection once?
                            _a = this;
                            return [4 /*yield*/, this.connectionManager.getConnection()];
                        case 1:
                            // only need to get connection once?
                            _a.connection = _b.sent();
                            this.connection.beginTransaction((/**
                             * @return {?}
                             */
                            function () { return __awaiter(_this, void 0, void 0, function () {
                                var results, _a, _b, _c, sql, argumentsToPrepare, result, e_1_1, e_2;
                                var e_1, _d;
                                return __generator(this, function (_e) {
                                    switch (_e.label) {
                                        case 0:
                                            _e.trys.push([0, 10, , 12]);
                                            results = [];
                                            _e.label = 1;
                                        case 1:
                                            _e.trys.push([1, 6, 7, 8]);
                                            _a = __values(this.queries), _b = _a.next();
                                            _e.label = 2;
                                        case 2:
                                            if (!!_b.done) return [3 /*break*/, 5];
                                            _c = _b.value, sql = _c.sql, argumentsToPrepare = _c.argumentsToPrepare;
                                            return [4 /*yield*/, executeQuery(sql, this.connection, argumentsToPrepare)];
                                        case 3:
                                            result = _e.sent();
                                            results.push(result);
                                            _e.label = 4;
                                        case 4:
                                            _b = _a.next();
                                            return [3 /*break*/, 2];
                                        case 5: return [3 /*break*/, 8];
                                        case 6:
                                            e_1_1 = _e.sent();
                                            e_1 = { error: e_1_1 };
                                            return [3 /*break*/, 8];
                                        case 7:
                                            try {
                                                if (_b && !_b.done && (_d = _a.return)) _d.call(_a);
                                            }
                                            finally { if (e_1) throw e_1.error; }
                                            return [7 /*endfinally*/];
                                        case 8: return [4 /*yield*/, this.commit()];
                                        case 9:
                                            _e.sent();
                                            this.resolve(results);
                                            return [3 /*break*/, 12];
                                        case 10:
                                            e_2 = _e.sent();
                                            return [4 /*yield*/, this.rollback()];
                                        case 11:
                                            _e.sent();
                                            this.reject(e_2);
                                            return [3 /*break*/, 12];
                                        case 12:
                                            this.onCompleteHandler();
                                            return [2 /*return*/];
                                    }
                                });
                            }); }));
                            return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * @private
         * @return {?}
         */
        MySQLTransactionExecuter.prototype.commit = /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                _this.connection.commit((/**
                 * @param {?} error
                 * @return {?}
                 */
                function (error) { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        if (error) {
                            earnbetCommon.logWithTime('ERROR in committing queries', this.queries);
                            return [2 /*return*/, reject(error)];
                        }
                        resolve();
                        return [2 /*return*/];
                    });
                }); }));
            }));
        };
        /**
         * @private
         * @return {?}
         */
        MySQLTransactionExecuter.prototype.rollback = /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            return new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            function (resolve) {
                _this.connection.rollback(resolve);
            }));
        };
        return MySQLTransactionExecuter;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        MySQLTransactionExecuter.prototype.resolve;
        /**
         * @type {?}
         * @private
         */
        MySQLTransactionExecuter.prototype.reject;
        /**
         * @type {?}
         * @private
         */
        MySQLTransactionExecuter.prototype.connection;
        /** @type {?} */
        MySQLTransactionExecuter.prototype.init;
        /**
         * @type {?}
         * @private
         */
        MySQLTransactionExecuter.prototype.queries;
        /**
         * @type {?}
         * @private
         */
        MySQLTransactionExecuter.prototype.onInitHandler;
        /**
         * @type {?}
         * @private
         */
        MySQLTransactionExecuter.prototype.onCompleteHandler;
        /**
         * @type {?}
         * @private
         */
        MySQLTransactionExecuter.prototype.connectionManager;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/database/engine/mysql/mysql-database.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MySQLDatabase = /** @class */ (function () {
        function MySQLDatabase(config) {
            this.builder = new MySQLQueryBuilder(config);
            this.transactionManager = new MySQLTransactionManager(config);
        }
        return MySQLDatabase;
    }());
    if (false) {
        /** @type {?} */
        MySQLDatabase.prototype.builder;
        /** @type {?} */
        MySQLDatabase.prototype.transactionManager;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/database/engine/mysql/mysql-repository.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @template TypeForSelect
     */
    var   /**
     * @template TypeForSelect
     */
    MySQLRepository = /** @class */ (function () {
        function MySQLRepository(db, _tableName, defaultFieldsToSelect, _primaryKey, isPrimaryKeyAString) {
            if (defaultFieldsToSelect === void 0) { defaultFieldsToSelect = []; }
            if (_primaryKey === void 0) { _primaryKey = 'id'; }
            if (isPrimaryKeyAString === void 0) { isPrimaryKeyAString = false; }
            this.db = db;
            this._tableName = _tableName;
            this.defaultFieldsToSelect = defaultFieldsToSelect;
            this._primaryKey = _primaryKey;
            this.isPrimaryKeyAString = isPrimaryKeyAString;
        }
        /**
         * @template T
         * @param {?} entity
         * @return {?}
         */
        MySQLRepository.prototype.insert = /**
         * @template T
         * @param {?} entity
         * @return {?}
         */
        function (entity) {
            return this.insertMany([entity]);
        };
        /**
         * @template T
         * @param {?} entities
         * @return {?}
         */
        MySQLRepository.prototype.insertMany = /**
         * @template T
         * @param {?} entities
         * @return {?}
         */
        function (entities) {
            return __awaiter(this, void 0, void 0, function () {
                var query;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.buildInsert(entities)];
                        case 1:
                            query = _a.sent();
                            return [4 /*yield*/, query.execute()];
                        case 2: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        /**
         * @template T
         * @param {?} entities
         * @return {?}
         */
        MySQLRepository.prototype.buildInsert = /**
         * @template T
         * @param {?} entities
         * @return {?}
         */
        function (entities) {
            return this.db.builder.insert(this, entities);
        };
        /**
         * @param {?} primaryKeyValue
         * @param {?=} fields
         * @return {?}
         */
        MySQLRepository.prototype.selectOneByPrimaryKey = /**
         * @param {?} primaryKeyValue
         * @param {?=} fields
         * @return {?}
         */
        function (primaryKeyValue, fields) {
            if (fields === void 0) { fields = this.defaultFieldsToSelect; }
            return __awaiter(this, void 0, void 0, function () {
                var rows;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.selectManyByPrimaryKeys([primaryKeyValue], fields)];
                        case 1:
                            rows = _a.sent();
                            return [2 /*return*/, rows.length > 0 ?
                                    rows[0] :
                                    null];
                    }
                });
            });
        };
        /**
         * @param {?} primaryKeyValues
         * @param {?=} fields
         * @return {?}
         */
        MySQLRepository.prototype.selectManyByPrimaryKeys = /**
         * @param {?} primaryKeyValues
         * @param {?=} fields
         * @return {?}
         */
        function (primaryKeyValues, fields) {
            if (fields === void 0) { fields = this.defaultFieldsToSelect; }
            return this.db.builder.selectByPrimaryKeys(this, fields, primaryKeyValues).execute();
        };
        /**
         * @param {?=} fields
         * @return {?}
         */
        MySQLRepository.prototype.selectAll = /**
         * @param {?=} fields
         * @return {?}
         */
        function (fields) {
            if (fields === void 0) { fields = this.defaultFieldsToSelect; }
            return this.db.builder.selectAll(this, fields).execute();
        };
        /**
         * @param {?} params
         * @return {?}
         */
        MySQLRepository.prototype.selectOne = /**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            return __awaiter(this, void 0, void 0, function () {
                var rows;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.select(__assign({}, params, { limit: 1 }))];
                        case 1:
                            rows = _a.sent();
                            return [2 /*return*/, rows.length > 0 ?
                                    rows[0] :
                                    null];
                    }
                });
            });
        };
        /**
         * @param {?} params
         * @return {?}
         */
        MySQLRepository.prototype.select = /**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            return __awaiter(this, void 0, void 0, function () {
                var query;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.buildSelect(params)];
                        case 1:
                            query = _a.sent();
                            return [2 /*return*/, query.execute()];
                    }
                });
            });
        };
        /**
         * @param {?} params
         * @return {?}
         */
        MySQLRepository.prototype.buildSelect = /**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            /** @type {?} */
            var fields = params.fields ?
                params.fields :
                this.defaultFieldsToSelect;
            /** @type {?} */
            var newParams = __assign({}, params, { fields: fields, repository: this });
            return this.db.builder.select(newParams);
        };
        /**
         * @param {?} params
         * @return {?}
         */
        MySQLRepository.prototype.updateOne = /**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            return __awaiter(this, void 0, void 0, function () {
                var query;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.buildUpdateOne(params)];
                        case 1:
                            query = _a.sent();
                            return [2 /*return*/, query.execute()];
                    }
                });
            });
        };
        /**
         * @param {?} params
         * @return {?}
         */
        MySQLRepository.prototype.buildUpdateOne = /**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            return this.buildUpdate(__assign({}, params, { limit: 1 }));
        };
        /**
         * @param {?} params
         * @return {?}
         */
        MySQLRepository.prototype.update = /**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            return __awaiter(this, void 0, void 0, function () {
                var query;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.buildUpdate(params)];
                        case 1:
                            query = _a.sent();
                            return [2 /*return*/, query.execute()];
                    }
                });
            });
        };
        /**
         * @param {?} params
         * @return {?}
         */
        MySQLRepository.prototype.buildUpdate = /**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            /** @type {?} */
            var builderParams = __assign({}, params, { repository: this });
            return this.db.builder.update(builderParams);
        };
        /**
         * @param {?} primaryKeyValues
         * @return {?}
         */
        MySQLRepository.prototype.delete = /**
         * @param {?} primaryKeyValues
         * @return {?}
         */
        function (primaryKeyValues) {
            return this.db.builder.delete(this, primaryKeyValues).execute();
        };
        Object.defineProperty(MySQLRepository.prototype, "tableName", {
            get: /**
             * @return {?}
             */
            function () {
                return this._tableName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MySQLRepository.prototype, "primaryKey", {
            get: /**
             * @return {?}
             */
            function () {
                return this._primaryKey;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MySQLRepository.prototype, "primaryKeyFieldList", {
            get: /**
             * @private
             * @return {?}
             */
            function () {
                return [this.primaryKey];
            },
            enumerable: true,
            configurable: true
        });
        return MySQLRepository;
    }());
    if (false) {
        /**
         * @type {?}
         * @protected
         */
        MySQLRepository.prototype.db;
        /**
         * @type {?}
         * @private
         */
        MySQLRepository.prototype._tableName;
        /**
         * @type {?}
         * @protected
         */
        MySQLRepository.prototype.defaultFieldsToSelect;
        /**
         * @type {?}
         * @private
         */
        MySQLRepository.prototype._primaryKey;
        /** @type {?} */
        MySQLRepository.prototype.isPrimaryKeyAString;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/database/coins.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {number} */
    var CoinId = {
        BTC: 0,
        ETH: 1,
        EOS: 2,
        BET_BINANCE: 3,
        LTC: 4,
        XRP: 5,
        BCH: 6,
        BNB: 7,
        WAX: 8,
        TRX: 9,
        LINK: 10,
        BET_ETHEREUM: 11,
        DAI: 12,
        USDC: 13,
        USDT: 14,
        STACK: 15,
    };
    CoinId[CoinId.BTC] = 'BTC';
    CoinId[CoinId.ETH] = 'ETH';
    CoinId[CoinId.EOS] = 'EOS';
    CoinId[CoinId.BET_BINANCE] = 'BET_BINANCE';
    CoinId[CoinId.LTC] = 'LTC';
    CoinId[CoinId.XRP] = 'XRP';
    CoinId[CoinId.BCH] = 'BCH';
    CoinId[CoinId.BNB] = 'BNB';
    CoinId[CoinId.WAX] = 'WAX';
    CoinId[CoinId.TRX] = 'TRX';
    CoinId[CoinId.LINK] = 'LINK';
    CoinId[CoinId.BET_ETHEREUM] = 'BET_ETHEREUM';
    CoinId[CoinId.DAI] = 'DAI';
    CoinId[CoinId.USDC] = 'USDC';
    CoinId[CoinId.USDT] = 'USDT';
    CoinId[CoinId.STACK] = 'STACK';
    /** @enum {string} */
    var CoinSymbol = {
        BTC: "BTC",
        ETH: "ETH",
        EOS: "EOS",
        BET: "BET",
        LTC: "LTC",
        XRP: "XRP",
        BCH: "BCH",
        BNB: "BNB",
        WAX: "WAX",
        TRX: "TRX",
        LINK: "LINK",
        DAI: "DAI",
        USDC: "USDC",
        USDT: "USDT",
    };

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/mnemonic/mnemonic-util.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MnemonicUtility = /** @class */ (function () {
        function MnemonicUtility(_words, network) {
            this._words = _words;
            this.network = network;
            this.isInit = false;
            this.root = undefined;
        }
        Object.defineProperty(MnemonicUtility.prototype, "words", {
            get: /**
             * @return {?}
             */
            function () {
                return this._words;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        MnemonicUtility.prototype.init = /**
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var seed;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (this.isInit) {
                                return [2 /*return*/];
                            }
                            this.isInit = true;
                            return [4 /*yield*/, bip39.mnemonicToSeed(this.words)];
                        case 1:
                            seed = _a.sent();
                            this.root = bip32.fromSeed(seed, this.network);
                            return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * @param {?} coinId
         * @return {?}
         */
        MnemonicUtility.prototype.getNodeForChangeAddress = /**
         * @param {?} coinId
         * @return {?}
         */
        function (coinId) {
            /** @type {?} */
            var basePath = getBasePath(coinId);
            // console.log(basePath);
            switch (coinId) {
                case CoinId.BTC:
                case CoinId.LTC:
                case CoinId.BCH:
                    basePath = basePath.substr(0, basePath.length - 2) + '1/';
                    break;
            }
            /** @type {?} */
            var path = basePath + '0';
            // console.log(path);
            /** @type {?} */
            var node = this.root.derivePath(path);
            return node;
        };
        /**
         * @param {?} coinId
         * @param {?} userId
         * @return {?}
         */
        MnemonicUtility.prototype.getNodeForDepositAddress = /**
         * @param {?} coinId
         * @param {?} userId
         * @return {?}
         */
        function (coinId, userId) {
            /** @type {?} */
            var path = getBasePath(coinId) + userId;
            return this.root.derivePath(path);
        };
        /**
         * @param {?} userId
         * @param {?} coinId
         * @param {?} prefix
         * @return {?}
         */
        MnemonicUtility.prototype.getDepositMemo = /**
         * @param {?} userId
         * @param {?} coinId
         * @param {?} prefix
         * @return {?}
         */
        function (userId, coinId, prefix) {
            /** @type {?} */
            var node = this.getNodeForDepositAddress(coinId, userId);
            /** @type {?} */
            var memo = bs58.encode(node.publicKey.slice(0, 12)).toLowerCase();
            return "" + prefix + memo;
        };
        return MnemonicUtility;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        MnemonicUtility.prototype.isInit;
        /**
         * @type {?}
         * @private
         */
        MnemonicUtility.prototype.root;
        /**
         * @type {?}
         * @private
         */
        MnemonicUtility.prototype._words;
        /**
         * @type {?}
         * @private
         */
        MnemonicUtility.prototype.network;
    }
    /**
     * the MnemonicUtility class should be a singleton: **
     * @type {?}
     */
    var mnemonicUtility;
    /** @type {?} */
    var allowedNetworkNames = ['mainnet', 'testnet', 'regtest'];
    /**
     * @param {?} config
     * @return {?}
     * @this {*}
     */
    function getMnemonicUtil(config) {
        return __awaiter(this, void 0, void 0, function () {
            var words, networkName;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!mnemonicUtility) return [3 /*break*/, 2];
                        words = config.mnemonicPhrase;
                        networkName = config.blockchainNetwork;
                        if (allowedNetworkNames.indexOf(networkName) < 0) {
                            throw new Error(networkName + " network not supported.");
                        }
                        mnemonicUtility = new MnemonicUtility(words, bitcoinjsLib.networks[networkName]);
                        return [4 /*yield*/, mnemonicUtility.init()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/, mnemonicUtility];
                }
            });
        });
    }
    /**
     *
     * @param {?} coinId
     * @return {?}
     */
    function getBasePath(coinId) {
        switch (coinId) {
            case CoinId.BTC:
                return 'm/49\'/0\'/0\'/0/';
            case CoinId.LTC:
                return 'm/49\'/2\'/0\'/0/';
            case CoinId.BCH:
                return 'm/44\'/145\'/0\'/0/';
            case CoinId.ETH:
                return 'm/44\'/60\'/0\'/0/';
            case CoinId.BNB:
            case CoinId.BET_BINANCE:
                // https://docs.binance.org/blockchain.html
                // derive the private key using BIP32 / BIP44 with HD prefix as "44'/714'/", which is reserved at SLIP 44.
                // 714 comes from Binance's birthday, July 14th
                return "m/44'/714'/0'/0/";
            case CoinId.XRP:
                return "m/44'/144'/0'/0/";
            case CoinId.TRX:
                return "m/44'/195'/0'/0/";
            case CoinId.EOS:
                return "m/44'/194'/0'/0/";
            default:
                throw new Error('CoinId NOT SUPPORTED: ' + coinId);
        }
    }
    /**
     * @return {?}
     */
    function generateMnemonicPhrase() {
        return bip39.generateMnemonic(256);
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/mnemonic/interfaces.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var MemoPrefix = {
        EOSBET: "EOSBET_",
        EARNBET: "EARNBET_",
        XRP: "EARNBET_XRP_",
    };
    /**
     * @record
     */
    function IMnemonicUtility() { }
    if (false) {
        /** @type {?} */
        IMnemonicUtility.prototype.words;
        /**
         * @param {?} userId
         * @param {?} coinId
         * @param {?} prefix
         * @return {?}
         */
        IMnemonicUtility.prototype.getDepositMemo = function (userId, coinId, prefix) { };
        /**
         * @param {?} coinId
         * @return {?}
         */
        IMnemonicUtility.prototype.getNodeForChangeAddress = function (coinId) { };
        /**
         * @param {?} coinId
         * @param {?} userId
         * @return {?}
         */
        IMnemonicUtility.prototype.getNodeForDepositAddress = function (coinId, userId) { };
    }
    /**
     * @record
     */
    function IMnemonicUtilityConfig() { }
    if (false) {
        /** @type {?} */
        IMnemonicUtilityConfig.prototype.mnemonicPhrase;
        /** @type {?} */
        IMnemonicUtilityConfig.prototype.blockchainNetwork;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/eos/eos-settings.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IEosConfig() { }
    if (false) {
        /** @type {?} */
        IEosConfig.prototype.eosjs;
        /** @type {?} */
        IEosConfig.prototype.endpoints;
        /** @type {?|undefined} */
        IEosConfig.prototype.messageSigningPrivateKey;
    }
    /**
     * @record
     */
    function IEosAccountNamesBase() { }
    if (false) {
        /** @type {?} */
        IEosAccountNamesBase.prototype.easyAccount;
        /** @type {?} */
        IEosAccountNamesBase.prototype.withdrawal;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/eos/eos-txn-executor.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var _1HoursInSeconds = 60 * 60;
    var EosTransactionExecutorBase = /** @class */ (function () {
        function EosTransactionExecutorBase(actions, eos, txnChecker, retryOnError, secondsBeforeRetry, expireInSeconds) {
            if (retryOnError === void 0) { retryOnError = true; }
            if (secondsBeforeRetry === void 0) { secondsBeforeRetry = 10; }
            if (expireInSeconds === void 0) { expireInSeconds = _1HoursInSeconds; }
            this.actions = actions;
            this.eos = eos;
            this.txnChecker = txnChecker;
            this.retryOnError = retryOnError;
            this.secondsBeforeRetry = secondsBeforeRetry;
            this.expireInSeconds = expireInSeconds;
            this.numOfRetries = 0;
            this.signedTransaction = undefined;
            // include first action for now
            // include other actions as well?
            var _a = this.actions[0], account = _a.account, name = _a.name;
            this.actionId = account + ' - ' + name;
        }
        /**
         * @return {?}
         */
        EosTransactionExecutorBase.prototype.execute = /**
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a, error_1;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            earnbetCommon.debugMessage('Attempting to Execute Transaction: ' + this.actionId);
                            console.log(this.actions);
                            _b.label = 1;
                        case 1:
                            _b.trys.push([1, 4, , 5]);
                            return [4 /*yield*/, this.sign()];
                        case 2:
                            _b.sent();
                            // attempt to broadcast
                            _a = this;
                            return [4 /*yield*/, broadcastSignedTransaction(this.eos, this.signedTransaction.transaction)];
                        case 3:
                            // attempt to broadcast
                            _a.broadcastedTxnId = _b.sent();
                            this.broadcastedTime = Date.now();
                            earnbetCommon.debugMessage(this.actionId + ' Broadcasted: ' + this.broadcastedTxnId);
                            return [2 /*return*/, this.broadcastedTxnId];
                        case 4:
                            error_1 = _b.sent();
                            return [2 /*return*/, this.onBroadcastError(error_1)];
                        case 5: return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * @return {?}
         */
        EosTransactionExecutorBase.prototype.sign = /**
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a, error_2;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            _b.trys.push([0, 3, , 5]);
                            if (!(this.signedTransaction == undefined)) return [3 /*break*/, 2];
                            _a = this;
                            return [4 /*yield*/, signTransaction(this.eos, this.actions, this.expireInSeconds)];
                        case 1:
                            _a.signedTransaction = _b.sent();
                            _b.label = 2;
                        case 2:
                            earnbetCommon.debugMessage('Transaction Signed: ' + this.signedTransaction.transaction_id);
                            return [2 /*return*/, this.signedTransaction];
                        case 3:
                            error_2 = _b.sent();
                            console.error(error_2);
                            return [4 /*yield*/, earnbetCommon.sleep(1000)];
                        case 4:
                            _b.sent();
                            return [2 /*return*/, this.sign()];
                        case 5: return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * @protected
         * @param {?} error
         * @return {?}
         */
        EosTransactionExecutorBase.prototype.onBroadcastError = /**
         * @protected
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return __awaiter(this, void 0, void 0, function () {
                var isDuplicateTxn, maxNumOfRetries;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            console.error(error);
                            isDuplicateTxn = isDuplicateTxnError(error);
                            // if duplicate error, then stop broadcasting process
                            // return originally broadcasted txnId
                            if (isDuplicateTxn) {
                                return [2 /*return*/, this.broadcastedTxnId];
                            }
                            if (!this.retryOnError) return [3 /*break*/, 2];
                            maxNumOfRetries = (_1HoursInSeconds / this.secondsBeforeRetry) - 1;
                            if (!(this.numOfRetries++ < maxNumOfRetries)) return [3 /*break*/, 2];
                            // wait and then retry
                            return [4 /*yield*/, earnbetCommon.sleep(1000 * this.secondsBeforeRetry)];
                        case 1:
                            // wait and then retry
                            _a.sent();
                            return [2 /*return*/, this.execute()];
                        case 2: throw error;
                    }
                });
            });
        };
        /**
         * @return {?}
         */
        EosTransactionExecutorBase.prototype.confirm = /**
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var timeToWait, remainingTimeToWait, txnId, isConfirmed, error_3;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!this.broadcastedTxnId) {
                                throw new Error('Transaction has not yet been succesfully broadcasted!');
                            }
                            if (!this.txnChecker) {
                                throw new Error('no Transaction Checker function defined!');
                            }
                            // wait 5 minutes before checking irreversibility
                            timeToWait = 5 * 60 * 1000;
                            remainingTimeToWait = timeToWait -
                                (Date.now() - this.broadcastedTime);
                            console.log({ remainingTimeToWait: remainingTimeToWait });
                            if (!(remainingTimeToWait > 0)) return [3 /*break*/, 2];
                            return [4 /*yield*/, earnbetCommon.sleep(remainingTimeToWait)];
                        case 1:
                            _a.sent();
                            _a.label = 2;
                        case 2:
                            txnId = this.broadcastedTxnId;
                            _a.label = 3;
                        case 3:
                            _a.trys.push([3, 5, , 7]);
                            return [4 /*yield*/, this.txnChecker(txnId)];
                        case 4:
                            isConfirmed = _a.sent();
                            if (!isConfirmed) {
                                earnbetCommon.debugMessage(this.actionId + ' Txn: ' + txnId + ' is NOT CONFIRMED!');
                                return [2 /*return*/, this.execute()];
                            }
                            earnbetCommon.debugMessage(this.actionId + ' Txn: ' + txnId + ' is IRREVERSIBLE!');
                            return [2 /*return*/, txnId];
                        case 5:
                            error_3 = _a.sent();
                            earnbetCommon.debugMessage('Cound Not Confirm Txn: ' + txnId);
                            console.error(error_3);
                            // wait
                            return [4 /*yield*/, earnbetCommon.sleep(5 * 1000)];
                        case 6:
                            // wait
                            _a.sent();
                            return [2 /*return*/, this.execute()];
                        case 7: return [2 /*return*/];
                    }
                });
            });
        };
        return EosTransactionExecutorBase;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        EosTransactionExecutorBase.prototype.numOfRetries;
        /**
         * @type {?}
         * @protected
         */
        EosTransactionExecutorBase.prototype.actionId;
        /**
         * @type {?}
         * @private
         */
        EosTransactionExecutorBase.prototype.signedTransaction;
        /**
         * @type {?}
         * @private
         */
        EosTransactionExecutorBase.prototype.broadcastedTxnId;
        /**
         * @type {?}
         * @private
         */
        EosTransactionExecutorBase.prototype.broadcastedTime;
        /** @type {?} */
        EosTransactionExecutorBase.prototype.actions;
        /**
         * @type {?}
         * @private
         */
        EosTransactionExecutorBase.prototype.eos;
        /**
         * @type {?}
         * @private
         */
        EosTransactionExecutorBase.prototype.txnChecker;
        /**
         * @type {?}
         * @private
         */
        EosTransactionExecutorBase.prototype.retryOnError;
        /**
         * @type {?}
         * @private
         */
        EosTransactionExecutorBase.prototype.secondsBeforeRetry;
        /**
         * @type {?}
         * @private
         */
        EosTransactionExecutorBase.prototype.expireInSeconds;
    }
    /**
     * @template T
     */
    var   /**
     * @template T
     */
    EosTransactionExecutor = /** @class */ (function (_super) {
        __extends(EosTransactionExecutor, _super);
        function EosTransactionExecutor(actions, eosUtility, txnChecker, retryOnError, secondsBeforeRetry) {
            if (retryOnError === void 0) { retryOnError = true; }
            if (secondsBeforeRetry === void 0) { secondsBeforeRetry = 10; }
            return _super.call(this, actions.map((/**
             * @param {?} action
             * @return {?}
             */
            function (action) { return (__assign({}, action, { authorization: eosUtility.authorization })); })), eosUtility.transactor, txnChecker, retryOnError, secondsBeforeRetry) || this;
        }
        return EosTransactionExecutor;
    }(EosTransactionExecutorBase));
    /*
    Error: {"code":409,"message":"Conflict","error":{"code":3040008,"name":"tx_duplicate","what":"Duplicate transaction","details":[{"message":"duplicate transaction 7be2845618fa965d89ff8f3b368ed1c768dab63303c38c7d8a5e9f031d7a23c6","file":"producer_plugin.cpp","line_number":527,"method":"process_incoming_transaction_async"}]}}
    */
    /**
     * @param {?} error
     * @return {?}
     */
    function isDuplicateTxnError(error) {
        var e_1, _a;
        /** @type {?} */
        var searchStrings = [
            'tx_duplicate',
            'Duplicate transaction',
            'duplicate transaction'
        ];
        /** @type {?} */
        var errorString = String(error);
        try {
            for (var searchStrings_1 = __values(searchStrings), searchStrings_1_1 = searchStrings_1.next(); !searchStrings_1_1.done; searchStrings_1_1 = searchStrings_1.next()) {
                var searchString = searchStrings_1_1.value;
                if (errorString.indexOf(searchString) > -1) {
                    return true;
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (searchStrings_1_1 && !searchStrings_1_1.done && (_a = searchStrings_1.return)) _a.call(searchStrings_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return false;
    }
    /**
     * @record
     */
    function ISignedTransaction() { }
    if (false) {
        /** @type {?} */
        ISignedTransaction.prototype.compression;
        /** @type {?} */
        ISignedTransaction.prototype.transaction;
        /** @type {?} */
        ISignedTransaction.prototype.signatures;
    }
    /**
     * @record
     */
    function ISignedTransactionResult() { }
    if (false) {
        /** @type {?} */
        ISignedTransactionResult.prototype.transaction_id;
        /** @type {?} */
        ISignedTransactionResult.prototype.transaction;
    }
    /**
     * @record
     */
    function IBroadcastedTransactionResult() { }
    if (false) {
        /** @type {?} */
        IBroadcastedTransactionResult.prototype.transaction_id;
    }
    /**
     * @param {?} transactor
     * @param {?} actions
     * @param {?} expireInSeconds
     * @return {?}
     * @this {*}
     */
    function signTransaction(transactor, actions, expireInSeconds) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, transactor.transaction({
                            actions: actions
                        }, {
                            broadcast: false,
                            sign: true,
                            expireInSeconds: expireInSeconds
                        })];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    }
    /**
     * @param {?} transactor
     * @param {?} transaction
     * @return {?}
     * @this {*}
     */
    function broadcastSignedTransaction(transactor, transaction) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, transactor.pushTransaction(transaction)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, result.transaction_id];
                }
            });
        });
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/eos/interfaces.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IEOSInfo() { }
    if (false) {
        /** @type {?} */
        IEOSInfo.prototype.server_version;
        /** @type {?} */
        IEOSInfo.prototype.chain_id;
        /** @type {?} */
        IEOSInfo.prototype.head_block_num;
        /** @type {?} */
        IEOSInfo.prototype.last_irreversible_block_num;
        /** @type {?} */
        IEOSInfo.prototype.last_irreversible_block_id;
        /** @type {?} */
        IEOSInfo.prototype.head_block_id;
        /** @type {?} */
        IEOSInfo.prototype.head_block_time;
        /** @type {?} */
        IEOSInfo.prototype.head_block_producer;
        /** @type {?} */
        IEOSInfo.prototype.virtual_block_cpu_limit;
        /** @type {?} */
        IEOSInfo.prototype.virtual_block_net_limit;
        /** @type {?} */
        IEOSInfo.prototype.block_cpu_limit;
        /** @type {?} */
        IEOSInfo.prototype.block_net_limit;
        /** @type {?} */
        IEOSInfo.prototype.server_version_string;
    }
    /**
     * @record
     * @template T
     */
    function IEosUtilityParams() { }
    if (false) {
        /** @type {?} */
        IEosUtilityParams.prototype.chainId;
        /** @type {?} */
        IEosUtilityParams.prototype.config;
        /** @type {?} */
        IEosUtilityParams.prototype.contracts;
        /** @type {?} */
        IEosUtilityParams.prototype.authorization;
    }
    /**
     * @record
     * @template T
     */
    function IEosUtility() { }
    if (false) {
        /** @type {?} */
        IEosUtility.prototype.chainId;
        /** @type {?} */
        IEosUtility.prototype.config;
        /** @type {?} */
        IEosUtility.prototype.contracts;
        /** @type {?} */
        IEosUtility.prototype.authorization;
        /** @type {?} */
        IEosUtility.prototype.transactor;
        /**
         * @return {?}
         */
        IEosUtility.prototype.getInfo = function () { };
        /**
         * @param {?} contract
         * @param {?} account
         * @param {?} symbol
         * @param {?=} table
         * @return {?}
         */
        IEosUtility.prototype.getTokenBalance = function (contract, account, symbol, table) { };
        /**
         * @template T
         * @param {?} code
         * @param {?} scope
         * @param {?} table
         * @param {?} rowId
         * @return {?}
         */
        IEosUtility.prototype.getUniqueTableRow = function (code, scope, table, rowId) { };
        /**
         * @template T
         * @param {?} code
         * @param {?} scope
         * @param {?} table
         * @param {?} key
         * @return {?}
         */
        IEosUtility.prototype.getAllTableRows = function (code, scope, table, key) { };
        /**
         * @template T
         * @param {?} parameters
         * @return {?}
         */
        IEosUtility.prototype.getTableRows = function (parameters) { };
        /**
         * @param {?} actions
         * @return {?}
         */
        IEosUtility.prototype.executeTransaction = function (actions) { };
        /**
         * @param {?} eosAccountName
         * @return {?}
         */
        IEosUtility.prototype.isAccountSafe = function (eosAccountName) { };
        /**
         * @param {?} name
         * @return {?}
         */
        IEosUtility.prototype.isEasyAccountContract = function (name) { };
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/eos/irreversible.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var waxDatabase;
    /** @type {?} */
    var eosDatabase;
    /**
     * @param {?} txnId
     * @return {?}
     * @this {*}
     */
    function isWaxTxnIrreversible(txnId) {
        return __awaiter(this, void 0, void 0, function () {
            var waxDatabase;
            return __generator(this, function (_a) {
                waxDatabase = getWaxDatabase();
                return [2 /*return*/, isTxnIrreversible(waxDatabase, txnId)];
            });
        });
    }
    /**
     * @param {?} txnId
     * @return {?}
     * @this {*}
     */
    function isEosTxnIrreversible(txnId) {
        return __awaiter(this, void 0, void 0, function () {
            var eosDatabase;
            return __generator(this, function (_a) {
                eosDatabase = getEosDatabase();
                return [2 /*return*/, isTxnIrreversible(eosDatabase, txnId)];
            });
        });
    }
    /**
     * @return {?}
     */
    function getWaxDatabase() {
        if (waxDatabase == undefined) {
            if (process.env.WAX_IRREV_DB_HOSTNAME == undefined ||
                process.env.WAX_IRREV_DB_PORT == undefined ||
                process.env.WAX_IRREV_DB_USERNAME == undefined ||
                process.env.WAX_IRREV_DB_PASSWORD == undefined ||
                process.env.WAX_IRREV_DB_NAME == undefined) {
                throw new Error('must define ENV VARS for WAX_IRREV_DB!');
            }
            waxDatabase = new MySQLDatabase({
                host: process.env.WAX_IRREV_DB_HOSTNAME,
                port: Number(process.env.WAX_IRREV_DB_PORT),
                user: process.env.WAX_IRREV_DB_USERNAME,
                password: process.env.WAX_IRREV_DB_PASSWORD,
                database: process.env.WAX_IRREV_DB_NAME,
                charset: 'utf8',
            });
        }
        return waxDatabase;
    }
    /**
     * @return {?}
     */
    function getEosDatabase() {
        if (eosDatabase == undefined) {
            if (process.env.EOS_IRREV_DB_HOSTNAME == undefined ||
                process.env.EOS_IRREV_DB_PORT == undefined ||
                process.env.EOS_IRREV_DB_USERNAME == undefined ||
                process.env.EOS_IRREV_DB_PASSWORD == undefined ||
                process.env.EOS_IRREV_DB_NAME == undefined) {
                throw new Error('must define ENV VARS for EOS_IRREV_DB!');
            }
            eosDatabase = new MySQLDatabase({
                host: process.env.EOS_IRREV_DB_HOSTNAME,
                port: Number(process.env.EOS_IRREV_DB_PORT),
                user: process.env.EOS_IRREV_DB_USERNAME,
                password: process.env.EOS_IRREV_DB_PASSWORD,
                database: process.env.EOS_IRREV_DB_NAME,
                charset: 'utf8',
            });
        }
        return eosDatabase;
    }
    /**
     * @param {?} database
     * @param {?} txnId
     * @return {?}
     * @this {*}
     */
    function isTxnIrreversible(database, txnId) {
        return __awaiter(this, void 0, void 0, function () {
            var rows;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, database.builder.rawQuery("select block_num from IRREVTX where trx_id = '" + txnId.toLowerCase() + "' LIMIT 1").execute()];
                    case 1:
                        rows = _a.sent();
                        console.log(rows);
                        return [2 /*return*/, rows.length > 0];
                }
            });
        });
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/eos/eos-util.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} eosConfig
     * @param {?=} endpoint
     * @return {?}
     */
    function getEosJs(eosConfig, endpoint) {
        if (endpoint === void 0) { endpoint = undefined; }
        /** @type {?} */
        var cfg = Object.assign({}, eosConfig.eosjs);
        if (endpoint != undefined) {
            cfg.httpEndpoint = endpoint;
        }
        return Eos(cfg);
    }
    /**
     * @param {?} signedData
     * @param {?} info
     * @return {?}
     */
    function recoverPublicKey(signedData, info) {
        /** @type {?} */
        var recoveredPublicKey;
        try {
            if (!info.isSignedDataHashed) {
                recoveredPublicKey = eos_ecc.recover(info.signature, signedData);
            }
            else {
                /** @type {?} */
                var signedHash = eos_ecc.sha256(eos_ecc.sha256(signedData) +
                    eos_ecc.sha256(info.nonce));
                recoveredPublicKey = eos_ecc.recoverHash(info.signature, signedHash);
            }
        }
        catch (error) {
            console.error(error);
        }
        return recoveredPublicKey;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/eos/eos-util-class.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @template T
     */
    var   /**
     * @template T
     */
    EosUtility = /** @class */ (function () {
        function EosUtility(params) {
            var config = params.config;
            /** @type {?} */
            var endpoints = config.endpoints;
            this.poller = getEosJs(config, endpoints.poller);
            this.transactor = getEosJs(config, endpoints.transactor);
            this.chainId = params.chainId;
            this.config = config;
            this.contracts = params.contracts;
            this.authorization = params.authorization;
        }
        /**
         * @return {?}
         */
        EosUtility.prototype.getInfo = /**
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/, this.poller.getInfo({})];
                });
            });
        };
        /**
         * @template T
         * @param {?} code
         * @param {?} scope
         * @param {?} table
         * @param {?} rowId
         * @return {?}
         */
        EosUtility.prototype.getUniqueTableRow = /**
         * @template T
         * @param {?} code
         * @param {?} scope
         * @param {?} table
         * @param {?} rowId
         * @return {?}
         */
        function (code, scope, table, rowId) {
            return earnbetCommon.getUniqueTableRow(this.poller, code, scope, table, rowId);
        };
        /**
         * @template T
         * @param {?} code
         * @param {?} scope
         * @param {?} table
         * @param {?} key
         * @return {?}
         */
        EosUtility.prototype.getAllTableRows = /**
         * @template T
         * @param {?} code
         * @param {?} scope
         * @param {?} table
         * @param {?} key
         * @return {?}
         */
        function (code, scope, table, key) {
            return earnbetCommon.getAllTableRows(this.poller, code, scope, table, key);
        };
        /**
         * @template T
         * @param {?} parameters
         * @return {?}
         */
        EosUtility.prototype.getTableRows = /**
         * @template T
         * @param {?} parameters
         * @return {?}
         */
        function (parameters) {
            return earnbetCommon.getTableRows(this.poller, parameters);
        };
        /**
         * @param {?} contract
         * @param {?} account
         * @param {?} symbol
         * @param {?=} table
         * @return {?}
         */
        EosUtility.prototype.getTokenBalance = /**
         * @param {?} contract
         * @param {?} account
         * @param {?} symbol
         * @param {?=} table
         * @return {?}
         */
        function (contract, account, symbol, table) {
            if (table === void 0) { table = 'accounts'; }
            return earnbetCommon.getTokenBalance(this.poller, contract, account, symbol, table);
        };
        /**
         * @param {?} actions
         * @return {?}
         */
        EosUtility.prototype.executeTransaction = /**
         * @param {?} actions
         * @return {?}
         */
        function (actions) {
            return __awaiter(this, void 0, void 0, function () {
                var result, error_1;
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            _a.trys.push([0, 2, , 3]);
                            return [4 /*yield*/, this.transactor.transaction({
                                    actions: actions.map((/**
                                     * @param {?} action
                                     * @return {?}
                                     */
                                    function (action) { return (__assign({}, action, { authorization: _this.authorization })); }))
                                })];
                        case 1:
                            result = _a.sent();
                            return [2 /*return*/, result.transaction_id];
                        case 2:
                            error_1 = _a.sent();
                            throw error_1;
                        case 3: return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * @param {?} eosAccountName
         * @return {?}
         */
        EosUtility.prototype.isAccountSafe = /**
         * @param {?} eosAccountName
         * @return {?}
         */
        function (eosAccountName) {
            return __awaiter(this, void 0, void 0, function () {
                var result, error_2;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (this.isEasyAccountContract(eosAccountName)) {
                                return [2 /*return*/, true];
                            }
                            _a.label = 1;
                        case 1:
                            _a.trys.push([1, 3, , 4]);
                            return [4 /*yield*/, this.poller.getCodeHash(eosAccountName)];
                        case 2:
                            result = _a.sent();
                            return [2 /*return*/, result.code_hash == '0000000000000000000000000000000000000000000000000000000000000000'];
                        case 3:
                            error_2 = _a.sent();
                            return [2 /*return*/, false];
                        case 4: return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * @param {?} name
         * @return {?}
         */
        EosUtility.prototype.isEasyAccountContract = /**
         * @param {?} name
         * @return {?}
         */
        function (name) {
            return this.contracts.easyAccount != undefined &&
                name == this.contracts.easyAccount;
        };
        return EosUtility;
    }());
    if (false) {
        /** @type {?} */
        EosUtility.prototype.poller;
        /** @type {?} */
        EosUtility.prototype.transactor;
        /** @type {?} */
        EosUtility.prototype.chainId;
        /** @type {?} */
        EosUtility.prototype.config;
        /** @type {?} */
        EosUtility.prototype.contracts;
        /** @type {?} */
        EosUtility.prototype.authorization;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/database/models/withdrawal-request.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function INewWithdrawalRequestRow() { }
    if (false) {
        /** @type {?} */
        INewWithdrawalRequestRow.prototype.id;
        /** @type {?} */
        INewWithdrawalRequestRow.prototype.memo;
        /** @type {?} */
        INewWithdrawalRequestRow.prototype.coin_id;
        /** @type {?} */
        INewWithdrawalRequestRow.prototype.tx_hash;
        /** @type {?} */
        INewWithdrawalRequestRow.prototype.is_processed;
        /** @type {?} */
        INewWithdrawalRequestRow.prototype.easy_account_id;
        /** @type {?} */
        INewWithdrawalRequestRow.prototype.eos_account_name;
        /** @type {?} */
        INewWithdrawalRequestRow.prototype.withdraw_address;
        /** @type {?} */
        INewWithdrawalRequestRow.prototype.coin_decimal_amount;
    }
    /**
     * @record
     */
    function IFailedWithdrawalRequestRow() { }
    if (false) {
        /** @type {?} */
        IFailedWithdrawalRequestRow.prototype.error_message;
    }
    /**
     * @record
     */
    function ISucceededWithdrawalRequest() { }
    if (false) {
        /** @type {?} */
        ISucceededWithdrawalRequest.prototype.processed_at;
        /** @type {?} */
        ISucceededWithdrawalRequest.prototype.withdraw_transaction_id;
    }
    /**
     * @record
     */
    function IWithdrawalRequestInsufficientFunds() { }
    if (false) {
        /** @type {?} */
        IWithdrawalRequestInsufficientFunds.prototype.insufficient_funds;
    }
    /**
     * @record
     */
    function ISavedWithdrawalRequestRow() { }
    if (false) {
        /** @type {?} */
        ISavedWithdrawalRequestRow.prototype.requested_at;
    }
    /**
     * @record
     */
    function IWithdrawalRequest() { }
    if (false) {
        /** @type {?} */
        IWithdrawalRequest.prototype.address;
        /** @type {?} */
        IWithdrawalRequest.prototype.memo;
        /**
         * @return {?}
         */
        IWithdrawalRequest.prototype.getAmount = function () { };
    }
    /**
     * @record
     */
    function ISavedWithdrawalRequest() { }
    if (false) {
        /**
         * @return {?}
         */
        ISavedWithdrawalRequest.prototype.getRefundParams = function () { };
    }
    var SavedWithdrawalRequest = /** @class */ (function () {
        function SavedWithdrawalRequest(data, account, amountFactory) {
            this.account = account;
            this.amountFactory = amountFactory;
            this.userId = undefined;
            this.amount = undefined;
            this.id = data.id;
            this.coinId = data.coin_id;
            this.decimalAmount = data.coin_decimal_amount;
            this.transactionHash = data.tx_hash;
            this.address = data.withdraw_address;
            this.memo = data.memo ? data.memo : '';
            this.eosAccountName = data.eos_account_name;
            this.easyAccountId = data.easy_account_id;
            this.isEosAccount = Number(this.easyAccountId) == 0;
            this.errorMessage = data.error_message;
        }
        /**
         * @return {?}
         */
        SavedWithdrawalRequest.prototype.getDataForApi = /**
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var amount;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.getAmount()];
                        case 1:
                            amount = _a.sent();
                            return [2 /*return*/, {
                                    id: this.id,
                                    memo: this.memo,
                                    address: this.address,
                                    errorMessage: this.errorMessage,
                                    coin: amount.currency.symbol,
                                    amount: amount.decimal,
                                }];
                    }
                });
            });
        };
        /**
         * @return {?}
         */
        SavedWithdrawalRequest.prototype.getAmount = /**
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (!(this.amount == undefined)) return [3 /*break*/, 2];
                            _a = this;
                            return [4 /*yield*/, this.amountFactory.newAmountFromDecimalAndCoinId(this.decimalAmount, this.coinId)];
                        case 1:
                            _a.amount = _b.sent();
                            _b.label = 2;
                        case 2: return [2 /*return*/, this.amount];
                    }
                });
            });
        };
        /**
         * @return {?}
         */
        SavedWithdrawalRequest.prototype.getRefundParams = /**
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var userId, info;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.getUserId()];
                        case 1:
                            userId = _a.sent();
                            return [4 /*yield*/, this.account.getAccountInfo(userId)];
                        case 2:
                            info = _a.sent();
                            return [2 /*return*/, {
                                    requestId: this.id,
                                    easyAccountPublicKey: info.easy_account_public_key ?
                                        info.easy_account_public_key :
                                        '',
                                    reason: this.errorMessage
                                }];
                    }
                });
            });
        };
        /**
         * @return {?}
         */
        SavedWithdrawalRequest.prototype.getUserId = /**
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a, _b;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            if (!(this.userId == undefined)) return [3 /*break*/, 5];
                            _a = this;
                            if (!this.isEosAccount) return [3 /*break*/, 2];
                            return [4 /*yield*/, this.account.getUserIdForEosAccount(this.eosAccountName)];
                        case 1:
                            _b = _c.sent();
                            return [3 /*break*/, 4];
                        case 2: return [4 /*yield*/, this.account.getUserIdForEasyAccount(this.easyAccountId)];
                        case 3:
                            _b = _c.sent();
                            _c.label = 4;
                        case 4:
                            _a.userId = _b;
                            _c.label = 5;
                        case 5: return [2 /*return*/, this.userId];
                    }
                });
            });
        };
        return SavedWithdrawalRequest;
    }());
    if (false) {
        /** @type {?} */
        SavedWithdrawalRequest.prototype.address;
        /** @type {?} */
        SavedWithdrawalRequest.prototype.id;
        /** @type {?} */
        SavedWithdrawalRequest.prototype.transactionHash;
        /** @type {?} */
        SavedWithdrawalRequest.prototype.memo;
        /** @type {?} */
        SavedWithdrawalRequest.prototype.coinId;
        /** @type {?} */
        SavedWithdrawalRequest.prototype.decimalAmount;
        /** @type {?} */
        SavedWithdrawalRequest.prototype.errorMessage;
        /**
         * @type {?}
         * @private
         */
        SavedWithdrawalRequest.prototype.isEosAccount;
        /**
         * @type {?}
         * @private
         */
        SavedWithdrawalRequest.prototype.eosAccountName;
        /**
         * @type {?}
         * @private
         */
        SavedWithdrawalRequest.prototype.easyAccountId;
        /**
         * @type {?}
         * @private
         */
        SavedWithdrawalRequest.prototype.userId;
        /**
         * @type {?}
         * @private
         */
        SavedWithdrawalRequest.prototype.amount;
        /**
         * @type {?}
         * @private
         */
        SavedWithdrawalRequest.prototype.account;
        /**
         * @type {?}
         * @private
         */
        SavedWithdrawalRequest.prototype.amountFactory;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/currency-amount/precise-math.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @template T
     */
    var   /**
     * @template T
     */
    PreciseMath = /** @class */ (function () {
        function PreciseMath(startingValue, factory) {
            this.startingValue = startingValue;
            this.factory = factory;
        }
        /**
         * @param {?} decimal
         * @return {?}
         */
        PreciseMath.prototype.addDecimal = /**
         * @param {?} decimal
         * @return {?}
         */
        function (decimal) {
            /** @type {?} */
            var other = this.factory.newAmountFromDecimal(decimal, this.precision);
            return this.add(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseMath.prototype.add = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            this.validator.isMatchingType(other);
            /** @type {?} */
            var newInteger = this.startingValue.bigInteger.plus(other.integer);
            /** @type {?} */
            var newNumber = this.factory.newAmountFromInteger(newInteger, this.precision);
            return new PreciseMath(newNumber, this.factory);
        };
        /**
         * @param {?} decimal
         * @return {?}
         */
        PreciseMath.prototype.subtractDecimal = /**
         * @param {?} decimal
         * @return {?}
         */
        function (decimal) {
            /** @type {?} */
            var other = this.factory.newAmountFromDecimal(decimal, this.precision);
            return this.subtract(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseMath.prototype.subtract = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            this.validator.isMatchingType(other);
            /** @type {?} */
            var newInteger = this.startingValue.bigInteger.minus(other.integer);
            /** @type {?} */
            var newNumber = this.factory.newAmountFromInteger(newInteger, this.precision);
            return new PreciseMath(newNumber, this.factory);
        };
        /**
         * @param {?} decimal
         * @return {?}
         */
        PreciseMath.prototype.divideDecimal = /**
         * @param {?} decimal
         * @return {?}
         */
        function (decimal) {
            return this.multiplyDecimal(new big_js.Big(1).div(decimal));
        };
        /**
         * @param {?} decimal
         * @return {?}
         */
        PreciseMath.prototype.multiplyDecimal = /**
         * @param {?} decimal
         * @return {?}
         */
        function (decimal) {
            /** @type {?} */
            var product = new big_js.Big(this.startingValue.decimal).times(decimal);
            /** @type {?} */
            var newInteger = product
                .times(this.factor)
                .round(0, 0 /* RoundDown */);
            /** @type {?} */
            var newNumber = this.factory.newAmountFromInteger(newInteger, this.precision);
            return new PreciseMath(newNumber, this.factory);
        };
        /**
         * @return {?}
         */
        PreciseMath.prototype.absoluteValue = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var newNumber = this.factory.newAmountFromInteger(new big_js.Big(this.bigInteger).abs(), this.precision);
            return new PreciseMath(newNumber, this.factory);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseMath.prototype.isLessThan = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return this.number.isLessThan(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseMath.prototype.isLessThanDecimal = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return this.number.isLessThanDecimal(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseMath.prototype.isGreaterThan = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return this.number.isGreaterThan(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseMath.prototype.isGreaterThanDecimal = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return this.number.isGreaterThanDecimal(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseMath.prototype.isEqualTo = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return this.number.isEqualTo(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseMath.prototype.isEqualToDecimal = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return this.number.isEqualToDecimal(other);
        };
        Object.defineProperty(PreciseMath.prototype, "decimal", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.decimal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PreciseMath.prototype, "integer", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.integer;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PreciseMath.prototype, "isZero", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.isZero;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PreciseMath.prototype, "isPositive", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.isPositive;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PreciseMath.prototype, "isNegative", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.isNegative;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PreciseMath.prototype, "bigInteger", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.bigInteger;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PreciseMath.prototype, "precision", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.precision;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PreciseMath.prototype, "factor", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.factor;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PreciseMath.prototype, "number", {
            get: /**
             * @return {?}
             */
            function () {
                return this.startingValue;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PreciseMath.prototype, "validator", {
            get: /**
             * @private
             * @return {?}
             */
            function () {
                return this.factory.validator;
            },
            enumerable: true,
            configurable: true
        });
        return PreciseMath;
    }());
    if (false) {
        /** @type {?} */
        PreciseMath.prototype.startingValue;
        /** @type {?} */
        PreciseMath.prototype.factory;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/currency-amount/precise-math-numbers.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PreciseNumber = /** @class */ (function () {
        function PreciseNumber(precision, integerValue) {
            if (integerValue === void 0) { integerValue = 0; }
            this.precision = precision;
            this.factor = new big_js.Big(Math.pow(10, precision));
            this.bigInteger = new big_js.Big(integerValue);
            big_js.Big.RM = 0 /* RoundDown */;
            this.decimal = this.bigInteger.div(this.factor)
                .toFixed(precision);
        }
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseNumber.prototype.isLessThan = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            this.validateMatchingCurrency(other);
            return this.bigInteger.lt(other.integer);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseNumber.prototype.isLessThanDecimal = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return new big_js.Big(this.decimal).lt(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseNumber.prototype.isGreaterThan = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            this.validateMatchingCurrency(other);
            return this.bigInteger.gt(other.integer);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseNumber.prototype.isGreaterThanDecimal = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return new big_js.Big(this.decimal).gt(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseNumber.prototype.isEqualTo = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            this.validateMatchingCurrency(other);
            return this.bigInteger.eq(other.integer);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        PreciseNumber.prototype.isEqualToDecimal = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return new big_js.Big(this.decimal).eq(other);
        };
        /**
         * @protected
         * @param {?} other
         * @return {?}
         */
        PreciseNumber.prototype.validateMatchingCurrency = /**
         * @protected
         * @param {?} other
         * @return {?}
         */
        function (other) {
            if (this.precision != other.precision) {
                throw new Error('both amounts must be the same precision!');
            }
        };
        Object.defineProperty(PreciseNumber.prototype, "integer", {
            get: /**
             * @return {?}
             */
            function () {
                return this.bigInteger.toFixed(0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PreciseNumber.prototype, "isZero", {
            get: /**
             * @return {?}
             */
            function () {
                return this.bigInteger.eq(0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PreciseNumber.prototype, "isPositive", {
            get: /**
             * @return {?}
             */
            function () {
                return this.bigInteger.gt(0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PreciseNumber.prototype, "isNegative", {
            get: /**
             * @return {?}
             */
            function () {
                return this.bigInteger.lt(0);
            },
            enumerable: true,
            configurable: true
        });
        return PreciseNumber;
    }());
    if (false) {
        /** @type {?} */
        PreciseNumber.prototype.factor;
        /** @type {?} */
        PreciseNumber.prototype.bigInteger;
        /** @type {?} */
        PreciseNumber.prototype.decimal;
        /** @type {?} */
        PreciseNumber.prototype.precision;
    }
    var PreciseDecimal = /** @class */ (function (_super) {
        __extends(PreciseDecimal, _super);
        function PreciseDecimal(decimalValue, precision) {
            var _this = this;
            /** @type {?} */
            var factor = new big_js.Big(Math.pow(10, precision));
            /** @type {?} */
            var decimal = new big_js.Big(decimalValue);
            _this = _super.call(this, precision, decimal
                .times(factor)
                .round(0, 0 /* RoundDown */)) || this;
            return _this;
        }
        return PreciseDecimal;
    }(PreciseNumber));
    var MatchingPrecisionValidator = /** @class */ (function () {
        function MatchingPrecisionValidator(precision) {
            this.precision = precision;
        }
        /**
         * @param {?} other
         * @return {?}
         */
        MatchingPrecisionValidator.prototype.isMatchingType = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            if (this.precision != other.precision) {
                throw new Error('both amounts must be the same precision!');
            }
            else {
                return true;
            }
        };
        return MatchingPrecisionValidator;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        MatchingPrecisionValidator.prototype.precision;
    }
    var PreciseNumberFactory = /** @class */ (function () {
        function PreciseNumberFactory(precision) {
            this.validator = new MatchingPrecisionValidator(precision);
        }
        /**
         * @param {?} integer
         * @param {?} precision
         * @return {?}
         */
        PreciseNumberFactory.prototype.newAmountFromInteger = /**
         * @param {?} integer
         * @param {?} precision
         * @return {?}
         */
        function (integer, precision) {
            /** @type {?} */
            var number = new PreciseNumber(precision, integer);
            this.validator.isMatchingType(number);
            return number;
        };
        /**
         * @param {?} decimal
         * @param {?} precision
         * @return {?}
         */
        PreciseNumberFactory.prototype.newAmountFromDecimal = /**
         * @param {?} decimal
         * @param {?} precision
         * @return {?}
         */
        function (decimal, precision) {
            /** @type {?} */
            var number = new PreciseDecimal(decimal, precision);
            this.validator.isMatchingType(number);
            return number;
        };
        return PreciseNumberFactory;
    }());
    if (false) {
        /** @type {?} */
        PreciseNumberFactory.prototype.validator;
    }
    /**
     * @template T
     */
    var   /**
     * @template T
     */
    NumberForPreciseMathBase = /** @class */ (function () {
        function NumberForPreciseMathBase(precision, integerValue, factory) {
            this.factory = factory;
            /** @type {?} */
            var number = factory.newAmountFromInteger(integerValue, precision);
            this.math = new PreciseMath(number, factory);
        }
        /**
         * @param {?} decimal
         * @return {?}
         */
        NumberForPreciseMathBase.prototype.addDecimal = /**
         * @param {?} decimal
         * @return {?}
         */
        function (decimal) {
            return this.math.addDecimal(decimal);
        };
        /**
         * @param {?} amount
         * @return {?}
         */
        NumberForPreciseMathBase.prototype.add = /**
         * @param {?} amount
         * @return {?}
         */
        function (amount) {
            return this.math.add(amount);
        };
        /**
         * @param {?} decimal
         * @return {?}
         */
        NumberForPreciseMathBase.prototype.subtractDecimal = /**
         * @param {?} decimal
         * @return {?}
         */
        function (decimal) {
            return this.math.subtractDecimal(decimal);
        };
        /**
         * @param {?} amount
         * @return {?}
         */
        NumberForPreciseMathBase.prototype.subtract = /**
         * @param {?} amount
         * @return {?}
         */
        function (amount) {
            return this.math.subtract(amount);
        };
        /**
         * @param {?} decimal
         * @return {?}
         */
        NumberForPreciseMathBase.prototype.multiplyDecimal = /**
         * @param {?} decimal
         * @return {?}
         */
        function (decimal) {
            return this.math.multiplyDecimal(decimal);
        };
        /**
         * @param {?} decimal
         * @return {?}
         */
        NumberForPreciseMathBase.prototype.divideDecimal = /**
         * @param {?} decimal
         * @return {?}
         */
        function (decimal) {
            return this.math.divideDecimal(decimal);
        };
        /**
         * @return {?}
         */
        NumberForPreciseMathBase.prototype.absoluteValue = /**
         * @return {?}
         */
        function () {
            return this.math.absoluteValue();
        };
        /**
         * @param {?} other
         * @return {?}
         */
        NumberForPreciseMathBase.prototype.isLessThan = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return this.number.isLessThan(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        NumberForPreciseMathBase.prototype.isLessThanDecimal = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return this.number.isLessThanDecimal(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        NumberForPreciseMathBase.prototype.isGreaterThan = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return this.number.isGreaterThan(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        NumberForPreciseMathBase.prototype.isGreaterThanDecimal = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return this.number.isGreaterThanDecimal(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        NumberForPreciseMathBase.prototype.isEqualTo = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return this.number.isEqualTo(other);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        NumberForPreciseMathBase.prototype.isEqualToDecimal = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return this.number.isEqualToDecimal(other);
        };
        Object.defineProperty(NumberForPreciseMathBase.prototype, "decimal", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.decimal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NumberForPreciseMathBase.prototype, "integer", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.integer;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NumberForPreciseMathBase.prototype, "isZero", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.isZero;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NumberForPreciseMathBase.prototype, "isPositive", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.isPositive;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NumberForPreciseMathBase.prototype, "isNegative", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.isNegative;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NumberForPreciseMathBase.prototype, "bigInteger", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.bigInteger;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NumberForPreciseMathBase.prototype, "precision", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.precision;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NumberForPreciseMathBase.prototype, "factor", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.factor;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NumberForPreciseMathBase.prototype, "number", {
            get: /**
             * @return {?}
             */
            function () {
                return this.startingValue;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NumberForPreciseMathBase.prototype, "startingValue", {
            get: /**
             * @return {?}
             */
            function () {
                return this.math.startingValue;
            },
            enumerable: true,
            configurable: true
        });
        return NumberForPreciseMathBase;
    }());
    if (false) {
        /** @type {?} */
        NumberForPreciseMathBase.prototype.math;
        /** @type {?} */
        NumberForPreciseMathBase.prototype.factory;
    }
    var NumberForPreciseMath = /** @class */ (function (_super) {
        __extends(NumberForPreciseMath, _super);
        function NumberForPreciseMath(precision, integerValue) {
            if (integerValue === void 0) { integerValue = 0; }
            return _super.call(this, precision, integerValue, new PreciseNumberFactory(precision)) || this;
        }
        return NumberForPreciseMath;
    }(NumberForPreciseMathBase));
    var DecimalForPreciseMath = /** @class */ (function (_super) {
        __extends(DecimalForPreciseMath, _super);
        function DecimalForPreciseMath(decimalValue, precision) {
            var _this = this;
            /** @type {?} */
            var factor = new big_js.Big(Math.pow(10, precision));
            /** @type {?} */
            var decimal = new big_js.Big(decimalValue);
            _this = _super.call(this, precision, decimal
                .times(factor)
                .round(0, 0 /* RoundDown */)) || this;
            return _this;
        }
        return DecimalForPreciseMath;
    }(NumberForPreciseMath));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/database/models/coin.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function ISavedCoinRow() { }
    if (false) {
        /** @type {?} */
        ISavedCoinRow.prototype.id;
        /** @type {?} */
        ISavedCoinRow.prototype.symbol;
        /** @type {?} */
        ISavedCoinRow.prototype.precision;
        /** @type {?} */
        ISavedCoinRow.prototype.uses_memo_for_deposits;
        /** @type {?} */
        ISavedCoinRow.prototype.minimum_withdrawal_amount;
    }
    var Coin = /** @class */ (function () {
        function Coin(data) {
            this.id = data.id;
            this.symbol = data.symbol;
            this.precision = data.precision;
            this.usesMemoForDeposits = data.uses_memo_for_deposits == 1;
            this.minimumWithdrawalAmount = new PreciseDecimal(data.minimum_withdrawal_amount, this.precision);
        }
        Object.defineProperty(Coin.prototype, "data", {
            get: /**
             * @return {?}
             */
            function () {
                return {
                    id: this.id,
                    symbol: this.symbol,
                    precision: this.precision,
                    usesMemoForDeposits: this.usesMemoForDeposits,
                    minimumWithdrawalAmount: this.minimumWithdrawalAmount.decimal
                };
            },
            enumerable: true,
            configurable: true
        });
        return Coin;
    }());
    if (false) {
        /** @type {?} */
        Coin.prototype.id;
        /** @type {?} */
        Coin.prototype.symbol;
        /** @type {?} */
        Coin.prototype.precision;
        /** @type {?} */
        Coin.prototype.usesMemoForDeposits;
        /** @type {?} */
        Coin.prototype.minimumWithdrawalAmount;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/database/models/account.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function ISavedAccountRow() { }
    if (false) {
        /** @type {?} */
        ISavedAccountRow.prototype.id;
        /** @type {?} */
        ISavedAccountRow.prototype.eos_account;
        /** @type {?} */
        ISavedAccountRow.prototype.ezeos_id;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/database/repositories/account-repository.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var FieldNames = {
        ID: 'id',
        EOS_ACCOUNT: 'eos_account',
        EZEOS_ID: 'ezeos_id',
    };
    var AccountRepository = /** @class */ (function (_super) {
        __extends(AccountRepository, _super);
        function AccountRepository(db) {
            return _super.call(this, db, 'account', [
                FieldNames.ID,
                FieldNames.EOS_ACCOUNT,
                FieldNames.EZEOS_ID
            ]) || this;
        }
        return AccountRepository;
    }(MySQLRepository));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/database/interfaces.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IUserAccountRow() { }
    if (false) {
        /** @type {?} */
        IUserAccountRow.prototype.eos_account_name;
        /** @type {?} */
        IUserAccountRow.prototype.easy_account_public_key;
    }
    /**
     * @record
     */
    function IUserAccountDatabaseService() { }
    if (false) {
        /**
         * @param {?} userId
         * @return {?}
         */
        IUserAccountDatabaseService.prototype.getAccountInfo = function (userId) { };
        /**
         * @param {?} eos_account
         * @return {?}
         */
        IUserAccountDatabaseService.prototype.getUserIdForEosAccount = function (eos_account) { };
        /**
         * @param {?} ezeos_id
         * @return {?}
         */
        IUserAccountDatabaseService.prototype.getUserIdForEasyAccount = function (ezeos_id) { };
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/database/user-account-database-service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var UserAccountDatabaseService = /** @class */ (function () {
        function UserAccountDatabaseService(database) {
            this.database = database;
            this.accountRepository = new AccountRepository(this.database);
        }
        /**
         * @param {?} userId
         * @return {?}
         */
        UserAccountDatabaseService.prototype.getAccountInfo = /**
         * @param {?} userId
         * @return {?}
         */
        function (userId) {
            return __awaiter(this, void 0, void 0, function () {
                var rows;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.database.builder.rawQuery("\n            SELECT\n                eos_account AS eos_account_name,\n                public_key AS easy_account_public_key\n\n            FROM\n                account\n                LEFT JOIN account_ezeos ON account.id = account_id\n\n            WHERE\n                account.id = " + userId + ";\n        ").execute()];
                        case 1:
                            rows = _a.sent();
                            return [2 /*return*/, rows[0]];
                    }
                });
            });
        };
        /**
         * @param {?} eos_account
         * @return {?}
         */
        UserAccountDatabaseService.prototype.getUserIdForEosAccount = /**
         * @param {?} eos_account
         * @return {?}
         */
        function (eos_account) {
            return __awaiter(this, void 0, void 0, function () {
                var row, insertId;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.accountRepository.selectOne({
                                whereMap: { eos_account: eos_account },
                            })];
                        case 1:
                            row = _a.sent();
                            if (!row) return [3 /*break*/, 2];
                            return [2 /*return*/, row.id];
                        case 2: return [4 /*yield*/, this.accountRepository.insert({ eos_account: eos_account })];
                        case 3:
                            insertId = (_a.sent()).insertId;
                            return [2 /*return*/, insertId];
                    }
                });
            });
        };
        /**
         * @param {?} ezeos_id
         * @return {?}
         */
        UserAccountDatabaseService.prototype.getUserIdForEasyAccount = /**
         * @param {?} ezeos_id
         * @return {?}
         */
        function (ezeos_id) {
            return __awaiter(this, void 0, void 0, function () {
                var rows;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.accountRepository.select({
                                whereMap: { ezeos_id: ezeos_id },
                            })];
                        case 1:
                            rows = _a.sent();
                            if (rows.length == 0) {
                                throw new Error('User ID NOT FOUND for Easy Account ID: ' + ezeos_id);
                            }
                            return [2 /*return*/, rows[0].id];
                    }
                });
            });
        };
        return UserAccountDatabaseService;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        UserAccountDatabaseService.prototype.accountRepository;
        /**
         * @type {?}
         * @protected
         */
        UserAccountDatabaseService.prototype.database;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/database/coin-data-provider.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CoinDataProvider = /** @class */ (function () {
        function CoinDataProvider(database) {
            this.database = database;
            this.isInit = false;
            this.ids = {};
            this.data = {};
            this.allCoins = [];
        }
        /**
         * @return {?}
         */
        CoinDataProvider.prototype.init = /**
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var _a, _b, _c, row;
                var e_1, _d;
                return __generator(this, function (_e) {
                    switch (_e.label) {
                        case 0:
                            _a = this;
                            return [4 /*yield*/, this.database.getAllCoins()];
                        case 1:
                            _a.allCoins = (_e.sent()).map((/**
                             * @param {?} row
                             * @return {?}
                             */
                            function (row) { return new Coin(row); }));
                            try {
                                for (_b = __values(this.allCoins), _c = _b.next(); !_c.done; _c = _b.next()) {
                                    row = _c.value;
                                    // map symbol to ID
                                    this.ids[row.symbol] = row.id;
                                    // map ID to data
                                    this.data[row.id] = row;
                                }
                            }
                            catch (e_1_1) { e_1 = { error: e_1_1 }; }
                            finally {
                                try {
                                    if (_c && !_c.done && (_d = _b.return)) _d.call(_b);
                                }
                                finally { if (e_1) throw e_1.error; }
                            }
                            this.isInit = true;
                            return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * @param {?} symbol
         * @return {?}
         */
        CoinDataProvider.prototype.getCoinDataBySymbol = /**
         * @param {?} symbol
         * @return {?}
         */
        function (symbol) {
            return __awaiter(this, void 0, void 0, function () {
                var id;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.getCoinId(symbol)];
                        case 1:
                            id = _a.sent();
                            return [2 /*return*/, this.getCoinData(id)];
                    }
                });
            });
        };
        /**
         * @param {?} symbol
         * @return {?}
         */
        CoinDataProvider.prototype.getCoinId = /**
         * @param {?} symbol
         * @return {?}
         */
        function (symbol) {
            return __awaiter(this, void 0, void 0, function () {
                var id;
                return __generator(this, function (_a) {
                    if (!this.isInit) {
                        throw new Error('CoinDataProvider is NOT Initialized!');
                    }
                    symbol = symbol.toUpperCase();
                    id = this.ids[symbol];
                    if (id === undefined) {
                        throw new Error('CoinId NOT FOUND: ' + symbol);
                    }
                    return [2 /*return*/, id];
                });
            });
        };
        /**
         * @param {?} coinId
         * @return {?}
         */
        CoinDataProvider.prototype.getCoinSymbol = /**
         * @param {?} coinId
         * @return {?}
         */
        function (coinId) {
            return __awaiter(this, void 0, void 0, function () {
                var data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.getCoinData(coinId)];
                        case 1:
                            data = _a.sent();
                            return [2 /*return*/, data.symbol];
                    }
                });
            });
        };
        /**
         * @param {?} coinId
         * @return {?}
         */
        CoinDataProvider.prototype.getCoinData = /**
         * @param {?} coinId
         * @return {?}
         */
        function (coinId) {
            return __awaiter(this, void 0, void 0, function () {
                var data;
                return __generator(this, function (_a) {
                    if (!this.isInit) {
                        throw new Error('CoinDataProvider is NOT Initialized!');
                    }
                    data = this.data[coinId];
                    if (!data) {
                        throw new Error('Coin NOT FOUND: ' + coinId);
                    }
                    return [2 /*return*/, data];
                });
            });
        };
        /**
         * @return {?}
         */
        CoinDataProvider.prototype.getAllCoins = /**
         * @return {?}
         */
        function () {
            return this.allCoins.slice();
        };
        return CoinDataProvider;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        CoinDataProvider.prototype.isInit;
        /**
         * @type {?}
         * @private
         */
        CoinDataProvider.prototype.ids;
        /**
         * @type {?}
         * @private
         */
        CoinDataProvider.prototype.data;
        /**
         * @type {?}
         * @private
         */
        CoinDataProvider.prototype.allCoins;
        /**
         * @type {?}
         * @private
         */
        CoinDataProvider.prototype.database;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/database/models/common.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function ISavedEntity() { }
    if (false) {
        /** @type {?} */
        ISavedEntity.prototype.id;
    }
    /** @type {?} */
    var CommonFieldNames = {
        ID: 'id',
        CREATED_AT: 'created_at',
    };

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/database/repositories/coin-repository.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var FieldNames$1 = {
        SYMBOL: 'symbol',
        PRECISION: 'precision',
        USES_MEMO_FOR_DEPOSITS: 'uses_memo_for_deposits',
        MINIMUM_WITHDRAWAL_AMOUNT: 'minimum_withdrawal_amount'
    };
    var CoinRepository = /** @class */ (function (_super) {
        __extends(CoinRepository, _super);
        function CoinRepository(database) {
            return _super.call(this, database, 'coin', [
                CommonFieldNames.ID,
                FieldNames$1.SYMBOL,
                FieldNames$1.PRECISION,
                FieldNames$1.USES_MEMO_FOR_DEPOSITS,
                FieldNames$1.MINIMUM_WITHDRAWAL_AMOUNT
            ]) || this;
        }
        return CoinRepository;
    }(MySQLRepository));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/database/coin-database-service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CoinDatabaseService = /** @class */ (function () {
        function CoinDatabaseService(database) {
            this.coins = new CoinRepository(database);
        }
        /**
         * @return {?}
         */
        CoinDatabaseService.prototype.getAllCoins = /**
         * @return {?}
         */
        function () {
            return this.coins.select({
                orderBy: 'id'
            });
        };
        /**
         * @param {?} coinId
         * @return {?}
         */
        CoinDatabaseService.prototype.getCoinData = /**
         * @param {?} coinId
         * @return {?}
         */
        function (coinId) {
            return __awaiter(this, void 0, void 0, function () {
                var result;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.coins.selectOneByPrimaryKey(coinId)];
                        case 1:
                            result = _a.sent();
                            return [2 /*return*/, result];
                    }
                });
            });
        };
        return CoinDatabaseService;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        CoinDatabaseService.prototype.coins;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/services/withdrawal-contract-service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @template T
     */
    var   /**
     * @template T
     */
    WithdrawalContractService = /** @class */ (function () {
        function WithdrawalContractService(eosUtility) {
            this.eosUtility = eosUtility;
            this.contractName = this.contracts.withdrawal;
        }
        /**
         * @return {?}
         */
        WithdrawalContractService.prototype.getPendingRequests = /**
         * @return {?}
         */
        function () {
            return this.eosUtility.getTableRows({
                code: this.contractName,
                scope: this.contractName,
                table: 'withdraws',
                limit: 1000
            });
        };
        /**
         * @param {?} requestId
         * @param {?} txnId
         * @return {?}
         */
        WithdrawalContractService.prototype.processRequest = /**
         * @param {?} requestId
         * @param {?} txnId
         * @return {?}
         */
        function (requestId, txnId) {
            return __awaiter(this, void 0, void 0, function () {
                var executor, broadcastedTxnId, confirmedTxnId;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            executor = new EosTransactionExecutor([{
                                    account: this.contractName,
                                    name: 'process',
                                    data: {
                                        withdraw_id: requestId,
                                        memo: txnId
                                    }
                                }], this.eosUtility, isWaxTxnIrreversible);
                            return [4 /*yield*/, executor.execute()];
                        case 1:
                            broadcastedTxnId = _a.sent();
                            return [4 /*yield*/, executor.confirm()];
                        case 2:
                            confirmedTxnId = _a.sent();
                            /*** save this in DB? ***/
                            return [2 /*return*/, confirmedTxnId];
                    }
                });
            });
        };
        /**
         * @param {?} request
         * @return {?}
         */
        WithdrawalContractService.prototype.refundRequest = /**
         * @param {?} request
         * @return {?}
         */
        function (request) {
            return __awaiter(this, void 0, void 0, function () {
                var params, requestId, easyAccountPublicKey, reason, data, executor, broadcastedTxnId, confirmedTxnId;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, request.getRefundParams()];
                        case 1:
                            params = _a.sent();
                            requestId = params.requestId, easyAccountPublicKey = params.easyAccountPublicKey, reason = params.reason;
                            data = {
                                withdraw_id: requestId,
                                ezacct_deposit_memo: easyAccountPublicKey,
                                memo: reason
                            };
                            executor = new EosTransactionExecutor([{
                                    account: this.contractName,
                                    name: 'refund',
                                    data: data
                                }], this.eosUtility, isWaxTxnIrreversible);
                            return [4 /*yield*/, executor.execute()];
                        case 2:
                            broadcastedTxnId = _a.sent();
                            return [4 /*yield*/, executor.confirm()];
                        case 3:
                            confirmedTxnId = _a.sent();
                            /*** save this in DB? ***/
                            return [2 /*return*/, confirmedTxnId];
                    }
                });
            });
        };
        Object.defineProperty(WithdrawalContractService.prototype, "contracts", {
            get: /**
             * @private
             * @return {?}
             */
            function () {
                return this.eosUtility.contracts;
            },
            enumerable: true,
            configurable: true
        });
        return WithdrawalContractService;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        WithdrawalContractService.prototype.contractName;
        /**
         * @type {?}
         * @private
         */
        WithdrawalContractService.prototype.eosUtility;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/services/interfaces.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IWithdrawalRequestData() { }
    if (false) {
        /** @type {?} */
        IWithdrawalRequestData.prototype.user;
        /** @type {?} */
        IWithdrawalRequestData.prototype.tx_hash;
        /** @type {?} */
        IWithdrawalRequestData.prototype.extra_data;
        /** @type {?} */
        IWithdrawalRequestData.prototype.withdraw_id;
        /** @type {?} */
        IWithdrawalRequestData.prototype.withdraw_memo;
        /** @type {?} */
        IWithdrawalRequestData.prototype.withdraw_address;
        /** @type {?} */
        IWithdrawalRequestData.prototype.withdraw_quantity;
    }
    /**
     * @record
     */
    function IRefundWithdrawalRequestParams() { }
    if (false) {
        /** @type {?} */
        IRefundWithdrawalRequestParams.prototype.requestId;
        /** @type {?} */
        IRefundWithdrawalRequestParams.prototype.easyAccountPublicKey;
        /** @type {?} */
        IRefundWithdrawalRequestParams.prototype.reason;
    }
    /**
     * @record
     */
    function IWithdrawalContractService() { }
    if (false) {
        /**
         * @return {?}
         */
        IWithdrawalContractService.prototype.getPendingRequests = function () { };
        /**
         * @param {?} requestId
         * @param {?} txnId
         * @return {?}
         */
        IWithdrawalContractService.prototype.processRequest = function (requestId, txnId) { };
        /**
         * @param {?} request
         * @return {?}
         */
        IWithdrawalContractService.prototype.refundRequest = function (request) { };
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/currency-amount/interfaces.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function ICurrency() { }
    if (false) {
        /** @type {?} */
        ICurrency.prototype.id;
        /** @type {?} */
        ICurrency.prototype.symbol;
        /** @type {?} */
        ICurrency.prototype.precision;
    }
    /**
     * @record
     */
    function IPreciseNumber() { }
    if (false) {
        /** @type {?} */
        IPreciseNumber.prototype.precision;
        /** @type {?} */
        IPreciseNumber.prototype.decimal;
        /** @type {?} */
        IPreciseNumber.prototype.integer;
        /** @type {?} */
        IPreciseNumber.prototype.isZero;
        /** @type {?} */
        IPreciseNumber.prototype.isPositive;
        /** @type {?} */
        IPreciseNumber.prototype.isNegative;
        /** @type {?} */
        IPreciseNumber.prototype.bigInteger;
        /** @type {?} */
        IPreciseNumber.prototype.factor;
        /**
         * @param {?} other
         * @return {?}
         */
        IPreciseNumber.prototype.isLessThan = function (other) { };
        /**
         * @param {?} other
         * @return {?}
         */
        IPreciseNumber.prototype.isLessThanDecimal = function (other) { };
        /**
         * @param {?} other
         * @return {?}
         */
        IPreciseNumber.prototype.isGreaterThan = function (other) { };
        /**
         * @param {?} other
         * @return {?}
         */
        IPreciseNumber.prototype.isGreaterThanDecimal = function (other) { };
        /**
         * @param {?} other
         * @return {?}
         */
        IPreciseNumber.prototype.isEqualTo = function (other) { };
        /**
         * @param {?} other
         * @return {?}
         */
        IPreciseNumber.prototype.isEqualToDecimal = function (other) { };
    }
    /**
     * @record
     * @template T
     */
    function IMatchingNumberTypeValidator() { }
    if (false) {
        /**
         * @param {?} other
         * @return {?}
         */
        IMatchingNumberTypeValidator.prototype.isMatchingType = function (other) { };
    }
    /**
     * @record
     * @template T
     */
    function IPreciseNumberFactory() { }
    if (false) {
        /** @type {?} */
        IPreciseNumberFactory.prototype.validator;
        /**
         * @param {?} integer
         * @param {?} precision
         * @return {?}
         */
        IPreciseNumberFactory.prototype.newAmountFromInteger = function (integer, precision) { };
        /**
         * @param {?} decimal
         * @param {?} precision
         * @return {?}
         */
        IPreciseNumberFactory.prototype.newAmountFromDecimal = function (decimal, precision) { };
    }
    /**
     * @record
     * @template T
     */
    function IPreciseMath() { }
    if (false) {
        /** @type {?} */
        IPreciseMath.prototype.startingValue;
        /** @type {?} */
        IPreciseMath.prototype.factory;
        /**
         * @param {?} decimal
         * @return {?}
         */
        IPreciseMath.prototype.addDecimal = function (decimal) { };
        /**
         * @param {?} amount
         * @return {?}
         */
        IPreciseMath.prototype.add = function (amount) { };
        /**
         * @param {?} decimal
         * @return {?}
         */
        IPreciseMath.prototype.subtractDecimal = function (decimal) { };
        /**
         * @param {?} amount
         * @return {?}
         */
        IPreciseMath.prototype.subtract = function (amount) { };
        /**
         * @param {?} decimal
         * @return {?}
         */
        IPreciseMath.prototype.multiplyDecimal = function (decimal) { };
        /**
         * @param {?} decimal
         * @return {?}
         */
        IPreciseMath.prototype.divideDecimal = function (decimal) { };
        /**
         * @return {?}
         */
        IPreciseMath.prototype.absoluteValue = function () { };
    }
    /**
     * @record
     */
    function IPreciseCurrencyAmount() { }
    if (false) {
        /** @type {?} */
        IPreciseCurrencyAmount.prototype.currency;
        /** @type {?} */
        IPreciseCurrencyAmount.prototype.quantity;
    }
    /**
     * @record
     */
    function IPreciseCurrencyAmountWithPrice() { }
    if (false) {
        /** @type {?} */
        IPreciseCurrencyAmountWithPrice.prototype.priceInUSD;
        /** @type {?} */
        IPreciseCurrencyAmountWithPrice.prototype.amountInUSD;
    }
    /**
     * @record
     */
    function ICurrencyAmount() { }
    if (false) {
        /** @type {?} */
        ICurrencyAmount.prototype.math;
    }
    /**
     * @record
     */
    function ICurrencyAmountWithPrice() { }
    if (false) {
        /** @type {?} */
        ICurrencyAmountWithPrice.prototype.math;
    }
    /**
     * @record
     */
    function ICurrencyAmountFactory() { }
    if (false) {
        /** @type {?} */
        ICurrencyAmountFactory.prototype.coinDataProvider;
        /**
         * @param {?} quantity
         * @return {?}
         */
        ICurrencyAmountFactory.prototype.newAmountFromQuantity = function (quantity) { };
        /**
         * @param {?} decimalAmount
         * @param {?} tokenSymbol
         * @return {?}
         */
        ICurrencyAmountFactory.prototype.newAmountFromDecimal = function (decimalAmount, tokenSymbol) { };
        /**
         * @param {?} decimalAmount
         * @param {?} coinId
         * @return {?}
         */
        ICurrencyAmountFactory.prototype.newAmountFromDecimalAndCoinId = function (decimalAmount, coinId) { };
        /**
         * @param {?} integerSubunits
         * @param {?} tokenSymbol
         * @return {?}
         */
        ICurrencyAmountFactory.prototype.newAmountFromInteger = function (integerSubunits, tokenSymbol) { };
    }
    /**
     * @record
     */
    function ICurrencyAmountWithPriceFactory() { }
    if (false) {
        /** @type {?} */
        ICurrencyAmountWithPriceFactory.prototype.priceService;
        /**
         * @param {?} quantity
         * @return {?}
         */
        ICurrencyAmountWithPriceFactory.prototype.newAmountFromQuantity = function (quantity) { };
        /**
         * @param {?} decimalAmount
         * @param {?} tokenSymbol
         * @return {?}
         */
        ICurrencyAmountWithPriceFactory.prototype.newAmountFromDecimal = function (decimalAmount, tokenSymbol) { };
        /**
         * @param {?} decimalAmount
         * @param {?} coinId
         * @return {?}
         */
        ICurrencyAmountWithPriceFactory.prototype.newAmountFromDecimalAndCoinId = function (decimalAmount, coinId) { };
        /**
         * @param {?} integerSubunits
         * @param {?} tokenSymbol
         * @return {?}
         */
        ICurrencyAmountWithPriceFactory.prototype.newAmountFromInteger = function (integerSubunits, tokenSymbol) { };
    }
    /**
     * @record
     */
    function ICurrencyPriceService() { }
    if (false) {
        /**
         * @param {?} currencySymbol
         * @return {?}
         */
        ICurrencyPriceService.prototype.getPriceInUSD = function (currencySymbol) { };
    }
    /**
     * @record
     */
    function ICoinDatabaseService() { }
    if (false) {
        /**
         * @return {?}
         */
        ICoinDatabaseService.prototype.getAllCoins = function () { };
    }
    /**
     * @record
     */
    function ICoinDataProvider() { }
    if (false) {
        /**
         * @param {?} symbol
         * @return {?}
         */
        ICoinDataProvider.prototype.getCoinId = function (symbol) { };
        /**
         * @param {?} coinId
         * @return {?}
         */
        ICoinDataProvider.prototype.getCoinSymbol = function (coinId) { };
        /**
         * @param {?} coinId
         * @return {?}
         */
        ICoinDataProvider.prototype.getCoinData = function (coinId) { };
        /**
         * @param {?} symbol
         * @return {?}
         */
        ICoinDataProvider.prototype.getCoinDataBySymbol = function (symbol) { };
        /**
         * @return {?}
         */
        ICoinDataProvider.prototype.getAllCoins = function () { };
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/http-util.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @template T
     * @param {?} url
     * @return {?}
     */
    function httpsGetJson(url) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            https.get('https://' + url, (/**
             * @param {?} resp
             * @return {?}
             */
            function (resp) {
                /** @type {?} */
                var data = '';
                // A chunk of data has been recieved.
                resp.on('data', (/**
                 * @param {?} chunk
                 * @return {?}
                 */
                function (chunk) {
                    data += chunk;
                }));
                // The whole response has been received. Print out the result.
                resp.on('end', (/**
                 * @return {?}
                 */
                function () {
                    try {
                        /** @type {?} */
                        var parsedData = JSON.parse(data);
                        resolve(parsedData);
                    }
                    catch (error) {
                        console.log(data);
                        reject(error);
                    }
                }));
            })).on('error', (/**
             * @param {?} err
             * @return {?}
             */
            function (err) {
                // console.log("Error: " + err.message);
                // throw err;
                reject(err);
            }));
        }));
    }
    /**
     * @template T
     * @param {?} url
     * @return {?}
     */
    function httpGetJson(url) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            http.get('http://' + url, (/**
             * @param {?} resp
             * @return {?}
             */
            function (resp) {
                /** @type {?} */
                var data = '';
                // A chunk of data has been recieved.
                resp.on('data', (/**
                 * @param {?} chunk
                 * @return {?}
                 */
                function (chunk) {
                    data += chunk;
                }));
                // The whole response has been received. Print out the result.
                resp.on('end', (/**
                 * @return {?}
                 */
                function () {
                    try {
                        /** @type {?} */
                        var parsedData = JSON.parse(data);
                        resolve(parsedData);
                    }
                    catch (error) {
                        console.log(data);
                        reject(error);
                    }
                }));
            })).on('error', (/**
             * @param {?} err
             * @return {?}
             */
            function (err) {
                // console.log("Error: " + err.message);
                // throw err;
                reject(err);
            }));
        }));
    }
    /**
     * @param {?} url
     * @return {?}
     */
    function httpGet(url) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            http.get('http://' + url, (/**
             * @param {?} resp
             * @return {?}
             */
            function (resp) {
                /** @type {?} */
                var data = '';
                // A chunk of data has been recieved.
                resp.on('data', (/**
                 * @param {?} chunk
                 * @return {?}
                 */
                function (chunk) {
                    data += chunk;
                }));
                // The whole response has been received. Print out the result.
                resp.on('end', (/**
                 * @return {?}
                 */
                function () {
                    // console.log(data);
                    resolve(data);
                }));
            })).on('error', (/**
             * @param {?} err
             * @return {?}
             */
            function (err) {
                // console.log("Error: " + err.message);
                // throw err;
                reject(err);
            }));
        }));
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/coin-util.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CoinPriceService = /** @class */ (function () {
        function CoinPriceService() {
            var _this = this;
            this.symbols = [];
            this.prices = {};
            this.updateCoinPrices = (/**
             * @return {?}
             */
            function () { return __awaiter(_this, void 0, void 0, function () {
                var _a, _b, symbol, price, e_1_1;
                var e_1, _c;
                return __generator(this, function (_d) {
                    switch (_d.label) {
                        case 0:
                            _d.trys.push([0, 5, 6, 7]);
                            _a = __values(this.symbols), _b = _a.next();
                            _d.label = 1;
                        case 1:
                            if (!!_b.done) return [3 /*break*/, 4];
                            symbol = _b.value;
                            symbol = symbol.split('_')[0];
                            return [4 /*yield*/, fetchPriceOfCoin(symbol)];
                        case 2:
                            price = _d.sent();
                            if (price != undefined) {
                                this.prices[symbol] = price;
                            }
                            _d.label = 3;
                        case 3:
                            _b = _a.next();
                            return [3 /*break*/, 1];
                        case 4: return [3 /*break*/, 7];
                        case 5:
                            e_1_1 = _d.sent();
                            e_1 = { error: e_1_1 };
                            return [3 /*break*/, 7];
                        case 6:
                            try {
                                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                            }
                            finally { if (e_1) throw e_1.error; }
                            return [7 /*endfinally*/];
                        case 7: return [2 /*return*/];
                    }
                });
            }); });
        }
        /**
         * @param {?} database
         * @return {?}
         */
        CoinPriceService.prototype.init = /**
         * @param {?} database
         * @return {?}
         */
        function (database) {
            return __awaiter(this, void 0, void 0, function () {
                var coins;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, database.getAllCoins()];
                        case 1:
                            coins = _a.sent();
                            this.symbols = coins.map((/**
                             * @param {?} row
                             * @return {?}
                             */
                            function (row) { return row.symbol; }));
                            setInterval(this.updateCoinPrices, 1000 * 60 * 60);
                            return [4 /*yield*/, this.updateCoinPrices()];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * @param {?} symbol
         * @return {?}
         */
        CoinPriceService.prototype.getPriceInUSD = /**
         * @param {?} symbol
         * @return {?}
         */
        function (symbol) {
            return __awaiter(this, void 0, void 0, function () {
                var price;
                return __generator(this, function (_a) {
                    symbol = symbol.split('_')[0];
                    if (symbol == 'FUN') {
                        return [2 /*return*/, 0];
                    }
                    if (symbol == 'USD') {
                        return [2 /*return*/, 1];
                    }
                    price = this.prices[symbol.toUpperCase()];
                    if (price == undefined) {
                        console.log(this.prices);
                        throw new Error(symbol + ' Price is UNDEFINED!');
                    }
                    return [2 /*return*/, price];
                });
            });
        };
        return CoinPriceService;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        CoinPriceService.prototype.symbols;
        /**
         * @type {?}
         * @private
         */
        CoinPriceService.prototype.prices;
        /**
         * @type {?}
         * @private
         */
        CoinPriceService.prototype.updateCoinPrices;
    }
    /** @type {?} */
    var coinPriceService;
    /**
     * @param {?} database
     * @return {?}
     * @this {*}
     */
    function getCoinPriceService(database) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(coinPriceService == undefined)) return [3 /*break*/, 2];
                        coinPriceService = new CoinPriceService();
                        return [4 /*yield*/, coinPriceService.init(database)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/, coinPriceService];
                }
            });
        });
    }
    /**
     * @record
     */
    function ICoinPriceData() { }
    if (false) {
        /** @type {?} */
        ICoinPriceData.prototype.price;
    }
    /**
     * @param {?} symbol
     * @return {?}
     * @this {*}
     */
    function fetchPriceOfCoin(symbol) {
        return __awaiter(this, void 0, void 0, function () {
            var url, data, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        url = 'internal.eosbet.io/token_price/' +
                            symbol.toUpperCase();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, httpGetJson(url)];
                    case 2:
                        data = _a.sent();
                        // console.log(data);
                        return [2 /*return*/, Number(data.price)];
                    case 3:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [2 /*return*/, undefined];
                    case 4: return [2 /*return*/];
                }
            });
        });
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/currency/currency-amount/currency-amount-factory.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PreciseCurrencyAmount = /** @class */ (function (_super) {
        __extends(PreciseCurrencyAmount, _super);
        function PreciseCurrencyAmount(decimalValue, currency) {
            var _this = _super.call(this, decimalValue, currency.precision) || this;
            _this.currency = currency;
            return _this;
        }
        Object.defineProperty(PreciseCurrencyAmount.prototype, "quantity", {
            get: /**
             * @return {?}
             */
            function () {
                return this.decimal + ' ' + this.currency.symbol;
            },
            enumerable: true,
            configurable: true
        });
        return PreciseCurrencyAmount;
    }(PreciseDecimal));
    if (false) {
        /** @type {?} */
        PreciseCurrencyAmount.prototype.currency;
    }
    var PreciseCurrencyAmountWithPrice = /** @class */ (function (_super) {
        __extends(PreciseCurrencyAmountWithPrice, _super);
        function PreciseCurrencyAmountWithPrice(decimalValue, currency, priceInUSD) {
            var _this = _super.call(this, decimalValue, currency) || this;
            _this.priceInUSD = priceInUSD;
            return _this;
        }
        Object.defineProperty(PreciseCurrencyAmountWithPrice.prototype, "amountInUSD", {
            get: /**
             * @return {?}
             */
            function () {
                this.checkPrice();
                return (new big_js.Big(this.decimal))
                    .times(this.priceInUSD)
                    .round(6, 0 /* RoundDown */)
                    .toString();
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @private
         * @return {?}
         */
        PreciseCurrencyAmountWithPrice.prototype.checkPrice = /**
         * @private
         * @return {?}
         */
        function () {
            if (this.priceInUSD == undefined) {
                throw new Error('Price for Currency IS NOT DEFINED!');
            }
        };
        return PreciseCurrencyAmountWithPrice;
    }(PreciseCurrencyAmount));
    if (false) {
        /** @type {?} */
        PreciseCurrencyAmountWithPrice.prototype.priceInUSD;
    }
    var MatchingCurrencyValidator = /** @class */ (function () {
        function MatchingCurrencyValidator(currency) {
            this.currency = currency;
        }
        /**
         * @param {?} __0
         * @return {?}
         */
        MatchingCurrencyValidator.prototype.isMatchingType = /**
         * @param {?} __0
         * @return {?}
         */
        function (_a) {
            var currency = _a.currency;
            if (this.currency.symbol != currency.symbol ||
                this.currency.precision != currency.precision) {
                throw new Error('both amounts must be the same currency!');
            }
            else {
                return true;
            }
        };
        return MatchingCurrencyValidator;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        MatchingCurrencyValidator.prototype.currency;
    }
    var PreciseCurrencyAmountFactory = /** @class */ (function () {
        function PreciseCurrencyAmountFactory(currency) {
            this.currency = currency;
            this.validator = new MatchingCurrencyValidator(currency);
        }
        /**
         * @param {?} integer
         * @param {?} precision
         * @return {?}
         */
        PreciseCurrencyAmountFactory.prototype.newAmountFromInteger = /**
         * @param {?} integer
         * @param {?} precision
         * @return {?}
         */
        function (integer, precision) {
            /** @type {?} */
            var number = new PreciseCurrencyAmount(new big_js.Big(integer).div(Math.pow(10, precision)), this.currency);
            this.validator.isMatchingType(number);
            return number;
        };
        /**
         * @param {?} decimal
         * @param {?} precision
         * @return {?}
         */
        PreciseCurrencyAmountFactory.prototype.newAmountFromDecimal = /**
         * @param {?} decimal
         * @param {?} precision
         * @return {?}
         */
        function (decimal, precision) {
            /** @type {?} */
            var number = new PreciseCurrencyAmount(decimal, this.currency);
            this.validator.isMatchingType(number);
            return number;
        };
        return PreciseCurrencyAmountFactory;
    }());
    if (false) {
        /** @type {?} */
        PreciseCurrencyAmountFactory.prototype.validator;
        /**
         * @type {?}
         * @private
         */
        PreciseCurrencyAmountFactory.prototype.currency;
    }
    var PreciseCurrencyAmountWithPriceFactory = /** @class */ (function () {
        function PreciseCurrencyAmountWithPriceFactory(currency, priceInUSD) {
            this.currency = currency;
            this.priceInUSD = priceInUSD;
            this.validator = new MatchingCurrencyValidator(currency);
        }
        /**
         * @param {?} integer
         * @param {?} precision
         * @return {?}
         */
        PreciseCurrencyAmountWithPriceFactory.prototype.newAmountFromInteger = /**
         * @param {?} integer
         * @param {?} precision
         * @return {?}
         */
        function (integer, precision) {
            /** @type {?} */
            var number = new PreciseCurrencyAmountWithPrice(new big_js.Big(integer).div(Math.pow(10, precision)), this.currency, this.priceInUSD);
            this.validator.isMatchingType(number);
            return number;
        };
        /**
         * @param {?} decimal
         * @param {?} precision
         * @return {?}
         */
        PreciseCurrencyAmountWithPriceFactory.prototype.newAmountFromDecimal = /**
         * @param {?} decimal
         * @param {?} precision
         * @return {?}
         */
        function (decimal, precision) {
            /** @type {?} */
            var number = new PreciseCurrencyAmountWithPrice(decimal, this.currency, this.priceInUSD);
            this.validator.isMatchingType(number);
            return number;
        };
        return PreciseCurrencyAmountWithPriceFactory;
    }());
    if (false) {
        /** @type {?} */
        PreciseCurrencyAmountWithPriceFactory.prototype.validator;
        /**
         * @type {?}
         * @private
         */
        PreciseCurrencyAmountWithPriceFactory.prototype.currency;
        /**
         * @type {?}
         * @private
         */
        PreciseCurrencyAmountWithPriceFactory.prototype.priceInUSD;
    }
    var CurrencyAmount = /** @class */ (function (_super) {
        __extends(CurrencyAmount, _super);
        function CurrencyAmount(decimalValue, currency) {
            var _this = _super.call(this, currency.precision, new big_js.Big(decimalValue).times(Math.pow(10, currency.precision)).round(0, 0 /* RoundDown */), new PreciseCurrencyAmountFactory(currency)) || this;
            _this.currency = currency;
            return _this;
        }
        Object.defineProperty(CurrencyAmount.prototype, "quantity", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.quantity;
            },
            enumerable: true,
            configurable: true
        });
        return CurrencyAmount;
    }(NumberForPreciseMathBase));
    if (false) {
        /** @type {?} */
        CurrencyAmount.prototype.currency;
    }
    var CurrencyAmountWithPrice = /** @class */ (function (_super) {
        __extends(CurrencyAmountWithPrice, _super);
        function CurrencyAmountWithPrice(decimalValue, currency, priceInUSD) {
            var _this = _super.call(this, currency.precision, new big_js.Big(decimalValue).times(Math.pow(10, currency.precision)).round(0, 0 /* RoundDown */), new PreciseCurrencyAmountWithPriceFactory(currency, priceInUSD)) || this;
            _this.currency = currency;
            return _this;
        }
        Object.defineProperty(CurrencyAmountWithPrice.prototype, "amountInUSD", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.amountInUSD;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CurrencyAmountWithPrice.prototype, "priceInUSD", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.priceInUSD;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CurrencyAmountWithPrice.prototype, "quantity", {
            get: /**
             * @return {?}
             */
            function () {
                return this.number.quantity;
            },
            enumerable: true,
            configurable: true
        });
        return CurrencyAmountWithPrice;
    }(NumberForPreciseMathBase));
    if (false) {
        /** @type {?} */
        CurrencyAmountWithPrice.prototype.currency;
    }
    var CurrencyAmountFactory = /** @class */ (function () {
        function CurrencyAmountFactory(coinDataProvider) {
            this.coinDataProvider = coinDataProvider;
        }
        /**
         * @param {?} quantity
         * @return {?}
         */
        CurrencyAmountFactory.prototype.newAmountFromQuantity = /**
         * @param {?} quantity
         * @return {?}
         */
        function (quantity) {
            return __awaiter(this, void 0, void 0, function () {
                var _a, amount, tokenSymbol;
                return __generator(this, function (_b) {
                    _a = __read(quantity.split(' '), 2), amount = _a[0], tokenSymbol = _a[1];
                    return [2 /*return*/, this.newAmountFromDecimal(amount, tokenSymbol)];
                });
            });
        };
        /**
         * @param {?} decimalAmount
         * @param {?} tokenSymbol
         * @return {?}
         */
        CurrencyAmountFactory.prototype.newAmountFromDecimal = /**
         * @param {?} decimalAmount
         * @param {?} tokenSymbol
         * @return {?}
         */
        function (decimalAmount, tokenSymbol) {
            return __awaiter(this, void 0, void 0, function () {
                var data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.coinDataProvider.getCoinDataBySymbol(tokenSymbol)];
                        case 1:
                            data = _a.sent();
                            return [2 /*return*/, new CurrencyAmount(decimalAmount, data)];
                    }
                });
            });
        };
        /**
         * @param {?} decimalAmount
         * @param {?} coinId
         * @return {?}
         */
        CurrencyAmountFactory.prototype.newAmountFromDecimalAndCoinId = /**
         * @param {?} decimalAmount
         * @param {?} coinId
         * @return {?}
         */
        function (decimalAmount, coinId) {
            return __awaiter(this, void 0, void 0, function () {
                var data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.coinDataProvider.getCoinData(coinId)];
                        case 1:
                            data = _a.sent();
                            return [2 /*return*/, new CurrencyAmount(decimalAmount, data)];
                    }
                });
            });
        };
        /**
         * @param {?} integerSubunits
         * @param {?} tokenSymbol
         * @return {?}
         */
        CurrencyAmountFactory.prototype.newAmountFromInteger = /**
         * @param {?} integerSubunits
         * @param {?} tokenSymbol
         * @return {?}
         */
        function (integerSubunits, tokenSymbol) {
            return __awaiter(this, void 0, void 0, function () {
                var data, decimalAmount;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.coinDataProvider.getCoinDataBySymbol(tokenSymbol)];
                        case 1:
                            data = _a.sent();
                            decimalAmount = new big_js.Big(integerSubunits).div(Math.pow(10, data.precision));
                            return [2 /*return*/, new CurrencyAmount(decimalAmount, data)];
                    }
                });
            });
        };
        return CurrencyAmountFactory;
    }());
    if (false) {
        /** @type {?} */
        CurrencyAmountFactory.prototype.coinDataProvider;
    }
    /** @type {?} */
    var currencyAmountFactory;
    /**
     * @param {?} database
     * @return {?}
     * @this {*}
     */
    function getCurrencyAmountFactory(database) {
        return __awaiter(this, void 0, void 0, function () {
            var coinDataProvider;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(currencyAmountFactory == undefined)) return [3 /*break*/, 2];
                        coinDataProvider = new CoinDataProvider(database);
                        return [4 /*yield*/, coinDataProvider.init()];
                    case 1:
                        _a.sent();
                        currencyAmountFactory = new CurrencyAmountFactory(coinDataProvider);
                        _a.label = 2;
                    case 2: return [2 /*return*/, currencyAmountFactory];
                }
            });
        });
    }
    var CurrencyAmountWithPriceFactory = /** @class */ (function () {
        function CurrencyAmountWithPriceFactory(coinDataProvider, priceService) {
            this.coinDataProvider = coinDataProvider;
            this.priceService = priceService;
        }
        /**
         * @param {?} quantity
         * @return {?}
         */
        CurrencyAmountWithPriceFactory.prototype.newAmountFromQuantity = /**
         * @param {?} quantity
         * @return {?}
         */
        function (quantity) {
            return __awaiter(this, void 0, void 0, function () {
                var _a, amount, tokenSymbol;
                return __generator(this, function (_b) {
                    _a = __read(quantity.split(' '), 2), amount = _a[0], tokenSymbol = _a[1];
                    return [2 /*return*/, this.newAmountFromDecimal(amount, tokenSymbol)];
                });
            });
        };
        /**
         * @param {?} decimalAmount
         * @param {?} tokenSymbol
         * @return {?}
         */
        CurrencyAmountWithPriceFactory.prototype.newAmountFromDecimal = /**
         * @param {?} decimalAmount
         * @param {?} tokenSymbol
         * @return {?}
         */
        function (decimalAmount, tokenSymbol) {
            return __awaiter(this, void 0, void 0, function () {
                var data, _a, _b;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0: return [4 /*yield*/, this.coinDataProvider.getCoinDataBySymbol(tokenSymbol)];
                        case 1:
                            data = _c.sent();
                            _a = CurrencyAmountWithPrice.bind;
                            _b = [void 0, decimalAmount,
                                data];
                            return [4 /*yield*/, this.priceService.getPriceInUSD(data.symbol)];
                        case 2: return [2 /*return*/, new (_a.apply(CurrencyAmountWithPrice, _b.concat([_c.sent()])))()];
                    }
                });
            });
        };
        /**
         * @param {?} decimalAmount
         * @param {?} coinId
         * @return {?}
         */
        CurrencyAmountWithPriceFactory.prototype.newAmountFromDecimalAndCoinId = /**
         * @param {?} decimalAmount
         * @param {?} coinId
         * @return {?}
         */
        function (decimalAmount, coinId) {
            return __awaiter(this, void 0, void 0, function () {
                var data, _a, _b;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0: return [4 /*yield*/, this.coinDataProvider.getCoinData(coinId)];
                        case 1:
                            data = _c.sent();
                            _a = CurrencyAmountWithPrice.bind;
                            _b = [void 0, decimalAmount,
                                data];
                            return [4 /*yield*/, this.priceService.getPriceInUSD(data.symbol)];
                        case 2: return [2 /*return*/, new (_a.apply(CurrencyAmountWithPrice, _b.concat([_c.sent()])))()];
                    }
                });
            });
        };
        /**
         * @param {?} integerSubunits
         * @param {?} tokenSymbol
         * @return {?}
         */
        CurrencyAmountWithPriceFactory.prototype.newAmountFromInteger = /**
         * @param {?} integerSubunits
         * @param {?} tokenSymbol
         * @return {?}
         */
        function (integerSubunits, tokenSymbol) {
            return __awaiter(this, void 0, void 0, function () {
                var data, decimalAmount, _a, _b;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0: return [4 /*yield*/, this.coinDataProvider.getCoinDataBySymbol(tokenSymbol)];
                        case 1:
                            data = _c.sent();
                            decimalAmount = new big_js.Big(integerSubunits).div(Math.pow(10, data.precision));
                            _a = CurrencyAmountWithPrice.bind;
                            _b = [void 0, decimalAmount,
                                data];
                            return [4 /*yield*/, this.priceService.getPriceInUSD(data.symbol)];
                        case 2: return [2 /*return*/, new (_a.apply(CurrencyAmountWithPrice, _b.concat([_c.sent()])))()];
                    }
                });
            });
        };
        return CurrencyAmountWithPriceFactory;
    }());
    if (false) {
        /** @type {?} */
        CurrencyAmountWithPriceFactory.prototype.coinDataProvider;
        /** @type {?} */
        CurrencyAmountWithPriceFactory.prototype.priceService;
    }
    /** @type {?} */
    var currencyAmountWithPriceFactory;
    /**
     * @param {?} database
     * @param {?=} priceService
     * @return {?}
     * @this {*}
     */
    function getCurrencyAmountWithPriceFactory(database, priceService) {
        if (priceService === void 0) { priceService = undefined; }
        return __awaiter(this, void 0, void 0, function () {
            var coinDataProvider;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(currencyAmountWithPriceFactory == undefined)) return [3 /*break*/, 4];
                        coinDataProvider = new CoinDataProvider(database);
                        return [4 /*yield*/, coinDataProvider.init()];
                    case 1:
                        _a.sent();
                        if (!(priceService == undefined)) return [3 /*break*/, 3];
                        return [4 /*yield*/, getCoinPriceService(database)];
                    case 2:
                        priceService = _a.sent();
                        _a.label = 3;
                    case 3:
                        currencyAmountWithPriceFactory = new CurrencyAmountWithPriceFactory(coinDataProvider, priceService);
                        _a.label = 4;
                    case 4: return [2 /*return*/, currencyAmountWithPriceFactory];
                }
            });
        });
    }

    exports.AccountRepository = AccountRepository;
    exports.AndExpressionGroup = AndExpressionGroup;
    exports.Coin = Coin;
    exports.CoinDataProvider = CoinDataProvider;
    exports.CoinDatabaseService = CoinDatabaseService;
    exports.CoinId = CoinId;
    exports.CoinSymbol = CoinSymbol;
    exports.DecimalForPreciseMath = DecimalForPreciseMath;
    exports.EosTransactionExecutor = EosTransactionExecutor;
    exports.EosTransactionExecutorBase = EosTransactionExecutorBase;
    exports.EosUtility = EosUtility;
    exports.Expression = Expression;
    exports.ExpressionGroup = ExpressionGroup;
    exports.ExpressionGroupJoiner = ExpressionGroupJoiner;
    exports.ExpressionOperator = ExpressionOperator;
    exports.FieldIsNotNullStatement = FieldIsNotNullStatement;
    exports.FieldIsNullStatement = FieldIsNullStatement;
    exports.FieldStatement = FieldStatement;
    exports.MemoPrefix = MemoPrefix;
    exports.MySQLConnectionManager = MySQLConnectionManager;
    exports.MySQLDatabase = MySQLDatabase;
    exports.MySQLExpression = MySQLExpression;
    exports.MySQLQuery = MySQLQuery;
    exports.MySQLQueryBuilder = MySQLQueryBuilder;
    exports.MySQLRepository = MySQLRepository;
    exports.MySQLTransactionManager = MySQLTransactionManager;
    exports.MySQLValue = MySQLValue;
    exports.MySQLValueType = MySQLValueType;
    exports.NowUpdateStatement = NowUpdateStatement;
    exports.NullUpdateStatement = NullUpdateStatement;
    exports.NumberForPreciseMath = NumberForPreciseMath;
    exports.NumberForPreciseMathBase = NumberForPreciseMathBase;
    exports.NumberStatement = NumberStatement;
    exports.OrExpressionGroup = OrExpressionGroup;
    exports.PreciseDecimal = PreciseDecimal;
    exports.PreciseMath = PreciseMath;
    exports.PreciseNumber = PreciseNumber;
    exports.SavedWithdrawalRequest = SavedWithdrawalRequest;
    exports.SingleExpression = SingleExpression;
    exports.StringExpressionStatement = StringExpressionStatement;
    exports.StringStatement = StringStatement;
    exports.UpdateStatements = UpdateStatements;
    exports.UserAccountDatabaseService = UserAccountDatabaseService;
    exports.WithdrawalContractService = WithdrawalContractService;
    exports.createExpressionGroup = createExpressionGroup;
    exports.executeQuery = executeQuery;
    exports.generateMnemonicPhrase = generateMnemonicPhrase;
    exports.getCoinPriceService = getCoinPriceService;
    exports.getCurrencyAmountFactory = getCurrencyAmountFactory;
    exports.getCurrencyAmountWithPriceFactory = getCurrencyAmountWithPriceFactory;
    exports.getEosJs = getEosJs;
    exports.getMnemonicUtil = getMnemonicUtil;
    exports.httpGet = httpGet;
    exports.httpGetJson = httpGetJson;
    exports.httpsGetJson = httpsGetJson;
    exports.isEosTxnIrreversible = isEosTxnIrreversible;
    exports.isWaxTxnIrreversible = isWaxTxnIrreversible;
    exports.recoverPublicKey = recoverPublicKey;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=earnbet-common-back-end.umd.js.map
