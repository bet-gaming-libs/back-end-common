/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/currency-amount/currency-amount-factory.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Big } from 'big.js';
import { getCoinPriceService } from '../../util/coin-util';
import { CoinDataProvider } from '../database/coin-data-provider';
import { NumberForPreciseMathBase, PreciseDecimal } from './precise-math-numbers';
class PreciseCurrencyAmount extends PreciseDecimal {
    /**
     * @param {?} decimalValue
     * @param {?} currency
     */
    constructor(decimalValue, currency) {
        super(decimalValue, currency.precision);
        this.currency = currency;
    }
    /**
     * @return {?}
     */
    get quantity() {
        return this.decimal + ' ' + this.currency.symbol;
    }
}
if (false) {
    /** @type {?} */
    PreciseCurrencyAmount.prototype.currency;
}
class PreciseCurrencyAmountWithPrice extends PreciseCurrencyAmount {
    /**
     * @param {?} decimalValue
     * @param {?} currency
     * @param {?} priceInUSD
     */
    constructor(decimalValue, currency, priceInUSD) {
        super(decimalValue, currency);
        this.priceInUSD = priceInUSD;
    }
    /**
     * @return {?}
     */
    get amountInUSD() {
        this.checkPrice();
        return (new Big(this.decimal))
            .times(this.priceInUSD)
            .round(6, 0 /* RoundDown */)
            .toString();
    }
    /**
     * @private
     * @return {?}
     */
    checkPrice() {
        if (this.priceInUSD == undefined) {
            throw new Error('Price for Currency IS NOT DEFINED!');
        }
    }
}
if (false) {
    /** @type {?} */
    PreciseCurrencyAmountWithPrice.prototype.priceInUSD;
}
class MatchingCurrencyValidator {
    /**
     * @param {?} currency
     */
    constructor(currency) {
        this.currency = currency;
    }
    /**
     * @param {?} __0
     * @return {?}
     */
    isMatchingType({ currency }) {
        if (this.currency.symbol != currency.symbol ||
            this.currency.precision != currency.precision) {
            throw new Error('both amounts must be the same currency!');
        }
        else {
            return true;
        }
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MatchingCurrencyValidator.prototype.currency;
}
class PreciseCurrencyAmountFactory {
    /**
     * @param {?} currency
     */
    constructor(currency) {
        this.currency = currency;
        this.validator = new MatchingCurrencyValidator(currency);
    }
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    newAmountFromInteger(integer, precision) {
        /** @type {?} */
        const number = new PreciseCurrencyAmount(new Big(integer).div(Math.pow(10, precision)), this.currency);
        this.validator.isMatchingType(number);
        return number;
    }
    /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    newAmountFromDecimal(decimal, precision) {
        /** @type {?} */
        const number = new PreciseCurrencyAmount(decimal, this.currency);
        this.validator.isMatchingType(number);
        return number;
    }
}
if (false) {
    /** @type {?} */
    PreciseCurrencyAmountFactory.prototype.validator;
    /**
     * @type {?}
     * @private
     */
    PreciseCurrencyAmountFactory.prototype.currency;
}
class PreciseCurrencyAmountWithPriceFactory {
    /**
     * @param {?} currency
     * @param {?} priceInUSD
     */
    constructor(currency, priceInUSD) {
        this.currency = currency;
        this.priceInUSD = priceInUSD;
        this.validator = new MatchingCurrencyValidator(currency);
    }
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    newAmountFromInteger(integer, precision) {
        /** @type {?} */
        const number = new PreciseCurrencyAmountWithPrice(new Big(integer).div(Math.pow(10, precision)), this.currency, this.priceInUSD);
        this.validator.isMatchingType(number);
        return number;
    }
    /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    newAmountFromDecimal(decimal, precision) {
        /** @type {?} */
        const number = new PreciseCurrencyAmountWithPrice(decimal, this.currency, this.priceInUSD);
        this.validator.isMatchingType(number);
        return number;
    }
}
if (false) {
    /** @type {?} */
    PreciseCurrencyAmountWithPriceFactory.prototype.validator;
    /**
     * @type {?}
     * @private
     */
    PreciseCurrencyAmountWithPriceFactory.prototype.currency;
    /**
     * @type {?}
     * @private
     */
    PreciseCurrencyAmountWithPriceFactory.prototype.priceInUSD;
}
class CurrencyAmount extends NumberForPreciseMathBase {
    /**
     * @param {?} decimalValue
     * @param {?} currency
     */
    constructor(decimalValue, currency) {
        super(currency.precision, new Big(decimalValue).times(Math.pow(10, currency.precision)).round(0, 0 /* RoundDown */), new PreciseCurrencyAmountFactory(currency));
        this.currency = currency;
    }
    /**
     * @return {?}
     */
    get quantity() {
        return this.number.quantity;
    }
}
if (false) {
    /** @type {?} */
    CurrencyAmount.prototype.currency;
}
class CurrencyAmountWithPrice extends NumberForPreciseMathBase {
    /**
     * @param {?} decimalValue
     * @param {?} currency
     * @param {?} priceInUSD
     */
    constructor(decimalValue, currency, priceInUSD) {
        super(currency.precision, new Big(decimalValue).times(Math.pow(10, currency.precision)).round(0, 0 /* RoundDown */), new PreciseCurrencyAmountWithPriceFactory(currency, priceInUSD));
        this.currency = currency;
    }
    /**
     * @return {?}
     */
    get amountInUSD() {
        return this.number.amountInUSD;
    }
    /**
     * @return {?}
     */
    get priceInUSD() {
        return this.number.priceInUSD;
    }
    /**
     * @return {?}
     */
    get quantity() {
        return this.number.quantity;
    }
}
if (false) {
    /** @type {?} */
    CurrencyAmountWithPrice.prototype.currency;
}
class CurrencyAmountFactory {
    /**
     * @param {?} coinDataProvider
     */
    constructor(coinDataProvider) {
        this.coinDataProvider = coinDataProvider;
    }
    /**
     * @param {?} quantity
     * @return {?}
     */
    newAmountFromQuantity(quantity) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const [amount, tokenSymbol] = quantity.split(' ');
            return this.newAmountFromDecimal(amount, tokenSymbol);
        });
    }
    /**
     * @param {?} decimalAmount
     * @param {?} tokenSymbol
     * @return {?}
     */
    newAmountFromDecimal(decimalAmount, tokenSymbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.coinDataProvider.getCoinDataBySymbol(tokenSymbol);
            return new CurrencyAmount(decimalAmount, data);
        });
    }
    /**
     * @param {?} decimalAmount
     * @param {?} coinId
     * @return {?}
     */
    newAmountFromDecimalAndCoinId(decimalAmount, coinId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.coinDataProvider.getCoinData(coinId);
            return new CurrencyAmount(decimalAmount, data);
        });
    }
    /**
     * @param {?} integerSubunits
     * @param {?} tokenSymbol
     * @return {?}
     */
    newAmountFromInteger(integerSubunits, tokenSymbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.coinDataProvider.getCoinDataBySymbol(tokenSymbol);
            /** @type {?} */
            const decimalAmount = new Big(integerSubunits).div(Math.pow(10, data.precision));
            return new CurrencyAmount(decimalAmount, data);
        });
    }
}
if (false) {
    /** @type {?} */
    CurrencyAmountFactory.prototype.coinDataProvider;
}
/** @type {?} */
let currencyAmountFactory;
/**
 * @param {?} database
 * @return {?}
 * @this {*}
 */
export function getCurrencyAmountFactory(database) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        if (currencyAmountFactory == undefined) {
            /** @type {?} */
            const coinDataProvider = new CoinDataProvider(database);
            yield coinDataProvider.init();
            currencyAmountFactory = new CurrencyAmountFactory(coinDataProvider);
        }
        return currencyAmountFactory;
    });
}
class CurrencyAmountWithPriceFactory {
    /**
     * @param {?} coinDataProvider
     * @param {?} priceService
     */
    constructor(coinDataProvider, priceService) {
        this.coinDataProvider = coinDataProvider;
        this.priceService = priceService;
    }
    /**
     * @param {?} quantity
     * @return {?}
     */
    newAmountFromQuantity(quantity) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const [amount, tokenSymbol] = quantity.split(' ');
            return this.newAmountFromDecimal(amount, tokenSymbol);
        });
    }
    /**
     * @param {?} decimalAmount
     * @param {?} tokenSymbol
     * @return {?}
     */
    newAmountFromDecimal(decimalAmount, tokenSymbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.coinDataProvider.getCoinDataBySymbol(tokenSymbol);
            return new CurrencyAmountWithPrice(decimalAmount, data, yield this.priceService.getPriceInUSD(data.symbol));
        });
    }
    /**
     * @param {?} decimalAmount
     * @param {?} coinId
     * @return {?}
     */
    newAmountFromDecimalAndCoinId(decimalAmount, coinId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.coinDataProvider.getCoinData(coinId);
            return new CurrencyAmountWithPrice(decimalAmount, data, yield this.priceService.getPriceInUSD(data.symbol));
        });
    }
    /**
     * @param {?} integerSubunits
     * @param {?} tokenSymbol
     * @return {?}
     */
    newAmountFromInteger(integerSubunits, tokenSymbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.coinDataProvider.getCoinDataBySymbol(tokenSymbol);
            /** @type {?} */
            const decimalAmount = new Big(integerSubunits).div(Math.pow(10, data.precision));
            return new CurrencyAmountWithPrice(decimalAmount, data, yield this.priceService.getPriceInUSD(data.symbol));
        });
    }
}
if (false) {
    /** @type {?} */
    CurrencyAmountWithPriceFactory.prototype.coinDataProvider;
    /** @type {?} */
    CurrencyAmountWithPriceFactory.prototype.priceService;
}
/** @type {?} */
let currencyAmountWithPriceFactory;
/**
 * @param {?} database
 * @param {?=} priceService
 * @return {?}
 * @this {*}
 */
export function getCurrencyAmountWithPriceFactory(database, priceService = undefined) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        if (currencyAmountWithPriceFactory == undefined) {
            /** @type {?} */
            const coinDataProvider = new CoinDataProvider(database);
            yield coinDataProvider.init();
            if (priceService == undefined) {
                priceService = yield getCoinPriceService(database);
            }
            currencyAmountWithPriceFactory = new CurrencyAmountWithPriceFactory(coinDataProvider, priceService);
        }
        return currencyAmountWithPriceFactory;
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VycmVuY3ktYW1vdW50LWZhY3RvcnkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9jdXJyZW5jeS9jdXJyZW5jeS1hbW91bnQvY3VycmVuY3ktYW1vdW50LWZhY3RvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLEdBQUcsRUFBMkIsTUFBTSxRQUFRLENBQUM7QUFJdEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFM0QsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sZ0NBQWdDLENBQUM7QUFDaEUsT0FBTyxFQUFDLHdCQUF3QixFQUFFLGNBQWMsRUFBQyxNQUFNLHdCQUF3QixDQUFDO0FBSWhGLE1BQU0scUJBQXNCLFNBQVEsY0FBYzs7Ozs7SUFDakQsWUFDQyxZQUF1QixFQUNkLFFBQW1CO1FBRTVCLEtBQUssQ0FBQyxZQUFZLEVBQUUsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRi9CLGFBQVEsR0FBUixRQUFRLENBQVc7SUFHN0IsQ0FBQzs7OztJQUVELElBQUksUUFBUTtRQUNYLE9BQU8sSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7SUFDbEQsQ0FBQztDQUNEOzs7SUFSQyx5Q0FBNEI7O0FBVTlCLE1BQU0sOEJBQStCLFNBQVEscUJBQXFCOzs7Ozs7SUFDakUsWUFDQyxZQUF1QixFQUN2QixRQUFtQixFQUNWLFVBQWtCO1FBRTNCLEtBQUssQ0FBQyxZQUFZLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFGckIsZUFBVSxHQUFWLFVBQVUsQ0FBUTtJQUc1QixDQUFDOzs7O0lBRUQsSUFBSSxXQUFXO1FBQ2QsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBRWxCLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDNUIsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7YUFDdEIsS0FBSyxDQUFDLENBQUMsb0JBQXlCO2FBQ2hDLFFBQVEsRUFBRSxDQUFDO0lBQ2QsQ0FBQzs7Ozs7SUFFTyxVQUFVO1FBQ2pCLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxTQUFTLEVBQUU7WUFDakMsTUFBTSxJQUFJLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDO1NBQ3REO0lBQ0YsQ0FBQztDQUNEOzs7SUFuQkMsb0RBQTJCOztBQXNCN0IsTUFBTSx5QkFBeUI7Ozs7SUFDOUIsWUFBb0IsUUFBbUI7UUFBbkIsYUFBUSxHQUFSLFFBQVEsQ0FBVztJQUN2QyxDQUFDOzs7OztJQUVELGNBQWMsQ0FBQyxFQUFDLFFBQVEsRUFBa0I7UUFDekMsSUFDQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsTUFBTTtZQUN2QyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsSUFBSSxRQUFRLENBQUMsU0FBUyxFQUM1QztZQUNELE1BQU0sSUFBSSxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQztTQUMzRDthQUFNO1lBQ04sT0FBTyxJQUFJLENBQUM7U0FDWjtJQUNGLENBQUM7Q0FDRDs7Ozs7O0lBYlksNkNBQTJCOztBQWV4QyxNQUFNLDRCQUE0Qjs7OztJQUdqQyxZQUE2QixRQUFtQjtRQUFuQixhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQy9DLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSx5QkFBeUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMxRCxDQUFDOzs7Ozs7SUFFRCxvQkFBb0IsQ0FBQyxPQUFrQixFQUFFLFNBQWlCOztjQUNuRCxNQUFNLEdBQUcsSUFBSSxxQkFBcUIsQ0FDdkMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsQ0FDdkIsRUFDRCxJQUFJLENBQUMsUUFBUSxDQUNiO1FBRUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFdEMsT0FBTyxNQUFNLENBQUM7SUFDZixDQUFDOzs7Ozs7SUFDRCxvQkFBb0IsQ0FBQyxPQUFrQixFQUFFLFNBQWlCOztjQUNuRCxNQUFNLEdBQUcsSUFBSSxxQkFBcUIsQ0FDdkMsT0FBTyxFQUNQLElBQUksQ0FBQyxRQUFRLENBQ2I7UUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUV0QyxPQUFPLE1BQU0sQ0FBQztJQUNmLENBQUM7Q0FDRDs7O0lBNUJBLGlEQUF5RTs7Ozs7SUFFN0QsZ0RBQW9DOztBQTRCakQsTUFBTSxxQ0FBcUM7Ozs7O0lBRzFDLFlBQ2tCLFFBQW1CLEVBQ25CLFVBQWtCO1FBRGxCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIsZUFBVSxHQUFWLFVBQVUsQ0FBUTtRQUVuQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUkseUJBQXlCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDMUQsQ0FBQzs7Ozs7O0lBRUQsb0JBQW9CLENBQUMsT0FBa0IsRUFBRSxTQUFpQjs7Y0FDbkQsTUFBTSxHQUFHLElBQUksOEJBQThCLENBQ2hELElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FDbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsU0FBUyxDQUFDLENBQ3ZCLEVBQ0QsSUFBSSxDQUFDLFFBQVEsRUFDYixJQUFJLENBQUMsVUFBVSxDQUNmO1FBRUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFdEMsT0FBTyxNQUFNLENBQUM7SUFDZixDQUFDOzs7Ozs7SUFDRCxvQkFBb0IsQ0FBQyxPQUFrQixFQUFFLFNBQWlCOztjQUNuRCxNQUFNLEdBQUcsSUFBSSw4QkFBOEIsQ0FDaEQsT0FBTyxFQUNQLElBQUksQ0FBQyxRQUFRLEVBQ2IsSUFBSSxDQUFDLFVBQVUsQ0FDZjtRQUVELElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXRDLE9BQU8sTUFBTSxDQUFDO0lBQ2YsQ0FBQztDQUNEOzs7SUFqQ0EsMERBQXlFOzs7OztJQUd4RSx5REFBb0M7Ozs7O0lBQ3BDLDJEQUFtQzs7QUFnQ3JDLE1BQU0sY0FBZSxTQUFRLHdCQUFnRDs7Ozs7SUFFNUUsWUFDQyxZQUF1QixFQUNkLFFBQW1CO1FBRTVCLEtBQUssQ0FDSixRQUFRLENBQUMsU0FBUyxFQUNsQixJQUFJLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxLQUFLLENBQzFCLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FDaEMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxvQkFBeUIsRUFDbEMsSUFBSSw0QkFBNEIsQ0FBQyxRQUFRLENBQUMsQ0FDMUMsQ0FBQztRQVJPLGFBQVEsR0FBUixRQUFRLENBQVc7SUFTN0IsQ0FBQzs7OztJQUVELElBQUksUUFBUTtRQUNYLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7SUFDN0IsQ0FBQztDQUNEOzs7SUFkQyxrQ0FBNEI7O0FBZ0I5QixNQUFNLHVCQUF3QixTQUFRLHdCQUF5RDs7Ozs7O0lBRTlGLFlBQ0MsWUFBdUIsRUFDZCxRQUFtQixFQUM1QixVQUFrQjtRQUVsQixLQUFLLENBQ0osUUFBUSxDQUFDLFNBQVMsRUFDbEIsSUFBSSxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUMxQixJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRSxRQUFRLENBQUMsU0FBUyxDQUFDLENBQ2hDLENBQUMsS0FBSyxDQUFDLENBQUMsb0JBQXlCLEVBQ2xDLElBQUkscUNBQXFDLENBQ3hDLFFBQVEsRUFBRSxVQUFVLENBQ3BCLENBQ0QsQ0FBQztRQVhPLGFBQVEsR0FBUixRQUFRLENBQVc7SUFZN0IsQ0FBQzs7OztJQUVELElBQUksV0FBVztRQUNkLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUM7SUFDaEMsQ0FBQzs7OztJQUVELElBQUksVUFBVTtRQUNiLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUM7SUFDL0IsQ0FBQzs7OztJQUVELElBQUksUUFBUTtRQUNYLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7SUFDN0IsQ0FBQztDQUNEOzs7SUF6QkMsMkNBQTRCOztBQThCOUIsTUFBTSxxQkFBcUI7Ozs7SUFDMUIsWUFDVSxnQkFBbUM7UUFBbkMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFtQjtJQUMxQyxDQUFDOzs7OztJQUVFLHFCQUFxQixDQUFDLFFBQWdCOztrQkFDckMsQ0FBQyxNQUFNLEVBQUUsV0FBVyxDQUFDLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7WUFFakQsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQy9CLE1BQU0sRUFDTixXQUFXLENBQ1gsQ0FBQztRQUNILENBQUM7S0FBQTs7Ozs7O0lBRUssb0JBQW9CLENBQUMsYUFBd0IsRUFBRSxXQUFtQjs7O2tCQUNqRSxJQUFJLEdBQUcsTUFBTSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDO1lBRXpFLE9BQU8sSUFBSSxjQUFjLENBQ3hCLGFBQWEsRUFDYixJQUFJLENBQ0osQ0FBQztRQUNILENBQUM7S0FBQTs7Ozs7O0lBRUssNkJBQTZCLENBQUMsYUFBd0IsRUFBRSxNQUFjOzs7a0JBQ3JFLElBQUksR0FBRyxNQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBRTVELE9BQU8sSUFBSSxjQUFjLENBQ3hCLGFBQWEsRUFDYixJQUFJLENBQ0osQ0FBQztRQUNILENBQUM7S0FBQTs7Ozs7O0lBRUssb0JBQW9CLENBQUMsZUFBMEIsRUFBRSxXQUFtQjs7O2tCQUNuRSxJQUFJLEdBQUcsTUFBTSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDOztrQkFFbkUsYUFBYSxHQUFHLElBQUksR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEdBQUcsQ0FDakQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUM1QjtZQUVELE9BQU8sSUFBSSxjQUFjLENBQ3hCLGFBQWEsRUFDYixJQUFJLENBQ0osQ0FBQztRQUNILENBQUM7S0FBQTtDQUNEOzs7SUExQ0MsaURBQTRDOzs7SUE0QzFDLHFCQUE0Qzs7Ozs7O0FBR2hELE1BQU0sVUFBZ0Isd0JBQXdCLENBQzdDLFFBQThCOztRQUU5QixJQUFJLHFCQUFxQixJQUFJLFNBQVMsRUFBRTs7a0JBRWpDLGdCQUFnQixHQUFHLElBQUksZ0JBQWdCLENBQzVDLFFBQVEsQ0FDUjtZQUVELE1BQU0sZ0JBQWdCLENBQUMsSUFBSSxFQUFFLENBQUM7WUFFOUIscUJBQXFCLEdBQUcsSUFBSSxxQkFBcUIsQ0FDaEQsZ0JBQWdCLENBQ2hCLENBQUM7U0FDRjtRQUVELE9BQU8scUJBQXFCLENBQUM7SUFDOUIsQ0FBQztDQUFBO0FBSUQsTUFBTSw4QkFBOEI7Ozs7O0lBQ25DLFlBQ1UsZ0JBQW1DLEVBQ25DLFlBQW1DO1FBRG5DLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBbUI7UUFDbkMsaUJBQVksR0FBWixZQUFZLENBQXVCO0lBRTdDLENBQUM7Ozs7O0lBRUsscUJBQXFCLENBQUMsUUFBZ0I7O2tCQUNyQyxDQUFDLE1BQU0sRUFBRSxXQUFXLENBQUMsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztZQUVqRCxPQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FDL0IsTUFBTSxFQUNOLFdBQVcsQ0FDWCxDQUFDO1FBQ0gsQ0FBQztLQUFBOzs7Ozs7SUFFSyxvQkFBb0IsQ0FBQyxhQUF3QixFQUFFLFdBQW1COzs7a0JBQ2pFLElBQUksR0FBRyxNQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUM7WUFFekUsT0FBTyxJQUFJLHVCQUF1QixDQUNqQyxhQUFhLEVBQ2IsSUFBSSxFQUNKLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUNsRCxDQUFDO1FBQ0gsQ0FBQztLQUFBOzs7Ozs7SUFFSyw2QkFBNkIsQ0FBQyxhQUF3QixFQUFFLE1BQWM7OztrQkFDckUsSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7WUFFNUQsT0FBTyxJQUFJLHVCQUF1QixDQUNqQyxhQUFhLEVBQ2IsSUFBSSxFQUNKLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUNsRCxDQUFDO1FBQ0gsQ0FBQztLQUFBOzs7Ozs7SUFFSyxvQkFBb0IsQ0FBQyxlQUEwQixFQUFFLFdBQW1COzs7a0JBQ25FLElBQUksR0FBRyxNQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUM7O2tCQUVuRSxhQUFhLEdBQUcsSUFBSSxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsR0FBRyxDQUNqRCxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQzVCO1lBRUQsT0FBTyxJQUFJLHVCQUF1QixDQUNqQyxhQUFhLEVBQ2IsSUFBSSxFQUNKLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUNsRCxDQUFDO1FBQ0gsQ0FBQztLQUFBO0NBQ0Q7OztJQS9DQywwREFBNEM7O0lBQzVDLHNEQUE0Qzs7O0lBaUQxQyw4QkFBOEQ7Ozs7Ozs7QUFHbEUsTUFBTSxVQUFnQixpQ0FBaUMsQ0FDdEQsUUFBOEIsRUFDOUIsZUFBc0MsU0FBUzs7UUFFL0MsSUFBSSw4QkFBOEIsSUFBSSxTQUFTLEVBQUU7O2tCQUUxQyxnQkFBZ0IsR0FBRyxJQUFJLGdCQUFnQixDQUM1QyxRQUFRLENBQ1I7WUFFRCxNQUFNLGdCQUFnQixDQUFDLElBQUksRUFBRSxDQUFDO1lBRTlCLElBQUksWUFBWSxJQUFJLFNBQVMsRUFBRTtnQkFDOUIsWUFBWSxHQUFHLE1BQU0sbUJBQW1CLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDbkQ7WUFFRCw4QkFBOEIsR0FBRyxJQUFJLDhCQUE4QixDQUNsRSxnQkFBZ0IsRUFDaEIsWUFBWSxDQUNaLENBQUM7U0FDRjtRQUVELE9BQU8sOEJBQThCLENBQUM7SUFDdkMsQ0FBQztDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQmlnLCBSb3VuZGluZ01vZGUsIEJpZ1NvdXJjZSB9IGZyb20gJ2JpZy5qcyc7XG5cbmltcG9ydCB7IElDdXJyZW5jeVByaWNlU2VydmljZSwgSUN1cnJlbmN5QW1vdW50LCBJQ3VycmVuY3ksIElDb2luRGF0YVByb3ZpZGVyLCBJQ3VycmVuY3lBbW91bnRGYWN0b3J5LCBJQ3VycmVuY3lBbW91bnRXaXRoUHJpY2UsIElDdXJyZW5jeUFtb3VudFdpdGhQcmljZUZhY3RvcnksIElNYXRjaGluZ051bWJlclR5cGVWYWxpZGF0b3IsIElQcmVjaXNlTnVtYmVyRmFjdG9yeSwgSVByZWNpc2VDdXJyZW5jeUFtb3VudCwgSVByZWNpc2VDdXJyZW5jeUFtb3VudFdpdGhQcmljZSB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBDb2luSWQgfSBmcm9tICcuLi9kYXRhYmFzZS9jb2lucyc7XG5pbXBvcnQgeyBnZXRDb2luUHJpY2VTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vdXRpbC9jb2luLXV0aWwnO1xuaW1wb3J0IHtJQ29pbkRhdGFiYXNlU2VydmljZX0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7Q29pbkRhdGFQcm92aWRlcn0gZnJvbSAnLi4vZGF0YWJhc2UvY29pbi1kYXRhLXByb3ZpZGVyJztcbmltcG9ydCB7TnVtYmVyRm9yUHJlY2lzZU1hdGhCYXNlLCBQcmVjaXNlRGVjaW1hbH0gZnJvbSAnLi9wcmVjaXNlLW1hdGgtbnVtYmVycyc7XG5cblxuXG5jbGFzcyBQcmVjaXNlQ3VycmVuY3lBbW91bnQgZXh0ZW5kcyBQcmVjaXNlRGVjaW1hbCBpbXBsZW1lbnRzIElQcmVjaXNlQ3VycmVuY3lBbW91bnQge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRkZWNpbWFsVmFsdWU6IEJpZ1NvdXJjZSxcblx0XHRyZWFkb25seSBjdXJyZW5jeTogSUN1cnJlbmN5LFxuXHQpIHtcblx0XHRzdXBlcihkZWNpbWFsVmFsdWUsIGN1cnJlbmN5LnByZWNpc2lvbik7XG5cdH1cblxuXHRnZXQgcXVhbnRpdHkoKSB7XG5cdFx0cmV0dXJuIHRoaXMuZGVjaW1hbCArICcgJyArIHRoaXMuY3VycmVuY3kuc3ltYm9sO1xuXHR9XG59XG5cbmNsYXNzIFByZWNpc2VDdXJyZW5jeUFtb3VudFdpdGhQcmljZSBleHRlbmRzIFByZWNpc2VDdXJyZW5jeUFtb3VudCBpbXBsZW1lbnRzIElQcmVjaXNlQ3VycmVuY3lBbW91bnRXaXRoUHJpY2Uge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRkZWNpbWFsVmFsdWU6IEJpZ1NvdXJjZSxcblx0XHRjdXJyZW5jeTogSUN1cnJlbmN5LFxuXHRcdHJlYWRvbmx5IHByaWNlSW5VU0Q6IG51bWJlcixcblx0KSB7XG5cdFx0c3VwZXIoZGVjaW1hbFZhbHVlLCBjdXJyZW5jeSk7XG5cdH1cblxuXHRnZXQgYW1vdW50SW5VU0QoKSB7XG5cdFx0dGhpcy5jaGVja1ByaWNlKCk7XG5cblx0XHRyZXR1cm4gKG5ldyBCaWcodGhpcy5kZWNpbWFsKSlcblx0XHRcdC50aW1lcyh0aGlzLnByaWNlSW5VU0QpXG5cdFx0XHQucm91bmQoNiwgUm91bmRpbmdNb2RlLlJvdW5kRG93bilcblx0XHRcdC50b1N0cmluZygpO1xuXHR9XG5cblx0cHJpdmF0ZSBjaGVja1ByaWNlKCkge1xuXHRcdGlmICh0aGlzLnByaWNlSW5VU0QgPT0gdW5kZWZpbmVkKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ1ByaWNlIGZvciBDdXJyZW5jeSBJUyBOT1QgREVGSU5FRCEnKTtcblx0XHR9XG5cdH1cbn1cblxuXG5jbGFzcyBNYXRjaGluZ0N1cnJlbmN5VmFsaWRhdG9yIGltcGxlbWVudHMgSU1hdGNoaW5nTnVtYmVyVHlwZVZhbGlkYXRvcjxJUHJlY2lzZUN1cnJlbmN5QW1vdW50PiB7XG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgY3VycmVuY3k6IElDdXJyZW5jeSkge1xuXHR9XG5cblx0aXNNYXRjaGluZ1R5cGUoe2N1cnJlbmN5fTogSUN1cnJlbmN5QW1vdW50KTogYm9vbGVhbiB7XG5cdFx0aWYgKFxuXHRcdFx0dGhpcy5jdXJyZW5jeS5zeW1ib2wgIT0gY3VycmVuY3kuc3ltYm9sIHx8XG5cdFx0XHR0aGlzLmN1cnJlbmN5LnByZWNpc2lvbiAhPSBjdXJyZW5jeS5wcmVjaXNpb25cblx0XHQpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignYm90aCBhbW91bnRzIG11c3QgYmUgdGhlIHNhbWUgY3VycmVuY3khJyk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH1cblx0fVxufVxuXG5jbGFzcyBQcmVjaXNlQ3VycmVuY3lBbW91bnRGYWN0b3J5IGltcGxlbWVudHMgSVByZWNpc2VOdW1iZXJGYWN0b3J5PElQcmVjaXNlQ3VycmVuY3lBbW91bnQ+IHtcblx0cmVhZG9ubHkgdmFsaWRhdG9yOiBJTWF0Y2hpbmdOdW1iZXJUeXBlVmFsaWRhdG9yPElQcmVjaXNlQ3VycmVuY3lBbW91bnQ+O1xuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcmVhZG9ubHkgY3VycmVuY3k6IElDdXJyZW5jeSkge1xuXHRcdHRoaXMudmFsaWRhdG9yID0gbmV3IE1hdGNoaW5nQ3VycmVuY3lWYWxpZGF0b3IoY3VycmVuY3kpO1xuXHR9XG5cblx0bmV3QW1vdW50RnJvbUludGVnZXIoaW50ZWdlcjogQmlnU291cmNlLCBwcmVjaXNpb246IG51bWJlcik6IElQcmVjaXNlQ3VycmVuY3lBbW91bnQge1xuXHRcdGNvbnN0IG51bWJlciA9IG5ldyBQcmVjaXNlQ3VycmVuY3lBbW91bnQoXG5cdFx0XHRuZXcgQmlnKGludGVnZXIpLmRpdihcblx0XHRcdFx0TWF0aC5wb3coMTAsIHByZWNpc2lvbilcblx0XHRcdCksXG5cdFx0XHR0aGlzLmN1cnJlbmN5XG5cdFx0KTtcblxuXHRcdHRoaXMudmFsaWRhdG9yLmlzTWF0Y2hpbmdUeXBlKG51bWJlcik7XG5cblx0XHRyZXR1cm4gbnVtYmVyO1xuXHR9XG5cdG5ld0Ftb3VudEZyb21EZWNpbWFsKGRlY2ltYWw6IEJpZ1NvdXJjZSwgcHJlY2lzaW9uOiBudW1iZXIpOiBJUHJlY2lzZUN1cnJlbmN5QW1vdW50IHtcblx0XHRjb25zdCBudW1iZXIgPSBuZXcgUHJlY2lzZUN1cnJlbmN5QW1vdW50KFxuXHRcdFx0ZGVjaW1hbCxcblx0XHRcdHRoaXMuY3VycmVuY3lcblx0XHQpO1xuXG5cdFx0dGhpcy52YWxpZGF0b3IuaXNNYXRjaGluZ1R5cGUobnVtYmVyKTtcblxuXHRcdHJldHVybiBudW1iZXI7XG5cdH1cbn1cblxuY2xhc3MgUHJlY2lzZUN1cnJlbmN5QW1vdW50V2l0aFByaWNlRmFjdG9yeSBpbXBsZW1lbnRzIElQcmVjaXNlTnVtYmVyRmFjdG9yeTxJUHJlY2lzZUN1cnJlbmN5QW1vdW50V2l0aFByaWNlPiB7XG5cdHJlYWRvbmx5IHZhbGlkYXRvcjogSU1hdGNoaW5nTnVtYmVyVHlwZVZhbGlkYXRvcjxJUHJlY2lzZUN1cnJlbmN5QW1vdW50PjtcblxuXHRjb25zdHJ1Y3Rvcihcblx0XHRwcml2YXRlIHJlYWRvbmx5IGN1cnJlbmN5OiBJQ3VycmVuY3ksXG5cdFx0cHJpdmF0ZSByZWFkb25seSBwcmljZUluVVNEOiBudW1iZXIsXG5cdCkge1xuXHRcdHRoaXMudmFsaWRhdG9yID0gbmV3IE1hdGNoaW5nQ3VycmVuY3lWYWxpZGF0b3IoY3VycmVuY3kpO1xuXHR9XG5cblx0bmV3QW1vdW50RnJvbUludGVnZXIoaW50ZWdlcjogQmlnU291cmNlLCBwcmVjaXNpb246IG51bWJlcik6IElQcmVjaXNlQ3VycmVuY3lBbW91bnRXaXRoUHJpY2Uge1xuXHRcdGNvbnN0IG51bWJlciA9IG5ldyBQcmVjaXNlQ3VycmVuY3lBbW91bnRXaXRoUHJpY2UoXG5cdFx0XHRuZXcgQmlnKGludGVnZXIpLmRpdihcblx0XHRcdFx0TWF0aC5wb3coMTAsIHByZWNpc2lvbilcblx0XHRcdCksXG5cdFx0XHR0aGlzLmN1cnJlbmN5LFxuXHRcdFx0dGhpcy5wcmljZUluVVNEXG5cdFx0KTtcblxuXHRcdHRoaXMudmFsaWRhdG9yLmlzTWF0Y2hpbmdUeXBlKG51bWJlcik7XG5cblx0XHRyZXR1cm4gbnVtYmVyO1xuXHR9XG5cdG5ld0Ftb3VudEZyb21EZWNpbWFsKGRlY2ltYWw6IEJpZ1NvdXJjZSwgcHJlY2lzaW9uOiBudW1iZXIpOiBJUHJlY2lzZUN1cnJlbmN5QW1vdW50V2l0aFByaWNlIHtcblx0XHRjb25zdCBudW1iZXIgPSBuZXcgUHJlY2lzZUN1cnJlbmN5QW1vdW50V2l0aFByaWNlKFxuXHRcdFx0ZGVjaW1hbCxcblx0XHRcdHRoaXMuY3VycmVuY3ksXG5cdFx0XHR0aGlzLnByaWNlSW5VU0Rcblx0XHQpO1xuXG5cdFx0dGhpcy52YWxpZGF0b3IuaXNNYXRjaGluZ1R5cGUobnVtYmVyKTtcblxuXHRcdHJldHVybiBudW1iZXI7XG5cdH1cbn1cblxuXG5jbGFzcyBDdXJyZW5jeUFtb3VudCBleHRlbmRzIE51bWJlckZvclByZWNpc2VNYXRoQmFzZTxJUHJlY2lzZUN1cnJlbmN5QW1vdW50PlxuXHRpbXBsZW1lbnRzIElDdXJyZW5jeUFtb3VudCB7XG5cdGNvbnN0cnVjdG9yKFxuXHRcdGRlY2ltYWxWYWx1ZTogQmlnU291cmNlLFxuXHRcdHJlYWRvbmx5IGN1cnJlbmN5OiBJQ3VycmVuY3ksXG5cdCkge1xuXHRcdHN1cGVyKFxuXHRcdFx0Y3VycmVuY3kucHJlY2lzaW9uLFxuXHRcdFx0bmV3IEJpZyhkZWNpbWFsVmFsdWUpLnRpbWVzKFxuXHRcdFx0XHRNYXRoLnBvdygxMCwgY3VycmVuY3kucHJlY2lzaW9uKVxuXHRcdFx0KS5yb3VuZCgwLCBSb3VuZGluZ01vZGUuUm91bmREb3duKSxcblx0XHRcdG5ldyBQcmVjaXNlQ3VycmVuY3lBbW91bnRGYWN0b3J5KGN1cnJlbmN5KVxuXHRcdCk7XG5cdH1cblxuXHRnZXQgcXVhbnRpdHkoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLnF1YW50aXR5O1xuXHR9XG59XG5cbmNsYXNzIEN1cnJlbmN5QW1vdW50V2l0aFByaWNlIGV4dGVuZHMgTnVtYmVyRm9yUHJlY2lzZU1hdGhCYXNlPElQcmVjaXNlQ3VycmVuY3lBbW91bnRXaXRoUHJpY2U+XG5cdGltcGxlbWVudHMgSUN1cnJlbmN5QW1vdW50V2l0aFByaWNlIHtcblx0Y29uc3RydWN0b3IoXG5cdFx0ZGVjaW1hbFZhbHVlOiBCaWdTb3VyY2UsXG5cdFx0cmVhZG9ubHkgY3VycmVuY3k6IElDdXJyZW5jeSxcblx0XHRwcmljZUluVVNEOiBudW1iZXIsXG5cdCkge1xuXHRcdHN1cGVyKFxuXHRcdFx0Y3VycmVuY3kucHJlY2lzaW9uLFxuXHRcdFx0bmV3IEJpZyhkZWNpbWFsVmFsdWUpLnRpbWVzKFxuXHRcdFx0XHRNYXRoLnBvdygxMCwgY3VycmVuY3kucHJlY2lzaW9uKVxuXHRcdFx0KS5yb3VuZCgwLCBSb3VuZGluZ01vZGUuUm91bmREb3duKSxcblx0XHRcdG5ldyBQcmVjaXNlQ3VycmVuY3lBbW91bnRXaXRoUHJpY2VGYWN0b3J5KFxuXHRcdFx0XHRjdXJyZW5jeSwgcHJpY2VJblVTRFxuXHRcdFx0KVxuXHRcdCk7XG5cdH1cblxuXHRnZXQgYW1vdW50SW5VU0QoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmFtb3VudEluVVNEO1xuXHR9XG5cblx0Z2V0IHByaWNlSW5VU0QoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLnByaWNlSW5VU0Q7XG5cdH1cblxuXHRnZXQgcXVhbnRpdHkoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLnF1YW50aXR5O1xuXHR9XG59XG5cblxuXG5cbmNsYXNzIEN1cnJlbmN5QW1vdW50RmFjdG9yeSBpbXBsZW1lbnRzIElDdXJyZW5jeUFtb3VudEZhY3Rvcnkge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRyZWFkb25seSBjb2luRGF0YVByb3ZpZGVyOiBJQ29pbkRhdGFQcm92aWRlclxuXHQpIHt9XG5cblx0YXN5bmMgbmV3QW1vdW50RnJvbVF1YW50aXR5KHF1YW50aXR5OiBzdHJpbmcpOiBQcm9taXNlPEN1cnJlbmN5QW1vdW50PiB7XG5cdFx0Y29uc3QgW2Ftb3VudCwgdG9rZW5TeW1ib2xdID0gcXVhbnRpdHkuc3BsaXQoJyAnKTtcblxuXHRcdHJldHVybiB0aGlzLm5ld0Ftb3VudEZyb21EZWNpbWFsKFxuXHRcdFx0YW1vdW50LFxuXHRcdFx0dG9rZW5TeW1ib2xcblx0XHQpO1xuXHR9XG5cblx0YXN5bmMgbmV3QW1vdW50RnJvbURlY2ltYWwoZGVjaW1hbEFtb3VudDogQmlnU291cmNlLCB0b2tlblN5bWJvbDogc3RyaW5nKTogUHJvbWlzZTxDdXJyZW5jeUFtb3VudD4ge1xuXHRcdGNvbnN0IGRhdGEgPSBhd2FpdCB0aGlzLmNvaW5EYXRhUHJvdmlkZXIuZ2V0Q29pbkRhdGFCeVN5bWJvbCh0b2tlblN5bWJvbCk7XG5cblx0XHRyZXR1cm4gbmV3IEN1cnJlbmN5QW1vdW50KFxuXHRcdFx0ZGVjaW1hbEFtb3VudCxcblx0XHRcdGRhdGFcblx0XHQpO1xuXHR9XG5cblx0YXN5bmMgbmV3QW1vdW50RnJvbURlY2ltYWxBbmRDb2luSWQoZGVjaW1hbEFtb3VudDogQmlnU291cmNlLCBjb2luSWQ6IENvaW5JZCk6IFByb21pc2U8Q3VycmVuY3lBbW91bnQ+IHtcblx0XHRjb25zdCBkYXRhID0gYXdhaXQgdGhpcy5jb2luRGF0YVByb3ZpZGVyLmdldENvaW5EYXRhKGNvaW5JZCk7XG5cblx0XHRyZXR1cm4gbmV3IEN1cnJlbmN5QW1vdW50KFxuXHRcdFx0ZGVjaW1hbEFtb3VudCxcblx0XHRcdGRhdGFcblx0XHQpO1xuXHR9XG5cblx0YXN5bmMgbmV3QW1vdW50RnJvbUludGVnZXIoaW50ZWdlclN1YnVuaXRzOiBCaWdTb3VyY2UsIHRva2VuU3ltYm9sOiBzdHJpbmcpOiBQcm9taXNlPEN1cnJlbmN5QW1vdW50PiB7XG5cdFx0Y29uc3QgZGF0YSA9IGF3YWl0IHRoaXMuY29pbkRhdGFQcm92aWRlci5nZXRDb2luRGF0YUJ5U3ltYm9sKHRva2VuU3ltYm9sKTtcblxuXHRcdGNvbnN0IGRlY2ltYWxBbW91bnQgPSBuZXcgQmlnKGludGVnZXJTdWJ1bml0cykuZGl2KFxuXHRcdFx0TWF0aC5wb3coMTAsIGRhdGEucHJlY2lzaW9uKVxuXHRcdCk7XG5cblx0XHRyZXR1cm4gbmV3IEN1cnJlbmN5QW1vdW50KFxuXHRcdFx0ZGVjaW1hbEFtb3VudCxcblx0XHRcdGRhdGFcblx0XHQpO1xuXHR9XG59XG5cbmxldCBjdXJyZW5jeUFtb3VudEZhY3Rvcnk6IEN1cnJlbmN5QW1vdW50RmFjdG9yeTtcblxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0Q3VycmVuY3lBbW91bnRGYWN0b3J5KFxuXHRkYXRhYmFzZTogSUNvaW5EYXRhYmFzZVNlcnZpY2Vcbik6IFByb21pc2U8Q3VycmVuY3lBbW91bnRGYWN0b3J5PiB7XG5cdGlmIChjdXJyZW5jeUFtb3VudEZhY3RvcnkgPT0gdW5kZWZpbmVkKSB7XG5cblx0XHRjb25zdCBjb2luRGF0YVByb3ZpZGVyID0gbmV3IENvaW5EYXRhUHJvdmlkZXIoXG5cdFx0XHRkYXRhYmFzZVxuXHRcdCk7XG5cblx0XHRhd2FpdCBjb2luRGF0YVByb3ZpZGVyLmluaXQoKTtcblxuXHRcdGN1cnJlbmN5QW1vdW50RmFjdG9yeSA9IG5ldyBDdXJyZW5jeUFtb3VudEZhY3RvcnkoXG5cdFx0XHRjb2luRGF0YVByb3ZpZGVyXG5cdFx0KTtcblx0fVxuXG5cdHJldHVybiBjdXJyZW5jeUFtb3VudEZhY3Rvcnk7XG59XG5cblxuXG5jbGFzcyBDdXJyZW5jeUFtb3VudFdpdGhQcmljZUZhY3RvcnkgaW1wbGVtZW50cyBJQ3VycmVuY3lBbW91bnRXaXRoUHJpY2VGYWN0b3J5IHtcblx0Y29uc3RydWN0b3IoXG5cdFx0cmVhZG9ubHkgY29pbkRhdGFQcm92aWRlcjogSUNvaW5EYXRhUHJvdmlkZXIsXG5cdFx0cmVhZG9ubHkgcHJpY2VTZXJ2aWNlOiBJQ3VycmVuY3lQcmljZVNlcnZpY2Vcblx0KSB7XG5cdH1cblxuXHRhc3luYyBuZXdBbW91bnRGcm9tUXVhbnRpdHkocXVhbnRpdHk6IHN0cmluZyk6IFByb21pc2U8Q3VycmVuY3lBbW91bnRXaXRoUHJpY2U+IHtcblx0XHRjb25zdCBbYW1vdW50LCB0b2tlblN5bWJvbF0gPSBxdWFudGl0eS5zcGxpdCgnICcpO1xuXG5cdFx0cmV0dXJuIHRoaXMubmV3QW1vdW50RnJvbURlY2ltYWwoXG5cdFx0XHRhbW91bnQsXG5cdFx0XHR0b2tlblN5bWJvbFxuXHRcdCk7XG5cdH1cblxuXHRhc3luYyBuZXdBbW91bnRGcm9tRGVjaW1hbChkZWNpbWFsQW1vdW50OiBCaWdTb3VyY2UsIHRva2VuU3ltYm9sOiBzdHJpbmcpOiBQcm9taXNlPEN1cnJlbmN5QW1vdW50V2l0aFByaWNlPiB7XG5cdFx0Y29uc3QgZGF0YSA9IGF3YWl0IHRoaXMuY29pbkRhdGFQcm92aWRlci5nZXRDb2luRGF0YUJ5U3ltYm9sKHRva2VuU3ltYm9sKTtcblxuXHRcdHJldHVybiBuZXcgQ3VycmVuY3lBbW91bnRXaXRoUHJpY2UoXG5cdFx0XHRkZWNpbWFsQW1vdW50LFxuXHRcdFx0ZGF0YSxcblx0XHRcdGF3YWl0IHRoaXMucHJpY2VTZXJ2aWNlLmdldFByaWNlSW5VU0QoZGF0YS5zeW1ib2wpXG5cdFx0KTtcblx0fVxuXG5cdGFzeW5jIG5ld0Ftb3VudEZyb21EZWNpbWFsQW5kQ29pbklkKGRlY2ltYWxBbW91bnQ6IEJpZ1NvdXJjZSwgY29pbklkOiBDb2luSWQpOiBQcm9taXNlPEN1cnJlbmN5QW1vdW50V2l0aFByaWNlPiB7XG5cdFx0Y29uc3QgZGF0YSA9IGF3YWl0IHRoaXMuY29pbkRhdGFQcm92aWRlci5nZXRDb2luRGF0YShjb2luSWQpO1xuXG5cdFx0cmV0dXJuIG5ldyBDdXJyZW5jeUFtb3VudFdpdGhQcmljZShcblx0XHRcdGRlY2ltYWxBbW91bnQsXG5cdFx0XHRkYXRhLFxuXHRcdFx0YXdhaXQgdGhpcy5wcmljZVNlcnZpY2UuZ2V0UHJpY2VJblVTRChkYXRhLnN5bWJvbClcblx0XHQpO1xuXHR9XG5cblx0YXN5bmMgbmV3QW1vdW50RnJvbUludGVnZXIoaW50ZWdlclN1YnVuaXRzOiBCaWdTb3VyY2UsIHRva2VuU3ltYm9sOiBzdHJpbmcpOiBQcm9taXNlPEN1cnJlbmN5QW1vdW50V2l0aFByaWNlPiB7XG5cdFx0Y29uc3QgZGF0YSA9IGF3YWl0IHRoaXMuY29pbkRhdGFQcm92aWRlci5nZXRDb2luRGF0YUJ5U3ltYm9sKHRva2VuU3ltYm9sKTtcblxuXHRcdGNvbnN0IGRlY2ltYWxBbW91bnQgPSBuZXcgQmlnKGludGVnZXJTdWJ1bml0cykuZGl2KFxuXHRcdFx0TWF0aC5wb3coMTAsIGRhdGEucHJlY2lzaW9uKVxuXHRcdCk7XG5cblx0XHRyZXR1cm4gbmV3IEN1cnJlbmN5QW1vdW50V2l0aFByaWNlKFxuXHRcdFx0ZGVjaW1hbEFtb3VudCxcblx0XHRcdGRhdGEsXG5cdFx0XHRhd2FpdCB0aGlzLnByaWNlU2VydmljZS5nZXRQcmljZUluVVNEKGRhdGEuc3ltYm9sKVxuXHRcdCk7XG5cdH1cbn1cblxuXG5sZXQgY3VycmVuY3lBbW91bnRXaXRoUHJpY2VGYWN0b3J5OiBDdXJyZW5jeUFtb3VudFdpdGhQcmljZUZhY3Rvcnk7XG5cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldEN1cnJlbmN5QW1vdW50V2l0aFByaWNlRmFjdG9yeShcblx0ZGF0YWJhc2U6IElDb2luRGF0YWJhc2VTZXJ2aWNlLFxuXHRwcmljZVNlcnZpY2U6IElDdXJyZW5jeVByaWNlU2VydmljZSA9IHVuZGVmaW5lZFxuKTogUHJvbWlzZTxDdXJyZW5jeUFtb3VudFdpdGhQcmljZUZhY3Rvcnk+IHtcblx0aWYgKGN1cnJlbmN5QW1vdW50V2l0aFByaWNlRmFjdG9yeSA9PSB1bmRlZmluZWQpIHtcblxuXHRcdGNvbnN0IGNvaW5EYXRhUHJvdmlkZXIgPSBuZXcgQ29pbkRhdGFQcm92aWRlcihcblx0XHRcdGRhdGFiYXNlXG5cdFx0KTtcblxuXHRcdGF3YWl0IGNvaW5EYXRhUHJvdmlkZXIuaW5pdCgpO1xuXG5cdFx0aWYgKHByaWNlU2VydmljZSA9PSB1bmRlZmluZWQpIHtcblx0XHRcdHByaWNlU2VydmljZSA9IGF3YWl0IGdldENvaW5QcmljZVNlcnZpY2UoZGF0YWJhc2UpO1xuXHRcdH1cblxuXHRcdGN1cnJlbmN5QW1vdW50V2l0aFByaWNlRmFjdG9yeSA9IG5ldyBDdXJyZW5jeUFtb3VudFdpdGhQcmljZUZhY3RvcnkoXG5cdFx0XHRjb2luRGF0YVByb3ZpZGVyLFxuXHRcdFx0cHJpY2VTZXJ2aWNlXG5cdFx0KTtcblx0fVxuXG5cdHJldHVybiBjdXJyZW5jeUFtb3VudFdpdGhQcmljZUZhY3Rvcnk7XG59XG4iXX0=