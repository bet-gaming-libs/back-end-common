/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/currency-amount/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ICurrency() { }
if (false) {
    /** @type {?} */
    ICurrency.prototype.id;
    /** @type {?} */
    ICurrency.prototype.symbol;
    /** @type {?} */
    ICurrency.prototype.precision;
}
/**
 * @record
 */
export function IPreciseNumber() { }
if (false) {
    /** @type {?} */
    IPreciseNumber.prototype.precision;
    /** @type {?} */
    IPreciseNumber.prototype.decimal;
    /** @type {?} */
    IPreciseNumber.prototype.integer;
    /** @type {?} */
    IPreciseNumber.prototype.isZero;
    /** @type {?} */
    IPreciseNumber.prototype.isPositive;
    /** @type {?} */
    IPreciseNumber.prototype.isNegative;
    /** @type {?} */
    IPreciseNumber.prototype.bigInteger;
    /** @type {?} */
    IPreciseNumber.prototype.factor;
    /**
     * @param {?} other
     * @return {?}
     */
    IPreciseNumber.prototype.isLessThan = function (other) { };
    /**
     * @param {?} other
     * @return {?}
     */
    IPreciseNumber.prototype.isLessThanDecimal = function (other) { };
    /**
     * @param {?} other
     * @return {?}
     */
    IPreciseNumber.prototype.isGreaterThan = function (other) { };
    /**
     * @param {?} other
     * @return {?}
     */
    IPreciseNumber.prototype.isGreaterThanDecimal = function (other) { };
    /**
     * @param {?} other
     * @return {?}
     */
    IPreciseNumber.prototype.isEqualTo = function (other) { };
    /**
     * @param {?} other
     * @return {?}
     */
    IPreciseNumber.prototype.isEqualToDecimal = function (other) { };
}
/**
 * @record
 * @template T
 */
export function IMatchingNumberTypeValidator() { }
if (false) {
    /**
     * @param {?} other
     * @return {?}
     */
    IMatchingNumberTypeValidator.prototype.isMatchingType = function (other) { };
}
/**
 * @record
 * @template T
 */
export function IPreciseNumberFactory() { }
if (false) {
    /** @type {?} */
    IPreciseNumberFactory.prototype.validator;
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    IPreciseNumberFactory.prototype.newAmountFromInteger = function (integer, precision) { };
    /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    IPreciseNumberFactory.prototype.newAmountFromDecimal = function (decimal, precision) { };
}
/**
 * @record
 * @template T
 */
export function IPreciseMath() { }
if (false) {
    /** @type {?} */
    IPreciseMath.prototype.startingValue;
    /** @type {?} */
    IPreciseMath.prototype.factory;
    /**
     * @param {?} decimal
     * @return {?}
     */
    IPreciseMath.prototype.addDecimal = function (decimal) { };
    /**
     * @param {?} amount
     * @return {?}
     */
    IPreciseMath.prototype.add = function (amount) { };
    /**
     * @param {?} decimal
     * @return {?}
     */
    IPreciseMath.prototype.subtractDecimal = function (decimal) { };
    /**
     * @param {?} amount
     * @return {?}
     */
    IPreciseMath.prototype.subtract = function (amount) { };
    /**
     * @param {?} decimal
     * @return {?}
     */
    IPreciseMath.prototype.multiplyDecimal = function (decimal) { };
    /**
     * @param {?} decimal
     * @return {?}
     */
    IPreciseMath.prototype.divideDecimal = function (decimal) { };
    /**
     * @return {?}
     */
    IPreciseMath.prototype.absoluteValue = function () { };
}
/**
 * @record
 */
export function IPreciseCurrencyAmount() { }
if (false) {
    /** @type {?} */
    IPreciseCurrencyAmount.prototype.currency;
    /** @type {?} */
    IPreciseCurrencyAmount.prototype.quantity;
}
/**
 * @record
 */
export function IPreciseCurrencyAmountWithPrice() { }
if (false) {
    /** @type {?} */
    IPreciseCurrencyAmountWithPrice.prototype.priceInUSD;
    /** @type {?} */
    IPreciseCurrencyAmountWithPrice.prototype.amountInUSD;
}
/**
 * @record
 */
export function ICurrencyAmount() { }
if (false) {
    /** @type {?} */
    ICurrencyAmount.prototype.math;
}
/**
 * @record
 */
export function ICurrencyAmountWithPrice() { }
if (false) {
    /** @type {?} */
    ICurrencyAmountWithPrice.prototype.math;
}
/**
 * @record
 */
export function ICurrencyAmountFactory() { }
if (false) {
    /** @type {?} */
    ICurrencyAmountFactory.prototype.coinDataProvider;
    /**
     * @param {?} quantity
     * @return {?}
     */
    ICurrencyAmountFactory.prototype.newAmountFromQuantity = function (quantity) { };
    /**
     * @param {?} decimalAmount
     * @param {?} tokenSymbol
     * @return {?}
     */
    ICurrencyAmountFactory.prototype.newAmountFromDecimal = function (decimalAmount, tokenSymbol) { };
    /**
     * @param {?} decimalAmount
     * @param {?} coinId
     * @return {?}
     */
    ICurrencyAmountFactory.prototype.newAmountFromDecimalAndCoinId = function (decimalAmount, coinId) { };
    /**
     * @param {?} integerSubunits
     * @param {?} tokenSymbol
     * @return {?}
     */
    ICurrencyAmountFactory.prototype.newAmountFromInteger = function (integerSubunits, tokenSymbol) { };
}
/**
 * @record
 */
export function ICurrencyAmountWithPriceFactory() { }
if (false) {
    /** @type {?} */
    ICurrencyAmountWithPriceFactory.prototype.priceService;
    /**
     * @param {?} quantity
     * @return {?}
     */
    ICurrencyAmountWithPriceFactory.prototype.newAmountFromQuantity = function (quantity) { };
    /**
     * @param {?} decimalAmount
     * @param {?} tokenSymbol
     * @return {?}
     */
    ICurrencyAmountWithPriceFactory.prototype.newAmountFromDecimal = function (decimalAmount, tokenSymbol) { };
    /**
     * @param {?} decimalAmount
     * @param {?} coinId
     * @return {?}
     */
    ICurrencyAmountWithPriceFactory.prototype.newAmountFromDecimalAndCoinId = function (decimalAmount, coinId) { };
    /**
     * @param {?} integerSubunits
     * @param {?} tokenSymbol
     * @return {?}
     */
    ICurrencyAmountWithPriceFactory.prototype.newAmountFromInteger = function (integerSubunits, tokenSymbol) { };
}
/**
 * @record
 */
export function ICurrencyPriceService() { }
if (false) {
    /**
     * @param {?} currencySymbol
     * @return {?}
     */
    ICurrencyPriceService.prototype.getPriceInUSD = function (currencySymbol) { };
}
/**
 * @record
 */
export function ICoinDatabaseService() { }
if (false) {
    /**
     * @return {?}
     */
    ICoinDatabaseService.prototype.getAllCoins = function () { };
}
/**
 * @record
 */
export function ICoinDataProvider() { }
if (false) {
    /**
     * @param {?} symbol
     * @return {?}
     */
    ICoinDataProvider.prototype.getCoinId = function (symbol) { };
    /**
     * @param {?} coinId
     * @return {?}
     */
    ICoinDataProvider.prototype.getCoinSymbol = function (coinId) { };
    /**
     * @param {?} coinId
     * @return {?}
     */
    ICoinDataProvider.prototype.getCoinData = function (coinId) { };
    /**
     * @param {?} symbol
     * @return {?}
     */
    ICoinDataProvider.prototype.getCoinDataBySymbol = function (symbol) { };
    /**
     * @return {?}
     */
    ICoinDataProvider.prototype.getAllCoins = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2N1cnJlbmN5L2N1cnJlbmN5LWFtb3VudC9pbnRlcmZhY2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBT0EsK0JBSUM7OztJQUhBLHVCQUFXOztJQUNYLDJCQUFlOztJQUNmLDhCQUFrQjs7Ozs7QUFJbkIsb0NBa0JDOzs7SUFqQkEsbUNBQTJCOztJQUMzQixpQ0FBeUI7O0lBQ3pCLGlDQUF5Qjs7SUFDekIsZ0NBQXlCOztJQUN6QixvQ0FBNkI7O0lBQzdCLG9DQUE2Qjs7SUFDN0Isb0NBQXlCOztJQUN6QixnQ0FBcUI7Ozs7O0lBRXJCLDJEQUEyQzs7Ozs7SUFDM0Msa0VBQTZDOzs7OztJQUU3Qyw4REFBOEM7Ozs7O0lBQzlDLHFFQUFnRDs7Ozs7SUFFaEQsMERBQTBDOzs7OztJQUMxQyxpRUFBNEM7Ozs7OztBQUs3QyxrREFFQzs7Ozs7O0lBREEsNkVBQWtDOzs7Ozs7QUFHbkMsMkNBTUM7OztJQUpBLDBDQUFvRDs7Ozs7O0lBRXBELHlGQUErRDs7Ozs7O0lBQy9ELHlGQUErRDs7Ozs7O0FBR2hFLGtDQVdDOzs7SUFWQSxxQ0FBMEI7O0lBQzFCLCtCQUEyQzs7Ozs7SUFFM0MsMkRBQStDOzs7OztJQUMvQyxtREFBK0I7Ozs7O0lBQy9CLGdFQUFvRDs7Ozs7SUFDcEQsd0RBQW9DOzs7OztJQUNwQyxnRUFBb0Q7Ozs7O0lBQ3BELDhEQUFrRDs7OztJQUNsRCx1REFBZ0M7Ozs7O0FBSWpDLDRDQUdDOzs7SUFGQSwwQ0FBNkI7O0lBQzdCLDBDQUEwQjs7Ozs7QUFHM0IscURBR0M7OztJQUZBLHFEQUE0Qjs7SUFDNUIsc0RBQTZCOzs7OztBQVM5QixxQ0FFQzs7O0lBREEsK0JBQXFDOzs7OztBQUd0Qyw4Q0FHQzs7O0lBREEsd0NBQThDOzs7OztBQUkvQyw0Q0FPQzs7O0lBTkEsa0RBQTZDOzs7OztJQUU3QyxpRkFBa0U7Ozs7OztJQUNsRSxrR0FBOEY7Ozs7OztJQUM5RixzR0FBa0c7Ozs7OztJQUNsRyxvR0FBZ0c7Ozs7O0FBR2pHLHFEQU9DOzs7SUFOQSx1REFBNkM7Ozs7O0lBRTdDLDBGQUEyRTs7Ozs7O0lBQzNFLDJHQUF1Rzs7Ozs7O0lBQ3ZHLCtHQUEyRzs7Ozs7O0lBQzNHLDZHQUF5Rzs7Ozs7QUFJMUcsMkNBR0M7Ozs7OztJQURBLDhFQUF1RDs7Ozs7QUFJeEQsMENBRUM7Ozs7O0lBREEsNkRBQXdDOzs7OztBQUd6Qyx1Q0FNQzs7Ozs7O0lBTEEsOERBQTJDOzs7OztJQUMzQyxrRUFBK0M7Ozs7O0lBQy9DLGdFQUEyQzs7Ozs7SUFDM0Msd0VBQW1EOzs7O0lBQ25ELDBEQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBCaWcsIHtCaWdTb3VyY2V9IGZyb20gJ2JpZy5qcyc7XG5cbmltcG9ydCB7IENvaW5JZCB9IGZyb20gJy4uL2RhdGFiYXNlL2NvaW5zJztcbmltcG9ydCB7IElTYXZlZENvaW5Sb3csIENvaW4gfSBmcm9tICcuLi9kYXRhYmFzZS9tb2RlbHMvY29pbic7XG5pbXBvcnQgeyBQcmVjaXNlTWF0aCB9IGZyb20gJy4vcHJlY2lzZS1tYXRoJztcblxuXG5leHBvcnQgaW50ZXJmYWNlIElDdXJyZW5jeSB7XG5cdGlkOiBudW1iZXI7XG5cdHN5bWJvbDogc3RyaW5nO1xuXHRwcmVjaXNpb246IG51bWJlcjtcbn1cblxuXG5leHBvcnQgaW50ZXJmYWNlIElQcmVjaXNlTnVtYmVyIHtcblx0cmVhZG9ubHkgcHJlY2lzaW9uOiBudW1iZXI7XG5cdHJlYWRvbmx5IGRlY2ltYWw6IHN0cmluZztcblx0cmVhZG9ubHkgaW50ZWdlcjogc3RyaW5nO1xuXHRyZWFkb25seSBpc1plcm86IGJvb2xlYW47XG5cdHJlYWRvbmx5IGlzUG9zaXRpdmU6IGJvb2xlYW47XG5cdHJlYWRvbmx5IGlzTmVnYXRpdmU6IGJvb2xlYW47XG5cdHJlYWRvbmx5IGJpZ0ludGVnZXI6IEJpZztcblx0cmVhZG9ubHkgZmFjdG9yOiBCaWc7XG5cblx0aXNMZXNzVGhhbihvdGhlcjogSVByZWNpc2VOdW1iZXIpOiBib29sZWFuO1xuXHRpc0xlc3NUaGFuRGVjaW1hbChvdGhlcjogQmlnU291cmNlKTogYm9vbGVhbjtcblxuXHRpc0dyZWF0ZXJUaGFuKG90aGVyOiBJUHJlY2lzZU51bWJlcik6IGJvb2xlYW47XG5cdGlzR3JlYXRlclRoYW5EZWNpbWFsKG90aGVyOiBCaWdTb3VyY2UpOiBib29sZWFuO1xuXG5cdGlzRXF1YWxUbyhvdGhlcjogSVByZWNpc2VOdW1iZXIpOiBib29sZWFuO1xuXHRpc0VxdWFsVG9EZWNpbWFsKG90aGVyOiBCaWdTb3VyY2UpOiBib29sZWFuO1xufVxuXG5cblxuZXhwb3J0IGludGVyZmFjZSBJTWF0Y2hpbmdOdW1iZXJUeXBlVmFsaWRhdG9yPFQgZXh0ZW5kcyBJUHJlY2lzZU51bWJlcj4ge1xuXHRpc01hdGNoaW5nVHlwZShvdGhlcjogVCk6IGJvb2xlYW47XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVByZWNpc2VOdW1iZXJGYWN0b3J5PFQgZXh0ZW5kcyBJUHJlY2lzZU51bWJlcj4ge1xuXHQvLyByZWFkb25seSBwcmVjaXNpb246bnVtYmVyO1xuXHRyZWFkb25seSB2YWxpZGF0b3I6IElNYXRjaGluZ051bWJlclR5cGVWYWxpZGF0b3I8VD47XG5cblx0bmV3QW1vdW50RnJvbUludGVnZXIoaW50ZWdlcjogQmlnU291cmNlLCBwcmVjaXNpb246IG51bWJlcik6IFQ7XG5cdG5ld0Ftb3VudEZyb21EZWNpbWFsKGRlY2ltYWw6IEJpZ1NvdXJjZSwgcHJlY2lzaW9uOiBudW1iZXIpOiBUO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElQcmVjaXNlTWF0aDxUIGV4dGVuZHMgSVByZWNpc2VOdW1iZXI+IGV4dGVuZHMgSVByZWNpc2VOdW1iZXIge1xuXHRyZWFkb25seSBzdGFydGluZ1ZhbHVlOiBUO1xuXHRyZWFkb25seSBmYWN0b3J5OiBJUHJlY2lzZU51bWJlckZhY3Rvcnk8VD47XG5cblx0YWRkRGVjaW1hbChkZWNpbWFsOiBCaWdTb3VyY2UpOiBQcmVjaXNlTWF0aDxUPjtcblx0YWRkKGFtb3VudDogVCk6IFByZWNpc2VNYXRoPFQ+O1xuXHRzdWJ0cmFjdERlY2ltYWwoZGVjaW1hbDogQmlnU291cmNlKTogUHJlY2lzZU1hdGg8VD47XG5cdHN1YnRyYWN0KGFtb3VudDogVCk6IFByZWNpc2VNYXRoPFQ+O1xuXHRtdWx0aXBseURlY2ltYWwoZGVjaW1hbDogQmlnU291cmNlKTogUHJlY2lzZU1hdGg8VD47XG5cdGRpdmlkZURlY2ltYWwoZGVjaW1hbDogQmlnU291cmNlKTogUHJlY2lzZU1hdGg8VD47XG5cdGFic29sdXRlVmFsdWUoKTogUHJlY2lzZU1hdGg8VD47XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJUHJlY2lzZUN1cnJlbmN5QW1vdW50IGV4dGVuZHMgSVByZWNpc2VOdW1iZXIge1xuXHRyZWFkb25seSBjdXJyZW5jeTogSUN1cnJlbmN5O1xuXHRyZWFkb25seSBxdWFudGl0eTogc3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElQcmVjaXNlQ3VycmVuY3lBbW91bnRXaXRoUHJpY2UgZXh0ZW5kcyBJUHJlY2lzZUN1cnJlbmN5QW1vdW50IHtcblx0cmVhZG9ubHkgcHJpY2VJblVTRDogbnVtYmVyO1xuXHRyZWFkb25seSBhbW91bnRJblVTRDogc3RyaW5nO1xufVxuXG5cbmV4cG9ydCBkZWNsYXJlIHR5cGUgUHJlY2lzZU51bWJlckZvck1hdGggPSBQcmVjaXNlTWF0aDxJUHJlY2lzZU51bWJlcj47XG5leHBvcnQgZGVjbGFyZSB0eXBlIEN1cnJlbmN5QW1vdW50Rm9yTWF0aCA9IFByZWNpc2VNYXRoPElQcmVjaXNlQ3VycmVuY3lBbW91bnQ+O1xuZXhwb3J0IGRlY2xhcmUgdHlwZSBDdXJyZW5jeUFtb3VudFdpdGhQcmljZUZvck1hdGggPSBQcmVjaXNlTWF0aDxJUHJlY2lzZUN1cnJlbmN5QW1vdW50V2l0aFByaWNlPjtcblxuXG5leHBvcnQgaW50ZXJmYWNlIElDdXJyZW5jeUFtb3VudCBleHRlbmRzIElQcmVjaXNlQ3VycmVuY3lBbW91bnQsIElQcmVjaXNlTWF0aDxJUHJlY2lzZUN1cnJlbmN5QW1vdW50PiB7XG5cdHJlYWRvbmx5IG1hdGg6IEN1cnJlbmN5QW1vdW50Rm9yTWF0aDtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQ3VycmVuY3lBbW91bnRXaXRoUHJpY2Vcblx0ZXh0ZW5kcyBJUHJlY2lzZUN1cnJlbmN5QW1vdW50V2l0aFByaWNlLCBJUHJlY2lzZU1hdGg8SVByZWNpc2VDdXJyZW5jeUFtb3VudFdpdGhQcmljZT4ge1xuXHRyZWFkb25seSBtYXRoOiBDdXJyZW5jeUFtb3VudFdpdGhQcmljZUZvck1hdGg7XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQ3VycmVuY3lBbW91bnRGYWN0b3J5IHtcblx0cmVhZG9ubHkgY29pbkRhdGFQcm92aWRlcjogSUNvaW5EYXRhUHJvdmlkZXI7XG5cblx0bmV3QW1vdW50RnJvbVF1YW50aXR5KHF1YW50aXR5OiBzdHJpbmcpOiBQcm9taXNlPElDdXJyZW5jeUFtb3VudD47XG5cdG5ld0Ftb3VudEZyb21EZWNpbWFsKGRlY2ltYWxBbW91bnQ6IEJpZ1NvdXJjZSwgdG9rZW5TeW1ib2w6IHN0cmluZyk6IFByb21pc2U8SUN1cnJlbmN5QW1vdW50Pjtcblx0bmV3QW1vdW50RnJvbURlY2ltYWxBbmRDb2luSWQoZGVjaW1hbEFtb3VudDogQmlnU291cmNlLCBjb2luSWQ6IENvaW5JZCk6IFByb21pc2U8SUN1cnJlbmN5QW1vdW50Pjtcblx0bmV3QW1vdW50RnJvbUludGVnZXIoaW50ZWdlclN1YnVuaXRzOiBCaWdTb3VyY2UsIHRva2VuU3ltYm9sOiBzdHJpbmcpOiBQcm9taXNlPElDdXJyZW5jeUFtb3VudD47XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUN1cnJlbmN5QW1vdW50V2l0aFByaWNlRmFjdG9yeSBleHRlbmRzIElDdXJyZW5jeUFtb3VudEZhY3Rvcnkge1xuXHRyZWFkb25seSBwcmljZVNlcnZpY2U6IElDdXJyZW5jeVByaWNlU2VydmljZTtcblxuXHRuZXdBbW91bnRGcm9tUXVhbnRpdHkocXVhbnRpdHk6IHN0cmluZyk6IFByb21pc2U8SUN1cnJlbmN5QW1vdW50V2l0aFByaWNlPjtcblx0bmV3QW1vdW50RnJvbURlY2ltYWwoZGVjaW1hbEFtb3VudDogQmlnU291cmNlLCB0b2tlblN5bWJvbDogc3RyaW5nKTogUHJvbWlzZTxJQ3VycmVuY3lBbW91bnRXaXRoUHJpY2U+O1xuXHRuZXdBbW91bnRGcm9tRGVjaW1hbEFuZENvaW5JZChkZWNpbWFsQW1vdW50OiBCaWdTb3VyY2UsIGNvaW5JZDogQ29pbklkKTogUHJvbWlzZTxJQ3VycmVuY3lBbW91bnRXaXRoUHJpY2U+O1xuXHRuZXdBbW91bnRGcm9tSW50ZWdlcihpbnRlZ2VyU3VidW5pdHM6IEJpZ1NvdXJjZSwgdG9rZW5TeW1ib2w6IHN0cmluZyk6IFByb21pc2U8SUN1cnJlbmN5QW1vdW50V2l0aFByaWNlPjtcbn1cblxuXG5leHBvcnQgaW50ZXJmYWNlIElDdXJyZW5jeVByaWNlU2VydmljZSB7XG5cdC8vIGluaXQoKTpQcm9taXNlPHZvaWQ+O1xuXHRnZXRQcmljZUluVVNEKGN1cnJlbmN5U3ltYm9sOiBzdHJpbmcpOiBQcm9taXNlPG51bWJlcj47XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQ29pbkRhdGFiYXNlU2VydmljZSB7XG5cdGdldEFsbENvaW5zKCk6IFByb21pc2U8SVNhdmVkQ29pblJvd1tdPjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQ29pbkRhdGFQcm92aWRlciB7XG5cdGdldENvaW5JZChzeW1ib2w6IHN0cmluZyk6IFByb21pc2U8Q29pbklkPjtcblx0Z2V0Q29pblN5bWJvbChjb2luSWQ6IENvaW5JZCk6IFByb21pc2U8c3RyaW5nPjtcblx0Z2V0Q29pbkRhdGEoY29pbklkOiBDb2luSWQpOiBQcm9taXNlPENvaW4+O1xuXHRnZXRDb2luRGF0YUJ5U3ltYm9sKHN5bWJvbDogc3RyaW5nKTogUHJvbWlzZTxDb2luPjtcblx0Z2V0QWxsQ29pbnMoKTogQ29pbltdO1xufVxuIl19