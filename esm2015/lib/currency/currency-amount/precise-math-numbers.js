/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/currency-amount/precise-math-numbers.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Big } from 'big.js';
import { PreciseMath } from './precise-math';
export class PreciseNumber {
    /**
     * @param {?} precision
     * @param {?=} integerValue
     */
    constructor(precision, integerValue = 0) {
        this.precision = precision;
        this.factor = new Big(Math.pow(10, precision));
        this.bigInteger = new Big(integerValue);
        Big.RM = 0 /* RoundDown */;
        this.decimal = this.bigInteger.div(this.factor)
            .toFixed(precision);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isLessThan(other) {
        this.validateMatchingCurrency(other);
        return this.bigInteger.lt(other.integer);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isLessThanDecimal(other) {
        return new Big(this.decimal).lt(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isGreaterThan(other) {
        this.validateMatchingCurrency(other);
        return this.bigInteger.gt(other.integer);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isGreaterThanDecimal(other) {
        return new Big(this.decimal).gt(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isEqualTo(other) {
        this.validateMatchingCurrency(other);
        return this.bigInteger.eq(other.integer);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isEqualToDecimal(other) {
        return new Big(this.decimal).eq(other);
    }
    /**
     * @protected
     * @param {?} other
     * @return {?}
     */
    validateMatchingCurrency(other) {
        if (this.precision != other.precision) {
            throw new Error('both amounts must be the same precision!');
        }
    }
    /**
     * @return {?}
     */
    get integer() {
        return this.bigInteger.toFixed(0);
    }
    /**
     * @return {?}
     */
    get isZero() {
        return this.bigInteger.eq(0);
    }
    /**
     * @return {?}
     */
    get isPositive() {
        return this.bigInteger.gt(0);
    }
    /**
     * @return {?}
     */
    get isNegative() {
        return this.bigInteger.lt(0);
    }
}
if (false) {
    /** @type {?} */
    PreciseNumber.prototype.factor;
    /** @type {?} */
    PreciseNumber.prototype.bigInteger;
    /** @type {?} */
    PreciseNumber.prototype.decimal;
    /** @type {?} */
    PreciseNumber.prototype.precision;
}
export class PreciseDecimal extends PreciseNumber {
    /**
     * @param {?} decimalValue
     * @param {?} precision
     */
    constructor(decimalValue, precision) {
        /** @type {?} */
        const factor = new Big(Math.pow(10, precision));
        /** @type {?} */
        const decimal = new Big(decimalValue);
        super(precision, decimal
            .times(factor)
            .round(0, 0 /* RoundDown */));
    }
}
class MatchingPrecisionValidator {
    /**
     * @param {?} precision
     */
    constructor(precision) {
        this.precision = precision;
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isMatchingType(other) {
        if (this.precision != other.precision) {
            throw new Error('both amounts must be the same precision!');
        }
        else {
            return true;
        }
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MatchingPrecisionValidator.prototype.precision;
}
class PreciseNumberFactory {
    /**
     * @param {?} precision
     */
    constructor(precision) {
        this.validator = new MatchingPrecisionValidator(precision);
    }
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    newAmountFromInteger(integer, precision) {
        /** @type {?} */
        const number = new PreciseNumber(precision, integer);
        this.validator.isMatchingType(number);
        return number;
    }
    /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    newAmountFromDecimal(decimal, precision) {
        /** @type {?} */
        const number = new PreciseDecimal(decimal, precision);
        this.validator.isMatchingType(number);
        return number;
    }
}
if (false) {
    /** @type {?} */
    PreciseNumberFactory.prototype.validator;
}
/**
 * @template T
 */
export class NumberForPreciseMathBase {
    /**
     * @param {?} precision
     * @param {?} integerValue
     * @param {?} factory
     */
    constructor(precision, integerValue, factory) {
        this.factory = factory;
        /** @type {?} */
        const number = factory.newAmountFromInteger(integerValue, precision);
        this.math = new PreciseMath(number, factory);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    addDecimal(decimal) {
        return this.math.addDecimal(decimal);
    }
    /**
     * @param {?} amount
     * @return {?}
     */
    add(amount) {
        return this.math.add(amount);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    subtractDecimal(decimal) {
        return this.math.subtractDecimal(decimal);
    }
    /**
     * @param {?} amount
     * @return {?}
     */
    subtract(amount) {
        return this.math.subtract(amount);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    multiplyDecimal(decimal) {
        return this.math.multiplyDecimal(decimal);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    divideDecimal(decimal) {
        return this.math.divideDecimal(decimal);
    }
    /**
     * @return {?}
     */
    absoluteValue() {
        return this.math.absoluteValue();
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isLessThan(other) {
        return this.number.isLessThan(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isLessThanDecimal(other) {
        return this.number.isLessThanDecimal(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isGreaterThan(other) {
        return this.number.isGreaterThan(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isGreaterThanDecimal(other) {
        return this.number.isGreaterThanDecimal(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isEqualTo(other) {
        return this.number.isEqualTo(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isEqualToDecimal(other) {
        return this.number.isEqualToDecimal(other);
    }
    /**
     * @return {?}
     */
    get decimal() {
        return this.number.decimal;
    }
    /**
     * @return {?}
     */
    get integer() {
        return this.number.integer;
    }
    /**
     * @return {?}
     */
    get isZero() {
        return this.number.isZero;
    }
    /**
     * @return {?}
     */
    get isPositive() {
        return this.number.isPositive;
    }
    /**
     * @return {?}
     */
    get isNegative() {
        return this.number.isNegative;
    }
    /**
     * @return {?}
     */
    get bigInteger() {
        return this.number.bigInteger;
    }
    /**
     * @return {?}
     */
    get precision() {
        return this.number.precision;
    }
    /**
     * @return {?}
     */
    get factor() {
        return this.number.factor;
    }
    /**
     * @return {?}
     */
    get number() {
        return this.startingValue;
    }
    /**
     * @return {?}
     */
    get startingValue() {
        return this.math.startingValue;
    }
}
if (false) {
    /** @type {?} */
    NumberForPreciseMathBase.prototype.math;
    /** @type {?} */
    NumberForPreciseMathBase.prototype.factory;
}
export class NumberForPreciseMath extends NumberForPreciseMathBase {
    /**
     * @param {?} precision
     * @param {?=} integerValue
     */
    constructor(precision, integerValue = 0) {
        super(precision, integerValue, new PreciseNumberFactory(precision));
    }
}
export class DecimalForPreciseMath extends NumberForPreciseMath {
    /**
     * @param {?} decimalValue
     * @param {?} precision
     */
    constructor(decimalValue, precision) {
        /** @type {?} */
        const factor = new Big(Math.pow(10, precision));
        /** @type {?} */
        const decimal = new Big(decimalValue);
        super(precision, decimal
            .times(factor)
            .round(0, 0 /* RoundDown */));
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJlY2lzZS1tYXRoLW51bWJlcnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9jdXJyZW5jeS9jdXJyZW5jeS1hbW91bnQvcHJlY2lzZS1tYXRoLW51bWJlcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsR0FBRyxFQUEyQixNQUFNLFFBQVEsQ0FBQztBQUV0RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHN0MsTUFBTSxPQUFPLGFBQWE7Ozs7O0lBS3pCLFlBQ1UsU0FBaUIsRUFDMUIsZUFBMEIsQ0FBQztRQURsQixjQUFTLEdBQVQsU0FBUyxDQUFRO1FBRzFCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLENBQ3BCLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLFNBQVMsQ0FBQyxDQUN2QixDQUFDO1FBRUYsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUV4QyxHQUFHLENBQUMsRUFBRSxvQkFBeUIsQ0FBQztRQUNoQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7YUFDN0MsT0FBTyxDQUNQLFNBQVMsQ0FDVCxDQUFDO0lBQ0osQ0FBQzs7Ozs7SUFHRCxVQUFVLENBQUMsS0FBcUI7UUFDL0IsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXJDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7O0lBQ0QsaUJBQWlCLENBQUMsS0FBZ0I7UUFDakMsT0FBTyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3hDLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLEtBQXFCO1FBQ2xDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVyQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMxQyxDQUFDOzs7OztJQUNELG9CQUFvQixDQUFDLEtBQWdCO1FBQ3BDLE9BQU8sSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN4QyxDQUFDOzs7OztJQUVELFNBQVMsQ0FBQyxLQUFxQjtRQUM5QixJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFckMsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Ozs7SUFDRCxnQkFBZ0IsQ0FBQyxLQUFnQjtRQUNoQyxPQUFPLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDeEMsQ0FBQzs7Ozs7O0lBRVMsd0JBQXdCLENBQUMsS0FBcUI7UUFDdkQsSUFDQyxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQ2hDO1lBQ0QsTUFBTSxJQUFJLEtBQUssQ0FBQywwQ0FBMEMsQ0FBQyxDQUFDO1NBQzVEO0lBQ0YsQ0FBQzs7OztJQUdELElBQUksT0FBTztRQUNWLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7OztJQUVELElBQUksTUFBTTtRQUNULE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDOUIsQ0FBQzs7OztJQUVELElBQUksVUFBVTtRQUNiLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDOUIsQ0FBQzs7OztJQUVELElBQUksVUFBVTtRQUNiLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDOUIsQ0FBQztDQUNEOzs7SUF6RUEsK0JBQXFCOztJQUNyQixtQ0FBeUI7O0lBQ3pCLGdDQUF5Qjs7SUFHeEIsa0NBQTBCOztBQXVFNUIsTUFBTSxPQUFPLGNBQWUsU0FBUSxhQUFhOzs7OztJQUNoRCxZQUNDLFlBQXVCLEVBQ3ZCLFNBQWlCOztjQUVYLE1BQU0sR0FBRyxJQUFJLEdBQUcsQ0FBRSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsQ0FBRTs7Y0FFM0MsT0FBTyxHQUFHLElBQUksR0FBRyxDQUFDLFlBQVksQ0FBQztRQUVyQyxLQUFLLENBQ0osU0FBUyxFQUNULE9BQU87YUFDTCxLQUFLLENBQUMsTUFBTSxDQUFDO2FBQ2IsS0FBSyxDQUFDLENBQUMsb0JBQXlCLENBQ2xDLENBQUM7SUFDSCxDQUFDO0NBQ0Q7QUFHRCxNQUFNLDBCQUEwQjs7OztJQUMvQixZQUE2QixTQUFpQjtRQUFqQixjQUFTLEdBQVQsU0FBUyxDQUFRO0lBQzlDLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLEtBQXFCO1FBQ25DLElBQ0MsSUFBSSxDQUFDLFNBQVMsSUFBSSxLQUFLLENBQUMsU0FBUyxFQUNoQztZQUNELE1BQU0sSUFBSSxLQUFLLENBQUMsMENBQTBDLENBQUMsQ0FBQztTQUM1RDthQUFNO1lBQ04sT0FBTyxJQUFJLENBQUM7U0FDWjtJQUNGLENBQUM7Q0FDRDs7Ozs7O0lBWlksK0NBQWtDOztBQWMvQyxNQUFNLG9CQUFvQjs7OztJQUd6QixZQUFZLFNBQWlCO1FBQzVCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM1RCxDQUFDOzs7Ozs7SUFFRCxvQkFBb0IsQ0FBQyxPQUFrQixFQUFFLFNBQWlCOztjQUNuRCxNQUFNLEdBQUcsSUFBSSxhQUFhLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQztRQUVwRCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUV0QyxPQUFPLE1BQU0sQ0FBQztJQUNmLENBQUM7Ozs7OztJQUVELG9CQUFvQixDQUFDLE9BQWtCLEVBQUUsU0FBaUI7O2NBQ25ELE1BQU0sR0FBRyxJQUFJLGNBQWMsQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDO1FBRXJELElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXRDLE9BQU8sTUFBTSxDQUFDO0lBQ2YsQ0FBQztDQUNEOzs7SUFyQkEseUNBQWlFOzs7OztBQXdCbEUsTUFBTSxPQUFPLHdCQUF3Qjs7Ozs7O0lBSXBDLFlBQ0MsU0FBaUIsRUFDakIsWUFBdUIsRUFDZCxPQUFpQztRQUFqQyxZQUFPLEdBQVAsT0FBTyxDQUEwQjs7Y0FFcEMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxvQkFBb0IsQ0FDMUMsWUFBWSxFQUFFLFNBQVMsQ0FDdkI7UUFFRCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksV0FBVyxDQUMxQixNQUFNLEVBQ04sT0FBTyxDQUNQLENBQUM7SUFDSCxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxPQUFrQjtRQUM1QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3RDLENBQUM7Ozs7O0lBQ0QsR0FBRyxDQUFDLE1BQVM7UUFDWixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzlCLENBQUM7Ozs7O0lBRUQsZUFBZSxDQUFDLE9BQWtCO1FBQ2pDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDM0MsQ0FBQzs7Ozs7SUFDRCxRQUFRLENBQUMsTUFBUztRQUNqQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsZUFBZSxDQUFDLE9BQWtCO1FBQ2pDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDM0MsQ0FBQzs7Ozs7SUFDRCxhQUFhLENBQUMsT0FBa0I7UUFDL0IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7O0lBRUQsYUFBYTtRQUNaLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUNsQyxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxLQUFRO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7Ozs7SUFDRCxpQkFBaUIsQ0FBQyxLQUFnQjtRQUNqQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDN0MsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsS0FBUTtRQUNyQixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pDLENBQUM7Ozs7O0lBQ0Qsb0JBQW9CLENBQUMsS0FBZ0I7UUFDcEMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hELENBQUM7Ozs7O0lBRUQsU0FBUyxDQUFDLEtBQVE7UUFDakIsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNyQyxDQUFDOzs7OztJQUNELGdCQUFnQixDQUFDLEtBQWdCO1FBQ2hDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM1QyxDQUFDOzs7O0lBR0QsSUFBSSxPQUFPO1FBQ1YsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsSUFBSSxPQUFPO1FBQ1YsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsSUFBSSxNQUFNO1FBQ1QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUMzQixDQUFDOzs7O0lBQ0QsSUFBSSxVQUFVO1FBQ2IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztJQUMvQixDQUFDOzs7O0lBQ0QsSUFBSSxVQUFVO1FBQ2IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztJQUMvQixDQUFDOzs7O0lBRUQsSUFBSSxVQUFVO1FBQ2IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztJQUMvQixDQUFDOzs7O0lBRUQsSUFBSSxTQUFTO1FBQ1osT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztJQUM5QixDQUFDOzs7O0lBRUQsSUFBSSxNQUFNO1FBQ1QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUMzQixDQUFDOzs7O0lBRUQsSUFBSSxNQUFNO1FBQ1QsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzNCLENBQUM7Ozs7SUFFRCxJQUFJLGFBQWE7UUFDaEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUNoQyxDQUFDO0NBQ0Q7OztJQXJHQSx3Q0FBOEI7O0lBSzdCLDJDQUEwQzs7QUFrRzVDLE1BQU0sT0FBTyxvQkFBcUIsU0FBUSx3QkFBd0M7Ozs7O0lBQ2pGLFlBQ0MsU0FBaUIsRUFDakIsZUFBMEIsQ0FBQztRQUUzQixLQUFLLENBQ0osU0FBUyxFQUNULFlBQVksRUFDWixJQUFJLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxDQUNuQyxDQUFDO0lBQ0gsQ0FBQztDQUNEO0FBR0QsTUFBTSxPQUFPLHFCQUFzQixTQUFRLG9CQUFvQjs7Ozs7SUFDOUQsWUFDQyxZQUF1QixFQUN2QixTQUFpQjs7Y0FFWCxNQUFNLEdBQUcsSUFBSSxHQUFHLENBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsU0FBUyxDQUFDLENBQUU7O2NBRTNDLE9BQU8sR0FBRyxJQUFJLEdBQUcsQ0FBQyxZQUFZLENBQUM7UUFFckMsS0FBSyxDQUNKLFNBQVMsRUFDVCxPQUFPO2FBQ0wsS0FBSyxDQUFDLE1BQU0sQ0FBQzthQUNiLEtBQUssQ0FBQyxDQUFDLG9CQUF5QixDQUNsQyxDQUFDO0lBQ0gsQ0FBQztDQUNEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQmlnLCBSb3VuZGluZ01vZGUsIEJpZ1NvdXJjZSB9IGZyb20gJ2JpZy5qcyc7XG5pbXBvcnQgeyBJUHJlY2lzZU51bWJlciwgSVByZWNpc2VNYXRoLCBJUHJlY2lzZU51bWJlckZhY3RvcnksIElNYXRjaGluZ051bWJlclR5cGVWYWxpZGF0b3IgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgUHJlY2lzZU1hdGggfSBmcm9tICcuL3ByZWNpc2UtbWF0aCc7XG5cblxuZXhwb3J0IGNsYXNzIFByZWNpc2VOdW1iZXIgaW1wbGVtZW50cyBJUHJlY2lzZU51bWJlciB7XG5cdHJlYWRvbmx5IGZhY3RvcjogQmlnO1xuXHRyZWFkb25seSBiaWdJbnRlZ2VyOiBCaWc7XG5cdHJlYWRvbmx5IGRlY2ltYWw6IHN0cmluZztcblxuXHRjb25zdHJ1Y3Rvcihcblx0XHRyZWFkb25seSBwcmVjaXNpb246IG51bWJlcixcblx0XHRpbnRlZ2VyVmFsdWU6IEJpZ1NvdXJjZSA9IDAsXG5cdCkge1xuXHRcdHRoaXMuZmFjdG9yID0gbmV3IEJpZyhcblx0XHRcdE1hdGgucG93KDEwLCBwcmVjaXNpb24pXG5cdFx0KTtcblxuXHRcdHRoaXMuYmlnSW50ZWdlciA9IG5ldyBCaWcoaW50ZWdlclZhbHVlKTtcblxuXHRcdEJpZy5STSA9IFJvdW5kaW5nTW9kZS5Sb3VuZERvd247XG5cdFx0dGhpcy5kZWNpbWFsID0gdGhpcy5iaWdJbnRlZ2VyLmRpdih0aGlzLmZhY3Rvcilcblx0XHRcdC50b0ZpeGVkKFxuXHRcdFx0XHRwcmVjaXNpb25cblx0XHRcdCk7XG5cdH1cblxuXG5cdGlzTGVzc1RoYW4ob3RoZXI6IElQcmVjaXNlTnVtYmVyKSB7XG5cdFx0dGhpcy52YWxpZGF0ZU1hdGNoaW5nQ3VycmVuY3kob3RoZXIpO1xuXG5cdFx0cmV0dXJuIHRoaXMuYmlnSW50ZWdlci5sdChvdGhlci5pbnRlZ2VyKTtcblx0fVxuXHRpc0xlc3NUaGFuRGVjaW1hbChvdGhlcjogQmlnU291cmNlKSB7XG5cdFx0cmV0dXJuIG5ldyBCaWcodGhpcy5kZWNpbWFsKS5sdChvdGhlcik7XG5cdH1cblxuXHRpc0dyZWF0ZXJUaGFuKG90aGVyOiBJUHJlY2lzZU51bWJlcikge1xuXHRcdHRoaXMudmFsaWRhdGVNYXRjaGluZ0N1cnJlbmN5KG90aGVyKTtcblxuXHRcdHJldHVybiB0aGlzLmJpZ0ludGVnZXIuZ3Qob3RoZXIuaW50ZWdlcik7XG5cdH1cblx0aXNHcmVhdGVyVGhhbkRlY2ltYWwob3RoZXI6IEJpZ1NvdXJjZSkge1xuXHRcdHJldHVybiBuZXcgQmlnKHRoaXMuZGVjaW1hbCkuZ3Qob3RoZXIpO1xuXHR9XG5cblx0aXNFcXVhbFRvKG90aGVyOiBJUHJlY2lzZU51bWJlcikge1xuXHRcdHRoaXMudmFsaWRhdGVNYXRjaGluZ0N1cnJlbmN5KG90aGVyKTtcblxuXHRcdHJldHVybiB0aGlzLmJpZ0ludGVnZXIuZXEob3RoZXIuaW50ZWdlcik7XG5cdH1cblx0aXNFcXVhbFRvRGVjaW1hbChvdGhlcjogQmlnU291cmNlKSB7XG5cdFx0cmV0dXJuIG5ldyBCaWcodGhpcy5kZWNpbWFsKS5lcShvdGhlcik7XG5cdH1cblxuXHRwcm90ZWN0ZWQgdmFsaWRhdGVNYXRjaGluZ0N1cnJlbmN5KG90aGVyOiBJUHJlY2lzZU51bWJlcikge1xuXHRcdGlmIChcblx0XHRcdHRoaXMucHJlY2lzaW9uICE9IG90aGVyLnByZWNpc2lvblxuXHRcdCkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdib3RoIGFtb3VudHMgbXVzdCBiZSB0aGUgc2FtZSBwcmVjaXNpb24hJyk7XG5cdFx0fVxuXHR9XG5cblxuXHRnZXQgaW50ZWdlcigpIHtcblx0XHRyZXR1cm4gdGhpcy5iaWdJbnRlZ2VyLnRvRml4ZWQoMCk7XG5cdH1cblxuXHRnZXQgaXNaZXJvKCkge1xuXHRcdHJldHVybiB0aGlzLmJpZ0ludGVnZXIuZXEoMCk7XG5cdH1cblxuXHRnZXQgaXNQb3NpdGl2ZSgpIHtcblx0XHRyZXR1cm4gdGhpcy5iaWdJbnRlZ2VyLmd0KDApO1xuXHR9XG5cblx0Z2V0IGlzTmVnYXRpdmUoKSB7XG5cdFx0cmV0dXJuIHRoaXMuYmlnSW50ZWdlci5sdCgwKTtcblx0fVxufVxuXG5cbmV4cG9ydCBjbGFzcyBQcmVjaXNlRGVjaW1hbCBleHRlbmRzIFByZWNpc2VOdW1iZXIge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRkZWNpbWFsVmFsdWU6IEJpZ1NvdXJjZSxcblx0XHRwcmVjaXNpb246IG51bWJlclxuXHQpIHtcblx0XHRjb25zdCBmYWN0b3IgPSBuZXcgQmlnKCBNYXRoLnBvdygxMCwgcHJlY2lzaW9uKSApO1xuXG5cdFx0Y29uc3QgZGVjaW1hbCA9IG5ldyBCaWcoZGVjaW1hbFZhbHVlKTtcblxuXHRcdHN1cGVyKFxuXHRcdFx0cHJlY2lzaW9uLFxuXHRcdFx0ZGVjaW1hbFxuXHRcdFx0XHQudGltZXMoZmFjdG9yKVxuXHRcdFx0XHQucm91bmQoMCwgUm91bmRpbmdNb2RlLlJvdW5kRG93bilcblx0XHQpO1xuXHR9XG59XG5cblxuY2xhc3MgTWF0Y2hpbmdQcmVjaXNpb25WYWxpZGF0b3IgaW1wbGVtZW50cyBJTWF0Y2hpbmdOdW1iZXJUeXBlVmFsaWRhdG9yPElQcmVjaXNlTnVtYmVyPiB7XG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcmVhZG9ubHkgcHJlY2lzaW9uOiBudW1iZXIpIHtcblx0fVxuXG5cdGlzTWF0Y2hpbmdUeXBlKG90aGVyOiBJUHJlY2lzZU51bWJlcik6IGJvb2xlYW4ge1xuXHRcdGlmIChcblx0XHRcdHRoaXMucHJlY2lzaW9uICE9IG90aGVyLnByZWNpc2lvblxuXHRcdCkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdib3RoIGFtb3VudHMgbXVzdCBiZSB0aGUgc2FtZSBwcmVjaXNpb24hJyk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH1cblx0fVxufVxuXG5jbGFzcyBQcmVjaXNlTnVtYmVyRmFjdG9yeSBpbXBsZW1lbnRzIElQcmVjaXNlTnVtYmVyRmFjdG9yeTxJUHJlY2lzZU51bWJlcj4ge1xuXHRyZWFkb25seSB2YWxpZGF0b3I6IElNYXRjaGluZ051bWJlclR5cGVWYWxpZGF0b3I8SVByZWNpc2VOdW1iZXI+O1xuXG5cdGNvbnN0cnVjdG9yKHByZWNpc2lvbjogbnVtYmVyKSB7XG5cdFx0dGhpcy52YWxpZGF0b3IgPSBuZXcgTWF0Y2hpbmdQcmVjaXNpb25WYWxpZGF0b3IocHJlY2lzaW9uKTtcblx0fVxuXG5cdG5ld0Ftb3VudEZyb21JbnRlZ2VyKGludGVnZXI6IEJpZ1NvdXJjZSwgcHJlY2lzaW9uOiBudW1iZXIpOiBJUHJlY2lzZU51bWJlciB7XG5cdFx0Y29uc3QgbnVtYmVyID0gbmV3IFByZWNpc2VOdW1iZXIocHJlY2lzaW9uLCBpbnRlZ2VyKTtcblxuXHRcdHRoaXMudmFsaWRhdG9yLmlzTWF0Y2hpbmdUeXBlKG51bWJlcik7XG5cblx0XHRyZXR1cm4gbnVtYmVyO1xuXHR9XG5cblx0bmV3QW1vdW50RnJvbURlY2ltYWwoZGVjaW1hbDogQmlnU291cmNlLCBwcmVjaXNpb246IG51bWJlcik6IElQcmVjaXNlTnVtYmVyIHtcblx0XHRjb25zdCBudW1iZXIgPSBuZXcgUHJlY2lzZURlY2ltYWwoZGVjaW1hbCwgcHJlY2lzaW9uKTtcblxuXHRcdHRoaXMudmFsaWRhdG9yLmlzTWF0Y2hpbmdUeXBlKG51bWJlcik7XG5cblx0XHRyZXR1cm4gbnVtYmVyO1xuXHR9XG59XG5cblxuZXhwb3J0IGNsYXNzIE51bWJlckZvclByZWNpc2VNYXRoQmFzZTxUIGV4dGVuZHMgSVByZWNpc2VOdW1iZXI+XG5cdGltcGxlbWVudHMgSVByZWNpc2VOdW1iZXIsIElQcmVjaXNlTWF0aDxUPiB7XG5cdHJlYWRvbmx5IG1hdGg6IFByZWNpc2VNYXRoPFQ+O1xuXG5cdGNvbnN0cnVjdG9yKFxuXHRcdHByZWNpc2lvbjogbnVtYmVyLFxuXHRcdGludGVnZXJWYWx1ZTogQmlnU291cmNlLFxuXHRcdHJlYWRvbmx5IGZhY3Rvcnk6IElQcmVjaXNlTnVtYmVyRmFjdG9yeTxUPlxuXHQpIHtcblx0XHRjb25zdCBudW1iZXIgPSBmYWN0b3J5Lm5ld0Ftb3VudEZyb21JbnRlZ2VyKFxuXHRcdFx0aW50ZWdlclZhbHVlLCBwcmVjaXNpb25cblx0XHQpO1xuXG5cdFx0dGhpcy5tYXRoID0gbmV3IFByZWNpc2VNYXRoKFxuXHRcdFx0bnVtYmVyLFxuXHRcdFx0ZmFjdG9yeVxuXHRcdCk7XG5cdH1cblxuXHRhZGREZWNpbWFsKGRlY2ltYWw6IEJpZ1NvdXJjZSkge1xuXHRcdHJldHVybiB0aGlzLm1hdGguYWRkRGVjaW1hbChkZWNpbWFsKTtcblx0fVxuXHRhZGQoYW1vdW50OiBUKSB7XG5cdFx0cmV0dXJuIHRoaXMubWF0aC5hZGQoYW1vdW50KTtcblx0fVxuXG5cdHN1YnRyYWN0RGVjaW1hbChkZWNpbWFsOiBCaWdTb3VyY2UpIHtcblx0XHRyZXR1cm4gdGhpcy5tYXRoLnN1YnRyYWN0RGVjaW1hbChkZWNpbWFsKTtcblx0fVxuXHRzdWJ0cmFjdChhbW91bnQ6IFQpIHtcblx0XHRyZXR1cm4gdGhpcy5tYXRoLnN1YnRyYWN0KGFtb3VudCk7XG5cdH1cblxuXHRtdWx0aXBseURlY2ltYWwoZGVjaW1hbDogQmlnU291cmNlKSB7XG5cdFx0cmV0dXJuIHRoaXMubWF0aC5tdWx0aXBseURlY2ltYWwoZGVjaW1hbCk7XG5cdH1cblx0ZGl2aWRlRGVjaW1hbChkZWNpbWFsOiBCaWdTb3VyY2UpIHtcblx0XHRyZXR1cm4gdGhpcy5tYXRoLmRpdmlkZURlY2ltYWwoZGVjaW1hbCk7XG5cdH1cblxuXHRhYnNvbHV0ZVZhbHVlKCkge1xuXHRcdHJldHVybiB0aGlzLm1hdGguYWJzb2x1dGVWYWx1ZSgpO1xuXHR9XG5cblx0aXNMZXNzVGhhbihvdGhlcjogVCk6IGJvb2xlYW4ge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5pc0xlc3NUaGFuKG90aGVyKTtcblx0fVxuXHRpc0xlc3NUaGFuRGVjaW1hbChvdGhlcjogQmlnU291cmNlKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmlzTGVzc1RoYW5EZWNpbWFsKG90aGVyKTtcblx0fVxuXG5cdGlzR3JlYXRlclRoYW4ob3RoZXI6IFQpOiBib29sZWFuIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuaXNHcmVhdGVyVGhhbihvdGhlcik7XG5cdH1cblx0aXNHcmVhdGVyVGhhbkRlY2ltYWwob3RoZXI6IEJpZ1NvdXJjZSkge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5pc0dyZWF0ZXJUaGFuRGVjaW1hbChvdGhlcik7XG5cdH1cblxuXHRpc0VxdWFsVG8ob3RoZXI6IFQpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuaXNFcXVhbFRvKG90aGVyKTtcblx0fVxuXHRpc0VxdWFsVG9EZWNpbWFsKG90aGVyOiBCaWdTb3VyY2UpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuaXNFcXVhbFRvRGVjaW1hbChvdGhlcik7XG5cdH1cblxuXG5cdGdldCBkZWNpbWFsKCkge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5kZWNpbWFsO1xuXHR9XG5cblx0Z2V0IGludGVnZXIoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmludGVnZXI7XG5cdH1cblxuXHRnZXQgaXNaZXJvKCkge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5pc1plcm87XG5cdH1cblx0Z2V0IGlzUG9zaXRpdmUoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmlzUG9zaXRpdmU7XG5cdH1cblx0Z2V0IGlzTmVnYXRpdmUoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmlzTmVnYXRpdmU7XG5cdH1cblxuXHRnZXQgYmlnSW50ZWdlcigpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuYmlnSW50ZWdlcjtcblx0fVxuXG5cdGdldCBwcmVjaXNpb24oKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLnByZWNpc2lvbjtcblx0fVxuXG5cdGdldCBmYWN0b3IoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmZhY3Rvcjtcblx0fVxuXG5cdGdldCBudW1iZXIoKSB7XG5cdFx0cmV0dXJuIHRoaXMuc3RhcnRpbmdWYWx1ZTtcblx0fVxuXG5cdGdldCBzdGFydGluZ1ZhbHVlKCkge1xuXHRcdHJldHVybiB0aGlzLm1hdGguc3RhcnRpbmdWYWx1ZTtcblx0fVxufVxuXG5leHBvcnQgY2xhc3MgTnVtYmVyRm9yUHJlY2lzZU1hdGggZXh0ZW5kcyBOdW1iZXJGb3JQcmVjaXNlTWF0aEJhc2U8SVByZWNpc2VOdW1iZXI+IHtcblx0Y29uc3RydWN0b3IoXG5cdFx0cHJlY2lzaW9uOiBudW1iZXIsXG5cdFx0aW50ZWdlclZhbHVlOiBCaWdTb3VyY2UgPSAwLFxuXHQpIHtcblx0XHRzdXBlcihcblx0XHRcdHByZWNpc2lvbixcblx0XHRcdGludGVnZXJWYWx1ZSxcblx0XHRcdG5ldyBQcmVjaXNlTnVtYmVyRmFjdG9yeShwcmVjaXNpb24pXG5cdFx0KTtcblx0fVxufVxuXG5cbmV4cG9ydCBjbGFzcyBEZWNpbWFsRm9yUHJlY2lzZU1hdGggZXh0ZW5kcyBOdW1iZXJGb3JQcmVjaXNlTWF0aCB7XG5cdGNvbnN0cnVjdG9yKFxuXHRcdGRlY2ltYWxWYWx1ZTogQmlnU291cmNlLFxuXHRcdHByZWNpc2lvbjogbnVtYmVyXG5cdCkge1xuXHRcdGNvbnN0IGZhY3RvciA9IG5ldyBCaWcoIE1hdGgucG93KDEwLCBwcmVjaXNpb24pICk7XG5cblx0XHRjb25zdCBkZWNpbWFsID0gbmV3IEJpZyhkZWNpbWFsVmFsdWUpO1xuXG5cdFx0c3VwZXIoXG5cdFx0XHRwcmVjaXNpb24sXG5cdFx0XHRkZWNpbWFsXG5cdFx0XHRcdC50aW1lcyhmYWN0b3IpXG5cdFx0XHRcdC5yb3VuZCgwLCBSb3VuZGluZ01vZGUuUm91bmREb3duKVxuXHRcdCk7XG5cdH1cbn1cbiJdfQ==