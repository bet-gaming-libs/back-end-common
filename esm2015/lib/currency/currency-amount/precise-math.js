/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/currency-amount/precise-math.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Big } from 'big.js';
/**
 * @template T
 */
export class PreciseMath {
    /**
     * @param {?} startingValue
     * @param {?} factory
     */
    constructor(startingValue, factory) {
        this.startingValue = startingValue;
        this.factory = factory;
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    addDecimal(decimal) {
        /** @type {?} */
        const other = this.factory.newAmountFromDecimal(decimal, this.precision);
        return this.add(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    add(other) {
        this.validator.isMatchingType(other);
        /** @type {?} */
        const newInteger = this.startingValue.bigInteger.plus(other.integer);
        /** @type {?} */
        const newNumber = this.factory.newAmountFromInteger(newInteger, this.precision);
        return new PreciseMath(newNumber, this.factory);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    subtractDecimal(decimal) {
        /** @type {?} */
        const other = this.factory.newAmountFromDecimal(decimal, this.precision);
        return this.subtract(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    subtract(other) {
        this.validator.isMatchingType(other);
        /** @type {?} */
        const newInteger = this.startingValue.bigInteger.minus(other.integer);
        /** @type {?} */
        const newNumber = this.factory.newAmountFromInteger(newInteger, this.precision);
        return new PreciseMath(newNumber, this.factory);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    divideDecimal(decimal) {
        return this.multiplyDecimal(new Big(1).div(decimal));
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    multiplyDecimal(decimal) {
        /** @type {?} */
        const product = new Big(this.startingValue.decimal).times(decimal);
        /** @type {?} */
        const newInteger = product
            .times(this.factor)
            .round(0, 0 /* RoundDown */);
        /** @type {?} */
        const newNumber = this.factory.newAmountFromInteger(newInteger, this.precision);
        return new PreciseMath(newNumber, this.factory);
    }
    /**
     * @return {?}
     */
    absoluteValue() {
        /** @type {?} */
        const newNumber = this.factory.newAmountFromInteger(new Big(this.bigInteger).abs(), this.precision);
        return new PreciseMath(newNumber, this.factory);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isLessThan(other) {
        return this.number.isLessThan(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isLessThanDecimal(other) {
        return this.number.isLessThanDecimal(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isGreaterThan(other) {
        return this.number.isGreaterThan(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isGreaterThanDecimal(other) {
        return this.number.isGreaterThanDecimal(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isEqualTo(other) {
        return this.number.isEqualTo(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isEqualToDecimal(other) {
        return this.number.isEqualToDecimal(other);
    }
    /**
     * @return {?}
     */
    get decimal() {
        return this.number.decimal;
    }
    /**
     * @return {?}
     */
    get integer() {
        return this.number.integer;
    }
    /**
     * @return {?}
     */
    get isZero() {
        return this.number.isZero;
    }
    /**
     * @return {?}
     */
    get isPositive() {
        return this.number.isPositive;
    }
    /**
     * @return {?}
     */
    get isNegative() {
        return this.number.isNegative;
    }
    /**
     * @return {?}
     */
    get bigInteger() {
        return this.number.bigInteger;
    }
    /**
     * @return {?}
     */
    get precision() {
        return this.number.precision;
    }
    /**
     * @return {?}
     */
    get factor() {
        return this.number.factor;
    }
    /**
     * @return {?}
     */
    get number() {
        return this.startingValue;
    }
    /**
     * @private
     * @return {?}
     */
    get validator() {
        return this.factory.validator;
    }
}
if (false) {
    /** @type {?} */
    PreciseMath.prototype.startingValue;
    /** @type {?} */
    PreciseMath.prototype.factory;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJlY2lzZS1tYXRoLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvY3VycmVuY3ktYW1vdW50L3ByZWNpc2UtbWF0aC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxHQUFHLEVBQTJCLE1BQU0sUUFBUSxDQUFDOzs7O0FBS3RELE1BQU0sT0FBTyxXQUFXOzs7OztJQUV2QixZQUNVLGFBQWdCLEVBQ2hCLE9BQWlDO1FBRGpDLGtCQUFhLEdBQWIsYUFBYSxDQUFHO1FBQ2hCLFlBQU8sR0FBUCxPQUFPLENBQTBCO0lBRTNDLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLE9BQWtCOztjQUN0QixLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FDOUMsT0FBTyxFQUNQLElBQUksQ0FBQyxTQUFTLENBQ2Q7UUFFRCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDeEIsQ0FBQzs7Ozs7SUFDRCxHQUFHLENBQUMsS0FBUTtRQUNYLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDOztjQUUvQixVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUNwRCxLQUFLLENBQUMsT0FBTyxDQUNiOztjQUVLLFNBQVMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLG9CQUFvQixDQUNsRCxVQUFVLEVBQ1YsSUFBSSxDQUFDLFNBQVMsQ0FDZDtRQUVELE9BQU8sSUFBSSxXQUFXLENBQ3JCLFNBQVMsRUFDVCxJQUFJLENBQUMsT0FBTyxDQUNaLENBQUM7SUFDSCxDQUFDOzs7OztJQUVELGVBQWUsQ0FBQyxPQUFrQjs7Y0FDM0IsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQzlDLE9BQU8sRUFDUCxJQUFJLENBQUMsU0FBUyxDQUNkO1FBRUQsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzdCLENBQUM7Ozs7O0lBQ0QsUUFBUSxDQUFDLEtBQVE7UUFDaEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7O2NBRS9CLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQ3JELEtBQUssQ0FBQyxPQUFPLENBQ2I7O2NBRUssU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQ2xELFVBQVUsRUFDVixJQUFJLENBQUMsU0FBUyxDQUNkO1FBRUQsT0FBTyxJQUFJLFdBQVcsQ0FDckIsU0FBUyxFQUNULElBQUksQ0FBQyxPQUFPLENBQ1osQ0FBQztJQUNILENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLE9BQWtCO1FBQy9CLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FDMUIsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUN2QixDQUFDO0lBQ0gsQ0FBQzs7Ozs7SUFDRCxlQUFlLENBQUMsT0FBa0I7O2NBQzNCLE9BQU8sR0FBRyxJQUFJLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQzFCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQzs7Y0FFVixVQUFVLEdBQUcsT0FBTzthQUNyQixLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQzthQUNsQixLQUFLLENBQUMsQ0FBQyxvQkFBeUI7O2NBRS9CLFNBQVMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLG9CQUFvQixDQUNsRCxVQUFVLEVBQ1YsSUFBSSxDQUFDLFNBQVMsQ0FDZDtRQUVELE9BQU8sSUFBSSxXQUFXLENBQ3JCLFNBQVMsRUFDVCxJQUFJLENBQUMsT0FBTyxDQUNaLENBQUM7SUFDSCxDQUFDOzs7O0lBRUQsYUFBYTs7Y0FDTixTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FDbEQsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUM5QixJQUFJLENBQUMsU0FBUyxDQUNkO1FBRUQsT0FBTyxJQUFJLFdBQVcsQ0FDckIsU0FBUyxFQUNULElBQUksQ0FBQyxPQUFPLENBQ1osQ0FBQztJQUNILENBQUM7Ozs7O0lBR0QsVUFBVSxDQUFDLEtBQVE7UUFDbEIsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN0QyxDQUFDOzs7OztJQUNELGlCQUFpQixDQUFDLEtBQWdCO1FBQ2pDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7OztJQUVELGFBQWEsQ0FBQyxLQUFRO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDekMsQ0FBQzs7Ozs7SUFDRCxvQkFBb0IsQ0FBQyxLQUFnQjtRQUNwQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEQsQ0FBQzs7Ozs7SUFFRCxTQUFTLENBQUMsS0FBUTtRQUNqQixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JDLENBQUM7Ozs7O0lBQ0QsZ0JBQWdCLENBQUMsS0FBZ0I7UUFDaEMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzVDLENBQUM7Ozs7SUFHRCxJQUFJLE9BQU87UUFDVixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQzVCLENBQUM7Ozs7SUFFRCxJQUFJLE9BQU87UUFDVixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQzVCLENBQUM7Ozs7SUFFRCxJQUFJLE1BQU07UUFDVCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQzNCLENBQUM7Ozs7SUFDRCxJQUFJLFVBQVU7UUFDYixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO0lBQy9CLENBQUM7Ozs7SUFDRCxJQUFJLFVBQVU7UUFDYixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO0lBQy9CLENBQUM7Ozs7SUFFRCxJQUFJLFVBQVU7UUFDYixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO0lBQy9CLENBQUM7Ozs7SUFFRCxJQUFJLFNBQVM7UUFDWixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO0lBQzlCLENBQUM7Ozs7SUFFRCxJQUFJLE1BQU07UUFDVCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQzNCLENBQUM7Ozs7SUFFRCxJQUFJLE1BQU07UUFDVCxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFHRCxJQUFZLFNBQVM7UUFDcEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQztJQUMvQixDQUFDO0NBQ0Q7OztJQTNKQyxvQ0FBeUI7O0lBQ3pCLDhCQUEwQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJpZywgUm91bmRpbmdNb2RlLCBCaWdTb3VyY2UgfSBmcm9tICdiaWcuanMnO1xuXG5pbXBvcnQgeyBJUHJlY2lzZU51bWJlckZhY3RvcnksIElQcmVjaXNlTnVtYmVyLCBJUHJlY2lzZU1hdGggfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuXG5cbmV4cG9ydCBjbGFzcyBQcmVjaXNlTWF0aDxUIGV4dGVuZHMgSVByZWNpc2VOdW1iZXI+XG5cdGltcGxlbWVudHMgSVByZWNpc2VNYXRoPFQ+IHtcblx0Y29uc3RydWN0b3IoXG5cdFx0cmVhZG9ubHkgc3RhcnRpbmdWYWx1ZTogVCxcblx0XHRyZWFkb25seSBmYWN0b3J5OiBJUHJlY2lzZU51bWJlckZhY3Rvcnk8VD5cblx0KSB7XG5cdH1cblxuXHRhZGREZWNpbWFsKGRlY2ltYWw6IEJpZ1NvdXJjZSkge1xuXHRcdGNvbnN0IG90aGVyID0gdGhpcy5mYWN0b3J5Lm5ld0Ftb3VudEZyb21EZWNpbWFsKFxuXHRcdFx0ZGVjaW1hbCxcblx0XHRcdHRoaXMucHJlY2lzaW9uXG5cdFx0KTtcblxuXHRcdHJldHVybiB0aGlzLmFkZChvdGhlcik7XG5cdH1cblx0YWRkKG90aGVyOiBUKSB7XG5cdFx0dGhpcy52YWxpZGF0b3IuaXNNYXRjaGluZ1R5cGUob3RoZXIpO1xuXG5cdFx0Y29uc3QgbmV3SW50ZWdlciA9IHRoaXMuc3RhcnRpbmdWYWx1ZS5iaWdJbnRlZ2VyLnBsdXMoXG5cdFx0XHRvdGhlci5pbnRlZ2VyXG5cdFx0KTtcblxuXHRcdGNvbnN0IG5ld051bWJlciA9IHRoaXMuZmFjdG9yeS5uZXdBbW91bnRGcm9tSW50ZWdlcihcblx0XHRcdG5ld0ludGVnZXIsXG5cdFx0XHR0aGlzLnByZWNpc2lvblxuXHRcdCk7XG5cblx0XHRyZXR1cm4gbmV3IFByZWNpc2VNYXRoKFxuXHRcdFx0bmV3TnVtYmVyLFxuXHRcdFx0dGhpcy5mYWN0b3J5XG5cdFx0KTtcblx0fVxuXG5cdHN1YnRyYWN0RGVjaW1hbChkZWNpbWFsOiBCaWdTb3VyY2UpIHtcblx0XHRjb25zdCBvdGhlciA9IHRoaXMuZmFjdG9yeS5uZXdBbW91bnRGcm9tRGVjaW1hbChcblx0XHRcdGRlY2ltYWwsXG5cdFx0XHR0aGlzLnByZWNpc2lvblxuXHRcdCk7XG5cblx0XHRyZXR1cm4gdGhpcy5zdWJ0cmFjdChvdGhlcik7XG5cdH1cblx0c3VidHJhY3Qob3RoZXI6IFQpIHtcblx0XHR0aGlzLnZhbGlkYXRvci5pc01hdGNoaW5nVHlwZShvdGhlcik7XG5cblx0XHRjb25zdCBuZXdJbnRlZ2VyID0gdGhpcy5zdGFydGluZ1ZhbHVlLmJpZ0ludGVnZXIubWludXMoXG5cdFx0XHRvdGhlci5pbnRlZ2VyXG5cdFx0KTtcblxuXHRcdGNvbnN0IG5ld051bWJlciA9IHRoaXMuZmFjdG9yeS5uZXdBbW91bnRGcm9tSW50ZWdlcihcblx0XHRcdG5ld0ludGVnZXIsXG5cdFx0XHR0aGlzLnByZWNpc2lvblxuXHRcdCk7XG5cblx0XHRyZXR1cm4gbmV3IFByZWNpc2VNYXRoKFxuXHRcdFx0bmV3TnVtYmVyLFxuXHRcdFx0dGhpcy5mYWN0b3J5XG5cdFx0KTtcblx0fVxuXG5cdGRpdmlkZURlY2ltYWwoZGVjaW1hbDogQmlnU291cmNlKSB7XG5cdFx0cmV0dXJuIHRoaXMubXVsdGlwbHlEZWNpbWFsKFxuXHRcdFx0bmV3IEJpZygxKS5kaXYoZGVjaW1hbClcblx0XHQpO1xuXHR9XG5cdG11bHRpcGx5RGVjaW1hbChkZWNpbWFsOiBCaWdTb3VyY2UpIHtcblx0XHRjb25zdCBwcm9kdWN0ID0gbmV3IEJpZyhcblx0XHRcdHRoaXMuc3RhcnRpbmdWYWx1ZS5kZWNpbWFsXG5cdFx0KS50aW1lcyhkZWNpbWFsKTtcblxuXHRcdGNvbnN0IG5ld0ludGVnZXIgPSBwcm9kdWN0XG5cdFx0XHRcdFx0XHQudGltZXModGhpcy5mYWN0b3IpXG5cdFx0XHRcdFx0XHQucm91bmQoMCwgUm91bmRpbmdNb2RlLlJvdW5kRG93bik7XG5cblx0XHRjb25zdCBuZXdOdW1iZXIgPSB0aGlzLmZhY3RvcnkubmV3QW1vdW50RnJvbUludGVnZXIoXG5cdFx0XHRuZXdJbnRlZ2VyLFxuXHRcdFx0dGhpcy5wcmVjaXNpb25cblx0XHQpO1xuXG5cdFx0cmV0dXJuIG5ldyBQcmVjaXNlTWF0aChcblx0XHRcdG5ld051bWJlcixcblx0XHRcdHRoaXMuZmFjdG9yeVxuXHRcdCk7XG5cdH1cblxuXHRhYnNvbHV0ZVZhbHVlKCkge1xuXHRcdGNvbnN0IG5ld051bWJlciA9IHRoaXMuZmFjdG9yeS5uZXdBbW91bnRGcm9tSW50ZWdlcihcblx0XHRcdG5ldyBCaWcodGhpcy5iaWdJbnRlZ2VyKS5hYnMoKSxcblx0XHRcdHRoaXMucHJlY2lzaW9uXG5cdFx0KTtcblxuXHRcdHJldHVybiBuZXcgUHJlY2lzZU1hdGgoXG5cdFx0XHRuZXdOdW1iZXIsXG5cdFx0XHR0aGlzLmZhY3Rvcnlcblx0XHQpO1xuXHR9XG5cblxuXHRpc0xlc3NUaGFuKG90aGVyOiBUKTogYm9vbGVhbiB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmlzTGVzc1RoYW4ob3RoZXIpO1xuXHR9XG5cdGlzTGVzc1RoYW5EZWNpbWFsKG90aGVyOiBCaWdTb3VyY2UpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuaXNMZXNzVGhhbkRlY2ltYWwob3RoZXIpO1xuXHR9XG5cblx0aXNHcmVhdGVyVGhhbihvdGhlcjogVCk6IGJvb2xlYW4ge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5pc0dyZWF0ZXJUaGFuKG90aGVyKTtcblx0fVxuXHRpc0dyZWF0ZXJUaGFuRGVjaW1hbChvdGhlcjogQmlnU291cmNlKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmlzR3JlYXRlclRoYW5EZWNpbWFsKG90aGVyKTtcblx0fVxuXG5cdGlzRXF1YWxUbyhvdGhlcjogVCkge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5pc0VxdWFsVG8ob3RoZXIpO1xuXHR9XG5cdGlzRXF1YWxUb0RlY2ltYWwob3RoZXI6IEJpZ1NvdXJjZSkge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5pc0VxdWFsVG9EZWNpbWFsKG90aGVyKTtcblx0fVxuXG5cblx0Z2V0IGRlY2ltYWwoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmRlY2ltYWw7XG5cdH1cblxuXHRnZXQgaW50ZWdlcigpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuaW50ZWdlcjtcblx0fVxuXG5cdGdldCBpc1plcm8oKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmlzWmVybztcblx0fVxuXHRnZXQgaXNQb3NpdGl2ZSgpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuaXNQb3NpdGl2ZTtcblx0fVxuXHRnZXQgaXNOZWdhdGl2ZSgpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuaXNOZWdhdGl2ZTtcblx0fVxuXG5cdGdldCBiaWdJbnRlZ2VyKCkge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5iaWdJbnRlZ2VyO1xuXHR9XG5cblx0Z2V0IHByZWNpc2lvbigpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIucHJlY2lzaW9uO1xuXHR9XG5cblx0Z2V0IGZhY3RvcigpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuZmFjdG9yO1xuXHR9XG5cblx0Z2V0IG51bWJlcigpIHtcblx0XHRyZXR1cm4gdGhpcy5zdGFydGluZ1ZhbHVlO1xuXHR9XG5cblxuXHRwcml2YXRlIGdldCB2YWxpZGF0b3IoKSB7XG5cdFx0cmV0dXJuIHRoaXMuZmFjdG9yeS52YWxpZGF0b3I7XG5cdH1cbn1cbiJdfQ==