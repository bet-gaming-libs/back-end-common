/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/coin-data-provider.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Coin } from './models/coin';
export class CoinDataProvider {
    /**
     * @param {?} database
     */
    constructor(database) {
        this.database = database;
        this.isInit = false;
        this.ids = {};
        this.data = {};
        this.allCoins = [];
    }
    /**
     * @return {?}
     */
    init() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.allCoins = (yield this.database.getAllCoins()).map((/**
             * @param {?} row
             * @return {?}
             */
            row => new Coin(row)));
            for (const row of this.allCoins) {
                // map symbol to ID
                this.ids[row.symbol] = row.id;
                // map ID to data
                this.data[row.id] = row;
            }
            this.isInit = true;
        });
    }
    /**
     * @param {?} symbol
     * @return {?}
     */
    getCoinDataBySymbol(symbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const id = yield this.getCoinId(symbol);
            return this.getCoinData(id);
        });
    }
    /**
     * @param {?} symbol
     * @return {?}
     */
    getCoinId(symbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!this.isInit) {
                throw new Error('CoinDataProvider is NOT Initialized!');
            }
            symbol = symbol.toUpperCase();
            /** @type {?} */
            const id = this.ids[symbol];
            if (id === undefined) {
                throw new Error('CoinId NOT FOUND: ' + symbol);
            }
            return id;
        });
    }
    /**
     * @param {?} coinId
     * @return {?}
     */
    getCoinSymbol(coinId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.getCoinData(coinId);
            return data.symbol;
        });
    }
    /**
     * @param {?} coinId
     * @return {?}
     */
    getCoinData(coinId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!this.isInit) {
                throw new Error('CoinDataProvider is NOT Initialized!');
            }
            /** @type {?} */
            const data = this.data[coinId];
            if (!data) {
                throw new Error('Coin NOT FOUND: ' + coinId);
            }
            return data;
        });
    }
    /**
     * @return {?}
     */
    getAllCoins() {
        return this.allCoins.slice();
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.isInit;
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.ids;
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.data;
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.allCoins;
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.database;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29pbi1kYXRhLXByb3ZpZGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvZGF0YWJhc2UvY29pbi1kYXRhLXByb3ZpZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUVBLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHckMsTUFBTSxPQUFPLGdCQUFnQjs7OztJQVE1QixZQUE2QixRQUE4QjtRQUE5QixhQUFRLEdBQVIsUUFBUSxDQUFzQjtRQVBuRCxXQUFNLEdBQUcsS0FBSyxDQUFDO1FBRWYsUUFBRyxHQUErQixFQUFFLENBQUM7UUFDckMsU0FBSSxHQUF5QixFQUFFLENBQUM7UUFDaEMsYUFBUSxHQUFXLEVBQUUsQ0FBQztJQUk5QixDQUFDOzs7O0lBRUssSUFBSTs7WUFDVCxJQUFJLENBQUMsUUFBUSxHQUFHLENBQ2YsTUFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUNqQyxDQUFDLEdBQUc7Ozs7WUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUM7WUFFOUIsS0FBSyxNQUFNLEdBQUcsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNoQyxtQkFBbUI7Z0JBQ25CLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQyxFQUFFLENBQUM7Z0JBQzlCLGlCQUFpQjtnQkFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDO2FBQ3hCO1lBRUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDcEIsQ0FBQztLQUFBOzs7OztJQUVLLG1CQUFtQixDQUFDLE1BQWM7OztrQkFDakMsRUFBRSxHQUFHLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7WUFFdkMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzdCLENBQUM7S0FBQTs7Ozs7SUFFSyxTQUFTLENBQUMsTUFBYzs7WUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ2pCLE1BQU0sSUFBSSxLQUFLLENBQUMsc0NBQXNDLENBQUMsQ0FBQzthQUN4RDtZQUVELE1BQU0sR0FBRyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUM7O2tCQUV4QixFQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUM7WUFFM0IsSUFBSSxFQUFFLEtBQUssU0FBUyxFQUFFO2dCQUNyQixNQUFNLElBQUksS0FBSyxDQUFDLG9CQUFvQixHQUFHLE1BQU0sQ0FBQyxDQUFDO2FBQy9DO1lBRUQsT0FBTyxFQUFFLENBQUM7UUFDWCxDQUFDO0tBQUE7Ozs7O0lBRUssYUFBYSxDQUFDLE1BQWM7OztrQkFDM0IsSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7WUFFM0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3BCLENBQUM7S0FBQTs7Ozs7SUFFSyxXQUFXLENBQUMsTUFBYzs7WUFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ2pCLE1BQU0sSUFBSSxLQUFLLENBQUMsc0NBQXNDLENBQUMsQ0FBQzthQUN4RDs7a0JBRUssSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBRTlCLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ1YsTUFBTSxJQUFJLEtBQUssQ0FBQyxrQkFBa0IsR0FBRyxNQUFNLENBQUMsQ0FBQzthQUM3QztZQUVELE9BQU8sSUFBSSxDQUFDO1FBQ2IsQ0FBQztLQUFBOzs7O0lBRUQsV0FBVztRQUNWLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUM5QixDQUFDO0NBQ0Q7Ozs7OztJQXRFQSxrQ0FBdUI7Ozs7O0lBRXZCLCtCQUE2Qzs7Ozs7SUFDN0MsZ0NBQXdDOzs7OztJQUN4QyxvQ0FBOEI7Ozs7O0lBR2xCLG9DQUErQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElDb2luRGF0YVByb3ZpZGVyLCBJQ29pbkRhdGFiYXNlU2VydmljZSB9IGZyb20gJy4uL2N1cnJlbmN5LWFtb3VudC9pbnRlcmZhY2VzJztcbmltcG9ydCB7IENvaW5JZCB9IGZyb20gJy4vY29pbnMnO1xuaW1wb3J0IHsgQ29pbiB9IGZyb20gJy4vbW9kZWxzL2NvaW4nO1xuXG5cbmV4cG9ydCBjbGFzcyBDb2luRGF0YVByb3ZpZGVyIGltcGxlbWVudHMgSUNvaW5EYXRhUHJvdmlkZXIge1xuXHRwcml2YXRlIGlzSW5pdCA9IGZhbHNlO1xuXG5cdHByaXZhdGUgaWRzOiB7W3N5bWJvbDogc3RyaW5nXTogQ29pbklkfSA9IHt9O1xuXHRwcml2YXRlIGRhdGE6IHtbaWQ6IG51bWJlcl06IENvaW59ID0ge307XG5cdHByaXZhdGUgYWxsQ29pbnM6IENvaW5bXSA9IFtdO1xuXG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSByZWFkb25seSBkYXRhYmFzZTogSUNvaW5EYXRhYmFzZVNlcnZpY2UpIHtcblx0fVxuXG5cdGFzeW5jIGluaXQoKSB7XG5cdFx0dGhpcy5hbGxDb2lucyA9IChcblx0XHRcdGF3YWl0IHRoaXMuZGF0YWJhc2UuZ2V0QWxsQ29pbnMoKVxuXHRcdCkubWFwKCByb3cgPT4gbmV3IENvaW4ocm93KSApO1xuXG5cdFx0Zm9yIChjb25zdCByb3cgb2YgdGhpcy5hbGxDb2lucykge1xuXHRcdFx0Ly8gbWFwIHN5bWJvbCB0byBJRFxuXHRcdFx0dGhpcy5pZHNbcm93LnN5bWJvbF0gPSByb3cuaWQ7XG5cdFx0XHQvLyBtYXAgSUQgdG8gZGF0YVxuXHRcdFx0dGhpcy5kYXRhW3Jvdy5pZF0gPSByb3c7XG5cdFx0fVxuXG5cdFx0dGhpcy5pc0luaXQgPSB0cnVlO1xuXHR9XG5cblx0YXN5bmMgZ2V0Q29pbkRhdGFCeVN5bWJvbChzeW1ib2w6IHN0cmluZyk6IFByb21pc2U8Q29pbj4ge1xuXHRcdGNvbnN0IGlkID0gYXdhaXQgdGhpcy5nZXRDb2luSWQoc3ltYm9sKTtcblxuXHRcdHJldHVybiB0aGlzLmdldENvaW5EYXRhKGlkKTtcblx0fVxuXG5cdGFzeW5jIGdldENvaW5JZChzeW1ib2w6IHN0cmluZyk6IFByb21pc2U8Q29pbklkPiB7XG5cdFx0aWYgKCF0aGlzLmlzSW5pdCkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdDb2luRGF0YVByb3ZpZGVyIGlzIE5PVCBJbml0aWFsaXplZCEnKTtcblx0XHR9XG5cblx0XHRzeW1ib2wgPSBzeW1ib2wudG9VcHBlckNhc2UoKTtcblxuXHRcdGNvbnN0IGlkID0gdGhpcy5pZHNbc3ltYm9sXTtcblxuXHRcdGlmIChpZCA9PT0gdW5kZWZpbmVkKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ0NvaW5JZCBOT1QgRk9VTkQ6ICcgKyBzeW1ib2wpO1xuXHRcdH1cblxuXHRcdHJldHVybiBpZDtcblx0fVxuXG5cdGFzeW5jIGdldENvaW5TeW1ib2woY29pbklkOiBDb2luSWQpOiBQcm9taXNlPHN0cmluZz4ge1xuXHRcdGNvbnN0IGRhdGEgPSBhd2FpdCB0aGlzLmdldENvaW5EYXRhKGNvaW5JZCk7XG5cblx0XHRyZXR1cm4gZGF0YS5zeW1ib2w7XG5cdH1cblxuXHRhc3luYyBnZXRDb2luRGF0YShjb2luSWQ6IENvaW5JZCk6IFByb21pc2U8Q29pbj4ge1xuXHRcdGlmICghdGhpcy5pc0luaXQpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignQ29pbkRhdGFQcm92aWRlciBpcyBOT1QgSW5pdGlhbGl6ZWQhJyk7XG5cdFx0fVxuXG5cdFx0Y29uc3QgZGF0YSA9IHRoaXMuZGF0YVtjb2luSWRdO1xuXG5cdFx0aWYgKCFkYXRhKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ0NvaW4gTk9UIEZPVU5EOiAnICsgY29pbklkKTtcblx0XHR9XG5cblx0XHRyZXR1cm4gZGF0YTtcblx0fVxuXG5cdGdldEFsbENvaW5zKCkge1xuXHRcdHJldHVybiB0aGlzLmFsbENvaW5zLnNsaWNlKCk7XG5cdH1cbn1cbiJdfQ==