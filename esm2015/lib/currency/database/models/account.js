/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/models/account.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ISavedAccountRow() { }
if (false) {
    /** @type {?} */
    ISavedAccountRow.prototype.id;
    /** @type {?} */
    ISavedAccountRow.prototype.eos_account;
    /** @type {?} */
    ISavedAccountRow.prototype.ezeos_id;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2N1cnJlbmN5L2RhdGFiYXNlL21vZGVscy9hY2NvdW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsc0NBSUM7OztJQUhBLDhCQUFXOztJQUNYLHVDQUFvQjs7SUFDcEIsb0NBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBJU2F2ZWRBY2NvdW50Um93IHtcblx0aWQ6IG51bWJlcjtcblx0ZW9zX2FjY291bnQ6IHN0cmluZztcblx0ZXplb3NfaWQ6IHN0cmluZztcbn1cbiJdfQ==