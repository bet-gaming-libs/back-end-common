/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/models/coin.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { PreciseDecimal } from '../../currency-amount/precise-math-numbers';
/**
 * @record
 */
export function ISavedCoinRow() { }
if (false) {
    /** @type {?} */
    ISavedCoinRow.prototype.id;
    /** @type {?} */
    ISavedCoinRow.prototype.symbol;
    /** @type {?} */
    ISavedCoinRow.prototype.precision;
    /** @type {?} */
    ISavedCoinRow.prototype.uses_memo_for_deposits;
    /** @type {?} */
    ISavedCoinRow.prototype.minimum_withdrawal_amount;
}
export class Coin {
    /**
     * @param {?} data
     */
    constructor(data) {
        this.id = data.id;
        this.symbol = data.symbol;
        this.precision = data.precision;
        this.usesMemoForDeposits = data.uses_memo_for_deposits == 1;
        this.minimumWithdrawalAmount = new PreciseDecimal(data.minimum_withdrawal_amount, this.precision);
    }
    /**
     * @return {?}
     */
    get data() {
        return {
            id: this.id,
            symbol: this.symbol,
            precision: this.precision,
            usesMemoForDeposits: this.usesMemoForDeposits,
            minimumWithdrawalAmount: this.minimumWithdrawalAmount.decimal
        };
    }
}
if (false) {
    /** @type {?} */
    Coin.prototype.id;
    /** @type {?} */
    Coin.prototype.symbol;
    /** @type {?} */
    Coin.prototype.precision;
    /** @type {?} */
    Coin.prototype.usesMemoForDeposits;
    /** @type {?} */
    Coin.prototype.minimumWithdrawalAmount;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29pbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2N1cnJlbmN5L2RhdGFiYXNlL21vZGVscy9jb2luLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRDQUE0QyxDQUFDOzs7O0FBSzVFLG1DQU1DOzs7SUFMQSwyQkFBVzs7SUFDWCwrQkFBZTs7SUFDZixrQ0FBa0I7O0lBQ2xCLCtDQUErQjs7SUFDL0Isa0RBQWtDOztBQUduQyxNQUFNLE9BQU8sSUFBSTs7OztJQU9oQixZQUFZLElBQW1CO1FBQzlCLElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsc0JBQXNCLElBQUksQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLGNBQWMsQ0FDaEQsSUFBSSxDQUFDLHlCQUF5QixFQUM5QixJQUFJLENBQUMsU0FBUyxDQUNkLENBQUM7SUFDSCxDQUFDOzs7O0lBRUQsSUFBSSxJQUFJO1FBQ1AsT0FBTztZQUNOLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNYLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTtZQUNuQixTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7WUFDekIsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLG1CQUFtQjtZQUM3Qyx1QkFBdUIsRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTztTQUM3RCxDQUFDO0lBQ0gsQ0FBQztDQUNEOzs7SUExQkEsa0JBQW9COztJQUNwQixzQkFBd0I7O0lBQ3hCLHlCQUEyQjs7SUFDM0IsbUNBQXNDOztJQUN0Qyx1Q0FBaUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQcmVjaXNlRGVjaW1hbCB9IGZyb20gJy4uLy4uL2N1cnJlbmN5LWFtb3VudC9wcmVjaXNlLW1hdGgtbnVtYmVycyc7XG5pbXBvcnQgeyBDb2luSWQgfSBmcm9tICcuLi9jb2lucyc7XG5pbXBvcnQgeyBJQ3VycmVuY3kgfSBmcm9tICcuLi8uLi9jdXJyZW5jeS1hbW91bnQvaW50ZXJmYWNlcyc7XG5cblxuZXhwb3J0IGludGVyZmFjZSBJU2F2ZWRDb2luUm93IHtcblx0aWQ6IG51bWJlcjtcblx0c3ltYm9sOiBzdHJpbmc7XG5cdHByZWNpc2lvbjogbnVtYmVyO1xuXHR1c2VzX21lbW9fZm9yX2RlcG9zaXRzOiBudW1iZXI7XG5cdG1pbmltdW1fd2l0aGRyYXdhbF9hbW91bnQ6IHN0cmluZztcbn1cblxuZXhwb3J0IGNsYXNzIENvaW4gaW1wbGVtZW50cyBJQ3VycmVuY3kge1xuXHRyZWFkb25seSBpZDogQ29pbklkO1xuXHRyZWFkb25seSBzeW1ib2w6IHN0cmluZztcblx0cmVhZG9ubHkgcHJlY2lzaW9uOiBudW1iZXI7XG5cdHJlYWRvbmx5IHVzZXNNZW1vRm9yRGVwb3NpdHM6IGJvb2xlYW47XG5cdHJlYWRvbmx5IG1pbmltdW1XaXRoZHJhd2FsQW1vdW50OiBQcmVjaXNlRGVjaW1hbDtcblxuXHRjb25zdHJ1Y3RvcihkYXRhOiBJU2F2ZWRDb2luUm93KSB7XG5cdFx0dGhpcy5pZCA9IGRhdGEuaWQ7XG5cdFx0dGhpcy5zeW1ib2wgPSBkYXRhLnN5bWJvbDtcblx0XHR0aGlzLnByZWNpc2lvbiA9IGRhdGEucHJlY2lzaW9uO1xuXHRcdHRoaXMudXNlc01lbW9Gb3JEZXBvc2l0cyA9IGRhdGEudXNlc19tZW1vX2Zvcl9kZXBvc2l0cyA9PSAxO1xuXHRcdHRoaXMubWluaW11bVdpdGhkcmF3YWxBbW91bnQgPSBuZXcgUHJlY2lzZURlY2ltYWwoXG5cdFx0XHRkYXRhLm1pbmltdW1fd2l0aGRyYXdhbF9hbW91bnQsXG5cdFx0XHR0aGlzLnByZWNpc2lvblxuXHRcdCk7XG5cdH1cblxuXHRnZXQgZGF0YSgpIHtcblx0XHRyZXR1cm4ge1xuXHRcdFx0aWQ6IHRoaXMuaWQsXG5cdFx0XHRzeW1ib2w6IHRoaXMuc3ltYm9sLFxuXHRcdFx0cHJlY2lzaW9uOiB0aGlzLnByZWNpc2lvbixcblx0XHRcdHVzZXNNZW1vRm9yRGVwb3NpdHM6IHRoaXMudXNlc01lbW9Gb3JEZXBvc2l0cyxcblx0XHRcdG1pbmltdW1XaXRoZHJhd2FsQW1vdW50OiB0aGlzLm1pbmltdW1XaXRoZHJhd2FsQW1vdW50LmRlY2ltYWxcblx0XHR9O1xuXHR9XG59XG4iXX0=