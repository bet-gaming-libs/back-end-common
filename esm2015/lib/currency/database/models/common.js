/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/models/common.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ISavedEntity() { }
if (false) {
    /** @type {?} */
    ISavedEntity.prototype.id;
}
/** @type {?} */
export const CommonFieldNames = {
    ID: 'id',
    CREATED_AT: 'created_at',
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvZGF0YWJhc2UvbW9kZWxzL2NvbW1vbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLGtDQUVDOzs7SUFEQSwwQkFBVzs7O0FBR1osTUFBTSxPQUFPLGdCQUFnQixHQUFHO0lBQy9CLEVBQUUsRUFBRSxJQUFJO0lBQ1IsVUFBVSxFQUFFLFlBQVk7Q0FDeEIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIElTYXZlZEVudGl0eSB7XG5cdGlkOiBudW1iZXI7XG59XG5cbmV4cG9ydCBjb25zdCBDb21tb25GaWVsZE5hbWVzID0ge1xuXHRJRDogJ2lkJyxcblx0Q1JFQVRFRF9BVDogJ2NyZWF0ZWRfYXQnLFxufTtcbiJdfQ==