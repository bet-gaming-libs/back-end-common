/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/models/withdrawal-request.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * @record
 */
export function INewWithdrawalRequestRow() { }
if (false) {
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.id;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.memo;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.coin_id;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.tx_hash;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.is_processed;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.easy_account_id;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.eos_account_name;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.withdraw_address;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.coin_decimal_amount;
}
/**
 * @record
 */
export function IFailedWithdrawalRequestRow() { }
if (false) {
    /** @type {?} */
    IFailedWithdrawalRequestRow.prototype.error_message;
}
/**
 * @record
 */
export function ISucceededWithdrawalRequest() { }
if (false) {
    /** @type {?} */
    ISucceededWithdrawalRequest.prototype.processed_at;
    /** @type {?} */
    ISucceededWithdrawalRequest.prototype.withdraw_transaction_id;
}
/**
 * @record
 */
export function IWithdrawalRequestInsufficientFunds() { }
if (false) {
    /** @type {?} */
    IWithdrawalRequestInsufficientFunds.prototype.insufficient_funds;
}
/**
 * @record
 */
export function ISavedWithdrawalRequestRow() { }
if (false) {
    /** @type {?} */
    ISavedWithdrawalRequestRow.prototype.requested_at;
}
/**
 * @record
 */
export function IWithdrawalRequest() { }
if (false) {
    /** @type {?} */
    IWithdrawalRequest.prototype.address;
    /** @type {?} */
    IWithdrawalRequest.prototype.memo;
    /**
     * @return {?}
     */
    IWithdrawalRequest.prototype.getAmount = function () { };
}
/**
 * @record
 */
export function ISavedWithdrawalRequest() { }
if (false) {
    /**
     * @return {?}
     */
    ISavedWithdrawalRequest.prototype.getRefundParams = function () { };
}
export class SavedWithdrawalRequest {
    /**
     * @param {?} data
     * @param {?} account
     * @param {?} amountFactory
     */
    constructor(data, account, amountFactory) {
        this.account = account;
        this.amountFactory = amountFactory;
        this.userId = undefined;
        this.amount = undefined;
        this.id = data.id;
        this.coinId = data.coin_id;
        this.decimalAmount = data.coin_decimal_amount;
        this.transactionHash = data.tx_hash;
        this.address = data.withdraw_address;
        this.memo = data.memo ? data.memo : '';
        this.eosAccountName = data.eos_account_name;
        this.easyAccountId = data.easy_account_id;
        this.isEosAccount = Number(this.easyAccountId) == 0;
        this.errorMessage = data.error_message;
    }
    /**
     * @return {?}
     */
    getDataForApi() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const amount = yield this.getAmount();
            return {
                id: this.id,
                memo: this.memo,
                address: this.address,
                errorMessage: this.errorMessage,
                coin: amount.currency.symbol,
                amount: amount.decimal,
            };
        });
    }
    /**
     * @return {?}
     */
    getAmount() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this.amount == undefined) {
                this.amount = yield this.amountFactory.newAmountFromDecimalAndCoinId(this.decimalAmount, this.coinId);
            }
            return this.amount;
        });
    }
    /**
     * @return {?}
     */
    getRefundParams() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const userId = yield this.getUserId();
            /** @type {?} */
            const info = yield this.account.getAccountInfo(userId);
            return {
                requestId: this.id,
                easyAccountPublicKey: info.easy_account_public_key ?
                    info.easy_account_public_key :
                    '',
                reason: this.errorMessage
            };
        });
    }
    /**
     * @return {?}
     */
    getUserId() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this.userId == undefined) {
                this.userId =
                    this.isEosAccount ?
                        yield this.account.getUserIdForEosAccount(this.eosAccountName) :
                        yield this.account.getUserIdForEasyAccount(this.easyAccountId);
            }
            return this.userId;
        });
    }
}
if (false) {
    /** @type {?} */
    SavedWithdrawalRequest.prototype.address;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.id;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.transactionHash;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.memo;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.coinId;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.decimalAmount;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.errorMessage;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.isEosAccount;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.eosAccountName;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.easyAccountId;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.userId;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.amount;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.account;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.amountFactory;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2l0aGRyYXdhbC1yZXF1ZXN0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvZGF0YWJhc2UvbW9kZWxzL3dpdGhkcmF3YWwtcmVxdWVzdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFNQSw4Q0FVQzs7O0lBVEEsc0NBQVc7O0lBQ1gsd0NBQWE7O0lBQ2IsMkNBQWdCOztJQUNoQiwyQ0FBZ0I7O0lBQ2hCLGdEQUFxQjs7SUFDckIsbURBQXdCOztJQUN4QixvREFBeUI7O0lBQ3pCLG9EQUF5Qjs7SUFDekIsdURBQTRCOzs7OztBQUc3QixpREFFQzs7O0lBREEsb0RBQXNCOzs7OztBQUd2QixpREFHQzs7O0lBRkEsbURBQW1COztJQUNuQiw4REFBZ0M7Ozs7O0FBR2pDLHlEQUVDOzs7SUFEQSxpRUFBMkI7Ozs7O0FBRzVCLGdEQUVDOzs7SUFEQSxrREFBbUI7Ozs7O0FBS3BCLHdDQUtDOzs7SUFKQSxxQ0FBZ0I7O0lBQ2hCLGtDQUFhOzs7O0lBRWIseURBQXNDOzs7OztBQUd2Qyw2Q0FFQzs7Ozs7SUFEQSxvRUFBMkQ7O0FBSTVELE1BQU0sT0FBTyxzQkFBc0I7Ozs7OztJQWdCbEMsWUFDQyxJQUFnQyxFQUNmLE9BQW9DLEVBQ3BDLGFBQXFDO1FBRHJDLFlBQU8sR0FBUCxPQUFPLENBQTZCO1FBQ3BDLGtCQUFhLEdBQWIsYUFBYSxDQUF3QjtRQU4vQyxXQUFNLEdBQVcsU0FBUyxDQUFDO1FBQzNCLFdBQU0sR0FBb0IsU0FBUyxDQUFDO1FBTzNDLElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDM0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7UUFFOUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBQ3JDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBRXZDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBQzVDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUMxQyxJQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUN4QyxDQUFDOzs7O0lBRUssYUFBYTs7O2tCQUNaLE1BQU0sR0FBRyxNQUFNLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFFckMsT0FBTztnQkFDTixFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUU7Z0JBQ1gsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO2dCQUNmLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTztnQkFDckIsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZO2dCQUMvQixJQUFJLEVBQUUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNO2dCQUM1QixNQUFNLEVBQUUsTUFBTSxDQUFDLE9BQU87YUFDdEIsQ0FBQztRQUNILENBQUM7S0FBQTs7OztJQUVLLFNBQVM7O1lBQ2QsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLFNBQVMsRUFBRTtnQkFDN0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsNkJBQTZCLENBQ25FLElBQUksQ0FBQyxhQUFhLEVBQ2xCLElBQUksQ0FBQyxNQUFNLENBQ1gsQ0FBQzthQUNGO1lBRUQsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3BCLENBQUM7S0FBQTs7OztJQUVLLGVBQWU7OztrQkFDZCxNQUFNLEdBQUcsTUFBTSxJQUFJLENBQUMsU0FBUyxFQUFFOztrQkFFL0IsSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO1lBRXRELE9BQU87Z0JBQ04sU0FBUyxFQUFFLElBQUksQ0FBQyxFQUFFO2dCQUNsQixvQkFBb0IsRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQztvQkFDOUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7b0JBQzlCLEVBQUU7Z0JBQ1IsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZO2FBQ3pCLENBQUM7UUFDSCxDQUFDO0tBQUE7Ozs7SUFFSyxTQUFTOztZQUNkLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxTQUFTLEVBQUU7Z0JBQzdCLElBQUksQ0FBQyxNQUFNO29CQUNWLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzt3QkFDbEIsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUN4QyxJQUFJLENBQUMsY0FBYyxDQUNuQixDQUFDLENBQUM7d0JBQ0gsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUN6QyxJQUFJLENBQUMsYUFBYSxDQUNsQixDQUFDO2FBQ0o7WUFFRCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDcEIsQ0FBQztLQUFBO0NBQ0Q7OztJQXRGQSx5Q0FBeUI7O0lBQ3pCLG9DQUFvQjs7SUFDcEIsaURBQWlDOztJQUNqQyxzQ0FBc0I7O0lBQ3RCLHdDQUF3Qjs7SUFDeEIsK0NBQStCOztJQUMvQiw4Q0FBOEI7Ozs7O0lBRTlCLDhDQUF1Qzs7Ozs7SUFDdkMsZ0RBQXdDOzs7OztJQUN4QywrQ0FBdUM7Ozs7O0lBRXZDLHdDQUFtQzs7Ozs7SUFDbkMsd0NBQTRDOzs7OztJQUkzQyx5Q0FBcUQ7Ozs7O0lBQ3JELCtDQUFzRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElSZWZ1bmRXaXRoZHJhd2FsUmVxdWVzdFBhcmFtcyB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSVVzZXJBY2NvdW50RGF0YWJhc2VTZXJ2aWNlIH0gZnJvbSAnLi4vaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJQ3VycmVuY3lBbW91bnQsIElDdXJyZW5jeUFtb3VudEZhY3RvcnkgfSBmcm9tICcuLi8uLi9jdXJyZW5jeS1hbW91bnQvaW50ZXJmYWNlcyc7XG5cblxuXG5leHBvcnQgaW50ZXJmYWNlIElOZXdXaXRoZHJhd2FsUmVxdWVzdFJvdyB7XG5cdGlkOiBzdHJpbmc7XG5cdG1lbW86IHN0cmluZztcblx0Y29pbl9pZDogbnVtYmVyO1xuXHR0eF9oYXNoOiBzdHJpbmc7XG5cdGlzX3Byb2Nlc3NlZDogbnVtYmVyO1xuXHRlYXN5X2FjY291bnRfaWQ6IHN0cmluZztcblx0ZW9zX2FjY291bnRfbmFtZTogc3RyaW5nO1xuXHR3aXRoZHJhd19hZGRyZXNzOiBzdHJpbmc7XG5cdGNvaW5fZGVjaW1hbF9hbW91bnQ6IHN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJRmFpbGVkV2l0aGRyYXdhbFJlcXVlc3RSb3cgZXh0ZW5kcyBJTmV3V2l0aGRyYXdhbFJlcXVlc3RSb3cge1xuXHRlcnJvcl9tZXNzYWdlOiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVN1Y2NlZWRlZFdpdGhkcmF3YWxSZXF1ZXN0IGV4dGVuZHMgSU5ld1dpdGhkcmF3YWxSZXF1ZXN0Um93ICB7XG5cdHByb2Nlc3NlZF9hdDogRGF0ZTtcblx0d2l0aGRyYXdfdHJhbnNhY3Rpb25faWQ6IG51bWJlcjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJV2l0aGRyYXdhbFJlcXVlc3RJbnN1ZmZpY2llbnRGdW5kcyBleHRlbmRzIElOZXdXaXRoZHJhd2FsUmVxdWVzdFJvdyB7XG5cdGluc3VmZmljaWVudF9mdW5kczogbnVtYmVyO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElTYXZlZFdpdGhkcmF3YWxSZXF1ZXN0Um93IGV4dGVuZHMgSUZhaWxlZFdpdGhkcmF3YWxSZXF1ZXN0Um93LCBJU3VjY2VlZGVkV2l0aGRyYXdhbFJlcXVlc3QsIElXaXRoZHJhd2FsUmVxdWVzdEluc3VmZmljaWVudEZ1bmRzIHtcblx0cmVxdWVzdGVkX2F0OiBEYXRlO1xufVxuXG5cblxuZXhwb3J0IGludGVyZmFjZSBJV2l0aGRyYXdhbFJlcXVlc3Qge1xuXHRhZGRyZXNzOiBzdHJpbmc7XG5cdG1lbW86IHN0cmluZztcblxuXHRnZXRBbW91bnQoKTogUHJvbWlzZTxJQ3VycmVuY3lBbW91bnQ+O1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElTYXZlZFdpdGhkcmF3YWxSZXF1ZXN0IGV4dGVuZHMgSVdpdGhkcmF3YWxSZXF1ZXN0IHtcblx0Z2V0UmVmdW5kUGFyYW1zKCk6IFByb21pc2U8SVJlZnVuZFdpdGhkcmF3YWxSZXF1ZXN0UGFyYW1zPjtcbn1cblxuXG5leHBvcnQgY2xhc3MgU2F2ZWRXaXRoZHJhd2FsUmVxdWVzdCBpbXBsZW1lbnRzIElTYXZlZFdpdGhkcmF3YWxSZXF1ZXN0IHtcblx0cmVhZG9ubHkgYWRkcmVzczogc3RyaW5nO1xuXHRyZWFkb25seSBpZDogc3RyaW5nO1xuXHRyZWFkb25seSB0cmFuc2FjdGlvbkhhc2g6IHN0cmluZztcblx0cmVhZG9ubHkgbWVtbzogc3RyaW5nO1xuXHRyZWFkb25seSBjb2luSWQ6IG51bWJlcjtcblx0cmVhZG9ubHkgZGVjaW1hbEFtb3VudDogc3RyaW5nO1xuXHRyZWFkb25seSBlcnJvck1lc3NhZ2U6IHN0cmluZztcblxuXHRwcml2YXRlIHJlYWRvbmx5IGlzRW9zQWNjb3VudDogYm9vbGVhbjtcblx0cHJpdmF0ZSByZWFkb25seSBlb3NBY2NvdW50TmFtZTogc3RyaW5nO1xuXHRwcml2YXRlIHJlYWRvbmx5IGVhc3lBY2NvdW50SWQ6IHN0cmluZztcblxuXHRwcml2YXRlIHVzZXJJZDogbnVtYmVyID0gdW5kZWZpbmVkO1xuXHRwcml2YXRlIGFtb3VudDogSUN1cnJlbmN5QW1vdW50ID0gdW5kZWZpbmVkO1xuXG5cdGNvbnN0cnVjdG9yKFxuXHRcdGRhdGE6IElTYXZlZFdpdGhkcmF3YWxSZXF1ZXN0Um93LFxuXHRcdHByaXZhdGUgcmVhZG9ubHkgYWNjb3VudDogSVVzZXJBY2NvdW50RGF0YWJhc2VTZXJ2aWNlLFxuXHRcdHByaXZhdGUgcmVhZG9ubHkgYW1vdW50RmFjdG9yeTogSUN1cnJlbmN5QW1vdW50RmFjdG9yeVxuXHQpIHtcblx0XHR0aGlzLmlkID0gZGF0YS5pZDtcblx0XHR0aGlzLmNvaW5JZCA9IGRhdGEuY29pbl9pZDtcblx0XHR0aGlzLmRlY2ltYWxBbW91bnQgPSBkYXRhLmNvaW5fZGVjaW1hbF9hbW91bnQ7XG5cblx0XHR0aGlzLnRyYW5zYWN0aW9uSGFzaCA9IGRhdGEudHhfaGFzaDtcblx0XHR0aGlzLmFkZHJlc3MgPSBkYXRhLndpdGhkcmF3X2FkZHJlc3M7XG5cdFx0dGhpcy5tZW1vID0gZGF0YS5tZW1vID8gZGF0YS5tZW1vIDogJyc7XG5cblx0XHR0aGlzLmVvc0FjY291bnROYW1lID0gZGF0YS5lb3NfYWNjb3VudF9uYW1lO1xuXHRcdHRoaXMuZWFzeUFjY291bnRJZCA9IGRhdGEuZWFzeV9hY2NvdW50X2lkO1xuXHRcdHRoaXMuaXNFb3NBY2NvdW50ID0gTnVtYmVyKHRoaXMuZWFzeUFjY291bnRJZCkgPT0gMDtcblx0XHR0aGlzLmVycm9yTWVzc2FnZSA9IGRhdGEuZXJyb3JfbWVzc2FnZTtcblx0fVxuXG5cdGFzeW5jIGdldERhdGFGb3JBcGkoKSB7XG5cdFx0Y29uc3QgYW1vdW50ID0gYXdhaXQgdGhpcy5nZXRBbW91bnQoKTtcblxuXHRcdHJldHVybiB7XG5cdFx0XHRpZDogdGhpcy5pZCxcblx0XHRcdG1lbW86IHRoaXMubWVtbyxcblx0XHRcdGFkZHJlc3M6IHRoaXMuYWRkcmVzcyxcblx0XHRcdGVycm9yTWVzc2FnZTogdGhpcy5lcnJvck1lc3NhZ2UsXG5cdFx0XHRjb2luOiBhbW91bnQuY3VycmVuY3kuc3ltYm9sLFxuXHRcdFx0YW1vdW50OiBhbW91bnQuZGVjaW1hbCxcblx0XHR9O1xuXHR9XG5cblx0YXN5bmMgZ2V0QW1vdW50KCk6IFByb21pc2U8SUN1cnJlbmN5QW1vdW50PiB7XG5cdFx0aWYgKHRoaXMuYW1vdW50ID09IHVuZGVmaW5lZCkge1xuXHRcdFx0dGhpcy5hbW91bnQgPSBhd2FpdCB0aGlzLmFtb3VudEZhY3RvcnkubmV3QW1vdW50RnJvbURlY2ltYWxBbmRDb2luSWQoXG5cdFx0XHRcdHRoaXMuZGVjaW1hbEFtb3VudCxcblx0XHRcdFx0dGhpcy5jb2luSWRcblx0XHRcdCk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHRoaXMuYW1vdW50O1xuXHR9XG5cblx0YXN5bmMgZ2V0UmVmdW5kUGFyYW1zKCk6IFByb21pc2U8SVJlZnVuZFdpdGhkcmF3YWxSZXF1ZXN0UGFyYW1zPiB7XG5cdFx0Y29uc3QgdXNlcklkID0gYXdhaXQgdGhpcy5nZXRVc2VySWQoKTtcblxuXHRcdGNvbnN0IGluZm8gPSBhd2FpdCB0aGlzLmFjY291bnQuZ2V0QWNjb3VudEluZm8odXNlcklkKTtcblxuXHRcdHJldHVybiB7XG5cdFx0XHRyZXF1ZXN0SWQ6IHRoaXMuaWQsXG5cdFx0XHRlYXN5QWNjb3VudFB1YmxpY0tleTogaW5mby5lYXN5X2FjY291bnRfcHVibGljX2tleSA/XG5cdFx0XHRcdFx0XHRcdFx0XHRpbmZvLmVhc3lfYWNjb3VudF9wdWJsaWNfa2V5IDpcblx0XHRcdFx0XHRcdFx0XHRcdCcnLFxuXHRcdFx0cmVhc29uOiB0aGlzLmVycm9yTWVzc2FnZVxuXHRcdH07XG5cdH1cblxuXHRhc3luYyBnZXRVc2VySWQoKTogUHJvbWlzZTxudW1iZXI+IHtcblx0XHRpZiAodGhpcy51c2VySWQgPT0gdW5kZWZpbmVkKSB7XG5cdFx0XHR0aGlzLnVzZXJJZCA9XG5cdFx0XHRcdHRoaXMuaXNFb3NBY2NvdW50ID9cblx0XHRcdFx0XHRhd2FpdCB0aGlzLmFjY291bnQuZ2V0VXNlcklkRm9yRW9zQWNjb3VudChcblx0XHRcdFx0XHRcdHRoaXMuZW9zQWNjb3VudE5hbWVcblx0XHRcdFx0XHQpIDpcblx0XHRcdFx0XHRhd2FpdCB0aGlzLmFjY291bnQuZ2V0VXNlcklkRm9yRWFzeUFjY291bnQoXG5cdFx0XHRcdFx0XHR0aGlzLmVhc3lBY2NvdW50SWRcblx0XHRcdFx0XHQpO1xuXHRcdH1cblxuXHRcdHJldHVybiB0aGlzLnVzZXJJZDtcblx0fVxufVxuIl19