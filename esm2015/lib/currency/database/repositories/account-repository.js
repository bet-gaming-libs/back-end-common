/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/repositories/account-repository.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { MySQLRepository } from '../../../database/engine/mysql/mysql-repository';
/** @type {?} */
const FieldNames = {
    ID: 'id',
    EOS_ACCOUNT: 'eos_account',
    EZEOS_ID: 'ezeos_id',
};
export class AccountRepository extends MySQLRepository {
    /**
     * @param {?} db
     */
    constructor(db) {
        super(db, 'account', [
            FieldNames.ID,
            FieldNames.EOS_ACCOUNT,
            FieldNames.EZEOS_ID
        ]);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC1yZXBvc2l0b3J5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvZGF0YWJhc2UvcmVwb3NpdG9yaWVzL2FjY291bnQtcmVwb3NpdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpREFBaUQsQ0FBQzs7TUFNNUUsVUFBVSxHQUFHO0lBQ2xCLEVBQUUsRUFBRSxJQUFJO0lBQ1IsV0FBVyxFQUFFLGFBQWE7SUFDMUIsUUFBUSxFQUFFLFVBQVU7Q0FDcEI7QUFJRCxNQUFNLE9BQU8saUJBQWtCLFNBQVEsZUFBaUM7Ozs7SUFDdkUsWUFBWSxFQUFrQjtRQUM3QixLQUFLLENBQ0osRUFBRSxFQUNGLFNBQVMsRUFDVDtZQUNDLFVBQVUsQ0FBQyxFQUFFO1lBQ2IsVUFBVSxDQUFDLFdBQVc7WUFDdEIsVUFBVSxDQUFDLFFBQVE7U0FDbkIsQ0FDRCxDQUFDO0lBQ0gsQ0FBQztDQUNEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTXlTUUxSZXBvc2l0b3J5IH0gZnJvbSAnLi4vLi4vLi4vZGF0YWJhc2UvZW5naW5lL215c3FsL215c3FsLXJlcG9zaXRvcnknO1xuaW1wb3J0IHsgSU15U1FMRGF0YWJhc2UgfSBmcm9tICcuLi8uLi8uLi9kYXRhYmFzZS9lbmdpbmUvbXlzcWwvaW50ZXJmYWNlcyc7XG5pbXBvcnQge0lTYXZlZEFjY291bnRSb3d9IGZyb20gJy4uL21vZGVscy9hY2NvdW50JztcblxuXG5cbmNvbnN0IEZpZWxkTmFtZXMgPSB7XG5cdElEOiAnaWQnLFxuXHRFT1NfQUNDT1VOVDogJ2Vvc19hY2NvdW50Jyxcblx0RVpFT1NfSUQ6ICdlemVvc19pZCcsXG59O1xuXG5cblxuZXhwb3J0IGNsYXNzIEFjY291bnRSZXBvc2l0b3J5IGV4dGVuZHMgTXlTUUxSZXBvc2l0b3J5PElTYXZlZEFjY291bnRSb3c+IHtcblx0Y29uc3RydWN0b3IoZGI6IElNeVNRTERhdGFiYXNlKSB7XG5cdFx0c3VwZXIoXG5cdFx0XHRkYixcblx0XHRcdCdhY2NvdW50Jyxcblx0XHRcdFtcblx0XHRcdFx0RmllbGROYW1lcy5JRCxcblx0XHRcdFx0RmllbGROYW1lcy5FT1NfQUNDT1VOVCxcblx0XHRcdFx0RmllbGROYW1lcy5FWkVPU19JRFxuXHRcdFx0XVxuXHRcdCk7XG5cdH1cbn1cbiJdfQ==