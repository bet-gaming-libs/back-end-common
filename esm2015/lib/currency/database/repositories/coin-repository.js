/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/repositories/coin-repository.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { CommonFieldNames } from '../models/common';
import { MySQLRepository } from '../../../database/engine/mysql/mysql-repository';
/** @type {?} */
const FieldNames = {
    SYMBOL: 'symbol',
    PRECISION: 'precision',
    USES_MEMO_FOR_DEPOSITS: 'uses_memo_for_deposits',
    MINIMUM_WITHDRAWAL_AMOUNT: 'minimum_withdrawal_amount'
};
export class CoinRepository extends MySQLRepository {
    /**
     * @param {?} database
     */
    constructor(database) {
        super(database, 'coin', [
            CommonFieldNames.ID,
            FieldNames.SYMBOL,
            FieldNames.PRECISION,
            FieldNames.USES_MEMO_FOR_DEPOSITS,
            FieldNames.MINIMUM_WITHDRAWAL_AMOUNT
        ]);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29pbi1yZXBvc2l0b3J5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvZGF0YWJhc2UvcmVwb3NpdG9yaWVzL2NvaW4tcmVwb3NpdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpREFBaUQsQ0FBQzs7TUFLNUUsVUFBVSxHQUFHO0lBQ2xCLE1BQU0sRUFBRSxRQUFRO0lBQ2hCLFNBQVMsRUFBRSxXQUFXO0lBQ3RCLHNCQUFzQixFQUFFLHdCQUF3QjtJQUNoRCx5QkFBeUIsRUFBRSwyQkFBMkI7Q0FDdEQ7QUFHRCxNQUFNLE9BQU8sY0FBZSxTQUFRLGVBQThCOzs7O0lBQ2pFLFlBQVksUUFBd0I7UUFDbkMsS0FBSyxDQUNKLFFBQVEsRUFDUixNQUFNLEVBQ047WUFDQyxnQkFBZ0IsQ0FBQyxFQUFFO1lBQ25CLFVBQVUsQ0FBQyxNQUFNO1lBQ2pCLFVBQVUsQ0FBQyxTQUFTO1lBQ3BCLFVBQVUsQ0FBQyxzQkFBc0I7WUFDakMsVUFBVSxDQUFDLHlCQUF5QjtTQUNwQyxDQUNELENBQUM7SUFDSCxDQUFDO0NBQ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21tb25GaWVsZE5hbWVzIH0gZnJvbSAnLi4vbW9kZWxzL2NvbW1vbic7XG5pbXBvcnQgeyBNeVNRTFJlcG9zaXRvcnkgfSBmcm9tICcuLi8uLi8uLi9kYXRhYmFzZS9lbmdpbmUvbXlzcWwvbXlzcWwtcmVwb3NpdG9yeSc7XG5pbXBvcnQgeyBJTXlTUUxEYXRhYmFzZSB9IGZyb20gJy4uLy4uLy4uL2RhdGFiYXNlL2VuZ2luZS9teXNxbC9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElTYXZlZENvaW5Sb3cgfSBmcm9tICcuLi9tb2RlbHMvY29pbic7XG5cblxuY29uc3QgRmllbGROYW1lcyA9IHtcblx0U1lNQk9MOiAnc3ltYm9sJyxcblx0UFJFQ0lTSU9OOiAncHJlY2lzaW9uJyxcblx0VVNFU19NRU1PX0ZPUl9ERVBPU0lUUzogJ3VzZXNfbWVtb19mb3JfZGVwb3NpdHMnLFxuXHRNSU5JTVVNX1dJVEhEUkFXQUxfQU1PVU5UOiAnbWluaW11bV93aXRoZHJhd2FsX2Ftb3VudCdcbn07XG5cblxuZXhwb3J0IGNsYXNzIENvaW5SZXBvc2l0b3J5IGV4dGVuZHMgTXlTUUxSZXBvc2l0b3J5PElTYXZlZENvaW5Sb3c+IHtcblx0Y29uc3RydWN0b3IoZGF0YWJhc2U6IElNeVNRTERhdGFiYXNlKSB7XG5cdFx0c3VwZXIoXG5cdFx0XHRkYXRhYmFzZSxcblx0XHRcdCdjb2luJyxcblx0XHRcdFtcblx0XHRcdFx0Q29tbW9uRmllbGROYW1lcy5JRCxcblx0XHRcdFx0RmllbGROYW1lcy5TWU1CT0wsXG5cdFx0XHRcdEZpZWxkTmFtZXMuUFJFQ0lTSU9OLFxuXHRcdFx0XHRGaWVsZE5hbWVzLlVTRVNfTUVNT19GT1JfREVQT1NJVFMsXG5cdFx0XHRcdEZpZWxkTmFtZXMuTUlOSU1VTV9XSVRIRFJBV0FMX0FNT1VOVFxuXHRcdFx0XSxcblx0XHQpO1xuXHR9XG59XG4iXX0=