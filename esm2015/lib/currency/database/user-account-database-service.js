/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/user-account-database-service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { AccountRepository } from './repositories/account-repository';
export class UserAccountDatabaseService {
    /**
     * @param {?} database
     */
    constructor(database) {
        this.database = database;
        this.accountRepository = new AccountRepository(this.database);
    }
    /**
     * @param {?} userId
     * @return {?}
     */
    getAccountInfo(userId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const rows = yield this.database.builder.rawQuery(`
            SELECT
                eos_account AS eos_account_name,
                public_key AS easy_account_public_key

            FROM
                account
                LEFT JOIN account_ezeos ON account.id = account_id

            WHERE
                account.id = ${userId};
        `).execute();
            return rows[0];
        });
    }
    /**
     * @param {?} eos_account
     * @return {?}
     */
    getUserIdForEosAccount(eos_account) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const row = yield this.accountRepository.selectOne({
                whereMap: { eos_account },
            });
            if (row) {
                return row.id;
            }
            else {
                const { insertId } = yield this.accountRepository.insert({ eos_account });
                return insertId;
            }
        });
    }
    /**
     * @param {?} ezeos_id
     * @return {?}
     */
    getUserIdForEasyAccount(ezeos_id) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const rows = yield this.accountRepository.select({
                whereMap: { ezeos_id },
            });
            if (rows.length == 0) {
                throw new Error('User ID NOT FOUND for Easy Account ID: ' + ezeos_id);
            }
            return rows[0].id;
        });
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    UserAccountDatabaseService.prototype.accountRepository;
    /**
     * @type {?}
     * @protected
     */
    UserAccountDatabaseService.prototype.database;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1hY2NvdW50LWRhdGFiYXNlLXNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9jdXJyZW5jeS9kYXRhYmFzZS91c2VyLWFjY291bnQtZGF0YWJhc2Utc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFFQSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUd0RSxNQUFNLE9BQU8sMEJBQTBCOzs7O0lBR3RDLFlBQStCLFFBQXVCO1FBQXZCLGFBQVEsR0FBUixRQUFRLENBQWU7UUFDckQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQy9ELENBQUM7Ozs7O0lBRUssY0FBYyxDQUFDLE1BQWM7OztrQkFDNUIsSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFvQjs7Ozs7Ozs7OzsrQkFVeEMsTUFBTTtTQUM1QixDQUFDLENBQUMsT0FBTyxFQUFFO1lBRWxCLE9BQU8sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2hCLENBQUM7S0FBQTs7Ozs7SUFFSyxzQkFBc0IsQ0FBQyxXQUFtQjs7O2tCQUN6QyxHQUFHLEdBQUcsTUFBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDO2dCQUNsRCxRQUFRLEVBQUUsRUFBRSxXQUFXLEVBQUU7YUFDekIsQ0FBQztZQUVGLElBQUksR0FBRyxFQUFFO2dCQUNSLE9BQU8sR0FBRyxDQUFDLEVBQUUsQ0FBQzthQUNkO2lCQUFNO3NCQUNBLEVBQUUsUUFBUSxFQUFFLEdBQUcsTUFBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLEVBQUUsV0FBVyxFQUFDLENBQUM7Z0JBRXhFLE9BQU8sUUFBUSxDQUFDO2FBQ2hCO1FBQ0YsQ0FBQztLQUFBOzs7OztJQUVLLHVCQUF1QixDQUFDLFFBQWdCOzs7a0JBQ3ZDLElBQUksR0FBRyxNQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUM7Z0JBQ2hELFFBQVEsRUFBRSxFQUFFLFFBQVEsRUFBRTthQUN0QixDQUFDO1lBRUYsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDckIsTUFBTSxJQUFJLEtBQUssQ0FBQyx5Q0FBeUMsR0FBRyxRQUFRLENBQUMsQ0FBQzthQUN0RTtZQUVELE9BQU8sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNuQixDQUFDO0tBQUE7Q0FDRDs7Ozs7O0lBaERBLHVEQUFzRDs7Ozs7SUFFMUMsOENBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTXlTUUxEYXRhYmFzZSB9IGZyb20gJy4uLy4uL2RhdGFiYXNlL2VuZ2luZS9teXNxbC9teXNxbC1kYXRhYmFzZSc7XG5pbXBvcnQgeyBJVXNlckFjY291bnREYXRhYmFzZVNlcnZpY2UsIElVc2VyQWNjb3VudFJvdyB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBBY2NvdW50UmVwb3NpdG9yeSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL2FjY291bnQtcmVwb3NpdG9yeSc7XG5cblxuZXhwb3J0IGNsYXNzIFVzZXJBY2NvdW50RGF0YWJhc2VTZXJ2aWNlIGltcGxlbWVudHMgSVVzZXJBY2NvdW50RGF0YWJhc2VTZXJ2aWNlIHtcblx0cHJpdmF0ZSByZWFkb25seSBhY2NvdW50UmVwb3NpdG9yeTogQWNjb3VudFJlcG9zaXRvcnk7XG5cblx0Y29uc3RydWN0b3IocHJvdGVjdGVkIHJlYWRvbmx5IGRhdGFiYXNlOiBNeVNRTERhdGFiYXNlKSB7XG5cdFx0dGhpcy5hY2NvdW50UmVwb3NpdG9yeSA9IG5ldyBBY2NvdW50UmVwb3NpdG9yeSh0aGlzLmRhdGFiYXNlKTtcblx0fVxuXG5cdGFzeW5jIGdldEFjY291bnRJbmZvKHVzZXJJZDogbnVtYmVyKSB7XG5cdFx0Y29uc3Qgcm93cyA9IGF3YWl0IHRoaXMuZGF0YWJhc2UuYnVpbGRlci5yYXdRdWVyeTxJVXNlckFjY291bnRSb3dbXT4oYFxuICAgICAgICAgICAgU0VMRUNUXG4gICAgICAgICAgICAgICAgZW9zX2FjY291bnQgQVMgZW9zX2FjY291bnRfbmFtZSxcbiAgICAgICAgICAgICAgICBwdWJsaWNfa2V5IEFTIGVhc3lfYWNjb3VudF9wdWJsaWNfa2V5XG5cbiAgICAgICAgICAgIEZST01cbiAgICAgICAgICAgICAgICBhY2NvdW50XG4gICAgICAgICAgICAgICAgTEVGVCBKT0lOIGFjY291bnRfZXplb3MgT04gYWNjb3VudC5pZCA9IGFjY291bnRfaWRcblxuICAgICAgICAgICAgV0hFUkVcbiAgICAgICAgICAgICAgICBhY2NvdW50LmlkID0gJHt1c2VySWR9O1xuICAgICAgICBgKS5leGVjdXRlKCk7XG5cblx0XHRyZXR1cm4gcm93c1swXTtcblx0fVxuXG5cdGFzeW5jIGdldFVzZXJJZEZvckVvc0FjY291bnQoZW9zX2FjY291bnQ6IHN0cmluZyk6IFByb21pc2U8bnVtYmVyPiB7XG5cdFx0Y29uc3Qgcm93ID0gYXdhaXQgdGhpcy5hY2NvdW50UmVwb3NpdG9yeS5zZWxlY3RPbmUoe1xuXHRcdFx0d2hlcmVNYXA6IHsgZW9zX2FjY291bnQgfSxcblx0XHR9KTtcblxuXHRcdGlmIChyb3cpIHtcblx0XHRcdHJldHVybiByb3cuaWQ7XG5cdFx0fSBlbHNlIHtcblx0XHRcdGNvbnN0IHsgaW5zZXJ0SWQgfSA9IGF3YWl0IHRoaXMuYWNjb3VudFJlcG9zaXRvcnkuaW5zZXJ0KHsgZW9zX2FjY291bnR9KTtcblxuXHRcdFx0cmV0dXJuIGluc2VydElkO1xuXHRcdH1cblx0fVxuXG5cdGFzeW5jIGdldFVzZXJJZEZvckVhc3lBY2NvdW50KGV6ZW9zX2lkOiBzdHJpbmcpOiBQcm9taXNlPG51bWJlcj4ge1xuXHRcdGNvbnN0IHJvd3MgPSBhd2FpdCB0aGlzLmFjY291bnRSZXBvc2l0b3J5LnNlbGVjdCh7XG5cdFx0XHR3aGVyZU1hcDogeyBlemVvc19pZCB9LFxuXHRcdH0pO1xuXG5cdFx0aWYgKHJvd3MubGVuZ3RoID09IDApIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignVXNlciBJRCBOT1QgRk9VTkQgZm9yIEVhc3kgQWNjb3VudCBJRDogJyArIGV6ZW9zX2lkKTtcblx0XHR9XG5cblx0XHRyZXR1cm4gcm93c1swXS5pZDtcblx0fVxufVxuIl19