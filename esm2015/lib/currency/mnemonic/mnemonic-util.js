/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/mnemonic/mnemonic-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import * as bs58 from 'bs58';
import * as bip39 from 'bip39';
import * as bip32 from 'bip32';
import * as bitcoin from 'bitcoinjs-lib';
import { CoinId } from '../database/coins';
class MnemonicUtility {
    /**
     * @param {?} _words
     * @param {?} network
     */
    constructor(_words, network) {
        this._words = _words;
        this.network = network;
        this.isInit = false;
        this.root = undefined;
    }
    /**
     * @return {?}
     */
    get words() {
        return this._words;
    }
    /**
     * @return {?}
     */
    init() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this.isInit) {
                return;
            }
            this.isInit = true;
            /** @type {?} */
            const seed = yield bip39.mnemonicToSeed(this.words);
            this.root = bip32.fromSeed(seed, this.network);
        });
    }
    /**
     * @param {?} coinId
     * @return {?}
     */
    getNodeForChangeAddress(coinId) {
        /** @type {?} */
        let basePath = getBasePath(coinId);
        // console.log(basePath);
        switch (coinId) {
            case CoinId.BTC:
            case CoinId.LTC:
            case CoinId.BCH:
                basePath = basePath.substr(0, basePath.length - 2) + '1/';
                break;
        }
        /** @type {?} */
        const path = basePath + '0';
        // console.log(path);
        /** @type {?} */
        const node = this.root.derivePath(path);
        return node;
    }
    /**
     * @param {?} coinId
     * @param {?} userId
     * @return {?}
     */
    getNodeForDepositAddress(coinId, userId) {
        /** @type {?} */
        const path = getBasePath(coinId) + userId;
        return this.root.derivePath(path);
    }
    /**
     * @param {?} userId
     * @param {?} coinId
     * @param {?} prefix
     * @return {?}
     */
    getDepositMemo(userId, coinId, prefix) {
        /** @type {?} */
        const node = this.getNodeForDepositAddress(coinId, userId);
        /** @type {?} */
        const memo = bs58.encode(node.publicKey.slice(0, 12)).toLowerCase();
        return `${prefix}${memo}`;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MnemonicUtility.prototype.isInit;
    /**
     * @type {?}
     * @private
     */
    MnemonicUtility.prototype.root;
    /**
     * @type {?}
     * @private
     */
    MnemonicUtility.prototype._words;
    /**
     * @type {?}
     * @private
     */
    MnemonicUtility.prototype.network;
}
/**
 * the MnemonicUtility class should be a singleton: **
 * @type {?}
 */
let mnemonicUtility;
/** @type {?} */
const allowedNetworkNames = ['mainnet', 'testnet', 'regtest'];
/**
 * @param {?} config
 * @return {?}
 * @this {*}
 */
export function getMnemonicUtil(config) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        if (!mnemonicUtility) {
            /** @type {?} */
            const words = config.mnemonicPhrase;
            /** @type {?} */
            const networkName = config.blockchainNetwork;
            if (allowedNetworkNames.indexOf(networkName) < 0) {
                throw new Error(`${networkName} network not supported.`);
            }
            mnemonicUtility = new MnemonicUtility(words, bitcoin.networks[networkName]);
            yield mnemonicUtility.init();
        }
        return mnemonicUtility;
    });
}
/**
 *
 * @param {?} coinId
 * @return {?}
 */
function getBasePath(coinId) {
    switch (coinId) {
        case CoinId.BTC:
            return 'm/49\'/0\'/0\'/0/';
        case CoinId.LTC:
            return 'm/49\'/2\'/0\'/0/';
        case CoinId.BCH:
            return 'm/44\'/145\'/0\'/0/';
        case CoinId.ETH:
            return 'm/44\'/60\'/0\'/0/';
        case CoinId.BNB:
        case CoinId.BET_BINANCE:
            // https://docs.binance.org/blockchain.html
            // derive the private key using BIP32 / BIP44 with HD prefix as "44'/714'/", which is reserved at SLIP 44.
            // 714 comes from Binance's birthday, July 14th
            return `m/44'/714'/0'/0/`;
        case CoinId.XRP:
            return `m/44'/144'/0'/0/`;
        case CoinId.TRX:
            return `m/44'/195'/0'/0/`;
        case CoinId.EOS:
            return `m/44'/194'/0'/0/`;
        default:
            throw new Error('CoinId NOT SUPPORTED: ' + coinId);
    }
}
/**
 * @return {?}
 */
export function generateMnemonicPhrase() {
    return bip39.generateMnemonic(256);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW5lbW9uaWMtdXRpbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2N1cnJlbmN5L21uZW1vbmljL21uZW1vbmljLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxLQUFLLElBQUksTUFBTSxNQUFNLENBQUM7QUFDN0IsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE9BQU8sTUFBTSxlQUFlLENBQUM7QUFJekMsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRzNDLE1BQU0sZUFBZTs7Ozs7SUFJcEIsWUFDUyxNQUFjLEVBQ2QsT0FBd0I7UUFEeEIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLFlBQU8sR0FBUCxPQUFPLENBQWlCO1FBTHpCLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFDZixTQUFJLEdBQXlCLFNBQVMsQ0FBQztJQU0vQyxDQUFDOzs7O0lBRUQsSUFBSSxLQUFLO1FBQ1IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3BCLENBQUM7Ozs7SUFFSyxJQUFJOztZQUNULElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDaEIsT0FBTzthQUNQO1lBRUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7O2tCQUViLElBQUksR0FBRyxNQUFNLEtBQUssQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUVuRCxJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNoRCxDQUFDO0tBQUE7Ozs7O0lBR0QsdUJBQXVCLENBQUMsTUFBYzs7WUFDakMsUUFBUSxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUM7UUFDbEMseUJBQXlCO1FBRXpCLFFBQVEsTUFBTSxFQUFFO1lBQ2YsS0FBSyxNQUFNLENBQUMsR0FBRyxDQUFDO1lBQ2hCLEtBQUssTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUNoQixLQUFLLE1BQU0sQ0FBQyxHQUFHO2dCQUNkLFFBQVEsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQztnQkFDMUQsTUFBTTtTQUNQOztjQUVLLElBQUksR0FBRyxRQUFRLEdBQUcsR0FBRzs7O2NBSXJCLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7UUFFdkMsT0FBTyxJQUFJLENBQUM7SUFDYixDQUFDOzs7Ozs7SUFFRCx3QkFBd0IsQ0FBQyxNQUFjLEVBQUUsTUFBYzs7Y0FDaEQsSUFBSSxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUMsR0FBRyxNQUFNO1FBRXpDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7Ozs7OztJQUVELGNBQWMsQ0FBQyxNQUFjLEVBQUUsTUFBYyxFQUFFLE1BQWtCOztjQUMxRCxJQUFJLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUM7O2NBRXBELElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRTtRQUVuRSxPQUFPLEdBQUcsTUFBTSxHQUFHLElBQUksRUFBRSxDQUFDO0lBQzNCLENBQUM7Q0FDRDs7Ozs7O0lBNURBLGlDQUF1Qjs7Ozs7SUFDdkIsK0JBQStDOzs7OztJQUc5QyxpQ0FBc0I7Ozs7O0lBQ3RCLGtDQUFnQzs7Ozs7O0lBMkQ5QixlQUFnQzs7TUFFOUIsbUJBQW1CLEdBQUcsQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQzs7Ozs7O0FBRTdELE1BQU0sVUFBZ0IsZUFBZSxDQUFDLE1BQThCOztRQUNuRSxJQUFJLENBQUMsZUFBZSxFQUFFOztrQkFDZixLQUFLLEdBQUcsTUFBTSxDQUFDLGNBQWM7O2tCQUM3QixXQUFXLEdBQUcsTUFBTSxDQUFDLGlCQUFpQjtZQUU1QyxJQUFJLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ2pELE1BQU0sSUFBSSxLQUFLLENBQUMsR0FBRyxXQUFXLHlCQUF5QixDQUFDLENBQUM7YUFDekQ7WUFFRCxlQUFlLEdBQUcsSUFBSSxlQUFlLENBQ3BDLEtBQUssRUFDTCxPQUFPLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUM3QixDQUFDO1lBRUYsTUFBTSxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDN0I7UUFFRCxPQUFPLGVBQWUsQ0FBQztJQUN4QixDQUFDO0NBQUE7Ozs7OztBQUtELFNBQVMsV0FBVyxDQUFDLE1BQWM7SUFDbEMsUUFBUSxNQUFNLEVBQUU7UUFDZixLQUFLLE1BQU0sQ0FBQyxHQUFHO1lBQ2QsT0FBTyxtQkFBbUIsQ0FBQztRQUU1QixLQUFLLE1BQU0sQ0FBQyxHQUFHO1lBQ2QsT0FBTyxtQkFBbUIsQ0FBQztRQUU1QixLQUFLLE1BQU0sQ0FBQyxHQUFHO1lBQ2QsT0FBTyxxQkFBcUIsQ0FBQztRQUU5QixLQUFLLE1BQU0sQ0FBQyxHQUFHO1lBQ2QsT0FBTyxvQkFBb0IsQ0FBQztRQUU3QixLQUFLLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDaEIsS0FBSyxNQUFNLENBQUMsV0FBVztZQUN0QiwyQ0FBMkM7WUFDM0MsMEdBQTBHO1lBQzFHLCtDQUErQztZQUMvQyxPQUFPLGtCQUFrQixDQUFDO1FBRTNCLEtBQUssTUFBTSxDQUFDLEdBQUc7WUFDZCxPQUFPLGtCQUFrQixDQUFDO1FBRTNCLEtBQUssTUFBTSxDQUFDLEdBQUc7WUFDZCxPQUFPLGtCQUFrQixDQUFDO1FBRTNCLEtBQUssTUFBTSxDQUFDLEdBQUc7WUFDZCxPQUFPLGtCQUFrQixDQUFDO1FBRTNCO1lBQ0MsTUFBTSxJQUFJLEtBQUssQ0FBQyx3QkFBd0IsR0FBRyxNQUFNLENBQUMsQ0FBQztLQUNwRDtBQUNGLENBQUM7Ozs7QUFHRCxNQUFNLFVBQVUsc0JBQXNCO0lBQ3JDLE9BQU8sS0FBSyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ3BDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBiczU4IGZyb20gJ2JzNTgnO1xuaW1wb3J0ICogYXMgYmlwMzkgZnJvbSAnYmlwMzknO1xuaW1wb3J0ICogYXMgYmlwMzIgZnJvbSAnYmlwMzInO1xuaW1wb3J0ICogYXMgYml0Y29pbiBmcm9tICdiaXRjb2luanMtbGliJztcblxuXG5pbXBvcnQgeyBJTW5lbW9uaWNVdGlsaXR5LCBNZW1vUHJlZml4LCBJTW5lbW9uaWNVdGlsaXR5Q29uZmlnIH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7IENvaW5JZCB9IGZyb20gJy4uL2RhdGFiYXNlL2NvaW5zJztcblxuXG5jbGFzcyBNbmVtb25pY1V0aWxpdHkgaW1wbGVtZW50cyBJTW5lbW9uaWNVdGlsaXR5IHtcblx0cHJpdmF0ZSBpc0luaXQgPSBmYWxzZTtcblx0cHJpdmF0ZSByb290OiBiaXAzMi5CSVAzMkludGVyZmFjZSA9IHVuZGVmaW5lZDtcblxuXHRjb25zdHJ1Y3Rvcihcblx0XHRwcml2YXRlIF93b3Jkczogc3RyaW5nLFxuXHRcdHByaXZhdGUgbmV0d29yazogYml0Y29pbi5OZXR3b3JrXG5cdCkge1xuXHR9XG5cblx0Z2V0IHdvcmRzKCkge1xuXHRcdHJldHVybiB0aGlzLl93b3Jkcztcblx0fVxuXG5cdGFzeW5jIGluaXQoKSB7XG5cdFx0aWYgKHRoaXMuaXNJbml0KSB7XG5cdFx0XHRyZXR1cm47XG5cdFx0fVxuXG5cdFx0dGhpcy5pc0luaXQgPSB0cnVlO1xuXG5cdFx0Y29uc3Qgc2VlZCA9IGF3YWl0IGJpcDM5Lm1uZW1vbmljVG9TZWVkKHRoaXMud29yZHMpO1xuXG5cdFx0dGhpcy5yb290ID0gYmlwMzIuZnJvbVNlZWQoc2VlZCwgdGhpcy5uZXR3b3JrKTtcblx0fVxuXG5cblx0Z2V0Tm9kZUZvckNoYW5nZUFkZHJlc3MoY29pbklkOiBDb2luSWQpOiBiaXAzMi5CSVAzMkludGVyZmFjZSB7XG5cdFx0bGV0IGJhc2VQYXRoID0gZ2V0QmFzZVBhdGgoY29pbklkKTtcblx0XHQvLyBjb25zb2xlLmxvZyhiYXNlUGF0aCk7XG5cblx0XHRzd2l0Y2ggKGNvaW5JZCkge1xuXHRcdFx0Y2FzZSBDb2luSWQuQlRDOlxuXHRcdFx0Y2FzZSBDb2luSWQuTFRDOlxuXHRcdFx0Y2FzZSBDb2luSWQuQkNIOlxuXHRcdFx0XHRiYXNlUGF0aCA9IGJhc2VQYXRoLnN1YnN0cigwLCBiYXNlUGF0aC5sZW5ndGggLSAyKSArICcxLyc7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdH1cblxuXHRcdGNvbnN0IHBhdGggPSBiYXNlUGF0aCArICcwJztcblx0XHQvLyBjb25zb2xlLmxvZyhwYXRoKTtcblxuXG5cdFx0Y29uc3Qgbm9kZSA9IHRoaXMucm9vdC5kZXJpdmVQYXRoKHBhdGgpO1xuXG5cdFx0cmV0dXJuIG5vZGU7XG5cdH1cblxuXHRnZXROb2RlRm9yRGVwb3NpdEFkZHJlc3MoY29pbklkOiBDb2luSWQsIHVzZXJJZDogbnVtYmVyKTogYmlwMzIuQklQMzJJbnRlcmZhY2Uge1xuXHRcdGNvbnN0IHBhdGggPSBnZXRCYXNlUGF0aChjb2luSWQpICsgdXNlcklkO1xuXG5cdFx0cmV0dXJuIHRoaXMucm9vdC5kZXJpdmVQYXRoKHBhdGgpO1xuXHR9XG5cblx0Z2V0RGVwb3NpdE1lbW8odXNlcklkOiBudW1iZXIsIGNvaW5JZDogQ29pbklkLCBwcmVmaXg6IE1lbW9QcmVmaXgpOiBzdHJpbmcge1xuXHRcdGNvbnN0IG5vZGUgPSB0aGlzLmdldE5vZGVGb3JEZXBvc2l0QWRkcmVzcyhjb2luSWQsIHVzZXJJZCk7XG5cblx0XHRjb25zdCBtZW1vID0gYnM1OC5lbmNvZGUobm9kZS5wdWJsaWNLZXkuc2xpY2UoMCwgMTIpKS50b0xvd2VyQ2FzZSgpO1xuXG5cdFx0cmV0dXJuIGAke3ByZWZpeH0ke21lbW99YDtcblx0fVxufVxuXG5cbi8qKiogdGhlIE1uZW1vbmljVXRpbGl0eSBjbGFzcyBzaG91bGQgYmUgYSBzaW5nbGV0b246ICoqKi9cbmxldCBtbmVtb25pY1V0aWxpdHk6IE1uZW1vbmljVXRpbGl0eTtcblxuY29uc3QgYWxsb3dlZE5ldHdvcmtOYW1lcyA9IFsnbWFpbm5ldCcsICd0ZXN0bmV0JywgJ3JlZ3Rlc3QnXTtcblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldE1uZW1vbmljVXRpbChjb25maWc6IElNbmVtb25pY1V0aWxpdHlDb25maWcpOiBQcm9taXNlPElNbmVtb25pY1V0aWxpdHk+IHtcblx0aWYgKCFtbmVtb25pY1V0aWxpdHkpIHtcblx0XHRjb25zdCB3b3JkcyA9IGNvbmZpZy5tbmVtb25pY1BocmFzZTtcblx0XHRjb25zdCBuZXR3b3JrTmFtZSA9IGNvbmZpZy5ibG9ja2NoYWluTmV0d29yaztcblxuXHRcdGlmIChhbGxvd2VkTmV0d29ya05hbWVzLmluZGV4T2YobmV0d29ya05hbWUpIDwgMCkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKGAke25ldHdvcmtOYW1lfSBuZXR3b3JrIG5vdCBzdXBwb3J0ZWQuYCk7XG5cdFx0fVxuXG5cdFx0bW5lbW9uaWNVdGlsaXR5ID0gbmV3IE1uZW1vbmljVXRpbGl0eShcblx0XHRcdHdvcmRzLFxuXHRcdFx0Yml0Y29pbi5uZXR3b3Jrc1tuZXR3b3JrTmFtZV1cblx0XHQpO1xuXG5cdFx0YXdhaXQgbW5lbW9uaWNVdGlsaXR5LmluaXQoKTtcblx0fVxuXG5cdHJldHVybiBtbmVtb25pY1V0aWxpdHk7XG59XG4vKioqL1xuXG5cblxuZnVuY3Rpb24gZ2V0QmFzZVBhdGgoY29pbklkOiBDb2luSWQpOiBzdHJpbmcge1xuXHRzd2l0Y2ggKGNvaW5JZCkge1xuXHRcdGNhc2UgQ29pbklkLkJUQzpcblx0XHRcdHJldHVybiAnbS80OVxcJy8wXFwnLzBcXCcvMC8nO1xuXG5cdFx0Y2FzZSBDb2luSWQuTFRDOlxuXHRcdFx0cmV0dXJuICdtLzQ5XFwnLzJcXCcvMFxcJy8wLyc7XG5cblx0XHRjYXNlIENvaW5JZC5CQ0g6XG5cdFx0XHRyZXR1cm4gJ20vNDRcXCcvMTQ1XFwnLzBcXCcvMC8nO1xuXG5cdFx0Y2FzZSBDb2luSWQuRVRIOlxuXHRcdFx0cmV0dXJuICdtLzQ0XFwnLzYwXFwnLzBcXCcvMC8nO1xuXG5cdFx0Y2FzZSBDb2luSWQuQk5COlxuXHRcdGNhc2UgQ29pbklkLkJFVF9CSU5BTkNFOlxuXHRcdFx0Ly8gaHR0cHM6Ly9kb2NzLmJpbmFuY2Uub3JnL2Jsb2NrY2hhaW4uaHRtbFxuXHRcdFx0Ly8gZGVyaXZlIHRoZSBwcml2YXRlIGtleSB1c2luZyBCSVAzMiAvIEJJUDQ0IHdpdGggSEQgcHJlZml4IGFzIFwiNDQnLzcxNCcvXCIsIHdoaWNoIGlzIHJlc2VydmVkIGF0IFNMSVAgNDQuXG5cdFx0XHQvLyA3MTQgY29tZXMgZnJvbSBCaW5hbmNlJ3MgYmlydGhkYXksIEp1bHkgMTR0aFxuXHRcdFx0cmV0dXJuIGBtLzQ0Jy83MTQnLzAnLzAvYDtcblxuXHRcdGNhc2UgQ29pbklkLlhSUDpcblx0XHRcdHJldHVybiBgbS80NCcvMTQ0Jy8wJy8wL2A7XG5cblx0XHRjYXNlIENvaW5JZC5UUlg6XG5cdFx0XHRyZXR1cm4gYG0vNDQnLzE5NScvMCcvMC9gO1xuXG5cdFx0Y2FzZSBDb2luSWQuRU9TOlxuXHRcdFx0cmV0dXJuIGBtLzQ0Jy8xOTQnLzAnLzAvYDtcblxuXHRcdGRlZmF1bHQ6XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ0NvaW5JZCBOT1QgU1VQUE9SVEVEOiAnICsgY29pbklkKTtcblx0fVxufVxuXG5cbmV4cG9ydCBmdW5jdGlvbiBnZW5lcmF0ZU1uZW1vbmljUGhyYXNlKCkge1xuXHRyZXR1cm4gYmlwMzkuZ2VuZXJhdGVNbmVtb25pYygyNTYpO1xufVxuXG5cblxuLypcblxuXG5cblBBVEhTOlxuXG5CVENcblxuTFRDXG5cIm0vNDQnLzInLzAnLzAvXCJcblxuXG5cbkJDSFxuYG0vNDQnLzE0NScvMCcvMC9gXG5cblxuRVRIXG5gbS80NCcvNjAnLzAnLzAvYFxuXG5cblxuXG5YUlBcbmBtLzQ0Jy8xNDQnLzAnLzAvYFxuXG5CRVQgKEJpbmFuY2UpXG5gbS80NCcvNzE0Jy8wJy8wL2BcblxuXG4qL1xuIl19