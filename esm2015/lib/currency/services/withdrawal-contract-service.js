/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/services/withdrawal-contract-service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { EosTransactionExecutor } from '../../eos/eos-txn-executor';
import { isWaxTxnIrreversible } from '../../eos/irreversible';
/**
 * @template T
 */
export class WithdrawalContractService {
    /**
     * @param {?} eosUtility
     */
    constructor(eosUtility) {
        this.eosUtility = eosUtility;
        this.contractName = this.contracts.withdrawal;
    }
    /**
     * @return {?}
     */
    getPendingRequests() {
        return this.eosUtility.getTableRows({
            code: this.contractName,
            scope: this.contractName,
            table: 'withdraws',
            limit: 1000
        });
    }
    /**
     * @param {?} requestId
     * @param {?} txnId
     * @return {?}
     */
    processRequest(requestId, txnId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const executor = new EosTransactionExecutor([{
                    account: this.contractName,
                    name: 'process',
                    data: {
                        withdraw_id: requestId,
                        memo: txnId
                    }
                }], this.eosUtility, isWaxTxnIrreversible);
            /**
             * save this in DB? **
             * @type {?}
             */
            const broadcastedTxnId = yield executor.execute();
            /** @type {?} */
            const confirmedTxnId = yield executor.confirm();
            /*** save this in DB? ***/
            return confirmedTxnId;
        });
    }
    /**
     * @param {?} request
     * @return {?}
     */
    refundRequest(request) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const params = yield request.getRefundParams();
            const { requestId, easyAccountPublicKey, reason } = params;
            /** @type {?} */
            const data = {
                withdraw_id: requestId,
                ezacct_deposit_memo: easyAccountPublicKey,
                memo: reason
            };
            /** @type {?} */
            const executor = new EosTransactionExecutor([{
                    account: this.contractName,
                    name: 'refund',
                    data
                }], this.eosUtility, isWaxTxnIrreversible);
            /**
             * save this in DB? **
             * @type {?}
             */
            const broadcastedTxnId = yield executor.execute();
            /** @type {?} */
            const confirmedTxnId = yield executor.confirm();
            /*** save this in DB? ***/
            return confirmedTxnId;
        });
    }
    /**
     * @private
     * @return {?}
     */
    get contracts() {
        return this.eosUtility.contracts;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    WithdrawalContractService.prototype.contractName;
    /**
     * @type {?}
     * @private
     */
    WithdrawalContractService.prototype.eosUtility;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2l0aGRyYXdhbC1jb250cmFjdC1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvc2VydmljZXMvd2l0aGRyYXdhbC1jb250cmFjdC1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUVBLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDOzs7O0FBSzlELE1BQU0sT0FBTyx5QkFBeUI7Ozs7SUFHckMsWUFBNkIsVUFBMEI7UUFBMUIsZUFBVSxHQUFWLFVBQVUsQ0FBZ0I7UUFDdEQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQztJQUMvQyxDQUFDOzs7O0lBRUQsa0JBQWtCO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQXlCO1lBQzNELElBQUksRUFBRSxJQUFJLENBQUMsWUFBWTtZQUN2QixLQUFLLEVBQUUsSUFBSSxDQUFDLFlBQVk7WUFDeEIsS0FBSyxFQUFFLFdBQVc7WUFDbEIsS0FBSyxFQUFFLElBQUk7U0FDWCxDQUFDLENBQUM7SUFDSixDQUFDOzs7Ozs7SUFFSyxjQUFjLENBQUMsU0FBaUIsRUFBRSxLQUFhOzs7a0JBQzlDLFFBQVEsR0FBRyxJQUFJLHNCQUFzQixDQUMxQyxDQUFDO29CQUNBLE9BQU8sRUFBRSxJQUFJLENBQUMsWUFBWTtvQkFDMUIsSUFBSSxFQUFFLFNBQVM7b0JBQ2YsSUFBSSxFQUFFO3dCQUNMLFdBQVcsRUFBRSxTQUFTO3dCQUN0QixJQUFJLEVBQUUsS0FBSztxQkFDWDtpQkFDRCxDQUFDLEVBQ0YsSUFBSSxDQUFDLFVBQVUsRUFDZixvQkFBb0IsQ0FDcEI7Ozs7O2tCQUdLLGdCQUFnQixHQUFHLE1BQU0sUUFBUSxDQUFDLE9BQU8sRUFBRTs7a0JBRTNDLGNBQWMsR0FBRyxNQUFNLFFBQVEsQ0FBQyxPQUFPLEVBQUU7WUFFL0MsMEJBQTBCO1lBQzFCLE9BQU8sY0FBYyxDQUFDO1FBQ3ZCLENBQUM7S0FBQTs7Ozs7SUFFSyxhQUFhLENBQUMsT0FBZ0M7OztrQkFDN0MsTUFBTSxHQUFHLE1BQU0sT0FBTyxDQUFDLGVBQWUsRUFBRTtrQkFFeEMsRUFBQyxTQUFTLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxFQUFDLEdBQUcsTUFBTTs7a0JBRWxELElBQUksR0FBRztnQkFDWixXQUFXLEVBQUUsU0FBUztnQkFDdEIsbUJBQW1CLEVBQUUsb0JBQW9CO2dCQUN6QyxJQUFJLEVBQUUsTUFBTTthQUNaOztrQkFFSyxRQUFRLEdBQUcsSUFBSSxzQkFBc0IsQ0FDMUMsQ0FBQztvQkFDQSxPQUFPLEVBQUUsSUFBSSxDQUFDLFlBQVk7b0JBQzFCLElBQUksRUFBRSxRQUFRO29CQUNkLElBQUk7aUJBQ0osQ0FBQyxFQUNGLElBQUksQ0FBQyxVQUFVLEVBQ2Ysb0JBQW9CLENBQ3BCOzs7OztrQkFHSyxnQkFBZ0IsR0FBRyxNQUFNLFFBQVEsQ0FBQyxPQUFPLEVBQUU7O2tCQUUzQyxjQUFjLEdBQUcsTUFBTSxRQUFRLENBQUMsT0FBTyxFQUFFO1lBRS9DLDBCQUEwQjtZQUMxQixPQUFPLGNBQWMsQ0FBQztRQUN2QixDQUFDO0tBQUE7Ozs7O0lBR0QsSUFBWSxTQUFTO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUM7SUFDbEMsQ0FBQztDQUNEOzs7Ozs7SUF4RUEsaURBQXNDOzs7OztJQUUxQiwrQ0FBMkMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJV2l0aGRyYXdhbENvbnRyYWN0U2VydmljZSwgSVdpdGhkcmF3YWxSZXF1ZXN0RGF0YSB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5pbXBvcnQge0lFb3NVdGlsaXR5fSBmcm9tICcuLi8uLi9lb3MvaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBFb3NUcmFuc2FjdGlvbkV4ZWN1dG9yIH0gZnJvbSAnLi4vLi4vZW9zL2Vvcy10eG4tZXhlY3V0b3InO1xuaW1wb3J0IHsgaXNXYXhUeG5JcnJldmVyc2libGUgfSBmcm9tICcuLi8uLi9lb3MvaXJyZXZlcnNpYmxlJztcbmltcG9ydCB7IElFb3NBY2NvdW50TmFtZXNCYXNlIH0gZnJvbSAnLi4vLi4vZW9zL2Vvcy1zZXR0aW5ncyc7XG5pbXBvcnQgeyBJU2F2ZWRXaXRoZHJhd2FsUmVxdWVzdCB9IGZyb20gJy4uL2RhdGFiYXNlL21vZGVscy93aXRoZHJhd2FsLXJlcXVlc3QnO1xuXG5cbmV4cG9ydCBjbGFzcyBXaXRoZHJhd2FsQ29udHJhY3RTZXJ2aWNlPFQgZXh0ZW5kcyBJRW9zQWNjb3VudE5hbWVzQmFzZT4gaW1wbGVtZW50cyBJV2l0aGRyYXdhbENvbnRyYWN0U2VydmljZSB7XG5cdHByaXZhdGUgcmVhZG9ubHkgY29udHJhY3ROYW1lOiBzdHJpbmc7XG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSByZWFkb25seSBlb3NVdGlsaXR5OiBJRW9zVXRpbGl0eTxUPikge1xuXHRcdHRoaXMuY29udHJhY3ROYW1lID0gdGhpcy5jb250cmFjdHMud2l0aGRyYXdhbDtcblx0fVxuXG5cdGdldFBlbmRpbmdSZXF1ZXN0cygpOiBQcm9taXNlPElXaXRoZHJhd2FsUmVxdWVzdERhdGFbXT4ge1xuXHRcdHJldHVybiB0aGlzLmVvc1V0aWxpdHkuZ2V0VGFibGVSb3dzPElXaXRoZHJhd2FsUmVxdWVzdERhdGE+KHtcblx0XHRcdGNvZGU6IHRoaXMuY29udHJhY3ROYW1lLFxuXHRcdFx0c2NvcGU6IHRoaXMuY29udHJhY3ROYW1lLFxuXHRcdFx0dGFibGU6ICd3aXRoZHJhd3MnLFxuXHRcdFx0bGltaXQ6IDEwMDBcblx0XHR9KTtcblx0fVxuXG5cdGFzeW5jIHByb2Nlc3NSZXF1ZXN0KHJlcXVlc3RJZDogc3RyaW5nLCB0eG5JZDogc3RyaW5nKSB7XG5cdFx0Y29uc3QgZXhlY3V0b3IgPSBuZXcgRW9zVHJhbnNhY3Rpb25FeGVjdXRvcihcblx0XHRcdFt7XG5cdFx0XHRcdGFjY291bnQ6IHRoaXMuY29udHJhY3ROYW1lLFxuXHRcdFx0XHRuYW1lOiAncHJvY2VzcycsXG5cdFx0XHRcdGRhdGE6IHtcblx0XHRcdFx0XHR3aXRoZHJhd19pZDogcmVxdWVzdElkLFxuXHRcdFx0XHRcdG1lbW86IHR4bklkXG5cdFx0XHRcdH1cblx0XHRcdH1dLFxuXHRcdFx0dGhpcy5lb3NVdGlsaXR5LFxuXHRcdFx0aXNXYXhUeG5JcnJldmVyc2libGVcblx0XHQpO1xuXG5cdFx0LyoqKiBzYXZlIHRoaXMgaW4gREI/ICoqKi9cblx0XHRjb25zdCBicm9hZGNhc3RlZFR4bklkID0gYXdhaXQgZXhlY3V0b3IuZXhlY3V0ZSgpO1xuXG5cdFx0Y29uc3QgY29uZmlybWVkVHhuSWQgPSBhd2FpdCBleGVjdXRvci5jb25maXJtKCk7XG5cblx0XHQvKioqIHNhdmUgdGhpcyBpbiBEQj8gKioqL1xuXHRcdHJldHVybiBjb25maXJtZWRUeG5JZDtcblx0fVxuXG5cdGFzeW5jIHJlZnVuZFJlcXVlc3QocmVxdWVzdDogSVNhdmVkV2l0aGRyYXdhbFJlcXVlc3QpOiBQcm9taXNlPHN0cmluZz4ge1xuXHRcdGNvbnN0IHBhcmFtcyA9IGF3YWl0IHJlcXVlc3QuZ2V0UmVmdW5kUGFyYW1zKCk7XG5cblx0XHRjb25zdCB7cmVxdWVzdElkLCBlYXN5QWNjb3VudFB1YmxpY0tleSwgcmVhc29ufSA9IHBhcmFtcztcblxuXHRcdGNvbnN0IGRhdGEgPSB7XG5cdFx0XHR3aXRoZHJhd19pZDogcmVxdWVzdElkLFxuXHRcdFx0ZXphY2N0X2RlcG9zaXRfbWVtbzogZWFzeUFjY291bnRQdWJsaWNLZXksXG5cdFx0XHRtZW1vOiByZWFzb25cblx0XHR9O1xuXG5cdFx0Y29uc3QgZXhlY3V0b3IgPSBuZXcgRW9zVHJhbnNhY3Rpb25FeGVjdXRvcihcblx0XHRcdFt7XG5cdFx0XHRcdGFjY291bnQ6IHRoaXMuY29udHJhY3ROYW1lLFxuXHRcdFx0XHRuYW1lOiAncmVmdW5kJyxcblx0XHRcdFx0ZGF0YVxuXHRcdFx0fV0sXG5cdFx0XHR0aGlzLmVvc1V0aWxpdHksXG5cdFx0XHRpc1dheFR4bklycmV2ZXJzaWJsZVxuXHRcdCk7XG5cblx0XHQvKioqIHNhdmUgdGhpcyBpbiBEQj8gKioqL1xuXHRcdGNvbnN0IGJyb2FkY2FzdGVkVHhuSWQgPSBhd2FpdCBleGVjdXRvci5leGVjdXRlKCk7XG5cblx0XHRjb25zdCBjb25maXJtZWRUeG5JZCA9IGF3YWl0IGV4ZWN1dG9yLmNvbmZpcm0oKTtcblxuXHRcdC8qKiogc2F2ZSB0aGlzIGluIERCPyAqKiovXG5cdFx0cmV0dXJuIGNvbmZpcm1lZFR4bklkO1xuXHR9XG5cblxuXHRwcml2YXRlIGdldCBjb250cmFjdHMoKSB7XG5cdFx0cmV0dXJuIHRoaXMuZW9zVXRpbGl0eS5jb250cmFjdHM7XG5cdH1cbn1cbiJdfQ==