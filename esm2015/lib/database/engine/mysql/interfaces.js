/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IMySQLConfig() { }
if (false) {
    /** @type {?} */
    IMySQLConfig.prototype.host;
    /** @type {?|undefined} */
    IMySQLConfig.prototype.port;
    /** @type {?} */
    IMySQLConfig.prototype.user;
    /** @type {?} */
    IMySQLConfig.prototype.password;
    /** @type {?} */
    IMySQLConfig.prototype.database;
    /** @type {?} */
    IMySQLConfig.prototype.charset;
}
/**
 * @record
 */
export function IMySQLRepository() { }
if (false) {
    /** @type {?} */
    IMySQLRepository.prototype.tableName;
    /** @type {?} */
    IMySQLRepository.prototype.primaryKey;
    /** @type {?} */
    IMySQLRepository.prototype.isPrimaryKeyAString;
}
/**
 * @record
 */
export function IMySQLQueryResult() { }
if (false) {
    /** @type {?} */
    IMySQLQueryResult.prototype.fieldCount;
    /** @type {?} */
    IMySQLQueryResult.prototype.affectedRows;
    /** @type {?} */
    IMySQLQueryResult.prototype.insertId;
    /** @type {?} */
    IMySQLQueryResult.prototype.serverStatus;
    /** @type {?} */
    IMySQLQueryResult.prototype.warningCount;
    /** @type {?} */
    IMySQLQueryResult.prototype.message;
    /** @type {?} */
    IMySQLQueryResult.prototype.changedRows;
}
/**
 * @record
 */
export function IMySQLDatabase() { }
if (false) {
    /** @type {?} */
    IMySQLDatabase.prototype.builder;
    /** @type {?} */
    IMySQLDatabase.prototype.transactionManager;
}
/**
 * @record
 * @template T
 */
export function IQueryParams() { }
if (false) {
    /** @type {?|undefined} */
    IQueryParams.prototype.whereMap;
    /** @type {?|undefined} */
    IQueryParams.prototype.whereAndMap;
    /** @type {?|undefined} */
    IQueryParams.prototype.whereOrMap;
    /** @type {?|undefined} */
    IQueryParams.prototype.whereClause;
    /** @type {?|undefined} */
    IQueryParams.prototype.wherePrimaryKeys;
    /** @type {?|undefined} */
    IQueryParams.prototype.whereGroup;
    /** @type {?|undefined} */
    IQueryParams.prototype.orderBy;
    /** @type {?|undefined} */
    IQueryParams.prototype.limit;
}
/**
 * @record
 * @template T
 */
export function ISelectQueryParams() { }
if (false) {
    /** @type {?|undefined} */
    ISelectQueryParams.prototype.fields;
}
/**
 * @record
 * @template T
 */
export function IQueryBuilderParams() { }
if (false) {
    /** @type {?} */
    IQueryBuilderParams.prototype.repository;
}
/**
 * @record
 * @template T
 */
export function ISelectQueryBuilderParams() { }
if (false) {
    /** @type {?} */
    ISelectQueryBuilderParams.prototype.fields;
}
/**
 * @record
 * @template T
 */
export function IUpdateQueryParams() { }
if (false) {
    /** @type {?|undefined} */
    IUpdateQueryParams.prototype.singleStatement;
    /** @type {?|undefined} */
    IUpdateQueryParams.prototype.statements;
}
/**
 * @record
 * @template T
 */
export function IUpdateQueryBuilderParams() { }
/**
 * @record
 */
export function IQueryData() { }
if (false) {
    /** @type {?} */
    IQueryData.prototype.sql;
    /** @type {?} */
    IQueryData.prototype.argumentsToPrepare;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RhdGFiYXNlL2VuZ2luZS9teXNxbC9pbnRlcmZhY2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBTUEsa0NBT0M7OztJQU5BLDRCQUFhOztJQUNiLDRCQUFjOztJQUNkLDRCQUFhOztJQUNiLGdDQUFpQjs7SUFDakIsZ0NBQWlCOztJQUNqQiwrQkFBZ0I7Ozs7O0FBbUJqQixzQ0FLQzs7O0lBSkEscUNBQWtCOztJQUNsQixzQ0FBbUI7O0lBRW5CLCtDQUE2Qjs7Ozs7QUFJOUIsdUNBUUM7OztJQVBBLHVDQUFtQjs7SUFDbkIseUNBQXFCOztJQUNyQixxQ0FBaUI7O0lBQ2pCLHlDQUFxQjs7SUFDckIseUNBQXFCOztJQUNyQixvQ0FBZ0I7O0lBQ2hCLHdDQUFvQjs7Ozs7QUF1QnJCLG9DQUdDOzs7SUFGQSxpQ0FBb0M7O0lBQ3BDLDRDQUFxRDs7Ozs7O0FBSXRELGtDQVlDOzs7SUFYQSxnQ0FBc0I7O0lBQ3RCLG1DQUF5Qjs7SUFDekIsa0NBQXdCOztJQUV4QixtQ0FBeUI7O0lBQ3pCLHdDQUFxQzs7SUFFckMsa0NBQTZCOztJQUU3QiwrQkFBaUI7O0lBQ2pCLDZCQUFzQjs7Ozs7O0FBR3ZCLHdDQUVDOzs7SUFEQSxvQ0FBa0I7Ozs7OztBQUduQix5Q0FFQzs7O0lBREEseUNBQTZCOzs7Ozs7QUFHOUIsK0NBR0M7OztJQURBLDJDQUFpQjs7Ozs7O0FBR2xCLHdDQUdDOzs7SUFGQSw2Q0FBNkI7O0lBQzdCLHdDQUEwQjs7Ozs7O0FBRzNCLCtDQUVDOzs7O0FBR0QsZ0NBR0M7OztJQUZBLHlCQUFZOztJQUNaLHdDQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE15U1FMUXVlcnlCdWlsZGVyIH0gZnJvbSAnLi9teXNxbC1xdWVyeS1idWlsZGVyJztcbmltcG9ydCB7IE15U1FMVHJhbnNhY3Rpb25NYW5hZ2VyIH0gZnJvbSAnLi9teXNxbC10cmFuc2FjdGlvbi1tYW5hZ2VyJztcbmltcG9ydCB7IEV4cHJlc3Npb25Hcm91cCwgRXhwcmVzc2lvbiB9IGZyb20gJy4vbXlzcWwtc3RhdGVtZW50cyc7XG5pbXBvcnQgeyBNeVNRTFF1ZXJ5IH0gZnJvbSAnLi9teXNxbC1xdWVyeSc7XG5cblxuZXhwb3J0IGludGVyZmFjZSBJTXlTUUxDb25maWcge1xuXHRob3N0OiBzdHJpbmc7XG5cdHBvcnQ/OiBudW1iZXI7XG5cdHVzZXI6IHN0cmluZztcblx0cGFzc3dvcmQ6IHN0cmluZztcblx0ZGF0YWJhc2U6IHN0cmluZztcblx0Y2hhcnNldDogc3RyaW5nO1xufVxuXG4vKlxuZXhwb3J0IGludGVyZmFjZSBJVXBkYXRlU3RhdGVtZW50RGF0YVxue1xuICAgIGZpZWxkTmFtZTpzdHJpbmc7XG5cbiAgICB2YWx1ZToge1xuICAgICAgICB0eXBlOm51bWJlcjtcbiAgICAgICAgZGF0YTphbnk7XG4gICAgfVxufVxuXG5leHBvcnQgdHlwZSBGaWVsZE1hcCA9IHtcbiAgICBbY29sdW1uTmFtZTpzdHJpbmddIDogc3RyaW5nO1xufTtcbiovXG5cbmV4cG9ydCBpbnRlcmZhY2UgSU15U1FMUmVwb3NpdG9yeSB7XG5cdHRhYmxlTmFtZTogc3RyaW5nO1xuXHRwcmltYXJ5S2V5OiBzdHJpbmc7XG5cblx0aXNQcmltYXJ5S2V5QVN0cmluZzogYm9vbGVhbjtcbn1cblxuXG5leHBvcnQgaW50ZXJmYWNlIElNeVNRTFF1ZXJ5UmVzdWx0IHtcblx0ZmllbGRDb3VudDogbnVtYmVyO1xuXHRhZmZlY3RlZFJvd3M6IG51bWJlcjtcblx0aW5zZXJ0SWQ6IG51bWJlcjtcblx0c2VydmVyU3RhdHVzOiBudW1iZXI7XG5cdHdhcm5pbmdDb3VudDogbnVtYmVyO1xuXHRtZXNzYWdlOiBzdHJpbmc7XG5cdGNoYW5nZWRSb3dzOiBudW1iZXI7XG59XG5cbmV4cG9ydCBkZWNsYXJlIHR5cGUgR2VuZXJpY015U1FMUXVlcnkgPSBNeVNRTFF1ZXJ5PElNeVNRTFF1ZXJ5UmVzdWx0PjtcbmV4cG9ydCBkZWNsYXJlIHR5cGUgTXlTUUxJbnNlcnRRdWVyeSA9IEdlbmVyaWNNeVNRTFF1ZXJ5O1xuZXhwb3J0IGRlY2xhcmUgdHlwZSBNeVNRTFVwZGF0ZVF1ZXJ5ID0gR2VuZXJpY015U1FMUXVlcnk7XG5leHBvcnQgZGVjbGFyZSB0eXBlIE15U1FMRGVsZXRlUXVlcnkgPSBHZW5lcmljTXlTUUxRdWVyeTtcblxuXG4vKlxuZXhwb3J0IGludGVyZmFjZSBJV2hlcmVGaWx0ZXIge1xuICAgIFtrZXk6IHN0cmluZ106IHN0cmluZ3xudW1iZXI7XG59XG4qL1xuXG5leHBvcnQgdHlwZSBQcmltYXJ5S2V5VmFsdWUgPSBudW1iZXJ8c3RyaW5nO1xuXG5leHBvcnQgdHlwZSBRdWVyeUV4ZWN1dGVyPFQ+ID0gKCkgPT4gUHJvbWlzZTxUPjtcblxuXG4vLyBJTXlTUUwgZGF0YWJhc2VkIGlzIG5vdyBDT01QT1NFRCBvZjpcblx0Ly8gMS4gYSBxdWVyeSBidWlsZGVyXG5cdC8vIDIuIGEgdHJhbnNhY3Rpb24gTWFuYWdlclxuZXhwb3J0IGludGVyZmFjZSBJTXlTUUxEYXRhYmFzZSB7XG5cdHJlYWRvbmx5IGJ1aWxkZXI6IE15U1FMUXVlcnlCdWlsZGVyO1xuXHRyZWFkb25seSB0cmFuc2FjdGlvbk1hbmFnZXI6IE15U1FMVHJhbnNhY3Rpb25NYW5hZ2VyO1xufVxuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSVF1ZXJ5UGFyYW1zPFQ+IHtcblx0d2hlcmVNYXA/OiBQYXJ0aWFsPFQ+O1xuXHR3aGVyZUFuZE1hcD86IFBhcnRpYWw8VD47XG5cdHdoZXJlT3JNYXA/OiBQYXJ0aWFsPFQ+O1xuXG5cdHdoZXJlQ2xhdXNlPzogRXhwcmVzc2lvbjtcblx0d2hlcmVQcmltYXJ5S2V5cz86IChudW1iZXJ8c3RyaW5nKVtdO1xuXG5cdHdoZXJlR3JvdXA/OiBFeHByZXNzaW9uR3JvdXA7XG5cblx0b3JkZXJCeT86IHN0cmluZztcblx0bGltaXQ/OiBudW1iZXJ8c3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElTZWxlY3RRdWVyeVBhcmFtczxUPiBleHRlbmRzIElRdWVyeVBhcmFtczxUPiB7XG5cdGZpZWxkcz86IHN0cmluZ1tdO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElRdWVyeUJ1aWxkZXJQYXJhbXM8VD4gZXh0ZW5kcyBJUXVlcnlQYXJhbXM8VD4ge1xuXHRyZXBvc2l0b3J5OiBJTXlTUUxSZXBvc2l0b3J5O1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElTZWxlY3RRdWVyeUJ1aWxkZXJQYXJhbXM8VD5cblx0ZXh0ZW5kcyBJU2VsZWN0UXVlcnlQYXJhbXM8VD4sIElRdWVyeUJ1aWxkZXJQYXJhbXM8VD4ge1xuXHRmaWVsZHM6IHN0cmluZ1tdO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElVcGRhdGVRdWVyeVBhcmFtczxUPiBleHRlbmRzIElRdWVyeVBhcmFtczxUPiB7XG5cdHNpbmdsZVN0YXRlbWVudD86IEV4cHJlc3Npb247XG5cdHN0YXRlbWVudHM/OiBFeHByZXNzaW9uW107XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVVwZGF0ZVF1ZXJ5QnVpbGRlclBhcmFtczxUPlxuXHRleHRlbmRzIElVcGRhdGVRdWVyeVBhcmFtczxUPiwgSVF1ZXJ5QnVpbGRlclBhcmFtczxUPiB7XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJUXVlcnlEYXRhIHtcblx0c3FsOiBzdHJpbmc7XG5cdGFyZ3VtZW50c1RvUHJlcGFyZTogYW55O1xufVxuIl19