/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-connection-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import * as mysql from 'mysql';
import { sleep, logWithTime } from 'earnbet-common';
/** @type {?} */
const logSqlQueries = process.env.LOG_SQL_QUERIES === 'true';
export class MySQLConnectionManager {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.config = config;
        this.isConnecting = false;
        this.connection = undefined;
        this.connect();
    }
    /**
     * @return {?}
     */
    getConnection() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            yield this.reconnect();
            return this.connection;
        });
    }
    /**
     * @private
     * @return {?}
     */
    reconnect() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            while (!this.isConnected) {
                logWithTime('NOT CONNECTED:', this.connection.threadId);
                this.printConnectionState();
                /** @type {?} */
                const isConnected = yield this.connect();
                if (isConnected) {
                    return;
                }
                yield sleep(1000);
            }
        });
    }
    /**
     * @private
     * @return {?}
     */
    connect() {
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        (resolve) => {
            if (this.isConnected) {
                resolve(true);
                return;
            }
            if (this.isConnecting) {
                resolve(false);
                return;
            }
            this.isConnecting = true;
            // end existing connection
            if (this.connection) {
                this.connection.end();
            }
            // connection parameters
            this.connection = mysql.createConnection(this.config);
            // logWithTime('MySQLConnection State:',this.connection.state);
            // establish connection
            this.connection.connect((/**
             * @param {?} err
             * @return {?}
             */
            (err) => {
                logWithTime(this.config);
                this.isConnecting = false;
                if (err) {
                    console.error('error connecting: ' + err.stack);
                    resolve(false);
                }
                else {
                    logWithTime('connected as id ' + this.connection.threadId);
                    logWithTime('MySQLConnection State:', this.connection.state);
                    // set query format
                    this.connection.config.queryFormat = (/**
                     * @param {?} query
                     * @param {?} values
                     * @return {?}
                     */
                    function (query, values) {
                        if (!values) {
                            return query;
                        }
                        /** @type {?} */
                        const escapedQuery = query.replace(/\:(\w+)/g, (/**
                         * @param {?} txt
                         * @param {?} key
                         * @return {?}
                         */
                        function (txt, key) {
                            if (values.hasOwnProperty(key)) {
                                return this.escape(values[key]);
                            }
                            return txt;
                        }).bind(this));
                        if (logSqlQueries) {
                            console.log({ escapedQuery });
                        }
                        return escapedQuery;
                    });
                    resolve(true);
                }
            }));
            this.connection.on('error', (/**
             * @param {?} err
             * @return {?}
             */
            (err) => {
                logWithTime('******************* MySQL Connection onError EVENT: *******************');
                console.error(err);
                this.printConnectionState();
            }));
        }));
    }
    /**
     * @private
     * @return {?}
     */
    printConnectionState() {
        logWithTime('MySQLConnection State:', this.connection.state);
    }
    /*
        private get isDisconnected():boolean {
            return this.connection == undefined ||
                    this.connection.state === 'disconnected';
        }
        */
    /**
     * @private
     * @return {?}
     */
    get isConnected() {
        return this.connection !== undefined && (this.connection.state === 'connected' ||
            this.connection.state === 'authenticated');
    }
    /**
     * @return {?}
     */
    endConnection() {
        this.connection.end();
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.isConnecting;
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.connection;
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.config;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvZGF0YWJhc2UvZW5naW5lL215c3FsL215c3FsLWNvbm5lY3Rpb24tbWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUUvQixPQUFPLEVBQUMsS0FBSyxFQUFFLFdBQVcsRUFBQyxNQUFNLGdCQUFnQixDQUFDOztNQUk1QyxhQUFhLEdBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxLQUFLLE1BQU07QUFHdkMsTUFBTSxPQUFPLHNCQUFzQjs7OztJQUtsQyxZQUFvQixNQUFvQjtRQUFwQixXQUFNLEdBQU4sTUFBTSxDQUFjO1FBSmhDLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBRXJCLGVBQVUsR0FBcUIsU0FBUyxDQUFDO1FBR2hELElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNoQixDQUFDOzs7O0lBRUssYUFBYTs7WUFDbEIsTUFBTSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFFdkIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ3hCLENBQUM7S0FBQTs7Ozs7SUFFYSxTQUFTOztZQUN0QixPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRTtnQkFDekIsV0FBVyxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBRXhELElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDOztzQkFHdEIsV0FBVyxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFFeEMsSUFBSSxXQUFXLEVBQUU7b0JBQ2hCLE9BQU87aUJBQ1A7Z0JBRUQsTUFBTSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDbEI7UUFDRixDQUFDO0tBQUE7Ozs7O0lBRU8sT0FBTztRQUNkLE9BQU8sSUFBSSxPQUFPOzs7O1FBQVcsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUN4QyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ3JCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDZCxPQUFPO2FBQ1A7WUFHRCxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ3RCLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDZixPQUFPO2FBQ1A7WUFHRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUd6QiwwQkFBMEI7WUFDMUIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNwQixJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDO2FBQ3RCO1lBR0Qsd0JBQXdCO1lBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUV0RCwrREFBK0Q7WUFFL0QsdUJBQXVCO1lBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTzs7OztZQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQy9CLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBRXpCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO2dCQUUxQixJQUFJLEdBQUcsRUFBRTtvQkFDUixPQUFPLENBQUMsS0FBSyxDQUFDLG9CQUFvQixHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFFaEQsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNmO3FCQUFNO29CQUNOLFdBQVcsQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUMzRCxXQUFXLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFHN0QsbUJBQW1CO29CQUNuQixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxXQUFXOzs7OztvQkFBRyxVQUFVLEtBQUssRUFBRSxNQUFNO3dCQUMzRCxJQUFJLENBQUMsTUFBTSxFQUFFOzRCQUFDLE9BQU8sS0FBSyxDQUFDO3lCQUFFOzs4QkFFdkIsWUFBWSxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFOzs7Ozt3QkFBQSxVQUFVLEdBQUcsRUFBRSxHQUFHOzRCQUNoRSxJQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0NBQy9CLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzs2QkFDaEM7NEJBQ0QsT0FBTyxHQUFHLENBQUM7d0JBQ1gsQ0FBQyxFQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFHZCxJQUFJLGFBQWEsRUFBRTs0QkFDbEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFDLFlBQVksRUFBQyxDQUFDLENBQUM7eUJBQzVCO3dCQUVELE9BQU8sWUFBWSxDQUFDO29CQUNyQixDQUFDLENBQUEsQ0FBQztvQkFHRixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2Q7WUFDRixDQUFDLEVBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLE9BQU87Ozs7WUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUNuQyxXQUFXLENBQUMseUVBQXlFLENBQUMsQ0FBQztnQkFFdkYsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFHbkIsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7WUFDN0IsQ0FBQyxFQUFDLENBQUM7UUFDSixDQUFDLEVBQUUsQ0FBQztJQUNMLENBQUM7Ozs7O0lBR08sb0JBQW9CO1FBQzNCLFdBQVcsQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzlELENBQUM7Ozs7Ozs7Ozs7O0lBUUQsSUFBWSxXQUFXO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLFVBQVUsS0FBSyxTQUFTLElBQUksQ0FDdkMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEtBQUssV0FBVztZQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssS0FBSyxlQUFlLENBQ3pDLENBQUM7SUFDSCxDQUFDOzs7O0lBR0QsYUFBYTtRQUNaLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDdkIsQ0FBQztDQUNEOzs7Ozs7SUFuSUEsOENBQTZCOzs7OztJQUU3Qiw0Q0FBaUQ7Ozs7O0lBRXJDLHdDQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIG15c3FsIGZyb20gJ215c3FsJztcblxuaW1wb3J0IHtzbGVlcCwgbG9nV2l0aFRpbWV9IGZyb20gJ2Vhcm5iZXQtY29tbW9uJztcbmltcG9ydCB7IElNeVNRTENvbmZpZyB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5cblxuY29uc3QgbG9nU3FsUXVlcmllcyA9XG5cdHByb2Nlc3MuZW52LkxPR19TUUxfUVVFUklFUyA9PT0gJ3RydWUnO1xuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyIHtcblx0cHJpdmF0ZSBpc0Nvbm5lY3RpbmcgPSBmYWxzZTtcblxuXHRwcml2YXRlIGNvbm5lY3Rpb246IG15c3FsLkNvbm5lY3Rpb24gPSB1bmRlZmluZWQ7XG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSBjb25maWc6IElNeVNRTENvbmZpZykge1xuXHRcdHRoaXMuY29ubmVjdCgpO1xuXHR9XG5cblx0YXN5bmMgZ2V0Q29ubmVjdGlvbigpOiBQcm9taXNlPG15c3FsLkNvbm5lY3Rpb24+IHtcblx0XHRhd2FpdCB0aGlzLnJlY29ubmVjdCgpO1xuXG5cdFx0cmV0dXJuIHRoaXMuY29ubmVjdGlvbjtcblx0fVxuXG5cdHByaXZhdGUgYXN5bmMgcmVjb25uZWN0KCkge1xuXHRcdHdoaWxlICghdGhpcy5pc0Nvbm5lY3RlZCkge1xuXHRcdFx0bG9nV2l0aFRpbWUoJ05PVCBDT05ORUNURUQ6JywgdGhpcy5jb25uZWN0aW9uLnRocmVhZElkKTtcblxuXHRcdFx0dGhpcy5wcmludENvbm5lY3Rpb25TdGF0ZSgpO1xuXG5cblx0XHRcdGNvbnN0IGlzQ29ubmVjdGVkID0gYXdhaXQgdGhpcy5jb25uZWN0KCk7XG5cblx0XHRcdGlmIChpc0Nvbm5lY3RlZCkge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cblx0XHRcdGF3YWl0IHNsZWVwKDEwMDApO1xuXHRcdH1cblx0fVxuXG5cdHByaXZhdGUgY29ubmVjdCgpOiBQcm9taXNlPGJvb2xlYW4+IHtcblx0XHRyZXR1cm4gbmV3IFByb21pc2U8Ym9vbGVhbj4oIChyZXNvbHZlKSA9PiB7XG5cdFx0XHRpZiAodGhpcy5pc0Nvbm5lY3RlZCkge1xuXHRcdFx0XHRyZXNvbHZlKHRydWUpO1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cblxuXHRcdFx0aWYgKHRoaXMuaXNDb25uZWN0aW5nKSB7XG5cdFx0XHRcdHJlc29sdmUoZmFsc2UpO1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cblxuXHRcdFx0dGhpcy5pc0Nvbm5lY3RpbmcgPSB0cnVlO1xuXG5cblx0XHRcdC8vIGVuZCBleGlzdGluZyBjb25uZWN0aW9uXG5cdFx0XHRpZiAodGhpcy5jb25uZWN0aW9uKSB7XG5cdFx0XHRcdHRoaXMuY29ubmVjdGlvbi5lbmQoKTtcblx0XHRcdH1cblxuXG5cdFx0XHQvLyBjb25uZWN0aW9uIHBhcmFtZXRlcnNcblx0XHRcdHRoaXMuY29ubmVjdGlvbiA9IG15c3FsLmNyZWF0ZUNvbm5lY3Rpb24odGhpcy5jb25maWcpO1xuXG5cdFx0XHQvLyBsb2dXaXRoVGltZSgnTXlTUUxDb25uZWN0aW9uIFN0YXRlOicsdGhpcy5jb25uZWN0aW9uLnN0YXRlKTtcblxuXHRcdFx0Ly8gZXN0YWJsaXNoIGNvbm5lY3Rpb25cblx0XHRcdHRoaXMuY29ubmVjdGlvbi5jb25uZWN0KChlcnIpID0+IHtcblx0XHRcdFx0bG9nV2l0aFRpbWUodGhpcy5jb25maWcpO1xuXG5cdFx0XHRcdHRoaXMuaXNDb25uZWN0aW5nID0gZmFsc2U7XG5cblx0XHRcdFx0aWYgKGVycikge1xuXHRcdFx0XHRcdGNvbnNvbGUuZXJyb3IoJ2Vycm9yIGNvbm5lY3Rpbmc6ICcgKyBlcnIuc3RhY2spO1xuXG5cdFx0XHRcdFx0cmVzb2x2ZShmYWxzZSk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0bG9nV2l0aFRpbWUoJ2Nvbm5lY3RlZCBhcyBpZCAnICsgdGhpcy5jb25uZWN0aW9uLnRocmVhZElkKTtcblx0XHRcdFx0XHRsb2dXaXRoVGltZSgnTXlTUUxDb25uZWN0aW9uIFN0YXRlOicsIHRoaXMuY29ubmVjdGlvbi5zdGF0ZSk7XG5cblxuXHRcdFx0XHRcdC8vIHNldCBxdWVyeSBmb3JtYXRcblx0XHRcdFx0XHR0aGlzLmNvbm5lY3Rpb24uY29uZmlnLnF1ZXJ5Rm9ybWF0ID0gZnVuY3Rpb24gKHF1ZXJ5LCB2YWx1ZXMpIHtcblx0XHRcdFx0XHRcdGlmICghdmFsdWVzKSB7cmV0dXJuIHF1ZXJ5OyB9XG5cblx0XHRcdFx0XHRcdGNvbnN0IGVzY2FwZWRRdWVyeSA9IHF1ZXJ5LnJlcGxhY2UoL1xcOihcXHcrKS9nLCBmdW5jdGlvbiAodHh0LCBrZXkpIHtcblx0XHRcdFx0XHRcdFx0aWYgKHZhbHVlcy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG5cdFx0XHRcdFx0XHRcdFx0cmV0dXJuIHRoaXMuZXNjYXBlKHZhbHVlc1trZXldKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRyZXR1cm4gdHh0O1xuXHRcdFx0XHRcdFx0XHR9LmJpbmQodGhpcykpO1xuXG5cblx0XHRcdFx0XHRcdGlmIChsb2dTcWxRdWVyaWVzKSB7XG5cdFx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKHtlc2NhcGVkUXVlcnl9KTtcblx0XHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdFx0cmV0dXJuIGVzY2FwZWRRdWVyeTtcblx0XHRcdFx0XHR9O1xuXG5cblx0XHRcdFx0XHRyZXNvbHZlKHRydWUpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblxuXHRcdFx0dGhpcy5jb25uZWN0aW9uLm9uKCdlcnJvcicsIChlcnIpID0+IHtcblx0XHRcdFx0bG9nV2l0aFRpbWUoJyoqKioqKioqKioqKioqKioqKiogTXlTUUwgQ29ubmVjdGlvbiBvbkVycm9yIEVWRU5UOiAqKioqKioqKioqKioqKioqKioqJyk7XG5cblx0XHRcdFx0Y29uc29sZS5lcnJvcihlcnIpO1xuXG5cblx0XHRcdFx0dGhpcy5wcmludENvbm5lY3Rpb25TdGF0ZSgpO1xuXHRcdFx0fSk7XG5cdFx0fSApO1xuXHR9XG5cblxuXHRwcml2YXRlIHByaW50Q29ubmVjdGlvblN0YXRlKCkge1xuXHRcdGxvZ1dpdGhUaW1lKCdNeVNRTENvbm5lY3Rpb24gU3RhdGU6JywgdGhpcy5jb25uZWN0aW9uLnN0YXRlKTtcblx0fVxuXG5cdC8qXG4gICAgcHJpdmF0ZSBnZXQgaXNEaXNjb25uZWN0ZWQoKTpib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29ubmVjdGlvbiA9PSB1bmRlZmluZWQgfHxcbiAgICAgICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb24uc3RhdGUgPT09ICdkaXNjb25uZWN0ZWQnO1xuICAgIH1cbiAgICAqL1xuXHRwcml2YXRlIGdldCBpc0Nvbm5lY3RlZCgpOiBib29sZWFuIHtcblx0XHRyZXR1cm4gdGhpcy5jb25uZWN0aW9uICE9PSB1bmRlZmluZWQgJiYgKFxuXHRcdFx0dGhpcy5jb25uZWN0aW9uLnN0YXRlID09PSAnY29ubmVjdGVkJyB8fFxuXHRcdFx0dGhpcy5jb25uZWN0aW9uLnN0YXRlID09PSAnYXV0aGVudGljYXRlZCdcblx0XHQpO1xuXHR9XG5cblxuXHRlbmRDb25uZWN0aW9uKCkge1xuXHRcdHRoaXMuY29ubmVjdGlvbi5lbmQoKTtcblx0fVxufVxuIl19