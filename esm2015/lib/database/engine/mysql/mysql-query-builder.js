/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-query-builder.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { MySQLConnectionManager } from './mysql-connection-manager';
import { MySQLExpression, SingleExpression, StringStatement, OrExpressionGroup, UpdateStatements, createExpressionGroup, ExpressionGroupJoiner } from './mysql-statements';
import { MySQLQuery } from './mysql-query';
export class MySQLQueryBuilder {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.connectionManager = new MySQLConnectionManager(config);
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    insert(repository, entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            // construct query
            /** @type {?} */
            const query = yield this.constructInsertQuery(repository, entities);
            return new MySQLQuery(query, this.connectionManager);
        });
    }
    /**
     * @private
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    constructInsertQuery(repository, entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const fields = [];
            for (const fieldName in entities[0]) {
                fields.push(fieldName);
            }
            /** @type {?} */
            const rows = [];
            for (const entity of entities) {
                rows.push(yield this.constructRowForInsert(entity));
            }
            return 'INSERT INTO ' + repository.tableName +
                ' (' + this.prepareFieldList(fields) + ')' +
                ' VALUES ' + rows.join(', ') + ';';
        });
    }
    /**
     * @private
     * @param {?} entity
     * @return {?}
     */
    constructRowForInsert(entity) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const values = [];
            for (const propertyName in entity) {
                /** @type {?} */
                const rawValue = entity[propertyName];
                /** @type {?} */
                const preparedValue = yield this.prepareValueForInsert(rawValue);
                values.push(preparedValue);
            }
            return '(' + values.join(',') + ')';
        });
    }
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    prepareValueForInsert(value) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            switch (typeof value) {
                case 'string':
                    switch (value) {
                        case MySQLExpression.NULL:
                        case MySQLExpression.NOW:
                            break;
                        default:
                            value = yield this.escape(value);
                            break;
                    }
                    break;
                default:
                    value = yield this.escape(value);
                    break;
            }
            return '' + value;
        });
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @param {?} primaryKeyValues
     * @return {?}
     */
    selectByPrimaryKeys(repository, fields, primaryKeyValues) {
        /** @type {?} */
        const query = this.prepareFieldsForSelect(fields, repository.tableName) +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @return {?}
     */
    selectAll(repository, fields) {
        /** @type {?} */
        const query = this.prepareFieldsForSelect(fields, repository.tableName) + ';';
        return new MySQLQuery(query, this.connectionManager);
    }
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    select(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.prepareSelectStatement(params);
            return new MySQLQuery(`${query};`, this.connectionManager);
        });
    }
    /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    prepareSelectStatement(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { fields, repository } = params;
            /** @type {?} */
            let query = this.prepareFieldsForSelect(fields, repository.tableName);
            query += yield this.prepareWhereOrderAndLimit(params);
            return query;
        });
    }
    /**
     * @private
     * @param {?} fields
     * @param {?} tableName
     * @return {?}
     */
    prepareFieldsForSelect(fields, tableName) {
        /** @type {?} */
        const fieldsList = this.prepareFieldList(fields);
        return `SELECT ${fieldsList} FROM ${tableName}`;
    }
    /**
     * @private
     * @param {?} fields
     * @return {?}
     */
    prepareFieldList(fields) {
        /** @type {?} */
        const prepared = [];
        for (const field of fields) {
            /** @type {?} */
            const isAlias = field.indexOf(' AS ') > -1;
            prepared.push(isAlias ?
                field :
                '`' + field + '`');
        }
        return prepared.join(',');
    }
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    update(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (params.singleStatement) {
                params.statements = [
                    params.singleStatement
                ];
            }
            const { statements, repository } = params;
            if (!statements ||
                statements.length < 1) {
                throw new Error('there must be at least 1 update statement!');
            }
            /** @type {?} */
            const updateStatements = yield this.prepareExpressionGroup(new UpdateStatements(statements));
            /** @type {?} */
            const remainingQuery = yield this.prepareWhereOrderAndLimit(params);
            /** @type {?} */
            const query = 'UPDATE ' + repository.tableName +
                ' SET ' + updateStatements +
                remainingQuery + ';';
            return new MySQLQuery(query, this.connectionManager);
        });
    }
    /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    prepareWhereOrderAndLimit(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (params.whereMap) {
                params.whereAndMap = params.whereMap;
            }
            if (params.whereAndMap ||
                params.whereOrMap) {
                params.whereGroup = createExpressionGroup(params.whereAndMap ?
                    params.whereAndMap :
                    params.whereOrMap, params.whereAndMap ?
                    ExpressionGroupJoiner.AND :
                    ExpressionGroupJoiner.OR);
            }
            else if (params.wherePrimaryKeys) {
                /** @type {?} */
                const expressions = params.wherePrimaryKeys.map((/**
                 * @param {?} primaryKeyValue
                 * @return {?}
                 */
                (primaryKeyValue) => new StringStatement(params.repository.primaryKey, '' + primaryKeyValue)));
                params.whereGroup = new OrExpressionGroup(expressions);
            }
            else if (params.whereClause) {
                params.whereGroup = new SingleExpression(params.whereClause);
            }
            const { whereGroup, orderBy, limit } = params;
            /** @type {?} */
            let query = '';
            /** @type {?} */
            const whereClause = whereGroup &&
                (yield this.prepareExpressionGroup(whereGroup));
            if (whereClause) {
                query += `
                WHERE ${whereClause}`;
            }
            if (orderBy) {
                query += `
                ORDER BY ${orderBy}`;
            }
            if (limit) {
                query += `
                LIMIT ${limit}`;
            }
            return query;
        });
    }
    /**
     * @private
     * @param {?} group
     * @return {?}
     */
    prepareExpressionGroup(group) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const statements = [];
            /** @type {?} */
            const useBrackets = group.surroundExpressionWithBrackets;
            for (const expression of group.expressions) {
                const { leftSide, operator, rightSide } = expression;
                /** @type {?} */
                const rightSideValue = rightSide.isString ?
                    `${yield this.escape((/** @type {?} */ (rightSide.data)))}` :
                    rightSide.data;
                statements.push(`${useBrackets ? '(' : ''} \`${leftSide}\` ${operator} ${rightSideValue} ${useBrackets ? ')' : ''}`);
            }
            return statements.join(` ${group.joiner} `);
        });
    }
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    escape(value) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const connection = yield this.connectionManager.getConnection();
            return connection.escape(value);
        });
    }
    /**
     * @param {?} repository
     * @param {?} primaryKeyValues
     * @return {?}
     */
    delete(repository, primaryKeyValues) {
        if (primaryKeyValues.length < 1) {
            throw new Error('Please specify which records to delete!');
        }
        /** @type {?} */
        const query = 'DELETE FROM ' + repository.tableName +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    }
    /**
     * @template T
     * @param {?} sql
     * @param {?=} argumentsToPrepare
     * @return {?}
     */
    rawQuery(sql, argumentsToPrepare) {
        return new MySQLQuery(sql, this.connectionManager, argumentsToPrepare);
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLQueryBuilder.prototype.connectionManager;
}
/**
 * @param {?} repository
 * @param {?} primaryKeyValues
 * @return {?}
 */
function constructWhereWithPrimaryKeys(repository, primaryKeyValues) {
    /** @type {?} */
    const conditions = [];
    for (let primaryKeyValue of primaryKeyValues) {
        if (repository.isPrimaryKeyAString) {
            primaryKeyValue = '\'' + primaryKeyValue + '\'';
        }
        /** @type {?} */
        const condition = repository.primaryKey + ' = ' + primaryKeyValue;
        conditions.push(condition);
    }
    return ' WHERE ' + conditions.join(' OR ');
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcXVlcnktYnVpbGRlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RhdGFiYXNlL2VuZ2luZS9teXNxbC9teXNxbC1xdWVyeS1idWlsZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBbUIsZUFBZSxFQUFFLGdCQUFnQixFQUFFLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxnQkFBZ0IsRUFBRSxxQkFBcUIsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQzVMLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFHekMsTUFBTSxPQUFPLGlCQUFpQjs7OztJQUc3QixZQUFZLE1BQW9CO1FBQy9CLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzdELENBQUM7Ozs7Ozs7SUFFWSxNQUFNLENBQ2xCLFVBQTRCLEVBQzVCLFFBQWE7Ozs7a0JBR1AsS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLG9CQUFvQixDQUM1QyxVQUFVLEVBQUUsUUFBUSxDQUNwQjtZQUVELE9BQU8sSUFBSSxVQUFVLENBQW9CLEtBQUssRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUN6RSxDQUFDO0tBQUE7Ozs7Ozs7SUFDYSxvQkFBb0IsQ0FDakMsVUFBNEIsRUFDNUIsUUFBZTs7O2tCQUVULE1BQU0sR0FBYSxFQUFFO1lBQzNCLEtBQUssTUFBTSxTQUFTLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUNwQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3ZCOztrQkFFSyxJQUFJLEdBQWEsRUFBRTtZQUN6QixLQUFLLE1BQU0sTUFBTSxJQUFJLFFBQVEsRUFBRTtnQkFDOUIsSUFBSSxDQUFDLElBQUksQ0FDUixNQUFNLElBQUksQ0FBQyxxQkFBcUIsQ0FDL0IsTUFBTSxDQUNOLENBQ0QsQ0FBQzthQUNGO1lBRUQsT0FBTyxjQUFjLEdBQUcsVUFBVSxDQUFDLFNBQVM7Z0JBQzNDLElBQUksR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLEdBQUcsR0FBRztnQkFDMUMsVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDO1FBRXJDLENBQUM7S0FBQTs7Ozs7O0lBQ2EscUJBQXFCLENBQ2xDLE1BQVc7OztrQkFFTCxNQUFNLEdBQWEsRUFBRTtZQUUzQixLQUFLLE1BQU0sWUFBWSxJQUFJLE1BQU0sRUFBRTs7c0JBQzVCLFFBQVEsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDOztzQkFFL0IsYUFBYSxHQUNsQixNQUFNLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLENBQUM7Z0JBRTNDLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7YUFDM0I7WUFFRCxPQUFPLEdBQUcsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQztRQUNyQyxDQUFDO0tBQUE7Ozs7OztJQUNhLHFCQUFxQixDQUFDLEtBQVU7O1lBQzdDLFFBQVEsT0FBTyxLQUFLLEVBQUU7Z0JBQ3JCLEtBQUssUUFBUTtvQkFDWixRQUFRLEtBQUssRUFBRTt3QkFDZCxLQUFLLGVBQWUsQ0FBQyxJQUFJLENBQUM7d0JBQzFCLEtBQUssZUFBZSxDQUFDLEdBQUc7NEJBQ3hCLE1BQU07d0JBRU47NEJBQ0MsS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQzs0QkFDbEMsTUFBTTtxQkFDTjtvQkFDRixNQUFNO2dCQUVOO29CQUNDLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ2xDLE1BQU07YUFDTjtZQUVELE9BQU8sRUFBRSxHQUFHLEtBQUssQ0FBQztRQUNuQixDQUFDO0tBQUE7Ozs7Ozs7O0lBR00sbUJBQW1CLENBQ3pCLFVBQTRCLEVBQzVCLE1BQWdCLEVBQ2hCLGdCQUFtQzs7Y0FFN0IsS0FBSyxHQUNWLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQztZQUN6RCw2QkFBNkIsQ0FBQyxVQUFVLEVBQUUsZ0JBQWdCLENBQUMsR0FBRyxHQUFHO1FBRWxFLE9BQU8sSUFBSSxVQUFVLENBQU0sS0FBSyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQzNELENBQUM7Ozs7Ozs7SUFFTSxTQUFTLENBQ2YsVUFBNEIsRUFDNUIsTUFBZ0I7O2NBRVYsS0FBSyxHQUNWLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEdBQUc7UUFFaEUsT0FBTyxJQUFJLFVBQVUsQ0FBTSxLQUFLLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDM0QsQ0FBQzs7Ozs7O0lBR1ksTUFBTSxDQUFJLE1BQW9DOzs7a0JBQ3BELEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLENBQUM7WUFFdkQsT0FBTyxJQUFJLFVBQVUsQ0FBTSxHQUFHLEtBQUssR0FBRyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ2pFLENBQUM7S0FBQTs7Ozs7OztJQUVhLHNCQUFzQixDQUFJLE1BQW9DOztrQkFDckUsRUFBQyxNQUFNLEVBQUUsVUFBVSxFQUFDLEdBQUcsTUFBTTs7Z0JBRS9CLEtBQUssR0FBVyxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUM7WUFFN0UsS0FBSyxJQUFJLE1BQU0sSUFBSSxDQUFDLHlCQUF5QixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRXRELE9BQU8sS0FBSyxDQUFDO1FBQ2QsQ0FBQztLQUFBOzs7Ozs7O0lBRU8sc0JBQXNCLENBQUMsTUFBZ0IsRUFBRSxTQUFpQjs7Y0FDM0QsVUFBVSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUM7UUFFaEQsT0FBTyxVQUFVLFVBQVUsU0FBUyxTQUFTLEVBQUUsQ0FBQztJQUNqRCxDQUFDOzs7Ozs7SUFHTyxnQkFBZ0IsQ0FBQyxNQUFnQjs7Y0FDbEMsUUFBUSxHQUFhLEVBQUU7UUFFN0IsS0FBSyxNQUFNLEtBQUssSUFBSSxNQUFNLEVBQUU7O2tCQUNyQixPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFMUMsUUFBUSxDQUFDLElBQUksQ0FDWixPQUFPLENBQUMsQ0FBQztnQkFDVCxLQUFLLENBQUMsQ0FBQztnQkFDUCxHQUFHLEdBQUcsS0FBSyxHQUFHLEdBQUcsQ0FDakIsQ0FBQztTQUNGO1FBRUQsT0FBTyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzNCLENBQUM7Ozs7OztJQUVZLE1BQU0sQ0FBSSxNQUFvQzs7WUFDMUQsSUFBSSxNQUFNLENBQUMsZUFBZSxFQUFFO2dCQUMzQixNQUFNLENBQUMsVUFBVSxHQUFHO29CQUNuQixNQUFNLENBQUMsZUFBZTtpQkFDdEIsQ0FBQzthQUNGO2tCQUdLLEVBQUMsVUFBVSxFQUFFLFVBQVUsRUFBQyxHQUFHLE1BQU07WUFFdkMsSUFDQyxDQUFDLFVBQVU7Z0JBQ1gsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQ3BCO2dCQUNELE1BQU0sSUFBSSxLQUFLLENBQUMsNENBQTRDLENBQUMsQ0FBQzthQUM5RDs7a0JBR0ssZ0JBQWdCLEdBQVcsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQ2pFLElBQUksZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQ2hDOztrQkFFSyxjQUFjLEdBQVcsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQ2xFLE1BQU0sQ0FDTjs7a0JBRUssS0FBSyxHQUNWLFNBQVMsR0FBRyxVQUFVLENBQUMsU0FBUztnQkFDaEMsT0FBTyxHQUFHLGdCQUFnQjtnQkFDekIsY0FBYyxHQUFHLEdBQUc7WUFFdEIsT0FBTyxJQUFJLFVBQVUsQ0FBb0IsS0FBSyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3pFLENBQUM7S0FBQTs7Ozs7OztJQUVhLHlCQUF5QixDQUFJLE1BQThCOztZQUN4RSxJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7Z0JBQ3BCLE1BQU0sQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQzthQUNyQztZQUVELElBQ0MsTUFBTSxDQUFDLFdBQVc7Z0JBQ2xCLE1BQU0sQ0FBQyxVQUFVLEVBQ2hCO2dCQUNELE1BQU0sQ0FBQyxVQUFVLEdBQUcscUJBQXFCLENBQ3hDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDbkIsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUNwQixNQUFNLENBQUMsVUFBVSxFQUNsQixNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQ25CLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUMzQixxQkFBcUIsQ0FBQyxFQUFFLENBQ3pCLENBQUM7YUFDRjtpQkFBTSxJQUFJLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRTs7c0JBQzdCLFdBQVcsR0FBc0IsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEdBQUc7Ozs7Z0JBQ2pFLENBQUMsZUFBZSxFQUFFLEVBQUUsQ0FBQyxJQUFJLGVBQWUsQ0FDdkMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQzVCLEVBQUUsR0FBRyxlQUFlLENBQ3BCLEVBQ0Q7Z0JBRUQsTUFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ3ZEO2lCQUFNLElBQUksTUFBTSxDQUFDLFdBQVcsRUFBRTtnQkFDOUIsTUFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLGdCQUFnQixDQUN2QyxNQUFNLENBQUMsV0FBVyxDQUNsQixDQUFDO2FBQ0Y7a0JBR0ssRUFBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBQyxHQUFHLE1BQU07O2dCQUV2QyxLQUFLLEdBQUcsRUFBRTs7a0JBRVIsV0FBVyxHQUNoQixVQUFVO2lCQUNWLE1BQU0sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxDQUFBO1lBRTlDLElBQUksV0FBVyxFQUFFO2dCQUNoQixLQUFLLElBQUk7d0JBQ1ksV0FBVyxFQUFFLENBQUM7YUFDbkM7WUFFRCxJQUFJLE9BQU8sRUFBRTtnQkFDWixLQUFLLElBQUk7MkJBQ2UsT0FBTyxFQUFFLENBQUM7YUFDbEM7WUFFRCxJQUFJLEtBQUssRUFBRTtnQkFDVixLQUFLLElBQUk7d0JBQ1ksS0FBSyxFQUFFLENBQUM7YUFDN0I7WUFFRCxPQUFPLEtBQUssQ0FBQztRQUNkLENBQUM7S0FBQTs7Ozs7O0lBRWEsc0JBQXNCLENBQUMsS0FBc0I7OztrQkFDcEQsVUFBVSxHQUFhLEVBQUU7O2tCQUV6QixXQUFXLEdBQUcsS0FBSyxDQUFDLDhCQUE4QjtZQUV4RCxLQUFLLE1BQU0sVUFBVSxJQUFJLEtBQUssQ0FBQyxXQUFXLEVBQUU7c0JBQ3JDLEVBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUMsR0FBRyxVQUFVOztzQkFFNUMsY0FBYyxHQUNuQixTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ25CLEdBQUksTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFBLFNBQVMsQ0FBQyxJQUFJLEVBQVUsQ0FBRSxFQUFFLENBQUMsQ0FBQztvQkFDcEQsU0FBUyxDQUFDLElBQUk7Z0JBRWhCLFVBQVUsQ0FBQyxJQUFJLENBQ2QsR0FDQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFDckIsTUFBTSxRQUFRLE1BQU0sUUFBUSxJQUFJLGNBQWMsSUFDN0MsV0FBVyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQ3JCLEVBQUUsQ0FBQyxDQUFDO2FBQ0w7WUFFRCxPQUFPLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUM3QyxDQUFDO0tBQUE7Ozs7OztJQUdhLE1BQU0sQ0FBQyxLQUFhOzs7a0JBQzNCLFVBQVUsR0FBRyxNQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUU7WUFFL0QsT0FBTyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2pDLENBQUM7S0FBQTs7Ozs7O0lBRU0sTUFBTSxDQUNaLFVBQTRCLEVBQzVCLGdCQUF1QjtRQUV2QixJQUFJLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDaEMsTUFBTSxJQUFJLEtBQUssQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFDO1NBQzNEOztjQUdLLEtBQUssR0FBRyxjQUFjLEdBQUcsVUFBVSxDQUFDLFNBQVM7WUFDaEQsNkJBQTZCLENBQUMsVUFBVSxFQUFFLGdCQUFnQixDQUFDLEdBQUcsR0FBRztRQUVwRSxPQUFPLElBQUksVUFBVSxDQUFvQixLQUFLLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDekUsQ0FBQzs7Ozs7OztJQUVELFFBQVEsQ0FBSSxHQUFXLEVBQUUsa0JBQXdCO1FBQ2hELE9BQU8sSUFBSSxVQUFVLENBQUksR0FBRyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO0lBQzNFLENBQUM7Q0FDRDs7Ozs7O0lBM1JBLDhDQUFrRDs7Ozs7OztBQThSbkQsU0FBUyw2QkFBNkIsQ0FDckMsVUFBNEIsRUFDNUIsZ0JBQXVCOztVQUVqQixVQUFVLEdBQWEsRUFBRTtJQUUvQixLQUFLLElBQUksZUFBZSxJQUFJLGdCQUFnQixFQUFFO1FBQzdDLElBQUksVUFBVSxDQUFDLG1CQUFtQixFQUFFO1lBQ25DLGVBQWUsR0FBRyxJQUFJLEdBQUcsZUFBZSxHQUFHLElBQUksQ0FBQztTQUNoRDs7Y0FFSyxTQUFTLEdBQUcsVUFBVSxDQUFDLFVBQVUsR0FBRyxLQUFLLEdBQUcsZUFBZTtRQUNqRSxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0tBQzNCO0lBRUQsT0FBTyxTQUFTLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUM1QyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSU15U1FMUmVwb3NpdG9yeSwgUHJpbWFyeUtleVZhbHVlLCBJTXlTUUxRdWVyeVJlc3VsdCwgSU15U1FMQ29uZmlnLCBJU2VsZWN0UXVlcnlCdWlsZGVyUGFyYW1zLCBJVXBkYXRlUXVlcnlCdWlsZGVyUGFyYW1zLCBJUXVlcnlCdWlsZGVyUGFyYW1zIH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIgfSBmcm9tICcuL215c3FsLWNvbm5lY3Rpb24tbWFuYWdlcic7XG5pbXBvcnQgeyBFeHByZXNzaW9uR3JvdXAsIE15U1FMRXhwcmVzc2lvbiwgU2luZ2xlRXhwcmVzc2lvbiwgU3RyaW5nU3RhdGVtZW50LCBPckV4cHJlc3Npb25Hcm91cCwgVXBkYXRlU3RhdGVtZW50cywgY3JlYXRlRXhwcmVzc2lvbkdyb3VwLCBFeHByZXNzaW9uR3JvdXBKb2luZXIgfSBmcm9tICcuL215c3FsLXN0YXRlbWVudHMnO1xuaW1wb3J0IHtNeVNRTFF1ZXJ5fSBmcm9tICcuL215c3FsLXF1ZXJ5JztcblxuXG5leHBvcnQgY2xhc3MgTXlTUUxRdWVyeUJ1aWxkZXIge1xuXHRwcml2YXRlIGNvbm5lY3Rpb25NYW5hZ2VyOiBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyO1xuXG5cdGNvbnN0cnVjdG9yKGNvbmZpZzogSU15U1FMQ29uZmlnKSB7XG5cdFx0dGhpcy5jb25uZWN0aW9uTWFuYWdlciA9IG5ldyBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyKGNvbmZpZyk7XG5cdH1cblxuXHRwdWJsaWMgYXN5bmMgaW5zZXJ0PFQ+KFxuXHRcdHJlcG9zaXRvcnk6IElNeVNRTFJlcG9zaXRvcnksXG5cdFx0ZW50aXRpZXM6IFRbXVxuXHQpIHtcblx0XHQvLyBjb25zdHJ1Y3QgcXVlcnlcblx0XHRjb25zdCBxdWVyeSA9IGF3YWl0IHRoaXMuY29uc3RydWN0SW5zZXJ0UXVlcnkoXG5cdFx0XHRyZXBvc2l0b3J5LCBlbnRpdGllc1xuXHRcdCk7XG5cblx0XHRyZXR1cm4gbmV3IE15U1FMUXVlcnk8SU15U1FMUXVlcnlSZXN1bHQ+KHF1ZXJ5LCB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyKTtcblx0fVxuXHRwcml2YXRlIGFzeW5jIGNvbnN0cnVjdEluc2VydFF1ZXJ5KFxuXHRcdHJlcG9zaXRvcnk6IElNeVNRTFJlcG9zaXRvcnksXG5cdFx0ZW50aXRpZXM6IGFueVtdXG5cdCk6IFByb21pc2U8c3RyaW5nPiB7XG5cdFx0Y29uc3QgZmllbGRzOiBzdHJpbmdbXSA9IFtdO1xuXHRcdGZvciAoY29uc3QgZmllbGROYW1lIGluIGVudGl0aWVzWzBdKSB7XG5cdFx0XHRmaWVsZHMucHVzaChmaWVsZE5hbWUpO1xuXHRcdH1cblxuXHRcdGNvbnN0IHJvd3M6IHN0cmluZ1tdID0gW107XG5cdFx0Zm9yIChjb25zdCBlbnRpdHkgb2YgZW50aXRpZXMpIHtcblx0XHRcdHJvd3MucHVzaChcblx0XHRcdFx0YXdhaXQgdGhpcy5jb25zdHJ1Y3RSb3dGb3JJbnNlcnQoXG5cdFx0XHRcdFx0ZW50aXR5XG5cdFx0XHRcdClcblx0XHRcdCk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuICdJTlNFUlQgSU5UTyAnICsgcmVwb3NpdG9yeS50YWJsZU5hbWUgK1xuXHRcdFx0JyAoJyArIHRoaXMucHJlcGFyZUZpZWxkTGlzdChmaWVsZHMpICsgJyknICtcblx0XHRcdCcgVkFMVUVTICcgKyByb3dzLmpvaW4oJywgJykgKyAnOyc7XG5cblx0fVxuXHRwcml2YXRlIGFzeW5jIGNvbnN0cnVjdFJvd0Zvckluc2VydChcblx0XHRlbnRpdHk6IGFueVxuXHQpOiBQcm9taXNlPHN0cmluZz4ge1xuXHRcdGNvbnN0IHZhbHVlczogc3RyaW5nW10gPSBbXTtcblxuXHRcdGZvciAoY29uc3QgcHJvcGVydHlOYW1lIGluIGVudGl0eSkge1xuXHRcdFx0Y29uc3QgcmF3VmFsdWUgPSBlbnRpdHlbcHJvcGVydHlOYW1lXTtcblxuXHRcdFx0Y29uc3QgcHJlcGFyZWRWYWx1ZTogc3RyaW5nID1cblx0XHRcdFx0YXdhaXQgdGhpcy5wcmVwYXJlVmFsdWVGb3JJbnNlcnQocmF3VmFsdWUpO1xuXG5cdFx0XHR2YWx1ZXMucHVzaChwcmVwYXJlZFZhbHVlKTtcblx0XHR9XG5cblx0XHRyZXR1cm4gJygnICsgdmFsdWVzLmpvaW4oJywnKSArICcpJztcblx0fVxuXHRwcml2YXRlIGFzeW5jIHByZXBhcmVWYWx1ZUZvckluc2VydCh2YWx1ZTogYW55KTogUHJvbWlzZTxzdHJpbmc+IHtcblx0XHRzd2l0Y2ggKHR5cGVvZiB2YWx1ZSkge1xuXHRcdFx0Y2FzZSAnc3RyaW5nJzpcblx0XHRcdFx0c3dpdGNoICh2YWx1ZSkge1xuXHRcdFx0XHRcdGNhc2UgTXlTUUxFeHByZXNzaW9uLk5VTEw6XG5cdFx0XHRcdFx0Y2FzZSBNeVNRTEV4cHJlc3Npb24uTk9XOlxuXHRcdFx0XHRcdGJyZWFrO1xuXG5cdFx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRcdHZhbHVlID0gYXdhaXQgdGhpcy5lc2NhcGUodmFsdWUpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHR9XG5cdFx0XHRicmVhaztcblxuXHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0dmFsdWUgPSBhd2FpdCB0aGlzLmVzY2FwZSh2YWx1ZSk7XG5cdFx0XHRicmVhaztcblx0XHR9XG5cblx0XHRyZXR1cm4gJycgKyB2YWx1ZTtcblx0fVxuXG5cblx0cHVibGljIHNlbGVjdEJ5UHJpbWFyeUtleXM8VD4oXG5cdFx0cmVwb3NpdG9yeTogSU15U1FMUmVwb3NpdG9yeSxcblx0XHRmaWVsZHM6IHN0cmluZ1tdLFxuXHRcdHByaW1hcnlLZXlWYWx1ZXM6IFByaW1hcnlLZXlWYWx1ZVtdXG5cdCkge1xuXHRcdGNvbnN0IHF1ZXJ5ID1cblx0XHRcdHRoaXMucHJlcGFyZUZpZWxkc0ZvclNlbGVjdChmaWVsZHMsIHJlcG9zaXRvcnkudGFibGVOYW1lKSArXG5cdFx0XHRjb25zdHJ1Y3RXaGVyZVdpdGhQcmltYXJ5S2V5cyhyZXBvc2l0b3J5LCBwcmltYXJ5S2V5VmFsdWVzKSArICc7JztcblxuXHRcdHJldHVybiBuZXcgTXlTUUxRdWVyeTxUW10+KHF1ZXJ5LCB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyKTtcblx0fVxuXG5cdHB1YmxpYyBzZWxlY3RBbGw8VD4oXG5cdFx0cmVwb3NpdG9yeTogSU15U1FMUmVwb3NpdG9yeSxcblx0XHRmaWVsZHM6IHN0cmluZ1tdLFxuXHQpIHtcblx0XHRjb25zdCBxdWVyeSA9XG5cdFx0XHR0aGlzLnByZXBhcmVGaWVsZHNGb3JTZWxlY3QoZmllbGRzLCByZXBvc2l0b3J5LnRhYmxlTmFtZSkgKyAnOyc7XG5cblx0XHRyZXR1cm4gbmV3IE15U1FMUXVlcnk8VFtdPihxdWVyeSwgdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG5cdH1cblxuXG5cdHB1YmxpYyBhc3luYyBzZWxlY3Q8VD4ocGFyYW1zOiBJU2VsZWN0UXVlcnlCdWlsZGVyUGFyYW1zPFQ+KSB7XG5cdFx0Y29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLnByZXBhcmVTZWxlY3RTdGF0ZW1lbnQocGFyYW1zKTtcblxuXHRcdHJldHVybiBuZXcgTXlTUUxRdWVyeTxUW10+KGAke3F1ZXJ5fTtgLCB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyKTtcblx0fVxuXG5cdHByaXZhdGUgYXN5bmMgcHJlcGFyZVNlbGVjdFN0YXRlbWVudDxUPihwYXJhbXM6IElTZWxlY3RRdWVyeUJ1aWxkZXJQYXJhbXM8VD4pOiBQcm9taXNlPHN0cmluZz4ge1xuXHRcdGNvbnN0IHtmaWVsZHMsIHJlcG9zaXRvcnl9ID0gcGFyYW1zO1xuXG5cdFx0bGV0IHF1ZXJ5OiBzdHJpbmcgPSB0aGlzLnByZXBhcmVGaWVsZHNGb3JTZWxlY3QoZmllbGRzLCByZXBvc2l0b3J5LnRhYmxlTmFtZSk7XG5cblx0XHRxdWVyeSArPSBhd2FpdCB0aGlzLnByZXBhcmVXaGVyZU9yZGVyQW5kTGltaXQocGFyYW1zKTtcblxuXHRcdHJldHVybiBxdWVyeTtcblx0fVxuXG5cdHByaXZhdGUgcHJlcGFyZUZpZWxkc0ZvclNlbGVjdChmaWVsZHM6IHN0cmluZ1tdLCB0YWJsZU5hbWU6IHN0cmluZyk6IHN0cmluZyB7XG5cdFx0Y29uc3QgZmllbGRzTGlzdCA9IHRoaXMucHJlcGFyZUZpZWxkTGlzdChmaWVsZHMpO1xuXG5cdFx0cmV0dXJuIGBTRUxFQ1QgJHtmaWVsZHNMaXN0fSBGUk9NICR7dGFibGVOYW1lfWA7XG5cdH1cblxuXG5cdHByaXZhdGUgcHJlcGFyZUZpZWxkTGlzdChmaWVsZHM6IHN0cmluZ1tdKTogc3RyaW5nIHtcblx0XHRjb25zdCBwcmVwYXJlZDogc3RyaW5nW10gPSBbXTtcblxuXHRcdGZvciAoY29uc3QgZmllbGQgb2YgZmllbGRzKSB7XG5cdFx0XHRjb25zdCBpc0FsaWFzID0gZmllbGQuaW5kZXhPZignIEFTICcpID4gLTE7XG5cblx0XHRcdHByZXBhcmVkLnB1c2goXG5cdFx0XHRcdGlzQWxpYXMgP1xuXHRcdFx0XHRmaWVsZCA6XG5cdFx0XHRcdCdgJyArIGZpZWxkICsgJ2AnXG5cdFx0XHQpO1xuXHRcdH1cblxuXHRcdHJldHVybiBwcmVwYXJlZC5qb2luKCcsJyk7XG5cdH1cblxuXHRwdWJsaWMgYXN5bmMgdXBkYXRlPFQ+KHBhcmFtczogSVVwZGF0ZVF1ZXJ5QnVpbGRlclBhcmFtczxUPikge1xuXHRcdGlmIChwYXJhbXMuc2luZ2xlU3RhdGVtZW50KSB7XG5cdFx0XHRwYXJhbXMuc3RhdGVtZW50cyA9IFtcblx0XHRcdFx0cGFyYW1zLnNpbmdsZVN0YXRlbWVudFxuXHRcdFx0XTtcblx0XHR9XG5cblxuXHRcdGNvbnN0IHtzdGF0ZW1lbnRzLCByZXBvc2l0b3J5fSA9IHBhcmFtcztcblxuXHRcdGlmIChcblx0XHRcdCFzdGF0ZW1lbnRzIHx8XG5cdFx0XHRzdGF0ZW1lbnRzLmxlbmd0aCA8IDFcblx0XHQpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcigndGhlcmUgbXVzdCBiZSBhdCBsZWFzdCAxIHVwZGF0ZSBzdGF0ZW1lbnQhJyk7XG5cdFx0fVxuXG5cblx0XHRjb25zdCB1cGRhdGVTdGF0ZW1lbnRzOiBzdHJpbmcgPSBhd2FpdCB0aGlzLnByZXBhcmVFeHByZXNzaW9uR3JvdXAoXG5cdFx0XHRuZXcgVXBkYXRlU3RhdGVtZW50cyhzdGF0ZW1lbnRzKVxuXHRcdCk7XG5cblx0XHRjb25zdCByZW1haW5pbmdRdWVyeTogc3RyaW5nID0gYXdhaXQgdGhpcy5wcmVwYXJlV2hlcmVPcmRlckFuZExpbWl0KFxuXHRcdFx0cGFyYW1zXG5cdFx0KTtcblxuXHRcdGNvbnN0IHF1ZXJ5OiBzdHJpbmcgPVxuXHRcdFx0J1VQREFURSAnICsgcmVwb3NpdG9yeS50YWJsZU5hbWUgK1xuXHRcdFx0JyBTRVQgJyArIHVwZGF0ZVN0YXRlbWVudHMgK1xuXHRcdFx0XHRyZW1haW5pbmdRdWVyeSArICc7JztcblxuXHRcdHJldHVybiBuZXcgTXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4ocXVlcnksIHRoaXMuY29ubmVjdGlvbk1hbmFnZXIpO1xuXHR9XG5cblx0cHJpdmF0ZSBhc3luYyBwcmVwYXJlV2hlcmVPcmRlckFuZExpbWl0PFQ+KHBhcmFtczogSVF1ZXJ5QnVpbGRlclBhcmFtczxUPikge1xuXHRcdGlmIChwYXJhbXMud2hlcmVNYXApIHtcblx0XHRcdHBhcmFtcy53aGVyZUFuZE1hcCA9IHBhcmFtcy53aGVyZU1hcDtcblx0XHR9XG5cblx0XHRpZiAoXG5cdFx0XHRwYXJhbXMud2hlcmVBbmRNYXAgfHxcblx0XHRcdHBhcmFtcy53aGVyZU9yTWFwXG5cdFx0KSB7XG5cdFx0XHRwYXJhbXMud2hlcmVHcm91cCA9IGNyZWF0ZUV4cHJlc3Npb25Hcm91cChcblx0XHRcdFx0cGFyYW1zLndoZXJlQW5kTWFwID9cblx0XHRcdFx0XHRwYXJhbXMud2hlcmVBbmRNYXAgOlxuXHRcdFx0XHRcdHBhcmFtcy53aGVyZU9yTWFwLFxuXHRcdFx0XHRwYXJhbXMud2hlcmVBbmRNYXAgP1xuXHRcdFx0XHRcdEV4cHJlc3Npb25Hcm91cEpvaW5lci5BTkQgOlxuXHRcdFx0XHRcdEV4cHJlc3Npb25Hcm91cEpvaW5lci5PUlxuXHRcdFx0KTtcblx0XHR9IGVsc2UgaWYgKHBhcmFtcy53aGVyZVByaW1hcnlLZXlzKSB7XG5cdFx0XHRjb25zdCBleHByZXNzaW9uczogU3RyaW5nU3RhdGVtZW50W10gPSBwYXJhbXMud2hlcmVQcmltYXJ5S2V5cy5tYXAoXG5cdFx0XHRcdChwcmltYXJ5S2V5VmFsdWUpID0+IG5ldyBTdHJpbmdTdGF0ZW1lbnQoXG5cdFx0XHRcdFx0cGFyYW1zLnJlcG9zaXRvcnkucHJpbWFyeUtleSxcblx0XHRcdFx0XHQnJyArIHByaW1hcnlLZXlWYWx1ZVxuXHRcdFx0XHQpXG5cdFx0XHQpO1xuXG5cdFx0XHRwYXJhbXMud2hlcmVHcm91cCA9IG5ldyBPckV4cHJlc3Npb25Hcm91cChleHByZXNzaW9ucyk7XG5cdFx0fSBlbHNlIGlmIChwYXJhbXMud2hlcmVDbGF1c2UpIHtcblx0XHRcdHBhcmFtcy53aGVyZUdyb3VwID0gbmV3IFNpbmdsZUV4cHJlc3Npb24oXG5cdFx0XHRcdHBhcmFtcy53aGVyZUNsYXVzZVxuXHRcdFx0KTtcblx0XHR9XG5cblxuXHRcdGNvbnN0IHt3aGVyZUdyb3VwLCBvcmRlckJ5LCBsaW1pdH0gPSBwYXJhbXM7XG5cblx0XHRsZXQgcXVlcnkgPSAnJztcblxuXHRcdGNvbnN0IHdoZXJlQ2xhdXNlID1cblx0XHRcdHdoZXJlR3JvdXAgJiZcblx0XHRcdGF3YWl0IHRoaXMucHJlcGFyZUV4cHJlc3Npb25Hcm91cCh3aGVyZUdyb3VwKTtcblxuXHRcdGlmICh3aGVyZUNsYXVzZSkge1xuXHRcdFx0cXVlcnkgKz0gYFxuICAgICAgICAgICAgICAgIFdIRVJFICR7d2hlcmVDbGF1c2V9YDtcblx0XHR9XG5cblx0XHRpZiAob3JkZXJCeSkge1xuXHRcdFx0cXVlcnkgKz0gYFxuICAgICAgICAgICAgICAgIE9SREVSIEJZICR7b3JkZXJCeX1gO1xuXHRcdH1cblxuXHRcdGlmIChsaW1pdCkge1xuXHRcdFx0cXVlcnkgKz0gYFxuICAgICAgICAgICAgICAgIExJTUlUICR7bGltaXR9YDtcblx0XHR9XG5cblx0XHRyZXR1cm4gcXVlcnk7XG5cdH1cblxuXHRwcml2YXRlIGFzeW5jIHByZXBhcmVFeHByZXNzaW9uR3JvdXAoZ3JvdXA6IEV4cHJlc3Npb25Hcm91cCk6IFByb21pc2U8c3RyaW5nPiB7XG5cdFx0Y29uc3Qgc3RhdGVtZW50czogc3RyaW5nW10gPSBbXTtcblxuXHRcdGNvbnN0IHVzZUJyYWNrZXRzID0gZ3JvdXAuc3Vycm91bmRFeHByZXNzaW9uV2l0aEJyYWNrZXRzO1xuXG5cdFx0Zm9yIChjb25zdCBleHByZXNzaW9uIG9mIGdyb3VwLmV4cHJlc3Npb25zKSB7XG5cdFx0XHRjb25zdCB7bGVmdFNpZGUsIG9wZXJhdG9yLCByaWdodFNpZGV9ID0gZXhwcmVzc2lvbjtcblxuXHRcdFx0Y29uc3QgcmlnaHRTaWRlVmFsdWUgPVxuXHRcdFx0XHRyaWdodFNpZGUuaXNTdHJpbmcgP1xuXHRcdFx0XHRcdGAkeyBhd2FpdCB0aGlzLmVzY2FwZShyaWdodFNpZGUuZGF0YSBhcyBzdHJpbmcpIH1gIDpcblx0XHRcdFx0XHRyaWdodFNpZGUuZGF0YTtcblxuXHRcdFx0c3RhdGVtZW50cy5wdXNoKFxuXHRcdFx0XHRgJHtcblx0XHRcdFx0XHR1c2VCcmFja2V0cyA/ICcoJyA6ICcnXG5cdFx0XHRcdH0gXFxgJHtsZWZ0U2lkZX1cXGAgJHtvcGVyYXRvcn0gJHtyaWdodFNpZGVWYWx1ZX0gJHtcblx0XHRcdFx0XHR1c2VCcmFja2V0cyA/ICcpJyA6ICcnXG5cdFx0XHRcdH1gKTtcblx0XHR9XG5cblx0XHRyZXR1cm4gc3RhdGVtZW50cy5qb2luKGAgJHtncm91cC5qb2luZXJ9IGApO1xuXHR9XG5cblxuXHRwcml2YXRlIGFzeW5jIGVzY2FwZSh2YWx1ZTogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcblx0XHRjb25zdCBjb25uZWN0aW9uID0gYXdhaXQgdGhpcy5jb25uZWN0aW9uTWFuYWdlci5nZXRDb25uZWN0aW9uKCk7XG5cblx0XHRyZXR1cm4gY29ubmVjdGlvbi5lc2NhcGUodmFsdWUpO1xuXHR9XG5cblx0cHVibGljIGRlbGV0ZShcblx0XHRyZXBvc2l0b3J5OiBJTXlTUUxSZXBvc2l0b3J5LFxuXHRcdHByaW1hcnlLZXlWYWx1ZXM6IGFueVtdXG5cdCkge1xuXHRcdGlmIChwcmltYXJ5S2V5VmFsdWVzLmxlbmd0aCA8IDEpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignUGxlYXNlIHNwZWNpZnkgd2hpY2ggcmVjb3JkcyB0byBkZWxldGUhJyk7XG5cdFx0fVxuXG5cblx0XHRjb25zdCBxdWVyeSA9ICdERUxFVEUgRlJPTSAnICsgcmVwb3NpdG9yeS50YWJsZU5hbWUgK1xuXHRcdFx0XHRcdGNvbnN0cnVjdFdoZXJlV2l0aFByaW1hcnlLZXlzKHJlcG9zaXRvcnksIHByaW1hcnlLZXlWYWx1ZXMpICsgJzsnO1xuXG5cdFx0cmV0dXJuIG5ldyBNeVNRTFF1ZXJ5PElNeVNRTFF1ZXJ5UmVzdWx0PihxdWVyeSwgdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG5cdH1cblxuXHRyYXdRdWVyeTxUPihzcWw6IHN0cmluZywgYXJndW1lbnRzVG9QcmVwYXJlPzogYW55KTogTXlTUUxRdWVyeTxUPiB7XG5cdFx0cmV0dXJuIG5ldyBNeVNRTFF1ZXJ5PFQ+KHNxbCwgdGhpcy5jb25uZWN0aW9uTWFuYWdlciwgYXJndW1lbnRzVG9QcmVwYXJlKTtcblx0fVxufVxuXG5cbmZ1bmN0aW9uIGNvbnN0cnVjdFdoZXJlV2l0aFByaW1hcnlLZXlzKFxuXHRyZXBvc2l0b3J5OiBJTXlTUUxSZXBvc2l0b3J5LFxuXHRwcmltYXJ5S2V5VmFsdWVzOiBhbnlbXVxuKTogc3RyaW5nIHtcblx0Y29uc3QgY29uZGl0aW9uczogc3RyaW5nW10gPSBbXTtcblxuXHRmb3IgKGxldCBwcmltYXJ5S2V5VmFsdWUgb2YgcHJpbWFyeUtleVZhbHVlcykge1xuXHRcdGlmIChyZXBvc2l0b3J5LmlzUHJpbWFyeUtleUFTdHJpbmcpIHtcblx0XHRcdHByaW1hcnlLZXlWYWx1ZSA9ICdcXCcnICsgcHJpbWFyeUtleVZhbHVlICsgJ1xcJyc7XG5cdFx0fVxuXG5cdFx0Y29uc3QgY29uZGl0aW9uID0gcmVwb3NpdG9yeS5wcmltYXJ5S2V5ICsgJyA9ICcgKyBwcmltYXJ5S2V5VmFsdWU7XG5cdFx0Y29uZGl0aW9ucy5wdXNoKGNvbmRpdGlvbik7XG5cdH1cblxuXHRyZXR1cm4gJyBXSEVSRSAnICsgY29uZGl0aW9ucy5qb2luKCcgT1IgJyk7XG59XG4iXX0=