/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-query.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { logWithTime } from 'earnbet-common';
/** @type {?} */
const logSqlQueries = process.env.LOG_SQL_QUERIES === 'true';
/**
 * @template T
 */
export class MySQLQuery {
    /**
     * @param {?} sql
     * @param {?} connectionManager
     * @param {?=} argumentsToPrepare
     */
    constructor(sql, connectionManager, argumentsToPrepare) {
        this.sql = sql;
        this.connectionManager = connectionManager;
        this.argumentsToPrepare = argumentsToPrepare;
    }
    /**
     * @return {?}
     */
    execute() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            // When execute is called directly,
            // by default, it will use the connectionManager used to construct the query...
            /** @type {?} */
            const connection = yield this.connectionManager.getConnection();
            return executeQuery(this.sql, connection, this.argumentsToPrepare);
        });
    }
}
if (false) {
    /** @type {?} */
    MySQLQuery.prototype.sql;
    /**
     * @type {?}
     * @private
     */
    MySQLQuery.prototype.connectionManager;
    /** @type {?} */
    MySQLQuery.prototype.argumentsToPrepare;
}
/**
 * @template T
 * @param {?} sql
 * @param {?} connection
 * @param {?} argumentsToPrepare
 * @return {?}
 */
export function executeQuery(sql, connection, argumentsToPrepare) {
    return new Promise((/**
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    (resolve, reject) => {
        if (logSqlQueries) {
            logWithTime(sql, argumentsToPrepare);
        }
        connection.query(sql, argumentsToPrepare, (/**
         * @param {?} error
         * @param {?} result
         * @param {?} fields
         * @return {?}
         */
        (error, result, fields) => {
            if (error) {
                logWithTime('*** MySQL Error while executing Query: ', sql, argumentsToPrepare);
                console.error(error);
                reject(error);
            }
            else {
                resolve(result);
            }
        }));
    }));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcXVlcnkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9kYXRhYmFzZS9lbmdpbmUvbXlzcWwvbXlzcWwtcXVlcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBRUEsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLGdCQUFnQixDQUFDOztNQUtyQyxhQUFhLEdBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxLQUFLLE1BQU07Ozs7QUFHdkMsTUFBTSxPQUFPLFVBQVU7Ozs7OztJQUN0QixZQUNVLEdBQVcsRUFDWixpQkFBeUMsRUFDeEMsa0JBQXdCO1FBRnhCLFFBQUcsR0FBSCxHQUFHLENBQVE7UUFDWixzQkFBaUIsR0FBakIsaUJBQWlCLENBQXdCO1FBQ3hDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBTTtJQUVsQyxDQUFDOzs7O0lBRUssT0FBTzs7Ozs7a0JBR04sVUFBVSxHQUFHLE1BQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRTtZQUUvRCxPQUFPLFlBQVksQ0FBSSxJQUFJLENBQUMsR0FBRyxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUN2RSxDQUFDO0tBQUE7Q0FDRDs7O0lBYkMseUJBQW9COzs7OztJQUNwQix1Q0FBaUQ7O0lBQ2pELHdDQUFpQzs7Ozs7Ozs7O0FBY25DLE1BQU0sVUFBVSxZQUFZLENBQzNCLEdBQVcsRUFDWCxVQUFzQixFQUN0QixrQkFBdUI7SUFFdkIsT0FBTyxJQUFJLE9BQU87Ozs7O0lBQUssQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7UUFDMUMsSUFBSSxhQUFhLEVBQUU7WUFDbEIsV0FBVyxDQUFDLEdBQUcsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1NBQ3JDO1FBRUQsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsa0JBQWtCOzs7Ozs7UUFBRSxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDbkUsSUFBSSxLQUFLLEVBQUU7Z0JBQ1YsV0FBVyxDQUFDLHlDQUF5QyxFQUFFLEdBQUcsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO2dCQUNoRixPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUVyQixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDZDtpQkFBTTtnQkFDTixPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDaEI7UUFDRixDQUFDLEVBQUMsQ0FBQztJQUNKLENBQUMsRUFBQyxDQUFDO0FBQ0osQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbm5lY3Rpb24gfSBmcm9tICdteXNxbCc7XG5cbmltcG9ydCB7bG9nV2l0aFRpbWV9IGZyb20gJ2Vhcm5iZXQtY29tbW9uJztcblxuaW1wb3J0IHsgTXlTUUxDb25uZWN0aW9uTWFuYWdlciB9IGZyb20gJy4vbXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyJztcblxuXG5jb25zdCBsb2dTcWxRdWVyaWVzID1cblx0cHJvY2Vzcy5lbnYuTE9HX1NRTF9RVUVSSUVTID09PSAndHJ1ZSc7XG5cblxuZXhwb3J0IGNsYXNzIE15U1FMUXVlcnk8VD4ge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRyZWFkb25seSBzcWw6IHN0cmluZyxcblx0XHRwcml2YXRlIGNvbm5lY3Rpb25NYW5hZ2VyOiBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyLFxuXHRcdHJlYWRvbmx5IGFyZ3VtZW50c1RvUHJlcGFyZT86IGFueSxcblx0KSB7XG5cdH1cblxuXHRhc3luYyBleGVjdXRlKCk6IFByb21pc2U8VD4ge1xuXHRcdC8vIFdoZW4gZXhlY3V0ZSBpcyBjYWxsZWQgZGlyZWN0bHksXG5cdFx0Ly8gYnkgZGVmYXVsdCwgaXQgd2lsbCB1c2UgdGhlIGNvbm5lY3Rpb25NYW5hZ2VyIHVzZWQgdG8gY29uc3RydWN0IHRoZSBxdWVyeS4uLlxuXHRcdGNvbnN0IGNvbm5lY3Rpb24gPSBhd2FpdCB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyLmdldENvbm5lY3Rpb24oKTtcblxuXHRcdHJldHVybiBleGVjdXRlUXVlcnk8VD4odGhpcy5zcWwsIGNvbm5lY3Rpb24sIHRoaXMuYXJndW1lbnRzVG9QcmVwYXJlKTtcblx0fVxufVxuXG5cbmV4cG9ydCBmdW5jdGlvbiBleGVjdXRlUXVlcnk8VD4oXG5cdHNxbDogc3RyaW5nLFxuXHRjb25uZWN0aW9uOiBDb25uZWN0aW9uLFxuXHRhcmd1bWVudHNUb1ByZXBhcmU6IGFueSxcbik6IFByb21pc2U8VD4ge1xuXHRyZXR1cm4gbmV3IFByb21pc2U8VD4oIChyZXNvbHZlLCByZWplY3QpID0+IHtcblx0XHRpZiAobG9nU3FsUXVlcmllcykge1xuXHRcdFx0bG9nV2l0aFRpbWUoc3FsLCBhcmd1bWVudHNUb1ByZXBhcmUpO1xuXHRcdH1cblxuXHRcdGNvbm5lY3Rpb24ucXVlcnkoc3FsLCBhcmd1bWVudHNUb1ByZXBhcmUsIChlcnJvciwgcmVzdWx0LCBmaWVsZHMpID0+IHtcblx0XHRcdGlmIChlcnJvcikge1xuXHRcdFx0XHRsb2dXaXRoVGltZSgnKioqIE15U1FMIEVycm9yIHdoaWxlIGV4ZWN1dGluZyBRdWVyeTogJywgc3FsLCBhcmd1bWVudHNUb1ByZXBhcmUpO1xuXHRcdFx0XHRjb25zb2xlLmVycm9yKGVycm9yKTtcblxuXHRcdFx0XHRyZWplY3QoZXJyb3IpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0cmVzb2x2ZShyZXN1bHQpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHR9KTtcbn1cbiJdfQ==