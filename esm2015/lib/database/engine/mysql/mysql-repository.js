/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-repository.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * @template TypeForSelect
 */
export class MySQLRepository {
    /**
     * @param {?} db
     * @param {?} _tableName
     * @param {?=} defaultFieldsToSelect
     * @param {?=} _primaryKey
     * @param {?=} isPrimaryKeyAString
     */
    constructor(db, _tableName, defaultFieldsToSelect = [], _primaryKey = 'id', isPrimaryKeyAString = false) {
        this.db = db;
        this._tableName = _tableName;
        this.defaultFieldsToSelect = defaultFieldsToSelect;
        this._primaryKey = _primaryKey;
        this.isPrimaryKeyAString = isPrimaryKeyAString;
    }
    /**
     * @template T
     * @param {?} entity
     * @return {?}
     */
    insert(entity) {
        return this.insertMany([entity]);
    }
    /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    insertMany(entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.buildInsert(entities);
            return yield query.execute();
        });
    }
    /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    buildInsert(entities) {
        return this.db.builder.insert(this, entities);
    }
    /**
     * @param {?} primaryKeyValue
     * @param {?=} fields
     * @return {?}
     */
    selectOneByPrimaryKey(primaryKeyValue, fields = this.defaultFieldsToSelect) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const rows = yield this.selectManyByPrimaryKeys([primaryKeyValue], fields);
            return rows.length > 0 ?
                rows[0] :
                null;
        });
    }
    /**
     * @param {?} primaryKeyValues
     * @param {?=} fields
     * @return {?}
     */
    selectManyByPrimaryKeys(primaryKeyValues, fields = this.defaultFieldsToSelect) {
        return this.db.builder.selectByPrimaryKeys(this, fields, primaryKeyValues).execute();
    }
    /**
     * @param {?=} fields
     * @return {?}
     */
    selectAll(fields = this.defaultFieldsToSelect) {
        return this.db.builder.selectAll(this, fields).execute();
    }
    /**
     * @param {?} params
     * @return {?}
     */
    selectOne(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const rows = yield this.select(Object.assign({}, params, { limit: 1 }));
            return rows.length > 0 ?
                rows[0] :
                null;
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    select(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.buildSelect(params);
            return query.execute();
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    buildSelect(params) {
        /** @type {?} */
        const fields = params.fields ?
            params.fields :
            this.defaultFieldsToSelect;
        /** @type {?} */
        const newParams = Object.assign({}, params, { fields, repository: this });
        return this.db.builder.select(newParams);
    }
    /**
     * @param {?} params
     * @return {?}
     */
    updateOne(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.buildUpdateOne(params);
            return query.execute();
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    buildUpdateOne(params) {
        return this.buildUpdate(Object.assign({}, params, { limit: 1 }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    update(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.buildUpdate(params);
            return query.execute();
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    buildUpdate(params) {
        /** @type {?} */
        const builderParams = Object.assign({}, params, { repository: this });
        return this.db.builder.update(builderParams);
    }
    /**
     * @param {?} primaryKeyValues
     * @return {?}
     */
    delete(primaryKeyValues) {
        return this.db.builder.delete(this, primaryKeyValues).execute();
    }
    /**
     * @return {?}
     */
    get tableName() {
        return this._tableName;
    }
    /**
     * @return {?}
     */
    get primaryKey() {
        return this._primaryKey;
    }
    /**
     * @private
     * @return {?}
     */
    get primaryKeyFieldList() {
        return [this.primaryKey];
    }
}
if (false) {
    /**
     * @type {?}
     * @protected
     */
    MySQLRepository.prototype.db;
    /**
     * @type {?}
     * @private
     */
    MySQLRepository.prototype._tableName;
    /**
     * @type {?}
     * @protected
     */
    MySQLRepository.prototype.defaultFieldsToSelect;
    /**
     * @type {?}
     * @private
     */
    MySQLRepository.prototype._primaryKey;
    /** @type {?} */
    MySQLRepository.prototype.isPrimaryKeyAString;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcmVwb3NpdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RhdGFiYXNlL2VuZ2luZS9teXNxbC9teXNxbC1yZXBvc2l0b3J5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQWtCQSxNQUFNLE9BQU8sZUFBZTs7Ozs7Ozs7SUFDM0IsWUFDVyxFQUFrQixFQUNwQixVQUFrQixFQUNoQix3QkFBa0MsRUFBRSxFQUN0QyxjQUFzQixJQUFJLEVBQ3pCLHNCQUFzQixLQUFLO1FBSjFCLE9BQUUsR0FBRixFQUFFLENBQWdCO1FBQ3BCLGVBQVUsR0FBVixVQUFVLENBQVE7UUFDaEIsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUFlO1FBQ3RDLGdCQUFXLEdBQVgsV0FBVyxDQUFlO1FBQ3pCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBUTtJQUVyQyxDQUFDOzs7Ozs7SUFFTSxNQUFNLENBQUksTUFBUztRQUN6QixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQ2xDLENBQUM7Ozs7OztJQUNZLFVBQVUsQ0FBSSxRQUFhOzs7a0JBQ2pDLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO1lBQzlDLE9BQU8sTUFBTSxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDOUIsQ0FBQztLQUFBOzs7Ozs7SUFFTSxXQUFXLENBQUksUUFBYTtRQUNsQyxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBSSxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDbEQsQ0FBQzs7Ozs7O0lBR1kscUJBQXFCLENBQ2pDLGVBQWdDLEVBQ2hDLFNBQW1CLElBQUksQ0FBQyxxQkFBcUI7OztrQkFFdkMsSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLHVCQUF1QixDQUM5QyxDQUFDLGVBQWUsQ0FBQyxFQUNqQixNQUFNLENBQ047WUFFRCxPQUFPLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNULElBQUksQ0FBQztRQUNSLENBQUM7S0FBQTs7Ozs7O0lBRU0sdUJBQXVCLENBQzdCLGdCQUFtQyxFQUNuQyxTQUFtQixJQUFJLENBQUMscUJBQXFCO1FBRTdDLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQ3pDLElBQUksRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLENBQzlCLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDYixDQUFDOzs7OztJQUVNLFNBQVMsQ0FBQyxTQUFtQixJQUFJLENBQUMscUJBQXFCO1FBQzdELE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUMvQixJQUFJLEVBQ0osTUFBTSxDQUNOLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDYixDQUFDOzs7OztJQUdZLFNBQVMsQ0FBQyxNQUF5Qzs7O2tCQUN6RCxJQUFJLEdBQUcsTUFBTSxJQUFJLENBQUMsTUFBTSxtQkFDMUIsTUFBTSxJQUNULEtBQUssRUFBRSxDQUFDLElBQ1A7WUFFRixPQUFPLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNULElBQUksQ0FBQztRQUNSLENBQUM7S0FBQTs7Ozs7SUFFWSxNQUFNLENBQUMsTUFBeUM7OztrQkFDdEQsS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7WUFFNUMsT0FBTyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDeEIsQ0FBQztLQUFBOzs7OztJQUVNLFdBQVcsQ0FBQyxNQUF5Qzs7Y0FDckQsTUFBTSxHQUNYLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNkLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxxQkFBcUI7O2NBRXRCLFNBQVMscUJBQ1gsTUFBTSxJQUNULE1BQU0sRUFDTixVQUFVLEVBQUUsSUFBSSxHQUNoQjtRQUVELE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFnQixTQUFTLENBQUMsQ0FBQztJQUN6RCxDQUFDOzs7OztJQUdZLFNBQVMsQ0FBQyxNQUF5Qzs7O2tCQUN6RCxLQUFLLEdBQUcsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztZQUUvQyxPQUFPLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN4QixDQUFDO0tBQUE7Ozs7O0lBQ00sY0FBYyxDQUFDLE1BQXlDO1FBQzlELE9BQU8sSUFBSSxDQUFDLFdBQVcsbUJBQ25CLE1BQU0sSUFDVCxLQUFLLEVBQUUsQ0FBQyxJQUNQLENBQUM7SUFDSixDQUFDOzs7OztJQUVZLE1BQU0sQ0FBQyxNQUF5Qzs7O2tCQUN0RCxLQUFLLEdBQUcsTUFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQztZQUM1QyxPQUFPLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN4QixDQUFDO0tBQUE7Ozs7O0lBQ00sV0FBVyxDQUFDLE1BQXlDOztjQUNyRCxhQUFhLHFCQUNmLE1BQU0sSUFDVCxVQUFVLEVBQUUsSUFBSSxHQUNoQjtRQUVELE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQzlDLENBQUM7Ozs7O0lBR00sTUFBTSxDQUFDLGdCQUFtQztRQUNoRCxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNqRSxDQUFDOzs7O0lBR0QsSUFBVyxTQUFTO1FBQ25CLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsSUFBVyxVQUFVO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUN6QixDQUFDOzs7OztJQUVELElBQVksbUJBQW1CO1FBQzlCLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDMUIsQ0FBQztDQUNEOzs7Ozs7SUEvSEMsNkJBQTRCOzs7OztJQUM1QixxQ0FBMEI7Ozs7O0lBQzFCLGdEQUE4Qzs7Ozs7SUFDOUMsc0NBQWtDOztJQUNsQyw4Q0FBb0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuXHRJTXlTUUxEYXRhYmFzZSxcblx0SU15U1FMUmVwb3NpdG9yeSxcblx0UHJpbWFyeUtleVZhbHVlLFxuXHRJTXlTUUxRdWVyeVJlc3VsdCxcblx0SVNlbGVjdFF1ZXJ5QnVpbGRlclBhcmFtcyxcblx0SVNlbGVjdFF1ZXJ5UGFyYW1zLFxuXHRJVXBkYXRlUXVlcnlQYXJhbXMsXG5cdElVcGRhdGVRdWVyeUJ1aWxkZXJQYXJhbXMsXG59IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBNeVNRTFF1ZXJ5IH0gZnJvbSAnLi9teXNxbC1xdWVyeSc7XG5cblxuXG5leHBvcnQgdHlwZSBTZWxlY3RPbmVIYW5kbGVyPFQ+ID0gKHJvd3M6IFRbXSkgPT4gdm9pZDtcblxuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTFJlcG9zaXRvcnk8VHlwZUZvclNlbGVjdD4gaW1wbGVtZW50cyBJTXlTUUxSZXBvc2l0b3J5IHtcblx0Y29uc3RydWN0b3IoXG5cdFx0cHJvdGVjdGVkIGRiOiBJTXlTUUxEYXRhYmFzZSxcblx0XHRwcml2YXRlIF90YWJsZU5hbWU6IHN0cmluZyxcblx0XHRwcm90ZWN0ZWQgZGVmYXVsdEZpZWxkc1RvU2VsZWN0OiBzdHJpbmdbXSA9IFtdLFxuXHRcdHByaXZhdGUgX3ByaW1hcnlLZXk6IHN0cmluZyA9ICdpZCcsXG5cdFx0cmVhZG9ubHkgaXNQcmltYXJ5S2V5QVN0cmluZyA9IGZhbHNlXG5cdCkge1xuXHR9XG5cblx0cHVibGljIGluc2VydDxUPihlbnRpdHk6IFQpIHtcblx0XHRyZXR1cm4gdGhpcy5pbnNlcnRNYW55KFtlbnRpdHldKTtcblx0fVxuXHRwdWJsaWMgYXN5bmMgaW5zZXJ0TWFueTxUPihlbnRpdGllczogVFtdKSB7XG5cdFx0Y29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLmJ1aWxkSW5zZXJ0KGVudGl0aWVzKTtcblx0XHRyZXR1cm4gYXdhaXQgcXVlcnkuZXhlY3V0ZSgpO1xuXHR9XG5cblx0cHVibGljIGJ1aWxkSW5zZXJ0PFQ+KGVudGl0aWVzOiBUW10pOiBQcm9taXNlPE15U1FMUXVlcnk8SU15U1FMUXVlcnlSZXN1bHQ+PiB7XG5cdFx0cmV0dXJuIHRoaXMuZGIuYnVpbGRlci5pbnNlcnQ8VD4odGhpcywgZW50aXRpZXMpO1xuXHR9XG5cblxuXHRwdWJsaWMgYXN5bmMgc2VsZWN0T25lQnlQcmltYXJ5S2V5KFxuXHRcdHByaW1hcnlLZXlWYWx1ZTogUHJpbWFyeUtleVZhbHVlLFxuXHRcdGZpZWxkczogc3RyaW5nW10gPSB0aGlzLmRlZmF1bHRGaWVsZHNUb1NlbGVjdFxuXHQpOiBQcm9taXNlPFR5cGVGb3JTZWxlY3Q+IHtcblx0XHRjb25zdCByb3dzID0gYXdhaXQgdGhpcy5zZWxlY3RNYW55QnlQcmltYXJ5S2V5cyhcblx0XHRcdFtwcmltYXJ5S2V5VmFsdWVdLFxuXHRcdFx0ZmllbGRzXG5cdFx0KTtcblxuXHRcdHJldHVybiByb3dzLmxlbmd0aCA+IDAgP1xuXHRcdFx0XHRyb3dzWzBdIDpcblx0XHRcdFx0bnVsbDtcblx0fVxuXG5cdHB1YmxpYyBzZWxlY3RNYW55QnlQcmltYXJ5S2V5cyhcblx0XHRwcmltYXJ5S2V5VmFsdWVzOiBQcmltYXJ5S2V5VmFsdWVbXSxcblx0XHRmaWVsZHM6IHN0cmluZ1tdID0gdGhpcy5kZWZhdWx0RmllbGRzVG9TZWxlY3Rcblx0KSB7XG5cdFx0cmV0dXJuIHRoaXMuZGIuYnVpbGRlci5zZWxlY3RCeVByaW1hcnlLZXlzPFR5cGVGb3JTZWxlY3Q+KFxuXHRcdFx0dGhpcywgZmllbGRzLCBwcmltYXJ5S2V5VmFsdWVzXG5cdFx0KS5leGVjdXRlKCk7XG5cdH1cblxuXHRwdWJsaWMgc2VsZWN0QWxsKGZpZWxkczogc3RyaW5nW10gPSB0aGlzLmRlZmF1bHRGaWVsZHNUb1NlbGVjdCkge1xuXHRcdHJldHVybiB0aGlzLmRiLmJ1aWxkZXIuc2VsZWN0QWxsPFR5cGVGb3JTZWxlY3Q+KFxuXHRcdFx0dGhpcyxcblx0XHRcdGZpZWxkc1xuXHRcdCkuZXhlY3V0ZSgpO1xuXHR9XG5cblxuXHRwdWJsaWMgYXN5bmMgc2VsZWN0T25lKHBhcmFtczogSVNlbGVjdFF1ZXJ5UGFyYW1zPFR5cGVGb3JTZWxlY3Q+KSB7XG5cdFx0Y29uc3Qgcm93cyA9IGF3YWl0IHRoaXMuc2VsZWN0KHtcblx0XHRcdC4uLnBhcmFtcyxcblx0XHRcdGxpbWl0OiAxXG5cdFx0fSk7XG5cblx0XHRyZXR1cm4gcm93cy5sZW5ndGggPiAwID9cblx0XHRcdFx0cm93c1swXSA6XG5cdFx0XHRcdG51bGw7XG5cdH1cblxuXHRwdWJsaWMgYXN5bmMgc2VsZWN0KHBhcmFtczogSVNlbGVjdFF1ZXJ5UGFyYW1zPFR5cGVGb3JTZWxlY3Q+KSB7XG5cdFx0Y29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLmJ1aWxkU2VsZWN0KHBhcmFtcyk7XG5cblx0XHRyZXR1cm4gcXVlcnkuZXhlY3V0ZSgpO1xuXHR9XG5cblx0cHVibGljIGJ1aWxkU2VsZWN0KHBhcmFtczogSVNlbGVjdFF1ZXJ5UGFyYW1zPFR5cGVGb3JTZWxlY3Q+KSB7XG5cdFx0Y29uc3QgZmllbGRzID1cblx0XHRcdHBhcmFtcy5maWVsZHMgP1xuXHRcdFx0XHRwYXJhbXMuZmllbGRzIDpcblx0XHRcdFx0dGhpcy5kZWZhdWx0RmllbGRzVG9TZWxlY3Q7XG5cblx0XHRjb25zdCBuZXdQYXJhbXM6IElTZWxlY3RRdWVyeUJ1aWxkZXJQYXJhbXM8VHlwZUZvclNlbGVjdD4gPSB7XG5cdFx0XHQuLi5wYXJhbXMsXG5cdFx0XHRmaWVsZHMsXG5cdFx0XHRyZXBvc2l0b3J5OiB0aGlzXG5cdFx0fTtcblxuXHRcdHJldHVybiB0aGlzLmRiLmJ1aWxkZXIuc2VsZWN0PFR5cGVGb3JTZWxlY3Q+KG5ld1BhcmFtcyk7XG5cdH1cblxuXG5cdHB1YmxpYyBhc3luYyB1cGRhdGVPbmUocGFyYW1zOiBJVXBkYXRlUXVlcnlQYXJhbXM8VHlwZUZvclNlbGVjdD4pIHtcblx0XHRjb25zdCBxdWVyeSA9IGF3YWl0IHRoaXMuYnVpbGRVcGRhdGVPbmUocGFyYW1zKTtcblxuXHRcdHJldHVybiBxdWVyeS5leGVjdXRlKCk7XG5cdH1cblx0cHVibGljIGJ1aWxkVXBkYXRlT25lKHBhcmFtczogSVVwZGF0ZVF1ZXJ5UGFyYW1zPFR5cGVGb3JTZWxlY3Q+KSB7XG5cdFx0cmV0dXJuIHRoaXMuYnVpbGRVcGRhdGUoe1xuXHRcdFx0Li4ucGFyYW1zLFxuXHRcdFx0bGltaXQ6IDFcblx0XHR9KTtcblx0fVxuXG5cdHB1YmxpYyBhc3luYyB1cGRhdGUocGFyYW1zOiBJVXBkYXRlUXVlcnlQYXJhbXM8VHlwZUZvclNlbGVjdD4pIHtcblx0XHRjb25zdCBxdWVyeSA9IGF3YWl0IHRoaXMuYnVpbGRVcGRhdGUocGFyYW1zKTtcblx0XHRyZXR1cm4gcXVlcnkuZXhlY3V0ZSgpO1xuXHR9XG5cdHB1YmxpYyBidWlsZFVwZGF0ZShwYXJhbXM6IElVcGRhdGVRdWVyeVBhcmFtczxUeXBlRm9yU2VsZWN0Pikge1xuXHRcdGNvbnN0IGJ1aWxkZXJQYXJhbXM6IElVcGRhdGVRdWVyeUJ1aWxkZXJQYXJhbXM8VHlwZUZvclNlbGVjdD4gPSB7XG5cdFx0XHQuLi5wYXJhbXMsXG5cdFx0XHRyZXBvc2l0b3J5OiB0aGlzXG5cdFx0fTtcblxuXHRcdHJldHVybiB0aGlzLmRiLmJ1aWxkZXIudXBkYXRlKGJ1aWxkZXJQYXJhbXMpO1xuXHR9XG5cblxuXHRwdWJsaWMgZGVsZXRlKHByaW1hcnlLZXlWYWx1ZXM6IFByaW1hcnlLZXlWYWx1ZVtdKSB7XG5cdFx0cmV0dXJuIHRoaXMuZGIuYnVpbGRlci5kZWxldGUodGhpcywgcHJpbWFyeUtleVZhbHVlcykuZXhlY3V0ZSgpO1xuXHR9XG5cblxuXHRwdWJsaWMgZ2V0IHRhYmxlTmFtZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl90YWJsZU5hbWU7XG5cdH1cblxuXHRwdWJsaWMgZ2V0IHByaW1hcnlLZXkoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fcHJpbWFyeUtleTtcblx0fVxuXG5cdHByaXZhdGUgZ2V0IHByaW1hcnlLZXlGaWVsZExpc3QoKTogc3RyaW5nW10ge1xuXHRcdHJldHVybiBbdGhpcy5wcmltYXJ5S2V5XTtcblx0fVxufVxuIl19