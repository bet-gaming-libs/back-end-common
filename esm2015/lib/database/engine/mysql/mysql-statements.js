/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-statements.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const MySQLValueType = {
    EXPRESSION: 0,
    STRING: 1,
};
export { MySQLValueType };
MySQLValueType[MySQLValueType.EXPRESSION] = 'EXPRESSION';
MySQLValueType[MySQLValueType.STRING] = 'STRING';
/** @enum {string} */
const ExpressionOperator = {
    EQUAL: "=",
    NOT_EQUAL: "!=",
    LESS_THAN: "<",
    LESS_THAN_OR_EQUAL: "<=",
    GREATER_THAN: ">",
    GREATER_THAN_OR_EQUAL: ">=",
    IS: "IS",
    IS_NOT: "IS NOT",
};
export { ExpressionOperator };
/** @enum {string} */
const ExpressionGroupJoiner = {
    AND: "AND",
    OR: "OR",
    COMMA: ",",
};
export { ExpressionGroupJoiner };
/**
 * @record
 */
export function IMySQLValue() { }
if (false) {
    /** @type {?} */
    IMySQLValue.prototype.data;
    /** @type {?} */
    IMySQLValue.prototype.isString;
}
export class MySQLValue {
    /**
     * @param {?} data
     * @param {?} type
     */
    constructor(data, type) {
        this.data = data;
        this.type = type;
        this.isString =
            type == MySQLValueType.STRING;
    }
}
if (false) {
    /** @type {?} */
    MySQLValue.prototype.isString;
    /** @type {?} */
    MySQLValue.prototype.data;
    /** @type {?} */
    MySQLValue.prototype.type;
}
class MySQLNumberValue extends MySQLValue {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data, MySQLValueType.EXPRESSION);
    }
}
class MySQLStringValue extends MySQLValue {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data, MySQLValueType.STRING);
    }
}
class MySQLStringExpressionValue extends MySQLValue {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data, MySQLValueType.EXPRESSION);
    }
}
/** @enum {string} */
const MySQLExpression = {
    NOW: "NOW()",
    NULL: "NULL",
};
export { MySQLExpression };
/** @type {?} */
const nowValue = new MySQLStringExpressionValue(MySQLExpression.NOW);
/** @type {?} */
const nullValue = new MySQLStringExpressionValue(MySQLExpression.NULL);
/*
class MySQLValue
{
    static booleanAsInt(bool:boolean):number
    {
        return bool ? 1 : 0;
    }
}
*/
/**
 * @abstract
 */
export class Expression {
    /**
     * @param {?} leftSide
     * @param {?} operator
     * @param {?} rightSide
     */
    constructor(leftSide, operator, rightSide) {
        this.leftSide = leftSide;
        this.operator = operator;
        this.rightSide = rightSide;
    }
}
if (false) {
    /** @type {?} */
    Expression.prototype.leftSide;
    /** @type {?} */
    Expression.prototype.operator;
    /** @type {?} */
    Expression.prototype.rightSide;
}
export class ExpressionGroup {
    /**
     * @param {?} expressions
     * @param {?} joiner
     * @param {?=} surroundExpressionWithBrackets
     */
    constructor(expressions, joiner, surroundExpressionWithBrackets = true) {
        this.expressions = expressions;
        this.joiner = joiner;
        this.surroundExpressionWithBrackets = surroundExpressionWithBrackets;
    }
}
if (false) {
    /** @type {?} */
    ExpressionGroup.prototype.expressions;
    /** @type {?} */
    ExpressionGroup.prototype.joiner;
    /** @type {?} */
    ExpressionGroup.prototype.surroundExpressionWithBrackets;
}
/**
 * @param {?} map
 * @param {?} joiner
 * @return {?}
 */
export function createExpressionGroup(map, joiner) {
    /** @type {?} */
    const expressions = [];
    // should we use Object.keys()?
    for (const fieldName in map) {
        /** @type {?} */
        const fieldValue = map[fieldName];
        expressions.push(typeof fieldValue == 'string' ?
            new StringStatement(fieldName, (/** @type {?} */ (fieldValue))) :
            new NumberStatement(fieldName, fieldValue));
    }
    return new ExpressionGroup(expressions, joiner);
}
export class SingleExpression extends ExpressionGroup {
    /**
     * @param {?} expression
     */
    constructor(expression) {
        super([expression], undefined);
    }
}
export class AndExpressionGroup extends ExpressionGroup {
    /**
     * @param {?} expressions
     */
    constructor(expressions) {
        super(expressions, ExpressionGroupJoiner.AND);
    }
}
export class OrExpressionGroup extends ExpressionGroup {
    /**
     * @param {?} expressions
     */
    constructor(expressions) {
        super(expressions, ExpressionGroupJoiner.OR);
    }
}
export class UpdateStatements extends ExpressionGroup {
    /**
     * @param {?} expressions
     */
    constructor(expressions) {
        super(expressions, ExpressionGroupJoiner.COMMA, false);
    }
}
/**
 * @abstract
 */
export class FieldStatement extends Expression {
    /**
     * @param {?} fieldName
     * @param {?} value
     * @param {?} operator
     */
    constructor(fieldName, value, operator) {
        super(fieldName, operator, value);
    }
}
export class NumberStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     * @param {?} data
     * @param {?=} operator
     */
    constructor(fieldName, data, operator = ExpressionOperator.EQUAL) {
        super(fieldName, new MySQLNumberValue(data), operator);
    }
}
export class StringStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     * @param {?} data
     * @param {?=} operator
     */
    constructor(fieldName, data, operator = ExpressionOperator.EQUAL) {
        super(fieldName, new MySQLStringValue(data), operator);
    }
}
export class StringExpressionStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     * @param {?} data
     * @param {?=} operator
     */
    constructor(fieldName, data, operator = ExpressionOperator.EQUAL) {
        super(fieldName, new MySQLStringExpressionValue(data), operator);
    }
}
export class NullUpdateStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nullValue, ExpressionOperator.EQUAL);
    }
}
export class NowUpdateStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nowValue, ExpressionOperator.EQUAL);
    }
}
export class FieldIsNullStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nullValue, ExpressionOperator.IS);
    }
}
export class FieldIsNotNullStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nullValue, ExpressionOperator.IS_NOT);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtc3RhdGVtZW50cy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RhdGFiYXNlL2VuZ2luZS9teXNxbC9teXNxbC1zdGF0ZW1lbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE1BQVksY0FBYztJQUN6QixVQUFVLEdBQUE7SUFDVixNQUFNLEdBQUE7RUFDTjs7Ozs7QUFFRCxNQUFZLGtCQUFrQjtJQUM3QixLQUFLLEtBQXNCO0lBQzNCLFNBQVMsTUFBbUI7SUFFNUIsU0FBUyxLQUFrQjtJQUMzQixrQkFBa0IsTUFBVTtJQUM1QixZQUFZLEtBQWU7SUFDM0IscUJBQXFCLE1BQU87SUFFNUIsRUFBRSxNQUEwQjtJQUM1QixNQUFNLFVBQTBCO0VBQ2hDOzs7QUFFRCxNQUFZLHFCQUFxQjtJQUNoQyxHQUFHLE9BQVU7SUFDYixFQUFFLE1BQVU7SUFDWixLQUFLLEtBQU07RUFDWDs7Ozs7QUFlRCxpQ0FHQzs7O0lBRkEsMkJBQTBCOztJQUMxQiwrQkFBa0I7O0FBS25CLE1BQU0sT0FBTyxVQUFVOzs7OztJQUd0QixZQUNVLElBQXlCLEVBQ3pCLElBQW9CO1FBRHBCLFNBQUksR0FBSixJQUFJLENBQXFCO1FBQ3pCLFNBQUksR0FBSixJQUFJLENBQWdCO1FBRTdCLElBQUksQ0FBQyxRQUFRO1lBQ1osSUFBSSxJQUFJLGNBQWMsQ0FBQyxNQUFNLENBQUM7SUFDaEMsQ0FBQztDQUNEOzs7SUFUQSw4QkFBMkI7O0lBRzFCLDBCQUFrQzs7SUFDbEMsMEJBQTZCOztBQU8vQixNQUFNLGdCQUFpQixTQUFRLFVBQVU7Ozs7SUFDeEMsWUFBWSxJQUFZO1FBQ3ZCLEtBQUssQ0FBQyxJQUFJLEVBQUUsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7Q0FDRDtBQUVELE1BQU0sZ0JBQWlCLFNBQVEsVUFBVTs7OztJQUN4QyxZQUFZLElBQVk7UUFDdkIsS0FBSyxDQUFDLElBQUksRUFBRSxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDcEMsQ0FBQztDQUNEO0FBRUQsTUFBTSwwQkFBMkIsU0FBUSxVQUFVOzs7O0lBQ2xELFlBQVksSUFBWTtRQUN2QixLQUFLLENBQUMsSUFBSSxFQUFFLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN4QyxDQUFDO0NBQ0Q7O0FBR0QsTUFBWSxlQUFlO0lBQzFCLEdBQUcsU0FBVTtJQUNiLElBQUksUUFBUztFQUNiOzs7TUFFSyxRQUFRLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDOztNQUM5RCxTQUFTLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDOzs7Ozs7Ozs7Ozs7O0FBZXRFLE1BQU0sT0FBZ0IsVUFBVTs7Ozs7O0lBQy9CLFlBQ1UsUUFBZ0IsRUFDaEIsUUFBZ0IsRUFDaEIsU0FBcUI7UUFGckIsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUNoQixhQUFRLEdBQVIsUUFBUSxDQUFRO1FBQ2hCLGNBQVMsR0FBVCxTQUFTLENBQVk7SUFFL0IsQ0FBQztDQUNEOzs7SUFMQyw4QkFBeUI7O0lBQ3pCLDhCQUF5Qjs7SUFDekIsK0JBQThCOztBQU1oQyxNQUFNLE9BQU8sZUFBZTs7Ozs7O0lBQzNCLFlBQ1UsV0FBeUIsRUFDekIsTUFBNkIsRUFDN0IsaUNBQTBDLElBQUk7UUFGOUMsZ0JBQVcsR0FBWCxXQUFXLENBQWM7UUFDekIsV0FBTSxHQUFOLE1BQU0sQ0FBdUI7UUFDN0IsbUNBQThCLEdBQTlCLDhCQUE4QixDQUFnQjtJQUV4RCxDQUFDO0NBQ0Q7OztJQUxDLHNDQUFrQzs7SUFDbEMsaUNBQXNDOztJQUN0Qyx5REFBdUQ7Ozs7Ozs7QUFNekQsTUFBTSxVQUFVLHFCQUFxQixDQUNwQyxHQUFRLEVBQUUsTUFBNkI7O1VBRWpDLFdBQVcsR0FBaUIsRUFBRTtJQUVwQywrQkFBK0I7SUFDL0IsS0FBSyxNQUFNLFNBQVMsSUFBSSxHQUFHLEVBQUU7O2NBQ3RCLFVBQVUsR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDO1FBRWpDLFdBQVcsQ0FBQyxJQUFJLENBQ2YsT0FBTyxVQUFVLElBQUksUUFBUSxDQUFDLENBQUM7WUFDOUIsSUFBSSxlQUFlLENBQ2xCLFNBQVMsRUFDVCxtQkFBQSxVQUFVLEVBQVUsQ0FDcEIsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxlQUFlLENBQ2xCLFNBQVMsRUFDVCxVQUFVLENBQ1YsQ0FDRixDQUFDO0tBQ0Y7SUFFRCxPQUFPLElBQUksZUFBZSxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQztBQUNqRCxDQUFDO0FBR0QsTUFBTSxPQUFPLGdCQUFpQixTQUFRLGVBQWU7Ozs7SUFDcEQsWUFBWSxVQUFzQjtRQUNqQyxLQUFLLENBQUMsQ0FBQyxVQUFVLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUNoQyxDQUFDO0NBQ0Q7QUFDRCxNQUFNLE9BQU8sa0JBQW1CLFNBQVEsZUFBZTs7OztJQUN0RCxZQUFZLFdBQXlCO1FBQ3BDLEtBQUssQ0FBQyxXQUFXLEVBQUUscUJBQXFCLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDL0MsQ0FBQztDQUNEO0FBQ0QsTUFBTSxPQUFPLGlCQUFrQixTQUFRLGVBQWU7Ozs7SUFDckQsWUFBWSxXQUF5QjtRQUNwQyxLQUFLLENBQUMsV0FBVyxFQUFFLHFCQUFxQixDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzlDLENBQUM7Q0FDRDtBQUVELE1BQU0sT0FBTyxnQkFBaUIsU0FBUSxlQUFlOzs7O0lBQ3BELFlBQVksV0FBeUI7UUFDcEMsS0FBSyxDQUFDLFdBQVcsRUFBRSxxQkFBcUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDeEQsQ0FBQztDQUNEOzs7O0FBSUQsTUFBTSxPQUFnQixjQUFlLFNBQVEsVUFBVTs7Ozs7O0lBQ3RELFlBQ0MsU0FBaUIsRUFDakIsS0FBaUIsRUFDakIsUUFBNEI7UUFFNUIsS0FBSyxDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQztDQUNEO0FBR0QsTUFBTSxPQUFPLGVBQWdCLFNBQVEsY0FBYzs7Ozs7O0lBQ2xELFlBQ0MsU0FBaUIsRUFDakIsSUFBWSxFQUNaLFdBQStCLGtCQUFrQixDQUFDLEtBQUs7UUFFdkQsS0FBSyxDQUNKLFNBQVMsRUFDVCxJQUFJLGdCQUFnQixDQUFDLElBQUksQ0FBQyxFQUMxQixRQUFRLENBQ1IsQ0FBQztJQUNILENBQUM7Q0FDRDtBQUVELE1BQU0sT0FBTyxlQUFnQixTQUFRLGNBQWM7Ozs7OztJQUNsRCxZQUNDLFNBQWlCLEVBQ2pCLElBQVksRUFDWixXQUErQixrQkFBa0IsQ0FBQyxLQUFLO1FBRXZELEtBQUssQ0FDSixTQUFTLEVBQ1QsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFDMUIsUUFBUSxDQUNSLENBQUM7SUFDSCxDQUFDO0NBQ0Q7QUFFRCxNQUFNLE9BQU8seUJBQTBCLFNBQVEsY0FBYzs7Ozs7O0lBQzVELFlBQ0MsU0FBaUIsRUFDakIsSUFBWSxFQUNaLFdBQStCLGtCQUFrQixDQUFDLEtBQUs7UUFFdkQsS0FBSyxDQUNKLFNBQVMsRUFDVCxJQUFJLDBCQUEwQixDQUFDLElBQUksQ0FBQyxFQUNwQyxRQUFRLENBQ1IsQ0FBQztJQUNILENBQUM7Q0FDRDtBQUdELE1BQU0sT0FBTyxtQkFBb0IsU0FBUSxjQUFjOzs7O0lBQ3RELFlBQVksU0FBaUI7UUFDNUIsS0FBSyxDQUFDLFNBQVMsRUFBRSxTQUFTLEVBQUUsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdkQsQ0FBQztDQUNEO0FBRUQsTUFBTSxPQUFPLGtCQUFtQixTQUFRLGNBQWM7Ozs7SUFDckQsWUFBWSxTQUFpQjtRQUM1QixLQUFLLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN0RCxDQUFDO0NBQ0Q7QUFHRCxNQUFNLE9BQU8sb0JBQXFCLFNBQVEsY0FBYzs7OztJQUN2RCxZQUFZLFNBQWlCO1FBQzVCLEtBQUssQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLGtCQUFrQixDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3BELENBQUM7Q0FDRDtBQUVELE1BQU0sT0FBTyx1QkFBd0IsU0FBUSxjQUFjOzs7O0lBQzFELFlBQVksU0FBaUI7UUFDNUIsS0FBSyxDQUFDLFNBQVMsRUFBRSxTQUFTLEVBQUUsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDeEQsQ0FBQztDQUNEIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gTXlTUUxWYWx1ZVR5cGUge1xuXHRFWFBSRVNTSU9OLFxuXHRTVFJJTkdcbn1cblxuZXhwb3J0IGVudW0gRXhwcmVzc2lvbk9wZXJhdG9yIHtcblx0RVFVQUwgICAgICAgICAgICAgICAgID0gJz0nLFxuXHROT1RfRVFVQUwgICAgICAgICAgICAgPSAnIT0nLFxuXG5cdExFU1NfVEhBTiAgICAgICAgICAgICA9ICc8Jyxcblx0TEVTU19USEFOX09SX0VRVUFMICAgID0gJzw9Jyxcblx0R1JFQVRFUl9USEFOICAgICAgICAgID0gJz4nLFxuXHRHUkVBVEVSX1RIQU5fT1JfRVFVQUwgPSAnPj0nLFxuXG5cdElTICAgICAgICAgICAgICAgICAgICA9ICdJUycsXG5cdElTX05PVCAgICAgICAgICAgICAgICA9ICdJUyBOT1QnLFxufVxuXG5leHBvcnQgZW51bSBFeHByZXNzaW9uR3JvdXBKb2luZXIge1xuXHRBTkQgICA9ICdBTkQnLFxuXHRPUiAgICA9ICdPUicsXG5cdENPTU1BID0gJywnXG59XG5cblxuXG5leHBvcnQgdHlwZSBFeHByZXNzaW9uVmFsdWVUeXBlID0gc3RyaW5nfG51bWJlcjtcblxuXG5cbi8qXG5pbnRlcmZhY2UgSU15U1FMVmFsdWU8VCBleHRlbmRzIEV4cHJlc3Npb25WYWx1ZVR5cGU+XG57XG4gICAgZGF0YTpULFxuICAgIC8vdHlwZTpNeVNRTFZhbHVlVHlwZVxufVxuKi9cbmV4cG9ydCBpbnRlcmZhY2UgSU15U1FMVmFsdWUge1xuXHRkYXRhOiBFeHByZXNzaW9uVmFsdWVUeXBlO1xuXHRpc1N0cmluZzogYm9vbGVhbjtcbn1cblxuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTFZhbHVlIGltcGxlbWVudHMgSU15U1FMVmFsdWUge1xuXHRyZWFkb25seSBpc1N0cmluZzogYm9vbGVhbjtcblxuXHRjb25zdHJ1Y3Rvcihcblx0XHRyZWFkb25seSBkYXRhOiBFeHByZXNzaW9uVmFsdWVUeXBlLFxuXHRcdHJlYWRvbmx5IHR5cGU6IE15U1FMVmFsdWVUeXBlXG5cdCkge1xuXHRcdHRoaXMuaXNTdHJpbmcgPVxuXHRcdFx0dHlwZSA9PSBNeVNRTFZhbHVlVHlwZS5TVFJJTkc7XG5cdH1cbn1cblxuY2xhc3MgTXlTUUxOdW1iZXJWYWx1ZSBleHRlbmRzIE15U1FMVmFsdWUge1xuXHRjb25zdHJ1Y3RvcihkYXRhOiBudW1iZXIpIHtcblx0XHRzdXBlcihkYXRhLCBNeVNRTFZhbHVlVHlwZS5FWFBSRVNTSU9OKTtcblx0fVxufVxuXG5jbGFzcyBNeVNRTFN0cmluZ1ZhbHVlIGV4dGVuZHMgTXlTUUxWYWx1ZSB7XG5cdGNvbnN0cnVjdG9yKGRhdGE6IHN0cmluZykge1xuXHRcdHN1cGVyKGRhdGEsIE15U1FMVmFsdWVUeXBlLlNUUklORyk7XG5cdH1cbn1cblxuY2xhc3MgTXlTUUxTdHJpbmdFeHByZXNzaW9uVmFsdWUgZXh0ZW5kcyBNeVNRTFZhbHVlIHtcblx0Y29uc3RydWN0b3IoZGF0YTogc3RyaW5nKSB7XG5cdFx0c3VwZXIoZGF0YSwgTXlTUUxWYWx1ZVR5cGUuRVhQUkVTU0lPTik7XG5cdH1cbn1cblxuXG5leHBvcnQgZW51bSBNeVNRTEV4cHJlc3Npb24ge1xuXHROT1cgPSAnTk9XKCknLFxuXHROVUxMID0gJ05VTEwnLFxufVxuXG5jb25zdCBub3dWYWx1ZSA9IG5ldyBNeVNRTFN0cmluZ0V4cHJlc3Npb25WYWx1ZShNeVNRTEV4cHJlc3Npb24uTk9XKTtcbmNvbnN0IG51bGxWYWx1ZSA9IG5ldyBNeVNRTFN0cmluZ0V4cHJlc3Npb25WYWx1ZShNeVNRTEV4cHJlc3Npb24uTlVMTCk7XG5cblxuLypcbmNsYXNzIE15U1FMVmFsdWVcbntcbiAgICBzdGF0aWMgYm9vbGVhbkFzSW50KGJvb2w6Ym9vbGVhbik6bnVtYmVyXG4gICAge1xuICAgICAgICByZXR1cm4gYm9vbCA/IDEgOiAwO1xuICAgIH1cbn1cbiovXG5cblxuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRXhwcmVzc2lvbiB7XG5cdGNvbnN0cnVjdG9yKFxuXHRcdHJlYWRvbmx5IGxlZnRTaWRlOiBzdHJpbmcsXG5cdFx0cmVhZG9ubHkgb3BlcmF0b3I6IHN0cmluZyxcblx0XHRyZWFkb25seSByaWdodFNpZGU6IE15U1FMVmFsdWVcblx0KSB7XG5cdH1cbn1cblxuXG5leHBvcnQgY2xhc3MgRXhwcmVzc2lvbkdyb3VwIHtcblx0Y29uc3RydWN0b3IoXG5cdFx0cmVhZG9ubHkgZXhwcmVzc2lvbnM6IEV4cHJlc3Npb25bXSxcblx0XHRyZWFkb25seSBqb2luZXI6IEV4cHJlc3Npb25Hcm91cEpvaW5lcixcblx0XHRyZWFkb25seSBzdXJyb3VuZEV4cHJlc3Npb25XaXRoQnJhY2tldHM6IGJvb2xlYW4gPSB0cnVlXG5cdCkge1xuXHR9XG59XG5cblxuZXhwb3J0IGZ1bmN0aW9uIGNyZWF0ZUV4cHJlc3Npb25Hcm91cChcblx0bWFwOiBhbnksIGpvaW5lcjogRXhwcmVzc2lvbkdyb3VwSm9pbmVyXG4pIHtcblx0Y29uc3QgZXhwcmVzc2lvbnM6IEV4cHJlc3Npb25bXSA9IFtdO1xuXG5cdC8vIHNob3VsZCB3ZSB1c2UgT2JqZWN0LmtleXMoKT9cblx0Zm9yIChjb25zdCBmaWVsZE5hbWUgaW4gbWFwKSB7XG5cdFx0Y29uc3QgZmllbGRWYWx1ZSA9IG1hcFtmaWVsZE5hbWVdO1xuXG5cdFx0ZXhwcmVzc2lvbnMucHVzaChcblx0XHRcdHR5cGVvZiBmaWVsZFZhbHVlID09ICdzdHJpbmcnID9cblx0XHRcdFx0bmV3IFN0cmluZ1N0YXRlbWVudChcblx0XHRcdFx0XHRmaWVsZE5hbWUsXG5cdFx0XHRcdFx0ZmllbGRWYWx1ZSBhcyBzdHJpbmdcblx0XHRcdFx0KSA6XG5cdFx0XHRcdG5ldyBOdW1iZXJTdGF0ZW1lbnQoXG5cdFx0XHRcdFx0ZmllbGROYW1lLFxuXHRcdFx0XHRcdGZpZWxkVmFsdWVcblx0XHRcdFx0KVxuXHRcdCk7XG5cdH1cblxuXHRyZXR1cm4gbmV3IEV4cHJlc3Npb25Hcm91cChleHByZXNzaW9ucywgam9pbmVyKTtcbn1cblxuXG5leHBvcnQgY2xhc3MgU2luZ2xlRXhwcmVzc2lvbiBleHRlbmRzIEV4cHJlc3Npb25Hcm91cCB7XG5cdGNvbnN0cnVjdG9yKGV4cHJlc3Npb246IEV4cHJlc3Npb24pIHtcblx0XHRzdXBlcihbZXhwcmVzc2lvbl0sIHVuZGVmaW5lZCk7XG5cdH1cbn1cbmV4cG9ydCBjbGFzcyBBbmRFeHByZXNzaW9uR3JvdXAgZXh0ZW5kcyBFeHByZXNzaW9uR3JvdXAge1xuXHRjb25zdHJ1Y3RvcihleHByZXNzaW9uczogRXhwcmVzc2lvbltdKSB7XG5cdFx0c3VwZXIoZXhwcmVzc2lvbnMsIEV4cHJlc3Npb25Hcm91cEpvaW5lci5BTkQpO1xuXHR9XG59XG5leHBvcnQgY2xhc3MgT3JFeHByZXNzaW9uR3JvdXAgZXh0ZW5kcyBFeHByZXNzaW9uR3JvdXAge1xuXHRjb25zdHJ1Y3RvcihleHByZXNzaW9uczogRXhwcmVzc2lvbltdKSB7XG5cdFx0c3VwZXIoZXhwcmVzc2lvbnMsIEV4cHJlc3Npb25Hcm91cEpvaW5lci5PUik7XG5cdH1cbn1cblxuZXhwb3J0IGNsYXNzIFVwZGF0ZVN0YXRlbWVudHMgZXh0ZW5kcyBFeHByZXNzaW9uR3JvdXAge1xuXHRjb25zdHJ1Y3RvcihleHByZXNzaW9uczogRXhwcmVzc2lvbltdKSB7XG5cdFx0c3VwZXIoZXhwcmVzc2lvbnMsIEV4cHJlc3Npb25Hcm91cEpvaW5lci5DT01NQSwgZmFsc2UpO1xuXHR9XG59XG5cblxuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRmllbGRTdGF0ZW1lbnQgZXh0ZW5kcyBFeHByZXNzaW9uIHtcblx0Y29uc3RydWN0b3IoXG5cdFx0ZmllbGROYW1lOiBzdHJpbmcsXG5cdFx0dmFsdWU6IE15U1FMVmFsdWUsXG5cdFx0b3BlcmF0b3I6IEV4cHJlc3Npb25PcGVyYXRvclxuXHQpIHtcblx0XHRzdXBlcihmaWVsZE5hbWUsIG9wZXJhdG9yLCB2YWx1ZSk7XG5cdH1cbn1cblxuXG5leHBvcnQgY2xhc3MgTnVtYmVyU3RhdGVtZW50IGV4dGVuZHMgRmllbGRTdGF0ZW1lbnQge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRmaWVsZE5hbWU6IHN0cmluZyxcblx0XHRkYXRhOiBudW1iZXIsXG5cdFx0b3BlcmF0b3I6IEV4cHJlc3Npb25PcGVyYXRvciA9IEV4cHJlc3Npb25PcGVyYXRvci5FUVVBTFxuXHQpIHtcblx0XHRzdXBlcihcblx0XHRcdGZpZWxkTmFtZSxcblx0XHRcdG5ldyBNeVNRTE51bWJlclZhbHVlKGRhdGEpLFxuXHRcdFx0b3BlcmF0b3Jcblx0XHQpO1xuXHR9XG59XG5cbmV4cG9ydCBjbGFzcyBTdHJpbmdTdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudCB7XG5cdGNvbnN0cnVjdG9yKFxuXHRcdGZpZWxkTmFtZTogc3RyaW5nLFxuXHRcdGRhdGE6IHN0cmluZyxcblx0XHRvcGVyYXRvcjogRXhwcmVzc2lvbk9wZXJhdG9yID0gRXhwcmVzc2lvbk9wZXJhdG9yLkVRVUFMXG5cdCkge1xuXHRcdHN1cGVyKFxuXHRcdFx0ZmllbGROYW1lLFxuXHRcdFx0bmV3IE15U1FMU3RyaW5nVmFsdWUoZGF0YSksXG5cdFx0XHRvcGVyYXRvclxuXHRcdCk7XG5cdH1cbn1cblxuZXhwb3J0IGNsYXNzIFN0cmluZ0V4cHJlc3Npb25TdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudCB7XG5cdGNvbnN0cnVjdG9yKFxuXHRcdGZpZWxkTmFtZTogc3RyaW5nLFxuXHRcdGRhdGE6IHN0cmluZyxcblx0XHRvcGVyYXRvcjogRXhwcmVzc2lvbk9wZXJhdG9yID0gRXhwcmVzc2lvbk9wZXJhdG9yLkVRVUFMXG5cdCkge1xuXHRcdHN1cGVyKFxuXHRcdFx0ZmllbGROYW1lLFxuXHRcdFx0bmV3IE15U1FMU3RyaW5nRXhwcmVzc2lvblZhbHVlKGRhdGEpLFxuXHRcdFx0b3BlcmF0b3Jcblx0XHQpO1xuXHR9XG59XG5cblxuZXhwb3J0IGNsYXNzIE51bGxVcGRhdGVTdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudCB7XG5cdGNvbnN0cnVjdG9yKGZpZWxkTmFtZTogc3RyaW5nKSB7XG5cdFx0c3VwZXIoZmllbGROYW1lLCBudWxsVmFsdWUsIEV4cHJlc3Npb25PcGVyYXRvci5FUVVBTCk7XG5cdH1cbn1cblxuZXhwb3J0IGNsYXNzIE5vd1VwZGF0ZVN0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50IHtcblx0Y29uc3RydWN0b3IoZmllbGROYW1lOiBzdHJpbmcpIHtcblx0XHRzdXBlcihmaWVsZE5hbWUsIG5vd1ZhbHVlLCBFeHByZXNzaW9uT3BlcmF0b3IuRVFVQUwpO1xuXHR9XG59XG5cblxuZXhwb3J0IGNsYXNzIEZpZWxkSXNOdWxsU3RhdGVtZW50IGV4dGVuZHMgRmllbGRTdGF0ZW1lbnQge1xuXHRjb25zdHJ1Y3RvcihmaWVsZE5hbWU6IHN0cmluZykge1xuXHRcdHN1cGVyKGZpZWxkTmFtZSwgbnVsbFZhbHVlLCBFeHByZXNzaW9uT3BlcmF0b3IuSVMpO1xuXHR9XG59XG5cbmV4cG9ydCBjbGFzcyBGaWVsZElzTm90TnVsbFN0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50IHtcblx0Y29uc3RydWN0b3IoZmllbGROYW1lOiBzdHJpbmcpIHtcblx0XHRzdXBlcihmaWVsZE5hbWUsIG51bGxWYWx1ZSwgRXhwcmVzc2lvbk9wZXJhdG9yLklTX05PVCk7XG5cdH1cbn1cbiJdfQ==