/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-transaction-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { logWithTime } from 'earnbet-common';
import { MySQLConnectionManager } from './mysql-connection-manager';
import { executeQuery } from './mysql-query';
export class MySQLTransactionManager {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.queue = [];
        this.isExecutingTransaction = false;
        /**
         * we need to manage a queue of transactions
         * to be executed in order
         * once the transaction either is successful or fails
         * then move on to the next
         */
        this.performNextTransaction = (/**
         * @return {?}
         */
        () => {
            if (this.isExecutingTransaction ||
                this.queue.length < 1) {
                return;
            }
            logWithTime('perform next transaction!');
            this.isExecutingTransaction = true;
            // remove first element from queue and return it
            /** @type {?} */
            const executor = this.queue.shift();
            executor.execute();
        });
        this.onTransactionCompleted = (/**
         * @return {?}
         */
        () => {
            logWithTime('onTransactionCompleted');
            this.isExecutingTransaction = false;
            this.performNextTransaction();
        });
        this.connectionManager = new MySQLConnectionManager(config);
    }
    /**
     * @param {?} queries
     * @return {?}
     */
    performTransaction(queries) {
        return this.performPreparedTransaction(queries.map((/**
         * @param {?} sql
         * @return {?}
         */
        sql => ({ sql, argumentsToPrepare: undefined }))));
    }
    /**
     * @param {?} data
     * @return {?}
     */
    performPreparedTransaction(data) {
        /*
                    we need to promise the result of each query executed
                    so that they can be inspected by consumer for Ids, etc
                    */
        /** @type {?} */
        const executor = new MySQLTransactionExecuter(data, this.performNextTransaction, this.onTransactionCompleted, this.connectionManager);
        // add executor to queue
        this.queue.push(executor);
        return new Promise(executor.init);
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.queue;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.isExecutingTransaction;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.connectionManager;
    /**
     * we need to manage a queue of transactions
     * to be executed in order
     * once the transaction either is successful or fails
     * then move on to the next
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.performNextTransaction;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.onTransactionCompleted;
}
class MySQLTransactionExecuter {
    /**
     * @param {?} queries
     * @param {?} onInitHandler
     * @param {?} onCompleteHandler
     * @param {?} connectionManager
     */
    constructor(queries, onInitHandler, onCompleteHandler, connectionManager) {
        this.queries = queries;
        this.onInitHandler = onInitHandler;
        this.onCompleteHandler = onCompleteHandler;
        this.connectionManager = connectionManager;
        this.init = (/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.resolve = resolve;
            this.reject = reject;
            this.onInitHandler();
        });
    }
    /**
     * @return {?}
     */
    execute() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            // only need to get connection once?
            this.connection = yield this.connectionManager.getConnection();
            this.connection.beginTransaction((/**
             * @return {?}
             */
            () => tslib_1.__awaiter(this, void 0, void 0, function* () {
                try {
                    /** @type {?} */
                    const results = [];
                    for (const { sql, argumentsToPrepare } of this.queries) {
                        /** @type {?} */
                        const result = yield executeQuery(sql, this.connection, argumentsToPrepare);
                        results.push(result);
                    }
                    yield this.commit();
                    this.resolve(results);
                }
                catch (e) {
                    yield this.rollback();
                    this.reject(e);
                }
                this.onCompleteHandler();
            })));
        });
    }
    /**
     * @private
     * @return {?}
     */
    commit() {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.connection.commit((/**
             * @param {?} error
             * @return {?}
             */
            (error) => tslib_1.__awaiter(this, void 0, void 0, function* () {
                if (error) {
                    logWithTime('ERROR in committing queries', this.queries);
                    return reject(error);
                }
                resolve();
            })));
        }));
    }
    /**
     * @private
     * @return {?}
     */
    rollback() {
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        (resolve) => {
            this.connection.rollback(resolve);
        }));
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.resolve;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.reject;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.connection;
    /** @type {?} */
    MySQLTransactionExecuter.prototype.init;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.queries;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.onInitHandler;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.onCompleteHandler;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.connectionManager;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtdHJhbnNhY3Rpb24tbWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RhdGFiYXNlL2VuZ2luZS9teXNxbC9teXNxbC10cmFuc2FjdGlvbi1tYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUVBLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUczQyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRzdDLE1BQU0sT0FBTyx1QkFBdUI7Ozs7SUFNbkMsWUFBWSxNQUFvQjtRQUx4QixVQUFLLEdBQStCLEVBQUUsQ0FBQztRQUN2QywyQkFBc0IsR0FBRyxLQUFLLENBQUM7Ozs7Ozs7UUF5Qy9CLDJCQUFzQjs7O1FBQUcsR0FBRyxFQUFFO1lBQ3JDLElBQ0MsSUFBSSxDQUFDLHNCQUFzQjtnQkFDM0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUNwQjtnQkFDRCxPQUFPO2FBQ1A7WUFFRCxXQUFXLENBQUMsMkJBQTJCLENBQUMsQ0FBQztZQUV6QyxJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDOzs7a0JBRzdCLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTtZQUVuQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDcEIsQ0FBQyxFQUFBO1FBRU8sMkJBQXNCOzs7UUFBRyxHQUFHLEVBQUU7WUFDckMsV0FBVyxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFFdEMsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztZQUVwQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUMvQixDQUFDLEVBQUE7UUE1REEsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksc0JBQXNCLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDN0QsQ0FBQzs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxPQUFpQjtRQUVuQyxPQUFPLElBQUksQ0FBQywwQkFBMEIsQ0FDckMsT0FBTyxDQUFDLEdBQUc7Ozs7UUFDVixHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBQyxHQUFHLEVBQUUsa0JBQWtCLEVBQUUsU0FBUyxFQUFDLENBQUMsRUFDN0MsQ0FDRCxDQUFDO0lBQ0gsQ0FBQzs7Ozs7SUFFRCwwQkFBMEIsQ0FBQyxJQUFrQjs7Ozs7O2NBS3RDLFFBQVEsR0FBRyxJQUFJLHdCQUF3QixDQUM1QyxJQUFJLEVBQ0osSUFBSSxDQUFDLHNCQUFzQixFQUMzQixJQUFJLENBQUMsc0JBQXNCLEVBQzNCLElBQUksQ0FBQyxpQkFBaUIsQ0FDdEI7UUFFRCx3QkFBd0I7UUFDeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFMUIsT0FBTyxJQUFJLE9BQU8sQ0FBc0IsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRXhELENBQUM7Q0FnQ0Q7Ozs7OztJQW5FQSx3Q0FBK0M7Ozs7O0lBQy9DLHlEQUF1Qzs7Ozs7SUFFdkMsb0RBQTJEOzs7Ozs7Ozs7SUF1QzNELHlEQWdCQzs7Ozs7SUFFRCx5REFNQzs7QUFLRixNQUFNLHdCQUF3Qjs7Ozs7OztJQUs3QixZQUNTLE9BQXFCLEVBQ3JCLGFBQXVCLEVBQ3ZCLGlCQUEyQixFQUMzQixpQkFBeUM7UUFIekMsWUFBTyxHQUFQLE9BQU8sQ0FBYztRQUNyQixrQkFBYSxHQUFiLGFBQWEsQ0FBVTtRQUN2QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQVU7UUFDM0Isc0JBQWlCLEdBQWpCLGlCQUFpQixDQUF3QjtRQUlsRCxTQUFJOzs7OztRQUFHLENBQUMsT0FBd0IsRUFBRSxNQUFnQixFQUFFLEVBQUU7WUFDckQsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7WUFDdkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7WUFFckIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3RCLENBQUMsRUFBQTtJQVBELENBQUM7Ozs7SUFTSyxPQUFPOztZQUNaLG9DQUFvQztZQUNwQyxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxDQUFDO1lBRS9ELElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCOzs7WUFBQyxHQUFTLEVBQUU7Z0JBQzNDLElBQUk7OzBCQUNHLE9BQU8sR0FBd0IsRUFBRTtvQkFFdkMsS0FBSyxNQUFNLEVBQUMsR0FBRyxFQUFFLGtCQUFrQixFQUFDLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTs7OEJBQy9DLE1BQU0sR0FBRyxNQUFNLFlBQVksQ0FDaEMsR0FBRyxFQUNILElBQUksQ0FBQyxVQUFVLEVBQ2Ysa0JBQWtCLENBQ2xCO3dCQUVELE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7cUJBQ3JCO29CQUVELE1BQU0sSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO29CQUVwQixJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUN0QjtnQkFBQyxPQUFPLENBQUMsRUFBRTtvQkFDWCxNQUFNLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFFdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDZjtnQkFFRCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztZQUMxQixDQUFDLENBQUEsRUFBQyxDQUFDO1FBQ0osQ0FBQztLQUFBOzs7OztJQUVPLE1BQU07UUFDYixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUN0QyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU07Ozs7WUFBQyxDQUFPLEtBQUssRUFBRSxFQUFFO2dCQUN0QyxJQUFJLEtBQUssRUFBRTtvQkFDVixXQUFXLENBQUMsNkJBQTZCLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUV6RCxPQUFPLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDckI7Z0JBRUQsT0FBTyxFQUFFLENBQUM7WUFDWCxDQUFDLENBQUEsRUFBQyxDQUFDO1FBQ0osQ0FBQyxFQUFDLENBQUM7SUFDSixDQUFDOzs7OztJQUVPLFFBQVE7UUFDZixPQUFPLElBQUksT0FBTzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbkMsQ0FBQyxFQUFDLENBQUM7SUFDSixDQUFDO0NBQ0Q7Ozs7OztJQXJFQSwyQ0FBaUM7Ozs7O0lBQ2pDLDBDQUF5Qjs7Ozs7SUFDekIsOENBQStCOztJQVUvQix3Q0FLQzs7Ozs7SUFaQSwyQ0FBNkI7Ozs7O0lBQzdCLGlEQUErQjs7Ozs7SUFDL0IscURBQW1DOzs7OztJQUNuQyxxREFBaUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb25uZWN0aW9uIH0gZnJvbSAnbXlzcWwnO1xuXG5pbXBvcnQge2xvZ1dpdGhUaW1lfSBmcm9tICdlYXJuYmV0LWNvbW1vbic7XG5cbmltcG9ydCB7IElNeVNRTENvbmZpZywgSU15U1FMUXVlcnlSZXN1bHQsIElRdWVyeURhdGEgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgTXlTUUxDb25uZWN0aW9uTWFuYWdlciB9IGZyb20gJy4vbXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyJztcbmltcG9ydCB7IGV4ZWN1dGVRdWVyeSB9IGZyb20gJy4vbXlzcWwtcXVlcnknO1xuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTFRyYW5zYWN0aW9uTWFuYWdlciB7XG5cdHByaXZhdGUgcXVldWU6IE15U1FMVHJhbnNhY3Rpb25FeGVjdXRlcltdID0gW107XG5cdHByaXZhdGUgaXNFeGVjdXRpbmdUcmFuc2FjdGlvbiA9IGZhbHNlO1xuXG5cdHByaXZhdGUgcmVhZG9ubHkgY29ubmVjdGlvbk1hbmFnZXI6IE15U1FMQ29ubmVjdGlvbk1hbmFnZXI7XG5cblx0Y29uc3RydWN0b3IoY29uZmlnOiBJTXlTUUxDb25maWcpIHtcblx0XHR0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyID0gbmV3IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIoY29uZmlnKTtcblx0fVxuXG5cdHBlcmZvcm1UcmFuc2FjdGlvbihxdWVyaWVzOiBzdHJpbmdbXSkge1xuXG5cdFx0cmV0dXJuIHRoaXMucGVyZm9ybVByZXBhcmVkVHJhbnNhY3Rpb24oXG5cdFx0XHRxdWVyaWVzLm1hcChcblx0XHRcdFx0c3FsID0+ICh7c3FsLCBhcmd1bWVudHNUb1ByZXBhcmU6IHVuZGVmaW5lZH0pXG5cdFx0XHQpXG5cdFx0KTtcblx0fVxuXG5cdHBlcmZvcm1QcmVwYXJlZFRyYW5zYWN0aW9uKGRhdGE6IElRdWVyeURhdGFbXSkge1xuXHRcdFx0LypcbiAgICAgICAgICAgIHdlIG5lZWQgdG8gcHJvbWlzZSB0aGUgcmVzdWx0IG9mIGVhY2ggcXVlcnkgZXhlY3V0ZWRcbiAgICAgICAgICAgIHNvIHRoYXQgdGhleSBjYW4gYmUgaW5zcGVjdGVkIGJ5IGNvbnN1bWVyIGZvciBJZHMsIGV0Y1xuICAgICAgICAgICAgKi9cblx0XHRjb25zdCBleGVjdXRvciA9IG5ldyBNeVNRTFRyYW5zYWN0aW9uRXhlY3V0ZXIoXG5cdFx0XHRkYXRhLFxuXHRcdFx0dGhpcy5wZXJmb3JtTmV4dFRyYW5zYWN0aW9uLFxuXHRcdFx0dGhpcy5vblRyYW5zYWN0aW9uQ29tcGxldGVkLFxuXHRcdFx0dGhpcy5jb25uZWN0aW9uTWFuYWdlclxuXHRcdCk7XG5cblx0XHQvLyBhZGQgZXhlY3V0b3IgdG8gcXVldWVcblx0XHR0aGlzLnF1ZXVlLnB1c2goZXhlY3V0b3IpO1xuXG5cdFx0cmV0dXJuIG5ldyBQcm9taXNlPElNeVNRTFF1ZXJ5UmVzdWx0W10+KGV4ZWN1dG9yLmluaXQpO1xuXG5cdH1cblxuXHQvKioqIHdlIG5lZWQgdG8gbWFuYWdlIGEgcXVldWUgb2YgdHJhbnNhY3Rpb25zXG4gICAgICogdG8gYmUgZXhlY3V0ZWQgaW4gb3JkZXJcbiAgICAgKiBvbmNlIHRoZSB0cmFuc2FjdGlvbiBlaXRoZXIgaXMgc3VjY2Vzc2Z1bCBvciBmYWlsc1xuICAgICAqIHRoZW4gbW92ZSBvbiB0byB0aGUgbmV4dFxuICAgICAqL1xuXHRwcml2YXRlIHBlcmZvcm1OZXh0VHJhbnNhY3Rpb24gPSAoKSA9PiB7XG5cdFx0aWYgKFxuXHRcdFx0dGhpcy5pc0V4ZWN1dGluZ1RyYW5zYWN0aW9uIHx8XG5cdFx0XHR0aGlzLnF1ZXVlLmxlbmd0aCA8IDFcblx0XHQpIHtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cblx0XHRsb2dXaXRoVGltZSgncGVyZm9ybSBuZXh0IHRyYW5zYWN0aW9uIScpO1xuXG5cdFx0dGhpcy5pc0V4ZWN1dGluZ1RyYW5zYWN0aW9uID0gdHJ1ZTtcblxuXHRcdC8vIHJlbW92ZSBmaXJzdCBlbGVtZW50IGZyb20gcXVldWUgYW5kIHJldHVybiBpdFxuXHRcdGNvbnN0IGV4ZWN1dG9yID0gdGhpcy5xdWV1ZS5zaGlmdCgpO1xuXG5cdFx0ZXhlY3V0b3IuZXhlY3V0ZSgpO1xuXHR9XG5cblx0cHJpdmF0ZSBvblRyYW5zYWN0aW9uQ29tcGxldGVkID0gKCkgPT4ge1xuXHRcdGxvZ1dpdGhUaW1lKCdvblRyYW5zYWN0aW9uQ29tcGxldGVkJyk7XG5cblx0XHR0aGlzLmlzRXhlY3V0aW5nVHJhbnNhY3Rpb24gPSBmYWxzZTtcblxuXHRcdHRoaXMucGVyZm9ybU5leHRUcmFuc2FjdGlvbigpO1xuXHR9XG59XG5cbnR5cGUgUmVzb2x2ZUZ1bmN0aW9uID0gKHJlc3VsdHM6IElNeVNRTFF1ZXJ5UmVzdWx0W10pID0+IHZvaWQ7XG5cbmNsYXNzIE15U1FMVHJhbnNhY3Rpb25FeGVjdXRlciB7XG5cdHByaXZhdGUgcmVzb2x2ZTogUmVzb2x2ZUZ1bmN0aW9uO1xuXHRwcml2YXRlIHJlamVjdDogRnVuY3Rpb247XG5cdHByaXZhdGUgY29ubmVjdGlvbjogQ29ubmVjdGlvbjtcblxuXHRjb25zdHJ1Y3Rvcihcblx0XHRwcml2YXRlIHF1ZXJpZXM6IElRdWVyeURhdGFbXSxcblx0XHRwcml2YXRlIG9uSW5pdEhhbmRsZXI6IEZ1bmN0aW9uLFxuXHRcdHByaXZhdGUgb25Db21wbGV0ZUhhbmRsZXI6IEZ1bmN0aW9uLFxuXHRcdHByaXZhdGUgY29ubmVjdGlvbk1hbmFnZXI6IE15U1FMQ29ubmVjdGlvbk1hbmFnZXJcblx0KSB7XG5cdH1cblxuXHRpbml0ID0gKHJlc29sdmU6IFJlc29sdmVGdW5jdGlvbiwgcmVqZWN0OiBGdW5jdGlvbikgPT4ge1xuXHRcdHRoaXMucmVzb2x2ZSA9IHJlc29sdmU7XG5cdFx0dGhpcy5yZWplY3QgPSByZWplY3Q7XG5cblx0XHR0aGlzLm9uSW5pdEhhbmRsZXIoKTtcblx0fVxuXG5cdGFzeW5jIGV4ZWN1dGUoKSB7XG5cdFx0Ly8gb25seSBuZWVkIHRvIGdldCBjb25uZWN0aW9uIG9uY2U/XG5cdFx0dGhpcy5jb25uZWN0aW9uID0gYXdhaXQgdGhpcy5jb25uZWN0aW9uTWFuYWdlci5nZXRDb25uZWN0aW9uKCk7XG5cblx0XHR0aGlzLmNvbm5lY3Rpb24uYmVnaW5UcmFuc2FjdGlvbihhc3luYyAoKSA9PiB7XG5cdFx0XHR0cnkge1xuXHRcdFx0XHRjb25zdCByZXN1bHRzOiBJTXlTUUxRdWVyeVJlc3VsdFtdID0gW107XG5cblx0XHRcdFx0Zm9yIChjb25zdCB7c3FsLCBhcmd1bWVudHNUb1ByZXBhcmV9IG9mIHRoaXMucXVlcmllcykge1xuXHRcdFx0XHRcdGNvbnN0IHJlc3VsdCA9IGF3YWl0IGV4ZWN1dGVRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4oXG5cdFx0XHRcdFx0XHRzcWwsXG5cdFx0XHRcdFx0XHR0aGlzLmNvbm5lY3Rpb24sXG5cdFx0XHRcdFx0XHRhcmd1bWVudHNUb1ByZXBhcmVcblx0XHRcdFx0XHQpO1xuXG5cdFx0XHRcdFx0cmVzdWx0cy5wdXNoKHJlc3VsdCk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRhd2FpdCB0aGlzLmNvbW1pdCgpO1xuXG5cdFx0XHRcdHRoaXMucmVzb2x2ZShyZXN1bHRzKTtcblx0XHRcdH0gY2F0Y2ggKGUpIHtcblx0XHRcdFx0YXdhaXQgdGhpcy5yb2xsYmFjaygpO1xuXG5cdFx0XHRcdHRoaXMucmVqZWN0KGUpO1xuXHRcdFx0fVxuXG5cdFx0XHR0aGlzLm9uQ29tcGxldGVIYW5kbGVyKCk7XG5cdFx0fSk7XG5cdH1cblxuXHRwcml2YXRlIGNvbW1pdCgpIHtcblx0XHRyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdFx0dGhpcy5jb25uZWN0aW9uLmNvbW1pdChhc3luYyAoZXJyb3IpID0+IHtcblx0XHRcdFx0aWYgKGVycm9yKSB7XG5cdFx0XHRcdFx0bG9nV2l0aFRpbWUoJ0VSUk9SIGluIGNvbW1pdHRpbmcgcXVlcmllcycsIHRoaXMucXVlcmllcyk7XG5cblx0XHRcdFx0XHRyZXR1cm4gcmVqZWN0KGVycm9yKTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdHJlc29sdmUoKTtcblx0XHRcdH0pO1xuXHRcdH0pO1xuXHR9XG5cblx0cHJpdmF0ZSByb2xsYmFjaygpIHtcblx0XHRyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcblx0XHRcdHRoaXMuY29ubmVjdGlvbi5yb2xsYmFjayhyZXNvbHZlKTtcblx0XHR9KTtcblx0fVxufVxuIl19