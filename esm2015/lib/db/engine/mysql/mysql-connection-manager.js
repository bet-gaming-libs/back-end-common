/**
 * @fileoverview added by tsickle
 * Generated from: lib/db/engine/mysql/mysql-connection-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import * as mysql from "mysql";
import { sleep } from 'earnbet-common';
export class MySQLConnectionManager {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.config = config;
        this.isConnecting = false;
        this.connect();
    }
    /**
     * @return {?}
     */
    getConnection() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            yield this.reconnect();
            return this.connection;
        });
    }
    /**
     * @private
     * @return {?}
     */
    reconnect() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            while (this.isDisconnected) {
                /** @type {?} */
                const isConnected = yield this.connect();
                if (isConnected) {
                    return;
                }
                yield sleep(1000);
            }
        });
    }
    /**
     * @private
     * @return {?}
     */
    connect() {
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        (resolve) => {
            if (!this.isDisconnected) {
                resolve(true);
                return;
            }
            if (this.isConnecting) {
                resolve(false);
                return;
            }
            this.isConnecting = true;
            // end existing connection
            if (this.connection) {
                this.connection.end();
            }
            // connection parameters
            this.connection = mysql.createConnection(this.config);
            // establish connection
            this.connection.connect((/**
             * @param {?} err
             * @return {?}
             */
            (err) => {
                console.log(this.config);
                if (err) {
                    console.error('error connecting: ' + err.stack);
                    resolve(false);
                }
                else {
                    console.log('connected as id ' + this.connection.threadId);
                    resolve(true);
                }
                this.isConnecting = false;
            }));
            this.connection.on('error', (/**
             * @param {?} err
             * @return {?}
             */
            (err) => {
                console.error(err);
            }));
        }));
    }
    /**
     * @private
     * @return {?}
     */
    get isDisconnected() {
        return this.connection == undefined ||
            this.connection.state === 'disconnected';
    }
    /**
     * @return {?}
     */
    endConnection() {
        this.connection.end();
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.isConnecting;
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.connection;
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.config;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvZGIvZW5naW5lL215c3FsL215c3FsLWNvbm5lY3Rpb24tbWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUUvQixPQUFPLEVBQUMsS0FBSyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFJckMsTUFBTSxPQUFPLHNCQUFzQjs7OztJQU0vQixZQUFvQixNQUFtQjtRQUFuQixXQUFNLEdBQU4sTUFBTSxDQUFhO1FBSi9CLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBS3pCLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNuQixDQUFDOzs7O0lBRUssYUFBYTs7WUFDZixNQUFNLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUV2QixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDM0IsQ0FBQztLQUFBOzs7OztJQUVhLFNBQVM7O1lBQ25CLE9BQU8sSUFBSSxDQUFDLGNBQWMsRUFBRTs7c0JBQ2xCLFdBQVcsR0FBRyxNQUFNLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBRXhDLElBQUksV0FBVyxFQUFFO29CQUNiLE9BQU87aUJBQ1Y7Z0JBRUQsTUFBTSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDckI7UUFDTCxDQUFDO0tBQUE7Ozs7O0lBRU8sT0FBTztRQUVYLE9BQU8sSUFBSSxPQUFPOzs7O1FBQVcsQ0FBQyxPQUFPLEVBQUMsRUFBRTtZQUNwQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtnQkFDdEIsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNkLE9BQU87YUFDVjtZQUdELElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDbkIsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNmLE9BQU87YUFDVjtZQUdELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBR3pCLDBCQUEwQjtZQUMxQixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUM7YUFDekI7WUFHRCx3QkFBd0I7WUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRXRELHVCQUF1QjtZQUN2QixJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU87Ozs7WUFBQyxDQUFDLEdBQUcsRUFBQyxFQUFFO2dCQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFHekIsSUFBSSxHQUFHLEVBQUU7b0JBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBRWhELE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDbEI7cUJBQU07b0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUUzRCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2pCO2dCQUVELElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzlCLENBQUMsRUFBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsT0FBTzs7OztZQUFDLENBQUMsR0FBRyxFQUFDLEVBQUU7Z0JBQzlCLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdkIsQ0FBQyxFQUFDLENBQUM7UUFDUCxDQUFDLEVBQUUsQ0FBQztJQUNSLENBQUM7Ozs7O0lBRUQsSUFBWSxjQUFjO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLFVBQVUsSUFBSSxTQUFTO1lBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxLQUFLLGNBQWMsQ0FBQztJQUNyRCxDQUFDOzs7O0lBRUQsYUFBYTtRQUNULElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDMUIsQ0FBQztDQUNKOzs7Ozs7SUFyRkcsOENBQTZCOzs7OztJQUU3Qiw0Q0FBb0M7Ozs7O0lBRXhCLHdDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIG15c3FsIGZyb20gXCJteXNxbFwiO1xuXG5pbXBvcnQge3NsZWVwfSBmcm9tICdlYXJuYmV0LWNvbW1vbic7XG5pbXBvcnQgeyBJTXlTUUxDb25maWcgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyXG57XG4gICAgcHJpdmF0ZSBpc0Nvbm5lY3RpbmcgPSBmYWxzZTtcblxuICAgIHByaXZhdGUgY29ubmVjdGlvbjpteXNxbC5Db25uZWN0aW9uO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjb25maWc6SU15U1FMQ29uZmlnKSB7XG4gICAgICAgIHRoaXMuY29ubmVjdCgpO1xuICAgIH1cblxuICAgIGFzeW5jIGdldENvbm5lY3Rpb24oKTpQcm9taXNlPG15c3FsLkNvbm5lY3Rpb24+IHtcbiAgICAgICAgYXdhaXQgdGhpcy5yZWNvbm5lY3QoKTtcblxuICAgICAgICByZXR1cm4gdGhpcy5jb25uZWN0aW9uO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgcmVjb25uZWN0KCkge1xuICAgICAgICB3aGlsZSAodGhpcy5pc0Rpc2Nvbm5lY3RlZCkge1xuICAgICAgICAgICAgY29uc3QgaXNDb25uZWN0ZWQgPSBhd2FpdCB0aGlzLmNvbm5lY3QoKTtcblxuICAgICAgICAgICAgaWYgKGlzQ29ubmVjdGVkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBhd2FpdCBzbGVlcCgxMDAwKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgY29ubmVjdCgpOlByb21pc2U8Ym9vbGVhbj5cbiAgICB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxib29sZWFuPiggKHJlc29sdmUpPT57XG4gICAgICAgICAgICBpZiAoIXRoaXMuaXNEaXNjb25uZWN0ZWQpIHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKHRydWUpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgICBpZiAodGhpcy5pc0Nvbm5lY3RpbmcpIHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKGZhbHNlKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgdGhpcy5pc0Nvbm5lY3RpbmcgPSB0cnVlO1xuXG5cbiAgICAgICAgICAgIC8vIGVuZCBleGlzdGluZyBjb25uZWN0aW9uXG4gICAgICAgICAgICBpZiAodGhpcy5jb25uZWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uLmVuZCgpO1xuICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgIC8vIGNvbm5lY3Rpb24gcGFyYW1ldGVyc1xuICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uID0gbXlzcWwuY3JlYXRlQ29ubmVjdGlvbih0aGlzLmNvbmZpZyk7XG5cbiAgICAgICAgICAgIC8vIGVzdGFibGlzaCBjb25uZWN0aW9uXG4gICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb24uY29ubmVjdCgoZXJyKT0+e1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMuY29uZmlnKTtcblxuXG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdlcnJvciBjb25uZWN0aW5nOiAnICsgZXJyLnN0YWNrKTtcblxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKGZhbHNlKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnY29ubmVjdGVkIGFzIGlkICcgKyB0aGlzLmNvbm5lY3Rpb24udGhyZWFkSWQpO1xuXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUodHJ1ZSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdGhpcy5pc0Nvbm5lY3RpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb24ub24oJ2Vycm9yJywoZXJyKT0+e1xuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9ICk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXQgaXNEaXNjb25uZWN0ZWQoKTpib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29ubmVjdGlvbiA9PSB1bmRlZmluZWQgfHxcbiAgICAgICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb24uc3RhdGUgPT09ICdkaXNjb25uZWN0ZWQnO1xuICAgIH1cblxuICAgIGVuZENvbm5lY3Rpb24oKSB7XG4gICAgICAgIHRoaXMuY29ubmVjdGlvbi5lbmQoKTtcbiAgICB9XG59Il19