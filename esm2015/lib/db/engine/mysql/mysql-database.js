/**
 * @fileoverview added by tsickle
 * Generated from: lib/db/engine/mysql/mysql-database.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { MySQLQueryBuilder } from './mysql-query-builder';
import { MySQLTransactionManager } from './mysql-transaction-manager';
export class MySQLDatabase {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.builder = new MySQLQueryBuilder(config);
        this.transactionManager = new MySQLTransactionManager(config);
    }
}
if (false) {
    /** @type {?} */
    MySQLDatabase.prototype.builder;
    /** @type {?} */
    MySQLDatabase.prototype.transactionManager;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtZGF0YWJhc2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9kYi9lbmdpbmUvbXlzcWwvbXlzcWwtZGF0YWJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFDQSxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSx1QkFBdUIsQ0FBQztBQUN4RCxPQUFPLEVBQUMsdUJBQXVCLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQUdwRSxNQUFNLE9BQU8sYUFBYTs7OztJQUt0QixZQUFZLE1BQW1CO1FBQzNCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSx1QkFBdUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNsRSxDQUFDO0NBQ0o7OztJQVBHLGdDQUFtQzs7SUFDbkMsMkNBQW9EIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSU15U1FMRGF0YWJhc2UsIElNeVNRTENvbmZpZyB9IGZyb20gXCIuL2ludGVyZmFjZXNcIjtcbmltcG9ydCB7TXlTUUxRdWVyeUJ1aWxkZXJ9IGZyb20gJy4vbXlzcWwtcXVlcnktYnVpbGRlcic7XG5pbXBvcnQge015U1FMVHJhbnNhY3Rpb25NYW5hZ2VyfSBmcm9tICcuL215c3FsLXRyYW5zYWN0aW9uLW1hbmFnZXInO1xuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTERhdGFiYXNlIGltcGxlbWVudHMgSU15U1FMRGF0YWJhc2VcbntcbiAgICByZWFkb25seSBidWlsZGVyOk15U1FMUXVlcnlCdWlsZGVyO1xuICAgIHJlYWRvbmx5IHRyYW5zYWN0aW9uTWFuYWdlcjpNeVNRTFRyYW5zYWN0aW9uTWFuYWdlcjtcblxuICAgIGNvbnN0cnVjdG9yKGNvbmZpZzpJTXlTUUxDb25maWcpIHtcbiAgICAgICAgdGhpcy5idWlsZGVyID0gbmV3IE15U1FMUXVlcnlCdWlsZGVyKGNvbmZpZyk7XG4gICAgICAgIHRoaXMudHJhbnNhY3Rpb25NYW5hZ2VyID0gbmV3IE15U1FMVHJhbnNhY3Rpb25NYW5hZ2VyKGNvbmZpZyk7XG4gICAgfVxufSJdfQ==