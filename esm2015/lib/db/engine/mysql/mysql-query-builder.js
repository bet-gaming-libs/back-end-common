/**
 * @fileoverview added by tsickle
 * Generated from: lib/db/engine/mysql/mysql-query-builder.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { MySQLConnectionManager } from "./mysql-connection-manager";
import { MySQLExpression, SingleExpression, StringStatement, OrExpressionGroup, UpdateStatements, createExpressionGroup, ExpressionGroupJoiner } from "./mysql-statements";
import { MySQLQuery } from './mysql-query';
export class MySQLQueryBuilder {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.connectionManager = new MySQLConnectionManager(config);
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    insert(repository, entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            // construct query
            /** @type {?} */
            const query = yield this.constructInsertQuery(repository, entities);
            return new MySQLQuery(query, this.connectionManager);
        });
    }
    /**
     * @private
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    constructInsertQuery(repository, entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const fields = [];
            for (const fieldName in entities[0]) {
                fields.push(fieldName);
            }
            /** @type {?} */
            const rows = [];
            for (const entity of entities) {
                rows.push(yield this.constructRowForInsert(entity));
            }
            return "INSERT INTO " + repository.tableName +
                ' (' + this.prepareFieldList(fields) + ')' +
                ' VALUES ' + rows.join(', ') + ';';
        });
    }
    /**
     * @private
     * @param {?} entity
     * @return {?}
     */
    constructRowForInsert(entity) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const values = [];
            for (const propertyName in entity) {
                /** @type {?} */
                const rawValue = entity[propertyName];
                /** @type {?} */
                const preparedValue = yield this.prepareValueForInsert(rawValue);
                values.push(preparedValue);
            }
            return '(' + values.join(',') + ')';
        });
    }
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    prepareValueForInsert(value) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            switch (typeof value) {
                case "string":
                    switch (value) {
                        case MySQLExpression.NULL:
                        case MySQLExpression.NOW:
                            break;
                        default:
                            value = yield this.escape(value);
                            break;
                    }
                    break;
            }
            return "" + value;
        });
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @param {?} primaryKeyValues
     * @return {?}
     */
    selectByPrimaryKeys(repository, fields, primaryKeyValues) {
        /** @type {?} */
        const query = this.prepareFieldsForSelect(fields, repository.tableName) +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @return {?}
     */
    selectAll(repository, fields) {
        /** @type {?} */
        const query = this.prepareFieldsForSelect(fields, repository.tableName) + ';';
        return new MySQLQuery(query, this.connectionManager);
    }
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    select(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.prepareSelectStatement(params);
            return new MySQLQuery(`${query};`, this.connectionManager);
        });
    }
    /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    prepareSelectStatement(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { fields, repository } = params;
            /** @type {?} */
            let query = this.prepareFieldsForSelect(fields, repository.tableName);
            query += yield this.prepareWhereOrderAndLimit(params);
            return query;
        });
    }
    /**
     * @private
     * @param {?} fields
     * @param {?} tableName
     * @return {?}
     */
    prepareFieldsForSelect(fields, tableName) {
        /** @type {?} */
        const fieldsList = this.prepareFieldList(fields);
        return `SELECT ${fieldsList} FROM ${tableName}`;
    }
    /**
     * @private
     * @param {?} fields
     * @return {?}
     */
    prepareFieldList(fields) {
        /** @type {?} */
        const prepared = [];
        for (const field of fields) {
            /** @type {?} */
            const isAlias = field.indexOf(' AS ') > -1;
            prepared.push(isAlias ?
                field :
                '`' + field + '`');
        }
        return prepared.join(',');
    }
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    update(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (params.singleStatement) {
                params.statements = [
                    params.singleStatement
                ];
            }
            const { statements, repository } = params;
            if (!statements ||
                statements.length < 1) {
                throw new Error('there must be at least 1 update statement!');
            }
            /** @type {?} */
            const updateStatements = yield this.prepareExpressionGroup(new UpdateStatements(statements));
            /** @type {?} */
            const remainingQuery = yield this.prepareWhereOrderAndLimit(params);
            /** @type {?} */
            const query = 'UPDATE ' + repository.tableName +
                ' SET ' + updateStatements +
                remainingQuery + ';';
            return new MySQLQuery(query, this.connectionManager);
        });
    }
    /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    prepareWhereOrderAndLimit(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (params.whereMap) {
                params.whereAndMap = params.whereMap;
            }
            if (params.whereAndMap ||
                params.whereOrMap) {
                params.whereGroup = createExpressionGroup(params.whereAndMap ?
                    params.whereAndMap :
                    params.whereOrMap, params.whereAndMap ?
                    ExpressionGroupJoiner.AND :
                    ExpressionGroupJoiner.OR);
            }
            else if (params.wherePrimaryKeys) {
                /** @type {?} */
                const expressions = params.wherePrimaryKeys.map((/**
                 * @param {?} primaryKeyValue
                 * @return {?}
                 */
                (primaryKeyValue) => new StringStatement(params.repository.primaryKey, '' + primaryKeyValue)));
                params.whereGroup = new OrExpressionGroup(expressions);
            }
            else if (params.whereClause) {
                params.whereGroup = new SingleExpression(params.whereClause);
            }
            const { whereGroup, orderBy, limit } = params;
            /** @type {?} */
            let query = '';
            /** @type {?} */
            const whereClause = whereGroup &&
                (yield this.prepareExpressionGroup(whereGroup));
            if (whereClause) {
                query += ` 
                WHERE ${whereClause}`;
            }
            if (orderBy) {
                query += ` 
                ORDER BY ${orderBy}`;
            }
            if (limit) {
                query += ` 
                LIMIT ${limit}`;
            }
            return query;
        });
    }
    /**
     * @private
     * @param {?} group
     * @return {?}
     */
    prepareExpressionGroup(group) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const statements = [];
            /** @type {?} */
            const useBrackets = group.surroundExpressionWithBrackets;
            for (const expression of group.expressions) {
                const { leftSide, operator, rightSide } = expression;
                /** @type {?} */
                const rightSideValue = rightSide.isString ?
                    `${yield this.escape((/** @type {?} */ (rightSide.data)))}` :
                    rightSide.data;
                statements.push(`${useBrackets ? '(' : ''} \`${leftSide}\` ${operator} ${rightSideValue} ${useBrackets ? ')' : ''}`);
            }
            return statements.join(` ${group.joiner} `);
        });
    }
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    escape(value) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const connection = yield this.connectionManager.getConnection();
            return connection.escape(value);
        });
    }
    /**
     * @param {?} repository
     * @param {?} primaryKeyValues
     * @return {?}
     */
    delete(repository, primaryKeyValues) {
        if (primaryKeyValues.length < 1) {
            throw new Error('Please specify which records to delete!');
        }
        /** @type {?} */
        const query = 'DELETE FROM ' + repository.tableName +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    }
    /**
     * @template T
     * @param {?} sql
     * @return {?}
     */
    rawQuery(sql) {
        return new MySQLQuery(sql, this.connectionManager);
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLQueryBuilder.prototype.connectionManager;
}
/**
 * @param {?} repository
 * @param {?} primaryKeyValues
 * @return {?}
 */
function constructWhereWithPrimaryKeys(repository, primaryKeyValues) {
    /** @type {?} */
    const conditions = [];
    for (let primaryKeyValue of primaryKeyValues) {
        if (repository.isPrimaryKeyAString) {
            primaryKeyValue = "'" + primaryKeyValue + "'";
        }
        /** @type {?} */
        const condition = repository.primaryKey + ' = ' + primaryKeyValue;
        conditions.push(condition);
    }
    return ' WHERE ' + conditions.join(' OR ');
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcXVlcnktYnVpbGRlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL2VuZ2luZS9teXNxbC9teXNxbC1xdWVyeS1idWlsZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBbUIsZUFBZSxFQUFFLGdCQUFnQixFQUFFLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxnQkFBZ0IsRUFBRSxxQkFBcUIsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQzVMLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFHekMsTUFBTSxPQUFPLGlCQUFpQjs7OztJQUkxQixZQUFZLE1BQW1CO1FBQzNCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Ozs7Ozs7SUFFWSxNQUFNLENBQ2YsVUFBMkIsRUFDM0IsUUFBWTs7OztrQkFHTixLQUFLLEdBQUcsTUFBTSxJQUFJLENBQUMsb0JBQW9CLENBQ3pDLFVBQVUsRUFBQyxRQUFRLENBQ3RCO1lBRUQsT0FBTyxJQUFJLFVBQVUsQ0FBb0IsS0FBSyxFQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQzNFLENBQUM7S0FBQTs7Ozs7OztJQUNhLG9CQUFvQixDQUM5QixVQUEyQixFQUMzQixRQUFjOzs7a0JBR1IsTUFBTSxHQUFZLEVBQUU7WUFDMUIsS0FBSyxNQUFNLFNBQVMsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ2pDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDMUI7O2tCQUVLLElBQUksR0FBWSxFQUFFO1lBQ3hCLEtBQUssTUFBTSxNQUFNLElBQUksUUFBUSxFQUFFO2dCQUMzQixJQUFJLENBQUMsSUFBSSxDQUNMLE1BQU0sSUFBSSxDQUFDLHFCQUFxQixDQUM1QixNQUFNLENBQ1QsQ0FDSixDQUFDO2FBQ0w7WUFFRCxPQUFPLGNBQWMsR0FBRyxVQUFVLENBQUMsU0FBUztnQkFDeEMsSUFBSSxHQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsR0FBRSxHQUFHO2dCQUN4QyxVQUFVLEdBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBQyxHQUFHLENBQUE7UUFFdEMsQ0FBQztLQUFBOzs7Ozs7SUFDYSxxQkFBcUIsQ0FDL0IsTUFBVTs7O2tCQUdKLE1BQU0sR0FBWSxFQUFFO1lBRTFCLEtBQUssTUFBTSxZQUFZLElBQUksTUFBTSxFQUFFOztzQkFDekIsUUFBUSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUM7O3NCQUUvQixhQUFhLEdBQ2YsTUFBTSxJQUFJLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDO2dCQUU5QyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQzlCO1lBRUQsT0FBTyxHQUFHLEdBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRSxHQUFHLENBQUM7UUFDdEMsQ0FBQztLQUFBOzs7Ozs7SUFDYSxxQkFBcUIsQ0FBQyxLQUFTOztZQUV6QyxRQUFRLE9BQU8sS0FBSyxFQUFFO2dCQUNsQixLQUFLLFFBQVE7b0JBQ1QsUUFBUSxLQUFLLEVBQUU7d0JBQ1gsS0FBSyxlQUFlLENBQUMsSUFBSSxDQUFDO3dCQUMxQixLQUFLLGVBQWUsQ0FBQyxHQUFHOzRCQUN4QixNQUFNO3dCQUVOOzRCQUNJLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQ3JDLE1BQU07cUJBQ1Q7b0JBQ0wsTUFBTTthQUNUO1lBRUQsT0FBTyxFQUFFLEdBQUMsS0FBSyxDQUFDO1FBQ3BCLENBQUM7S0FBQTs7Ozs7Ozs7SUFHTSxtQkFBbUIsQ0FDdEIsVUFBMkIsRUFDM0IsTUFBZSxFQUNmLGdCQUFrQzs7Y0FHNUIsS0FBSyxHQUNQLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQztZQUN4RCw2QkFBNkIsQ0FBQyxVQUFVLEVBQUMsZ0JBQWdCLENBQUMsR0FBRSxHQUFHO1FBRW5FLE9BQU8sSUFBSSxVQUFVLENBQU0sS0FBSyxFQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQzdELENBQUM7Ozs7Ozs7SUFFTSxTQUFTLENBQ1osVUFBMkIsRUFDM0IsTUFBZTs7Y0FFVCxLQUFLLEdBQ1AsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sRUFBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsR0FBRztRQUVsRSxPQUFPLElBQUksVUFBVSxDQUFNLEtBQUssRUFBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUM3RCxDQUFDOzs7Ozs7SUFHWSxNQUFNLENBQUksTUFBbUM7OztrQkFDaEQsS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQztZQUV2RCxPQUFPLElBQUksVUFBVSxDQUFNLEdBQUcsS0FBSyxHQUFHLEVBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDbkUsQ0FBQztLQUFBOzs7Ozs7O0lBRWEsc0JBQXNCLENBQUksTUFBbUM7O2tCQUVqRSxFQUFDLE1BQU0sRUFBQyxVQUFVLEVBQUMsR0FBRyxNQUFNOztnQkFFOUIsS0FBSyxHQUFVLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQztZQUUzRSxLQUFLLElBQUksTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFdEQsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQztLQUFBOzs7Ozs7O0lBRU8sc0JBQXNCLENBQUMsTUFBZSxFQUFDLFNBQWdCOztjQUNyRCxVQUFVLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQztRQUVoRCxPQUFPLFVBQVUsVUFBVSxTQUFTLFNBQVMsRUFBRSxDQUFDO0lBQ3BELENBQUM7Ozs7OztJQUdPLGdCQUFnQixDQUFDLE1BQWU7O2NBRTlCLFFBQVEsR0FBWSxFQUFFO1FBRTVCLEtBQUssTUFBTSxLQUFLLElBQUksTUFBTSxFQUFFOztrQkFDbEIsT0FBTyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRTFDLFFBQVEsQ0FBQyxJQUFJLENBQ1QsT0FBTyxDQUFDLENBQUM7Z0JBQ1QsS0FBSyxDQUFDLENBQUM7Z0JBQ1AsR0FBRyxHQUFDLEtBQUssR0FBQyxHQUFHLENBQ2hCLENBQUM7U0FDTDtRQUVELE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUM5QixDQUFDOzs7Ozs7SUFFWSxNQUFNLENBQUksTUFBbUM7O1lBQ3RELElBQUksTUFBTSxDQUFDLGVBQWUsRUFBRTtnQkFDeEIsTUFBTSxDQUFDLFVBQVUsR0FBRztvQkFDaEIsTUFBTSxDQUFDLGVBQWU7aUJBQ3pCLENBQUM7YUFDTDtrQkFHSyxFQUFDLFVBQVUsRUFBQyxVQUFVLEVBQUMsR0FBRyxNQUFNO1lBRXRDLElBQ0ksQ0FBQyxVQUFVO2dCQUNYLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUN2QjtnQkFDRSxNQUFNLElBQUksS0FBSyxDQUFDLDRDQUE0QyxDQUFDLENBQUM7YUFDakU7O2tCQUdLLGdCQUFnQixHQUFVLE1BQU0sSUFBSSxDQUFDLHNCQUFzQixDQUM3RCxJQUFJLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxDQUNuQzs7a0JBRUssY0FBYyxHQUFVLE1BQU0sSUFBSSxDQUFDLHlCQUF5QixDQUM5RCxNQUFNLENBQ1Q7O2tCQUVLLEtBQUssR0FDUCxTQUFTLEdBQUMsVUFBVSxDQUFDLFNBQVM7Z0JBQzlCLE9BQU8sR0FBRSxnQkFBZ0I7Z0JBQ3hCLGNBQWMsR0FBRSxHQUFHO1lBRXhCLE9BQU8sSUFBSSxVQUFVLENBQW9CLEtBQUssRUFBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUMzRSxDQUFDO0tBQUE7Ozs7Ozs7SUFFYSx5QkFBeUIsQ0FBSSxNQUE2Qjs7WUFDcEUsSUFBSSxNQUFNLENBQUMsUUFBUSxFQUFFO2dCQUNqQixNQUFNLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUM7YUFDeEM7WUFFRCxJQUNJLE1BQU0sQ0FBQyxXQUFXO2dCQUNsQixNQUFNLENBQUMsVUFBVSxFQUNuQjtnQkFDRSxNQUFNLENBQUMsVUFBVSxHQUFHLHFCQUFxQixDQUNyQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQ2hCLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDcEIsTUFBTSxDQUFDLFVBQVUsRUFDckIsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUNoQixxQkFBcUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDM0IscUJBQXFCLENBQUMsRUFBRSxDQUMvQixDQUFDO2FBQ0w7aUJBQ0ksSUFBSSxNQUFNLENBQUMsZ0JBQWdCLEVBQUU7O3NCQUN4QixXQUFXLEdBQXFCLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHOzs7O2dCQUM3RCxDQUFDLGVBQWUsRUFBRSxFQUFFLENBQUMsSUFBSSxlQUFlLENBQ3BDLE1BQU0sQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUM1QixFQUFFLEdBQUMsZUFBZSxDQUNyQixFQUNKO2dCQUVELE1BQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUMxRDtpQkFDSSxJQUFJLE1BQU0sQ0FBQyxXQUFXLEVBQUU7Z0JBQ3pCLE1BQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxnQkFBZ0IsQ0FDcEMsTUFBTSxDQUFDLFdBQVcsQ0FDckIsQ0FBQzthQUNMO2tCQUdLLEVBQUMsVUFBVSxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsR0FBRyxNQUFNOztnQkFFckMsS0FBSyxHQUFHLEVBQUU7O2tCQUVSLFdBQVcsR0FDYixVQUFVO2lCQUNWLE1BQU0sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxDQUFBO1lBRWpELElBQUksV0FBVyxFQUFFO2dCQUNiLEtBQUssSUFBSTt3QkFDRyxXQUFXLEVBQUUsQ0FBQzthQUM3QjtZQUVELElBQUksT0FBTyxFQUFFO2dCQUNULEtBQUssSUFBSTsyQkFDTSxPQUFPLEVBQUUsQ0FBQzthQUM1QjtZQUVELElBQUksS0FBSyxFQUFFO2dCQUNQLEtBQUssSUFBSTt3QkFDRyxLQUFLLEVBQUUsQ0FBQzthQUN2QjtZQUVELE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7S0FBQTs7Ozs7O0lBRWEsc0JBQXNCLENBQUMsS0FBcUI7OztrQkFDaEQsVUFBVSxHQUFZLEVBQUU7O2tCQUV4QixXQUFXLEdBQUcsS0FBSyxDQUFDLDhCQUE4QjtZQUV4RCxLQUFLLE1BQU0sVUFBVSxJQUFJLEtBQUssQ0FBQyxXQUFXLEVBQUU7c0JBQ2xDLEVBQUMsUUFBUSxFQUFDLFFBQVEsRUFBQyxTQUFTLEVBQUMsR0FBRyxVQUFVOztzQkFFMUMsY0FBYyxHQUNoQixTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ2hCLEdBQUksTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFBLFNBQVMsQ0FBQyxJQUFJLEVBQVUsQ0FBRSxFQUFFLENBQUMsQ0FBQztvQkFDcEQsU0FBUyxDQUFDLElBQUk7Z0JBRXRCLFVBQVUsQ0FBQyxJQUFJLENBQ1gsR0FDSSxXQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFDeEIsTUFBTSxRQUFRLE1BQU0sUUFBUSxJQUFJLGNBQWMsSUFDMUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQ3hCLEVBQUUsQ0FBQyxDQUFDO2FBQ1g7WUFFRCxPQUFPLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUNoRCxDQUFDO0tBQUE7Ozs7OztJQUdhLE1BQU0sQ0FBQyxLQUFZOzs7a0JBRXZCLFVBQVUsR0FBRyxNQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUU7WUFFL0QsT0FBTyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BDLENBQUM7S0FBQTs7Ozs7O0lBRU0sTUFBTSxDQUNULFVBQTJCLEVBQzNCLGdCQUFzQjtRQUd0QixJQUFJLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDN0IsTUFBTSxJQUFJLEtBQUssQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFDO1NBQzlEOztjQUdLLEtBQUssR0FBRyxjQUFjLEdBQUMsVUFBVSxDQUFDLFNBQVM7WUFDckMsNkJBQTZCLENBQUMsVUFBVSxFQUFDLGdCQUFnQixDQUFDLEdBQUMsR0FBRztRQUUxRSxPQUFPLElBQUksVUFBVSxDQUFvQixLQUFLLEVBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDM0UsQ0FBQzs7Ozs7O0lBRUQsUUFBUSxDQUFJLEdBQVc7UUFDbkIsT0FBTyxJQUFJLFVBQVUsQ0FBSSxHQUFHLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDMUQsQ0FBQztDQUNKOzs7Ozs7SUFqU0csOENBQWlEOzs7Ozs7O0FBb1NyRCxTQUFTLDZCQUE2QixDQUNsQyxVQUEyQixFQUMzQixnQkFBc0I7O1VBR2hCLFVBQVUsR0FBWSxFQUFFO0lBRTlCLEtBQUssSUFBSSxlQUFlLElBQUksZ0JBQWdCLEVBQUU7UUFDMUMsSUFBSSxVQUFVLENBQUMsbUJBQW1CLEVBQUU7WUFDaEMsZUFBZSxHQUFHLEdBQUcsR0FBQyxlQUFlLEdBQUMsR0FBRyxDQUFDO1NBQzdDOztjQUVLLFNBQVMsR0FBRyxVQUFVLENBQUMsVUFBVSxHQUFHLEtBQUssR0FBRyxlQUFlO1FBQ2pFLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7S0FDOUI7SUFFRCxPQUFPLFNBQVMsR0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQzdDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJTXlTUUxSZXBvc2l0b3J5LCBQcmltYXJ5S2V5VmFsdWUsIElNeVNRTFF1ZXJ5UmVzdWx0LCBJTXlTUUxDb25maWcsIElTZWxlY3RRdWVyeUJ1aWxkZXJQYXJhbXMsIElVcGRhdGVRdWVyeUJ1aWxkZXJQYXJhbXMsIElRdWVyeUJ1aWxkZXJQYXJhbXMgfSBmcm9tIFwiLi9pbnRlcmZhY2VzXCI7XG5pbXBvcnQgeyBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyIH0gZnJvbSBcIi4vbXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyXCI7XG5pbXBvcnQgeyBFeHByZXNzaW9uR3JvdXAsIE15U1FMRXhwcmVzc2lvbiwgU2luZ2xlRXhwcmVzc2lvbiwgU3RyaW5nU3RhdGVtZW50LCBPckV4cHJlc3Npb25Hcm91cCwgVXBkYXRlU3RhdGVtZW50cywgY3JlYXRlRXhwcmVzc2lvbkdyb3VwLCBFeHByZXNzaW9uR3JvdXBKb2luZXIgfSBmcm9tIFwiLi9teXNxbC1zdGF0ZW1lbnRzXCI7XG5pbXBvcnQge015U1FMUXVlcnl9IGZyb20gJy4vbXlzcWwtcXVlcnknO1xuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTFF1ZXJ5QnVpbGRlclxue1xuICAgIHByaXZhdGUgY29ubmVjdGlvbk1hbmFnZXI6TXlTUUxDb25uZWN0aW9uTWFuYWdlcjtcblxuICAgIGNvbnN0cnVjdG9yKGNvbmZpZzpJTXlTUUxDb25maWcpIHtcbiAgICAgICAgdGhpcy5jb25uZWN0aW9uTWFuYWdlciA9IG5ldyBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyKGNvbmZpZyk7XG4gICAgfVxuXG4gICAgcHVibGljIGFzeW5jIGluc2VydDxUPihcbiAgICAgICAgcmVwb3NpdG9yeTpJTXlTUUxSZXBvc2l0b3J5LFxuICAgICAgICBlbnRpdGllczpUW11cbiAgICApIHtcbiAgICAgICAgLy8gY29uc3RydWN0IHF1ZXJ5XG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gYXdhaXQgdGhpcy5jb25zdHJ1Y3RJbnNlcnRRdWVyeShcbiAgICAgICAgICAgIHJlcG9zaXRvcnksZW50aXRpZXNcbiAgICAgICAgKTtcblxuICAgICAgICByZXR1cm4gbmV3IE15U1FMUXVlcnk8SU15U1FMUXVlcnlSZXN1bHQ+KHF1ZXJ5LHRoaXMuY29ubmVjdGlvbk1hbmFnZXIpO1xuICAgIH1cbiAgICBwcml2YXRlIGFzeW5jIGNvbnN0cnVjdEluc2VydFF1ZXJ5KFxuICAgICAgICByZXBvc2l0b3J5OklNeVNRTFJlcG9zaXRvcnksXG4gICAgICAgIGVudGl0aWVzOmFueVtdXG4gICAgKTpQcm9taXNlPHN0cmluZz5cbiAgICB7XG4gICAgICAgIGNvbnN0IGZpZWxkczpzdHJpbmdbXSA9IFtdO1xuICAgICAgICBmb3IgKGNvbnN0IGZpZWxkTmFtZSBpbiBlbnRpdGllc1swXSkge1xuICAgICAgICAgICAgZmllbGRzLnB1c2goZmllbGROYW1lKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IHJvd3M6c3RyaW5nW10gPSBbXTtcbiAgICAgICAgZm9yIChjb25zdCBlbnRpdHkgb2YgZW50aXRpZXMpIHtcbiAgICAgICAgICAgIHJvd3MucHVzaChcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmNvbnN0cnVjdFJvd0Zvckluc2VydChcbiAgICAgICAgICAgICAgICAgICAgZW50aXR5XG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBcIklOU0VSVCBJTlRPIFwiICsgcmVwb3NpdG9yeS50YWJsZU5hbWUgK1xuICAgICAgICAgICAgJyAoJysgdGhpcy5wcmVwYXJlRmllbGRMaXN0KGZpZWxkcykgKycpJyArXG4gICAgICAgICAgICAnIFZBTFVFUyAnK3Jvd3Muam9pbignLCAnKSsnOydcblxuICAgIH1cbiAgICBwcml2YXRlIGFzeW5jIGNvbnN0cnVjdFJvd0Zvckluc2VydChcbiAgICAgICAgZW50aXR5OmFueVxuICAgICk6UHJvbWlzZTxzdHJpbmc+XG4gICAge1xuICAgICAgICBjb25zdCB2YWx1ZXM6c3RyaW5nW10gPSBbXTtcblxuICAgICAgICBmb3IgKGNvbnN0IHByb3BlcnR5TmFtZSBpbiBlbnRpdHkpIHtcbiAgICAgICAgICAgIGNvbnN0IHJhd1ZhbHVlID0gZW50aXR5W3Byb3BlcnR5TmFtZV07XG5cbiAgICAgICAgICAgIGNvbnN0IHByZXBhcmVkVmFsdWU6c3RyaW5nID1cbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLnByZXBhcmVWYWx1ZUZvckluc2VydChyYXdWYWx1ZSk7XG5cbiAgICAgICAgICAgIHZhbHVlcy5wdXNoKHByZXBhcmVkVmFsdWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuICcoJysgdmFsdWVzLmpvaW4oJywnKSArJyknO1xuICAgIH1cbiAgICBwcml2YXRlIGFzeW5jIHByZXBhcmVWYWx1ZUZvckluc2VydCh2YWx1ZTphbnkpOlByb21pc2U8c3RyaW5nPlxuICAgIHtcbiAgICAgICAgc3dpdGNoICh0eXBlb2YgdmFsdWUpIHtcbiAgICAgICAgICAgIGNhc2UgXCJzdHJpbmdcIjpcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgTXlTUUxFeHByZXNzaW9uLk5VTEw6XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgTXlTUUxFeHByZXNzaW9uLk5PVzpcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlID0gYXdhaXQgdGhpcy5lc2NhcGUodmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBcIlwiK3ZhbHVlO1xuICAgIH1cblxuXG4gICAgcHVibGljIHNlbGVjdEJ5UHJpbWFyeUtleXM8VD4oXG4gICAgICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeSxcbiAgICAgICAgZmllbGRzOnN0cmluZ1tdLFxuICAgICAgICBwcmltYXJ5S2V5VmFsdWVzOlByaW1hcnlLZXlWYWx1ZVtdXG4gICAgKVxuICAgIHtcbiAgICAgICAgY29uc3QgcXVlcnkgPSBcbiAgICAgICAgICAgIHRoaXMucHJlcGFyZUZpZWxkc0ZvclNlbGVjdChmaWVsZHMscmVwb3NpdG9yeS50YWJsZU5hbWUpICtcbiAgICAgICAgICAgIGNvbnN0cnVjdFdoZXJlV2l0aFByaW1hcnlLZXlzKHJlcG9zaXRvcnkscHJpbWFyeUtleVZhbHVlcykgKyc7JztcblxuICAgICAgICByZXR1cm4gbmV3IE15U1FMUXVlcnk8VFtdPihxdWVyeSx0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc2VsZWN0QWxsPFQ+KFxuICAgICAgICByZXBvc2l0b3J5OklNeVNRTFJlcG9zaXRvcnksXG4gICAgICAgIGZpZWxkczpzdHJpbmdbXSxcbiAgICApIHtcbiAgICAgICAgY29uc3QgcXVlcnkgPVxuICAgICAgICAgICAgdGhpcy5wcmVwYXJlRmllbGRzRm9yU2VsZWN0KGZpZWxkcyxyZXBvc2l0b3J5LnRhYmxlTmFtZSkgKyAnOyc7XG5cbiAgICAgICAgcmV0dXJuIG5ldyBNeVNRTFF1ZXJ5PFRbXT4ocXVlcnksdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxuXG5cbiAgICBwdWJsaWMgYXN5bmMgc2VsZWN0PFQ+KHBhcmFtczpJU2VsZWN0UXVlcnlCdWlsZGVyUGFyYW1zPFQ+KSB7XG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gYXdhaXQgdGhpcy5wcmVwYXJlU2VsZWN0U3RhdGVtZW50KHBhcmFtcyk7XG5cbiAgICAgICAgcmV0dXJuIG5ldyBNeVNRTFF1ZXJ5PFRbXT4oYCR7cXVlcnl9O2AsdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhc3luYyBwcmVwYXJlU2VsZWN0U3RhdGVtZW50PFQ+KHBhcmFtczpJU2VsZWN0UXVlcnlCdWlsZGVyUGFyYW1zPFQ+KTpQcm9taXNlPHN0cmluZz5cbiAgICB7XG4gICAgICAgIGNvbnN0IHtmaWVsZHMscmVwb3NpdG9yeX0gPSBwYXJhbXM7XG5cbiAgICAgICAgbGV0IHF1ZXJ5OnN0cmluZyA9IHRoaXMucHJlcGFyZUZpZWxkc0ZvclNlbGVjdChmaWVsZHMscmVwb3NpdG9yeS50YWJsZU5hbWUpO1xuXG4gICAgICAgIHF1ZXJ5ICs9IGF3YWl0IHRoaXMucHJlcGFyZVdoZXJlT3JkZXJBbmRMaW1pdChwYXJhbXMpO1xuXG4gICAgICAgIHJldHVybiBxdWVyeTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHByZXBhcmVGaWVsZHNGb3JTZWxlY3QoZmllbGRzOnN0cmluZ1tdLHRhYmxlTmFtZTpzdHJpbmcpOnN0cmluZyB7XG4gICAgICAgIGNvbnN0IGZpZWxkc0xpc3QgPSB0aGlzLnByZXBhcmVGaWVsZExpc3QoZmllbGRzKTtcblxuICAgICAgICByZXR1cm4gYFNFTEVDVCAke2ZpZWxkc0xpc3R9IEZST00gJHt0YWJsZU5hbWV9YDtcbiAgICB9XG5cblxuICAgIHByaXZhdGUgcHJlcGFyZUZpZWxkTGlzdChmaWVsZHM6c3RyaW5nW10pOnN0cmluZ1xuICAgIHtcbiAgICAgICAgY29uc3QgcHJlcGFyZWQ6c3RyaW5nW10gPSBbXTtcblxuICAgICAgICBmb3IgKGNvbnN0IGZpZWxkIG9mIGZpZWxkcykge1xuICAgICAgICAgICAgY29uc3QgaXNBbGlhcyA9IGZpZWxkLmluZGV4T2YoJyBBUyAnKSA+IC0xO1xuXG4gICAgICAgICAgICBwcmVwYXJlZC5wdXNoKFxuICAgICAgICAgICAgICAgIGlzQWxpYXMgP1xuICAgICAgICAgICAgICAgIGZpZWxkIDpcbiAgICAgICAgICAgICAgICAnYCcrZmllbGQrJ2AnXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHByZXBhcmVkLmpvaW4oJywnKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgYXN5bmMgdXBkYXRlPFQ+KHBhcmFtczpJVXBkYXRlUXVlcnlCdWlsZGVyUGFyYW1zPFQ+KSB7XG4gICAgICAgIGlmIChwYXJhbXMuc2luZ2xlU3RhdGVtZW50KSB7XG4gICAgICAgICAgICBwYXJhbXMuc3RhdGVtZW50cyA9IFtcbiAgICAgICAgICAgICAgICBwYXJhbXMuc2luZ2xlU3RhdGVtZW50XG4gICAgICAgICAgICBdO1xuICAgICAgICB9XG5cblxuICAgICAgICBjb25zdCB7c3RhdGVtZW50cyxyZXBvc2l0b3J5fSA9IHBhcmFtcztcblxuICAgICAgICBpZiAoXG4gICAgICAgICAgICAhc3RhdGVtZW50cyB8fFxuICAgICAgICAgICAgc3RhdGVtZW50cy5sZW5ndGggPCAxXG4gICAgICAgICkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCd0aGVyZSBtdXN0IGJlIGF0IGxlYXN0IDEgdXBkYXRlIHN0YXRlbWVudCEnKTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgY29uc3QgdXBkYXRlU3RhdGVtZW50czpzdHJpbmcgPSBhd2FpdCB0aGlzLnByZXBhcmVFeHByZXNzaW9uR3JvdXAoXG4gICAgICAgICAgICBuZXcgVXBkYXRlU3RhdGVtZW50cyhzdGF0ZW1lbnRzKVxuICAgICAgICApO1xuXG4gICAgICAgIGNvbnN0IHJlbWFpbmluZ1F1ZXJ5OnN0cmluZyA9IGF3YWl0IHRoaXMucHJlcGFyZVdoZXJlT3JkZXJBbmRMaW1pdChcbiAgICAgICAgICAgIHBhcmFtc1xuICAgICAgICApO1xuXG4gICAgICAgIGNvbnN0IHF1ZXJ5OnN0cmluZyA9IFxuICAgICAgICAgICAgJ1VQREFURSAnK3JlcG9zaXRvcnkudGFibGVOYW1lK1xuICAgICAgICAgICAgJyBTRVQgJysgdXBkYXRlU3RhdGVtZW50cyArXG4gICAgICAgICAgICAgcmVtYWluaW5nUXVlcnkgKyc7JztcblxuICAgICAgICByZXR1cm4gbmV3IE15U1FMUXVlcnk8SU15U1FMUXVlcnlSZXN1bHQ+KHF1ZXJ5LHRoaXMuY29ubmVjdGlvbk1hbmFnZXIpO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgcHJlcGFyZVdoZXJlT3JkZXJBbmRMaW1pdDxUPihwYXJhbXM6SVF1ZXJ5QnVpbGRlclBhcmFtczxUPikge1xuICAgICAgICBpZiAocGFyYW1zLndoZXJlTWFwKSB7XG4gICAgICAgICAgICBwYXJhbXMud2hlcmVBbmRNYXAgPSBwYXJhbXMud2hlcmVNYXA7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoXG4gICAgICAgICAgICBwYXJhbXMud2hlcmVBbmRNYXAgfHxcbiAgICAgICAgICAgIHBhcmFtcy53aGVyZU9yTWFwXG4gICAgICAgICkge1xuICAgICAgICAgICAgcGFyYW1zLndoZXJlR3JvdXAgPSBjcmVhdGVFeHByZXNzaW9uR3JvdXAoXG4gICAgICAgICAgICAgICAgcGFyYW1zLndoZXJlQW5kTWFwID9cbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zLndoZXJlQW5kTWFwIDpcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zLndoZXJlT3JNYXAsXG4gICAgICAgICAgICAgICAgcGFyYW1zLndoZXJlQW5kTWFwID9cbiAgICAgICAgICAgICAgICAgICAgRXhwcmVzc2lvbkdyb3VwSm9pbmVyLkFORCA6XG4gICAgICAgICAgICAgICAgICAgIEV4cHJlc3Npb25Hcm91cEpvaW5lci5PUlxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChwYXJhbXMud2hlcmVQcmltYXJ5S2V5cykge1xuICAgICAgICAgICAgY29uc3QgZXhwcmVzc2lvbnM6U3RyaW5nU3RhdGVtZW50W10gPSBwYXJhbXMud2hlcmVQcmltYXJ5S2V5cy5tYXAoXG4gICAgICAgICAgICAgICAgKHByaW1hcnlLZXlWYWx1ZSkgPT4gbmV3IFN0cmluZ1N0YXRlbWVudChcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zLnJlcG9zaXRvcnkucHJpbWFyeUtleSxcbiAgICAgICAgICAgICAgICAgICAgJycrcHJpbWFyeUtleVZhbHVlXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgcGFyYW1zLndoZXJlR3JvdXAgPSBuZXcgT3JFeHByZXNzaW9uR3JvdXAoZXhwcmVzc2lvbnMpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHBhcmFtcy53aGVyZUNsYXVzZSkge1xuICAgICAgICAgICAgcGFyYW1zLndoZXJlR3JvdXAgPSBuZXcgU2luZ2xlRXhwcmVzc2lvbihcbiAgICAgICAgICAgICAgICBwYXJhbXMud2hlcmVDbGF1c2VcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cblxuXG4gICAgICAgIGNvbnN0IHt3aGVyZUdyb3VwLG9yZGVyQnksbGltaXR9ID0gcGFyYW1zO1xuXG4gICAgICAgIGxldCBxdWVyeSA9ICcnO1xuXG4gICAgICAgIGNvbnN0IHdoZXJlQ2xhdXNlID0gXG4gICAgICAgICAgICB3aGVyZUdyb3VwICYmIFxuICAgICAgICAgICAgYXdhaXQgdGhpcy5wcmVwYXJlRXhwcmVzc2lvbkdyb3VwKHdoZXJlR3JvdXApO1xuXG4gICAgICAgIGlmICh3aGVyZUNsYXVzZSkge1xuICAgICAgICAgICAgcXVlcnkgKz0gYCBcbiAgICAgICAgICAgICAgICBXSEVSRSAke3doZXJlQ2xhdXNlfWA7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAob3JkZXJCeSkge1xuICAgICAgICAgICAgcXVlcnkgKz0gYCBcbiAgICAgICAgICAgICAgICBPUkRFUiBCWSAke29yZGVyQnl9YDtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKGxpbWl0KSB7XG4gICAgICAgICAgICBxdWVyeSArPSBgIFxuICAgICAgICAgICAgICAgIExJTUlUICR7bGltaXR9YDtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBxdWVyeTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGFzeW5jIHByZXBhcmVFeHByZXNzaW9uR3JvdXAoZ3JvdXA6RXhwcmVzc2lvbkdyb3VwKTpQcm9taXNlPHN0cmluZz4ge1xuICAgICAgICBjb25zdCBzdGF0ZW1lbnRzOnN0cmluZ1tdID0gW107XG5cbiAgICAgICAgY29uc3QgdXNlQnJhY2tldHMgPSBncm91cC5zdXJyb3VuZEV4cHJlc3Npb25XaXRoQnJhY2tldHM7XG5cbiAgICAgICAgZm9yIChjb25zdCBleHByZXNzaW9uIG9mIGdyb3VwLmV4cHJlc3Npb25zKSB7XG4gICAgICAgICAgICBjb25zdCB7bGVmdFNpZGUsb3BlcmF0b3IscmlnaHRTaWRlfSA9IGV4cHJlc3Npb247XG5cbiAgICAgICAgICAgIGNvbnN0IHJpZ2h0U2lkZVZhbHVlID1cbiAgICAgICAgICAgICAgICByaWdodFNpZGUuaXNTdHJpbmcgP1xuICAgICAgICAgICAgICAgICAgICBgJHsgYXdhaXQgdGhpcy5lc2NhcGUocmlnaHRTaWRlLmRhdGEgYXMgc3RyaW5nKSB9YCA6XG4gICAgICAgICAgICAgICAgICAgIHJpZ2h0U2lkZS5kYXRhO1xuXG4gICAgICAgICAgICBzdGF0ZW1lbnRzLnB1c2goXG4gICAgICAgICAgICAgICAgYCR7IFxuICAgICAgICAgICAgICAgICAgICB1c2VCcmFja2V0cyA/ICcoJyA6ICcnXG4gICAgICAgICAgICAgICAgfSBcXGAke2xlZnRTaWRlfVxcYCAke29wZXJhdG9yfSAke3JpZ2h0U2lkZVZhbHVlfSAke1xuICAgICAgICAgICAgICAgICAgICB1c2VCcmFja2V0cyA/ICcpJyA6ICcnXG4gICAgICAgICAgICAgICAgfWApO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHN0YXRlbWVudHMuam9pbihgICR7Z3JvdXAuam9pbmVyfSBgKTtcbiAgICB9XG5cblxuICAgIHByaXZhdGUgYXN5bmMgZXNjYXBlKHZhbHVlOnN0cmluZyk6UHJvbWlzZTxzdHJpbmc+XG4gICAge1xuICAgICAgICBjb25zdCBjb25uZWN0aW9uID0gYXdhaXQgdGhpcy5jb25uZWN0aW9uTWFuYWdlci5nZXRDb25uZWN0aW9uKCk7XG5cbiAgICAgICAgcmV0dXJuIGNvbm5lY3Rpb24uZXNjYXBlKHZhbHVlKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgZGVsZXRlKFxuICAgICAgICByZXBvc2l0b3J5OklNeVNRTFJlcG9zaXRvcnksXG4gICAgICAgIHByaW1hcnlLZXlWYWx1ZXM6YW55W11cbiAgICApXG4gICAge1xuICAgICAgICBpZiAocHJpbWFyeUtleVZhbHVlcy5sZW5ndGggPCAxKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1BsZWFzZSBzcGVjaWZ5IHdoaWNoIHJlY29yZHMgdG8gZGVsZXRlIScpO1xuICAgICAgICB9XG5cblxuICAgICAgICBjb25zdCBxdWVyeSA9ICdERUxFVEUgRlJPTSAnK3JlcG9zaXRvcnkudGFibGVOYW1lK1xuICAgICAgICAgICAgICAgICAgICBjb25zdHJ1Y3RXaGVyZVdpdGhQcmltYXJ5S2V5cyhyZXBvc2l0b3J5LHByaW1hcnlLZXlWYWx1ZXMpKyc7JztcblxuICAgICAgICByZXR1cm4gbmV3IE15U1FMUXVlcnk8SU15U1FMUXVlcnlSZXN1bHQ+KHF1ZXJ5LHRoaXMuY29ubmVjdGlvbk1hbmFnZXIpO1xuICAgIH1cblxuICAgIHJhd1F1ZXJ5PFQ+KHNxbDogc3RyaW5nKTogTXlTUUxRdWVyeTxUPiB7XG4gICAgICAgIHJldHVybiBuZXcgTXlTUUxRdWVyeTxUPihzcWwsIHRoaXMuY29ubmVjdGlvbk1hbmFnZXIpO1xuICAgIH1cbn1cblxuXG5mdW5jdGlvbiBjb25zdHJ1Y3RXaGVyZVdpdGhQcmltYXJ5S2V5cyhcbiAgICByZXBvc2l0b3J5OklNeVNRTFJlcG9zaXRvcnksXG4gICAgcHJpbWFyeUtleVZhbHVlczphbnlbXVxuKTpzdHJpbmdcbntcbiAgICBjb25zdCBjb25kaXRpb25zOnN0cmluZ1tdID0gW107XG5cbiAgICBmb3IgKGxldCBwcmltYXJ5S2V5VmFsdWUgb2YgcHJpbWFyeUtleVZhbHVlcykge1xuICAgICAgICBpZiAocmVwb3NpdG9yeS5pc1ByaW1hcnlLZXlBU3RyaW5nKSB7XG4gICAgICAgICAgICBwcmltYXJ5S2V5VmFsdWUgPSBcIidcIitwcmltYXJ5S2V5VmFsdWUrXCInXCI7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBjb25kaXRpb24gPSByZXBvc2l0b3J5LnByaW1hcnlLZXkgKyAnID0gJyArIHByaW1hcnlLZXlWYWx1ZTtcbiAgICAgICAgY29uZGl0aW9ucy5wdXNoKGNvbmRpdGlvbik7XG4gICAgfVxuXG4gICAgcmV0dXJuICcgV0hFUkUgJytjb25kaXRpb25zLmpvaW4oJyBPUiAnKTtcbn0iXX0=