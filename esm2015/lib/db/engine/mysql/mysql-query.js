/**
 * @fileoverview added by tsickle
 * Generated from: lib/db/engine/mysql/mysql-query.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/** @type {?} */
const logSqlQueries = process.env.LOG_SQL_QUERIES === 'true';
/**
 * @template T
 */
export class MySQLQuery {
    /**
     * @param {?} sql
     * @param {?} connectionManager
     */
    constructor(sql, connectionManager) {
        this.sql = sql;
        this.connectionManager = connectionManager;
    }
    /**
     * @return {?}
     */
    execute() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            // When execute is called directly,
            // by default, it will use the connectionManager used to construct the query...
            /** @type {?} */
            const connection = yield this.connectionManager.getConnection();
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            (resolve, reject) => {
                if (logSqlQueries) {
                    console.log(this.sql);
                }
                connection.query(this.sql, (/**
                 * @param {?} error
                 * @param {?} result
                 * @param {?} fields
                 * @return {?}
                 */
                function (error, result, fields) {
                    if (error) {
                        //console.log('*** MySQL Error: ***');
                        //console.log(err);
                        //throw error;
                        reject(error);
                    }
                    else {
                        resolve(result);
                    }
                }));
            }));
        });
    }
}
if (false) {
    /** @type {?} */
    MySQLQuery.prototype.sql;
    /**
     * @type {?}
     * @private
     */
    MySQLQuery.prototype.connectionManager;
}
export class MySQLTransactionQuery extends MySQLQuery {
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcXVlcnkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9kYi9lbmdpbmUvbXlzcWwvbXlzcWwtcXVlcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztNQUlNLGFBQWEsR0FDZixPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsS0FBSyxNQUFNOzs7O0FBRzFDLE1BQU0sT0FBTyxVQUFVOzs7OztJQUVuQixZQUNhLEdBQVUsRUFDWCxpQkFBd0M7UUFEdkMsUUFBRyxHQUFILEdBQUcsQ0FBTztRQUNYLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBdUI7SUFFcEQsQ0FBQzs7OztJQUVLLE9BQU87Ozs7O2tCQUdILFVBQVUsR0FBRyxNQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUU7WUFFL0QsT0FBTyxJQUFJLE9BQU87Ozs7O1lBQUssQ0FBQyxPQUFPLEVBQUMsTUFBTSxFQUFDLEVBQUU7Z0JBQ3JDLElBQUksYUFBYSxFQUFFO29CQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUN6QjtnQkFFRCxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHOzs7Ozs7Z0JBQUMsVUFBUyxLQUFLLEVBQUMsTUFBTSxFQUFDLE1BQU07b0JBQ2xELElBQUksS0FBSyxFQUFFO3dCQUNQLHNDQUFzQzt3QkFDdEMsbUJBQW1CO3dCQUNuQixjQUFjO3dCQUNkLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDakI7eUJBQU07d0JBQ0gsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUNuQjtnQkFDTCxDQUFDLEVBQUMsQ0FBQztZQUNQLENBQUMsRUFBQyxDQUFDO1FBQ1AsQ0FBQztLQUFBO0NBQ0o7OztJQTNCTyx5QkFBbUI7Ozs7O0lBQ25CLHVDQUFnRDs7QUE0QnhELE1BQU0sT0FBTyxxQkFBc0IsU0FBUSxVQUE2QjtDQUV2RSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIgfSBmcm9tIFwiLi9teXNxbC1jb25uZWN0aW9uLW1hbmFnZXJcIjtcbmltcG9ydCB7IElNeVNRTFF1ZXJ5UmVzdWx0IH0gZnJvbSBcIi4vaW50ZXJmYWNlc1wiO1xuXG5cbmNvbnN0IGxvZ1NxbFF1ZXJpZXMgPSBcbiAgICBwcm9jZXNzLmVudi5MT0dfU1FMX1FVRVJJRVMgPT09ICd0cnVlJztcblxuXG5leHBvcnQgY2xhc3MgTXlTUUxRdWVyeTxUPlxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICByZWFkb25seSBzcWw6c3RyaW5nLFxuICAgICAgICBwcml2YXRlIGNvbm5lY3Rpb25NYW5hZ2VyOk15U1FMQ29ubmVjdGlvbk1hbmFnZXJcbiAgICApIHtcbiAgICB9XG5cbiAgICBhc3luYyBleGVjdXRlKCk6UHJvbWlzZTxUPiB7XG4gICAgICAgIC8vIFdoZW4gZXhlY3V0ZSBpcyBjYWxsZWQgZGlyZWN0bHksXG4gICAgICAgIC8vIGJ5IGRlZmF1bHQsIGl0IHdpbGwgdXNlIHRoZSBjb25uZWN0aW9uTWFuYWdlciB1c2VkIHRvIGNvbnN0cnVjdCB0aGUgcXVlcnkuLi5cbiAgICAgICAgY29uc3QgY29ubmVjdGlvbiA9IGF3YWl0IHRoaXMuY29ubmVjdGlvbk1hbmFnZXIuZ2V0Q29ubmVjdGlvbigpO1xuXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxUPiggKHJlc29sdmUscmVqZWN0KT0+e1xuICAgICAgICAgICAgaWYgKGxvZ1NxbFF1ZXJpZXMpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnNxbCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGNvbm5lY3Rpb24ucXVlcnkodGhpcy5zcWwsZnVuY3Rpb24oZXJyb3IscmVzdWx0LGZpZWxkcyl7XG4gICAgICAgICAgICAgICAgaWYgKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coJyoqKiBNeVNRTCBFcnJvcjogKioqJyk7XG4gICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coZXJyKTtcbiAgICAgICAgICAgICAgICAgICAgLy90aHJvdyBlcnJvcjtcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycm9yKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHJlc3VsdCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIE15U1FMVHJhbnNhY3Rpb25RdWVyeSBleHRlbmRzIE15U1FMUXVlcnk8SU15U1FMUXVlcnlSZXN1bHQ+XG57XG59Il19