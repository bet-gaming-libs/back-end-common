/**
 * @fileoverview added by tsickle
 * Generated from: lib/db/engine/mysql/mysql-statements.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const MySQLValueType = {
    EXPRESSION: 0,
    STRING: 1,
};
export { MySQLValueType };
MySQLValueType[MySQLValueType.EXPRESSION] = 'EXPRESSION';
MySQLValueType[MySQLValueType.STRING] = 'STRING';
/** @enum {string} */
const ExpressionOperator = {
    EQUAL: "=",
    NOT_EQUAL: "!=",
    LESS_THAN: "<",
    LESS_THAN_OR_EQUAL: "<=",
    GREATER_THAN: ">",
    GREATER_THAN_OR_EQUAL: ">=",
    IS: "IS",
    IS_NOT: "IS NOT",
};
export { ExpressionOperator };
/** @enum {string} */
const ExpressionGroupJoiner = {
    AND: "AND",
    OR: "OR",
    COMMA: ",",
};
export { ExpressionGroupJoiner };
/**
 * @record
 */
export function IMySQLValue() { }
if (false) {
    /** @type {?} */
    IMySQLValue.prototype.data;
    /** @type {?} */
    IMySQLValue.prototype.isString;
}
export class MySQLValue {
    /**
     * @param {?} data
     * @param {?} type
     */
    constructor(data, type) {
        this.data = data;
        this.type = type;
        this.isString =
            type == MySQLValueType.STRING;
    }
}
if (false) {
    /** @type {?} */
    MySQLValue.prototype.isString;
    /** @type {?} */
    MySQLValue.prototype.data;
    /** @type {?} */
    MySQLValue.prototype.type;
}
class MySQLNumberValue extends MySQLValue {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data, MySQLValueType.EXPRESSION);
    }
}
class MySQLStringValue extends MySQLValue {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data, MySQLValueType.STRING);
    }
}
class MySQLStringExpressionValue extends MySQLValue {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data, MySQLValueType.EXPRESSION);
    }
}
/** @enum {string} */
const MySQLExpression = {
    NOW: "NOW()",
    NULL: "NULL",
};
export { MySQLExpression };
/** @type {?} */
const nowValue = new MySQLStringExpressionValue(MySQLExpression.NOW);
/** @type {?} */
const nullValue = new MySQLStringExpressionValue(MySQLExpression.NULL);
/*
class MySQLValue
{
    static booleanAsInt(bool:boolean):number
    {
        return bool ? 1 : 0;
    }
}
*/
/**
 * @abstract
 */
export class Expression {
    /**
     * @param {?} leftSide
     * @param {?} operator
     * @param {?} rightSide
     */
    constructor(leftSide, operator, rightSide) {
        this.leftSide = leftSide;
        this.operator = operator;
        this.rightSide = rightSide;
    }
}
if (false) {
    /** @type {?} */
    Expression.prototype.leftSide;
    /** @type {?} */
    Expression.prototype.operator;
    /** @type {?} */
    Expression.prototype.rightSide;
}
export class ExpressionGroup {
    /**
     * @param {?} expressions
     * @param {?} joiner
     * @param {?=} surroundExpressionWithBrackets
     */
    constructor(expressions, joiner, surroundExpressionWithBrackets = true) {
        this.expressions = expressions;
        this.joiner = joiner;
        this.surroundExpressionWithBrackets = surroundExpressionWithBrackets;
    }
}
if (false) {
    /** @type {?} */
    ExpressionGroup.prototype.expressions;
    /** @type {?} */
    ExpressionGroup.prototype.joiner;
    /** @type {?} */
    ExpressionGroup.prototype.surroundExpressionWithBrackets;
}
/**
 * @param {?} map
 * @param {?} joiner
 * @return {?}
 */
export function createExpressionGroup(map, joiner) {
    /** @type {?} */
    const expressions = [];
    // should we use Object.keys()?
    for (const fieldName in map) {
        /** @type {?} */
        const fieldValue = map[fieldName];
        expressions.push(typeof fieldValue == 'string' ?
            new StringStatement(fieldName, (/** @type {?} */ (fieldValue))) :
            new NumberStatement(fieldName, fieldValue));
    }
    return new ExpressionGroup(expressions, joiner);
}
export class SingleExpression extends ExpressionGroup {
    /**
     * @param {?} expression
     */
    constructor(expression) {
        super([expression], undefined);
    }
}
export class AndExpressionGroup extends ExpressionGroup {
    /**
     * @param {?} expressions
     */
    constructor(expressions) {
        super(expressions, ExpressionGroupJoiner.AND);
    }
}
export class OrExpressionGroup extends ExpressionGroup {
    /**
     * @param {?} expressions
     */
    constructor(expressions) {
        super(expressions, ExpressionGroupJoiner.OR);
    }
}
export class UpdateStatements extends ExpressionGroup {
    /**
     * @param {?} expressions
     */
    constructor(expressions) {
        super(expressions, ExpressionGroupJoiner.COMMA, false);
    }
}
/**
 * @abstract
 */
export class FieldStatement extends Expression {
    /**
     * @param {?} fieldName
     * @param {?} value
     * @param {?} operator
     */
    constructor(fieldName, value, operator) {
        super(fieldName, operator, value);
    }
}
export class NumberStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     * @param {?} data
     * @param {?=} operator
     */
    constructor(fieldName, data, operator = ExpressionOperator.EQUAL) {
        super(fieldName, new MySQLNumberValue(data), operator);
    }
}
export class StringStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     * @param {?} data
     * @param {?=} operator
     */
    constructor(fieldName, data, operator = ExpressionOperator.EQUAL) {
        super(fieldName, new MySQLStringValue(data), operator);
    }
}
export class StringExpressionStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     * @param {?} data
     * @param {?=} operator
     */
    constructor(fieldName, data, operator = ExpressionOperator.EQUAL) {
        super(fieldName, new MySQLStringExpressionValue(data), operator);
    }
}
export class NullUpdateStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nullValue, ExpressionOperator.EQUAL);
    }
}
export class NowUpdateStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nowValue, ExpressionOperator.EQUAL);
    }
}
export class FieldIsNullStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nullValue, ExpressionOperator.IS);
    }
}
export class FieldIsNotNullStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nullValue, ExpressionOperator.IS_NOT);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtc3RhdGVtZW50cy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL2VuZ2luZS9teXNxbC9teXNxbC1zdGF0ZW1lbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE1BQVksY0FBYztJQUV0QixVQUFVLEdBQUE7SUFDVixNQUFNLEdBQUE7RUFDVDs7Ozs7QUFFRCxNQUFZLGtCQUFrQjtJQUUxQixLQUFLLEtBQXNCO0lBQzNCLFNBQVMsTUFBbUI7SUFFNUIsU0FBUyxLQUFrQjtJQUMzQixrQkFBa0IsTUFBVTtJQUM1QixZQUFZLEtBQWU7SUFDM0IscUJBQXFCLE1BQU87SUFFNUIsRUFBRSxNQUEwQjtJQUM1QixNQUFNLFVBQTBCO0VBQ25DOzs7QUFFRCxNQUFZLHFCQUFxQjtJQUU3QixHQUFHLE9BQVU7SUFDYixFQUFFLE1BQVU7SUFDWixLQUFLLEtBQU07RUFDZDs7Ozs7QUFlRCxpQ0FJQzs7O0lBRkcsMkJBQXlCOztJQUN6QiwrQkFBaUI7O0FBS3JCLE1BQU0sT0FBTyxVQUFVOzs7OztJQUluQixZQUNhLElBQXdCLEVBQ3hCLElBQW1CO1FBRG5CLFNBQUksR0FBSixJQUFJLENBQW9CO1FBQ3hCLFNBQUksR0FBSixJQUFJLENBQWU7UUFFNUIsSUFBSSxDQUFDLFFBQVE7WUFDVCxJQUFJLElBQUksY0FBYyxDQUFDLE1BQU0sQ0FBQztJQUN0QyxDQUFDO0NBQ0o7OztJQVRHLDhCQUEwQjs7SUFHdEIsMEJBQWlDOztJQUNqQywwQkFBNEI7O0FBT3BDLE1BQU0sZ0JBQWlCLFNBQVEsVUFBVTs7OztJQUVyQyxZQUFZLElBQVc7UUFDbkIsS0FBSyxDQUFDLElBQUksRUFBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDMUMsQ0FBQztDQUNKO0FBRUQsTUFBTSxnQkFBaUIsU0FBUSxVQUFVOzs7O0lBRXJDLFlBQVksSUFBVztRQUNuQixLQUFLLENBQUMsSUFBSSxFQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN0QyxDQUFDO0NBQ0o7QUFFRCxNQUFNLDBCQUEyQixTQUFRLFVBQVU7Ozs7SUFFL0MsWUFBWSxJQUFXO1FBQ25CLEtBQUssQ0FBQyxJQUFJLEVBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzFDLENBQUM7Q0FDSjs7QUFHRCxNQUFZLGVBQWU7SUFDdkIsR0FBRyxTQUFVO0lBQ2IsSUFBSSxRQUFTO0VBQ2hCOzs7TUFFSyxRQUFRLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDOztNQUM5RCxTQUFTLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDOzs7Ozs7Ozs7Ozs7O0FBZXRFLE1BQU0sT0FBZ0IsVUFBVTs7Ozs7O0lBRTVCLFlBQ2EsUUFBZSxFQUNmLFFBQWUsRUFDZixTQUFvQjtRQUZwQixhQUFRLEdBQVIsUUFBUSxDQUFPO1FBQ2YsYUFBUSxHQUFSLFFBQVEsQ0FBTztRQUNmLGNBQVMsR0FBVCxTQUFTLENBQVc7SUFFakMsQ0FBQztDQUNKOzs7SUFMTyw4QkFBd0I7O0lBQ3hCLDhCQUF3Qjs7SUFDeEIsK0JBQTZCOztBQU1yQyxNQUFNLE9BQU8sZUFBZTs7Ozs7O0lBRXhCLFlBQ2EsV0FBd0IsRUFDeEIsTUFBNEIsRUFDNUIsaUNBQXlDLElBQUk7UUFGN0MsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsV0FBTSxHQUFOLE1BQU0sQ0FBc0I7UUFDNUIsbUNBQThCLEdBQTlCLDhCQUE4QixDQUFlO0lBRTFELENBQUM7Q0FDSjs7O0lBTE8sc0NBQWlDOztJQUNqQyxpQ0FBcUM7O0lBQ3JDLHlEQUFzRDs7Ozs7OztBQU05RCxNQUFNLFVBQVUscUJBQXFCLENBQ2pDLEdBQU8sRUFBQyxNQUE0Qjs7VUFFOUIsV0FBVyxHQUFnQixFQUFFO0lBRW5DLCtCQUErQjtJQUMvQixLQUFLLE1BQU0sU0FBUyxJQUFJLEdBQUcsRUFBRTs7Y0FDbkIsVUFBVSxHQUFHLEdBQUcsQ0FBQyxTQUFTLENBQUM7UUFFakMsV0FBVyxDQUFDLElBQUksQ0FDWixPQUFPLFVBQVUsSUFBSSxRQUFRLENBQUMsQ0FBQztZQUMzQixJQUFJLGVBQWUsQ0FDZixTQUFTLEVBQ1QsbUJBQUEsVUFBVSxFQUFVLENBQ3ZCLENBQUMsQ0FBQztZQUNILElBQUksZUFBZSxDQUNmLFNBQVMsRUFDVCxVQUFVLENBQ2IsQ0FDUixDQUFDO0tBQ0w7SUFFRCxPQUFPLElBQUksZUFBZSxDQUFDLFdBQVcsRUFBQyxNQUFNLENBQUMsQ0FBQztBQUNuRCxDQUFDO0FBR0QsTUFBTSxPQUFPLGdCQUFpQixTQUFRLGVBQWU7Ozs7SUFFakQsWUFBWSxVQUFxQjtRQUM3QixLQUFLLENBQUMsQ0FBQyxVQUFVLENBQUMsRUFBQyxTQUFTLENBQUMsQ0FBQztJQUNsQyxDQUFDO0NBQ0o7QUFDRCxNQUFNLE9BQU8sa0JBQW1CLFNBQVEsZUFBZTs7OztJQUVuRCxZQUFZLFdBQXdCO1FBQ2hDLEtBQUssQ0FBQyxXQUFXLEVBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDakQsQ0FBQztDQUNKO0FBQ0QsTUFBTSxPQUFPLGlCQUFrQixTQUFRLGVBQWU7Ozs7SUFFbEQsWUFBWSxXQUF3QjtRQUNoQyxLQUFLLENBQUMsV0FBVyxFQUFDLHFCQUFxQixDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ2hELENBQUM7Q0FDSjtBQUVELE1BQU0sT0FBTyxnQkFBaUIsU0FBUSxlQUFlOzs7O0lBRWpELFlBQVksV0FBd0I7UUFDaEMsS0FBSyxDQUFDLFdBQVcsRUFBQyxxQkFBcUIsQ0FBQyxLQUFLLEVBQUMsS0FBSyxDQUFDLENBQUM7SUFDekQsQ0FBQztDQUNKOzs7O0FBSUQsTUFBTSxPQUFnQixjQUFlLFNBQVEsVUFBVTs7Ozs7O0lBRW5ELFlBQ0ksU0FBZ0IsRUFDaEIsS0FBZ0IsRUFDaEIsUUFBMkI7UUFFM0IsS0FBSyxDQUFDLFNBQVMsRUFBQyxRQUFRLEVBQUMsS0FBSyxDQUFDLENBQUM7SUFDcEMsQ0FBQztDQUNKO0FBR0QsTUFBTSxPQUFPLGVBQWdCLFNBQVEsY0FBYzs7Ozs7O0lBRS9DLFlBQ0ksU0FBZ0IsRUFDaEIsSUFBVyxFQUNYLFdBQThCLGtCQUFrQixDQUFDLEtBQUs7UUFFdEQsS0FBSyxDQUNELFNBQVMsRUFDVCxJQUFJLGdCQUFnQixDQUFDLElBQUksQ0FBQyxFQUMxQixRQUFRLENBQ1gsQ0FBQztJQUNOLENBQUM7Q0FDSjtBQUVELE1BQU0sT0FBTyxlQUFnQixTQUFRLGNBQWM7Ozs7OztJQUUvQyxZQUNJLFNBQWdCLEVBQ2hCLElBQVcsRUFDWCxXQUE4QixrQkFBa0IsQ0FBQyxLQUFLO1FBRXRELEtBQUssQ0FDRCxTQUFTLEVBQ1QsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFDMUIsUUFBUSxDQUNYLENBQUM7SUFDTixDQUFDO0NBQ0o7QUFFRCxNQUFNLE9BQU8seUJBQTBCLFNBQVEsY0FBYzs7Ozs7O0lBRXpELFlBQ0ksU0FBZ0IsRUFDaEIsSUFBVyxFQUNYLFdBQThCLGtCQUFrQixDQUFDLEtBQUs7UUFFdEQsS0FBSyxDQUNELFNBQVMsRUFDVCxJQUFJLDBCQUEwQixDQUFDLElBQUksQ0FBQyxFQUNwQyxRQUFRLENBQ1gsQ0FBQztJQUNOLENBQUM7Q0FDSjtBQUdELE1BQU0sT0FBTyxtQkFBb0IsU0FBUSxjQUFjOzs7O0lBRW5ELFlBQVksU0FBZ0I7UUFDeEIsS0FBSyxDQUFDLFNBQVMsRUFBQyxTQUFTLEVBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDeEQsQ0FBQztDQUNKO0FBRUQsTUFBTSxPQUFPLGtCQUFtQixTQUFRLGNBQWM7Ozs7SUFFbEQsWUFBWSxTQUFnQjtRQUN4QixLQUFLLENBQUMsU0FBUyxFQUFDLFFBQVEsRUFBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN2RCxDQUFDO0NBQ0o7QUFHRCxNQUFNLE9BQU8sb0JBQXFCLFNBQVEsY0FBYzs7OztJQUVwRCxZQUFZLFNBQWdCO1FBQ3hCLEtBQUssQ0FBQyxTQUFTLEVBQUMsU0FBUyxFQUFDLGtCQUFrQixDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3JELENBQUM7Q0FDSjtBQUVELE1BQU0sT0FBTyx1QkFBd0IsU0FBUSxjQUFjOzs7O0lBRXZELFlBQVksU0FBZ0I7UUFDeEIsS0FBSyxDQUFDLFNBQVMsRUFBQyxTQUFTLEVBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDekQsQ0FBQztDQUNKIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gTXlTUUxWYWx1ZVR5cGVcbntcbiAgICBFWFBSRVNTSU9OLFxuICAgIFNUUklOR1xufVxuXG5leHBvcnQgZW51bSBFeHByZXNzaW9uT3BlcmF0b3JcbntcbiAgICBFUVVBTCAgICAgICAgICAgICAgICAgPSAnPScsXG4gICAgTk9UX0VRVUFMICAgICAgICAgICAgID0gJyE9JyxcblxuICAgIExFU1NfVEhBTiAgICAgICAgICAgICA9ICc8JyxcbiAgICBMRVNTX1RIQU5fT1JfRVFVQUwgICAgPSAnPD0nLFxuICAgIEdSRUFURVJfVEhBTiAgICAgICAgICA9ICc+JyxcbiAgICBHUkVBVEVSX1RIQU5fT1JfRVFVQUwgPSAnPj0nLFxuXG4gICAgSVMgICAgICAgICAgICAgICAgICAgID0gJ0lTJyxcbiAgICBJU19OT1QgICAgICAgICAgICAgICAgPSAnSVMgTk9UJyxcbn1cblxuZXhwb3J0IGVudW0gRXhwcmVzc2lvbkdyb3VwSm9pbmVyXG57XG4gICAgQU5EICAgPSAnQU5EJyxcbiAgICBPUiAgICA9ICdPUicsXG4gICAgQ09NTUEgPSAnLCdcbn1cblxuXG5cbmV4cG9ydCB0eXBlIEV4cHJlc3Npb25WYWx1ZVR5cGUgPSBzdHJpbmd8bnVtYmVyO1xuXG5cblxuLypcbmludGVyZmFjZSBJTXlTUUxWYWx1ZTxUIGV4dGVuZHMgRXhwcmVzc2lvblZhbHVlVHlwZT5cbntcbiAgICBkYXRhOlQsXG4gICAgLy90eXBlOk15U1FMVmFsdWVUeXBlXG59XG4qL1xuZXhwb3J0IGludGVyZmFjZSBJTXlTUUxWYWx1ZVxue1xuICAgIGRhdGE6RXhwcmVzc2lvblZhbHVlVHlwZTtcbiAgICBpc1N0cmluZzpib29sZWFuO1xufVxuXG5cblxuZXhwb3J0IGNsYXNzIE15U1FMVmFsdWUgaW1wbGVtZW50cyBJTXlTUUxWYWx1ZVxue1xuICAgIHJlYWRvbmx5IGlzU3RyaW5nOmJvb2xlYW47XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcmVhZG9ubHkgZGF0YTpFeHByZXNzaW9uVmFsdWVUeXBlLFxuICAgICAgICByZWFkb25seSB0eXBlOk15U1FMVmFsdWVUeXBlXG4gICAgKSB7XG4gICAgICAgIHRoaXMuaXNTdHJpbmcgPSBcbiAgICAgICAgICAgIHR5cGUgPT0gTXlTUUxWYWx1ZVR5cGUuU1RSSU5HO1xuICAgIH1cbn1cblxuY2xhc3MgTXlTUUxOdW1iZXJWYWx1ZSBleHRlbmRzIE15U1FMVmFsdWVcbntcbiAgICBjb25zdHJ1Y3RvcihkYXRhOm51bWJlcikge1xuICAgICAgICBzdXBlcihkYXRhLE15U1FMVmFsdWVUeXBlLkVYUFJFU1NJT04pO1xuICAgIH1cbn1cblxuY2xhc3MgTXlTUUxTdHJpbmdWYWx1ZSBleHRlbmRzIE15U1FMVmFsdWVcbntcbiAgICBjb25zdHJ1Y3RvcihkYXRhOnN0cmluZykge1xuICAgICAgICBzdXBlcihkYXRhLE15U1FMVmFsdWVUeXBlLlNUUklORyk7XG4gICAgfVxufVxuXG5jbGFzcyBNeVNRTFN0cmluZ0V4cHJlc3Npb25WYWx1ZSBleHRlbmRzIE15U1FMVmFsdWVcbntcbiAgICBjb25zdHJ1Y3RvcihkYXRhOnN0cmluZykge1xuICAgICAgICBzdXBlcihkYXRhLE15U1FMVmFsdWVUeXBlLkVYUFJFU1NJT04pO1xuICAgIH1cbn1cblxuXG5leHBvcnQgZW51bSBNeVNRTEV4cHJlc3Npb24ge1xuICAgIE5PVyA9ICdOT1coKScsXG4gICAgTlVMTCA9ICdOVUxMJyxcbn1cblxuY29uc3Qgbm93VmFsdWUgPSBuZXcgTXlTUUxTdHJpbmdFeHByZXNzaW9uVmFsdWUoTXlTUUxFeHByZXNzaW9uLk5PVyk7XG5jb25zdCBudWxsVmFsdWUgPSBuZXcgTXlTUUxTdHJpbmdFeHByZXNzaW9uVmFsdWUoTXlTUUxFeHByZXNzaW9uLk5VTEwpO1xuXG5cbi8qXG5jbGFzcyBNeVNRTFZhbHVlXG57XG4gICAgc3RhdGljIGJvb2xlYW5Bc0ludChib29sOmJvb2xlYW4pOm51bWJlclxuICAgIHtcbiAgICAgICAgcmV0dXJuIGJvb2wgPyAxIDogMDtcbiAgICB9XG59XG4qL1xuXG5cblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEV4cHJlc3Npb25cbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcmVhZG9ubHkgbGVmdFNpZGU6c3RyaW5nLFxuICAgICAgICByZWFkb25seSBvcGVyYXRvcjpzdHJpbmcsXG4gICAgICAgIHJlYWRvbmx5IHJpZ2h0U2lkZTpNeVNRTFZhbHVlXG4gICAgKSB7XG4gICAgfVxufVxuXG5cbmV4cG9ydCBjbGFzcyBFeHByZXNzaW9uR3JvdXBcbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcmVhZG9ubHkgZXhwcmVzc2lvbnM6RXhwcmVzc2lvbltdLFxuICAgICAgICByZWFkb25seSBqb2luZXI6RXhwcmVzc2lvbkdyb3VwSm9pbmVyLFxuICAgICAgICByZWFkb25seSBzdXJyb3VuZEV4cHJlc3Npb25XaXRoQnJhY2tldHM6Ym9vbGVhbiA9IHRydWVcbiAgICApIHtcbiAgICB9XG59XG5cblxuZXhwb3J0IGZ1bmN0aW9uIGNyZWF0ZUV4cHJlc3Npb25Hcm91cChcbiAgICBtYXA6YW55LGpvaW5lcjpFeHByZXNzaW9uR3JvdXBKb2luZXJcbikge1xuICAgIGNvbnN0IGV4cHJlc3Npb25zOkV4cHJlc3Npb25bXSA9IFtdO1xuXG4gICAgLy8gc2hvdWxkIHdlIHVzZSBPYmplY3Qua2V5cygpP1xuICAgIGZvciAoY29uc3QgZmllbGROYW1lIGluIG1hcCkge1xuICAgICAgICBjb25zdCBmaWVsZFZhbHVlID0gbWFwW2ZpZWxkTmFtZV07XG5cbiAgICAgICAgZXhwcmVzc2lvbnMucHVzaChcbiAgICAgICAgICAgIHR5cGVvZiBmaWVsZFZhbHVlID09ICdzdHJpbmcnID9cbiAgICAgICAgICAgICAgICBuZXcgU3RyaW5nU3RhdGVtZW50KFxuICAgICAgICAgICAgICAgICAgICBmaWVsZE5hbWUsXG4gICAgICAgICAgICAgICAgICAgIGZpZWxkVmFsdWUgYXMgc3RyaW5nXG4gICAgICAgICAgICAgICAgKSA6XG4gICAgICAgICAgICAgICAgbmV3IE51bWJlclN0YXRlbWVudChcbiAgICAgICAgICAgICAgICAgICAgZmllbGROYW1lLFxuICAgICAgICAgICAgICAgICAgICBmaWVsZFZhbHVlXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICApO1xuICAgIH1cbiAgICBcbiAgICByZXR1cm4gbmV3IEV4cHJlc3Npb25Hcm91cChleHByZXNzaW9ucyxqb2luZXIpO1xufVxuXG5cbmV4cG9ydCBjbGFzcyBTaW5nbGVFeHByZXNzaW9uIGV4dGVuZHMgRXhwcmVzc2lvbkdyb3VwXG57XG4gICAgY29uc3RydWN0b3IoZXhwcmVzc2lvbjpFeHByZXNzaW9uKSB7XG4gICAgICAgIHN1cGVyKFtleHByZXNzaW9uXSx1bmRlZmluZWQpO1xuICAgIH1cbn1cbmV4cG9ydCBjbGFzcyBBbmRFeHByZXNzaW9uR3JvdXAgZXh0ZW5kcyBFeHByZXNzaW9uR3JvdXBcbntcbiAgICBjb25zdHJ1Y3RvcihleHByZXNzaW9uczpFeHByZXNzaW9uW10pIHtcbiAgICAgICAgc3VwZXIoZXhwcmVzc2lvbnMsRXhwcmVzc2lvbkdyb3VwSm9pbmVyLkFORCk7XG4gICAgfVxufVxuZXhwb3J0IGNsYXNzIE9yRXhwcmVzc2lvbkdyb3VwIGV4dGVuZHMgRXhwcmVzc2lvbkdyb3VwXG57XG4gICAgY29uc3RydWN0b3IoZXhwcmVzc2lvbnM6RXhwcmVzc2lvbltdKSB7XG4gICAgICAgIHN1cGVyKGV4cHJlc3Npb25zLEV4cHJlc3Npb25Hcm91cEpvaW5lci5PUik7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgVXBkYXRlU3RhdGVtZW50cyBleHRlbmRzIEV4cHJlc3Npb25Hcm91cFxue1xuICAgIGNvbnN0cnVjdG9yKGV4cHJlc3Npb25zOkV4cHJlc3Npb25bXSkge1xuICAgICAgICBzdXBlcihleHByZXNzaW9ucyxFeHByZXNzaW9uR3JvdXBKb2luZXIuQ09NTUEsZmFsc2UpO1xuICAgIH1cbn1cblxuXG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBGaWVsZFN0YXRlbWVudCBleHRlbmRzIEV4cHJlc3Npb25cbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgZmllbGROYW1lOnN0cmluZyxcbiAgICAgICAgdmFsdWU6TXlTUUxWYWx1ZSxcbiAgICAgICAgb3BlcmF0b3I6RXhwcmVzc2lvbk9wZXJhdG9yXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKGZpZWxkTmFtZSxvcGVyYXRvcix2YWx1ZSk7XG4gICAgfVxufVxuXG5cbmV4cG9ydCBjbGFzcyBOdW1iZXJTdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudFxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBmaWVsZE5hbWU6c3RyaW5nLFxuICAgICAgICBkYXRhOm51bWJlcixcbiAgICAgICAgb3BlcmF0b3I6RXhwcmVzc2lvbk9wZXJhdG9yID0gRXhwcmVzc2lvbk9wZXJhdG9yLkVRVUFMXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKFxuICAgICAgICAgICAgZmllbGROYW1lLFxuICAgICAgICAgICAgbmV3IE15U1FMTnVtYmVyVmFsdWUoZGF0YSksXG4gICAgICAgICAgICBvcGVyYXRvclxuICAgICAgICApO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIFN0cmluZ1N0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50XG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIGZpZWxkTmFtZTpzdHJpbmcsXG4gICAgICAgIGRhdGE6c3RyaW5nLFxuICAgICAgICBvcGVyYXRvcjpFeHByZXNzaW9uT3BlcmF0b3IgPSBFeHByZXNzaW9uT3BlcmF0b3IuRVFVQUxcbiAgICApIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICBmaWVsZE5hbWUsXG4gICAgICAgICAgICBuZXcgTXlTUUxTdHJpbmdWYWx1ZShkYXRhKSxcbiAgICAgICAgICAgIG9wZXJhdG9yXG4gICAgICAgICk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgU3RyaW5nRXhwcmVzc2lvblN0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50XG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIGZpZWxkTmFtZTpzdHJpbmcsXG4gICAgICAgIGRhdGE6c3RyaW5nLFxuICAgICAgICBvcGVyYXRvcjpFeHByZXNzaW9uT3BlcmF0b3IgPSBFeHByZXNzaW9uT3BlcmF0b3IuRVFVQUxcbiAgICApIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICBmaWVsZE5hbWUsXG4gICAgICAgICAgICBuZXcgTXlTUUxTdHJpbmdFeHByZXNzaW9uVmFsdWUoZGF0YSksXG4gICAgICAgICAgICBvcGVyYXRvclxuICAgICAgICApO1xuICAgIH1cbn1cblxuXG5leHBvcnQgY2xhc3MgTnVsbFVwZGF0ZVN0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50XG57XG4gICAgY29uc3RydWN0b3IoZmllbGROYW1lOnN0cmluZykge1xuICAgICAgICBzdXBlcihmaWVsZE5hbWUsbnVsbFZhbHVlLEV4cHJlc3Npb25PcGVyYXRvci5FUVVBTCk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgTm93VXBkYXRlU3RhdGVtZW50IGV4dGVuZHMgRmllbGRTdGF0ZW1lbnRcbntcbiAgICBjb25zdHJ1Y3RvcihmaWVsZE5hbWU6c3RyaW5nKSB7XG4gICAgICAgIHN1cGVyKGZpZWxkTmFtZSxub3dWYWx1ZSxFeHByZXNzaW9uT3BlcmF0b3IuRVFVQUwpO1xuICAgIH1cbn1cblxuXG5leHBvcnQgY2xhc3MgRmllbGRJc051bGxTdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudFxue1xuICAgIGNvbnN0cnVjdG9yKGZpZWxkTmFtZTpzdHJpbmcpIHtcbiAgICAgICAgc3VwZXIoZmllbGROYW1lLG51bGxWYWx1ZSxFeHByZXNzaW9uT3BlcmF0b3IuSVMpO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIEZpZWxkSXNOb3ROdWxsU3RhdGVtZW50IGV4dGVuZHMgRmllbGRTdGF0ZW1lbnRcbntcbiAgICBjb25zdHJ1Y3RvcihmaWVsZE5hbWU6c3RyaW5nKSB7XG4gICAgICAgIHN1cGVyKGZpZWxkTmFtZSxudWxsVmFsdWUsRXhwcmVzc2lvbk9wZXJhdG9yLklTX05PVCk7XG4gICAgfVxufSJdfQ==