/**
 * @fileoverview added by tsickle
 * Generated from: lib/db/engine/mysql/mysql-transaction-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { MySQLConnectionManager } from './mysql-connection-manager';
import { MySQLTransactionQuery } from './mysql-query';
export class MySQLTransactionManager {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.queue = [];
        this.isExecutingTransaction = false;
        /**
         * we need to manage a queue of transactions
         * to be executed in order
         * once the transaction either is successful or fails
         * then move on to the next
         */
        this.performNextTransaction = (/**
         * @return {?}
         */
        () => {
            if (this.isExecutingTransaction ||
                this.queue.length < 1) {
                return;
            }
            console.log('perform next transaction!');
            this.isExecutingTransaction = true;
            // remove first element from queue and return it
            /** @type {?} */
            const executor = this.queue.shift();
            executor.execute();
        });
        this.onTransactionCompleted = (/**
         * @return {?}
         */
        () => {
            console.log('onTransactionCompleted');
            this.isExecutingTransaction = false;
            this.performNextTransaction();
        });
        this.connectionManager = new MySQLConnectionManager(config);
    }
    /**
     * @param {?} queries
     * @return {?}
     */
    performTransaction(queries) {
        /*
                we need to promise the result of each query executed
                so that they can be inspected by consumer for Ids, etc
                */
        /** @type {?} */
        const executor = new MySQLTransactionExecuter(queries, this.performNextTransaction, this.onTransactionCompleted, this.connectionManager);
        // add executor to queue
        this.queue.push(executor);
        return new Promise(executor.init);
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.queue;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.isExecutingTransaction;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.connectionManager;
    /**
     * we need to manage a queue of transactions
     * to be executed in order
     * once the transaction either is successful or fails
     * then move on to the next
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.performNextTransaction;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.onTransactionCompleted;
}
class MySQLTransactionExecuter {
    /**
     * @param {?} queries
     * @param {?} onInitHandler
     * @param {?} onCompleteHandler
     * @param {?} connectionManager
     */
    constructor(queries, onInitHandler, onCompleteHandler, connectionManager) {
        this.queries = queries;
        this.onInitHandler = onInitHandler;
        this.onCompleteHandler = onCompleteHandler;
        this.connectionManager = connectionManager;
        this.init = (/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.resolve = resolve;
            this.reject = reject;
            this.onInitHandler();
        });
    }
    /**
     * @return {?}
     */
    execute() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.connection = yield this.connectionManager.getConnection();
            this.connection.beginTransaction((/**
             * @return {?}
             */
            () => tslib_1.__awaiter(this, void 0, void 0, function* () {
                try {
                    /** @type {?} */
                    const results = [];
                    for (const queryString of this.queries) {
                        /** @type {?} */
                        const query = new MySQLTransactionQuery(queryString, this.connectionManager);
                        /** @type {?} */
                        const result = yield query.execute();
                        results.push(result);
                    }
                    yield this.commit();
                    this.resolve(results);
                }
                catch (e) {
                    yield this.rollback();
                    this.reject(e);
                }
                this.onCompleteHandler();
            })));
        });
    }
    /**
     * @private
     * @return {?}
     */
    commit() {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.connection.commit((/**
             * @param {?} error
             * @return {?}
             */
            (error) => tslib_1.__awaiter(this, void 0, void 0, function* () {
                if (error) {
                    return reject(error);
                }
                resolve();
            })));
        }));
    }
    /**
     * @private
     * @return {?}
     */
    rollback() {
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        (resolve) => {
            this.connection.rollback(resolve);
        }));
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.resolve;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.reject;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.connection;
    /** @type {?} */
    MySQLTransactionExecuter.prototype.init;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.queries;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.onInitHandler;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.onCompleteHandler;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.connectionManager;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtdHJhbnNhY3Rpb24tbWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL2VuZ2luZS9teXNxbC9teXNxbC10cmFuc2FjdGlvbi1tYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUdBLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUd0RCxNQUFNLE9BQU8sdUJBQXVCOzs7O0lBT2hDLFlBQVksTUFBbUI7UUFMdkIsVUFBSyxHQUE4QixFQUFFLENBQUM7UUFDdEMsMkJBQXNCLEdBQUcsS0FBSyxDQUFDOzs7Ozs7O1FBK0IvQiwyQkFBc0I7OztRQUFHLEdBQUcsRUFBRTtZQUNsQyxJQUNJLElBQUksQ0FBQyxzQkFBc0I7Z0JBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFDdkI7Z0JBQ0UsT0FBTzthQUNWO1lBRUQsT0FBTyxDQUFDLEdBQUcsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1lBRXpDLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7OztrQkFHN0IsUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFO1lBRW5DLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN2QixDQUFDLEVBQUM7UUFFTSwyQkFBc0I7OztRQUFHLEdBQUcsRUFBRTtZQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFFdEMsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztZQUVwQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUNsQyxDQUFDLEVBQUE7UUFsREcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksc0JBQXNCLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDaEUsQ0FBQzs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxPQUFnQjs7Ozs7O2NBS3pCLFFBQVEsR0FBRyxJQUFJLHdCQUF3QixDQUN6QyxPQUFPLEVBQ1AsSUFBSSxDQUFDLHNCQUFzQixFQUMzQixJQUFJLENBQUMsc0JBQXNCLEVBQzNCLElBQUksQ0FBQyxpQkFBaUIsQ0FDekI7UUFFRCx3QkFBd0I7UUFDeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFMUIsT0FBTyxJQUFJLE9BQU8sQ0FBc0IsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzNELENBQUM7Q0FnQ0o7Ozs7OztJQXpERyx3Q0FBOEM7Ozs7O0lBQzlDLHlEQUF1Qzs7Ozs7SUFFdkMsb0RBQTBEOzs7Ozs7Ozs7SUE2QjFELHlEQWdCRTs7Ozs7SUFFRix5REFNQzs7QUFLTCxNQUFNLHdCQUF3Qjs7Ozs7OztJQU0xQixZQUNZLE9BQWdCLEVBQ2hCLGFBQXNCLEVBQ3RCLGlCQUEwQixFQUMxQixpQkFBd0M7UUFIeEMsWUFBTyxHQUFQLE9BQU8sQ0FBUztRQUNoQixrQkFBYSxHQUFiLGFBQWEsQ0FBUztRQUN0QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQVM7UUFDMUIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUF1QjtRQUlwRCxTQUFJOzs7OztRQUFHLENBQUMsT0FBdUIsRUFBQyxNQUFlLEVBQUUsRUFBRTtZQUMvQyxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztZQUN2QixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztZQUVyQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDekIsQ0FBQyxFQUFBO0lBUEQsQ0FBQzs7OztJQVNLLE9BQU87O1lBQ1QsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUUvRCxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQjs7O1lBQUMsR0FBUyxFQUFFO2dCQUN4QyxJQUFJOzswQkFDTSxPQUFPLEdBQXVCLEVBQUU7b0JBRXRDLEtBQUssTUFBTSxXQUFXLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTs7OEJBQzlCLEtBQUssR0FBRyxJQUFJLHFCQUFxQixDQUNuQyxXQUFXLEVBQ1gsSUFBSSxDQUFDLGlCQUFpQixDQUN6Qjs7OEJBRUssTUFBTSxHQUFHLE1BQU0sS0FBSyxDQUFDLE9BQU8sRUFBRTt3QkFFcEMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztxQkFDeEI7b0JBRUQsTUFBTSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7b0JBRXBCLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ3pCO2dCQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNSLE1BQU0sSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUV0QixJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNsQjtnQkFFRCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztZQUM3QixDQUFDLENBQUEsRUFBQyxDQUFDO1FBQ1AsQ0FBQztLQUFBOzs7OztJQUVPLE1BQU07UUFDVixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU07Ozs7WUFBQyxDQUFPLEtBQUssRUFBRSxFQUFFO2dCQUNuQyxJQUFJLEtBQUssRUFBRTtvQkFDUCxPQUFPLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDeEI7Z0JBRUQsT0FBTyxFQUFFLENBQUM7WUFDZCxDQUFDLENBQUEsRUFBQyxDQUFDO1FBQ1AsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVPLFFBQVE7UUFDWixPQUFPLElBQUksT0FBTzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdEMsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDO0NBQ0o7Ozs7OztJQW5FRywyQ0FBZ0M7Ozs7O0lBQ2hDLDBDQUF3Qjs7Ozs7SUFDeEIsOENBQThCOztJQVU5Qix3Q0FLQzs7Ozs7SUFaRywyQ0FBd0I7Ozs7O0lBQ3hCLGlEQUE4Qjs7Ozs7SUFDOUIscURBQWtDOzs7OztJQUNsQyxxREFBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb25uZWN0aW9uIH0gZnJvbSBcIm15c3FsXCI7XG5cbmltcG9ydCB7IElNeVNRTENvbmZpZywgSU15U1FMUXVlcnlSZXN1bHQgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgTXlTUUxDb25uZWN0aW9uTWFuYWdlciB9IGZyb20gJy4vbXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyJztcbmltcG9ydCB7IE15U1FMVHJhbnNhY3Rpb25RdWVyeSB9IGZyb20gJy4vbXlzcWwtcXVlcnknO1xuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTFRyYW5zYWN0aW9uTWFuYWdlclxue1xuICAgIHByaXZhdGUgcXVldWU6TXlTUUxUcmFuc2FjdGlvbkV4ZWN1dGVyW10gPSBbXTtcbiAgICBwcml2YXRlIGlzRXhlY3V0aW5nVHJhbnNhY3Rpb24gPSBmYWxzZTtcblxuICAgIHByaXZhdGUgcmVhZG9ubHkgY29ubmVjdGlvbk1hbmFnZXI6TXlTUUxDb25uZWN0aW9uTWFuYWdlcjtcblxuICAgIGNvbnN0cnVjdG9yKGNvbmZpZzpJTXlTUUxDb25maWcpIHtcbiAgICAgICAgdGhpcy5jb25uZWN0aW9uTWFuYWdlciA9IG5ldyBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyKGNvbmZpZyk7XG4gICAgfVxuXG4gICAgcGVyZm9ybVRyYW5zYWN0aW9uKHF1ZXJpZXM6c3RyaW5nW10pIHtcbiAgICAgICAgLypcbiAgICAgICAgd2UgbmVlZCB0byBwcm9taXNlIHRoZSByZXN1bHQgb2YgZWFjaCBxdWVyeSBleGVjdXRlZFxuICAgICAgICBzbyB0aGF0IHRoZXkgY2FuIGJlIGluc3BlY3RlZCBieSBjb25zdW1lciBmb3IgSWRzLCBldGNcbiAgICAgICAgKi9cbiAgICAgICAgY29uc3QgZXhlY3V0b3IgPSBuZXcgTXlTUUxUcmFuc2FjdGlvbkV4ZWN1dGVyKFxuICAgICAgICAgICAgcXVlcmllcyxcbiAgICAgICAgICAgIHRoaXMucGVyZm9ybU5leHRUcmFuc2FjdGlvbixcbiAgICAgICAgICAgIHRoaXMub25UcmFuc2FjdGlvbkNvbXBsZXRlZCxcbiAgICAgICAgICAgIHRoaXMuY29ubmVjdGlvbk1hbmFnZXJcbiAgICAgICAgKTtcblxuICAgICAgICAvLyBhZGQgZXhlY3V0b3IgdG8gcXVldWVcbiAgICAgICAgdGhpcy5xdWV1ZS5wdXNoKGV4ZWN1dG9yKTtcblxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2U8SU15U1FMUXVlcnlSZXN1bHRbXT4oZXhlY3V0b3IuaW5pdCk7XG4gICAgfVxuXG4gICAgLyoqKiB3ZSBuZWVkIHRvIG1hbmFnZSBhIHF1ZXVlIG9mIHRyYW5zYWN0aW9uc1xuICAgICAqIHRvIGJlIGV4ZWN1dGVkIGluIG9yZGVyXG4gICAgICogb25jZSB0aGUgdHJhbnNhY3Rpb24gZWl0aGVyIGlzIHN1Y2Nlc3NmdWwgb3IgZmFpbHNcbiAgICAgKiB0aGVuIG1vdmUgb24gdG8gdGhlIG5leHRcbiAgICAgKi9cbiAgICBwcml2YXRlIHBlcmZvcm1OZXh0VHJhbnNhY3Rpb24gPSAoKSA9PiB7XG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIHRoaXMuaXNFeGVjdXRpbmdUcmFuc2FjdGlvbiB8fFxuICAgICAgICAgICAgdGhpcy5xdWV1ZS5sZW5ndGggPCAxXG4gICAgICAgICkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc29sZS5sb2coJ3BlcmZvcm0gbmV4dCB0cmFuc2FjdGlvbiEnKTtcblxuICAgICAgICB0aGlzLmlzRXhlY3V0aW5nVHJhbnNhY3Rpb24gPSB0cnVlO1xuXG4gICAgICAgIC8vIHJlbW92ZSBmaXJzdCBlbGVtZW50IGZyb20gcXVldWUgYW5kIHJldHVybiBpdFxuICAgICAgICBjb25zdCBleGVjdXRvciA9IHRoaXMucXVldWUuc2hpZnQoKTtcblxuICAgICAgICBleGVjdXRvci5leGVjdXRlKCk7XG4gICAgfTtcblxuICAgIHByaXZhdGUgb25UcmFuc2FjdGlvbkNvbXBsZXRlZCA9ICgpID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coJ29uVHJhbnNhY3Rpb25Db21wbGV0ZWQnKTtcblxuICAgICAgICB0aGlzLmlzRXhlY3V0aW5nVHJhbnNhY3Rpb24gPSBmYWxzZTtcblxuICAgICAgICB0aGlzLnBlcmZvcm1OZXh0VHJhbnNhY3Rpb24oKTtcbiAgICB9XG59XG5cbnR5cGUgUmVzb2x2ZUZ1bmN0aW9uID0gKHJlc3VsdHM6SU15U1FMUXVlcnlSZXN1bHRbXSkgPT4gdm9pZDtcblxuY2xhc3MgTXlTUUxUcmFuc2FjdGlvbkV4ZWN1dGVyXG57XG4gICAgcHJpdmF0ZSByZXNvbHZlOlJlc29sdmVGdW5jdGlvbjtcbiAgICBwcml2YXRlIHJlamVjdDpGdW5jdGlvbjtcbiAgICBwcml2YXRlIGNvbm5lY3Rpb246Q29ubmVjdGlvbjtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIHF1ZXJpZXM6c3RyaW5nW10sXG4gICAgICAgIHByaXZhdGUgb25Jbml0SGFuZGxlcjpGdW5jdGlvbixcbiAgICAgICAgcHJpdmF0ZSBvbkNvbXBsZXRlSGFuZGxlcjpGdW5jdGlvbixcbiAgICAgICAgcHJpdmF0ZSBjb25uZWN0aW9uTWFuYWdlcjpNeVNRTENvbm5lY3Rpb25NYW5hZ2VyXG4gICAgKSB7XG4gICAgfVxuXG4gICAgaW5pdCA9IChyZXNvbHZlOlJlc29sdmVGdW5jdGlvbixyZWplY3Q6RnVuY3Rpb24pID0+IHtcbiAgICAgICAgdGhpcy5yZXNvbHZlID0gcmVzb2x2ZTtcbiAgICAgICAgdGhpcy5yZWplY3QgPSByZWplY3Q7XG5cbiAgICAgICAgdGhpcy5vbkluaXRIYW5kbGVyKCk7XG4gICAgfVxuXG4gICAgYXN5bmMgZXhlY3V0ZSgpIHtcbiAgICAgICAgdGhpcy5jb25uZWN0aW9uID0gYXdhaXQgdGhpcy5jb25uZWN0aW9uTWFuYWdlci5nZXRDb25uZWN0aW9uKCk7XG5cbiAgICAgICAgdGhpcy5jb25uZWN0aW9uLmJlZ2luVHJhbnNhY3Rpb24oYXN5bmMgKCkgPT4ge1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBjb25zdCByZXN1bHRzOklNeVNRTFF1ZXJ5UmVzdWx0W10gPSBbXTtcblxuICAgICAgICAgICAgICAgIGZvciAoY29uc3QgcXVlcnlTdHJpbmcgb2YgdGhpcy5xdWVyaWVzKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHF1ZXJ5ID0gbmV3IE15U1FMVHJhbnNhY3Rpb25RdWVyeShcbiAgICAgICAgICAgICAgICAgICAgICAgIHF1ZXJ5U3RyaW5nLFxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uTWFuYWdlclxuICAgICAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IHF1ZXJ5LmV4ZWN1dGUoKTtcblxuICAgICAgICAgICAgICAgICAgICByZXN1bHRzLnB1c2gocmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmNvbW1pdCgpO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5yZXNvbHZlKHJlc3VsdHMpO1xuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMucm9sbGJhY2soKTtcblxuICAgICAgICAgICAgICAgIHRoaXMucmVqZWN0KGUpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLm9uQ29tcGxldGVIYW5kbGVyKCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgY29tbWl0KCkge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uLmNvbW1pdChhc3luYyAoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlamVjdChlcnJvcik7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgcm9sbGJhY2soKSB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uLnJvbGxiYWNrKHJlc29sdmUpO1xuICAgICAgICB9KTtcbiAgICB9XG59Il19