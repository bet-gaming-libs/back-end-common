/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import * as mysql from "mysql";
import { sleep } from 'earnbet-common';
export class MySQLConnectionManager {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.config = config;
        this.isConnecting = false;
        this.connect();
    }
    /**
     * @return {?}
     */
    getConnection() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            yield this.reconnect();
            return this.connection;
        });
    }
    /**
     * @return {?}
     */
    reconnect() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            while (this.isDisconnected) {
                /** @type {?} */
                const isConnected = yield this.connect();
                if (isConnected) {
                    return;
                }
                yield sleep(1000);
            }
        });
    }
    /**
     * @return {?}
     */
    connect() {
        return new Promise((resolve) => {
            if (!this.isDisconnected) {
                resolve(true);
                return;
            }
            if (this.isConnecting) {
                resolve(false);
                return;
            }
            this.isConnecting = true;
            // end existing connection
            if (this.connection) {
                this.connection.end();
            }
            // connection parameters
            this.connection = mysql.createConnection(this.config);
            // establish connection
            this.connection.connect((err) => {
                if (err) {
                    console.error('error connecting: ' + err.stack);
                    resolve(false);
                    return;
                }
                console.log('connected as id ' + this.connection.threadId);
                resolve(true);
                this.isConnecting = false;
            });
            this.connection.on('error', (err) => {
                console.error(err);
            });
        });
    }
    /**
     * @return {?}
     */
    get isDisconnected() {
        return this.connection == undefined ||
            this.connection.state === 'disconnected';
    }
    /**
     * @return {?}
     */
    endConnection() {
        this.connection.end();
    }
}
if (false) {
    /** @type {?} */
    MySQLConnectionManager.prototype.isConnecting;
    /** @type {?} */
    MySQLConnectionManager.prototype.connection;
    /** @type {?} */
    MySQLConnectionManager.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvZGIvbXlzcWwvbXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxFQUFDLEtBQUssRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBSXJDLE1BQU07Ozs7SUFNRixZQUFvQixNQUFtQjtRQUFuQixXQUFNLEdBQU4sTUFBTSxDQUFhOzRCQUpoQixLQUFLO1FBS3hCLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztLQUNsQjs7OztJQUVLLGFBQWE7O1lBQ2YsTUFBTSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFFdkIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7O0tBQzFCOzs7O0lBRWEsU0FBUzs7WUFDbkIsT0FBTyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7O2dCQUN6QixNQUFNLFdBQVcsR0FBRyxNQUFNLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFFekMsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztvQkFDZCxNQUFNLENBQUM7aUJBQ1Y7Z0JBRUQsTUFBTSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDckI7Ozs7OztJQUdHLE9BQU87UUFFWCxNQUFNLENBQUMsSUFBSSxPQUFPLENBQVcsQ0FBQyxPQUFPLEVBQUMsRUFBRTtZQUNwQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2QsTUFBTSxDQUFDO2FBQ1Y7WUFHRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDcEIsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNmLE1BQU0sQ0FBQzthQUNWO1lBR0QsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7O1lBSXpCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUNsQixJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDO2FBQ3pCOztZQUlELElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzs7WUFHdEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLEVBQUMsRUFBRTtnQkFDM0IsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDTixPQUFPLENBQUMsS0FBSyxDQUFDLG9CQUFvQixHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFFaEQsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNmLE1BQU0sQ0FBQztpQkFDVjtnQkFHRCxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzNELE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFHZCxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQzthQUM3QixDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUMsQ0FBQyxHQUFHLEVBQUMsRUFBRTtnQkFDOUIsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUN0QixDQUFDLENBQUM7U0FDTixDQUFFLENBQUM7Ozs7O1FBR0ksY0FBYztRQUN0QixNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxTQUFTO1lBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxLQUFLLGNBQWMsQ0FBQzs7Ozs7SUFHckQsYUFBYTtRQUNULElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUM7S0FDekI7Q0FDSiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIG15c3FsIGZyb20gXCJteXNxbFwiO1xuXG5pbXBvcnQge3NsZWVwfSBmcm9tICdlYXJuYmV0LWNvbW1vbic7XG5pbXBvcnQgeyBJTXlTUUxDb25maWcgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyXG57XG4gICAgcHJpdmF0ZSBpc0Nvbm5lY3RpbmcgPSBmYWxzZTtcblxuICAgIHByaXZhdGUgY29ubmVjdGlvbjpteXNxbC5Db25uZWN0aW9uO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjb25maWc6SU15U1FMQ29uZmlnKSB7XG4gICAgICAgIHRoaXMuY29ubmVjdCgpO1xuICAgIH1cblxuICAgIGFzeW5jIGdldENvbm5lY3Rpb24oKTpQcm9taXNlPG15c3FsLkNvbm5lY3Rpb24+IHtcbiAgICAgICAgYXdhaXQgdGhpcy5yZWNvbm5lY3QoKTtcblxuICAgICAgICByZXR1cm4gdGhpcy5jb25uZWN0aW9uO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgcmVjb25uZWN0KCkge1xuICAgICAgICB3aGlsZSAodGhpcy5pc0Rpc2Nvbm5lY3RlZCkge1xuICAgICAgICAgICAgY29uc3QgaXNDb25uZWN0ZWQgPSBhd2FpdCB0aGlzLmNvbm5lY3QoKTtcblxuICAgICAgICAgICAgaWYgKGlzQ29ubmVjdGVkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBhd2FpdCBzbGVlcCgxMDAwKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgY29ubmVjdCgpOlByb21pc2U8Ym9vbGVhbj5cbiAgICB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxib29sZWFuPiggKHJlc29sdmUpPT57XG4gICAgICAgICAgICBpZiAoIXRoaXMuaXNEaXNjb25uZWN0ZWQpIHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKHRydWUpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgICBpZiAodGhpcy5pc0Nvbm5lY3RpbmcpIHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKGZhbHNlKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgdGhpcy5pc0Nvbm5lY3RpbmcgPSB0cnVlO1xuXG5cbiAgICAgICAgICAgIC8vIGVuZCBleGlzdGluZyBjb25uZWN0aW9uXG4gICAgICAgICAgICBpZiAodGhpcy5jb25uZWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uLmVuZCgpO1xuICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgIC8vIGNvbm5lY3Rpb24gcGFyYW1ldGVyc1xuICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uID0gbXlzcWwuY3JlYXRlQ29ubmVjdGlvbih0aGlzLmNvbmZpZyk7XG5cbiAgICAgICAgICAgIC8vIGVzdGFibGlzaCBjb25uZWN0aW9uXG4gICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb24uY29ubmVjdCgoZXJyKT0+e1xuICAgICAgICAgICAgICAgIGlmIChlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignZXJyb3IgY29ubmVjdGluZzogJyArIGVyci5zdGFjayk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjb25uZWN0ZWQgYXMgaWQgJyArIHRoaXMuY29ubmVjdGlvbi50aHJlYWRJZCk7XG4gICAgICAgICAgICAgICAgcmVzb2x2ZSh0cnVlKTtcblxuXG4gICAgICAgICAgICAgICAgdGhpcy5pc0Nvbm5lY3RpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb24ub24oJ2Vycm9yJywoZXJyKT0+e1xuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9ICk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXQgaXNEaXNjb25uZWN0ZWQoKTpib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29ubmVjdGlvbiA9PSB1bmRlZmluZWQgfHxcbiAgICAgICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb24uc3RhdGUgPT09ICdkaXNjb25uZWN0ZWQnO1xuICAgIH1cblxuICAgIGVuZENvbm5lY3Rpb24oKSB7XG4gICAgICAgIHRoaXMuY29ubmVjdGlvbi5lbmQoKTtcbiAgICB9XG59Il19