/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { MySQLQueryBuilder } from './mysql-query-builder';
import { MySQLTransactionManager } from './mysql-transaction-manager';
export class MySQLDatabase {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.builder = new MySQLQueryBuilder(config);
        this.transactionManager = new MySQLTransactionManager(config);
    }
}
if (false) {
    /** @type {?} */
    MySQLDatabase.prototype.builder;
    /** @type {?} */
    MySQLDatabase.prototype.transactionManager;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtZGF0YWJhc2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9kYi9teXNxbC9teXNxbC1kYXRhYmFzZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDeEQsT0FBTyxFQUFDLHVCQUF1QixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFHcEUsTUFBTTs7OztJQUtGLFlBQVksTUFBbUI7UUFDM0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLHVCQUF1QixDQUFDLE1BQU0sQ0FBQyxDQUFDO0tBQ2pFO0NBQ0oiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJTXlTUUxEYXRhYmFzZSwgSU15U1FMQ29uZmlnIH0gZnJvbSBcIi4vaW50ZXJmYWNlc1wiO1xuaW1wb3J0IHtNeVNRTFF1ZXJ5QnVpbGRlcn0gZnJvbSAnLi9teXNxbC1xdWVyeS1idWlsZGVyJztcbmltcG9ydCB7TXlTUUxUcmFuc2FjdGlvbk1hbmFnZXJ9IGZyb20gJy4vbXlzcWwtdHJhbnNhY3Rpb24tbWFuYWdlcic7XG5cblxuZXhwb3J0IGNsYXNzIE15U1FMRGF0YWJhc2UgaW1wbGVtZW50cyBJTXlTUUxEYXRhYmFzZVxue1xuICAgIHJlYWRvbmx5IGJ1aWxkZXI6TXlTUUxRdWVyeUJ1aWxkZXI7XG4gICAgcmVhZG9ubHkgdHJhbnNhY3Rpb25NYW5hZ2VyOk15U1FMVHJhbnNhY3Rpb25NYW5hZ2VyO1xuXG4gICAgY29uc3RydWN0b3IoY29uZmlnOklNeVNRTENvbmZpZykge1xuICAgICAgICB0aGlzLmJ1aWxkZXIgPSBuZXcgTXlTUUxRdWVyeUJ1aWxkZXIoY29uZmlnKTtcbiAgICAgICAgdGhpcy50cmFuc2FjdGlvbk1hbmFnZXIgPSBuZXcgTXlTUUxUcmFuc2FjdGlvbk1hbmFnZXIoY29uZmlnKTtcbiAgICB9XG59Il19