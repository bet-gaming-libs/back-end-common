/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { MySQLConnectionManager } from "./mysql-connection-manager";
import { MySQLExpression, SingleExpression, StringStatement, OrExpressionGroup, UpdateStatements, createExpressionGroup, ExpressionGroupJoiner } from "./mysql-statements";
import { MySQLQuery } from './mysql-query';
export class MySQLQueryBuilder {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.connectionManager = new MySQLConnectionManager(config);
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    insert(repository, entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.constructInsertQuery(repository, entities);
            return new MySQLQuery(query, this.connectionManager);
        });
    }
    /**
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    constructInsertQuery(repository, entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const fields = [];
            for (const fieldName in entities[0]) {
                fields.push(fieldName);
            }
            /** @type {?} */
            const rows = [];
            for (const entity of entities) {
                rows.push(yield this.constructRowForInsert(entity));
            }
            return "INSERT INTO " + repository.tableName +
                ' (' + this.prepareFieldList(fields) + ')' +
                ' VALUES ' + rows.join(', ') + ';';
        });
    }
    /**
     * @param {?} entity
     * @return {?}
     */
    constructRowForInsert(entity) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const values = [];
            for (const propertyName in entity) {
                /** @type {?} */
                const rawValue = entity[propertyName];
                /** @type {?} */
                const preparedValue = yield this.prepareValueForInsert(rawValue);
                values.push(preparedValue);
            }
            return '(' + values.join(',') + ')';
        });
    }
    /**
     * @param {?} value
     * @return {?}
     */
    prepareValueForInsert(value) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            switch (typeof value) {
                case "string":
                    switch (value) {
                        case MySQLExpression.NULL:
                        case MySQLExpression.NOW:
                            break;
                        default:
                            value = yield this.escape(value);
                            break;
                    }
                    break;
            }
            return "" + value;
        });
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @param {?} primaryKeyValues
     * @return {?}
     */
    selectByPrimaryKeys(repository, fields, primaryKeyValues) {
        /** @type {?} */
        const query = this.prepareFieldsForSelect(fields, repository.tableName) +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @return {?}
     */
    selectAll(repository, fields) {
        /** @type {?} */
        const query = this.prepareFieldsForSelect(fields, repository.tableName) + ';';
        return new MySQLQuery(query, this.connectionManager);
    }
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    select(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.prepareSelectStatement(params);
            return new MySQLQuery(`${query};`, this.connectionManager);
        });
    }
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    prepareSelectStatement(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { fields, repository } = params;
            /** @type {?} */
            let query = this.prepareFieldsForSelect(fields, repository.tableName);
            query += yield this.prepareWhereOrderAndLimit(params);
            return query;
        });
    }
    /**
     * @param {?} fields
     * @param {?} tableName
     * @return {?}
     */
    prepareFieldsForSelect(fields, tableName) {
        /** @type {?} */
        const fieldsList = this.prepareFieldList(fields);
        return `SELECT ${fieldsList} FROM ${tableName}`;
    }
    /**
     * @param {?} fields
     * @return {?}
     */
    prepareFieldList(fields) {
        /** @type {?} */
        const prepared = [];
        for (const field of fields) {
            /** @type {?} */
            const isAlias = field.indexOf(' AS ') > -1;
            prepared.push(isAlias ?
                field :
                '`' + field + '`');
        }
        return prepared.join(',');
    }
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    update(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (params.singleStatement) {
                params.statements = [
                    params.singleStatement
                ];
            }
            const { statements, repository } = params;
            if (!statements ||
                statements.length < 1) {
                throw new Error('there must be at least 1 update statement!');
            }
            /** @type {?} */
            const updateStatements = yield this.prepareExpressionGroup(new UpdateStatements(statements));
            /** @type {?} */
            const remainingQuery = yield this.prepareWhereOrderAndLimit(params);
            /** @type {?} */
            const query = 'UPDATE ' + repository.tableName +
                ' SET ' + updateStatements +
                remainingQuery + ';';
            return new MySQLQuery(query, this.connectionManager);
        });
    }
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    prepareWhereOrderAndLimit(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (params.whereMap) {
                params.whereAndMap = params.whereMap;
            }
            if (params.whereAndMap ||
                params.whereOrMap) {
                params.whereGroup = createExpressionGroup(params.whereAndMap ?
                    params.whereAndMap :
                    params.whereOrMap, params.whereAndMap ?
                    ExpressionGroupJoiner.AND :
                    ExpressionGroupJoiner.OR);
            }
            else if (params.wherePrimaryKeys) {
                /** @type {?} */
                const expressions = params.wherePrimaryKeys.map((primaryKeyValue) => new StringStatement(params.repository.primaryKey, '' + primaryKeyValue));
                params.whereGroup = new OrExpressionGroup(expressions);
            }
            else if (params.whereClause) {
                params.whereGroup = new SingleExpression(params.whereClause);
            }
            const { whereGroup, orderBy, limit } = params;
            /** @type {?} */
            let query = '';
            /** @type {?} */
            const whereClause = whereGroup &&
                (yield this.prepareExpressionGroup(whereGroup));
            if (whereClause) {
                query += ` 
                WHERE ${whereClause}`;
            }
            if (orderBy) {
                query += ` 
                ORDER BY ${orderBy}`;
            }
            if (limit) {
                query += ` 
                LIMIT ${limit}`;
            }
            return query;
        });
    }
    /**
     * @param {?} group
     * @return {?}
     */
    prepareExpressionGroup(group) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const statements = [];
            /** @type {?} */
            const useBrackets = group.surroundExpressionWithBrackets;
            for (const expression of group.expressions) {
                const { leftSide, operator, rightSide } = expression;
                /** @type {?} */
                const rightSideValue = rightSide.isString ?
                    `${yield this.escape((/** @type {?} */ (rightSide.data)))}` :
                    rightSide.data;
                statements.push(`${useBrackets ? '(' : ''} \`${leftSide}\` ${operator} ${rightSideValue} ${useBrackets ? ')' : ''}`);
            }
            return statements.join(` ${group.joiner} `);
        });
    }
    /**
     * @param {?} value
     * @return {?}
     */
    escape(value) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const connection = yield this.connectionManager.getConnection();
            return connection.escape(value);
        });
    }
    /**
     * @param {?} repository
     * @param {?} primaryKeyValues
     * @return {?}
     */
    delete(repository, primaryKeyValues) {
        if (primaryKeyValues.length < 1) {
            throw new Error('Please specify which records to delete!');
        }
        /** @type {?} */
        const query = 'DELETE FROM ' + repository.tableName +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    }
    /**
     * @template T
     * @param {?} sql
     * @return {?}
     */
    rawQuery(sql) {
        return new MySQLQuery(sql, this.connectionManager);
    }
}
if (false) {
    /** @type {?} */
    MySQLQueryBuilder.prototype.connectionManager;
}
/**
 * @param {?} repository
 * @param {?} primaryKeyValues
 * @return {?}
 */
function constructWhereWithPrimaryKeys(repository, primaryKeyValues) {
    /** @type {?} */
    const conditions = [];
    for (let primaryKeyValue of primaryKeyValues) {
        if (repository.isPrimaryKeyAString) {
            primaryKeyValue = "'" + primaryKeyValue + "'";
        }
        /** @type {?} */
        const condition = repository.primaryKey + ' = ' + primaryKeyValue;
        conditions.push(condition);
    }
    return ' WHERE ' + conditions.join(' OR ');
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcXVlcnktYnVpbGRlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL215c3FsL215c3FsLXF1ZXJ5LWJ1aWxkZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFDQSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEVBQW1CLGVBQWUsRUFBRSxnQkFBZ0IsRUFBRSxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsZ0JBQWdCLEVBQUUscUJBQXFCLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUM1TCxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBR3pDLE1BQU07Ozs7SUFJRixZQUFZLE1BQW1CO1FBQzNCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0tBQy9EOzs7Ozs7O0lBRVksTUFBTSxDQUNmLFVBQTJCLEVBQzNCLFFBQVk7OztZQUdaLE1BQU0sS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLG9CQUFvQixDQUN6QyxVQUFVLEVBQUMsUUFBUSxDQUN0QixDQUFDO1lBRUYsTUFBTSxDQUFDLElBQUksVUFBVSxDQUFvQixLQUFLLEVBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7Ozs7Ozs7O0lBRTdELG9CQUFvQixDQUM5QixVQUEyQixFQUMzQixRQUFjOzs7WUFHZCxNQUFNLE1BQU0sR0FBWSxFQUFFLENBQUM7WUFDM0IsR0FBRyxDQUFDLENBQUMsTUFBTSxTQUFTLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUMxQjs7WUFFRCxNQUFNLElBQUksR0FBWSxFQUFFLENBQUM7WUFDekIsR0FBRyxDQUFDLENBQUMsTUFBTSxNQUFNLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDNUIsSUFBSSxDQUFDLElBQUksQ0FDTCxNQUFNLElBQUksQ0FBQyxxQkFBcUIsQ0FDNUIsTUFBTSxDQUNULENBQ0osQ0FBQzthQUNMO1lBRUQsTUFBTSxDQUFDLGNBQWMsR0FBRyxVQUFVLENBQUMsU0FBUztnQkFDeEMsSUFBSSxHQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsR0FBRSxHQUFHO2dCQUN4QyxVQUFVLEdBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBQyxHQUFHLENBQUE7Ozs7Ozs7SUFHeEIscUJBQXFCLENBQy9CLE1BQVU7OztZQUdWLE1BQU0sTUFBTSxHQUFZLEVBQUUsQ0FBQztZQUUzQixHQUFHLENBQUMsQ0FBQyxNQUFNLFlBQVksSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDOztnQkFDaEMsTUFBTSxRQUFRLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDOztnQkFFdEMsTUFBTSxhQUFhLEdBQ2YsTUFBTSxJQUFJLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBRS9DLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7YUFDOUI7WUFFRCxNQUFNLENBQUMsR0FBRyxHQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUUsR0FBRyxDQUFDOzs7Ozs7O0lBRXhCLHFCQUFxQixDQUFDLEtBQVM7O1lBRXpDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDbkIsS0FBSyxRQUFRO29CQUNULE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7d0JBQ1osS0FBSyxlQUFlLENBQUMsSUFBSSxDQUFDO3dCQUMxQixLQUFLLGVBQWUsQ0FBQyxHQUFHOzRCQUN4QixLQUFLLENBQUM7d0JBRU47NEJBQ0ksS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQzs0QkFDckMsS0FBSyxDQUFDO3FCQUNUO29CQUNMLEtBQUssQ0FBQzthQUNUO1lBRUQsTUFBTSxDQUFDLEVBQUUsR0FBQyxLQUFLLENBQUM7Ozs7Ozs7Ozs7SUFJYixtQkFBbUIsQ0FDdEIsVUFBMkIsRUFDM0IsTUFBZSxFQUNmLGdCQUFrQzs7UUFHbEMsTUFBTSxLQUFLLEdBQ1AsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sRUFBQyxVQUFVLENBQUMsU0FBUyxDQUFDO1lBQ3hELDZCQUE2QixDQUFDLFVBQVUsRUFBQyxnQkFBZ0IsQ0FBQyxHQUFFLEdBQUcsQ0FBQztRQUVwRSxNQUFNLENBQUMsSUFBSSxVQUFVLENBQU0sS0FBSyxFQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDOzs7Ozs7OztJQUd0RCxTQUFTLENBQ1osVUFBMkIsRUFDM0IsTUFBZTs7UUFFZixNQUFNLEtBQUssR0FDUCxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxFQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxHQUFHLENBQUM7UUFFbkUsTUFBTSxDQUFDLElBQUksVUFBVSxDQUFNLEtBQUssRUFBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzs7Ozs7OztJQUloRCxNQUFNLENBQUksTUFBbUM7OztZQUN0RCxNQUFNLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUV4RCxNQUFNLENBQUMsSUFBSSxVQUFVLENBQU0sR0FBRyxLQUFLLEdBQUcsRUFBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzs7Ozs7Ozs7SUFHckQsc0JBQXNCLENBQUksTUFBbUM7O1lBRXZFLE1BQU0sRUFBQyxNQUFNLEVBQUMsVUFBVSxFQUFDLEdBQUcsTUFBTSxDQUFDOztZQUVuQyxJQUFJLEtBQUssR0FBVSxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxFQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUU1RSxLQUFLLElBQUksTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFdEQsTUFBTSxDQUFDLEtBQUssQ0FBQzs7Ozs7Ozs7SUFHVCxzQkFBc0IsQ0FBQyxNQUFlLEVBQUMsU0FBZ0I7O1FBQzNELE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVqRCxNQUFNLENBQUMsVUFBVSxVQUFVLFNBQVMsU0FBUyxFQUFFLENBQUM7Ozs7OztJQUk1QyxnQkFBZ0IsQ0FBQyxNQUFlOztRQUVwQyxNQUFNLFFBQVEsR0FBWSxFQUFFLENBQUM7UUFFN0IsR0FBRyxDQUFDLENBQUMsTUFBTSxLQUFLLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQzs7WUFDekIsTUFBTSxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUUzQyxRQUFRLENBQUMsSUFBSSxDQUNULE9BQU8sQ0FBQyxDQUFDO2dCQUNULEtBQUssQ0FBQyxDQUFDO2dCQUNQLEdBQUcsR0FBQyxLQUFLLEdBQUMsR0FBRyxDQUNoQixDQUFDO1NBQ0w7UUFFRCxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzs7Ozs7OztJQUdqQixNQUFNLENBQUksTUFBbUM7O1lBQ3RELEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO2dCQUN6QixNQUFNLENBQUMsVUFBVSxHQUFHO29CQUNoQixNQUFNLENBQUMsZUFBZTtpQkFDekIsQ0FBQzthQUNMO1lBR0QsTUFBTSxFQUFDLFVBQVUsRUFBQyxVQUFVLEVBQUMsR0FBRyxNQUFNLENBQUM7WUFFdkMsRUFBRSxDQUFDLENBQ0MsQ0FBQyxVQUFVO2dCQUNYLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FDeEIsQ0FBQyxDQUFDLENBQUM7Z0JBQ0MsTUFBTSxJQUFJLEtBQUssQ0FBQyw0Q0FBNEMsQ0FBQyxDQUFDO2FBQ2pFOztZQUdELE1BQU0sZ0JBQWdCLEdBQVUsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQzdELElBQUksZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQ25DLENBQUM7O1lBRUYsTUFBTSxjQUFjLEdBQVUsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQzlELE1BQU0sQ0FDVCxDQUFDOztZQUVGLE1BQU0sS0FBSyxHQUNQLFNBQVMsR0FBQyxVQUFVLENBQUMsU0FBUztnQkFDOUIsT0FBTyxHQUFFLGdCQUFnQjtnQkFDeEIsY0FBYyxHQUFFLEdBQUcsQ0FBQztZQUV6QixNQUFNLENBQUMsSUFBSSxVQUFVLENBQW9CLEtBQUssRUFBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzs7Ozs7Ozs7SUFHN0QseUJBQXlCLENBQUksTUFBNkI7O1lBQ3BFLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNsQixNQUFNLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUM7YUFDeEM7WUFFRCxFQUFFLENBQUMsQ0FDQyxNQUFNLENBQUMsV0FBVztnQkFDbEIsTUFBTSxDQUFDLFVBQ1gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0MsTUFBTSxDQUFDLFVBQVUsR0FBRyxxQkFBcUIsQ0FDckMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUNoQixNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQ3BCLE1BQU0sQ0FBQyxVQUFVLEVBQ3JCLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDaEIscUJBQXFCLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQzNCLHFCQUFxQixDQUFDLEVBQUUsQ0FDL0IsQ0FBQzthQUNMO1lBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7O2dCQUMvQixNQUFNLFdBQVcsR0FBcUIsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FDN0QsQ0FBQyxlQUFlLEVBQUUsRUFBRSxDQUFDLElBQUksZUFBZSxDQUNwQyxNQUFNLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFDNUIsRUFBRSxHQUFDLGVBQWUsQ0FDckIsQ0FDSixDQUFDO2dCQUVGLE1BQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUMxRDtZQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDMUIsTUFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLGdCQUFnQixDQUNwQyxNQUFNLENBQUMsV0FBVyxDQUNyQixDQUFDO2FBQ0w7WUFHRCxNQUFNLEVBQUMsVUFBVSxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsR0FBRyxNQUFNLENBQUM7O1lBRTFDLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQzs7WUFFZixNQUFNLFdBQVcsR0FDYixVQUFVO2lCQUNWLE1BQU0sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxDQUFBLENBQUM7WUFFbEQsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDZCxLQUFLLElBQUk7d0JBQ0csV0FBVyxFQUFFLENBQUM7YUFDN0I7WUFFRCxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNWLEtBQUssSUFBSTsyQkFDTSxPQUFPLEVBQUUsQ0FBQzthQUM1QjtZQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ1IsS0FBSyxJQUFJO3dCQUNHLEtBQUssRUFBRSxDQUFDO2FBQ3ZCO1lBRUQsTUFBTSxDQUFDLEtBQUssQ0FBQzs7Ozs7OztJQUdILHNCQUFzQixDQUFDLEtBQXFCOzs7WUFDdEQsTUFBTSxVQUFVLEdBQVksRUFBRSxDQUFDOztZQUUvQixNQUFNLFdBQVcsR0FBRyxLQUFLLENBQUMsOEJBQThCLENBQUM7WUFFekQsR0FBRyxDQUFDLENBQUMsTUFBTSxVQUFVLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pDLE1BQU0sRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFDLFNBQVMsRUFBQyxHQUFHLFVBQVUsQ0FBQzs7Z0JBRWpELE1BQU0sY0FBYyxHQUNoQixTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ2hCLEdBQUksTUFBTSxJQUFJLENBQUMsTUFBTSxvQkFBQyxTQUFTLENBQUMsSUFBYyxHQUFFLEVBQUUsQ0FBQyxDQUFDO29CQUNwRCxTQUFTLENBQUMsSUFBSSxDQUFDO2dCQUV2QixVQUFVLENBQUMsSUFBSSxDQUNYLEdBQ0ksV0FBVyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQ3hCLE1BQU0sUUFBUSxNQUFNLFFBQVEsSUFBSSxjQUFjLElBQzFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUN4QixFQUFFLENBQUMsQ0FBQzthQUNYO1lBRUQsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzs7Ozs7OztJQUlsQyxNQUFNLENBQUMsS0FBWTs7O1lBRTdCLE1BQU0sVUFBVSxHQUFHLE1BQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxDQUFDO1lBRWhFLE1BQU0sQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDOzs7Ozs7OztJQUc3QixNQUFNLENBQ1QsVUFBMkIsRUFDM0IsZ0JBQXNCO1FBR3RCLEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlCLE1BQU0sSUFBSSxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQztTQUM5RDs7UUFHRCxNQUFNLEtBQUssR0FBRyxjQUFjLEdBQUMsVUFBVSxDQUFDLFNBQVM7WUFDckMsNkJBQTZCLENBQUMsVUFBVSxFQUFDLGdCQUFnQixDQUFDLEdBQUMsR0FBRyxDQUFDO1FBRTNFLE1BQU0sQ0FBQyxJQUFJLFVBQVUsQ0FBb0IsS0FBSyxFQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDOzs7Ozs7O0lBRzNFLFFBQVEsQ0FBSSxHQUFXO1FBQ25CLE1BQU0sQ0FBQyxJQUFJLFVBQVUsQ0FBSSxHQUFHLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7S0FDekQ7Q0FDSjs7Ozs7Ozs7OztBQUdELHVDQUNJLFVBQTJCLEVBQzNCLGdCQUFzQjs7SUFHdEIsTUFBTSxVQUFVLEdBQVksRUFBRSxDQUFDO0lBRS9CLEdBQUcsQ0FBQyxDQUFDLElBQUksZUFBZSxJQUFJLGdCQUFnQixDQUFDLENBQUMsQ0FBQztRQUMzQyxFQUFFLENBQUMsQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLGVBQWUsR0FBRyxHQUFHLEdBQUMsZUFBZSxHQUFDLEdBQUcsQ0FBQztTQUM3Qzs7UUFFRCxNQUFNLFNBQVMsR0FBRyxVQUFVLENBQUMsVUFBVSxHQUFHLEtBQUssR0FBRyxlQUFlLENBQUM7UUFDbEUsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztLQUM5QjtJQUVELE1BQU0sQ0FBQyxTQUFTLEdBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztDQUM1QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElNeVNRTFJlcG9zaXRvcnksIFByaW1hcnlLZXlWYWx1ZSwgSU15U1FMUXVlcnlSZXN1bHQsIElNeVNRTENvbmZpZywgSVNlbGVjdFF1ZXJ5QnVpbGRlclBhcmFtcywgSVVwZGF0ZVF1ZXJ5QnVpbGRlclBhcmFtcywgSVF1ZXJ5QnVpbGRlclBhcmFtcyB9IGZyb20gXCIuL2ludGVyZmFjZXNcIjtcbmltcG9ydCB7IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIgfSBmcm9tIFwiLi9teXNxbC1jb25uZWN0aW9uLW1hbmFnZXJcIjtcbmltcG9ydCB7IEV4cHJlc3Npb25Hcm91cCwgTXlTUUxFeHByZXNzaW9uLCBTaW5nbGVFeHByZXNzaW9uLCBTdHJpbmdTdGF0ZW1lbnQsIE9yRXhwcmVzc2lvbkdyb3VwLCBVcGRhdGVTdGF0ZW1lbnRzLCBjcmVhdGVFeHByZXNzaW9uR3JvdXAsIEV4cHJlc3Npb25Hcm91cEpvaW5lciB9IGZyb20gXCIuL215c3FsLXN0YXRlbWVudHNcIjtcbmltcG9ydCB7TXlTUUxRdWVyeX0gZnJvbSAnLi9teXNxbC1xdWVyeSc7XG5cblxuZXhwb3J0IGNsYXNzIE15U1FMUXVlcnlCdWlsZGVyXG57XG4gICAgcHJpdmF0ZSBjb25uZWN0aW9uTWFuYWdlcjpNeVNRTENvbm5lY3Rpb25NYW5hZ2VyO1xuXG4gICAgY29uc3RydWN0b3IoY29uZmlnOklNeVNRTENvbmZpZykge1xuICAgICAgICB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyID0gbmV3IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIoY29uZmlnKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgYXN5bmMgaW5zZXJ0PFQ+KFxuICAgICAgICByZXBvc2l0b3J5OklNeVNRTFJlcG9zaXRvcnksXG4gICAgICAgIGVudGl0aWVzOlRbXVxuICAgICkge1xuICAgICAgICAvLyBjb25zdHJ1Y3QgcXVlcnlcbiAgICAgICAgY29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLmNvbnN0cnVjdEluc2VydFF1ZXJ5KFxuICAgICAgICAgICAgcmVwb3NpdG9yeSxlbnRpdGllc1xuICAgICAgICApO1xuXG4gICAgICAgIHJldHVybiBuZXcgTXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4ocXVlcnksdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxuICAgIHByaXZhdGUgYXN5bmMgY29uc3RydWN0SW5zZXJ0UXVlcnkoXG4gICAgICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeSxcbiAgICAgICAgZW50aXRpZXM6YW55W11cbiAgICApOlByb21pc2U8c3RyaW5nPlxuICAgIHtcbiAgICAgICAgY29uc3QgZmllbGRzOnN0cmluZ1tdID0gW107XG4gICAgICAgIGZvciAoY29uc3QgZmllbGROYW1lIGluIGVudGl0aWVzWzBdKSB7XG4gICAgICAgICAgICBmaWVsZHMucHVzaChmaWVsZE5hbWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3Qgcm93czpzdHJpbmdbXSA9IFtdO1xuICAgICAgICBmb3IgKGNvbnN0IGVudGl0eSBvZiBlbnRpdGllcykge1xuICAgICAgICAgICAgcm93cy5wdXNoKFxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuY29uc3RydWN0Um93Rm9ySW5zZXJ0KFxuICAgICAgICAgICAgICAgICAgICBlbnRpdHlcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIFwiSU5TRVJUIElOVE8gXCIgKyByZXBvc2l0b3J5LnRhYmxlTmFtZSArXG4gICAgICAgICAgICAnICgnKyB0aGlzLnByZXBhcmVGaWVsZExpc3QoZmllbGRzKSArJyknICtcbiAgICAgICAgICAgICcgVkFMVUVTICcrcm93cy5qb2luKCcsICcpKyc7J1xuXG4gICAgfVxuICAgIHByaXZhdGUgYXN5bmMgY29uc3RydWN0Um93Rm9ySW5zZXJ0KFxuICAgICAgICBlbnRpdHk6YW55XG4gICAgKTpQcm9taXNlPHN0cmluZz5cbiAgICB7XG4gICAgICAgIGNvbnN0IHZhbHVlczpzdHJpbmdbXSA9IFtdO1xuXG4gICAgICAgIGZvciAoY29uc3QgcHJvcGVydHlOYW1lIGluIGVudGl0eSkge1xuICAgICAgICAgICAgY29uc3QgcmF3VmFsdWUgPSBlbnRpdHlbcHJvcGVydHlOYW1lXTtcblxuICAgICAgICAgICAgY29uc3QgcHJlcGFyZWRWYWx1ZTpzdHJpbmcgPVxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMucHJlcGFyZVZhbHVlRm9ySW5zZXJ0KHJhd1ZhbHVlKTtcblxuICAgICAgICAgICAgdmFsdWVzLnB1c2gocHJlcGFyZWRWYWx1ZSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gJygnKyB2YWx1ZXMuam9pbignLCcpICsnKSc7XG4gICAgfVxuICAgIHByaXZhdGUgYXN5bmMgcHJlcGFyZVZhbHVlRm9ySW5zZXJ0KHZhbHVlOmFueSk6UHJvbWlzZTxzdHJpbmc+XG4gICAge1xuICAgICAgICBzd2l0Y2ggKHR5cGVvZiB2YWx1ZSkge1xuICAgICAgICAgICAgY2FzZSBcInN0cmluZ1wiOlxuICAgICAgICAgICAgICAgIHN3aXRjaCAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBNeVNRTEV4cHJlc3Npb24uTlVMTDpcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBNeVNRTEV4cHJlc3Npb24uTk9XOlxuICAgICAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSBhd2FpdCB0aGlzLmVzY2FwZSh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIFwiXCIrdmFsdWU7XG4gICAgfVxuXG5cbiAgICBwdWJsaWMgc2VsZWN0QnlQcmltYXJ5S2V5czxUPihcbiAgICAgICAgcmVwb3NpdG9yeTpJTXlTUUxSZXBvc2l0b3J5LFxuICAgICAgICBmaWVsZHM6c3RyaW5nW10sXG4gICAgICAgIHByaW1hcnlLZXlWYWx1ZXM6UHJpbWFyeUtleVZhbHVlW11cbiAgICApXG4gICAge1xuICAgICAgICBjb25zdCBxdWVyeSA9IFxuICAgICAgICAgICAgdGhpcy5wcmVwYXJlRmllbGRzRm9yU2VsZWN0KGZpZWxkcyxyZXBvc2l0b3J5LnRhYmxlTmFtZSkgK1xuICAgICAgICAgICAgY29uc3RydWN0V2hlcmVXaXRoUHJpbWFyeUtleXMocmVwb3NpdG9yeSxwcmltYXJ5S2V5VmFsdWVzKSArJzsnO1xuXG4gICAgICAgIHJldHVybiBuZXcgTXlTUUxRdWVyeTxUW10+KHF1ZXJ5LHRoaXMuY29ubmVjdGlvbk1hbmFnZXIpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzZWxlY3RBbGw8VD4oXG4gICAgICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeSxcbiAgICAgICAgZmllbGRzOnN0cmluZ1tdLFxuICAgICkge1xuICAgICAgICBjb25zdCBxdWVyeSA9XG4gICAgICAgICAgICB0aGlzLnByZXBhcmVGaWVsZHNGb3JTZWxlY3QoZmllbGRzLHJlcG9zaXRvcnkudGFibGVOYW1lKSArICc7JztcblxuICAgICAgICByZXR1cm4gbmV3IE15U1FMUXVlcnk8VFtdPihxdWVyeSx0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyKTtcbiAgICB9XG5cblxuICAgIHB1YmxpYyBhc3luYyBzZWxlY3Q8VD4ocGFyYW1zOklTZWxlY3RRdWVyeUJ1aWxkZXJQYXJhbXM8VD4pIHtcbiAgICAgICAgY29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLnByZXBhcmVTZWxlY3RTdGF0ZW1lbnQocGFyYW1zKTtcblxuICAgICAgICByZXR1cm4gbmV3IE15U1FMUXVlcnk8VFtdPihgJHtxdWVyeX07YCx0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGFzeW5jIHByZXBhcmVTZWxlY3RTdGF0ZW1lbnQ8VD4ocGFyYW1zOklTZWxlY3RRdWVyeUJ1aWxkZXJQYXJhbXM8VD4pOlByb21pc2U8c3RyaW5nPlxuICAgIHtcbiAgICAgICAgY29uc3Qge2ZpZWxkcyxyZXBvc2l0b3J5fSA9IHBhcmFtcztcblxuICAgICAgICBsZXQgcXVlcnk6c3RyaW5nID0gdGhpcy5wcmVwYXJlRmllbGRzRm9yU2VsZWN0KGZpZWxkcyxyZXBvc2l0b3J5LnRhYmxlTmFtZSk7XG5cbiAgICAgICAgcXVlcnkgKz0gYXdhaXQgdGhpcy5wcmVwYXJlV2hlcmVPcmRlckFuZExpbWl0KHBhcmFtcyk7XG5cbiAgICAgICAgcmV0dXJuIHF1ZXJ5O1xuICAgIH1cblxuICAgIHByaXZhdGUgcHJlcGFyZUZpZWxkc0ZvclNlbGVjdChmaWVsZHM6c3RyaW5nW10sdGFibGVOYW1lOnN0cmluZyk6c3RyaW5nIHtcbiAgICAgICAgY29uc3QgZmllbGRzTGlzdCA9IHRoaXMucHJlcGFyZUZpZWxkTGlzdChmaWVsZHMpO1xuXG4gICAgICAgIHJldHVybiBgU0VMRUNUICR7ZmllbGRzTGlzdH0gRlJPTSAke3RhYmxlTmFtZX1gO1xuICAgIH1cblxuXG4gICAgcHJpdmF0ZSBwcmVwYXJlRmllbGRMaXN0KGZpZWxkczpzdHJpbmdbXSk6c3RyaW5nXG4gICAge1xuICAgICAgICBjb25zdCBwcmVwYXJlZDpzdHJpbmdbXSA9IFtdO1xuXG4gICAgICAgIGZvciAoY29uc3QgZmllbGQgb2YgZmllbGRzKSB7XG4gICAgICAgICAgICBjb25zdCBpc0FsaWFzID0gZmllbGQuaW5kZXhPZignIEFTICcpID4gLTE7XG5cbiAgICAgICAgICAgIHByZXBhcmVkLnB1c2goXG4gICAgICAgICAgICAgICAgaXNBbGlhcyA/XG4gICAgICAgICAgICAgICAgZmllbGQgOlxuICAgICAgICAgICAgICAgICdgJytmaWVsZCsnYCdcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gcHJlcGFyZWQuam9pbignLCcpO1xuICAgIH1cblxuICAgIHB1YmxpYyBhc3luYyB1cGRhdGU8VD4ocGFyYW1zOklVcGRhdGVRdWVyeUJ1aWxkZXJQYXJhbXM8VD4pIHtcbiAgICAgICAgaWYgKHBhcmFtcy5zaW5nbGVTdGF0ZW1lbnQpIHtcbiAgICAgICAgICAgIHBhcmFtcy5zdGF0ZW1lbnRzID0gW1xuICAgICAgICAgICAgICAgIHBhcmFtcy5zaW5nbGVTdGF0ZW1lbnRcbiAgICAgICAgICAgIF07XG4gICAgICAgIH1cblxuXG4gICAgICAgIGNvbnN0IHtzdGF0ZW1lbnRzLHJlcG9zaXRvcnl9ID0gcGFyYW1zO1xuXG4gICAgICAgIGlmIChcbiAgICAgICAgICAgICFzdGF0ZW1lbnRzIHx8XG4gICAgICAgICAgICBzdGF0ZW1lbnRzLmxlbmd0aCA8IDFcbiAgICAgICAgKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ3RoZXJlIG11c3QgYmUgYXQgbGVhc3QgMSB1cGRhdGUgc3RhdGVtZW50IScpO1xuICAgICAgICB9XG5cblxuICAgICAgICBjb25zdCB1cGRhdGVTdGF0ZW1lbnRzOnN0cmluZyA9IGF3YWl0IHRoaXMucHJlcGFyZUV4cHJlc3Npb25Hcm91cChcbiAgICAgICAgICAgIG5ldyBVcGRhdGVTdGF0ZW1lbnRzKHN0YXRlbWVudHMpXG4gICAgICAgICk7XG5cbiAgICAgICAgY29uc3QgcmVtYWluaW5nUXVlcnk6c3RyaW5nID0gYXdhaXQgdGhpcy5wcmVwYXJlV2hlcmVPcmRlckFuZExpbWl0KFxuICAgICAgICAgICAgcGFyYW1zXG4gICAgICAgICk7XG5cbiAgICAgICAgY29uc3QgcXVlcnk6c3RyaW5nID0gXG4gICAgICAgICAgICAnVVBEQVRFICcrcmVwb3NpdG9yeS50YWJsZU5hbWUrXG4gICAgICAgICAgICAnIFNFVCAnKyB1cGRhdGVTdGF0ZW1lbnRzICtcbiAgICAgICAgICAgICByZW1haW5pbmdRdWVyeSArJzsnO1xuXG4gICAgICAgIHJldHVybiBuZXcgTXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4ocXVlcnksdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhc3luYyBwcmVwYXJlV2hlcmVPcmRlckFuZExpbWl0PFQ+KHBhcmFtczpJUXVlcnlCdWlsZGVyUGFyYW1zPFQ+KSB7XG4gICAgICAgIGlmIChwYXJhbXMud2hlcmVNYXApIHtcbiAgICAgICAgICAgIHBhcmFtcy53aGVyZUFuZE1hcCA9IHBhcmFtcy53aGVyZU1hcDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIHBhcmFtcy53aGVyZUFuZE1hcCB8fFxuICAgICAgICAgICAgcGFyYW1zLndoZXJlT3JNYXBcbiAgICAgICAgKSB7XG4gICAgICAgICAgICBwYXJhbXMud2hlcmVHcm91cCA9IGNyZWF0ZUV4cHJlc3Npb25Hcm91cChcbiAgICAgICAgICAgICAgICBwYXJhbXMud2hlcmVBbmRNYXAgP1xuICAgICAgICAgICAgICAgICAgICBwYXJhbXMud2hlcmVBbmRNYXAgOlxuICAgICAgICAgICAgICAgICAgICBwYXJhbXMud2hlcmVPck1hcCxcbiAgICAgICAgICAgICAgICBwYXJhbXMud2hlcmVBbmRNYXAgP1xuICAgICAgICAgICAgICAgICAgICBFeHByZXNzaW9uR3JvdXBKb2luZXIuQU5EIDpcbiAgICAgICAgICAgICAgICAgICAgRXhwcmVzc2lvbkdyb3VwSm9pbmVyLk9SXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHBhcmFtcy53aGVyZVByaW1hcnlLZXlzKSB7XG4gICAgICAgICAgICBjb25zdCBleHByZXNzaW9uczpTdHJpbmdTdGF0ZW1lbnRbXSA9IHBhcmFtcy53aGVyZVByaW1hcnlLZXlzLm1hcChcbiAgICAgICAgICAgICAgICAocHJpbWFyeUtleVZhbHVlKSA9PiBuZXcgU3RyaW5nU3RhdGVtZW50KFxuICAgICAgICAgICAgICAgICAgICBwYXJhbXMucmVwb3NpdG9yeS5wcmltYXJ5S2V5LFxuICAgICAgICAgICAgICAgICAgICAnJytwcmltYXJ5S2V5VmFsdWVcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApO1xuXG4gICAgICAgICAgICBwYXJhbXMud2hlcmVHcm91cCA9IG5ldyBPckV4cHJlc3Npb25Hcm91cChleHByZXNzaW9ucyk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAocGFyYW1zLndoZXJlQ2xhdXNlKSB7XG4gICAgICAgICAgICBwYXJhbXMud2hlcmVHcm91cCA9IG5ldyBTaW5nbGVFeHByZXNzaW9uKFxuICAgICAgICAgICAgICAgIHBhcmFtcy53aGVyZUNsYXVzZVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgY29uc3Qge3doZXJlR3JvdXAsb3JkZXJCeSxsaW1pdH0gPSBwYXJhbXM7XG5cbiAgICAgICAgbGV0IHF1ZXJ5ID0gJyc7XG5cbiAgICAgICAgY29uc3Qgd2hlcmVDbGF1c2UgPSBcbiAgICAgICAgICAgIHdoZXJlR3JvdXAgJiYgXG4gICAgICAgICAgICBhd2FpdCB0aGlzLnByZXBhcmVFeHByZXNzaW9uR3JvdXAod2hlcmVHcm91cCk7XG5cbiAgICAgICAgaWYgKHdoZXJlQ2xhdXNlKSB7XG4gICAgICAgICAgICBxdWVyeSArPSBgIFxuICAgICAgICAgICAgICAgIFdIRVJFICR7d2hlcmVDbGF1c2V9YDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChvcmRlckJ5KSB7XG4gICAgICAgICAgICBxdWVyeSArPSBgIFxuICAgICAgICAgICAgICAgIE9SREVSIEJZICR7b3JkZXJCeX1gO1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBpZiAobGltaXQpIHtcbiAgICAgICAgICAgIHF1ZXJ5ICs9IGAgXG4gICAgICAgICAgICAgICAgTElNSVQgJHtsaW1pdH1gO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHF1ZXJ5O1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgcHJlcGFyZUV4cHJlc3Npb25Hcm91cChncm91cDpFeHByZXNzaW9uR3JvdXApOlByb21pc2U8c3RyaW5nPiB7XG4gICAgICAgIGNvbnN0IHN0YXRlbWVudHM6c3RyaW5nW10gPSBbXTtcblxuICAgICAgICBjb25zdCB1c2VCcmFja2V0cyA9IGdyb3VwLnN1cnJvdW5kRXhwcmVzc2lvbldpdGhCcmFja2V0cztcblxuICAgICAgICBmb3IgKGNvbnN0IGV4cHJlc3Npb24gb2YgZ3JvdXAuZXhwcmVzc2lvbnMpIHtcbiAgICAgICAgICAgIGNvbnN0IHtsZWZ0U2lkZSxvcGVyYXRvcixyaWdodFNpZGV9ID0gZXhwcmVzc2lvbjtcblxuICAgICAgICAgICAgY29uc3QgcmlnaHRTaWRlVmFsdWUgPVxuICAgICAgICAgICAgICAgIHJpZ2h0U2lkZS5pc1N0cmluZyA/XG4gICAgICAgICAgICAgICAgICAgIGAkeyBhd2FpdCB0aGlzLmVzY2FwZShyaWdodFNpZGUuZGF0YSBhcyBzdHJpbmcpIH1gIDpcbiAgICAgICAgICAgICAgICAgICAgcmlnaHRTaWRlLmRhdGE7XG5cbiAgICAgICAgICAgIHN0YXRlbWVudHMucHVzaChcbiAgICAgICAgICAgICAgICBgJHsgXG4gICAgICAgICAgICAgICAgICAgIHVzZUJyYWNrZXRzID8gJygnIDogJydcbiAgICAgICAgICAgICAgICB9IFxcYCR7bGVmdFNpZGV9XFxgICR7b3BlcmF0b3J9ICR7cmlnaHRTaWRlVmFsdWV9ICR7XG4gICAgICAgICAgICAgICAgICAgIHVzZUJyYWNrZXRzID8gJyknIDogJydcbiAgICAgICAgICAgICAgICB9YCk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gc3RhdGVtZW50cy5qb2luKGAgJHtncm91cC5qb2luZXJ9IGApO1xuICAgIH1cblxuXG4gICAgcHJpdmF0ZSBhc3luYyBlc2NhcGUodmFsdWU6c3RyaW5nKTpQcm9taXNlPHN0cmluZz5cbiAgICB7XG4gICAgICAgIGNvbnN0IGNvbm5lY3Rpb24gPSBhd2FpdCB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyLmdldENvbm5lY3Rpb24oKTtcblxuICAgICAgICByZXR1cm4gY29ubmVjdGlvbi5lc2NhcGUodmFsdWUpO1xuICAgIH1cblxuICAgIHB1YmxpYyBkZWxldGUoXG4gICAgICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeSxcbiAgICAgICAgcHJpbWFyeUtleVZhbHVlczphbnlbXVxuICAgIClcbiAgICB7XG4gICAgICAgIGlmIChwcmltYXJ5S2V5VmFsdWVzLmxlbmd0aCA8IDEpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignUGxlYXNlIHNwZWNpZnkgd2hpY2ggcmVjb3JkcyB0byBkZWxldGUhJyk7XG4gICAgICAgIH1cblxuXG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gJ0RFTEVURSBGUk9NICcrcmVwb3NpdG9yeS50YWJsZU5hbWUrXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0cnVjdFdoZXJlV2l0aFByaW1hcnlLZXlzKHJlcG9zaXRvcnkscHJpbWFyeUtleVZhbHVlcykrJzsnO1xuXG4gICAgICAgIHJldHVybiBuZXcgTXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4ocXVlcnksdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxuXG4gICAgcmF3UXVlcnk8VD4oc3FsOiBzdHJpbmcpOiBNeVNRTFF1ZXJ5PFQ+IHtcbiAgICAgICAgcmV0dXJuIG5ldyBNeVNRTFF1ZXJ5PFQ+KHNxbCwgdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxufVxuXG5cbmZ1bmN0aW9uIGNvbnN0cnVjdFdoZXJlV2l0aFByaW1hcnlLZXlzKFxuICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeSxcbiAgICBwcmltYXJ5S2V5VmFsdWVzOmFueVtdXG4pOnN0cmluZ1xue1xuICAgIGNvbnN0IGNvbmRpdGlvbnM6c3RyaW5nW10gPSBbXTtcblxuICAgIGZvciAobGV0IHByaW1hcnlLZXlWYWx1ZSBvZiBwcmltYXJ5S2V5VmFsdWVzKSB7XG4gICAgICAgIGlmIChyZXBvc2l0b3J5LmlzUHJpbWFyeUtleUFTdHJpbmcpIHtcbiAgICAgICAgICAgIHByaW1hcnlLZXlWYWx1ZSA9IFwiJ1wiK3ByaW1hcnlLZXlWYWx1ZStcIidcIjtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGNvbmRpdGlvbiA9IHJlcG9zaXRvcnkucHJpbWFyeUtleSArICcgPSAnICsgcHJpbWFyeUtleVZhbHVlO1xuICAgICAgICBjb25kaXRpb25zLnB1c2goY29uZGl0aW9uKTtcbiAgICB9XG5cbiAgICByZXR1cm4gJyBXSEVSRSAnK2NvbmRpdGlvbnMuam9pbignIE9SICcpO1xufSJdfQ==