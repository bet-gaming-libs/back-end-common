/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/** @type {?} */
const logSqlQueries = process.env["LOG_SQL_QUERIES"] === 'true';
/**
 * @template T
 */
export class MySQLQuery {
    /**
     * @param {?} sql
     * @param {?} connectionManager
     */
    constructor(sql, connectionManager) {
        this.sql = sql;
        this.connectionManager = connectionManager;
    }
    /**
     * @return {?}
     */
    execute() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const connection = yield this.connectionManager.getConnection();
            return new Promise((resolve, reject) => {
                if (logSqlQueries) {
                    console.log(this.sql);
                }
                connection.query(this.sql, function (error, result, fields) {
                    if (error) {
                        //console.log('*** MySQL Error: ***');
                        //console.log(err);
                        //throw error;
                        reject(error);
                    }
                    else {
                        resolve(result);
                    }
                });
            });
        });
    }
}
if (false) {
    /** @type {?} */
    MySQLQuery.prototype.sql;
    /** @type {?} */
    MySQLQuery.prototype.connectionManager;
}
export class MySQLTransactionQuery extends MySQLQuery {
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcXVlcnkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9kYi9teXNxbC9teXNxbC1xdWVyeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFJQSxNQUFNLGFBQWEsR0FDZixPQUFPLENBQUMsR0FBRyx3QkFBcUIsTUFBTSxDQUFDOzs7O0FBRzNDLE1BQU07Ozs7O0lBRUYsWUFDYSxHQUFVLEVBQ1g7UUFEQyxRQUFHLEdBQUgsR0FBRyxDQUFPO1FBQ1gsc0JBQWlCLEdBQWpCLGlCQUFpQjtLQUU1Qjs7OztJQUVLLE9BQU87OztZQUdULE1BQU0sVUFBVSxHQUFHLE1BQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxDQUFDO1lBRWhFLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBSyxDQUFDLE9BQU8sRUFBQyxNQUFNLEVBQUMsRUFBRTtnQkFDckMsRUFBRSxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztvQkFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ3pCO2dCQUVELFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBQyxVQUFTLEtBQUssRUFBQyxNQUFNLEVBQUMsTUFBTTtvQkFDbEQsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzs7Ozt3QkFJUixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQ2pCO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNKLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztxQkFDbkI7aUJBQ0osQ0FBQyxDQUFDO2FBQ04sQ0FBQyxDQUFDOztLQUNOO0NBQ0o7Ozs7Ozs7QUFFRCxNQUFNLDRCQUE2QixTQUFRLFVBQTZCO0NBRXZFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTXlTUUxDb25uZWN0aW9uTWFuYWdlciB9IGZyb20gXCIuL215c3FsLWNvbm5lY3Rpb24tbWFuYWdlclwiO1xuaW1wb3J0IHsgSU15U1FMUXVlcnlSZXN1bHQgfSBmcm9tIFwiLi9pbnRlcmZhY2VzXCI7XG5cblxuY29uc3QgbG9nU3FsUXVlcmllcyA9IFxuICAgIHByb2Nlc3MuZW52LkxPR19TUUxfUVVFUklFUyA9PT0gJ3RydWUnO1xuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTFF1ZXJ5PFQ+XG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHJlYWRvbmx5IHNxbDpzdHJpbmcsXG4gICAgICAgIHByaXZhdGUgY29ubmVjdGlvbk1hbmFnZXI6TXlTUUxDb25uZWN0aW9uTWFuYWdlclxuICAgICkge1xuICAgIH1cblxuICAgIGFzeW5jIGV4ZWN1dGUoKTpQcm9taXNlPFQ+IHtcbiAgICAgICAgLy8gV2hlbiBleGVjdXRlIGlzIGNhbGxlZCBkaXJlY3RseSxcbiAgICAgICAgLy8gYnkgZGVmYXVsdCwgaXQgd2lsbCB1c2UgdGhlIGNvbm5lY3Rpb25NYW5hZ2VyIHVzZWQgdG8gY29uc3RydWN0IHRoZSBxdWVyeS4uLlxuICAgICAgICBjb25zdCBjb25uZWN0aW9uID0gYXdhaXQgdGhpcy5jb25uZWN0aW9uTWFuYWdlci5nZXRDb25uZWN0aW9uKCk7XG5cbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlPFQ+KCAocmVzb2x2ZSxyZWplY3QpPT57XG4gICAgICAgICAgICBpZiAobG9nU3FsUXVlcmllcykge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMuc3FsKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29ubmVjdGlvbi5xdWVyeSh0aGlzLnNxbCxmdW5jdGlvbihlcnJvcixyZXN1bHQsZmllbGRzKXtcbiAgICAgICAgICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZygnKioqIE15U1FMIEVycm9yOiAqKionKTtcbiAgICAgICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhlcnIpO1xuICAgICAgICAgICAgICAgICAgICAvL3Rocm93IGVycm9yO1xuICAgICAgICAgICAgICAgICAgICByZWplY3QoZXJyb3IpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUocmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgTXlTUUxUcmFuc2FjdGlvblF1ZXJ5IGV4dGVuZHMgTXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD5cbntcbn0iXX0=