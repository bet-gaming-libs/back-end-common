/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/** @typedef {?} */
var SelectOneHandler;
export { SelectOneHandler };
/**
 * @template TypeForSelect
 */
export class MySQLRepository {
    /**
     * @param {?} db
     * @param {?} _tableName
     * @param {?=} defaultFieldsToSelect
     * @param {?=} _primaryKey
     * @param {?=} isPrimaryKeyAString
     */
    constructor(db, _tableName, defaultFieldsToSelect = [], _primaryKey = 'id', isPrimaryKeyAString = false) {
        this.db = db;
        this._tableName = _tableName;
        this.defaultFieldsToSelect = defaultFieldsToSelect;
        this._primaryKey = _primaryKey;
        this.isPrimaryKeyAString = isPrimaryKeyAString;
    }
    /**
     * @template T
     * @param {?} entity
     * @return {?}
     */
    insert(entity) {
        return this.insertMany([entity]);
    }
    /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    insertMany(entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.buildInsert(entities);
            return yield query.execute();
        });
    }
    /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    buildInsert(entities) {
        return this.db.builder.insert(this, entities);
    }
    /**
     * @param {?} primaryKeyValue
     * @param {?=} fields
     * @return {?}
     */
    selectOneByPrimaryKey(primaryKeyValue, fields = this.defaultFieldsToSelect) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const rows = yield this.selectManyByPrimaryKeys([primaryKeyValue], fields);
            return rows.length > 0 ?
                rows[0] :
                null;
        });
    }
    /**
     * @param {?} primaryKeyValues
     * @param {?=} fields
     * @return {?}
     */
    selectManyByPrimaryKeys(primaryKeyValues, fields = this.defaultFieldsToSelect) {
        return this.db.builder.selectByPrimaryKeys(this, fields, primaryKeyValues).execute();
    }
    /**
     * @param {?=} fields
     * @return {?}
     */
    selectAll(fields = this.defaultFieldsToSelect) {
        return this.db.builder.selectAll(this, fields).execute();
    }
    /**
     * @param {?} params
     * @return {?}
     */
    selectOne(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const rows = yield this.select(Object.assign({}, params, { limit: 1 }));
            return rows.length > 0 ?
                rows[0] :
                null;
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    select(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.buildSelect(params);
            return query.execute();
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    buildSelect(params) {
        /** @type {?} */
        const fields = params.fields ?
            params.fields :
            this.defaultFieldsToSelect;
        /** @type {?} */
        const newParams = Object.assign({}, params, { fields, repository: this });
        return this.db.builder.select(newParams);
    }
    /**
     * @param {?} params
     * @return {?}
     */
    updateOne(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.buildUpdateOne(params);
            return query.execute();
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    buildUpdateOne(params) {
        return this.buildUpdate(Object.assign({}, params, { limit: 1 }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    update(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.buildUpdate(params);
            return query.execute();
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    buildUpdate(params) {
        /** @type {?} */
        const builderParams = Object.assign({}, params, { repository: this });
        return this.db.builder.update(builderParams);
    }
    /**
     * @param {?} primaryKeyValues
     * @return {?}
     */
    delete(primaryKeyValues) {
        return this.db.builder.delete(this, primaryKeyValues).execute();
    }
    /**
     * @return {?}
     */
    get tableName() {
        return this._tableName;
    }
    /**
     * @return {?}
     */
    get primaryKey() {
        return this._primaryKey;
    }
    /**
     * @return {?}
     */
    get primaryKeyFieldList() {
        return [this.primaryKey];
    }
}
if (false) {
    /** @type {?} */
    MySQLRepository.prototype.db;
    /** @type {?} */
    MySQLRepository.prototype._tableName;
    /** @type {?} */
    MySQLRepository.prototype.defaultFieldsToSelect;
    /** @type {?} */
    MySQLRepository.prototype._primaryKey;
    /** @type {?} */
    MySQLRepository.prototype.isPrimaryKeyAString;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcmVwb3NpdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL215c3FsL215c3FsLXJlcG9zaXRvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFrQkEsTUFBTTs7Ozs7Ozs7SUFFRixZQUNjLEVBQWlCLEVBQ25CLFlBQ0Usd0JBQWlDLEVBQUUsRUFDckMsY0FBcUIsSUFBSSxFQUN4QixzQkFBc0IsS0FBSztRQUoxQixPQUFFLEdBQUYsRUFBRSxDQUFlO1FBQ25CLGVBQVUsR0FBVixVQUFVO1FBQ1IsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUFjO1FBQ3JDLGdCQUFXLEdBQVgsV0FBVztRQUNWLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBUTtLQUV2Qzs7Ozs7O0lBRU0sTUFBTSxDQUFJLE1BQVE7UUFFckIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDOzs7Ozs7O0lBRXhCLFVBQVUsQ0FBSSxRQUFZOzs7WUFFbkMsTUFBTSxLQUFLLEdBQUcsTUFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQy9DLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQzs7Ozs7Ozs7SUFHMUIsV0FBVyxDQUFJLFFBQVk7UUFFOUIsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBSSxJQUFJLEVBQUMsUUFBUSxDQUFDLENBQUM7Ozs7Ozs7SUFJdkMscUJBQXFCLENBQzlCLGVBQStCLEVBQy9CLFNBQWtCLElBQUksQ0FBQyxxQkFBcUI7OztZQUU1QyxNQUFNLElBQUksR0FBRyxNQUFNLElBQUksQ0FBQyx1QkFBdUIsQ0FDM0MsQ0FBQyxlQUFlLENBQUMsRUFDakIsTUFBTSxDQUNULENBQUM7WUFFRixNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDaEIsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ1QsSUFBSSxDQUFDOzs7Ozs7OztJQUdWLHVCQUF1QixDQUMxQixnQkFBa0MsRUFDbEMsU0FBa0IsSUFBSSxDQUFDLHFCQUFxQjtRQUU1QyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQ3RDLElBQUksRUFBQyxNQUFNLEVBQUMsZ0JBQWdCLENBQy9CLENBQUMsT0FBTyxFQUFFLENBQUM7Ozs7OztJQUdULFNBQVMsQ0FBQyxTQUFrQixJQUFJLENBQUMscUJBQXFCO1FBQ3pELE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQzVCLElBQUksRUFDSixNQUFNLENBQ1QsQ0FBQyxPQUFPLEVBQUUsQ0FBQzs7Ozs7O0lBSUgsU0FBUyxDQUFDLE1BQXdDOzs7WUFDM0QsTUFBTSxJQUFJLEdBQUcsTUFBTSxJQUFJLENBQUMsTUFBTSxtQkFDdkIsTUFBTSxJQUNULEtBQUssRUFBRSxDQUFDLElBQ1YsQ0FBQztZQUVILE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNoQixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDVCxJQUFJLENBQUM7Ozs7Ozs7SUFHSixNQUFNLENBQUMsTUFBd0M7OztZQUN4RCxNQUFNLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFN0MsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQzs7Ozs7OztJQUdwQixXQUFXLENBQUMsTUFBd0M7O1FBQ3ZELE1BQU0sTUFBTSxHQUNSLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNYLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxxQkFBcUIsQ0FBQzs7UUFFbkMsTUFBTSxTQUFTLHFCQUNSLE1BQU0sSUFDVCxNQUFNLEVBQ04sVUFBVSxFQUFFLElBQUksSUFDbkI7UUFFRCxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFnQixTQUFTLENBQUMsQ0FBQzs7Ozs7O0lBSS9DLFNBQVMsQ0FBQyxNQUF3Qzs7O1lBQzNELE1BQU0sS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUVoRCxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDOzs7Ozs7O0lBRXBCLGNBQWMsQ0FBQyxNQUF3QztRQUMxRCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsbUJBQ2hCLE1BQU0sSUFDVCxLQUFLLEVBQUUsQ0FBQyxJQUNWLENBQUM7Ozs7OztJQUdNLE1BQU0sQ0FBQyxNQUF3Qzs7O1lBQ3hELE1BQU0sS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM3QyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDOzs7Ozs7O0lBRXBCLFdBQVcsQ0FBQyxNQUF3Qzs7UUFDdkQsTUFBTSxhQUFhLHFCQUNaLE1BQU0sSUFDVCxVQUFVLEVBQUUsSUFBSSxJQUNuQjtRQUVELE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7Ozs7OztJQUkxQyxNQUFNLENBQUMsZ0JBQWtDO1FBRTVDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFDLGdCQUFnQixDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7Ozs7O1FBSXhELFNBQVM7UUFFaEIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7Ozs7O1FBR2hCLFVBQVU7UUFFakIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7Ozs7O1FBR2hCLG1CQUFtQjtRQUUzQixNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7O0NBRWhDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgICBJTXlTUUxEYXRhYmFzZSxcbiAgICBJTXlTUUxSZXBvc2l0b3J5LFxuICAgIFByaW1hcnlLZXlWYWx1ZSxcbiAgICBJTXlTUUxRdWVyeVJlc3VsdCxcbiAgICBJU2VsZWN0UXVlcnlCdWlsZGVyUGFyYW1zLFxuICAgIElTZWxlY3RRdWVyeVBhcmFtcyxcbiAgICBJVXBkYXRlUXVlcnlQYXJhbXMsXG4gICAgSVVwZGF0ZVF1ZXJ5QnVpbGRlclBhcmFtcyxcbn0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7IE15U1FMUXVlcnkgfSBmcm9tICcuL215c3FsLXF1ZXJ5JztcblxuXG5cbmV4cG9ydCB0eXBlIFNlbGVjdE9uZUhhbmRsZXI8VD4gPSAocm93czpUW10pID0+IHZvaWQ7XG5cblxuXG5leHBvcnQgY2xhc3MgTXlTUUxSZXBvc2l0b3J5PFR5cGVGb3JTZWxlY3Q+IGltcGxlbWVudHMgSU15U1FMUmVwb3NpdG9yeVxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgZGI6SU15U1FMRGF0YWJhc2UsXG4gICAgICAgIHByaXZhdGUgX3RhYmxlTmFtZTpzdHJpbmcsXG4gICAgICAgIHByb3RlY3RlZCBkZWZhdWx0RmllbGRzVG9TZWxlY3Q6c3RyaW5nW10gPSBbXSxcbiAgICAgICAgcHJpdmF0ZSBfcHJpbWFyeUtleTpzdHJpbmcgPSAnaWQnLFxuICAgICAgICByZWFkb25seSBpc1ByaW1hcnlLZXlBU3RyaW5nID0gZmFsc2VcbiAgICApIHtcbiAgICB9XG5cbiAgICBwdWJsaWMgaW5zZXJ0PFQ+KGVudGl0eTpUKVxuICAgIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5zZXJ0TWFueShbZW50aXR5XSk7XG4gICAgfVxuICAgIHB1YmxpYyBhc3luYyBpbnNlcnRNYW55PFQ+KGVudGl0aWVzOlRbXSlcbiAgICB7XG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gYXdhaXQgdGhpcy5idWlsZEluc2VydChlbnRpdGllcyk7XG4gICAgICAgIHJldHVybiBhd2FpdCBxdWVyeS5leGVjdXRlKCk7XG4gICAgfVxuXG4gICAgcHVibGljIGJ1aWxkSW5zZXJ0PFQ+KGVudGl0aWVzOlRbXSk6UHJvbWlzZTxNeVNRTFF1ZXJ5PElNeVNRTFF1ZXJ5UmVzdWx0Pj5cbiAgICB7XG4gICAgICAgIHJldHVybiB0aGlzLmRiLmJ1aWxkZXIuaW5zZXJ0PFQ+KHRoaXMsZW50aXRpZXMpO1xuICAgIH1cblxuXG4gICAgcHVibGljIGFzeW5jIHNlbGVjdE9uZUJ5UHJpbWFyeUtleShcbiAgICAgICAgcHJpbWFyeUtleVZhbHVlOlByaW1hcnlLZXlWYWx1ZSxcbiAgICAgICAgZmllbGRzOnN0cmluZ1tdID0gdGhpcy5kZWZhdWx0RmllbGRzVG9TZWxlY3RcbiAgICApOlByb21pc2U8VHlwZUZvclNlbGVjdD4ge1xuICAgICAgICBjb25zdCByb3dzID0gYXdhaXQgdGhpcy5zZWxlY3RNYW55QnlQcmltYXJ5S2V5cyhcbiAgICAgICAgICAgIFtwcmltYXJ5S2V5VmFsdWVdLFxuICAgICAgICAgICAgZmllbGRzXG4gICAgICAgICk7XG5cbiAgICAgICAgcmV0dXJuIHJvd3MubGVuZ3RoID4gMCA/XG4gICAgICAgICAgICAgICAgcm93c1swXSA6XG4gICAgICAgICAgICAgICAgbnVsbDtcbiAgICB9XG5cbiAgICBwdWJsaWMgc2VsZWN0TWFueUJ5UHJpbWFyeUtleXMoXG4gICAgICAgIHByaW1hcnlLZXlWYWx1ZXM6UHJpbWFyeUtleVZhbHVlW10sXG4gICAgICAgIGZpZWxkczpzdHJpbmdbXSA9IHRoaXMuZGVmYXVsdEZpZWxkc1RvU2VsZWN0XG4gICAgKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRiLmJ1aWxkZXIuc2VsZWN0QnlQcmltYXJ5S2V5czxUeXBlRm9yU2VsZWN0PihcbiAgICAgICAgICAgIHRoaXMsZmllbGRzLHByaW1hcnlLZXlWYWx1ZXNcbiAgICAgICAgKS5leGVjdXRlKCk7XG4gICAgfVxuXG4gICAgcHVibGljIHNlbGVjdEFsbChmaWVsZHM6c3RyaW5nW10gPSB0aGlzLmRlZmF1bHRGaWVsZHNUb1NlbGVjdCkge1xuICAgICAgICByZXR1cm4gdGhpcy5kYi5idWlsZGVyLnNlbGVjdEFsbDxUeXBlRm9yU2VsZWN0PihcbiAgICAgICAgICAgIHRoaXMsXG4gICAgICAgICAgICBmaWVsZHNcbiAgICAgICAgKS5leGVjdXRlKCk7XG4gICAgfVxuXG4gICBcbiAgICBwdWJsaWMgYXN5bmMgc2VsZWN0T25lKHBhcmFtczpJU2VsZWN0UXVlcnlQYXJhbXM8VHlwZUZvclNlbGVjdD4pIHtcbiAgICAgICAgY29uc3Qgcm93cyA9IGF3YWl0IHRoaXMuc2VsZWN0KHtcbiAgICAgICAgICAgIC4uLnBhcmFtcyxcbiAgICAgICAgICAgIGxpbWl0OiAxXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiByb3dzLmxlbmd0aCA+IDAgP1xuICAgICAgICAgICAgICAgIHJvd3NbMF0gOlxuICAgICAgICAgICAgICAgIG51bGw7XG4gICAgfVxuXG4gICAgcHVibGljIGFzeW5jIHNlbGVjdChwYXJhbXM6SVNlbGVjdFF1ZXJ5UGFyYW1zPFR5cGVGb3JTZWxlY3Q+KSB7XG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gYXdhaXQgdGhpcy5idWlsZFNlbGVjdChwYXJhbXMpO1xuXG4gICAgICAgIHJldHVybiBxdWVyeS5leGVjdXRlKCk7XG4gICAgfVxuICAgIFxuICAgIHB1YmxpYyBidWlsZFNlbGVjdChwYXJhbXM6SVNlbGVjdFF1ZXJ5UGFyYW1zPFR5cGVGb3JTZWxlY3Q+KSB7XG4gICAgICAgIGNvbnN0IGZpZWxkcyA9XG4gICAgICAgICAgICBwYXJhbXMuZmllbGRzID9cbiAgICAgICAgICAgICAgICBwYXJhbXMuZmllbGRzIDpcbiAgICAgICAgICAgICAgICB0aGlzLmRlZmF1bHRGaWVsZHNUb1NlbGVjdDtcblxuICAgICAgICBjb25zdCBuZXdQYXJhbXM6SVNlbGVjdFF1ZXJ5QnVpbGRlclBhcmFtczxUeXBlRm9yU2VsZWN0PiA9IHtcbiAgICAgICAgICAgIC4uLnBhcmFtcyxcbiAgICAgICAgICAgIGZpZWxkcyxcbiAgICAgICAgICAgIHJlcG9zaXRvcnk6IHRoaXNcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLmRiLmJ1aWxkZXIuc2VsZWN0PFR5cGVGb3JTZWxlY3Q+KG5ld1BhcmFtcyk7XG4gICAgfVxuXG5cbiAgICBwdWJsaWMgYXN5bmMgdXBkYXRlT25lKHBhcmFtczpJVXBkYXRlUXVlcnlQYXJhbXM8VHlwZUZvclNlbGVjdD4pIHtcbiAgICAgICAgY29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLmJ1aWxkVXBkYXRlT25lKHBhcmFtcyk7XG5cbiAgICAgICAgcmV0dXJuIHF1ZXJ5LmV4ZWN1dGUoKTtcbiAgICB9XG4gICAgcHVibGljIGJ1aWxkVXBkYXRlT25lKHBhcmFtczpJVXBkYXRlUXVlcnlQYXJhbXM8VHlwZUZvclNlbGVjdD4pIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYnVpbGRVcGRhdGUoe1xuICAgICAgICAgICAgLi4ucGFyYW1zLFxuICAgICAgICAgICAgbGltaXQ6IDFcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHVibGljIGFzeW5jIHVwZGF0ZShwYXJhbXM6SVVwZGF0ZVF1ZXJ5UGFyYW1zPFR5cGVGb3JTZWxlY3Q+KSB7XG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gYXdhaXQgdGhpcy5idWlsZFVwZGF0ZShwYXJhbXMpO1xuICAgICAgICByZXR1cm4gcXVlcnkuZXhlY3V0ZSgpO1xuICAgIH1cbiAgICBwdWJsaWMgYnVpbGRVcGRhdGUocGFyYW1zOklVcGRhdGVRdWVyeVBhcmFtczxUeXBlRm9yU2VsZWN0Pikge1xuICAgICAgICBjb25zdCBidWlsZGVyUGFyYW1zOklVcGRhdGVRdWVyeUJ1aWxkZXJQYXJhbXM8VHlwZUZvclNlbGVjdD4gPSB7XG4gICAgICAgICAgICAuLi5wYXJhbXMsXG4gICAgICAgICAgICByZXBvc2l0b3J5OiB0aGlzXG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gdGhpcy5kYi5idWlsZGVyLnVwZGF0ZShidWlsZGVyUGFyYW1zKTtcbiAgICB9XG5cblxuICAgIHB1YmxpYyBkZWxldGUocHJpbWFyeUtleVZhbHVlczpQcmltYXJ5S2V5VmFsdWVbXSlcbiAgICB7XG4gICAgICAgIHJldHVybiB0aGlzLmRiLmJ1aWxkZXIuZGVsZXRlKHRoaXMscHJpbWFyeUtleVZhbHVlcykuZXhlY3V0ZSgpO1xuICAgIH1cblxuXG4gICAgcHVibGljIGdldCB0YWJsZU5hbWUoKTpzdHJpbmdcbiAgICB7XG4gICAgICAgIHJldHVybiB0aGlzLl90YWJsZU5hbWU7XG4gICAgfVxuXG4gICAgcHVibGljIGdldCBwcmltYXJ5S2V5KCk6c3RyaW5nXG4gICAge1xuICAgICAgICByZXR1cm4gdGhpcy5fcHJpbWFyeUtleTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldCBwcmltYXJ5S2V5RmllbGRMaXN0KCk6c3RyaW5nW11cbiAgICB7XG4gICAgICAgIHJldHVybiBbdGhpcy5wcmltYXJ5S2V5XTtcbiAgICB9XG59Il19