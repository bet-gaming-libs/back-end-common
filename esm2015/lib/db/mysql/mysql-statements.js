/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/** @enum {number} */
const MySQLValueType = {
    EXPRESSION: 0,
    STRING: 1,
};
export { MySQLValueType };
MySQLValueType[MySQLValueType.EXPRESSION] = 'EXPRESSION';
MySQLValueType[MySQLValueType.STRING] = 'STRING';
/** @enum {string} */
const ExpressionOperator = {
    EQUAL: '=',
    LESS_THAN: '<',
    LESS_THAN_OR_EQUAL: '<=',
    GREATER_THAN: '>',
    GREATER_THAN_OR_EQUAL: '>=',
    IS: 'IS',
    IS_NOT: 'IS NOT',
};
export { ExpressionOperator };
/** @enum {string} */
const ExpressionGroupJoiner = {
    AND: 'AND',
    OR: 'OR',
    COMMA: ',',
};
export { ExpressionGroupJoiner };
/** @typedef {?} */
var ExpressionValueType;
export { ExpressionValueType };
/**
 * @record
 */
export function IMySQLValue() { }
/** @type {?} */
IMySQLValue.prototype.data;
/** @type {?} */
IMySQLValue.prototype.isString;
export class MySQLValue {
    /**
     * @param {?} data
     * @param {?} type
     */
    constructor(data, type) {
        this.data = data;
        this.type = type;
        this.isString =
            type == MySQLValueType.STRING;
    }
}
if (false) {
    /** @type {?} */
    MySQLValue.prototype.isString;
    /** @type {?} */
    MySQLValue.prototype.data;
    /** @type {?} */
    MySQLValue.prototype.type;
}
class MySQLNumberValue extends MySQLValue {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data, MySQLValueType.EXPRESSION);
    }
}
class MySQLStringValue extends MySQLValue {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data, MySQLValueType.STRING);
    }
}
class MySQLStringExpressionValue extends MySQLValue {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data, MySQLValueType.EXPRESSION);
    }
}
/** @enum {string} */
const MySQLExpression = {
    NOW: 'NOW()',
    NULL: 'NULL',
};
export { MySQLExpression };
/** @type {?} */
const nowValue = new MySQLStringExpressionValue(MySQLExpression.NOW);
/** @type {?} */
const nullValue = new MySQLStringExpressionValue(MySQLExpression.NULL);
/**
 * @abstract
 */
export class Expression {
    /**
     * @param {?} leftSide
     * @param {?} operator
     * @param {?} rightSide
     */
    constructor(leftSide, operator, rightSide) {
        this.leftSide = leftSide;
        this.operator = operator;
        this.rightSide = rightSide;
    }
}
if (false) {
    /** @type {?} */
    Expression.prototype.leftSide;
    /** @type {?} */
    Expression.prototype.operator;
    /** @type {?} */
    Expression.prototype.rightSide;
}
export class ExpressionGroup {
    /**
     * @param {?} expressions
     * @param {?} joiner
     * @param {?=} surroundExpressionWithBrackets
     */
    constructor(expressions, joiner, surroundExpressionWithBrackets = true) {
        this.expressions = expressions;
        this.joiner = joiner;
        this.surroundExpressionWithBrackets = surroundExpressionWithBrackets;
    }
}
if (false) {
    /** @type {?} */
    ExpressionGroup.prototype.expressions;
    /** @type {?} */
    ExpressionGroup.prototype.joiner;
    /** @type {?} */
    ExpressionGroup.prototype.surroundExpressionWithBrackets;
}
/**
 * @param {?} map
 * @param {?} joiner
 * @return {?}
 */
export function createExpressionGroup(map, joiner) {
    /** @type {?} */
    const expressions = [];
    // should we use Object.keys()?
    for (const fieldName in map) {
        /** @type {?} */
        const fieldValue = map[fieldName];
        expressions.push(typeof fieldValue == 'string' ?
            new StringStatement(fieldName, /** @type {?} */ (fieldValue)) :
            new NumberStatement(fieldName, fieldValue));
    }
    return new ExpressionGroup(expressions, joiner);
}
export class SingleExpression extends ExpressionGroup {
    /**
     * @param {?} expression
     */
    constructor(expression) {
        super([expression], undefined);
    }
}
export class AndExpressionGroup extends ExpressionGroup {
    /**
     * @param {?} expressions
     */
    constructor(expressions) {
        super(expressions, ExpressionGroupJoiner.AND);
    }
}
export class OrExpressionGroup extends ExpressionGroup {
    /**
     * @param {?} expressions
     */
    constructor(expressions) {
        super(expressions, ExpressionGroupJoiner.OR);
    }
}
export class UpdateStatements extends ExpressionGroup {
    /**
     * @param {?} expressions
     */
    constructor(expressions) {
        super(expressions, ExpressionGroupJoiner.COMMA, false);
    }
}
/**
 * @abstract
 */
export class FieldStatement extends Expression {
    /**
     * @param {?} fieldName
     * @param {?} value
     * @param {?} operator
     */
    constructor(fieldName, value, operator) {
        super(fieldName, operator, value);
    }
}
export class NumberStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     * @param {?} data
     * @param {?=} operator
     */
    constructor(fieldName, data, operator = ExpressionOperator.EQUAL) {
        super(fieldName, new MySQLNumberValue(data), operator);
    }
}
export class StringStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     * @param {?} data
     * @param {?=} operator
     */
    constructor(fieldName, data, operator = ExpressionOperator.EQUAL) {
        super(fieldName, new MySQLStringValue(data), operator);
    }
}
export class StringExpressionStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     * @param {?} data
     * @param {?=} operator
     */
    constructor(fieldName, data, operator = ExpressionOperator.EQUAL) {
        super(fieldName, new MySQLStringExpressionValue(data), operator);
    }
}
export class NullUpdateStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nullValue, ExpressionOperator.EQUAL);
    }
}
export class NowUpdateStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nowValue, ExpressionOperator.EQUAL);
    }
}
export class FieldIsNullStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nullValue, ExpressionOperator.IS);
    }
}
export class FieldIsNotNullStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nullValue, ExpressionOperator.IS_NOT);
    }
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtc3RhdGVtZW50cy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL215c3FsL215c3FsLXN0YXRlbWVudHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBRUksYUFBVTtJQUNWLFNBQU07Ozs4QkFETixVQUFVOzhCQUNWLE1BQU07OztJQUtOLE9BQXdCLEdBQUc7SUFDM0IsV0FBd0IsR0FBRztJQUMzQixvQkFBd0IsSUFBSTtJQUM1QixjQUF3QixHQUFHO0lBQzNCLHVCQUF3QixJQUFJO0lBRTVCLElBQXdCLElBQUk7SUFDNUIsUUFBd0IsUUFBUTs7Ozs7SUFLaEMsS0FBUSxLQUFLO0lBQ2IsSUFBUSxJQUFJO0lBQ1osT0FBUSxHQUFHOzs7Ozs7Ozs7Ozs7OztBQXdCZixNQUFNOzs7OztJQUlGLFlBQ2EsSUFBd0IsRUFDeEIsSUFBbUI7UUFEbkIsU0FBSSxHQUFKLElBQUksQ0FBb0I7UUFDeEIsU0FBSSxHQUFKLElBQUksQ0FBZTtRQUU1QixJQUFJLENBQUMsUUFBUTtZQUNULElBQUksSUFBSSxjQUFjLENBQUMsTUFBTSxDQUFDO0tBQ3JDO0NBQ0o7Ozs7Ozs7OztBQUVELHNCQUF1QixTQUFRLFVBQVU7Ozs7SUFFckMsWUFBWSxJQUFXO1FBQ25CLEtBQUssQ0FBQyxJQUFJLEVBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0tBQ3pDO0NBQ0o7QUFFRCxzQkFBdUIsU0FBUSxVQUFVOzs7O0lBRXJDLFlBQVksSUFBVztRQUNuQixLQUFLLENBQUMsSUFBSSxFQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztLQUNyQztDQUNKO0FBRUQsZ0NBQWlDLFNBQVEsVUFBVTs7OztJQUUvQyxZQUFZLElBQVc7UUFDbkIsS0FBSyxDQUFDLElBQUksRUFBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7S0FDekM7Q0FDSjs7O0lBSUcsS0FBTSxPQUFPO0lBQ2IsTUFBTyxNQUFNOzs7O0FBR2pCLE1BQU0sUUFBUSxHQUFHLElBQUksMEJBQTBCLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDOztBQUNyRSxNQUFNLFNBQVMsR0FBRyxJQUFJLDBCQUEwQixDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQzs7OztBQWV2RSxNQUFNOzs7Ozs7SUFFRixZQUNhLFFBQWUsRUFDZixRQUFlLEVBQ2YsU0FBb0I7UUFGcEIsYUFBUSxHQUFSLFFBQVEsQ0FBTztRQUNmLGFBQVEsR0FBUixRQUFRLENBQU87UUFDZixjQUFTLEdBQVQsU0FBUyxDQUFXO0tBRWhDO0NBQ0o7Ozs7Ozs7OztBQUdELE1BQU07Ozs7OztJQUVGLFlBQ2EsV0FBd0IsRUFDeEIsTUFBNEIsRUFDNUIsaUNBQXlDLElBQUk7UUFGN0MsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsV0FBTSxHQUFOLE1BQU0sQ0FBc0I7UUFDNUIsbUNBQThCLEdBQTlCLDhCQUE4QixDQUFlO0tBRXpEO0NBQ0o7Ozs7Ozs7Ozs7Ozs7O0FBR0QsTUFBTSxnQ0FDRixHQUFPLEVBQUMsTUFBNEI7O0lBRXBDLE1BQU0sV0FBVyxHQUFnQixFQUFFLENBQUM7O0lBR3BDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sU0FBUyxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7O1FBQzFCLE1BQU0sVUFBVSxHQUFHLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUVsQyxXQUFXLENBQUMsSUFBSSxDQUNaLE9BQU8sVUFBVSxJQUFJLFFBQVEsQ0FBQyxDQUFDO1lBQzNCLElBQUksZUFBZSxDQUNmLFNBQVMsb0JBQ1QsVUFBb0IsRUFDdkIsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxlQUFlLENBQ2YsU0FBUyxFQUNULFVBQVUsQ0FDYixDQUNSLENBQUM7S0FDTDtJQUVELE1BQU0sQ0FBQyxJQUFJLGVBQWUsQ0FBQyxXQUFXLEVBQUMsTUFBTSxDQUFDLENBQUM7Q0FDbEQ7QUFHRCxNQUFNLHVCQUF3QixTQUFRLGVBQWU7Ozs7SUFFakQsWUFBWSxVQUFxQjtRQUM3QixLQUFLLENBQUMsQ0FBQyxVQUFVLENBQUMsRUFBQyxTQUFTLENBQUMsQ0FBQztLQUNqQztDQUNKO0FBQ0QsTUFBTSx5QkFBMEIsU0FBUSxlQUFlOzs7O0lBRW5ELFlBQVksV0FBd0I7UUFDaEMsS0FBSyxDQUFDLFdBQVcsRUFBQyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztLQUNoRDtDQUNKO0FBQ0QsTUFBTSx3QkFBeUIsU0FBUSxlQUFlOzs7O0lBRWxELFlBQVksV0FBd0I7UUFDaEMsS0FBSyxDQUFDLFdBQVcsRUFBQyxxQkFBcUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUMvQztDQUNKO0FBRUQsTUFBTSx1QkFBd0IsU0FBUSxlQUFlOzs7O0lBRWpELFlBQVksV0FBd0I7UUFDaEMsS0FBSyxDQUFDLFdBQVcsRUFBQyxxQkFBcUIsQ0FBQyxLQUFLLEVBQUMsS0FBSyxDQUFDLENBQUM7S0FDeEQ7Q0FDSjs7OztBQUlELE1BQU0scUJBQStCLFNBQVEsVUFBVTs7Ozs7O0lBRW5ELFlBQ0ksU0FBZ0IsRUFDaEIsS0FBZ0IsRUFDaEIsUUFBMkI7UUFFM0IsS0FBSyxDQUFDLFNBQVMsRUFBQyxRQUFRLEVBQUMsS0FBSyxDQUFDLENBQUM7S0FDbkM7Q0FDSjtBQUdELE1BQU0sc0JBQXVCLFNBQVEsY0FBYzs7Ozs7O0lBRS9DLFlBQ0ksU0FBZ0IsRUFDaEIsSUFBVyxFQUNYLFdBQThCLGtCQUFrQixDQUFDLEtBQUs7UUFFdEQsS0FBSyxDQUNELFNBQVMsRUFDVCxJQUFJLGdCQUFnQixDQUFDLElBQUksQ0FBQyxFQUMxQixRQUFRLENBQ1gsQ0FBQztLQUNMO0NBQ0o7QUFFRCxNQUFNLHNCQUF1QixTQUFRLGNBQWM7Ozs7OztJQUUvQyxZQUNJLFNBQWdCLEVBQ2hCLElBQVcsRUFDWCxXQUE4QixrQkFBa0IsQ0FBQyxLQUFLO1FBRXRELEtBQUssQ0FDRCxTQUFTLEVBQ1QsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFDMUIsUUFBUSxDQUNYLENBQUM7S0FDTDtDQUNKO0FBRUQsTUFBTSxnQ0FBaUMsU0FBUSxjQUFjOzs7Ozs7SUFFekQsWUFDSSxTQUFnQixFQUNoQixJQUFXLEVBQ1gsV0FBOEIsa0JBQWtCLENBQUMsS0FBSztRQUV0RCxLQUFLLENBQ0QsU0FBUyxFQUNULElBQUksMEJBQTBCLENBQUMsSUFBSSxDQUFDLEVBQ3BDLFFBQVEsQ0FDWCxDQUFDO0tBQ0w7Q0FDSjtBQUdELE1BQU0sMEJBQTJCLFNBQVEsY0FBYzs7OztJQUVuRCxZQUFZLFNBQWdCO1FBQ3hCLEtBQUssQ0FBQyxTQUFTLEVBQUMsU0FBUyxFQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ3ZEO0NBQ0o7QUFFRCxNQUFNLHlCQUEwQixTQUFRLGNBQWM7Ozs7SUFFbEQsWUFBWSxTQUFnQjtRQUN4QixLQUFLLENBQUMsU0FBUyxFQUFDLFFBQVEsRUFBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUN0RDtDQUNKO0FBR0QsTUFBTSwyQkFBNEIsU0FBUSxjQUFjOzs7O0lBRXBELFlBQVksU0FBZ0I7UUFDeEIsS0FBSyxDQUFDLFNBQVMsRUFBQyxTQUFTLEVBQUMsa0JBQWtCLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDcEQ7Q0FDSjtBQUVELE1BQU0sOEJBQStCLFNBQVEsY0FBYzs7OztJQUV2RCxZQUFZLFNBQWdCO1FBQ3hCLEtBQUssQ0FBQyxTQUFTLEVBQUMsU0FBUyxFQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0tBQ3hEO0NBQ0oiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBNeVNRTFZhbHVlVHlwZVxue1xuICAgIEVYUFJFU1NJT04sXG4gICAgU1RSSU5HXG59XG5cbmV4cG9ydCBlbnVtIEV4cHJlc3Npb25PcGVyYXRvclxue1xuICAgIEVRVUFMICAgICAgICAgICAgICAgICA9ICc9JyxcbiAgICBMRVNTX1RIQU4gICAgICAgICAgICAgPSAnPCcsXG4gICAgTEVTU19USEFOX09SX0VRVUFMICAgID0gJzw9JyxcbiAgICBHUkVBVEVSX1RIQU4gICAgICAgICAgPSAnPicsXG4gICAgR1JFQVRFUl9USEFOX09SX0VRVUFMID0gJz49JyxcblxuICAgIElTICAgICAgICAgICAgICAgICAgICA9ICdJUycsXG4gICAgSVNfTk9UICAgICAgICAgICAgICAgID0gJ0lTIE5PVCcsXG59XG5cbmV4cG9ydCBlbnVtIEV4cHJlc3Npb25Hcm91cEpvaW5lclxue1xuICAgIEFORCAgID0gJ0FORCcsXG4gICAgT1IgICAgPSAnT1InLFxuICAgIENPTU1BID0gJywnXG59XG5cblxuXG5leHBvcnQgdHlwZSBFeHByZXNzaW9uVmFsdWVUeXBlID0gc3RyaW5nfG51bWJlcjtcblxuXG5cbi8qXG5pbnRlcmZhY2UgSU15U1FMVmFsdWU8VCBleHRlbmRzIEV4cHJlc3Npb25WYWx1ZVR5cGU+XG57XG4gICAgZGF0YTpULFxuICAgIC8vdHlwZTpNeVNRTFZhbHVlVHlwZVxufVxuKi9cbmV4cG9ydCBpbnRlcmZhY2UgSU15U1FMVmFsdWVcbntcbiAgICBkYXRhOkV4cHJlc3Npb25WYWx1ZVR5cGU7XG4gICAgaXNTdHJpbmc6Ym9vbGVhbjtcbn1cblxuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTFZhbHVlIGltcGxlbWVudHMgSU15U1FMVmFsdWVcbntcbiAgICByZWFkb25seSBpc1N0cmluZzpib29sZWFuO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHJlYWRvbmx5IGRhdGE6RXhwcmVzc2lvblZhbHVlVHlwZSxcbiAgICAgICAgcmVhZG9ubHkgdHlwZTpNeVNRTFZhbHVlVHlwZVxuICAgICkge1xuICAgICAgICB0aGlzLmlzU3RyaW5nID0gXG4gICAgICAgICAgICB0eXBlID09IE15U1FMVmFsdWVUeXBlLlNUUklORztcbiAgICB9XG59XG5cbmNsYXNzIE15U1FMTnVtYmVyVmFsdWUgZXh0ZW5kcyBNeVNRTFZhbHVlXG57XG4gICAgY29uc3RydWN0b3IoZGF0YTpudW1iZXIpIHtcbiAgICAgICAgc3VwZXIoZGF0YSxNeVNRTFZhbHVlVHlwZS5FWFBSRVNTSU9OKTtcbiAgICB9XG59XG5cbmNsYXNzIE15U1FMU3RyaW5nVmFsdWUgZXh0ZW5kcyBNeVNRTFZhbHVlXG57XG4gICAgY29uc3RydWN0b3IoZGF0YTpzdHJpbmcpIHtcbiAgICAgICAgc3VwZXIoZGF0YSxNeVNRTFZhbHVlVHlwZS5TVFJJTkcpO1xuICAgIH1cbn1cblxuY2xhc3MgTXlTUUxTdHJpbmdFeHByZXNzaW9uVmFsdWUgZXh0ZW5kcyBNeVNRTFZhbHVlXG57XG4gICAgY29uc3RydWN0b3IoZGF0YTpzdHJpbmcpIHtcbiAgICAgICAgc3VwZXIoZGF0YSxNeVNRTFZhbHVlVHlwZS5FWFBSRVNTSU9OKTtcbiAgICB9XG59XG5cblxuZXhwb3J0IGVudW0gTXlTUUxFeHByZXNzaW9uIHtcbiAgICBOT1cgPSAnTk9XKCknLFxuICAgIE5VTEwgPSAnTlVMTCcsXG59XG5cbmNvbnN0IG5vd1ZhbHVlID0gbmV3IE15U1FMU3RyaW5nRXhwcmVzc2lvblZhbHVlKE15U1FMRXhwcmVzc2lvbi5OT1cpO1xuY29uc3QgbnVsbFZhbHVlID0gbmV3IE15U1FMU3RyaW5nRXhwcmVzc2lvblZhbHVlKE15U1FMRXhwcmVzc2lvbi5OVUxMKTtcblxuXG4vKlxuY2xhc3MgTXlTUUxWYWx1ZVxue1xuICAgIHN0YXRpYyBib29sZWFuQXNJbnQoYm9vbDpib29sZWFuKTpudW1iZXJcbiAgICB7XG4gICAgICAgIHJldHVybiBib29sID8gMSA6IDA7XG4gICAgfVxufVxuKi9cblxuXG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBFeHByZXNzaW9uXG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHJlYWRvbmx5IGxlZnRTaWRlOnN0cmluZyxcbiAgICAgICAgcmVhZG9ubHkgb3BlcmF0b3I6c3RyaW5nLFxuICAgICAgICByZWFkb25seSByaWdodFNpZGU6TXlTUUxWYWx1ZVxuICAgICkge1xuICAgIH1cbn1cblxuXG5leHBvcnQgY2xhc3MgRXhwcmVzc2lvbkdyb3VwXG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHJlYWRvbmx5IGV4cHJlc3Npb25zOkV4cHJlc3Npb25bXSxcbiAgICAgICAgcmVhZG9ubHkgam9pbmVyOkV4cHJlc3Npb25Hcm91cEpvaW5lcixcbiAgICAgICAgcmVhZG9ubHkgc3Vycm91bmRFeHByZXNzaW9uV2l0aEJyYWNrZXRzOmJvb2xlYW4gPSB0cnVlXG4gICAgKSB7XG4gICAgfVxufVxuXG5cbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVFeHByZXNzaW9uR3JvdXAoXG4gICAgbWFwOmFueSxqb2luZXI6RXhwcmVzc2lvbkdyb3VwSm9pbmVyXG4pIHtcbiAgICBjb25zdCBleHByZXNzaW9uczpFeHByZXNzaW9uW10gPSBbXTtcblxuICAgIC8vIHNob3VsZCB3ZSB1c2UgT2JqZWN0LmtleXMoKT9cbiAgICBmb3IgKGNvbnN0IGZpZWxkTmFtZSBpbiBtYXApIHtcbiAgICAgICAgY29uc3QgZmllbGRWYWx1ZSA9IG1hcFtmaWVsZE5hbWVdO1xuXG4gICAgICAgIGV4cHJlc3Npb25zLnB1c2goXG4gICAgICAgICAgICB0eXBlb2YgZmllbGRWYWx1ZSA9PSAnc3RyaW5nJyA/XG4gICAgICAgICAgICAgICAgbmV3IFN0cmluZ1N0YXRlbWVudChcbiAgICAgICAgICAgICAgICAgICAgZmllbGROYW1lLFxuICAgICAgICAgICAgICAgICAgICBmaWVsZFZhbHVlIGFzIHN0cmluZ1xuICAgICAgICAgICAgICAgICkgOlxuICAgICAgICAgICAgICAgIG5ldyBOdW1iZXJTdGF0ZW1lbnQoXG4gICAgICAgICAgICAgICAgICAgIGZpZWxkTmFtZSxcbiAgICAgICAgICAgICAgICAgICAgZmllbGRWYWx1ZVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgKTtcbiAgICB9XG4gICAgXG4gICAgcmV0dXJuIG5ldyBFeHByZXNzaW9uR3JvdXAoZXhwcmVzc2lvbnMsam9pbmVyKTtcbn1cblxuXG5leHBvcnQgY2xhc3MgU2luZ2xlRXhwcmVzc2lvbiBleHRlbmRzIEV4cHJlc3Npb25Hcm91cFxue1xuICAgIGNvbnN0cnVjdG9yKGV4cHJlc3Npb246RXhwcmVzc2lvbikge1xuICAgICAgICBzdXBlcihbZXhwcmVzc2lvbl0sdW5kZWZpbmVkKTtcbiAgICB9XG59XG5leHBvcnQgY2xhc3MgQW5kRXhwcmVzc2lvbkdyb3VwIGV4dGVuZHMgRXhwcmVzc2lvbkdyb3VwXG57XG4gICAgY29uc3RydWN0b3IoZXhwcmVzc2lvbnM6RXhwcmVzc2lvbltdKSB7XG4gICAgICAgIHN1cGVyKGV4cHJlc3Npb25zLEV4cHJlc3Npb25Hcm91cEpvaW5lci5BTkQpO1xuICAgIH1cbn1cbmV4cG9ydCBjbGFzcyBPckV4cHJlc3Npb25Hcm91cCBleHRlbmRzIEV4cHJlc3Npb25Hcm91cFxue1xuICAgIGNvbnN0cnVjdG9yKGV4cHJlc3Npb25zOkV4cHJlc3Npb25bXSkge1xuICAgICAgICBzdXBlcihleHByZXNzaW9ucyxFeHByZXNzaW9uR3JvdXBKb2luZXIuT1IpO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIFVwZGF0ZVN0YXRlbWVudHMgZXh0ZW5kcyBFeHByZXNzaW9uR3JvdXBcbntcbiAgICBjb25zdHJ1Y3RvcihleHByZXNzaW9uczpFeHByZXNzaW9uW10pIHtcbiAgICAgICAgc3VwZXIoZXhwcmVzc2lvbnMsRXhwcmVzc2lvbkdyb3VwSm9pbmVyLkNPTU1BLGZhbHNlKTtcbiAgICB9XG59XG5cblxuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRmllbGRTdGF0ZW1lbnQgZXh0ZW5kcyBFeHByZXNzaW9uXG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIGZpZWxkTmFtZTpzdHJpbmcsXG4gICAgICAgIHZhbHVlOk15U1FMVmFsdWUsXG4gICAgICAgIG9wZXJhdG9yOkV4cHJlc3Npb25PcGVyYXRvclxuICAgICkge1xuICAgICAgICBzdXBlcihmaWVsZE5hbWUsb3BlcmF0b3IsdmFsdWUpO1xuICAgIH1cbn1cblxuXG5leHBvcnQgY2xhc3MgTnVtYmVyU3RhdGVtZW50IGV4dGVuZHMgRmllbGRTdGF0ZW1lbnRcbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgZmllbGROYW1lOnN0cmluZyxcbiAgICAgICAgZGF0YTpudW1iZXIsXG4gICAgICAgIG9wZXJhdG9yOkV4cHJlc3Npb25PcGVyYXRvciA9IEV4cHJlc3Npb25PcGVyYXRvci5FUVVBTFxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIGZpZWxkTmFtZSxcbiAgICAgICAgICAgIG5ldyBNeVNRTE51bWJlclZhbHVlKGRhdGEpLFxuICAgICAgICAgICAgb3BlcmF0b3JcbiAgICAgICAgKTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBTdHJpbmdTdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudFxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBmaWVsZE5hbWU6c3RyaW5nLFxuICAgICAgICBkYXRhOnN0cmluZyxcbiAgICAgICAgb3BlcmF0b3I6RXhwcmVzc2lvbk9wZXJhdG9yID0gRXhwcmVzc2lvbk9wZXJhdG9yLkVRVUFMXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKFxuICAgICAgICAgICAgZmllbGROYW1lLFxuICAgICAgICAgICAgbmV3IE15U1FMU3RyaW5nVmFsdWUoZGF0YSksXG4gICAgICAgICAgICBvcGVyYXRvclxuICAgICAgICApO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIFN0cmluZ0V4cHJlc3Npb25TdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudFxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBmaWVsZE5hbWU6c3RyaW5nLFxuICAgICAgICBkYXRhOnN0cmluZyxcbiAgICAgICAgb3BlcmF0b3I6RXhwcmVzc2lvbk9wZXJhdG9yID0gRXhwcmVzc2lvbk9wZXJhdG9yLkVRVUFMXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKFxuICAgICAgICAgICAgZmllbGROYW1lLFxuICAgICAgICAgICAgbmV3IE15U1FMU3RyaW5nRXhwcmVzc2lvblZhbHVlKGRhdGEpLFxuICAgICAgICAgICAgb3BlcmF0b3JcbiAgICAgICAgKTtcbiAgICB9XG59XG5cblxuZXhwb3J0IGNsYXNzIE51bGxVcGRhdGVTdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudFxue1xuICAgIGNvbnN0cnVjdG9yKGZpZWxkTmFtZTpzdHJpbmcpIHtcbiAgICAgICAgc3VwZXIoZmllbGROYW1lLG51bGxWYWx1ZSxFeHByZXNzaW9uT3BlcmF0b3IuRVFVQUwpO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIE5vd1VwZGF0ZVN0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50XG57XG4gICAgY29uc3RydWN0b3IoZmllbGROYW1lOnN0cmluZykge1xuICAgICAgICBzdXBlcihmaWVsZE5hbWUsbm93VmFsdWUsRXhwcmVzc2lvbk9wZXJhdG9yLkVRVUFMKTtcbiAgICB9XG59XG5cblxuZXhwb3J0IGNsYXNzIEZpZWxkSXNOdWxsU3RhdGVtZW50IGV4dGVuZHMgRmllbGRTdGF0ZW1lbnRcbntcbiAgICBjb25zdHJ1Y3RvcihmaWVsZE5hbWU6c3RyaW5nKSB7XG4gICAgICAgIHN1cGVyKGZpZWxkTmFtZSxudWxsVmFsdWUsRXhwcmVzc2lvbk9wZXJhdG9yLklTKTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBGaWVsZElzTm90TnVsbFN0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50XG57XG4gICAgY29uc3RydWN0b3IoZmllbGROYW1lOnN0cmluZykge1xuICAgICAgICBzdXBlcihmaWVsZE5hbWUsbnVsbFZhbHVlLEV4cHJlc3Npb25PcGVyYXRvci5JU19OT1QpO1xuICAgIH1cbn0iXX0=