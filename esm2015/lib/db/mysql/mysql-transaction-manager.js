/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { MySQLConnectionManager } from './mysql-connection-manager';
import { MySQLTransactionQuery } from './mysql-query';
export class MySQLTransactionManager {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.queue = [];
        this.isExecutingTransaction = false;
        /**
         * we need to manage a queue of transactions
         * to be executed in order
         * once the transaction either is successful or fails
         * then move on to the next
         */
        this.performNextTransaction = () => {
            if (this.isExecutingTransaction ||
                this.queue.length < 1) {
                return;
            }
            console.log('perform next transaction!');
            this.isExecutingTransaction = true;
            /** @type {?} */
            const executor = this.queue.shift();
            executor.execute();
        };
        this.onTransactionCompleted = () => {
            console.log('onTransactionCompleted');
            this.isExecutingTransaction = false;
            this.performNextTransaction();
        };
        this.connectionManager = new MySQLConnectionManager(config);
    }
    /**
     * @param {?} queries
     * @return {?}
     */
    performTransaction(queries) {
        /** @type {?} */
        const executor = new MySQLTransactionExecuter(queries, this.performNextTransaction, this.onTransactionCompleted, this.connectionManager);
        // add executor to queue
        this.queue.push(executor);
        return new Promise(executor.init);
    }
}
if (false) {
    /** @type {?} */
    MySQLTransactionManager.prototype.queue;
    /** @type {?} */
    MySQLTransactionManager.prototype.isExecutingTransaction;
    /** @type {?} */
    MySQLTransactionManager.prototype.connectionManager;
    /**
     * we need to manage a queue of transactions
     * to be executed in order
     * once the transaction either is successful or fails
     * then move on to the next
     * @type {?}
     */
    MySQLTransactionManager.prototype.performNextTransaction;
    /** @type {?} */
    MySQLTransactionManager.prototype.onTransactionCompleted;
}
/** @typedef {?} */
var ResolveFunction;
class MySQLTransactionExecuter {
    /**
     * @param {?} queries
     * @param {?} onInitHandler
     * @param {?} onCompleteHandler
     * @param {?} connectionManager
     */
    constructor(queries, onInitHandler, onCompleteHandler, connectionManager) {
        this.queries = queries;
        this.onInitHandler = onInitHandler;
        this.onCompleteHandler = onCompleteHandler;
        this.connectionManager = connectionManager;
        this.init = (resolve, reject) => {
            this.resolve = resolve;
            this.reject = reject;
            this.onInitHandler();
        };
    }
    /**
     * @return {?}
     */
    execute() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.connection = yield this.connectionManager.getConnection();
            this.connection.beginTransaction(() => tslib_1.__awaiter(this, void 0, void 0, function* () {
                try {
                    /** @type {?} */
                    const results = [];
                    for (const queryString of this.queries) {
                        /** @type {?} */
                        const query = new MySQLTransactionQuery(queryString, this.connectionManager);
                        /** @type {?} */
                        const result = yield query.execute();
                        results.push(result);
                    }
                    yield this.commit();
                    this.resolve(results);
                }
                catch (e) {
                    yield this.rollback();
                    this.reject(e);
                }
                this.onCompleteHandler();
            }));
        });
    }
    /**
     * @return {?}
     */
    commit() {
        return new Promise((resolve, reject) => {
            this.connection.commit((error) => tslib_1.__awaiter(this, void 0, void 0, function* () {
                if (error) {
                    return reject(error);
                }
                resolve();
            }));
        });
    }
    /**
     * @return {?}
     */
    rollback() {
        return new Promise((resolve) => {
            this.connection.rollback(resolve);
        });
    }
}
if (false) {
    /** @type {?} */
    MySQLTransactionExecuter.prototype.resolve;
    /** @type {?} */
    MySQLTransactionExecuter.prototype.reject;
    /** @type {?} */
    MySQLTransactionExecuter.prototype.connection;
    /** @type {?} */
    MySQLTransactionExecuter.prototype.init;
    /** @type {?} */
    MySQLTransactionExecuter.prototype.queries;
    /** @type {?} */
    MySQLTransactionExecuter.prototype.onInitHandler;
    /** @type {?} */
    MySQLTransactionExecuter.prototype.onCompleteHandler;
    /** @type {?} */
    MySQLTransactionExecuter.prototype.connectionManager;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtdHJhbnNhY3Rpb24tbWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL215c3FsL215c3FsLXRyYW5zYWN0aW9uLW1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFHQSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHdEQsTUFBTTs7OztJQU9GLFlBQVksTUFBbUI7cUJBTFksRUFBRTtzQ0FDWixLQUFLOzs7Ozs7O3NDQStCTCxHQUFHLEVBQUU7WUFDbEMsRUFBRSxDQUFDLENBQ0MsSUFBSSxDQUFDLHNCQUFzQjtnQkFDM0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FDeEIsQ0FBQyxDQUFDLENBQUM7Z0JBQ0MsTUFBTSxDQUFDO2FBQ1Y7WUFFRCxPQUFPLENBQUMsR0FBRyxDQUFDLDJCQUEyQixDQUFDLENBQUM7WUFFekMsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQzs7WUFHbkMsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUVwQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDdEI7c0NBRWdDLEdBQUcsRUFBRTtZQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFFdEMsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztZQUVwQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztTQUNqQztRQWxERyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztLQUMvRDs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxPQUFnQjs7UUFLL0IsTUFBTSxRQUFRLEdBQUcsSUFBSSx3QkFBd0IsQ0FDekMsT0FBTyxFQUNQLElBQUksQ0FBQyxzQkFBc0IsRUFDM0IsSUFBSSxDQUFDLHNCQUFzQixFQUMzQixJQUFJLENBQUMsaUJBQWlCLENBQ3pCLENBQUM7O1FBR0YsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFMUIsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFzQixRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDMUQ7Q0FnQ0o7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUlEOzs7Ozs7O0lBTUksWUFDWSxTQUNBLGVBQ0EsbUJBQ0E7UUFIQSxZQUFPLEdBQVAsT0FBTztRQUNQLGtCQUFhLEdBQWIsYUFBYTtRQUNiLHNCQUFpQixHQUFqQixpQkFBaUI7UUFDakIsc0JBQWlCLEdBQWpCLGlCQUFpQjtvQkFJdEIsQ0FBQyxPQUF1QixFQUFDLE1BQWUsRUFBRSxFQUFFO1lBQy9DLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1lBRXJCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztTQUN4QjtLQVBBOzs7O0lBU0ssT0FBTzs7WUFDVCxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxDQUFDO1lBRS9ELElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsR0FBUyxFQUFFO2dCQUN4QyxJQUFJLENBQUM7O29CQUNELE1BQU0sT0FBTyxHQUF1QixFQUFFLENBQUM7b0JBRXZDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sV0FBVyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOzt3QkFDckMsTUFBTSxLQUFLLEdBQUcsSUFBSSxxQkFBcUIsQ0FDbkMsV0FBVyxFQUNYLElBQUksQ0FBQyxpQkFBaUIsQ0FDekIsQ0FBQzs7d0JBRUYsTUFBTSxNQUFNLEdBQUcsTUFBTSxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBRXJDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7cUJBQ3hCO29CQUVELE1BQU0sSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO29CQUVwQixJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUN6QjtnQkFBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDVCxNQUFNLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFFdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDbEI7Z0JBRUQsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7Y0FDNUIsQ0FBQyxDQUFDOztLQUNOOzs7O0lBRU8sTUFBTTtRQUNWLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFPLEtBQUssRUFBRSxFQUFFO2dCQUNuQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNSLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3hCO2dCQUVELE9BQU8sRUFBRSxDQUFDO2NBQ2IsQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDOzs7OztJQUdDLFFBQVE7UUFDWixNQUFNLENBQUMsSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUNyQyxDQUFDLENBQUM7O0NBRVYiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb25uZWN0aW9uIH0gZnJvbSBcIm15c3FsXCI7XG5cbmltcG9ydCB7IElNeVNRTENvbmZpZywgSU15U1FMUXVlcnlSZXN1bHQgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgTXlTUUxDb25uZWN0aW9uTWFuYWdlciB9IGZyb20gJy4vbXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyJztcbmltcG9ydCB7IE15U1FMVHJhbnNhY3Rpb25RdWVyeSB9IGZyb20gJy4vbXlzcWwtcXVlcnknO1xuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTFRyYW5zYWN0aW9uTWFuYWdlclxue1xuICAgIHByaXZhdGUgcXVldWU6TXlTUUxUcmFuc2FjdGlvbkV4ZWN1dGVyW10gPSBbXTtcbiAgICBwcml2YXRlIGlzRXhlY3V0aW5nVHJhbnNhY3Rpb24gPSBmYWxzZTtcblxuICAgIHByaXZhdGUgcmVhZG9ubHkgY29ubmVjdGlvbk1hbmFnZXI6TXlTUUxDb25uZWN0aW9uTWFuYWdlcjtcblxuICAgIGNvbnN0cnVjdG9yKGNvbmZpZzpJTXlTUUxDb25maWcpIHtcbiAgICAgICAgdGhpcy5jb25uZWN0aW9uTWFuYWdlciA9IG5ldyBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyKGNvbmZpZyk7XG4gICAgfVxuXG4gICAgcGVyZm9ybVRyYW5zYWN0aW9uKHF1ZXJpZXM6c3RyaW5nW10pIHtcbiAgICAgICAgLypcbiAgICAgICAgd2UgbmVlZCB0byBwcm9taXNlIHRoZSByZXN1bHQgb2YgZWFjaCBxdWVyeSBleGVjdXRlZFxuICAgICAgICBzbyB0aGF0IHRoZXkgY2FuIGJlIGluc3BlY3RlZCBieSBjb25zdW1lciBmb3IgSWRzLCBldGNcbiAgICAgICAgKi9cbiAgICAgICAgY29uc3QgZXhlY3V0b3IgPSBuZXcgTXlTUUxUcmFuc2FjdGlvbkV4ZWN1dGVyKFxuICAgICAgICAgICAgcXVlcmllcyxcbiAgICAgICAgICAgIHRoaXMucGVyZm9ybU5leHRUcmFuc2FjdGlvbixcbiAgICAgICAgICAgIHRoaXMub25UcmFuc2FjdGlvbkNvbXBsZXRlZCxcbiAgICAgICAgICAgIHRoaXMuY29ubmVjdGlvbk1hbmFnZXJcbiAgICAgICAgKTtcblxuICAgICAgICAvLyBhZGQgZXhlY3V0b3IgdG8gcXVldWVcbiAgICAgICAgdGhpcy5xdWV1ZS5wdXNoKGV4ZWN1dG9yKTtcblxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2U8SU15U1FMUXVlcnlSZXN1bHRbXT4oZXhlY3V0b3IuaW5pdCk7XG4gICAgfVxuXG4gICAgLyoqKiB3ZSBuZWVkIHRvIG1hbmFnZSBhIHF1ZXVlIG9mIHRyYW5zYWN0aW9uc1xuICAgICAqIHRvIGJlIGV4ZWN1dGVkIGluIG9yZGVyXG4gICAgICogb25jZSB0aGUgdHJhbnNhY3Rpb24gZWl0aGVyIGlzIHN1Y2Nlc3NmdWwgb3IgZmFpbHNcbiAgICAgKiB0aGVuIG1vdmUgb24gdG8gdGhlIG5leHRcbiAgICAgKi9cbiAgICBwcml2YXRlIHBlcmZvcm1OZXh0VHJhbnNhY3Rpb24gPSAoKSA9PiB7XG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIHRoaXMuaXNFeGVjdXRpbmdUcmFuc2FjdGlvbiB8fFxuICAgICAgICAgICAgdGhpcy5xdWV1ZS5sZW5ndGggPCAxXG4gICAgICAgICkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc29sZS5sb2coJ3BlcmZvcm0gbmV4dCB0cmFuc2FjdGlvbiEnKTtcblxuICAgICAgICB0aGlzLmlzRXhlY3V0aW5nVHJhbnNhY3Rpb24gPSB0cnVlO1xuXG4gICAgICAgIC8vIHJlbW92ZSBmaXJzdCBlbGVtZW50IGZyb20gcXVldWUgYW5kIHJldHVybiBpdFxuICAgICAgICBjb25zdCBleGVjdXRvciA9IHRoaXMucXVldWUuc2hpZnQoKTtcblxuICAgICAgICBleGVjdXRvci5leGVjdXRlKCk7XG4gICAgfTtcblxuICAgIHByaXZhdGUgb25UcmFuc2FjdGlvbkNvbXBsZXRlZCA9ICgpID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coJ29uVHJhbnNhY3Rpb25Db21wbGV0ZWQnKTtcblxuICAgICAgICB0aGlzLmlzRXhlY3V0aW5nVHJhbnNhY3Rpb24gPSBmYWxzZTtcblxuICAgICAgICB0aGlzLnBlcmZvcm1OZXh0VHJhbnNhY3Rpb24oKTtcbiAgICB9XG59XG5cbnR5cGUgUmVzb2x2ZUZ1bmN0aW9uID0gKHJlc3VsdHM6SU15U1FMUXVlcnlSZXN1bHRbXSkgPT4gdm9pZDtcblxuY2xhc3MgTXlTUUxUcmFuc2FjdGlvbkV4ZWN1dGVyXG57XG4gICAgcHJpdmF0ZSByZXNvbHZlOlJlc29sdmVGdW5jdGlvbjtcbiAgICBwcml2YXRlIHJlamVjdDpGdW5jdGlvbjtcbiAgICBwcml2YXRlIGNvbm5lY3Rpb246Q29ubmVjdGlvbjtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIHF1ZXJpZXM6c3RyaW5nW10sXG4gICAgICAgIHByaXZhdGUgb25Jbml0SGFuZGxlcjpGdW5jdGlvbixcbiAgICAgICAgcHJpdmF0ZSBvbkNvbXBsZXRlSGFuZGxlcjpGdW5jdGlvbixcbiAgICAgICAgcHJpdmF0ZSBjb25uZWN0aW9uTWFuYWdlcjpNeVNRTENvbm5lY3Rpb25NYW5hZ2VyXG4gICAgKSB7XG4gICAgfVxuXG4gICAgaW5pdCA9IChyZXNvbHZlOlJlc29sdmVGdW5jdGlvbixyZWplY3Q6RnVuY3Rpb24pID0+IHtcbiAgICAgICAgdGhpcy5yZXNvbHZlID0gcmVzb2x2ZTtcbiAgICAgICAgdGhpcy5yZWplY3QgPSByZWplY3Q7XG5cbiAgICAgICAgdGhpcy5vbkluaXRIYW5kbGVyKCk7XG4gICAgfVxuXG4gICAgYXN5bmMgZXhlY3V0ZSgpIHtcbiAgICAgICAgdGhpcy5jb25uZWN0aW9uID0gYXdhaXQgdGhpcy5jb25uZWN0aW9uTWFuYWdlci5nZXRDb25uZWN0aW9uKCk7XG5cbiAgICAgICAgdGhpcy5jb25uZWN0aW9uLmJlZ2luVHJhbnNhY3Rpb24oYXN5bmMgKCkgPT4ge1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBjb25zdCByZXN1bHRzOklNeVNRTFF1ZXJ5UmVzdWx0W10gPSBbXTtcblxuICAgICAgICAgICAgICAgIGZvciAoY29uc3QgcXVlcnlTdHJpbmcgb2YgdGhpcy5xdWVyaWVzKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHF1ZXJ5ID0gbmV3IE15U1FMVHJhbnNhY3Rpb25RdWVyeShcbiAgICAgICAgICAgICAgICAgICAgICAgIHF1ZXJ5U3RyaW5nLFxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uTWFuYWdlclxuICAgICAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IHF1ZXJ5LmV4ZWN1dGUoKTtcblxuICAgICAgICAgICAgICAgICAgICByZXN1bHRzLnB1c2gocmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmNvbW1pdCgpO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5yZXNvbHZlKHJlc3VsdHMpO1xuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMucm9sbGJhY2soKTtcblxuICAgICAgICAgICAgICAgIHRoaXMucmVqZWN0KGUpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLm9uQ29tcGxldGVIYW5kbGVyKCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgY29tbWl0KCkge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uLmNvbW1pdChhc3luYyAoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlamVjdChlcnJvcik7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgcm9sbGJhY2soKSB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uLnJvbGxiYWNrKHJlc29sdmUpO1xuICAgICAgICB9KTtcbiAgICB9XG59Il19