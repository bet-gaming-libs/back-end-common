/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
export class MySQLExpression {
    /**
     * @param {?} bool
     * @return {?}
     */
    static booleanAsInt(bool) {
        return bool ? 1 : 0;
    }
}
MySQLExpression.NOW = 'NOW()';
MySQLExpression.NULL = 'NULL';
if (false) {
    /** @type {?} */
    MySQLExpression.NOW;
    /** @type {?} */
    MySQLExpression.NULL;
}
export class MySQLValueType {
}
MySQLValueType.EXPRESSION = 0;
MySQLValueType.STRING = 1;
if (false) {
    /** @type {?} */
    MySQLValueType.EXPRESSION;
    /** @type {?} */
    MySQLValueType.STRING;
}
// unsupported: template constraints.
/**
 * @template T
 */
export class UpdateStatement {
    /**
     * @param {?} fieldName
     * @param {?} type
     * @param {?} data
     */
    constructor(fieldName, type, data) {
        this.fieldName = fieldName;
        this.type = type;
        this.data = data;
    }
    /**
     * @return {?}
     */
    get value() {
        return {
            type: this.type,
            data: this.data
        };
    }
}
if (false) {
    /** @type {?} */
    UpdateStatement.prototype.fieldName;
    /** @type {?} */
    UpdateStatement.prototype.type;
    /** @type {?} */
    UpdateStatement.prototype.data;
}
export class StringUpdateStatement extends UpdateStatement {
    /**
     * @param {?} fieldName
     * @param {?} fieldValue
     */
    constructor(fieldName, fieldValue) {
        super(fieldName, MySQLValueType.STRING, fieldValue);
    }
}
// unsupported: template constraints.
/**
 * @template T
 */
export class ExpressionUpdateStatement extends UpdateStatement {
    /**
     * @param {?} fieldName
     * @param {?} fieldValue
     */
    constructor(fieldName, fieldValue) {
        super(fieldName, MySQLValueType.EXPRESSION, fieldValue);
    }
}
export class NumberUpdateStatement extends ExpressionUpdateStatement {
}
export class CalculationUpdateStatement extends ExpressionUpdateStatement {
}
export class NowUpdateStatement extends CalculationUpdateStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, MySQLExpression.NOW);
    }
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtdXRpbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL215c3FsL215c3FsLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUdBLE1BQU07Ozs7O0lBS0YsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFZO1FBRTVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQ3ZCOztzQkFOWSxPQUFPO3VCQUNOLE1BQU07Ozs7Ozs7QUFReEIsTUFBTTs7NEJBRWtCLENBQUM7d0JBQ0wsQ0FBQzs7Ozs7Ozs7Ozs7QUFHckIsTUFBTTs7Ozs7O0lBRUYsWUFDYSxTQUFnQixFQUNqQixNQUNBO1FBRkMsY0FBUyxHQUFULFNBQVMsQ0FBTztRQUNqQixTQUFJLEdBQUosSUFBSTtRQUNKLFNBQUksR0FBSixJQUFJO0tBR2Y7Ozs7SUFFRCxJQUFJLEtBQUs7UUFDTCxNQUFNLENBQUM7WUFDSCxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7WUFDZixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7U0FDbEIsQ0FBQTtLQUNKO0NBQ0o7Ozs7Ozs7OztBQUVELE1BQU0sNEJBQTZCLFNBQVEsZUFBdUI7Ozs7O0lBRTlELFlBQVksU0FBZ0IsRUFBQyxVQUFpQjtRQUMxQyxLQUFLLENBQUMsU0FBUyxFQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUMsVUFBVSxDQUFDLENBQUM7S0FDckQ7Q0FDSjs7Ozs7QUFFRCxNQUFNLGdDQUEwRCxTQUFRLGVBQWtCOzs7OztJQUV0RixZQUFZLFNBQWdCLEVBQUMsVUFBWTtRQUNyQyxLQUFLLENBQUMsU0FBUyxFQUFDLGNBQWMsQ0FBQyxVQUFVLEVBQUMsVUFBVSxDQUFDLENBQUM7S0FDekQ7Q0FDSjtBQUVELE1BQU0sNEJBQTZCLFNBQVEseUJBQWlDO0NBRTNFO0FBRUQsTUFBTSxpQ0FBa0MsU0FBUSx5QkFBaUM7Q0FFaEY7QUFFRCxNQUFNLHlCQUEwQixTQUFRLDBCQUEwQjs7OztJQUU5RCxZQUFZLFNBQWdCO1FBQ3hCLEtBQUssQ0FBQyxTQUFTLEVBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0tBQ3hDO0NBQ0oiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJVXBkYXRlU3RhdGVtZW50RGF0YSB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5cblxuZXhwb3J0IGNsYXNzIE15U1FMRXhwcmVzc2lvblxue1xuICAgIHN0YXRpYyBOT1cgPSAnTk9XKCknO1xuICAgIHN0YXRpYyBOVUxMID0gJ05VTEwnO1xuXG4gICAgc3RhdGljIGJvb2xlYW5Bc0ludChib29sOmJvb2xlYW4pOm51bWJlclxuICAgIHtcbiAgICAgICAgcmV0dXJuIGJvb2wgPyAxIDogMDtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBNeVNRTFZhbHVlVHlwZVxue1xuICAgIHN0YXRpYyBFWFBSRVNTSU9OID0gMDtcbiAgICBzdGF0aWMgU1RSSU5HID0gMTtcbn1cblxuZXhwb3J0IGNsYXNzIFVwZGF0ZVN0YXRlbWVudDxUIGV4dGVuZHMgc3RyaW5nfG51bWJlcj4gaW1wbGVtZW50cyBJVXBkYXRlU3RhdGVtZW50RGF0YVxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICByZWFkb25seSBmaWVsZE5hbWU6c3RyaW5nLFxuICAgICAgICBwcml2YXRlIHR5cGU6bnVtYmVyLFxuICAgICAgICBwcml2YXRlIGRhdGE6VFxuICAgICkge1xuXG4gICAgfVxuXG4gICAgZ2V0IHZhbHVlKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgdHlwZTogdGhpcy50eXBlLFxuICAgICAgICAgICAgZGF0YTogdGhpcy5kYXRhXG4gICAgICAgIH1cbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBTdHJpbmdVcGRhdGVTdGF0ZW1lbnQgZXh0ZW5kcyBVcGRhdGVTdGF0ZW1lbnQ8c3RyaW5nPlxue1xuICAgIGNvbnN0cnVjdG9yKGZpZWxkTmFtZTpzdHJpbmcsZmllbGRWYWx1ZTpzdHJpbmcpIHtcbiAgICAgICAgc3VwZXIoZmllbGROYW1lLE15U1FMVmFsdWVUeXBlLlNUUklORyxmaWVsZFZhbHVlKTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBFeHByZXNzaW9uVXBkYXRlU3RhdGVtZW50PFQgZXh0ZW5kcyBzdHJpbmd8bnVtYmVyPiBleHRlbmRzIFVwZGF0ZVN0YXRlbWVudDxUPlxue1xuICAgIGNvbnN0cnVjdG9yKGZpZWxkTmFtZTpzdHJpbmcsZmllbGRWYWx1ZTpUKSB7XG4gICAgICAgIHN1cGVyKGZpZWxkTmFtZSxNeVNRTFZhbHVlVHlwZS5FWFBSRVNTSU9OLGZpZWxkVmFsdWUpO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIE51bWJlclVwZGF0ZVN0YXRlbWVudCBleHRlbmRzIEV4cHJlc3Npb25VcGRhdGVTdGF0ZW1lbnQ8bnVtYmVyPlxue1xufVxuXG5leHBvcnQgY2xhc3MgQ2FsY3VsYXRpb25VcGRhdGVTdGF0ZW1lbnQgZXh0ZW5kcyBFeHByZXNzaW9uVXBkYXRlU3RhdGVtZW50PHN0cmluZz5cbntcbn1cblxuZXhwb3J0IGNsYXNzIE5vd1VwZGF0ZVN0YXRlbWVudCBleHRlbmRzIENhbGN1bGF0aW9uVXBkYXRlU3RhdGVtZW50XG57XG4gICAgY29uc3RydWN0b3IoZmllbGROYW1lOnN0cmluZykge1xuICAgICAgICBzdXBlcihmaWVsZE5hbWUsTXlTUUxFeHByZXNzaW9uLk5PVyk7XG4gICAgfVxufSJdfQ==