/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-settings.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IEosConfig() { }
if (false) {
    /** @type {?} */
    IEosConfig.prototype.eosjs;
    /** @type {?} */
    IEosConfig.prototype.endpoints;
    /** @type {?|undefined} */
    IEosConfig.prototype.messageSigningPrivateKey;
}
/**
 * @record
 */
export function IEosAccountNamesBase() { }
if (false) {
    /** @type {?} */
    IEosAccountNamesBase.prototype.easyAccount;
    /** @type {?} */
    IEosAccountNamesBase.prototype.withdrawal;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLXNldHRpbmdzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvZW9zL2Vvcy1zZXR0aW5ncy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLGdDQVNDOzs7SUFSQSwyQkFBVzs7SUFFWCwrQkFHRTs7SUFFRiw4Q0FBa0M7Ozs7O0FBR25DLDBDQXlCQzs7O0lBRkEsMkNBQW9COztJQUNwQiwwQ0FBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIElFb3NDb25maWcge1xuXHRlb3NqczogYW55O1xuXG5cdGVuZHBvaW50czoge1xuXHRcdHBvbGxlcjogc3RyaW5nO1xuXHRcdHRyYW5zYWN0b3I6IHN0cmluZztcblx0fTtcblxuXHRtZXNzYWdlU2lnbmluZ1ByaXZhdGVLZXk/OiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUVvc0FjY291bnROYW1lc0Jhc2Uge1xuXHQvKlxuICAgIGRpY2U6c3RyaW5nO1xuICAgIGNyYXNoOnN0cmluZztcbiAgICBiYWNjYXJhdDpzdHJpbmc7XG4gICAgaGlsbzpzdHJpbmc7XG4gICAgYmxhY2tqYWNrOnN0cmluZztcblxuICAgIGJhbmtyb2xsOnN0cmluZztcbiAgICBqYWNrcG90OnN0cmluZztcbiAgICBib251czpzdHJpbmc7XG5cbiAgICBzdGFraW5nOnN0cmluZztcbiAgICBkaXZpZGVuZHM6c3RyaW5nO1xuICAgIGVhcm5iZXRUb2tlbnNDb250cm9sbGVyOnN0cmluZztcblxuICAgIHRva2Vuczoge1xuICAgICAgICBzeXN0ZW06c3RyaW5nO1xuICAgICAgICBlYXJuYmV0OnN0cmluZztcbiAgICAgICAgQkVUOnN0cmluZztcbiAgICB9XG4gICAgKi9cblxuXHRlYXN5QWNjb3VudDogc3RyaW5nO1xuXHR3aXRoZHJhd2FsOiBzdHJpbmc7XG59XG4iXX0=