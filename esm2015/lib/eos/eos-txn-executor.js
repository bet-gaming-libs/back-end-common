/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-txn-executor.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { sleep, debugMessage } from 'earnbet-common';
/** @type {?} */
const _1HoursInSeconds = 60 * 60;
export class EosTransactionExecutorBase {
    /**
     * @param {?} actions
     * @param {?} eos
     * @param {?} txnChecker
     * @param {?=} retryOnError
     * @param {?=} secondsBeforeRetry
     * @param {?=} expireInSeconds
     */
    constructor(actions, eos, txnChecker, retryOnError = true, secondsBeforeRetry = 10, expireInSeconds = _1HoursInSeconds) {
        this.actions = actions;
        this.eos = eos;
        this.txnChecker = txnChecker;
        this.retryOnError = retryOnError;
        this.secondsBeforeRetry = secondsBeforeRetry;
        this.expireInSeconds = expireInSeconds;
        this.numOfRetries = 0;
        this.signedTransaction = undefined;
        // include first action for now
        // include other actions as well?
        const { account, name } = this.actions[0];
        this.actionId = account + ' - ' + name;
    }
    /**
     * @return {?}
     */
    execute() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            debugMessage('Attempting to Execute Transaction: ' + this.actionId);
            console.log(this.actions);
            try {
                yield this.sign();
                // attempt to broadcast
                this.broadcastedTxnId = yield broadcastSignedTransaction(this.eos, this.signedTransaction.transaction);
                this.broadcastedTime = Date.now();
                debugMessage(this.actionId + ' Broadcasted: ' + this.broadcastedTxnId);
                return this.broadcastedTxnId;
            }
            catch (error) {
                return this.onBroadcastError(error);
            }
        });
    }
    /**
     * @return {?}
     */
    sign() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                // sign transaction if not already signed
                if (this.signedTransaction == undefined) {
                    this.signedTransaction = yield signTransaction(this.eos, this.actions, this.expireInSeconds);
                }
                debugMessage('Transaction Signed: ' + this.signedTransaction.transaction_id);
                return this.signedTransaction;
            }
            catch (error) {
                console.error(error);
                yield sleep(1000);
                return this.sign();
            }
        });
    }
    /**
     * @protected
     * @param {?} error
     * @return {?}
     */
    onBroadcastError(error) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            console.error(error);
            /** @type {?} */
            const isDuplicateTxn = isDuplicateTxnError(error);
            // if duplicate error, then stop broadcasting process
            // return originally broadcasted txnId
            if (isDuplicateTxn) {
                return this.broadcastedTxnId;
            }
            if (this.retryOnError) {
                /** @type {?} */
                const maxNumOfRetries = (_1HoursInSeconds / this.secondsBeforeRetry) - 1;
                if (this.numOfRetries++ < maxNumOfRetries) {
                    // wait and then retry
                    yield sleep(1000 * this.secondsBeforeRetry);
                    return this.execute();
                }
            }
            throw error;
        });
    }
    /**
     * @return {?}
     */
    confirm() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!this.broadcastedTxnId) {
                throw new Error('Transaction has not yet been succesfully broadcasted!');
            }
            if (!this.txnChecker) {
                throw new Error('no Transaction Checker function defined!');
            }
            // wait 5 minutes before checking irreversibility
            /** @type {?} */
            const timeToWait = 5 * 60 * 1000;
            /** @type {?} */
            const remainingTimeToWait = timeToWait -
                (Date.now() - this.broadcastedTime);
            console.log({ remainingTimeToWait });
            if (remainingTimeToWait > 0) {
                yield sleep(remainingTimeToWait);
            }
            /** @type {?} */
            const txnId = this.broadcastedTxnId;
            // confirm irreversibility
            try {
                /** @type {?} */
                const isConfirmed = yield this.txnChecker(txnId);
                if (!isConfirmed) {
                    debugMessage(this.actionId + ' Txn: ' + txnId + ' is NOT CONFIRMED!');
                    return this.execute();
                }
                debugMessage(this.actionId + ' Txn: ' + txnId + ' is IRREVERSIBLE!');
                return txnId;
            }
            catch (error) {
                debugMessage('Cound Not Confirm Txn: ' + txnId);
                console.error(error);
                // wait
                yield sleep(5 * 1000);
                return this.execute();
            }
        });
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.numOfRetries;
    /**
     * @type {?}
     * @protected
     */
    EosTransactionExecutorBase.prototype.actionId;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.signedTransaction;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.broadcastedTxnId;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.broadcastedTime;
    /** @type {?} */
    EosTransactionExecutorBase.prototype.actions;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.eos;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.txnChecker;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.retryOnError;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.secondsBeforeRetry;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.expireInSeconds;
}
/**
 * @template T
 */
export class EosTransactionExecutor extends EosTransactionExecutorBase {
    /**
     * @param {?} actions
     * @param {?} eosUtility
     * @param {?} txnChecker
     * @param {?=} retryOnError
     * @param {?=} secondsBeforeRetry
     */
    constructor(actions, eosUtility, txnChecker, retryOnError = true, secondsBeforeRetry = 10) {
        super(actions.map((/**
         * @param {?} action
         * @return {?}
         */
        (action) => (Object.assign({}, action, { authorization: eosUtility.authorization })))), eosUtility.transactor, txnChecker, retryOnError, secondsBeforeRetry);
    }
}
/*
Error: {"code":409,"message":"Conflict","error":{"code":3040008,"name":"tx_duplicate","what":"Duplicate transaction","details":[{"message":"duplicate transaction 7be2845618fa965d89ff8f3b368ed1c768dab63303c38c7d8a5e9f031d7a23c6","file":"producer_plugin.cpp","line_number":527,"method":"process_incoming_transaction_async"}]}}
*/
/**
 * @param {?} error
 * @return {?}
 */
function isDuplicateTxnError(error) {
    /** @type {?} */
    const searchStrings = [
        'tx_duplicate',
        'Duplicate transaction',
        'duplicate transaction'
    ];
    /** @type {?} */
    const errorString = String(error);
    for (const searchString of searchStrings) {
        if (errorString.indexOf(searchString) > -1) {
            return true;
        }
    }
    return false;
}
/**
 * @record
 */
function ISignedTransaction() { }
if (false) {
    /** @type {?} */
    ISignedTransaction.prototype.compression;
    /** @type {?} */
    ISignedTransaction.prototype.transaction;
    /** @type {?} */
    ISignedTransaction.prototype.signatures;
}
/**
 * @record
 */
function ISignedTransactionResult() { }
if (false) {
    /** @type {?} */
    ISignedTransactionResult.prototype.transaction_id;
    /** @type {?} */
    ISignedTransactionResult.prototype.transaction;
}
/**
 * @record
 */
function IBroadcastedTransactionResult() { }
if (false) {
    /** @type {?} */
    IBroadcastedTransactionResult.prototype.transaction_id;
}
/**
 * @param {?} transactor
 * @param {?} actions
 * @param {?} expireInSeconds
 * @return {?}
 * @this {*}
 */
function signTransaction(transactor, actions, expireInSeconds) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const result = yield transactor.transaction({
            actions
        }, {
            broadcast: false,
            sign: true,
            expireInSeconds
        });
        return result;
    });
}
/**
 * @param {?} transactor
 * @param {?} transaction
 * @return {?}
 * @this {*}
 */
function broadcastSignedTransaction(transactor, transaction) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const result = yield transactor.pushTransaction(transaction);
        return result.transaction_id;
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLXR4bi1leGVjdXRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2Vvcy9lb3MtdHhuLWV4ZWN1dG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBc0MsS0FBSyxFQUFFLFlBQVksRUFBd0IsTUFBTSxnQkFBZ0IsQ0FBQzs7TUFPekcsZ0JBQWdCLEdBQUcsRUFBRSxHQUFHLEVBQUU7QUFHaEMsTUFBTSxPQUFPLDBCQUEwQjs7Ozs7Ozs7O0lBU3RDLFlBQ1UsT0FBZ0MsRUFDeEIsR0FBRyxFQUNILFVBQStCLEVBQy9CLGVBQXdCLElBQUksRUFDNUIscUJBQTZCLEVBQUUsRUFDL0Isa0JBQTBCLGdCQUFnQjtRQUxsRCxZQUFPLEdBQVAsT0FBTyxDQUF5QjtRQUN4QixRQUFHLEdBQUgsR0FBRyxDQUFBO1FBQ0gsZUFBVSxHQUFWLFVBQVUsQ0FBcUI7UUFDL0IsaUJBQVksR0FBWixZQUFZLENBQWdCO1FBQzVCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBYTtRQUMvQixvQkFBZSxHQUFmLGVBQWUsQ0FBMkI7UUFkcEQsaUJBQVksR0FBRyxDQUFDLENBQUM7UUFJakIsc0JBQWlCLEdBQTZCLFNBQVMsQ0FBQzs7O2NBY3pELEVBQUMsT0FBTyxFQUFFLElBQUksRUFBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBRXZDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxHQUFHLEtBQUssR0FBRyxJQUFJLENBQUM7SUFDeEMsQ0FBQzs7OztJQUlLLE9BQU87O1lBQ1osWUFBWSxDQUNYLHFDQUFxQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQ3JELENBQUM7WUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUcxQixJQUFJO2dCQUNILE1BQU0sSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUVsQix1QkFBdUI7Z0JBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLDBCQUEwQixDQUN2RCxJQUFJLENBQUMsR0FBRyxFQUNSLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQ2xDLENBQUM7Z0JBRUYsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBRWxDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUV2RSxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQzthQUM3QjtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNmLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3BDO1FBQ0YsQ0FBQztLQUFBOzs7O0lBRUssSUFBSTs7WUFDVCxJQUFJO2dCQUNILHlDQUF5QztnQkFDekMsSUFBSSxJQUFJLENBQUMsaUJBQWlCLElBQUksU0FBUyxFQUFFO29CQUN4QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsTUFBTSxlQUFlLENBQzdDLElBQUksQ0FBQyxHQUFHLEVBQ1IsSUFBSSxDQUFDLE9BQU8sRUFDWixJQUFJLENBQUMsZUFBZSxDQUNwQixDQUFDO2lCQUNGO2dCQUVELFlBQVksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBRTdFLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDO2FBQzlCO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ2YsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFFckIsTUFBTSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRWxCLE9BQU8sSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2FBQ25CO1FBQ0YsQ0FBQztLQUFBOzs7Ozs7SUFFZSxnQkFBZ0IsQ0FBQyxLQUFLOztZQUNyQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDOztrQkFFZixjQUFjLEdBQUcsbUJBQW1CLENBQUMsS0FBSyxDQUFDO1lBRWpELHFEQUFxRDtZQUNyRCxzQ0FBc0M7WUFDdEMsSUFBSSxjQUFjLEVBQUU7Z0JBQ25CLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO2FBQzdCO1lBR0QsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFOztzQkFDaEIsZUFBZSxHQUFHLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQztnQkFFeEUsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFLEdBQUcsZUFBZSxFQUFFO29CQUMxQyxzQkFBc0I7b0JBQ3RCLE1BQU0sS0FBSyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztvQkFFNUMsT0FBTyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7aUJBQ3RCO2FBQ0Q7WUFHRCxNQUFNLEtBQUssQ0FBQztRQUNiLENBQUM7S0FBQTs7OztJQUdLLE9BQU87O1lBQ1osSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDM0IsTUFBTSxJQUFJLEtBQUssQ0FBQyx1REFBdUQsQ0FBQyxDQUFDO2FBQ3pFO1lBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ3JCLE1BQU0sSUFBSSxLQUFLLENBQUMsMENBQTBDLENBQUMsQ0FBQzthQUM1RDs7O2tCQUlLLFVBQVUsR0FBRyxDQUFDLEdBQUcsRUFBRSxHQUFHLElBQUk7O2tCQUUxQixtQkFBbUIsR0FDeEIsVUFBVTtnQkFDVixDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1lBRXBDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBQyxtQkFBbUIsRUFBQyxDQUFDLENBQUM7WUFFbkMsSUFBSSxtQkFBbUIsR0FBRyxDQUFDLEVBQUU7Z0JBQzVCLE1BQU0sS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUM7YUFDakM7O2tCQUdLLEtBQUssR0FBRyxJQUFJLENBQUMsZ0JBQWdCO1lBRW5DLDBCQUEwQjtZQUMxQixJQUFJOztzQkFDRyxXQUFXLEdBQUcsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQztnQkFFaEQsSUFBSSxDQUFDLFdBQVcsRUFBRTtvQkFDakIsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxHQUFHLEtBQUssR0FBRyxvQkFBb0IsQ0FBQyxDQUFDO29CQUV0RSxPQUFPLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztpQkFDdEI7Z0JBR0QsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxHQUFHLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUVyRSxPQUFPLEtBQUssQ0FBQzthQUNiO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ2YsWUFBWSxDQUFDLHlCQUF5QixHQUFHLEtBQUssQ0FBQyxDQUFDO2dCQUNoRCxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUVyQixPQUFPO2dCQUNQLE1BQU0sS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQztnQkFFdEIsT0FBTyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDdEI7UUFDRixDQUFDO0tBQUE7Q0FDRDs7Ozs7O0lBeEpBLGtEQUF5Qjs7Ozs7SUFFekIsOENBQW9DOzs7OztJQUVwQyx1REFBZ0U7Ozs7O0lBQ2hFLHNEQUFpQzs7Ozs7SUFDakMscURBQWdDOztJQUcvQiw2Q0FBeUM7Ozs7O0lBQ3pDLHlDQUFvQjs7Ozs7SUFDcEIsZ0RBQWdEOzs7OztJQUNoRCxrREFBNkM7Ozs7O0lBQzdDLHdEQUFnRDs7Ozs7SUFDaEQscURBQTJEOzs7OztBQTZJN0QsTUFBTSxPQUFPLHNCQUF1RCxTQUFRLDBCQUEwQjs7Ozs7Ozs7SUFDckcsWUFDQyxPQUF5QixFQUN6QixVQUEwQixFQUMxQixVQUErQixFQUMvQixZQUFZLEdBQUcsSUFBSSxFQUNuQixrQkFBa0IsR0FBRyxFQUFFO1FBRXZCLEtBQUssQ0FDSixPQUFPLENBQUMsR0FBRzs7OztRQUNWLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxtQkFDUixNQUFNLElBQ1QsYUFBYSxFQUFFLFVBQVUsQ0FBQyxhQUFhLElBQ3RDLEVBQ0YsRUFDRCxVQUFVLENBQUMsVUFBVSxFQUNyQixVQUFVLEVBQ1YsWUFBWSxFQUNaLGtCQUFrQixDQUNsQixDQUFDO0lBQ0gsQ0FBQztDQUNEOzs7Ozs7OztBQU9ELFNBQVMsbUJBQW1CLENBQUMsS0FBSzs7VUFDM0IsYUFBYSxHQUFHO1FBQ3JCLGNBQWM7UUFDZCx1QkFBdUI7UUFDdkIsdUJBQXVCO0tBQ3ZCOztVQUVLLFdBQVcsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDO0lBRWpDLEtBQUssTUFBTSxZQUFZLElBQUksYUFBYSxFQUFFO1FBQ3pDLElBQUksV0FBVyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUMzQyxPQUFPLElBQUksQ0FBQztTQUNaO0tBQ0Q7SUFFRCxPQUFPLEtBQUssQ0FBQztBQUNkLENBQUM7Ozs7QUFJRCxpQ0FpQkM7OztJQWhCQSx5Q0FBb0I7O0lBQ3BCLHlDQVlFOztJQUVGLHdDQUFxQjs7Ozs7QUFHdEIsdUNBR0M7OztJQUZBLGtEQUF1Qjs7SUFDdkIsK0NBQWdDOzs7OztBQUdqQyw0Q0FFQzs7O0lBREEsdURBQXVCOzs7Ozs7Ozs7QUFLeEIsU0FBZSxlQUFlLENBQzdCLFVBQVUsRUFDVixPQUFnQyxFQUNoQyxlQUF1Qjs7O2NBRWpCLE1BQU0sR0FBNkIsTUFBTSxVQUFVLENBQUMsV0FBVyxDQUNwRTtZQUNDLE9BQU87U0FDUCxFQUNEO1lBQ0MsU0FBUyxFQUFFLEtBQUs7WUFDaEIsSUFBSSxFQUFFLElBQUk7WUFDVixlQUFlO1NBQ2YsQ0FDRDtRQUVELE9BQU8sTUFBTSxDQUFDO0lBQ2YsQ0FBQztDQUFBOzs7Ozs7O0FBRUQsU0FBZSwwQkFBMEIsQ0FDeEMsVUFBVSxFQUNWLFdBQStCOzs7Y0FFekIsTUFBTSxHQUFrQyxNQUFNLFVBQVUsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDO1FBRTNGLE9BQU8sTUFBTSxDQUFDLGNBQWMsQ0FBQztJQUM5QixDQUFDO0NBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1R4bkNvbmZpcm1lZENoZWNrZXIsIElFb3NBY3Rpb25EYXRhLCBzbGVlcCwgZGVidWdNZXNzYWdlLCBJRW9zVHJhbnNhY3Rpb25BY3Rpb259IGZyb20gJ2Vhcm5iZXQtY29tbW9uJztcblxuaW1wb3J0IHsgSUVvc1V0aWxpdHkgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSUVvc0FjY291bnROYW1lc0Jhc2UgfSBmcm9tICcuL2Vvcy1zZXR0aW5ncyc7XG5cblxuXG5jb25zdCBfMUhvdXJzSW5TZWNvbmRzID0gNjAgKiA2MDtcblxuXG5leHBvcnQgY2xhc3MgRW9zVHJhbnNhY3Rpb25FeGVjdXRvckJhc2Uge1xuXHRwcml2YXRlIG51bU9mUmV0cmllcyA9IDA7XG5cblx0cHJvdGVjdGVkIHJlYWRvbmx5IGFjdGlvbklkOiBzdHJpbmc7XG5cblx0cHJpdmF0ZSBzaWduZWRUcmFuc2FjdGlvbjogSVNpZ25lZFRyYW5zYWN0aW9uUmVzdWx0ID0gdW5kZWZpbmVkO1xuXHRwcml2YXRlIGJyb2FkY2FzdGVkVHhuSWQ6IHN0cmluZztcblx0cHJpdmF0ZSBicm9hZGNhc3RlZFRpbWU6IG51bWJlcjtcblxuXHRjb25zdHJ1Y3Rvcihcblx0XHRyZWFkb25seSBhY3Rpb25zOiBJRW9zVHJhbnNhY3Rpb25BY3Rpb25bXSxcblx0XHRwcml2YXRlIHJlYWRvbmx5IGVvcyxcblx0XHRwcml2YXRlIHJlYWRvbmx5IHR4bkNoZWNrZXI6IFR4bkNvbmZpcm1lZENoZWNrZXIsXG5cdFx0cHJpdmF0ZSByZWFkb25seSByZXRyeU9uRXJyb3I6IGJvb2xlYW4gPSB0cnVlLFxuXHRcdHByaXZhdGUgcmVhZG9ubHkgc2Vjb25kc0JlZm9yZVJldHJ5OiBudW1iZXIgPSAxMCxcblx0XHRwcml2YXRlIHJlYWRvbmx5IGV4cGlyZUluU2Vjb25kczogbnVtYmVyID0gXzFIb3Vyc0luU2Vjb25kc1xuXHQpIHtcblx0XHQvLyBpbmNsdWRlIGZpcnN0IGFjdGlvbiBmb3Igbm93XG5cdFx0Ly8gaW5jbHVkZSBvdGhlciBhY3Rpb25zIGFzIHdlbGw/XG5cdFx0Y29uc3Qge2FjY291bnQsIG5hbWV9ID0gdGhpcy5hY3Rpb25zWzBdO1xuXG5cdFx0dGhpcy5hY3Rpb25JZCA9IGFjY291bnQgKyAnIC0gJyArIG5hbWU7XG5cdH1cblxuXG5cblx0YXN5bmMgZXhlY3V0ZSgpOiBQcm9taXNlPHN0cmluZz4ge1xuXHRcdGRlYnVnTWVzc2FnZShcblx0XHRcdCdBdHRlbXB0aW5nIHRvIEV4ZWN1dGUgVHJhbnNhY3Rpb246ICcgKyB0aGlzLmFjdGlvbklkXG5cdFx0KTtcblx0XHRjb25zb2xlLmxvZyh0aGlzLmFjdGlvbnMpO1xuXG5cblx0XHR0cnkge1xuXHRcdFx0YXdhaXQgdGhpcy5zaWduKCk7XG5cblx0XHRcdC8vIGF0dGVtcHQgdG8gYnJvYWRjYXN0XG5cdFx0XHR0aGlzLmJyb2FkY2FzdGVkVHhuSWQgPSBhd2FpdCBicm9hZGNhc3RTaWduZWRUcmFuc2FjdGlvbihcblx0XHRcdFx0dGhpcy5lb3MsXG5cdFx0XHRcdHRoaXMuc2lnbmVkVHJhbnNhY3Rpb24udHJhbnNhY3Rpb25cblx0XHRcdCk7XG5cblx0XHRcdHRoaXMuYnJvYWRjYXN0ZWRUaW1lID0gRGF0ZS5ub3coKTtcblxuXHRcdFx0ZGVidWdNZXNzYWdlKHRoaXMuYWN0aW9uSWQgKyAnIEJyb2FkY2FzdGVkOiAnICsgdGhpcy5icm9hZGNhc3RlZFR4bklkKTtcblxuXHRcdFx0cmV0dXJuIHRoaXMuYnJvYWRjYXN0ZWRUeG5JZDtcblx0XHR9IGNhdGNoIChlcnJvcikge1xuXHRcdFx0cmV0dXJuIHRoaXMub25Ccm9hZGNhc3RFcnJvcihlcnJvcik7XG5cdFx0fVxuXHR9XG5cblx0YXN5bmMgc2lnbigpOiBQcm9taXNlPElTaWduZWRUcmFuc2FjdGlvblJlc3VsdD4ge1xuXHRcdHRyeSB7XG5cdFx0XHQvLyBzaWduIHRyYW5zYWN0aW9uIGlmIG5vdCBhbHJlYWR5IHNpZ25lZFxuXHRcdFx0aWYgKHRoaXMuc2lnbmVkVHJhbnNhY3Rpb24gPT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRcdHRoaXMuc2lnbmVkVHJhbnNhY3Rpb24gPSBhd2FpdCBzaWduVHJhbnNhY3Rpb24oXG5cdFx0XHRcdFx0dGhpcy5lb3MsXG5cdFx0XHRcdFx0dGhpcy5hY3Rpb25zLFxuXHRcdFx0XHRcdHRoaXMuZXhwaXJlSW5TZWNvbmRzXG5cdFx0XHRcdCk7XG5cdFx0XHR9XG5cblx0XHRcdGRlYnVnTWVzc2FnZSgnVHJhbnNhY3Rpb24gU2lnbmVkOiAnICsgdGhpcy5zaWduZWRUcmFuc2FjdGlvbi50cmFuc2FjdGlvbl9pZCk7XG5cblx0XHRcdHJldHVybiB0aGlzLnNpZ25lZFRyYW5zYWN0aW9uO1xuXHRcdH0gY2F0Y2ggKGVycm9yKSB7XG5cdFx0XHRjb25zb2xlLmVycm9yKGVycm9yKTtcblxuXHRcdFx0YXdhaXQgc2xlZXAoMTAwMCk7XG5cblx0XHRcdHJldHVybiB0aGlzLnNpZ24oKTtcblx0XHR9XG5cdH1cblxuXHRwcm90ZWN0ZWQgYXN5bmMgb25Ccm9hZGNhc3RFcnJvcihlcnJvcik6IFByb21pc2U8c3RyaW5nPiB7XG5cdFx0Y29uc29sZS5lcnJvcihlcnJvcik7XG5cblx0XHRjb25zdCBpc0R1cGxpY2F0ZVR4biA9IGlzRHVwbGljYXRlVHhuRXJyb3IoZXJyb3IpO1xuXG5cdFx0Ly8gaWYgZHVwbGljYXRlIGVycm9yLCB0aGVuIHN0b3AgYnJvYWRjYXN0aW5nIHByb2Nlc3Ncblx0XHQvLyByZXR1cm4gb3JpZ2luYWxseSBicm9hZGNhc3RlZCB0eG5JZFxuXHRcdGlmIChpc0R1cGxpY2F0ZVR4bikge1xuXHRcdFx0cmV0dXJuIHRoaXMuYnJvYWRjYXN0ZWRUeG5JZDtcblx0XHR9XG5cblxuXHRcdGlmICh0aGlzLnJldHJ5T25FcnJvcikge1xuXHRcdFx0Y29uc3QgbWF4TnVtT2ZSZXRyaWVzID0gKF8xSG91cnNJblNlY29uZHMgLyB0aGlzLnNlY29uZHNCZWZvcmVSZXRyeSkgLSAxO1xuXG5cdFx0XHRpZiAodGhpcy5udW1PZlJldHJpZXMrKyA8IG1heE51bU9mUmV0cmllcykge1xuXHRcdFx0XHQvLyB3YWl0IGFuZCB0aGVuIHJldHJ5XG5cdFx0XHRcdGF3YWl0IHNsZWVwKDEwMDAgKiB0aGlzLnNlY29uZHNCZWZvcmVSZXRyeSk7XG5cblx0XHRcdFx0cmV0dXJuIHRoaXMuZXhlY3V0ZSgpO1xuXHRcdFx0fVxuXHRcdH1cblxuXG5cdFx0dGhyb3cgZXJyb3I7XG5cdH1cblxuXG5cdGFzeW5jIGNvbmZpcm0oKTogUHJvbWlzZTxzdHJpbmc+IHtcblx0XHRpZiAoIXRoaXMuYnJvYWRjYXN0ZWRUeG5JZCkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdUcmFuc2FjdGlvbiBoYXMgbm90IHlldCBiZWVuIHN1Y2Nlc2Z1bGx5IGJyb2FkY2FzdGVkIScpO1xuXHRcdH1cblxuXHRcdGlmICghdGhpcy50eG5DaGVja2VyKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ25vIFRyYW5zYWN0aW9uIENoZWNrZXIgZnVuY3Rpb24gZGVmaW5lZCEnKTtcblx0XHR9XG5cblxuXHRcdC8vIHdhaXQgNSBtaW51dGVzIGJlZm9yZSBjaGVja2luZyBpcnJldmVyc2liaWxpdHlcblx0XHRjb25zdCB0aW1lVG9XYWl0ID0gNSAqIDYwICogMTAwMDtcblxuXHRcdGNvbnN0IHJlbWFpbmluZ1RpbWVUb1dhaXQgPVxuXHRcdFx0dGltZVRvV2FpdCAtXG5cdFx0XHQoRGF0ZS5ub3coKSAtIHRoaXMuYnJvYWRjYXN0ZWRUaW1lKTtcblxuXHRcdGNvbnNvbGUubG9nKHtyZW1haW5pbmdUaW1lVG9XYWl0fSk7XG5cblx0XHRpZiAocmVtYWluaW5nVGltZVRvV2FpdCA+IDApIHtcblx0XHRcdGF3YWl0IHNsZWVwKHJlbWFpbmluZ1RpbWVUb1dhaXQpO1xuXHRcdH1cblxuXG5cdFx0Y29uc3QgdHhuSWQgPSB0aGlzLmJyb2FkY2FzdGVkVHhuSWQ7XG5cblx0XHQvLyBjb25maXJtIGlycmV2ZXJzaWJpbGl0eVxuXHRcdHRyeSB7XG5cdFx0XHRjb25zdCBpc0NvbmZpcm1lZCA9IGF3YWl0IHRoaXMudHhuQ2hlY2tlcih0eG5JZCk7XG5cblx0XHRcdGlmICghaXNDb25maXJtZWQpIHtcblx0XHRcdFx0ZGVidWdNZXNzYWdlKHRoaXMuYWN0aW9uSWQgKyAnIFR4bjogJyArIHR4bklkICsgJyBpcyBOT1QgQ09ORklSTUVEIScpO1xuXG5cdFx0XHRcdHJldHVybiB0aGlzLmV4ZWN1dGUoKTtcblx0XHRcdH1cblxuXG5cdFx0XHRkZWJ1Z01lc3NhZ2UodGhpcy5hY3Rpb25JZCArICcgVHhuOiAnICsgdHhuSWQgKyAnIGlzIElSUkVWRVJTSUJMRSEnKTtcblxuXHRcdFx0cmV0dXJuIHR4bklkO1xuXHRcdH0gY2F0Y2ggKGVycm9yKSB7XG5cdFx0XHRkZWJ1Z01lc3NhZ2UoJ0NvdW5kIE5vdCBDb25maXJtIFR4bjogJyArIHR4bklkKTtcblx0XHRcdGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG5cdFx0XHQvLyB3YWl0XG5cdFx0XHRhd2FpdCBzbGVlcCg1ICogMTAwMCk7XG5cblx0XHRcdHJldHVybiB0aGlzLmV4ZWN1dGUoKTtcblx0XHR9XG5cdH1cbn1cblxuXG5leHBvcnQgY2xhc3MgRW9zVHJhbnNhY3Rpb25FeGVjdXRvcjxUIGV4dGVuZHMgSUVvc0FjY291bnROYW1lc0Jhc2U+IGV4dGVuZHMgRW9zVHJhbnNhY3Rpb25FeGVjdXRvckJhc2Uge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRhY3Rpb25zOiBJRW9zQWN0aW9uRGF0YVtdLFxuXHRcdGVvc1V0aWxpdHk6IElFb3NVdGlsaXR5PFQ+LFxuXHRcdHR4bkNoZWNrZXI6IFR4bkNvbmZpcm1lZENoZWNrZXIsXG5cdFx0cmV0cnlPbkVycm9yID0gdHJ1ZSxcblx0XHRzZWNvbmRzQmVmb3JlUmV0cnkgPSAxMFxuXHQpIHtcblx0XHRzdXBlcihcblx0XHRcdGFjdGlvbnMubWFwKFxuXHRcdFx0XHQoYWN0aW9uKSA9PiAoe1xuXHRcdFx0XHRcdC4uLmFjdGlvbixcblx0XHRcdFx0XHRhdXRob3JpemF0aW9uOiBlb3NVdGlsaXR5LmF1dGhvcml6YXRpb25cblx0XHRcdFx0fSlcblx0XHRcdCksXG5cdFx0XHRlb3NVdGlsaXR5LnRyYW5zYWN0b3IsXG5cdFx0XHR0eG5DaGVja2VyLFxuXHRcdFx0cmV0cnlPbkVycm9yLFxuXHRcdFx0c2Vjb25kc0JlZm9yZVJldHJ5XG5cdFx0KTtcblx0fVxufVxuXG5cblxuLypcbkVycm9yOiB7XCJjb2RlXCI6NDA5LFwibWVzc2FnZVwiOlwiQ29uZmxpY3RcIixcImVycm9yXCI6e1wiY29kZVwiOjMwNDAwMDgsXCJuYW1lXCI6XCJ0eF9kdXBsaWNhdGVcIixcIndoYXRcIjpcIkR1cGxpY2F0ZSB0cmFuc2FjdGlvblwiLFwiZGV0YWlsc1wiOlt7XCJtZXNzYWdlXCI6XCJkdXBsaWNhdGUgdHJhbnNhY3Rpb24gN2JlMjg0NTYxOGZhOTY1ZDg5ZmY4ZjNiMzY4ZWQxYzc2OGRhYjYzMzAzYzM4YzdkOGE1ZTlmMDMxZDdhMjNjNlwiLFwiZmlsZVwiOlwicHJvZHVjZXJfcGx1Z2luLmNwcFwiLFwibGluZV9udW1iZXJcIjo1MjcsXCJtZXRob2RcIjpcInByb2Nlc3NfaW5jb21pbmdfdHJhbnNhY3Rpb25fYXN5bmNcIn1dfX1cbiovXG5mdW5jdGlvbiBpc0R1cGxpY2F0ZVR4bkVycm9yKGVycm9yKTogYm9vbGVhbiB7XG5cdGNvbnN0IHNlYXJjaFN0cmluZ3MgPSBbXG5cdFx0J3R4X2R1cGxpY2F0ZScsXG5cdFx0J0R1cGxpY2F0ZSB0cmFuc2FjdGlvbicsXG5cdFx0J2R1cGxpY2F0ZSB0cmFuc2FjdGlvbidcblx0XTtcblxuXHRjb25zdCBlcnJvclN0cmluZyA9IFN0cmluZyhlcnJvcik7XG5cblx0Zm9yIChjb25zdCBzZWFyY2hTdHJpbmcgb2Ygc2VhcmNoU3RyaW5ncykge1xuXHRcdGlmIChlcnJvclN0cmluZy5pbmRleE9mKHNlYXJjaFN0cmluZykgPiAtMSkge1xuXHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0fVxuXHR9XG5cblx0cmV0dXJuIGZhbHNlO1xufVxuXG5cblxuaW50ZXJmYWNlIElTaWduZWRUcmFuc2FjdGlvbiB7XG5cdGNvbXByZXNzaW9uOiBzdHJpbmc7XG5cdHRyYW5zYWN0aW9uOiB7XG5cdFx0LypcbiAgICAgICAgZXhwaXJhdGlvbjogJzIwMjAtMDctMTNUMjE6NDk6MzEnLFxuICAgICByZWZfYmxvY2tfbnVtOiAzNDQyNixcbiAgICAgcmVmX2Jsb2NrX3ByZWZpeDogMzQ3MzI1MzgxNSxcbiAgICAgbWF4X25ldF91c2FnZV93b3JkczogMCxcbiAgICAgbWF4X2NwdV91c2FnZV9tczogMCxcbiAgICAgZGVsYXlfc2VjOiAwLFxuICAgICBjb250ZXh0X2ZyZWVfYWN0aW9uczogW10sXG4gICAgIGFjdGlvbnM6IFsgW09iamVjdF0gXSxcbiAgICAgdHJhbnNhY3Rpb25fZXh0ZW5zaW9uczogW11cbiAgICAgICAgKi9cblx0fTtcblxuXHRzaWduYXR1cmVzOiBzdHJpbmdbXTtcbn1cblxuaW50ZXJmYWNlIElTaWduZWRUcmFuc2FjdGlvblJlc3VsdCB7XG5cdHRyYW5zYWN0aW9uX2lkOiBzdHJpbmc7XG5cdHRyYW5zYWN0aW9uOiBJU2lnbmVkVHJhbnNhY3Rpb247XG59XG5cbmludGVyZmFjZSBJQnJvYWRjYXN0ZWRUcmFuc2FjdGlvblJlc3VsdCB7XG5cdHRyYW5zYWN0aW9uX2lkOiBzdHJpbmc7XG59XG5cblxuXG5hc3luYyBmdW5jdGlvbiBzaWduVHJhbnNhY3Rpb24oXG5cdHRyYW5zYWN0b3IsXG5cdGFjdGlvbnM6IElFb3NUcmFuc2FjdGlvbkFjdGlvbltdLFxuXHRleHBpcmVJblNlY29uZHM6IG51bWJlclxuKTogUHJvbWlzZTxJU2lnbmVkVHJhbnNhY3Rpb25SZXN1bHQ+IHtcblx0Y29uc3QgcmVzdWx0OiBJU2lnbmVkVHJhbnNhY3Rpb25SZXN1bHQgPSBhd2FpdCB0cmFuc2FjdG9yLnRyYW5zYWN0aW9uKFxuXHRcdHtcblx0XHRcdGFjdGlvbnNcblx0XHR9LFxuXHRcdHtcblx0XHRcdGJyb2FkY2FzdDogZmFsc2UsXG5cdFx0XHRzaWduOiB0cnVlLFxuXHRcdFx0ZXhwaXJlSW5TZWNvbmRzXG5cdFx0fVxuXHQpO1xuXG5cdHJldHVybiByZXN1bHQ7XG59XG5cbmFzeW5jIGZ1bmN0aW9uIGJyb2FkY2FzdFNpZ25lZFRyYW5zYWN0aW9uKFxuXHR0cmFuc2FjdG9yLFxuXHR0cmFuc2FjdGlvbjogSVNpZ25lZFRyYW5zYWN0aW9uXG4pOiBQcm9taXNlPHN0cmluZz4ge1xuXHRjb25zdCByZXN1bHQ6IElCcm9hZGNhc3RlZFRyYW5zYWN0aW9uUmVzdWx0ID0gYXdhaXQgdHJhbnNhY3Rvci5wdXNoVHJhbnNhY3Rpb24odHJhbnNhY3Rpb24pO1xuXG5cdHJldHVybiByZXN1bHQudHJhbnNhY3Rpb25faWQ7XG59XG4iXX0=