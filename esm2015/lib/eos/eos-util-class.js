/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-util-class.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { getUniqueTableRow, getAllTableRows, getTableRows, getTokenBalance } from 'earnbet-common';
import { getEosJs } from './eos-util';
/**
 * @template T
 */
export class EosUtility {
    /**
     * @param {?} params
     */
    constructor(params) {
        const { config } = params;
        /** @type {?} */
        const endpoints = config.endpoints;
        this.poller = getEosJs(config, endpoints.poller);
        this.transactor = getEosJs(config, endpoints.transactor);
        this.chainId = params.chainId;
        this.config = config;
        this.contracts = params.contracts;
        this.authorization = params.authorization;
    }
    /**
     * @return {?}
     */
    getInfo() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return this.poller.getInfo({});
        });
    }
    /**
     * @template T
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} rowId
     * @return {?}
     */
    getUniqueTableRow(code, scope, table, rowId) {
        return getUniqueTableRow(this.poller, code, scope, table, rowId);
    }
    /**
     * @template T
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} key
     * @return {?}
     */
    getAllTableRows(code, scope, table, key) {
        return getAllTableRows(this.poller, code, scope, table, key);
    }
    /**
     * @template T
     * @param {?} parameters
     * @return {?}
     */
    getTableRows(parameters) {
        return getTableRows(this.poller, parameters);
    }
    /**
     * @param {?} contract
     * @param {?} account
     * @param {?} symbol
     * @param {?=} table
     * @return {?}
     */
    getTokenBalance(contract, account, symbol, table = 'accounts') {
        return getTokenBalance(this.poller, contract, account, symbol, table);
    }
    /**
     * @param {?} actions
     * @return {?}
     */
    executeTransaction(actions) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                /** @type {?} */
                const result = yield this.transactor.transaction({
                    actions: actions.map((/**
                     * @param {?} action
                     * @return {?}
                     */
                    (action) => (Object.assign({}, action, { authorization: this.authorization }))))
                });
                return result.transaction_id;
            }
            catch (error) {
                throw error;
            }
        });
    }
    /**
     * @param {?} eosAccountName
     * @return {?}
     */
    isAccountSafe(eosAccountName) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this.isEasyAccountContract(eosAccountName)) {
                return true;
            }
            try {
                /** @type {?} */
                const result = yield this.poller.getCodeHash(eosAccountName);
                return result.code_hash == '0000000000000000000000000000000000000000000000000000000000000000';
            }
            catch (error) {
                return false;
            }
        });
    }
    /**
     * @param {?} name
     * @return {?}
     */
    isEasyAccountContract(name) {
        return this.contracts.easyAccount != undefined &&
            name == this.contracts.easyAccount;
    }
}
if (false) {
    /** @type {?} */
    EosUtility.prototype.poller;
    /** @type {?} */
    EosUtility.prototype.transactor;
    /** @type {?} */
    EosUtility.prototype.chainId;
    /** @type {?} */
    EosUtility.prototype.config;
    /** @type {?} */
    EosUtility.prototype.contracts;
    /** @type {?} */
    EosUtility.prototype.authorization;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLXV0aWwtY2xhc3MuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9lb3MvZW9zLXV0aWwtY2xhc3MudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUF3QixpQkFBaUIsRUFBRSxlQUFlLEVBQUUsWUFBWSxFQUE0QyxlQUFlLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUluSyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sWUFBWSxDQUFDOzs7O0FBR3RDLE1BQU0sT0FBTyxVQUFVOzs7O0lBU3RCLFlBQVksTUFBNEI7Y0FDakMsRUFBQyxNQUFNLEVBQUMsR0FBRyxNQUFNOztjQUVqQixTQUFTLEdBQUcsTUFBTSxDQUFDLFNBQVM7UUFFbEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRXpELElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUM5QixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUM7UUFDbEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsYUFBYSxDQUFDO0lBQzNDLENBQUM7Ozs7SUFFSyxPQUFPOztZQUNaLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDaEMsQ0FBQztLQUFBOzs7Ozs7Ozs7SUFFRCxpQkFBaUIsQ0FBSSxJQUFZLEVBQUUsS0FBYSxFQUFFLEtBQWEsRUFBRSxLQUFhO1FBQzdFLE9BQU8saUJBQWlCLENBQ3ZCLElBQUksQ0FBQyxNQUFNLEVBQ1gsSUFBSSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUN6QixDQUFDO0lBQ0gsQ0FBQzs7Ozs7Ozs7O0lBRUQsZUFBZSxDQUFJLElBQVksRUFBRSxLQUFhLEVBQUUsS0FBYSxFQUFFLEdBQVc7UUFDekUsT0FBTyxlQUFlLENBQ3JCLElBQUksQ0FBQyxNQUFNLEVBQ1gsSUFBSSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUN2QixDQUFDO0lBQ0gsQ0FBQzs7Ozs7O0lBRUQsWUFBWSxDQUFJLFVBQW9DO1FBQ25ELE9BQU8sWUFBWSxDQUNsQixJQUFJLENBQUMsTUFBTSxFQUNYLFVBQVUsQ0FDVixDQUFDO0lBQ0gsQ0FBQzs7Ozs7Ozs7SUFFRCxlQUFlLENBQ2QsUUFBZ0IsRUFBRSxPQUFlLEVBQUUsTUFBYyxFQUNqRCxLQUFLLEdBQUcsVUFBVTtRQUVsQixPQUFPLGVBQWUsQ0FDckIsSUFBSSxDQUFDLE1BQU0sRUFDWCxRQUFRLEVBQ1IsT0FBTyxFQUNQLE1BQU0sRUFDTixLQUFLLENBQ0wsQ0FBQztJQUNILENBQUM7Ozs7O0lBRUssa0JBQWtCLENBQ3ZCLE9BQXlCOztZQUV6QixJQUFJOztzQkFDRyxNQUFNLEdBQUcsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQztvQkFDaEQsT0FBTyxFQUFFLE9BQU8sQ0FBQyxHQUFHOzs7O29CQUNuQixDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsbUJBQ1IsTUFBTSxJQUNULGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYSxJQUNoQyxFQUNGO2lCQUNELENBQUM7Z0JBRUYsT0FBTyxNQUFNLENBQUMsY0FBYyxDQUFDO2FBQzdCO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ2YsTUFBTSxLQUFLLENBQUM7YUFDWjtRQUNGLENBQUM7S0FBQTs7Ozs7SUFHSyxhQUFhLENBQUMsY0FBc0I7O1lBQ3pDLElBQUssSUFBSSxDQUFDLHFCQUFxQixDQUFDLGNBQWMsQ0FBQyxFQUFHO2dCQUNqRCxPQUFPLElBQUksQ0FBQzthQUNaO1lBR0QsSUFBSTs7c0JBQ0csTUFBTSxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDO2dCQUU1RCxPQUFPLE1BQU0sQ0FBQyxTQUFTLElBQUksa0VBQWtFLENBQUM7YUFDOUY7WUFBQyxPQUFPLEtBQUssRUFBRTtnQkFDZixPQUFPLEtBQUssQ0FBQzthQUNiO1FBQ0YsQ0FBQztLQUFBOzs7OztJQUVELHFCQUFxQixDQUFDLElBQVk7UUFDakMsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsSUFBSSxTQUFTO1lBQzVDLElBQUksSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQztJQUN0QyxDQUFDO0NBQ0Q7OztJQW5HQSw0QkFBZ0I7O0lBQ2hCLGdDQUFvQjs7SUFFcEIsNkJBQXlCOztJQUN6Qiw0QkFBNEI7O0lBQzVCLCtCQUFzQjs7SUFDdEIsbUNBQStDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSUFjdGlvbkF1dGhvcml6YXRpb24sIGdldFVuaXF1ZVRhYmxlUm93LCBnZXRBbGxUYWJsZVJvd3MsIGdldFRhYmxlUm93cywgSVRhYmxlUm93UXVlcnlQYXJhbWV0ZXJzLCBJRW9zQWN0aW9uRGF0YSwgZ2V0VG9rZW5CYWxhbmNlIH0gZnJvbSAnZWFybmJldC1jb21tb24nO1xuXG5pbXBvcnQgeyBJRW9zVXRpbGl0eSwgSUVvc1V0aWxpdHlQYXJhbXMsIElFT1NJbmZvIH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElFb3NDb25maWcsIElFb3NBY2NvdW50TmFtZXNCYXNlIH0gZnJvbSAnLi9lb3Mtc2V0dGluZ3MnO1xuaW1wb3J0IHsgZ2V0RW9zSnMgfSBmcm9tICcuL2Vvcy11dGlsJztcblxuXG5leHBvcnQgY2xhc3MgRW9zVXRpbGl0eTxUIGV4dGVuZHMgSUVvc0FjY291bnROYW1lc0Jhc2U+IGltcGxlbWVudHMgSUVvc1V0aWxpdHk8VD4ge1xuXHRyZWFkb25seSBwb2xsZXI7XG5cdHJlYWRvbmx5IHRyYW5zYWN0b3I7XG5cblx0cmVhZG9ubHkgY2hhaW5JZDogbnVtYmVyO1xuXHRyZWFkb25seSBjb25maWc6IElFb3NDb25maWc7XG5cdHJlYWRvbmx5IGNvbnRyYWN0czogVDtcblx0cmVhZG9ubHkgYXV0aG9yaXphdGlvbjogSUFjdGlvbkF1dGhvcml6YXRpb25bXTtcblxuXHRjb25zdHJ1Y3RvcihwYXJhbXM6IElFb3NVdGlsaXR5UGFyYW1zPFQ+KSB7XG5cdFx0Y29uc3Qge2NvbmZpZ30gPSBwYXJhbXM7XG5cblx0XHRjb25zdCBlbmRwb2ludHMgPSBjb25maWcuZW5kcG9pbnRzO1xuXG5cdFx0dGhpcy5wb2xsZXIgPSBnZXRFb3NKcyhjb25maWcsIGVuZHBvaW50cy5wb2xsZXIpO1xuXHRcdHRoaXMudHJhbnNhY3RvciA9IGdldEVvc0pzKGNvbmZpZywgZW5kcG9pbnRzLnRyYW5zYWN0b3IpO1xuXG5cdFx0dGhpcy5jaGFpbklkID0gcGFyYW1zLmNoYWluSWQ7XG5cdFx0dGhpcy5jb25maWcgPSBjb25maWc7XG5cdFx0dGhpcy5jb250cmFjdHMgPSBwYXJhbXMuY29udHJhY3RzO1xuXHRcdHRoaXMuYXV0aG9yaXphdGlvbiA9IHBhcmFtcy5hdXRob3JpemF0aW9uO1xuXHR9XG5cblx0YXN5bmMgZ2V0SW5mbygpOiBQcm9taXNlPElFT1NJbmZvPiB7XG5cdFx0cmV0dXJuIHRoaXMucG9sbGVyLmdldEluZm8oe30pO1xuXHR9XG5cblx0Z2V0VW5pcXVlVGFibGVSb3c8VD4oY29kZTogc3RyaW5nLCBzY29wZTogc3RyaW5nLCB0YWJsZTogc3RyaW5nLCByb3dJZDogc3RyaW5nKSB7XG5cdFx0cmV0dXJuIGdldFVuaXF1ZVRhYmxlUm93PFQ+KFxuXHRcdFx0dGhpcy5wb2xsZXIsXG5cdFx0XHRjb2RlLCBzY29wZSwgdGFibGUsIHJvd0lkXG5cdFx0KTtcblx0fVxuXG5cdGdldEFsbFRhYmxlUm93czxUPihjb2RlOiBzdHJpbmcsIHNjb3BlOiBzdHJpbmcsIHRhYmxlOiBzdHJpbmcsIGtleTogc3RyaW5nKSB7XG5cdFx0cmV0dXJuIGdldEFsbFRhYmxlUm93czxUPihcblx0XHRcdHRoaXMucG9sbGVyLFxuXHRcdFx0Y29kZSwgc2NvcGUsIHRhYmxlLCBrZXlcblx0XHQpO1xuXHR9XG5cblx0Z2V0VGFibGVSb3dzPFQ+KHBhcmFtZXRlcnM6IElUYWJsZVJvd1F1ZXJ5UGFyYW1ldGVycykge1xuXHRcdHJldHVybiBnZXRUYWJsZVJvd3M8VD4oXG5cdFx0XHR0aGlzLnBvbGxlcixcblx0XHRcdHBhcmFtZXRlcnNcblx0XHQpO1xuXHR9XG5cblx0Z2V0VG9rZW5CYWxhbmNlKFxuXHRcdGNvbnRyYWN0OiBzdHJpbmcsIGFjY291bnQ6IHN0cmluZywgc3ltYm9sOiBzdHJpbmcsXG5cdFx0dGFibGUgPSAnYWNjb3VudHMnXG5cdCkge1xuXHRcdHJldHVybiBnZXRUb2tlbkJhbGFuY2UoXG5cdFx0XHR0aGlzLnBvbGxlcixcblx0XHRcdGNvbnRyYWN0LFxuXHRcdFx0YWNjb3VudCxcblx0XHRcdHN5bWJvbCxcblx0XHRcdHRhYmxlXG5cdFx0KTtcblx0fVxuXG5cdGFzeW5jIGV4ZWN1dGVUcmFuc2FjdGlvbihcblx0XHRhY3Rpb25zOiBJRW9zQWN0aW9uRGF0YVtdXG5cdCk6IFByb21pc2U8c3RyaW5nPiB7XG5cdFx0dHJ5IHtcblx0XHRcdGNvbnN0IHJlc3VsdCA9IGF3YWl0IHRoaXMudHJhbnNhY3Rvci50cmFuc2FjdGlvbih7XG5cdFx0XHRcdGFjdGlvbnM6IGFjdGlvbnMubWFwKFxuXHRcdFx0XHRcdChhY3Rpb24pID0+ICh7XG5cdFx0XHRcdFx0XHQuLi5hY3Rpb24sXG5cdFx0XHRcdFx0XHRhdXRob3JpemF0aW9uOiB0aGlzLmF1dGhvcml6YXRpb25cblx0XHRcdFx0XHR9KVxuXHRcdFx0XHQpXG5cdFx0XHR9KTtcblxuXHRcdFx0cmV0dXJuIHJlc3VsdC50cmFuc2FjdGlvbl9pZDtcblx0XHR9IGNhdGNoIChlcnJvcikge1xuXHRcdFx0dGhyb3cgZXJyb3I7XG5cdFx0fVxuXHR9XG5cblxuXHRhc3luYyBpc0FjY291bnRTYWZlKGVvc0FjY291bnROYW1lOiBzdHJpbmcpOiBQcm9taXNlPGJvb2xlYW4+IHtcblx0XHRpZiAoIHRoaXMuaXNFYXN5QWNjb3VudENvbnRyYWN0KGVvc0FjY291bnROYW1lKSApIHtcblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH1cblxuXG5cdFx0dHJ5IHtcblx0XHRcdGNvbnN0IHJlc3VsdCA9IGF3YWl0IHRoaXMucG9sbGVyLmdldENvZGVIYXNoKGVvc0FjY291bnROYW1lKTtcblxuXHRcdFx0cmV0dXJuIHJlc3VsdC5jb2RlX2hhc2ggPT0gJzAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAnO1xuXHRcdH0gY2F0Y2ggKGVycm9yKSB7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXHR9XG5cblx0aXNFYXN5QWNjb3VudENvbnRyYWN0KG5hbWU6IHN0cmluZyk6IGJvb2xlYW4ge1xuXHRcdHJldHVybiB0aGlzLmNvbnRyYWN0cy5lYXN5QWNjb3VudCAhPSB1bmRlZmluZWQgJiZcblx0XHRcdFx0bmFtZSA9PSB0aGlzLmNvbnRyYWN0cy5lYXN5QWNjb3VudDtcblx0fVxufVxuIl19