/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import Eos from 'eosjs-revised';
import eos_ecc from 'eosjs-ecc';
/**
 * @param {?} eosConfig
 * @param {?=} endpoint
 * @return {?}
 */
export function getEosJs(eosConfig, endpoint = undefined) {
    /** @type {?} */
    const cfg = Object.assign({}, eosConfig.eosjs);
    if (endpoint != undefined) {
        cfg.httpEndpoint = endpoint;
    }
    return Eos(cfg);
}
/**
 * @param {?} signedData
 * @param {?} info
 * @return {?}
 */
export function recoverPublicKey(signedData, info) {
    /** @type {?} */
    let recoveredPublicKey;
    try {
        if (!info.isSignedDataHashed) {
            recoveredPublicKey = eos_ecc.recover(info.signature, signedData);
        }
        else {
            /** @type {?} */
            const signedHash = eos_ecc.sha256(eos_ecc.sha256(signedData) +
                eos_ecc.sha256(info.nonce));
            recoveredPublicKey = eos_ecc.recoverHash(info.signature, signedHash);
        }
    }
    catch (error) {
        console.error(error);
    }
    return recoveredPublicKey;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLXV0aWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9lb3MvZW9zLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEdBQUcsTUFBTSxlQUFlLENBQUM7QUFDaEMsT0FBTyxPQUFPLE1BQU0sV0FBVyxDQUFDOzs7Ozs7QUFTaEMsTUFBTSxVQUFVLFFBQVEsQ0FBQyxTQUFxQixFQUFFLFdBQWtCLFNBQVM7O1VBQ3BFLEdBQUcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsS0FBSyxDQUFDO0lBRTlDLElBQUksUUFBUSxJQUFJLFNBQVMsRUFBRTtRQUMxQixHQUFHLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQztLQUM1QjtJQUVELE9BQU8sR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ2pCLENBQUM7Ozs7OztBQUdELE1BQU0sVUFBVSxnQkFBZ0IsQ0FBQyxVQUFrQixFQUFFLElBQW9COztRQUNwRSxrQkFBMEI7SUFFOUIsSUFBSTtRQUNILElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDN0Isa0JBQWtCLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FDbkMsSUFBSSxDQUFDLFNBQVMsRUFDZCxVQUFVLENBQ1YsQ0FBQztTQUNGO2FBQU07O2tCQUNBLFVBQVUsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUNoQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztnQkFDMUIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQzFCO1lBRUQsa0JBQWtCLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1NBQ3JFO0tBQ0Q7SUFBQyxPQUFPLEtBQUssRUFBRTtRQUNmLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDckI7SUFHRCxPQUFPLGtCQUFrQixDQUFDO0FBQzNCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgRW9zIGZyb20gJ2Vvc2pzLXJldmlzZWQnO1xuaW1wb3J0IGVvc19lY2MgZnJvbSAnZW9zanMtZWNjJztcblxuXG5pbXBvcnQgeyBJU2lnbmF0dXJlSW5mbyB9IGZyb20gJ2Vhcm5iZXQtY29tbW9uJztcblxuaW1wb3J0IHsgSUVvc0NvbmZpZyB9IGZyb20gJy4vZW9zLXNldHRpbmdzJztcblxuXG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRFb3NKcyhlb3NDb25maWc6IElFb3NDb25maWcsIGVuZHBvaW50OiBzdHJpbmc9IHVuZGVmaW5lZCkge1xuXHRjb25zdCBjZmcgPSBPYmplY3QuYXNzaWduKHt9LCBlb3NDb25maWcuZW9zanMpO1xuXG5cdGlmIChlbmRwb2ludCAhPSB1bmRlZmluZWQpIHtcblx0XHRjZmcuaHR0cEVuZHBvaW50ID0gZW5kcG9pbnQ7XG5cdH1cblxuXHRyZXR1cm4gRW9zKGNmZyk7XG59XG5cblxuZXhwb3J0IGZ1bmN0aW9uIHJlY292ZXJQdWJsaWNLZXkoc2lnbmVkRGF0YTogc3RyaW5nLCBpbmZvOiBJU2lnbmF0dXJlSW5mbyk6IHN0cmluZyB7XG5cdGxldCByZWNvdmVyZWRQdWJsaWNLZXk6IHN0cmluZztcblxuXHR0cnkge1xuXHRcdGlmICghaW5mby5pc1NpZ25lZERhdGFIYXNoZWQpIHtcblx0XHRcdHJlY292ZXJlZFB1YmxpY0tleSA9IGVvc19lY2MucmVjb3Zlcihcblx0XHRcdFx0aW5mby5zaWduYXR1cmUsXG5cdFx0XHRcdHNpZ25lZERhdGFcblx0XHRcdCk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdGNvbnN0IHNpZ25lZEhhc2ggPSBlb3NfZWNjLnNoYTI1Nihcblx0XHRcdFx0ZW9zX2VjYy5zaGEyNTYoc2lnbmVkRGF0YSkgK1xuXHRcdFx0XHRlb3NfZWNjLnNoYTI1NihpbmZvLm5vbmNlKVxuXHRcdFx0KTtcblxuXHRcdFx0cmVjb3ZlcmVkUHVibGljS2V5ID0gZW9zX2VjYy5yZWNvdmVySGFzaChpbmZvLnNpZ25hdHVyZSwgc2lnbmVkSGFzaCk7XG5cdFx0fVxuXHR9IGNhdGNoIChlcnJvcikge1xuXHRcdGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXHR9XG5cblxuXHRyZXR1cm4gcmVjb3ZlcmVkUHVibGljS2V5O1xufVxuIl19