/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/coin-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { httpGetJson } from './http-util';
class CoinPriceService {
    constructor() {
        this.symbols = [];
        this.prices = {};
        this.updateCoinPrices = (/**
         * @return {?}
         */
        () => tslib_1.__awaiter(this, void 0, void 0, function* () {
            for (let symbol of this.symbols) {
                symbol = symbol.split('_')[0];
                /** @type {?} */
                const price = yield fetchPriceOfCoin(symbol);
                if (price != undefined) {
                    this.prices[symbol] = price;
                }
            }
        }));
    }
    /**
     * @param {?} database
     * @return {?}
     */
    init(database) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const coins = yield database.getAllCoins();
            this.symbols = coins.map((/**
             * @param {?} row
             * @return {?}
             */
            row => row.symbol));
            setInterval(this.updateCoinPrices, 1000 * 60 * 60);
            yield this.updateCoinPrices();
        });
    }
    /**
     * @param {?} symbol
     * @return {?}
     */
    getPriceInUSD(symbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            symbol = symbol.split('_')[0];
            if (symbol == 'FUN') {
                return 0;
            }
            if (symbol == 'USD') {
                return 1;
            }
            /** @type {?} */
            const price = this.prices[symbol.toUpperCase()];
            if (price == undefined) {
                console.log(this.prices);
                throw new Error(symbol + ' Price is UNDEFINED!');
            }
            return price;
        });
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    CoinPriceService.prototype.symbols;
    /**
     * @type {?}
     * @private
     */
    CoinPriceService.prototype.prices;
    /**
     * @type {?}
     * @private
     */
    CoinPriceService.prototype.updateCoinPrices;
}
/** @type {?} */
let coinPriceService;
/**
 * @param {?} database
 * @return {?}
 * @this {*}
 */
export function getCoinPriceService(database) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        if (coinPriceService == undefined) {
            coinPriceService = new CoinPriceService();
            yield coinPriceService.init(database);
        }
        return coinPriceService;
    });
}
/**
 * @record
 */
function ICoinPriceData() { }
if (false) {
    /** @type {?} */
    ICoinPriceData.prototype.price;
}
/**
 * @param {?} symbol
 * @return {?}
 * @this {*}
 */
function fetchPriceOfCoin(symbol) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const url = 'internal.eosbet.io/token_price/' +
            symbol.toUpperCase();
        try {
            /** @type {?} */
            const data = yield httpGetJson(url);
            // console.log(data);
            return Number(data.price);
        }
        catch (error) {
            console.error(error);
            return undefined;
        }
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29pbi11dGlsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvdXRpbC9jb2luLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUkxQyxNQUFNLGdCQUFnQjtJQUlyQjtRQUhRLFlBQU8sR0FBYSxFQUFFLENBQUM7UUFDdkIsV0FBTSxHQUE2QixFQUFFLENBQUM7UUFtQnRDLHFCQUFnQjs7O1FBQUcsR0FBUSxFQUFFO1lBQ3BDLEtBQUssSUFBSSxNQUFNLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDaEMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7O3NCQUV4QixLQUFLLEdBQUcsTUFBTSxnQkFBZ0IsQ0FBQyxNQUFNLENBQUM7Z0JBRTVDLElBQUksS0FBSyxJQUFJLFNBQVMsRUFBRTtvQkFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLENBQUM7aUJBQzVCO2FBQ0Q7UUFDRixDQUFDLENBQUEsRUFBQTtJQXpCRCxDQUFDOzs7OztJQUVLLElBQUksQ0FBQyxRQUE4Qjs7O2tCQUNsQyxLQUFLLEdBQUcsTUFBTSxRQUFRLENBQUMsV0FBVyxFQUFFO1lBRTFDLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLEdBQUc7Ozs7WUFDdkIsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUNqQixDQUFDO1lBR0YsV0FBVyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBRW5ELE1BQU0sSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDL0IsQ0FBQztLQUFBOzs7OztJQWNLLGFBQWEsQ0FBQyxNQUFjOztZQUNqQyxNQUFNLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUc5QixJQUFJLE1BQU0sSUFBSSxLQUFLLEVBQUU7Z0JBQ3BCLE9BQU8sQ0FBQyxDQUFDO2FBQ1Q7WUFFRCxJQUFJLE1BQU0sSUFBSSxLQUFLLEVBQUU7Z0JBQ3BCLE9BQU8sQ0FBQyxDQUFDO2FBQ1Q7O2tCQUdLLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFFLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBRTtZQUVqRCxJQUFJLEtBQUssSUFBSSxTQUFTLEVBQUU7Z0JBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUV6QixNQUFNLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxzQkFBc0IsQ0FBQyxDQUFDO2FBQ2pEO1lBRUQsT0FBTyxLQUFLLENBQUM7UUFDZCxDQUFDO0tBQUE7Q0FDRDs7Ozs7O0lBdkRBLG1DQUErQjs7Ozs7SUFDL0Isa0NBQThDOzs7OztJQW1COUMsNENBVUM7OztJQTRCRSxnQkFBa0M7Ozs7OztBQUV0QyxNQUFNLFVBQWdCLG1CQUFtQixDQUFDLFFBQThCOztRQUN2RSxJQUFJLGdCQUFnQixJQUFJLFNBQVMsRUFBRTtZQUNsQyxnQkFBZ0IsR0FBRyxJQUFJLGdCQUFnQixFQUFFLENBQUM7WUFFMUMsTUFBTSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDdEM7UUFFRCxPQUFPLGdCQUFnQixDQUFDO0lBQ3pCLENBQUM7Q0FBQTs7OztBQUdELDZCQUVDOzs7SUFEQSwrQkFBYzs7Ozs7OztBQUdmLFNBQWUsZ0JBQWdCLENBQUMsTUFBYzs7O2NBQ3ZDLEdBQUcsR0FDUixpQ0FBaUM7WUFDakMsTUFBTSxDQUFDLFdBQVcsRUFBRTtRQUdyQixJQUFJOztrQkFDRyxJQUFJLEdBQUcsTUFBTSxXQUFXLENBQWlCLEdBQUcsQ0FBQztZQUNuRCxxQkFBcUI7WUFFckIsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzFCO1FBQUMsT0FBTyxLQUFLLEVBQUU7WUFDZixPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRXJCLE9BQU8sU0FBUyxDQUFDO1NBQ2pCO0lBQ0YsQ0FBQztDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgaHR0cEdldEpzb24gfSBmcm9tICcuL2h0dHAtdXRpbCc7XG5pbXBvcnQgeyBJQ3VycmVuY3lQcmljZVNlcnZpY2UsIElDb2luRGF0YWJhc2VTZXJ2aWNlIH0gZnJvbSAnLi4vY3VycmVuY3kvY3VycmVuY3ktYW1vdW50L2ludGVyZmFjZXMnO1xuXG5cbmNsYXNzIENvaW5QcmljZVNlcnZpY2UgaW1wbGVtZW50cyBJQ3VycmVuY3lQcmljZVNlcnZpY2Uge1xuXHRwcml2YXRlIHN5bWJvbHM6IHN0cmluZ1tdID0gW107XG5cdHByaXZhdGUgcHJpY2VzOiB7W2NvaW46IHN0cmluZ106IG51bWJlcn0gPSB7fTtcblxuXHRjb25zdHJ1Y3RvcigpIHtcblxuXHR9XG5cblx0YXN5bmMgaW5pdChkYXRhYmFzZTogSUNvaW5EYXRhYmFzZVNlcnZpY2UpIHtcblx0XHRjb25zdCBjb2lucyA9IGF3YWl0IGRhdGFiYXNlLmdldEFsbENvaW5zKCk7XG5cblx0XHR0aGlzLnN5bWJvbHMgPSBjb2lucy5tYXAoXG5cdFx0XHRyb3cgPT4gcm93LnN5bWJvbFxuXHRcdCk7XG5cblxuXHRcdHNldEludGVydmFsKHRoaXMudXBkYXRlQ29pblByaWNlcywgMTAwMCAqIDYwICogNjApO1xuXG5cdFx0YXdhaXQgdGhpcy51cGRhdGVDb2luUHJpY2VzKCk7XG5cdH1cblxuXHRwcml2YXRlIHVwZGF0ZUNvaW5QcmljZXMgPSBhc3luYygpID0+IHtcblx0XHRmb3IgKGxldCBzeW1ib2wgb2YgdGhpcy5zeW1ib2xzKSB7XG5cdFx0XHRzeW1ib2wgPSBzeW1ib2wuc3BsaXQoJ18nKVswXTtcblxuXHRcdFx0Y29uc3QgcHJpY2UgPSBhd2FpdCBmZXRjaFByaWNlT2ZDb2luKHN5bWJvbCk7XG5cblx0XHRcdGlmIChwcmljZSAhPSB1bmRlZmluZWQpIHtcblx0XHRcdFx0dGhpcy5wcmljZXNbc3ltYm9sXSA9IHByaWNlO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXG5cdGFzeW5jIGdldFByaWNlSW5VU0Qoc3ltYm9sOiBzdHJpbmcpOiBQcm9taXNlPG51bWJlcj4ge1xuXHRcdHN5bWJvbCA9IHN5bWJvbC5zcGxpdCgnXycpWzBdO1xuXG5cblx0XHRpZiAoc3ltYm9sID09ICdGVU4nKSB7XG5cdFx0XHRyZXR1cm4gMDtcblx0XHR9XG5cblx0XHRpZiAoc3ltYm9sID09ICdVU0QnKSB7XG5cdFx0XHRyZXR1cm4gMTtcblx0XHR9XG5cblxuXHRcdGNvbnN0IHByaWNlID0gdGhpcy5wcmljZXNbIHN5bWJvbC50b1VwcGVyQ2FzZSgpIF07XG5cblx0XHRpZiAocHJpY2UgPT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRjb25zb2xlLmxvZyh0aGlzLnByaWNlcyk7XG5cblx0XHRcdHRocm93IG5ldyBFcnJvcihzeW1ib2wgKyAnIFByaWNlIGlzIFVOREVGSU5FRCEnKTtcblx0XHR9XG5cblx0XHRyZXR1cm4gcHJpY2U7XG5cdH1cbn1cblxuXG5sZXQgY29pblByaWNlU2VydmljZTogQ29pblByaWNlU2VydmljZTtcblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldENvaW5QcmljZVNlcnZpY2UoZGF0YWJhc2U6IElDb2luRGF0YWJhc2VTZXJ2aWNlKTogUHJvbWlzZTxJQ3VycmVuY3lQcmljZVNlcnZpY2U+IHtcblx0aWYgKGNvaW5QcmljZVNlcnZpY2UgPT0gdW5kZWZpbmVkKSB7XG5cdFx0Y29pblByaWNlU2VydmljZSA9IG5ldyBDb2luUHJpY2VTZXJ2aWNlKCk7XG5cblx0XHRhd2FpdCBjb2luUHJpY2VTZXJ2aWNlLmluaXQoZGF0YWJhc2UpO1xuXHR9XG5cblx0cmV0dXJuIGNvaW5QcmljZVNlcnZpY2U7XG59XG5cblxuaW50ZXJmYWNlIElDb2luUHJpY2VEYXRhIHtcblx0cHJpY2U6IG51bWJlcjtcbn1cblxuYXN5bmMgZnVuY3Rpb24gZmV0Y2hQcmljZU9mQ29pbihzeW1ib2w6IHN0cmluZyk6IFByb21pc2U8bnVtYmVyPiB7XG5cdGNvbnN0IHVybCA9XG5cdFx0J2ludGVybmFsLmVvc2JldC5pby90b2tlbl9wcmljZS8nICtcblx0XHRzeW1ib2wudG9VcHBlckNhc2UoKSA7XG5cblxuXHR0cnkge1xuXHRcdGNvbnN0IGRhdGEgPSBhd2FpdCBodHRwR2V0SnNvbjxJQ29pblByaWNlRGF0YT4odXJsKTtcblx0XHQvLyBjb25zb2xlLmxvZyhkYXRhKTtcblxuXHRcdHJldHVybiBOdW1iZXIoZGF0YS5wcmljZSk7XG5cdH0gY2F0Y2ggKGVycm9yKSB7XG5cdFx0Y29uc29sZS5lcnJvcihlcnJvcik7XG5cblx0XHRyZXR1cm4gdW5kZWZpbmVkO1xuXHR9XG59XG4iXX0=