/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/http-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as https from 'https';
import * as http from 'http';
/**
 * @template T
 * @param {?} url
 * @return {?}
 */
export function httpsGetJson(url) {
    return new Promise((/**
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    (resolve, reject) => {
        https.get('https://' + url, (/**
         * @param {?} resp
         * @return {?}
         */
        (resp) => {
            /** @type {?} */
            let data = '';
            // A chunk of data has been recieved.
            resp.on('data', (/**
             * @param {?} chunk
             * @return {?}
             */
            (chunk) => {
                data += chunk;
            }));
            // The whole response has been received. Print out the result.
            resp.on('end', (/**
             * @return {?}
             */
            () => {
                try {
                    /** @type {?} */
                    const parsedData = JSON.parse(data);
                    resolve(parsedData);
                }
                catch (error) {
                    console.log(data);
                    reject(error);
                }
            }));
        })).on('error', (/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
            // console.log("Error: " + err.message);
            // throw err;
            reject(err);
        }));
    }));
}
/**
 * @template T
 * @param {?} url
 * @return {?}
 */
export function httpGetJson(url) {
    return new Promise((/**
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    (resolve, reject) => {
        http.get('http://' + url, (/**
         * @param {?} resp
         * @return {?}
         */
        (resp) => {
            /** @type {?} */
            let data = '';
            // A chunk of data has been recieved.
            resp.on('data', (/**
             * @param {?} chunk
             * @return {?}
             */
            (chunk) => {
                data += chunk;
            }));
            // The whole response has been received. Print out the result.
            resp.on('end', (/**
             * @return {?}
             */
            () => {
                try {
                    /** @type {?} */
                    const parsedData = JSON.parse(data);
                    resolve(parsedData);
                }
                catch (error) {
                    console.log(data);
                    reject(error);
                }
            }));
        })).on('error', (/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
            // console.log("Error: " + err.message);
            // throw err;
            reject(err);
        }));
    }));
}
/**
 * @param {?} url
 * @return {?}
 */
export function httpGet(url) {
    return new Promise((/**
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    (resolve, reject) => {
        http.get('http://' + url, (/**
         * @param {?} resp
         * @return {?}
         */
        (resp) => {
            /** @type {?} */
            let data = '';
            // A chunk of data has been recieved.
            resp.on('data', (/**
             * @param {?} chunk
             * @return {?}
             */
            (chunk) => {
                data += chunk;
            }));
            // The whole response has been received. Print out the result.
            resp.on('end', (/**
             * @return {?}
             */
            () => {
                // console.log(data);
                resolve(data);
            }));
        })).on('error', (/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
            // console.log("Error: " + err.message);
            // throw err;
            reject(err);
        }));
    }));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHR0cC11dGlsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvdXRpbC9odHRwLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUMvQixPQUFPLEtBQUssSUFBSSxNQUFNLE1BQU0sQ0FBQzs7Ozs7O0FBRzdCLE1BQU0sVUFBVSxZQUFZLENBQUksR0FBVztJQUMxQyxPQUFPLElBQUksT0FBTzs7Ozs7SUFBSyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtRQUMxQyxLQUFLLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxHQUFHOzs7O1FBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRTs7Z0JBQ2hDLElBQUksR0FBRyxFQUFFO1lBRWIscUNBQXFDO1lBQ3JDLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTTs7OztZQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7Z0JBQ3pCLElBQUksSUFBSSxLQUFLLENBQUM7WUFDZixDQUFDLEVBQUMsQ0FBQztZQUVILDhEQUE4RDtZQUM5RCxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUs7OztZQUFFLEdBQUcsRUFBRTtnQkFDbkIsSUFBSTs7MEJBQ0csVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO29CQUVuQyxPQUFPLENBQUUsVUFBVSxDQUFFLENBQUM7aUJBQ3RCO2dCQUFDLE9BQU8sS0FBSyxFQUFFO29CQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBRWxCLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDZDtZQUNGLENBQUMsRUFBQyxDQUFDO1FBQ0osQ0FBQyxFQUFDLENBQUMsRUFBRSxDQUFDLE9BQU87Ozs7UUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQ3RCLHdDQUF3QztZQUN4QyxhQUFhO1lBRWIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2IsQ0FBQyxFQUFDLENBQUM7SUFDSixDQUFDLEVBQUMsQ0FBQztBQUNKLENBQUM7Ozs7OztBQUVELE1BQU0sVUFBVSxXQUFXLENBQUksR0FBVztJQUN6QyxPQUFPLElBQUksT0FBTzs7Ozs7SUFBSyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtRQUMxQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxHQUFHOzs7O1FBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRTs7Z0JBQzlCLElBQUksR0FBRyxFQUFFO1lBRWIscUNBQXFDO1lBQ3JDLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTTs7OztZQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7Z0JBQ3pCLElBQUksSUFBSSxLQUFLLENBQUM7WUFDZixDQUFDLEVBQUMsQ0FBQztZQUVILDhEQUE4RDtZQUM5RCxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUs7OztZQUFFLEdBQUcsRUFBRTtnQkFDbkIsSUFBSTs7MEJBQ0csVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO29CQUVuQyxPQUFPLENBQUUsVUFBVSxDQUFFLENBQUM7aUJBQ3RCO2dCQUFDLE9BQU8sS0FBSyxFQUFFO29CQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBRWxCLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDZDtZQUNGLENBQUMsRUFBQyxDQUFDO1FBQ0osQ0FBQyxFQUFDLENBQUMsRUFBRSxDQUFDLE9BQU87Ozs7UUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQ3RCLHdDQUF3QztZQUN4QyxhQUFhO1lBRWIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2IsQ0FBQyxFQUFDLENBQUM7SUFDSixDQUFDLEVBQUMsQ0FBQztBQUNKLENBQUM7Ozs7O0FBRUQsTUFBTSxVQUFVLE9BQU8sQ0FBQyxHQUFXO0lBQ2xDLE9BQU8sSUFBSSxPQUFPOzs7OztJQUFVLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1FBQy9DLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLEdBQUc7Ozs7UUFBRSxDQUFDLElBQUksRUFBRSxFQUFFOztnQkFDOUIsSUFBSSxHQUFHLEVBQUU7WUFFYixxQ0FBcUM7WUFDckMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNOzs7O1lBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQkFDekIsSUFBSSxJQUFJLEtBQUssQ0FBQztZQUNmLENBQUMsRUFBQyxDQUFDO1lBRUgsOERBQThEO1lBQzlELElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSzs7O1lBQUUsR0FBRyxFQUFFO2dCQUNuQixxQkFBcUI7Z0JBRXJCLE9BQU8sQ0FBRSxJQUFJLENBQUUsQ0FBQztZQUNqQixDQUFDLEVBQUMsQ0FBQztRQUNKLENBQUMsRUFBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPOzs7O1FBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRTtZQUN0Qix3Q0FBd0M7WUFDeEMsYUFBYTtZQUViLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNiLENBQUMsRUFBQyxDQUFDO0lBQ0osQ0FBQyxFQUFDLENBQUM7QUFDSixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgaHR0cHMgZnJvbSAnaHR0cHMnO1xuaW1wb3J0ICogYXMgaHR0cCBmcm9tICdodHRwJztcblxuXG5leHBvcnQgZnVuY3Rpb24gaHR0cHNHZXRKc29uPFQ+KHVybDogc3RyaW5nKSB7XG5cdHJldHVybiBuZXcgUHJvbWlzZTxUPiggKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdGh0dHBzLmdldCgnaHR0cHM6Ly8nICsgdXJsLCAocmVzcCkgPT4ge1xuXHRcdFx0bGV0IGRhdGEgPSAnJztcblxuXHRcdFx0Ly8gQSBjaHVuayBvZiBkYXRhIGhhcyBiZWVuIHJlY2lldmVkLlxuXHRcdFx0cmVzcC5vbignZGF0YScsIChjaHVuaykgPT4ge1xuXHRcdFx0XHRkYXRhICs9IGNodW5rO1xuXHRcdFx0fSk7XG5cblx0XHRcdC8vIFRoZSB3aG9sZSByZXNwb25zZSBoYXMgYmVlbiByZWNlaXZlZC4gUHJpbnQgb3V0IHRoZSByZXN1bHQuXG5cdFx0XHRyZXNwLm9uKCdlbmQnLCAoKSA9PiB7XG5cdFx0XHRcdHRyeSB7XG5cdFx0XHRcdFx0Y29uc3QgcGFyc2VkRGF0YSA9IEpTT04ucGFyc2UoZGF0YSk7XG5cblx0XHRcdFx0XHRyZXNvbHZlKCBwYXJzZWREYXRhICk7XG5cdFx0XHRcdH0gY2F0Y2ggKGVycm9yKSB7XG5cdFx0XHRcdFx0Y29uc29sZS5sb2coZGF0YSk7XG5cblx0XHRcdFx0XHRyZWplY3QoZXJyb3IpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9KS5vbignZXJyb3InLCAoZXJyKSA9PiB7XG5cdFx0XHQvLyBjb25zb2xlLmxvZyhcIkVycm9yOiBcIiArIGVyci5tZXNzYWdlKTtcblx0XHRcdC8vIHRocm93IGVycjtcblxuXHRcdFx0cmVqZWN0KGVycik7XG5cdFx0fSk7XG5cdH0pO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaHR0cEdldEpzb248VD4odXJsOiBzdHJpbmcpIHtcblx0cmV0dXJuIG5ldyBQcm9taXNlPFQ+KCAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cdFx0aHR0cC5nZXQoJ2h0dHA6Ly8nICsgdXJsLCAocmVzcCkgPT4ge1xuXHRcdFx0bGV0IGRhdGEgPSAnJztcblxuXHRcdFx0Ly8gQSBjaHVuayBvZiBkYXRhIGhhcyBiZWVuIHJlY2lldmVkLlxuXHRcdFx0cmVzcC5vbignZGF0YScsIChjaHVuaykgPT4ge1xuXHRcdFx0XHRkYXRhICs9IGNodW5rO1xuXHRcdFx0fSk7XG5cblx0XHRcdC8vIFRoZSB3aG9sZSByZXNwb25zZSBoYXMgYmVlbiByZWNlaXZlZC4gUHJpbnQgb3V0IHRoZSByZXN1bHQuXG5cdFx0XHRyZXNwLm9uKCdlbmQnLCAoKSA9PiB7XG5cdFx0XHRcdHRyeSB7XG5cdFx0XHRcdFx0Y29uc3QgcGFyc2VkRGF0YSA9IEpTT04ucGFyc2UoZGF0YSk7XG5cblx0XHRcdFx0XHRyZXNvbHZlKCBwYXJzZWREYXRhICk7XG5cdFx0XHRcdH0gY2F0Y2ggKGVycm9yKSB7XG5cdFx0XHRcdFx0Y29uc29sZS5sb2coZGF0YSk7XG5cblx0XHRcdFx0XHRyZWplY3QoZXJyb3IpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9KS5vbignZXJyb3InLCAoZXJyKSA9PiB7XG5cdFx0XHQvLyBjb25zb2xlLmxvZyhcIkVycm9yOiBcIiArIGVyci5tZXNzYWdlKTtcblx0XHRcdC8vIHRocm93IGVycjtcblxuXHRcdFx0cmVqZWN0KGVycik7XG5cdFx0fSk7XG5cdH0pO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaHR0cEdldCh1cmw6IHN0cmluZykge1xuXHRyZXR1cm4gbmV3IFByb21pc2U8c3RyaW5nPiggKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdGh0dHAuZ2V0KCdodHRwOi8vJyArIHVybCwgKHJlc3ApID0+IHtcblx0XHRcdGxldCBkYXRhID0gJyc7XG5cblx0XHRcdC8vIEEgY2h1bmsgb2YgZGF0YSBoYXMgYmVlbiByZWNpZXZlZC5cblx0XHRcdHJlc3Aub24oJ2RhdGEnLCAoY2h1bmspID0+IHtcblx0XHRcdFx0ZGF0YSArPSBjaHVuaztcblx0XHRcdH0pO1xuXG5cdFx0XHQvLyBUaGUgd2hvbGUgcmVzcG9uc2UgaGFzIGJlZW4gcmVjZWl2ZWQuIFByaW50IG91dCB0aGUgcmVzdWx0LlxuXHRcdFx0cmVzcC5vbignZW5kJywgKCkgPT4ge1xuXHRcdFx0XHQvLyBjb25zb2xlLmxvZyhkYXRhKTtcblxuXHRcdFx0XHRyZXNvbHZlKCBkYXRhICk7XG5cdFx0XHR9KTtcblx0XHR9KS5vbignZXJyb3InLCAoZXJyKSA9PiB7XG5cdFx0XHQvLyBjb25zb2xlLmxvZyhcIkVycm9yOiBcIiArIGVyci5tZXNzYWdlKTtcblx0XHRcdC8vIHRocm93IGVycjtcblxuXHRcdFx0cmVqZWN0KGVycik7XG5cdFx0fSk7XG5cdH0pO1xufVxuIl19