/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/coin-ids.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
var CoinId = {
    BTC: 0,
    ETH: 1,
    EOS: 2,
    BET: 3,
    LTC: 4,
    XRP: 5,
    BCH: 6,
    BNB: 7,
    WAX: 8,
    TRX: 9,
    LINK: 10,
};
export { CoinId };
CoinId[CoinId.BTC] = 'BTC';
CoinId[CoinId.ETH] = 'ETH';
CoinId[CoinId.EOS] = 'EOS';
CoinId[CoinId.BET] = 'BET';
CoinId[CoinId.LTC] = 'LTC';
CoinId[CoinId.XRP] = 'XRP';
CoinId[CoinId.BCH] = 'BCH';
CoinId[CoinId.BNB] = 'BNB';
CoinId[CoinId.WAX] = 'WAX';
CoinId[CoinId.TRX] = 'TRX';
CoinId[CoinId.LINK] = 'LINK';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29pbi1pZHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9jdXJyZW5jeS9jb2luLWlkcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFDQSxJQUFZLE1BQU07SUFFZCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFFSCxHQUFHLEdBQUE7SUFDSCxJQUFJLElBQUE7RUFDUCIsInNvdXJjZXNDb250ZW50IjpbIi8qKiogSU1QT1JUQU5UOiBLRUVQIENvaW5JZHMgaW4gVEhJUyBPUkRFUiEgKioqL1xuZXhwb3J0IGVudW0gQ29pbklkXG57XG4gICAgQlRDLFxuICAgIEVUSCxcbiAgICBFT1MsXG4gICAgQkVULFxuICAgIExUQyxcbiAgICBYUlAsXG4gICAgQkNILFxuICAgIEJOQixcbiAgICBXQVgsXG5cbiAgICBUUlgsXG4gICAgTElOSyxcbn1cbi8qKiovIl19