/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/currency-amount/currency-amount-factory.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Big } from 'big.js';
import { getCoinPriceService } from '../../util/coin-util';
import { CoinDataProvider } from '../database/coin-data-provider';
import { NumberForPreciseMathBase, PreciseDecimal } from './precise-math-numbers';
var PreciseCurrencyAmount = /** @class */ (function (_super) {
    tslib_1.__extends(PreciseCurrencyAmount, _super);
    function PreciseCurrencyAmount(decimalValue, currency) {
        var _this = _super.call(this, decimalValue, currency.precision) || this;
        _this.currency = currency;
        return _this;
    }
    Object.defineProperty(PreciseCurrencyAmount.prototype, "quantity", {
        get: /**
         * @return {?}
         */
        function () {
            return this.decimal + ' ' + this.currency.symbol;
        },
        enumerable: true,
        configurable: true
    });
    return PreciseCurrencyAmount;
}(PreciseDecimal));
if (false) {
    /** @type {?} */
    PreciseCurrencyAmount.prototype.currency;
}
var PreciseCurrencyAmountWithPrice = /** @class */ (function (_super) {
    tslib_1.__extends(PreciseCurrencyAmountWithPrice, _super);
    function PreciseCurrencyAmountWithPrice(decimalValue, currency, priceInUSD) {
        var _this = _super.call(this, decimalValue, currency) || this;
        _this.priceInUSD = priceInUSD;
        return _this;
    }
    Object.defineProperty(PreciseCurrencyAmountWithPrice.prototype, "amountInUSD", {
        get: /**
         * @return {?}
         */
        function () {
            this.checkPrice();
            return (new Big(this.decimal))
                .times(this.priceInUSD)
                .round(6, 0 /* RoundDown */)
                .toString();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @private
     * @return {?}
     */
    PreciseCurrencyAmountWithPrice.prototype.checkPrice = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.priceInUSD == undefined) {
            throw new Error('Price for Currency IS NOT DEFINED!');
        }
    };
    return PreciseCurrencyAmountWithPrice;
}(PreciseCurrencyAmount));
if (false) {
    /** @type {?} */
    PreciseCurrencyAmountWithPrice.prototype.priceInUSD;
}
var MatchingCurrencyValidator = /** @class */ (function () {
    function MatchingCurrencyValidator(currency) {
        this.currency = currency;
    }
    /**
     * @param {?} __0
     * @return {?}
     */
    MatchingCurrencyValidator.prototype.isMatchingType = /**
     * @param {?} __0
     * @return {?}
     */
    function (_a) {
        var currency = _a.currency;
        if (this.currency.symbol != currency.symbol ||
            this.currency.precision != currency.precision) {
            throw new Error('both amounts must be the same currency!');
        }
        else {
            return true;
        }
    };
    return MatchingCurrencyValidator;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    MatchingCurrencyValidator.prototype.currency;
}
var PreciseCurrencyAmountFactory = /** @class */ (function () {
    function PreciseCurrencyAmountFactory(currency) {
        this.currency = currency;
        this.validator = new MatchingCurrencyValidator(currency);
    }
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    PreciseCurrencyAmountFactory.prototype.newAmountFromInteger = /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    function (integer, precision) {
        /** @type {?} */
        var number = new PreciseCurrencyAmount(new Big(integer).div(Math.pow(10, precision)), this.currency);
        this.validator.isMatchingType(number);
        return number;
    };
    /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    PreciseCurrencyAmountFactory.prototype.newAmountFromDecimal = /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    function (decimal, precision) {
        /** @type {?} */
        var number = new PreciseCurrencyAmount(decimal, this.currency);
        this.validator.isMatchingType(number);
        return number;
    };
    return PreciseCurrencyAmountFactory;
}());
if (false) {
    /** @type {?} */
    PreciseCurrencyAmountFactory.prototype.validator;
    /**
     * @type {?}
     * @private
     */
    PreciseCurrencyAmountFactory.prototype.currency;
}
var PreciseCurrencyAmountWithPriceFactory = /** @class */ (function () {
    function PreciseCurrencyAmountWithPriceFactory(currency, priceInUSD) {
        this.currency = currency;
        this.priceInUSD = priceInUSD;
        this.validator = new MatchingCurrencyValidator(currency);
    }
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    PreciseCurrencyAmountWithPriceFactory.prototype.newAmountFromInteger = /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    function (integer, precision) {
        /** @type {?} */
        var number = new PreciseCurrencyAmountWithPrice(new Big(integer).div(Math.pow(10, precision)), this.currency, this.priceInUSD);
        this.validator.isMatchingType(number);
        return number;
    };
    /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    PreciseCurrencyAmountWithPriceFactory.prototype.newAmountFromDecimal = /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    function (decimal, precision) {
        /** @type {?} */
        var number = new PreciseCurrencyAmountWithPrice(decimal, this.currency, this.priceInUSD);
        this.validator.isMatchingType(number);
        return number;
    };
    return PreciseCurrencyAmountWithPriceFactory;
}());
if (false) {
    /** @type {?} */
    PreciseCurrencyAmountWithPriceFactory.prototype.validator;
    /**
     * @type {?}
     * @private
     */
    PreciseCurrencyAmountWithPriceFactory.prototype.currency;
    /**
     * @type {?}
     * @private
     */
    PreciseCurrencyAmountWithPriceFactory.prototype.priceInUSD;
}
var CurrencyAmount = /** @class */ (function (_super) {
    tslib_1.__extends(CurrencyAmount, _super);
    function CurrencyAmount(decimalValue, currency) {
        var _this = _super.call(this, currency.precision, new Big(decimalValue).times(Math.pow(10, currency.precision)).round(0, 0 /* RoundDown */), new PreciseCurrencyAmountFactory(currency)) || this;
        _this.currency = currency;
        return _this;
    }
    Object.defineProperty(CurrencyAmount.prototype, "quantity", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.quantity;
        },
        enumerable: true,
        configurable: true
    });
    return CurrencyAmount;
}(NumberForPreciseMathBase));
if (false) {
    /** @type {?} */
    CurrencyAmount.prototype.currency;
}
var CurrencyAmountWithPrice = /** @class */ (function (_super) {
    tslib_1.__extends(CurrencyAmountWithPrice, _super);
    function CurrencyAmountWithPrice(decimalValue, currency, priceInUSD) {
        var _this = _super.call(this, currency.precision, new Big(decimalValue).times(Math.pow(10, currency.precision)).round(0, 0 /* RoundDown */), new PreciseCurrencyAmountWithPriceFactory(currency, priceInUSD)) || this;
        _this.currency = currency;
        return _this;
    }
    Object.defineProperty(CurrencyAmountWithPrice.prototype, "amountInUSD", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.amountInUSD;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CurrencyAmountWithPrice.prototype, "priceInUSD", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.priceInUSD;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CurrencyAmountWithPrice.prototype, "quantity", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.quantity;
        },
        enumerable: true,
        configurable: true
    });
    return CurrencyAmountWithPrice;
}(NumberForPreciseMathBase));
if (false) {
    /** @type {?} */
    CurrencyAmountWithPrice.prototype.currency;
}
var CurrencyAmountFactory = /** @class */ (function () {
    function CurrencyAmountFactory(coinDataProvider) {
        this.coinDataProvider = coinDataProvider;
    }
    /**
     * @param {?} quantity
     * @return {?}
     */
    CurrencyAmountFactory.prototype.newAmountFromQuantity = /**
     * @param {?} quantity
     * @return {?}
     */
    function (quantity) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, amount, tokenSymbol;
            return tslib_1.__generator(this, function (_b) {
                _a = tslib_1.__read(quantity.split(' '), 2), amount = _a[0], tokenSymbol = _a[1];
                return [2 /*return*/, this.newAmountFromDecimal(amount, tokenSymbol)];
            });
        });
    };
    /**
     * @param {?} decimalAmount
     * @param {?} tokenSymbol
     * @return {?}
     */
    CurrencyAmountFactory.prototype.newAmountFromDecimal = /**
     * @param {?} decimalAmount
     * @param {?} tokenSymbol
     * @return {?}
     */
    function (decimalAmount, tokenSymbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var data;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.coinDataProvider.getCoinDataBySymbol(tokenSymbol)];
                    case 1:
                        data = _a.sent();
                        return [2 /*return*/, new CurrencyAmount(decimalAmount, data)];
                }
            });
        });
    };
    /**
     * @param {?} decimalAmount
     * @param {?} coinId
     * @return {?}
     */
    CurrencyAmountFactory.prototype.newAmountFromDecimalAndCoinId = /**
     * @param {?} decimalAmount
     * @param {?} coinId
     * @return {?}
     */
    function (decimalAmount, coinId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var data;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.coinDataProvider.getCoinData(coinId)];
                    case 1:
                        data = _a.sent();
                        return [2 /*return*/, new CurrencyAmount(decimalAmount, data)];
                }
            });
        });
    };
    /**
     * @param {?} integerSubunits
     * @param {?} tokenSymbol
     * @return {?}
     */
    CurrencyAmountFactory.prototype.newAmountFromInteger = /**
     * @param {?} integerSubunits
     * @param {?} tokenSymbol
     * @return {?}
     */
    function (integerSubunits, tokenSymbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var data, decimalAmount;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.coinDataProvider.getCoinDataBySymbol(tokenSymbol)];
                    case 1:
                        data = _a.sent();
                        decimalAmount = new Big(integerSubunits).div(Math.pow(10, data.precision));
                        return [2 /*return*/, new CurrencyAmount(decimalAmount, data)];
                }
            });
        });
    };
    return CurrencyAmountFactory;
}());
if (false) {
    /** @type {?} */
    CurrencyAmountFactory.prototype.coinDataProvider;
}
/** @type {?} */
var currencyAmountFactory;
/**
 * @param {?} database
 * @return {?}
 * @this {*}
 */
export function getCurrencyAmountFactory(database) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var coinDataProvider;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!(currencyAmountFactory == undefined)) return [3 /*break*/, 2];
                    coinDataProvider = new CoinDataProvider(database);
                    return [4 /*yield*/, coinDataProvider.init()];
                case 1:
                    _a.sent();
                    currencyAmountFactory = new CurrencyAmountFactory(coinDataProvider);
                    _a.label = 2;
                case 2: return [2 /*return*/, currencyAmountFactory];
            }
        });
    });
}
var CurrencyAmountWithPriceFactory = /** @class */ (function () {
    function CurrencyAmountWithPriceFactory(coinDataProvider, priceService) {
        this.coinDataProvider = coinDataProvider;
        this.priceService = priceService;
    }
    /**
     * @param {?} quantity
     * @return {?}
     */
    CurrencyAmountWithPriceFactory.prototype.newAmountFromQuantity = /**
     * @param {?} quantity
     * @return {?}
     */
    function (quantity) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, amount, tokenSymbol;
            return tslib_1.__generator(this, function (_b) {
                _a = tslib_1.__read(quantity.split(' '), 2), amount = _a[0], tokenSymbol = _a[1];
                return [2 /*return*/, this.newAmountFromDecimal(amount, tokenSymbol)];
            });
        });
    };
    /**
     * @param {?} decimalAmount
     * @param {?} tokenSymbol
     * @return {?}
     */
    CurrencyAmountWithPriceFactory.prototype.newAmountFromDecimal = /**
     * @param {?} decimalAmount
     * @param {?} tokenSymbol
     * @return {?}
     */
    function (decimalAmount, tokenSymbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var data, _a, _b;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, this.coinDataProvider.getCoinDataBySymbol(tokenSymbol)];
                    case 1:
                        data = _c.sent();
                        _a = CurrencyAmountWithPrice.bind;
                        _b = [void 0, decimalAmount,
                            data];
                        return [4 /*yield*/, this.priceService.getPriceInUSD(data.symbol)];
                    case 2: return [2 /*return*/, new (_a.apply(CurrencyAmountWithPrice, _b.concat([_c.sent()])))()];
                }
            });
        });
    };
    /**
     * @param {?} decimalAmount
     * @param {?} coinId
     * @return {?}
     */
    CurrencyAmountWithPriceFactory.prototype.newAmountFromDecimalAndCoinId = /**
     * @param {?} decimalAmount
     * @param {?} coinId
     * @return {?}
     */
    function (decimalAmount, coinId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var data, _a, _b;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, this.coinDataProvider.getCoinData(coinId)];
                    case 1:
                        data = _c.sent();
                        _a = CurrencyAmountWithPrice.bind;
                        _b = [void 0, decimalAmount,
                            data];
                        return [4 /*yield*/, this.priceService.getPriceInUSD(data.symbol)];
                    case 2: return [2 /*return*/, new (_a.apply(CurrencyAmountWithPrice, _b.concat([_c.sent()])))()];
                }
            });
        });
    };
    /**
     * @param {?} integerSubunits
     * @param {?} tokenSymbol
     * @return {?}
     */
    CurrencyAmountWithPriceFactory.prototype.newAmountFromInteger = /**
     * @param {?} integerSubunits
     * @param {?} tokenSymbol
     * @return {?}
     */
    function (integerSubunits, tokenSymbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var data, decimalAmount, _a, _b;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, this.coinDataProvider.getCoinDataBySymbol(tokenSymbol)];
                    case 1:
                        data = _c.sent();
                        decimalAmount = new Big(integerSubunits).div(Math.pow(10, data.precision));
                        _a = CurrencyAmountWithPrice.bind;
                        _b = [void 0, decimalAmount,
                            data];
                        return [4 /*yield*/, this.priceService.getPriceInUSD(data.symbol)];
                    case 2: return [2 /*return*/, new (_a.apply(CurrencyAmountWithPrice, _b.concat([_c.sent()])))()];
                }
            });
        });
    };
    return CurrencyAmountWithPriceFactory;
}());
if (false) {
    /** @type {?} */
    CurrencyAmountWithPriceFactory.prototype.coinDataProvider;
    /** @type {?} */
    CurrencyAmountWithPriceFactory.prototype.priceService;
}
/** @type {?} */
var currencyAmountWithPriceFactory;
/**
 * @param {?} database
 * @param {?=} priceService
 * @return {?}
 * @this {*}
 */
export function getCurrencyAmountWithPriceFactory(database, priceService) {
    if (priceService === void 0) { priceService = undefined; }
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var coinDataProvider;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!(currencyAmountWithPriceFactory == undefined)) return [3 /*break*/, 4];
                    coinDataProvider = new CoinDataProvider(database);
                    return [4 /*yield*/, coinDataProvider.init()];
                case 1:
                    _a.sent();
                    if (!(priceService == undefined)) return [3 /*break*/, 3];
                    return [4 /*yield*/, getCoinPriceService(database)];
                case 2:
                    priceService = _a.sent();
                    _a.label = 3;
                case 3:
                    currencyAmountWithPriceFactory = new CurrencyAmountWithPriceFactory(coinDataProvider, priceService);
                    _a.label = 4;
                case 4: return [2 /*return*/, currencyAmountWithPriceFactory];
            }
        });
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VycmVuY3ktYW1vdW50LWZhY3RvcnkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9jdXJyZW5jeS9jdXJyZW5jeS1hbW91bnQvY3VycmVuY3ktYW1vdW50LWZhY3RvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLEdBQUcsRUFBMkIsTUFBTSxRQUFRLENBQUM7QUFJdEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFM0QsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sZ0NBQWdDLENBQUM7QUFDaEUsT0FBTyxFQUFDLHdCQUF3QixFQUFFLGNBQWMsRUFBQyxNQUFNLHdCQUF3QixDQUFDO0FBSWhGO0lBQW9DLGlEQUFjO0lBQ2pELCtCQUNDLFlBQXVCLEVBQ2QsUUFBbUI7UUFGN0IsWUFJQyxrQkFBTSxZQUFZLEVBQUUsUUFBUSxDQUFDLFNBQVMsQ0FBQyxTQUN2QztRQUhTLGNBQVEsR0FBUixRQUFRLENBQVc7O0lBRzdCLENBQUM7SUFFRCxzQkFBSSwyQ0FBUTs7OztRQUFaO1lBQ0MsT0FBTyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztRQUNsRCxDQUFDOzs7T0FBQTtJQUNGLDRCQUFDO0FBQUQsQ0FBQyxBQVhELENBQW9DLGNBQWMsR0FXakQ7OztJQVJDLHlDQUE0Qjs7QUFVOUI7SUFBNkMsMERBQXFCO0lBQ2pFLHdDQUNDLFlBQXVCLEVBQ3ZCLFFBQW1CLEVBQ1YsVUFBa0I7UUFINUIsWUFLQyxrQkFBTSxZQUFZLEVBQUUsUUFBUSxDQUFDLFNBQzdCO1FBSFMsZ0JBQVUsR0FBVixVQUFVLENBQVE7O0lBRzVCLENBQUM7SUFFRCxzQkFBSSx1REFBVzs7OztRQUFmO1lBQ0MsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBRWxCLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzVCLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO2lCQUN0QixLQUFLLENBQUMsQ0FBQyxvQkFBeUI7aUJBQ2hDLFFBQVEsRUFBRSxDQUFDO1FBQ2QsQ0FBQzs7O09BQUE7Ozs7O0lBRU8sbURBQVU7Ozs7SUFBbEI7UUFDQyxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksU0FBUyxFQUFFO1lBQ2pDLE1BQU0sSUFBSSxLQUFLLENBQUMsb0NBQW9DLENBQUMsQ0FBQztTQUN0RDtJQUNGLENBQUM7SUFDRixxQ0FBQztBQUFELENBQUMsQUF2QkQsQ0FBNkMscUJBQXFCLEdBdUJqRTs7O0lBbkJDLG9EQUEyQjs7QUFzQjdCO0lBQ0MsbUNBQW9CLFFBQW1CO1FBQW5CLGFBQVEsR0FBUixRQUFRLENBQVc7SUFDdkMsQ0FBQzs7Ozs7SUFFRCxrREFBYzs7OztJQUFkLFVBQWUsRUFBMkI7WUFBMUIsc0JBQVE7UUFDdkIsSUFDQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsTUFBTTtZQUN2QyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsSUFBSSxRQUFRLENBQUMsU0FBUyxFQUM1QztZQUNELE1BQU0sSUFBSSxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQztTQUMzRDthQUFNO1lBQ04sT0FBTyxJQUFJLENBQUM7U0FDWjtJQUNGLENBQUM7SUFDRixnQ0FBQztBQUFELENBQUMsQUFkRCxJQWNDOzs7Ozs7SUFiWSw2Q0FBMkI7O0FBZXhDO0lBR0Msc0NBQTZCLFFBQW1CO1FBQW5CLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDL0MsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLHlCQUF5QixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzFELENBQUM7Ozs7OztJQUVELDJEQUFvQjs7Ozs7SUFBcEIsVUFBcUIsT0FBa0IsRUFBRSxTQUFpQjs7WUFDbkQsTUFBTSxHQUFHLElBQUkscUJBQXFCLENBQ3ZDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FDbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsU0FBUyxDQUFDLENBQ3ZCLEVBQ0QsSUFBSSxDQUFDLFFBQVEsQ0FDYjtRQUVELElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXRDLE9BQU8sTUFBTSxDQUFDO0lBQ2YsQ0FBQzs7Ozs7O0lBQ0QsMkRBQW9COzs7OztJQUFwQixVQUFxQixPQUFrQixFQUFFLFNBQWlCOztZQUNuRCxNQUFNLEdBQUcsSUFBSSxxQkFBcUIsQ0FDdkMsT0FBTyxFQUNQLElBQUksQ0FBQyxRQUFRLENBQ2I7UUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUV0QyxPQUFPLE1BQU0sQ0FBQztJQUNmLENBQUM7SUFDRixtQ0FBQztBQUFELENBQUMsQUE3QkQsSUE2QkM7OztJQTVCQSxpREFBeUU7Ozs7O0lBRTdELGdEQUFvQzs7QUE0QmpEO0lBR0MsK0NBQ2tCLFFBQW1CLEVBQ25CLFVBQWtCO1FBRGxCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIsZUFBVSxHQUFWLFVBQVUsQ0FBUTtRQUVuQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUkseUJBQXlCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDMUQsQ0FBQzs7Ozs7O0lBRUQsb0VBQW9COzs7OztJQUFwQixVQUFxQixPQUFrQixFQUFFLFNBQWlCOztZQUNuRCxNQUFNLEdBQUcsSUFBSSw4QkFBOEIsQ0FDaEQsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsQ0FDdkIsRUFDRCxJQUFJLENBQUMsUUFBUSxFQUNiLElBQUksQ0FBQyxVQUFVLENBQ2Y7UUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUV0QyxPQUFPLE1BQU0sQ0FBQztJQUNmLENBQUM7Ozs7OztJQUNELG9FQUFvQjs7Ozs7SUFBcEIsVUFBcUIsT0FBa0IsRUFBRSxTQUFpQjs7WUFDbkQsTUFBTSxHQUFHLElBQUksOEJBQThCLENBQ2hELE9BQU8sRUFDUCxJQUFJLENBQUMsUUFBUSxFQUNiLElBQUksQ0FBQyxVQUFVLENBQ2Y7UUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUV0QyxPQUFPLE1BQU0sQ0FBQztJQUNmLENBQUM7SUFDRiw0Q0FBQztBQUFELENBQUMsQUFsQ0QsSUFrQ0M7OztJQWpDQSwwREFBeUU7Ozs7O0lBR3hFLHlEQUFvQzs7Ozs7SUFDcEMsMkRBQW1DOztBQWdDckM7SUFBNkIsMENBQWdEO0lBRTVFLHdCQUNDLFlBQXVCLEVBQ2QsUUFBbUI7UUFGN0IsWUFJQyxrQkFDQyxRQUFRLENBQUMsU0FBUyxFQUNsQixJQUFJLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxLQUFLLENBQzFCLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FDaEMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxvQkFBeUIsRUFDbEMsSUFBSSw0QkFBNEIsQ0FBQyxRQUFRLENBQUMsQ0FDMUMsU0FDRDtRQVRTLGNBQVEsR0FBUixRQUFRLENBQVc7O0lBUzdCLENBQUM7SUFFRCxzQkFBSSxvQ0FBUTs7OztRQUFaO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUM3QixDQUFDOzs7T0FBQTtJQUNGLHFCQUFDO0FBQUQsQ0FBQyxBQWxCRCxDQUE2Qix3QkFBd0IsR0FrQnBEOzs7SUFkQyxrQ0FBNEI7O0FBZ0I5QjtJQUFzQyxtREFBeUQ7SUFFOUYsaUNBQ0MsWUFBdUIsRUFDZCxRQUFtQixFQUM1QixVQUFrQjtRQUhuQixZQUtDLGtCQUNDLFFBQVEsQ0FBQyxTQUFTLEVBQ2xCLElBQUksR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FDMUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUNoQyxDQUFDLEtBQUssQ0FBQyxDQUFDLG9CQUF5QixFQUNsQyxJQUFJLHFDQUFxQyxDQUN4QyxRQUFRLEVBQUUsVUFBVSxDQUNwQixDQUNELFNBQ0Q7UUFaUyxjQUFRLEdBQVIsUUFBUSxDQUFXOztJQVk3QixDQUFDO0lBRUQsc0JBQUksZ0RBQVc7Ozs7UUFBZjtZQUNDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUM7UUFDaEMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwrQ0FBVTs7OztRQUFkO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztRQUMvQixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDZDQUFROzs7O1FBQVo7WUFDQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQzdCLENBQUM7OztPQUFBO0lBQ0YsOEJBQUM7QUFBRCxDQUFDLEFBN0JELENBQXNDLHdCQUF3QixHQTZCN0Q7OztJQXpCQywyQ0FBNEI7O0FBOEI5QjtJQUNDLCtCQUNVLGdCQUFtQztRQUFuQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQW1CO0lBQzFDLENBQUM7Ozs7O0lBRUUscURBQXFCOzs7O0lBQTNCLFVBQTRCLFFBQWdCOzs7O2dCQUNyQyxLQUFBLGVBQXdCLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUEsRUFBMUMsTUFBTSxRQUFBLEVBQUUsV0FBVyxRQUFBO2dCQUUxQixzQkFBTyxJQUFJLENBQUMsb0JBQW9CLENBQy9CLE1BQU0sRUFDTixXQUFXLENBQ1gsRUFBQzs7O0tBQ0Y7Ozs7OztJQUVLLG9EQUFvQjs7Ozs7SUFBMUIsVUFBMkIsYUFBd0IsRUFBRSxXQUFtQjs7Ozs7NEJBQzFELHFCQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsRUFBQTs7d0JBQW5FLElBQUksR0FBRyxTQUE0RDt3QkFFekUsc0JBQU8sSUFBSSxjQUFjLENBQ3hCLGFBQWEsRUFDYixJQUFJLENBQ0osRUFBQzs7OztLQUNGOzs7Ozs7SUFFSyw2REFBNkI7Ozs7O0lBQW5DLFVBQW9DLGFBQXdCLEVBQUUsTUFBYzs7Ozs7NEJBQzlELHFCQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUF0RCxJQUFJLEdBQUcsU0FBK0M7d0JBRTVELHNCQUFPLElBQUksY0FBYyxDQUN4QixhQUFhLEVBQ2IsSUFBSSxDQUNKLEVBQUM7Ozs7S0FDRjs7Ozs7O0lBRUssb0RBQW9COzs7OztJQUExQixVQUEyQixlQUEwQixFQUFFLFdBQW1COzs7Ozs0QkFDNUQscUJBQU0sSUFBSSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxFQUFBOzt3QkFBbkUsSUFBSSxHQUFHLFNBQTREO3dCQUVuRSxhQUFhLEdBQUcsSUFBSSxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsR0FBRyxDQUNqRCxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQzVCO3dCQUVELHNCQUFPLElBQUksY0FBYyxDQUN4QixhQUFhLEVBQ2IsSUFBSSxDQUNKLEVBQUM7Ozs7S0FDRjtJQUNGLDRCQUFDO0FBQUQsQ0FBQyxBQTVDRCxJQTRDQzs7O0lBMUNDLGlEQUE0Qzs7O0lBNEMxQyxxQkFBNEM7Ozs7OztBQUdoRCxNQUFNLFVBQWdCLHdCQUF3QixDQUM3QyxRQUE4Qjs7Ozs7O3lCQUUxQixDQUFBLHFCQUFxQixJQUFJLFNBQVMsQ0FBQSxFQUFsQyx3QkFBa0M7b0JBRS9CLGdCQUFnQixHQUFHLElBQUksZ0JBQWdCLENBQzVDLFFBQVEsQ0FDUjtvQkFFRCxxQkFBTSxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsRUFBQTs7b0JBQTdCLFNBQTZCLENBQUM7b0JBRTlCLHFCQUFxQixHQUFHLElBQUkscUJBQXFCLENBQ2hELGdCQUFnQixDQUNoQixDQUFDOzt3QkFHSCxzQkFBTyxxQkFBcUIsRUFBQzs7OztDQUM3QjtBQUlEO0lBQ0Msd0NBQ1UsZ0JBQW1DLEVBQ25DLFlBQW1DO1FBRG5DLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBbUI7UUFDbkMsaUJBQVksR0FBWixZQUFZLENBQXVCO0lBRTdDLENBQUM7Ozs7O0lBRUssOERBQXFCOzs7O0lBQTNCLFVBQTRCLFFBQWdCOzs7O2dCQUNyQyxLQUFBLGVBQXdCLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUEsRUFBMUMsTUFBTSxRQUFBLEVBQUUsV0FBVyxRQUFBO2dCQUUxQixzQkFBTyxJQUFJLENBQUMsb0JBQW9CLENBQy9CLE1BQU0sRUFDTixXQUFXLENBQ1gsRUFBQzs7O0tBQ0Y7Ozs7OztJQUVLLDZEQUFvQjs7Ozs7SUFBMUIsVUFBMkIsYUFBd0IsRUFBRSxXQUFtQjs7Ozs7NEJBQzFELHFCQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsRUFBQTs7d0JBQW5FLElBQUksR0FBRyxTQUE0RDs2QkFFOUQsdUJBQXVCO3NDQUNqQyxhQUFhOzRCQUNiLElBQUk7d0JBQ0oscUJBQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzRCQUhuRCxzQkFBTyxjQUFJLHVCQUF1QixhQUdqQyxTQUFrRCxNQUNsRCxFQUFDOzs7O0tBQ0Y7Ozs7OztJQUVLLHNFQUE2Qjs7Ozs7SUFBbkMsVUFBb0MsYUFBd0IsRUFBRSxNQUFjOzs7Ozs0QkFDOUQscUJBQU0sSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQXRELElBQUksR0FBRyxTQUErQzs2QkFFakQsdUJBQXVCO3NDQUNqQyxhQUFhOzRCQUNiLElBQUk7d0JBQ0oscUJBQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzRCQUhuRCxzQkFBTyxjQUFJLHVCQUF1QixhQUdqQyxTQUFrRCxNQUNsRCxFQUFDOzs7O0tBQ0Y7Ozs7OztJQUVLLDZEQUFvQjs7Ozs7SUFBMUIsVUFBMkIsZUFBMEIsRUFBRSxXQUFtQjs7Ozs7NEJBQzVELHFCQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsRUFBQTs7d0JBQW5FLElBQUksR0FBRyxTQUE0RDt3QkFFbkUsYUFBYSxHQUFHLElBQUksR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEdBQUcsQ0FDakQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUM1Qjs2QkFFVSx1QkFBdUI7c0NBQ2pDLGFBQWE7NEJBQ2IsSUFBSTt3QkFDSixxQkFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUE7NEJBSG5ELHNCQUFPLGNBQUksdUJBQXVCLGFBR2pDLFNBQWtELE1BQ2xELEVBQUM7Ozs7S0FDRjtJQUNGLHFDQUFDO0FBQUQsQ0FBQyxBQWpERCxJQWlEQzs7O0lBL0NDLDBEQUE0Qzs7SUFDNUMsc0RBQTRDOzs7SUFpRDFDLDhCQUE4RDs7Ozs7OztBQUdsRSxNQUFNLFVBQWdCLGlDQUFpQyxDQUN0RCxRQUE4QixFQUM5QixZQUErQztJQUEvQyw2QkFBQSxFQUFBLHdCQUErQzs7Ozs7O3lCQUUzQyxDQUFBLDhCQUE4QixJQUFJLFNBQVMsQ0FBQSxFQUEzQyx3QkFBMkM7b0JBRXhDLGdCQUFnQixHQUFHLElBQUksZ0JBQWdCLENBQzVDLFFBQVEsQ0FDUjtvQkFFRCxxQkFBTSxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsRUFBQTs7b0JBQTdCLFNBQTZCLENBQUM7eUJBRTFCLENBQUEsWUFBWSxJQUFJLFNBQVMsQ0FBQSxFQUF6Qix3QkFBeUI7b0JBQ2IscUJBQU0sbUJBQW1CLENBQUMsUUFBUSxDQUFDLEVBQUE7O29CQUFsRCxZQUFZLEdBQUcsU0FBbUMsQ0FBQzs7O29CQUdwRCw4QkFBOEIsR0FBRyxJQUFJLDhCQUE4QixDQUNsRSxnQkFBZ0IsRUFDaEIsWUFBWSxDQUNaLENBQUM7O3dCQUdILHNCQUFPLDhCQUE4QixFQUFDOzs7O0NBQ3RDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQmlnLCBSb3VuZGluZ01vZGUsIEJpZ1NvdXJjZSB9IGZyb20gJ2JpZy5qcyc7XG5cbmltcG9ydCB7IElDdXJyZW5jeVByaWNlU2VydmljZSwgSUN1cnJlbmN5QW1vdW50LCBJQ3VycmVuY3ksIElDb2luRGF0YVByb3ZpZGVyLCBJQ3VycmVuY3lBbW91bnRGYWN0b3J5LCBJQ3VycmVuY3lBbW91bnRXaXRoUHJpY2UsIElDdXJyZW5jeUFtb3VudFdpdGhQcmljZUZhY3RvcnksIElNYXRjaGluZ051bWJlclR5cGVWYWxpZGF0b3IsIElQcmVjaXNlTnVtYmVyRmFjdG9yeSwgSVByZWNpc2VDdXJyZW5jeUFtb3VudCwgSVByZWNpc2VDdXJyZW5jeUFtb3VudFdpdGhQcmljZSB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBDb2luSWQgfSBmcm9tICcuLi9kYXRhYmFzZS9jb2lucyc7XG5pbXBvcnQgeyBnZXRDb2luUHJpY2VTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vdXRpbC9jb2luLXV0aWwnO1xuaW1wb3J0IHtJQ29pbkRhdGFiYXNlU2VydmljZX0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7Q29pbkRhdGFQcm92aWRlcn0gZnJvbSAnLi4vZGF0YWJhc2UvY29pbi1kYXRhLXByb3ZpZGVyJztcbmltcG9ydCB7TnVtYmVyRm9yUHJlY2lzZU1hdGhCYXNlLCBQcmVjaXNlRGVjaW1hbH0gZnJvbSAnLi9wcmVjaXNlLW1hdGgtbnVtYmVycyc7XG5cblxuXG5jbGFzcyBQcmVjaXNlQ3VycmVuY3lBbW91bnQgZXh0ZW5kcyBQcmVjaXNlRGVjaW1hbCBpbXBsZW1lbnRzIElQcmVjaXNlQ3VycmVuY3lBbW91bnQge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRkZWNpbWFsVmFsdWU6IEJpZ1NvdXJjZSxcblx0XHRyZWFkb25seSBjdXJyZW5jeTogSUN1cnJlbmN5LFxuXHQpIHtcblx0XHRzdXBlcihkZWNpbWFsVmFsdWUsIGN1cnJlbmN5LnByZWNpc2lvbik7XG5cdH1cblxuXHRnZXQgcXVhbnRpdHkoKSB7XG5cdFx0cmV0dXJuIHRoaXMuZGVjaW1hbCArICcgJyArIHRoaXMuY3VycmVuY3kuc3ltYm9sO1xuXHR9XG59XG5cbmNsYXNzIFByZWNpc2VDdXJyZW5jeUFtb3VudFdpdGhQcmljZSBleHRlbmRzIFByZWNpc2VDdXJyZW5jeUFtb3VudCBpbXBsZW1lbnRzIElQcmVjaXNlQ3VycmVuY3lBbW91bnRXaXRoUHJpY2Uge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRkZWNpbWFsVmFsdWU6IEJpZ1NvdXJjZSxcblx0XHRjdXJyZW5jeTogSUN1cnJlbmN5LFxuXHRcdHJlYWRvbmx5IHByaWNlSW5VU0Q6IG51bWJlcixcblx0KSB7XG5cdFx0c3VwZXIoZGVjaW1hbFZhbHVlLCBjdXJyZW5jeSk7XG5cdH1cblxuXHRnZXQgYW1vdW50SW5VU0QoKSB7XG5cdFx0dGhpcy5jaGVja1ByaWNlKCk7XG5cblx0XHRyZXR1cm4gKG5ldyBCaWcodGhpcy5kZWNpbWFsKSlcblx0XHRcdC50aW1lcyh0aGlzLnByaWNlSW5VU0QpXG5cdFx0XHQucm91bmQoNiwgUm91bmRpbmdNb2RlLlJvdW5kRG93bilcblx0XHRcdC50b1N0cmluZygpO1xuXHR9XG5cblx0cHJpdmF0ZSBjaGVja1ByaWNlKCkge1xuXHRcdGlmICh0aGlzLnByaWNlSW5VU0QgPT0gdW5kZWZpbmVkKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ1ByaWNlIGZvciBDdXJyZW5jeSBJUyBOT1QgREVGSU5FRCEnKTtcblx0XHR9XG5cdH1cbn1cblxuXG5jbGFzcyBNYXRjaGluZ0N1cnJlbmN5VmFsaWRhdG9yIGltcGxlbWVudHMgSU1hdGNoaW5nTnVtYmVyVHlwZVZhbGlkYXRvcjxJUHJlY2lzZUN1cnJlbmN5QW1vdW50PiB7XG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgY3VycmVuY3k6IElDdXJyZW5jeSkge1xuXHR9XG5cblx0aXNNYXRjaGluZ1R5cGUoe2N1cnJlbmN5fTogSUN1cnJlbmN5QW1vdW50KTogYm9vbGVhbiB7XG5cdFx0aWYgKFxuXHRcdFx0dGhpcy5jdXJyZW5jeS5zeW1ib2wgIT0gY3VycmVuY3kuc3ltYm9sIHx8XG5cdFx0XHR0aGlzLmN1cnJlbmN5LnByZWNpc2lvbiAhPSBjdXJyZW5jeS5wcmVjaXNpb25cblx0XHQpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignYm90aCBhbW91bnRzIG11c3QgYmUgdGhlIHNhbWUgY3VycmVuY3khJyk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH1cblx0fVxufVxuXG5jbGFzcyBQcmVjaXNlQ3VycmVuY3lBbW91bnRGYWN0b3J5IGltcGxlbWVudHMgSVByZWNpc2VOdW1iZXJGYWN0b3J5PElQcmVjaXNlQ3VycmVuY3lBbW91bnQ+IHtcblx0cmVhZG9ubHkgdmFsaWRhdG9yOiBJTWF0Y2hpbmdOdW1iZXJUeXBlVmFsaWRhdG9yPElQcmVjaXNlQ3VycmVuY3lBbW91bnQ+O1xuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcmVhZG9ubHkgY3VycmVuY3k6IElDdXJyZW5jeSkge1xuXHRcdHRoaXMudmFsaWRhdG9yID0gbmV3IE1hdGNoaW5nQ3VycmVuY3lWYWxpZGF0b3IoY3VycmVuY3kpO1xuXHR9XG5cblx0bmV3QW1vdW50RnJvbUludGVnZXIoaW50ZWdlcjogQmlnU291cmNlLCBwcmVjaXNpb246IG51bWJlcik6IElQcmVjaXNlQ3VycmVuY3lBbW91bnQge1xuXHRcdGNvbnN0IG51bWJlciA9IG5ldyBQcmVjaXNlQ3VycmVuY3lBbW91bnQoXG5cdFx0XHRuZXcgQmlnKGludGVnZXIpLmRpdihcblx0XHRcdFx0TWF0aC5wb3coMTAsIHByZWNpc2lvbilcblx0XHRcdCksXG5cdFx0XHR0aGlzLmN1cnJlbmN5XG5cdFx0KTtcblxuXHRcdHRoaXMudmFsaWRhdG9yLmlzTWF0Y2hpbmdUeXBlKG51bWJlcik7XG5cblx0XHRyZXR1cm4gbnVtYmVyO1xuXHR9XG5cdG5ld0Ftb3VudEZyb21EZWNpbWFsKGRlY2ltYWw6IEJpZ1NvdXJjZSwgcHJlY2lzaW9uOiBudW1iZXIpOiBJUHJlY2lzZUN1cnJlbmN5QW1vdW50IHtcblx0XHRjb25zdCBudW1iZXIgPSBuZXcgUHJlY2lzZUN1cnJlbmN5QW1vdW50KFxuXHRcdFx0ZGVjaW1hbCxcblx0XHRcdHRoaXMuY3VycmVuY3lcblx0XHQpO1xuXG5cdFx0dGhpcy52YWxpZGF0b3IuaXNNYXRjaGluZ1R5cGUobnVtYmVyKTtcblxuXHRcdHJldHVybiBudW1iZXI7XG5cdH1cbn1cblxuY2xhc3MgUHJlY2lzZUN1cnJlbmN5QW1vdW50V2l0aFByaWNlRmFjdG9yeSBpbXBsZW1lbnRzIElQcmVjaXNlTnVtYmVyRmFjdG9yeTxJUHJlY2lzZUN1cnJlbmN5QW1vdW50V2l0aFByaWNlPiB7XG5cdHJlYWRvbmx5IHZhbGlkYXRvcjogSU1hdGNoaW5nTnVtYmVyVHlwZVZhbGlkYXRvcjxJUHJlY2lzZUN1cnJlbmN5QW1vdW50PjtcblxuXHRjb25zdHJ1Y3Rvcihcblx0XHRwcml2YXRlIHJlYWRvbmx5IGN1cnJlbmN5OiBJQ3VycmVuY3ksXG5cdFx0cHJpdmF0ZSByZWFkb25seSBwcmljZUluVVNEOiBudW1iZXIsXG5cdCkge1xuXHRcdHRoaXMudmFsaWRhdG9yID0gbmV3IE1hdGNoaW5nQ3VycmVuY3lWYWxpZGF0b3IoY3VycmVuY3kpO1xuXHR9XG5cblx0bmV3QW1vdW50RnJvbUludGVnZXIoaW50ZWdlcjogQmlnU291cmNlLCBwcmVjaXNpb246IG51bWJlcik6IElQcmVjaXNlQ3VycmVuY3lBbW91bnRXaXRoUHJpY2Uge1xuXHRcdGNvbnN0IG51bWJlciA9IG5ldyBQcmVjaXNlQ3VycmVuY3lBbW91bnRXaXRoUHJpY2UoXG5cdFx0XHRuZXcgQmlnKGludGVnZXIpLmRpdihcblx0XHRcdFx0TWF0aC5wb3coMTAsIHByZWNpc2lvbilcblx0XHRcdCksXG5cdFx0XHR0aGlzLmN1cnJlbmN5LFxuXHRcdFx0dGhpcy5wcmljZUluVVNEXG5cdFx0KTtcblxuXHRcdHRoaXMudmFsaWRhdG9yLmlzTWF0Y2hpbmdUeXBlKG51bWJlcik7XG5cblx0XHRyZXR1cm4gbnVtYmVyO1xuXHR9XG5cdG5ld0Ftb3VudEZyb21EZWNpbWFsKGRlY2ltYWw6IEJpZ1NvdXJjZSwgcHJlY2lzaW9uOiBudW1iZXIpOiBJUHJlY2lzZUN1cnJlbmN5QW1vdW50V2l0aFByaWNlIHtcblx0XHRjb25zdCBudW1iZXIgPSBuZXcgUHJlY2lzZUN1cnJlbmN5QW1vdW50V2l0aFByaWNlKFxuXHRcdFx0ZGVjaW1hbCxcblx0XHRcdHRoaXMuY3VycmVuY3ksXG5cdFx0XHR0aGlzLnByaWNlSW5VU0Rcblx0XHQpO1xuXG5cdFx0dGhpcy52YWxpZGF0b3IuaXNNYXRjaGluZ1R5cGUobnVtYmVyKTtcblxuXHRcdHJldHVybiBudW1iZXI7XG5cdH1cbn1cblxuXG5jbGFzcyBDdXJyZW5jeUFtb3VudCBleHRlbmRzIE51bWJlckZvclByZWNpc2VNYXRoQmFzZTxJUHJlY2lzZUN1cnJlbmN5QW1vdW50PlxuXHRpbXBsZW1lbnRzIElDdXJyZW5jeUFtb3VudCB7XG5cdGNvbnN0cnVjdG9yKFxuXHRcdGRlY2ltYWxWYWx1ZTogQmlnU291cmNlLFxuXHRcdHJlYWRvbmx5IGN1cnJlbmN5OiBJQ3VycmVuY3ksXG5cdCkge1xuXHRcdHN1cGVyKFxuXHRcdFx0Y3VycmVuY3kucHJlY2lzaW9uLFxuXHRcdFx0bmV3IEJpZyhkZWNpbWFsVmFsdWUpLnRpbWVzKFxuXHRcdFx0XHRNYXRoLnBvdygxMCwgY3VycmVuY3kucHJlY2lzaW9uKVxuXHRcdFx0KS5yb3VuZCgwLCBSb3VuZGluZ01vZGUuUm91bmREb3duKSxcblx0XHRcdG5ldyBQcmVjaXNlQ3VycmVuY3lBbW91bnRGYWN0b3J5KGN1cnJlbmN5KVxuXHRcdCk7XG5cdH1cblxuXHRnZXQgcXVhbnRpdHkoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLnF1YW50aXR5O1xuXHR9XG59XG5cbmNsYXNzIEN1cnJlbmN5QW1vdW50V2l0aFByaWNlIGV4dGVuZHMgTnVtYmVyRm9yUHJlY2lzZU1hdGhCYXNlPElQcmVjaXNlQ3VycmVuY3lBbW91bnRXaXRoUHJpY2U+XG5cdGltcGxlbWVudHMgSUN1cnJlbmN5QW1vdW50V2l0aFByaWNlIHtcblx0Y29uc3RydWN0b3IoXG5cdFx0ZGVjaW1hbFZhbHVlOiBCaWdTb3VyY2UsXG5cdFx0cmVhZG9ubHkgY3VycmVuY3k6IElDdXJyZW5jeSxcblx0XHRwcmljZUluVVNEOiBudW1iZXIsXG5cdCkge1xuXHRcdHN1cGVyKFxuXHRcdFx0Y3VycmVuY3kucHJlY2lzaW9uLFxuXHRcdFx0bmV3IEJpZyhkZWNpbWFsVmFsdWUpLnRpbWVzKFxuXHRcdFx0XHRNYXRoLnBvdygxMCwgY3VycmVuY3kucHJlY2lzaW9uKVxuXHRcdFx0KS5yb3VuZCgwLCBSb3VuZGluZ01vZGUuUm91bmREb3duKSxcblx0XHRcdG5ldyBQcmVjaXNlQ3VycmVuY3lBbW91bnRXaXRoUHJpY2VGYWN0b3J5KFxuXHRcdFx0XHRjdXJyZW5jeSwgcHJpY2VJblVTRFxuXHRcdFx0KVxuXHRcdCk7XG5cdH1cblxuXHRnZXQgYW1vdW50SW5VU0QoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmFtb3VudEluVVNEO1xuXHR9XG5cblx0Z2V0IHByaWNlSW5VU0QoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLnByaWNlSW5VU0Q7XG5cdH1cblxuXHRnZXQgcXVhbnRpdHkoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLnF1YW50aXR5O1xuXHR9XG59XG5cblxuXG5cbmNsYXNzIEN1cnJlbmN5QW1vdW50RmFjdG9yeSBpbXBsZW1lbnRzIElDdXJyZW5jeUFtb3VudEZhY3Rvcnkge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRyZWFkb25seSBjb2luRGF0YVByb3ZpZGVyOiBJQ29pbkRhdGFQcm92aWRlclxuXHQpIHt9XG5cblx0YXN5bmMgbmV3QW1vdW50RnJvbVF1YW50aXR5KHF1YW50aXR5OiBzdHJpbmcpOiBQcm9taXNlPEN1cnJlbmN5QW1vdW50PiB7XG5cdFx0Y29uc3QgW2Ftb3VudCwgdG9rZW5TeW1ib2xdID0gcXVhbnRpdHkuc3BsaXQoJyAnKTtcblxuXHRcdHJldHVybiB0aGlzLm5ld0Ftb3VudEZyb21EZWNpbWFsKFxuXHRcdFx0YW1vdW50LFxuXHRcdFx0dG9rZW5TeW1ib2xcblx0XHQpO1xuXHR9XG5cblx0YXN5bmMgbmV3QW1vdW50RnJvbURlY2ltYWwoZGVjaW1hbEFtb3VudDogQmlnU291cmNlLCB0b2tlblN5bWJvbDogc3RyaW5nKTogUHJvbWlzZTxDdXJyZW5jeUFtb3VudD4ge1xuXHRcdGNvbnN0IGRhdGEgPSBhd2FpdCB0aGlzLmNvaW5EYXRhUHJvdmlkZXIuZ2V0Q29pbkRhdGFCeVN5bWJvbCh0b2tlblN5bWJvbCk7XG5cblx0XHRyZXR1cm4gbmV3IEN1cnJlbmN5QW1vdW50KFxuXHRcdFx0ZGVjaW1hbEFtb3VudCxcblx0XHRcdGRhdGFcblx0XHQpO1xuXHR9XG5cblx0YXN5bmMgbmV3QW1vdW50RnJvbURlY2ltYWxBbmRDb2luSWQoZGVjaW1hbEFtb3VudDogQmlnU291cmNlLCBjb2luSWQ6IENvaW5JZCk6IFByb21pc2U8Q3VycmVuY3lBbW91bnQ+IHtcblx0XHRjb25zdCBkYXRhID0gYXdhaXQgdGhpcy5jb2luRGF0YVByb3ZpZGVyLmdldENvaW5EYXRhKGNvaW5JZCk7XG5cblx0XHRyZXR1cm4gbmV3IEN1cnJlbmN5QW1vdW50KFxuXHRcdFx0ZGVjaW1hbEFtb3VudCxcblx0XHRcdGRhdGFcblx0XHQpO1xuXHR9XG5cblx0YXN5bmMgbmV3QW1vdW50RnJvbUludGVnZXIoaW50ZWdlclN1YnVuaXRzOiBCaWdTb3VyY2UsIHRva2VuU3ltYm9sOiBzdHJpbmcpOiBQcm9taXNlPEN1cnJlbmN5QW1vdW50PiB7XG5cdFx0Y29uc3QgZGF0YSA9IGF3YWl0IHRoaXMuY29pbkRhdGFQcm92aWRlci5nZXRDb2luRGF0YUJ5U3ltYm9sKHRva2VuU3ltYm9sKTtcblxuXHRcdGNvbnN0IGRlY2ltYWxBbW91bnQgPSBuZXcgQmlnKGludGVnZXJTdWJ1bml0cykuZGl2KFxuXHRcdFx0TWF0aC5wb3coMTAsIGRhdGEucHJlY2lzaW9uKVxuXHRcdCk7XG5cblx0XHRyZXR1cm4gbmV3IEN1cnJlbmN5QW1vdW50KFxuXHRcdFx0ZGVjaW1hbEFtb3VudCxcblx0XHRcdGRhdGFcblx0XHQpO1xuXHR9XG59XG5cbmxldCBjdXJyZW5jeUFtb3VudEZhY3Rvcnk6IEN1cnJlbmN5QW1vdW50RmFjdG9yeTtcblxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0Q3VycmVuY3lBbW91bnRGYWN0b3J5KFxuXHRkYXRhYmFzZTogSUNvaW5EYXRhYmFzZVNlcnZpY2Vcbik6IFByb21pc2U8Q3VycmVuY3lBbW91bnRGYWN0b3J5PiB7XG5cdGlmIChjdXJyZW5jeUFtb3VudEZhY3RvcnkgPT0gdW5kZWZpbmVkKSB7XG5cblx0XHRjb25zdCBjb2luRGF0YVByb3ZpZGVyID0gbmV3IENvaW5EYXRhUHJvdmlkZXIoXG5cdFx0XHRkYXRhYmFzZVxuXHRcdCk7XG5cblx0XHRhd2FpdCBjb2luRGF0YVByb3ZpZGVyLmluaXQoKTtcblxuXHRcdGN1cnJlbmN5QW1vdW50RmFjdG9yeSA9IG5ldyBDdXJyZW5jeUFtb3VudEZhY3RvcnkoXG5cdFx0XHRjb2luRGF0YVByb3ZpZGVyXG5cdFx0KTtcblx0fVxuXG5cdHJldHVybiBjdXJyZW5jeUFtb3VudEZhY3Rvcnk7XG59XG5cblxuXG5jbGFzcyBDdXJyZW5jeUFtb3VudFdpdGhQcmljZUZhY3RvcnkgaW1wbGVtZW50cyBJQ3VycmVuY3lBbW91bnRXaXRoUHJpY2VGYWN0b3J5IHtcblx0Y29uc3RydWN0b3IoXG5cdFx0cmVhZG9ubHkgY29pbkRhdGFQcm92aWRlcjogSUNvaW5EYXRhUHJvdmlkZXIsXG5cdFx0cmVhZG9ubHkgcHJpY2VTZXJ2aWNlOiBJQ3VycmVuY3lQcmljZVNlcnZpY2Vcblx0KSB7XG5cdH1cblxuXHRhc3luYyBuZXdBbW91bnRGcm9tUXVhbnRpdHkocXVhbnRpdHk6IHN0cmluZyk6IFByb21pc2U8Q3VycmVuY3lBbW91bnRXaXRoUHJpY2U+IHtcblx0XHRjb25zdCBbYW1vdW50LCB0b2tlblN5bWJvbF0gPSBxdWFudGl0eS5zcGxpdCgnICcpO1xuXG5cdFx0cmV0dXJuIHRoaXMubmV3QW1vdW50RnJvbURlY2ltYWwoXG5cdFx0XHRhbW91bnQsXG5cdFx0XHR0b2tlblN5bWJvbFxuXHRcdCk7XG5cdH1cblxuXHRhc3luYyBuZXdBbW91bnRGcm9tRGVjaW1hbChkZWNpbWFsQW1vdW50OiBCaWdTb3VyY2UsIHRva2VuU3ltYm9sOiBzdHJpbmcpOiBQcm9taXNlPEN1cnJlbmN5QW1vdW50V2l0aFByaWNlPiB7XG5cdFx0Y29uc3QgZGF0YSA9IGF3YWl0IHRoaXMuY29pbkRhdGFQcm92aWRlci5nZXRDb2luRGF0YUJ5U3ltYm9sKHRva2VuU3ltYm9sKTtcblxuXHRcdHJldHVybiBuZXcgQ3VycmVuY3lBbW91bnRXaXRoUHJpY2UoXG5cdFx0XHRkZWNpbWFsQW1vdW50LFxuXHRcdFx0ZGF0YSxcblx0XHRcdGF3YWl0IHRoaXMucHJpY2VTZXJ2aWNlLmdldFByaWNlSW5VU0QoZGF0YS5zeW1ib2wpXG5cdFx0KTtcblx0fVxuXG5cdGFzeW5jIG5ld0Ftb3VudEZyb21EZWNpbWFsQW5kQ29pbklkKGRlY2ltYWxBbW91bnQ6IEJpZ1NvdXJjZSwgY29pbklkOiBDb2luSWQpOiBQcm9taXNlPEN1cnJlbmN5QW1vdW50V2l0aFByaWNlPiB7XG5cdFx0Y29uc3QgZGF0YSA9IGF3YWl0IHRoaXMuY29pbkRhdGFQcm92aWRlci5nZXRDb2luRGF0YShjb2luSWQpO1xuXG5cdFx0cmV0dXJuIG5ldyBDdXJyZW5jeUFtb3VudFdpdGhQcmljZShcblx0XHRcdGRlY2ltYWxBbW91bnQsXG5cdFx0XHRkYXRhLFxuXHRcdFx0YXdhaXQgdGhpcy5wcmljZVNlcnZpY2UuZ2V0UHJpY2VJblVTRChkYXRhLnN5bWJvbClcblx0XHQpO1xuXHR9XG5cblx0YXN5bmMgbmV3QW1vdW50RnJvbUludGVnZXIoaW50ZWdlclN1YnVuaXRzOiBCaWdTb3VyY2UsIHRva2VuU3ltYm9sOiBzdHJpbmcpOiBQcm9taXNlPEN1cnJlbmN5QW1vdW50V2l0aFByaWNlPiB7XG5cdFx0Y29uc3QgZGF0YSA9IGF3YWl0IHRoaXMuY29pbkRhdGFQcm92aWRlci5nZXRDb2luRGF0YUJ5U3ltYm9sKHRva2VuU3ltYm9sKTtcblxuXHRcdGNvbnN0IGRlY2ltYWxBbW91bnQgPSBuZXcgQmlnKGludGVnZXJTdWJ1bml0cykuZGl2KFxuXHRcdFx0TWF0aC5wb3coMTAsIGRhdGEucHJlY2lzaW9uKVxuXHRcdCk7XG5cblx0XHRyZXR1cm4gbmV3IEN1cnJlbmN5QW1vdW50V2l0aFByaWNlKFxuXHRcdFx0ZGVjaW1hbEFtb3VudCxcblx0XHRcdGRhdGEsXG5cdFx0XHRhd2FpdCB0aGlzLnByaWNlU2VydmljZS5nZXRQcmljZUluVVNEKGRhdGEuc3ltYm9sKVxuXHRcdCk7XG5cdH1cbn1cblxuXG5sZXQgY3VycmVuY3lBbW91bnRXaXRoUHJpY2VGYWN0b3J5OiBDdXJyZW5jeUFtb3VudFdpdGhQcmljZUZhY3Rvcnk7XG5cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldEN1cnJlbmN5QW1vdW50V2l0aFByaWNlRmFjdG9yeShcblx0ZGF0YWJhc2U6IElDb2luRGF0YWJhc2VTZXJ2aWNlLFxuXHRwcmljZVNlcnZpY2U6IElDdXJyZW5jeVByaWNlU2VydmljZSA9IHVuZGVmaW5lZFxuKTogUHJvbWlzZTxDdXJyZW5jeUFtb3VudFdpdGhQcmljZUZhY3Rvcnk+IHtcblx0aWYgKGN1cnJlbmN5QW1vdW50V2l0aFByaWNlRmFjdG9yeSA9PSB1bmRlZmluZWQpIHtcblxuXHRcdGNvbnN0IGNvaW5EYXRhUHJvdmlkZXIgPSBuZXcgQ29pbkRhdGFQcm92aWRlcihcblx0XHRcdGRhdGFiYXNlXG5cdFx0KTtcblxuXHRcdGF3YWl0IGNvaW5EYXRhUHJvdmlkZXIuaW5pdCgpO1xuXG5cdFx0aWYgKHByaWNlU2VydmljZSA9PSB1bmRlZmluZWQpIHtcblx0XHRcdHByaWNlU2VydmljZSA9IGF3YWl0IGdldENvaW5QcmljZVNlcnZpY2UoZGF0YWJhc2UpO1xuXHRcdH1cblxuXHRcdGN1cnJlbmN5QW1vdW50V2l0aFByaWNlRmFjdG9yeSA9IG5ldyBDdXJyZW5jeUFtb3VudFdpdGhQcmljZUZhY3RvcnkoXG5cdFx0XHRjb2luRGF0YVByb3ZpZGVyLFxuXHRcdFx0cHJpY2VTZXJ2aWNlXG5cdFx0KTtcblx0fVxuXG5cdHJldHVybiBjdXJyZW5jeUFtb3VudFdpdGhQcmljZUZhY3Rvcnk7XG59XG4iXX0=