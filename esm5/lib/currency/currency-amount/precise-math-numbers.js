/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/currency-amount/precise-math-numbers.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Big } from 'big.js';
import { PreciseMath } from './precise-math';
var PreciseNumber = /** @class */ (function () {
    function PreciseNumber(precision, integerValue) {
        if (integerValue === void 0) { integerValue = 0; }
        this.precision = precision;
        this.factor = new Big(Math.pow(10, precision));
        this.bigInteger = new Big(integerValue);
        Big.RM = 0 /* RoundDown */;
        this.decimal = this.bigInteger.div(this.factor)
            .toFixed(precision);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseNumber.prototype.isLessThan = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        this.validateMatchingCurrency(other);
        return this.bigInteger.lt(other.integer);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseNumber.prototype.isLessThanDecimal = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return new Big(this.decimal).lt(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseNumber.prototype.isGreaterThan = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        this.validateMatchingCurrency(other);
        return this.bigInteger.gt(other.integer);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseNumber.prototype.isGreaterThanDecimal = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return new Big(this.decimal).gt(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseNumber.prototype.isEqualTo = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        this.validateMatchingCurrency(other);
        return this.bigInteger.eq(other.integer);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseNumber.prototype.isEqualToDecimal = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return new Big(this.decimal).eq(other);
    };
    /**
     * @protected
     * @param {?} other
     * @return {?}
     */
    PreciseNumber.prototype.validateMatchingCurrency = /**
     * @protected
     * @param {?} other
     * @return {?}
     */
    function (other) {
        if (this.precision != other.precision) {
            throw new Error('both amounts must be the same precision!');
        }
    };
    Object.defineProperty(PreciseNumber.prototype, "integer", {
        get: /**
         * @return {?}
         */
        function () {
            return this.bigInteger.toFixed(0);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreciseNumber.prototype, "isZero", {
        get: /**
         * @return {?}
         */
        function () {
            return this.bigInteger.eq(0);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreciseNumber.prototype, "isPositive", {
        get: /**
         * @return {?}
         */
        function () {
            return this.bigInteger.gt(0);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreciseNumber.prototype, "isNegative", {
        get: /**
         * @return {?}
         */
        function () {
            return this.bigInteger.lt(0);
        },
        enumerable: true,
        configurable: true
    });
    return PreciseNumber;
}());
export { PreciseNumber };
if (false) {
    /** @type {?} */
    PreciseNumber.prototype.factor;
    /** @type {?} */
    PreciseNumber.prototype.bigInteger;
    /** @type {?} */
    PreciseNumber.prototype.decimal;
    /** @type {?} */
    PreciseNumber.prototype.precision;
}
var PreciseDecimal = /** @class */ (function (_super) {
    tslib_1.__extends(PreciseDecimal, _super);
    function PreciseDecimal(decimalValue, precision) {
        var _this = this;
        /** @type {?} */
        var factor = new Big(Math.pow(10, precision));
        /** @type {?} */
        var decimal = new Big(decimalValue);
        _this = _super.call(this, precision, decimal
            .times(factor)
            .round(0, 0 /* RoundDown */)) || this;
        return _this;
    }
    return PreciseDecimal;
}(PreciseNumber));
export { PreciseDecimal };
var MatchingPrecisionValidator = /** @class */ (function () {
    function MatchingPrecisionValidator(precision) {
        this.precision = precision;
    }
    /**
     * @param {?} other
     * @return {?}
     */
    MatchingPrecisionValidator.prototype.isMatchingType = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        if (this.precision != other.precision) {
            throw new Error('both amounts must be the same precision!');
        }
        else {
            return true;
        }
    };
    return MatchingPrecisionValidator;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    MatchingPrecisionValidator.prototype.precision;
}
var PreciseNumberFactory = /** @class */ (function () {
    function PreciseNumberFactory(precision) {
        this.validator = new MatchingPrecisionValidator(precision);
    }
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    PreciseNumberFactory.prototype.newAmountFromInteger = /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    function (integer, precision) {
        /** @type {?} */
        var number = new PreciseNumber(precision, integer);
        this.validator.isMatchingType(number);
        return number;
    };
    /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    PreciseNumberFactory.prototype.newAmountFromDecimal = /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    function (decimal, precision) {
        /** @type {?} */
        var number = new PreciseDecimal(decimal, precision);
        this.validator.isMatchingType(number);
        return number;
    };
    return PreciseNumberFactory;
}());
if (false) {
    /** @type {?} */
    PreciseNumberFactory.prototype.validator;
}
/**
 * @template T
 */
var /**
 * @template T
 */
NumberForPreciseMathBase = /** @class */ (function () {
    function NumberForPreciseMathBase(precision, integerValue, factory) {
        this.factory = factory;
        /** @type {?} */
        var number = factory.newAmountFromInteger(integerValue, precision);
        this.math = new PreciseMath(number, factory);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    NumberForPreciseMathBase.prototype.addDecimal = /**
     * @param {?} decimal
     * @return {?}
     */
    function (decimal) {
        return this.math.addDecimal(decimal);
    };
    /**
     * @param {?} amount
     * @return {?}
     */
    NumberForPreciseMathBase.prototype.add = /**
     * @param {?} amount
     * @return {?}
     */
    function (amount) {
        return this.math.add(amount);
    };
    /**
     * @param {?} decimal
     * @return {?}
     */
    NumberForPreciseMathBase.prototype.subtractDecimal = /**
     * @param {?} decimal
     * @return {?}
     */
    function (decimal) {
        return this.math.subtractDecimal(decimal);
    };
    /**
     * @param {?} amount
     * @return {?}
     */
    NumberForPreciseMathBase.prototype.subtract = /**
     * @param {?} amount
     * @return {?}
     */
    function (amount) {
        return this.math.subtract(amount);
    };
    /**
     * @param {?} decimal
     * @return {?}
     */
    NumberForPreciseMathBase.prototype.multiplyDecimal = /**
     * @param {?} decimal
     * @return {?}
     */
    function (decimal) {
        return this.math.multiplyDecimal(decimal);
    };
    /**
     * @param {?} decimal
     * @return {?}
     */
    NumberForPreciseMathBase.prototype.divideDecimal = /**
     * @param {?} decimal
     * @return {?}
     */
    function (decimal) {
        return this.math.divideDecimal(decimal);
    };
    /**
     * @return {?}
     */
    NumberForPreciseMathBase.prototype.absoluteValue = /**
     * @return {?}
     */
    function () {
        return this.math.absoluteValue();
    };
    /**
     * @param {?} other
     * @return {?}
     */
    NumberForPreciseMathBase.prototype.isLessThan = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return this.number.isLessThan(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    NumberForPreciseMathBase.prototype.isLessThanDecimal = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return this.number.isLessThanDecimal(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    NumberForPreciseMathBase.prototype.isGreaterThan = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return this.number.isGreaterThan(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    NumberForPreciseMathBase.prototype.isGreaterThanDecimal = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return this.number.isGreaterThanDecimal(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    NumberForPreciseMathBase.prototype.isEqualTo = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return this.number.isEqualTo(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    NumberForPreciseMathBase.prototype.isEqualToDecimal = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return this.number.isEqualToDecimal(other);
    };
    Object.defineProperty(NumberForPreciseMathBase.prototype, "decimal", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.decimal;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NumberForPreciseMathBase.prototype, "integer", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.integer;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NumberForPreciseMathBase.prototype, "isZero", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.isZero;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NumberForPreciseMathBase.prototype, "isPositive", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.isPositive;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NumberForPreciseMathBase.prototype, "isNegative", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.isNegative;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NumberForPreciseMathBase.prototype, "bigInteger", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.bigInteger;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NumberForPreciseMathBase.prototype, "precision", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.precision;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NumberForPreciseMathBase.prototype, "factor", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.factor;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NumberForPreciseMathBase.prototype, "number", {
        get: /**
         * @return {?}
         */
        function () {
            return this.startingValue;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NumberForPreciseMathBase.prototype, "startingValue", {
        get: /**
         * @return {?}
         */
        function () {
            return this.math.startingValue;
        },
        enumerable: true,
        configurable: true
    });
    return NumberForPreciseMathBase;
}());
/**
 * @template T
 */
export { NumberForPreciseMathBase };
if (false) {
    /** @type {?} */
    NumberForPreciseMathBase.prototype.math;
    /** @type {?} */
    NumberForPreciseMathBase.prototype.factory;
}
var NumberForPreciseMath = /** @class */ (function (_super) {
    tslib_1.__extends(NumberForPreciseMath, _super);
    function NumberForPreciseMath(precision, integerValue) {
        if (integerValue === void 0) { integerValue = 0; }
        return _super.call(this, precision, integerValue, new PreciseNumberFactory(precision)) || this;
    }
    return NumberForPreciseMath;
}(NumberForPreciseMathBase));
export { NumberForPreciseMath };
var DecimalForPreciseMath = /** @class */ (function (_super) {
    tslib_1.__extends(DecimalForPreciseMath, _super);
    function DecimalForPreciseMath(decimalValue, precision) {
        var _this = this;
        /** @type {?} */
        var factor = new Big(Math.pow(10, precision));
        /** @type {?} */
        var decimal = new Big(decimalValue);
        _this = _super.call(this, precision, decimal
            .times(factor)
            .round(0, 0 /* RoundDown */)) || this;
        return _this;
    }
    return DecimalForPreciseMath;
}(NumberForPreciseMath));
export { DecimalForPreciseMath };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJlY2lzZS1tYXRoLW51bWJlcnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9jdXJyZW5jeS9jdXJyZW5jeS1hbW91bnQvcHJlY2lzZS1tYXRoLW51bWJlcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLEdBQUcsRUFBMkIsTUFBTSxRQUFRLENBQUM7QUFFdEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRzdDO0lBS0MsdUJBQ1UsU0FBaUIsRUFDMUIsWUFBMkI7UUFBM0IsNkJBQUEsRUFBQSxnQkFBMkI7UUFEbEIsY0FBUyxHQUFULFNBQVMsQ0FBUTtRQUcxQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksR0FBRyxDQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsQ0FDdkIsQ0FBQztRQUVGLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7UUFFeEMsR0FBRyxDQUFDLEVBQUUsb0JBQXlCLENBQUM7UUFDaEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO2FBQzdDLE9BQU8sQ0FDUCxTQUFTLENBQ1QsQ0FBQztJQUNKLENBQUM7Ozs7O0lBR0Qsa0NBQVU7Ozs7SUFBVixVQUFXLEtBQXFCO1FBQy9CLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVyQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMxQyxDQUFDOzs7OztJQUNELHlDQUFpQjs7OztJQUFqQixVQUFrQixLQUFnQjtRQUNqQyxPQUFPLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDeEMsQ0FBQzs7Ozs7SUFFRCxxQ0FBYTs7OztJQUFiLFVBQWMsS0FBcUI7UUFDbEMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXJDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7O0lBQ0QsNENBQW9COzs7O0lBQXBCLFVBQXFCLEtBQWdCO1FBQ3BDLE9BQU8sSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN4QyxDQUFDOzs7OztJQUVELGlDQUFTOzs7O0lBQVQsVUFBVSxLQUFxQjtRQUM5QixJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFckMsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Ozs7SUFDRCx3Q0FBZ0I7Ozs7SUFBaEIsVUFBaUIsS0FBZ0I7UUFDaEMsT0FBTyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3hDLENBQUM7Ozs7OztJQUVTLGdEQUF3Qjs7Ozs7SUFBbEMsVUFBbUMsS0FBcUI7UUFDdkQsSUFDQyxJQUFJLENBQUMsU0FBUyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQ2hDO1lBQ0QsTUFBTSxJQUFJLEtBQUssQ0FBQywwQ0FBMEMsQ0FBQyxDQUFDO1NBQzVEO0lBQ0YsQ0FBQztJQUdELHNCQUFJLGtDQUFPOzs7O1FBQVg7WUFDQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25DLENBQUM7OztPQUFBO0lBRUQsc0JBQUksaUNBQU07Ozs7UUFBVjtZQUNDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDOUIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxxQ0FBVTs7OztRQUFkO1lBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM5QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHFDQUFVOzs7O1FBQWQ7WUFDQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzlCLENBQUM7OztPQUFBO0lBQ0Ysb0JBQUM7QUFBRCxDQUFDLEFBMUVELElBMEVDOzs7O0lBekVBLCtCQUFxQjs7SUFDckIsbUNBQXlCOztJQUN6QixnQ0FBeUI7O0lBR3hCLGtDQUEwQjs7QUF1RTVCO0lBQW9DLDBDQUFhO0lBQ2hELHdCQUNDLFlBQXVCLEVBQ3ZCLFNBQWlCO1FBRmxCLGlCQWNDOztZQVZNLE1BQU0sR0FBRyxJQUFJLEdBQUcsQ0FBRSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsQ0FBRTs7WUFFM0MsT0FBTyxHQUFHLElBQUksR0FBRyxDQUFDLFlBQVksQ0FBQztRQUVyQyxRQUFBLGtCQUNDLFNBQVMsRUFDVCxPQUFPO2FBQ0wsS0FBSyxDQUFDLE1BQU0sQ0FBQzthQUNiLEtBQUssQ0FBQyxDQUFDLG9CQUF5QixDQUNsQyxTQUFDOztJQUNILENBQUM7SUFDRixxQkFBQztBQUFELENBQUMsQUFoQkQsQ0FBb0MsYUFBYSxHQWdCaEQ7O0FBR0Q7SUFDQyxvQ0FBNkIsU0FBaUI7UUFBakIsY0FBUyxHQUFULFNBQVMsQ0FBUTtJQUM5QyxDQUFDOzs7OztJQUVELG1EQUFjOzs7O0lBQWQsVUFBZSxLQUFxQjtRQUNuQyxJQUNDLElBQUksQ0FBQyxTQUFTLElBQUksS0FBSyxDQUFDLFNBQVMsRUFDaEM7WUFDRCxNQUFNLElBQUksS0FBSyxDQUFDLDBDQUEwQyxDQUFDLENBQUM7U0FDNUQ7YUFBTTtZQUNOLE9BQU8sSUFBSSxDQUFDO1NBQ1o7SUFDRixDQUFDO0lBQ0YsaUNBQUM7QUFBRCxDQUFDLEFBYkQsSUFhQzs7Ozs7O0lBWlksK0NBQWtDOztBQWMvQztJQUdDLDhCQUFZLFNBQWlCO1FBQzVCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM1RCxDQUFDOzs7Ozs7SUFFRCxtREFBb0I7Ozs7O0lBQXBCLFVBQXFCLE9BQWtCLEVBQUUsU0FBaUI7O1lBQ25ELE1BQU0sR0FBRyxJQUFJLGFBQWEsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDO1FBRXBELElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXRDLE9BQU8sTUFBTSxDQUFDO0lBQ2YsQ0FBQzs7Ozs7O0lBRUQsbURBQW9COzs7OztJQUFwQixVQUFxQixPQUFrQixFQUFFLFNBQWlCOztZQUNuRCxNQUFNLEdBQUcsSUFBSSxjQUFjLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQztRQUVyRCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUV0QyxPQUFPLE1BQU0sQ0FBQztJQUNmLENBQUM7SUFDRiwyQkFBQztBQUFELENBQUMsQUF0QkQsSUFzQkM7OztJQXJCQSx5Q0FBaUU7Ozs7O0FBd0JsRTs7OztJQUlDLGtDQUNDLFNBQWlCLEVBQ2pCLFlBQXVCLEVBQ2QsT0FBaUM7UUFBakMsWUFBTyxHQUFQLE9BQU8sQ0FBMEI7O1lBRXBDLE1BQU0sR0FBRyxPQUFPLENBQUMsb0JBQW9CLENBQzFDLFlBQVksRUFBRSxTQUFTLENBQ3ZCO1FBRUQsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLFdBQVcsQ0FDMUIsTUFBTSxFQUNOLE9BQU8sQ0FDUCxDQUFDO0lBQ0gsQ0FBQzs7Ozs7SUFFRCw2Q0FBVTs7OztJQUFWLFVBQVcsT0FBa0I7UUFDNUIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN0QyxDQUFDOzs7OztJQUNELHNDQUFHOzs7O0lBQUgsVUFBSSxNQUFTO1FBQ1osT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM5QixDQUFDOzs7OztJQUVELGtEQUFlOzs7O0lBQWYsVUFBZ0IsT0FBa0I7UUFDakMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMzQyxDQUFDOzs7OztJQUNELDJDQUFROzs7O0lBQVIsVUFBUyxNQUFTO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7Ozs7SUFFRCxrREFBZTs7OztJQUFmLFVBQWdCLE9BQWtCO1FBQ2pDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDM0MsQ0FBQzs7Ozs7SUFDRCxnREFBYTs7OztJQUFiLFVBQWMsT0FBa0I7UUFDL0IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7O0lBRUQsZ0RBQWE7OztJQUFiO1FBQ0MsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ2xDLENBQUM7Ozs7O0lBRUQsNkNBQVU7Ozs7SUFBVixVQUFXLEtBQVE7UUFDbEIsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN0QyxDQUFDOzs7OztJQUNELG9EQUFpQjs7OztJQUFqQixVQUFrQixLQUFnQjtRQUNqQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDN0MsQ0FBQzs7Ozs7SUFFRCxnREFBYTs7OztJQUFiLFVBQWMsS0FBUTtRQUNyQixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pDLENBQUM7Ozs7O0lBQ0QsdURBQW9COzs7O0lBQXBCLFVBQXFCLEtBQWdCO1FBQ3BDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoRCxDQUFDOzs7OztJQUVELDRDQUFTOzs7O0lBQVQsVUFBVSxLQUFRO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckMsQ0FBQzs7Ozs7SUFDRCxtREFBZ0I7Ozs7SUFBaEIsVUFBaUIsS0FBZ0I7UUFDaEMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFHRCxzQkFBSSw2Q0FBTzs7OztRQUFYO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUM1QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDZDQUFPOzs7O1FBQVg7WUFDQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO1FBQzVCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksNENBQU07Ozs7UUFBVjtZQUNDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDM0IsQ0FBQzs7O09BQUE7SUFDRCxzQkFBSSxnREFBVTs7OztRQUFkO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztRQUMvQixDQUFDOzs7T0FBQTtJQUNELHNCQUFJLGdEQUFVOzs7O1FBQWQ7WUFDQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO1FBQy9CLENBQUM7OztPQUFBO0lBRUQsc0JBQUksZ0RBQVU7Ozs7UUFBZDtZQUNDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFDL0IsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwrQ0FBUzs7OztRQUFiO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUM5QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDRDQUFNOzs7O1FBQVY7WUFDQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQzNCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksNENBQU07Ozs7UUFBVjtZQUNDLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUMzQixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLG1EQUFhOzs7O1FBQWpCO1lBQ0MsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUNoQyxDQUFDOzs7T0FBQTtJQUNGLCtCQUFDO0FBQUQsQ0FBQyxBQXZHRCxJQXVHQzs7Ozs7OztJQXJHQSx3Q0FBOEI7O0lBSzdCLDJDQUEwQzs7QUFrRzVDO0lBQTBDLGdEQUF3QztJQUNqRiw4QkFDQyxTQUFpQixFQUNqQixZQUEyQjtRQUEzQiw2QkFBQSxFQUFBLGdCQUEyQjtlQUUzQixrQkFDQyxTQUFTLEVBQ1QsWUFBWSxFQUNaLElBQUksb0JBQW9CLENBQUMsU0FBUyxDQUFDLENBQ25DO0lBQ0YsQ0FBQztJQUNGLDJCQUFDO0FBQUQsQ0FBQyxBQVhELENBQTBDLHdCQUF3QixHQVdqRTs7QUFHRDtJQUEyQyxpREFBb0I7SUFDOUQsK0JBQ0MsWUFBdUIsRUFDdkIsU0FBaUI7UUFGbEIsaUJBY0M7O1lBVk0sTUFBTSxHQUFHLElBQUksR0FBRyxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLFNBQVMsQ0FBQyxDQUFFOztZQUUzQyxPQUFPLEdBQUcsSUFBSSxHQUFHLENBQUMsWUFBWSxDQUFDO1FBRXJDLFFBQUEsa0JBQ0MsU0FBUyxFQUNULE9BQU87YUFDTCxLQUFLLENBQUMsTUFBTSxDQUFDO2FBQ2IsS0FBSyxDQUFDLENBQUMsb0JBQXlCLENBQ2xDLFNBQUM7O0lBQ0gsQ0FBQztJQUNGLDRCQUFDO0FBQUQsQ0FBQyxBQWhCRCxDQUEyQyxvQkFBb0IsR0FnQjlEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQmlnLCBSb3VuZGluZ01vZGUsIEJpZ1NvdXJjZSB9IGZyb20gJ2JpZy5qcyc7XG5pbXBvcnQgeyBJUHJlY2lzZU51bWJlciwgSVByZWNpc2VNYXRoLCBJUHJlY2lzZU51bWJlckZhY3RvcnksIElNYXRjaGluZ051bWJlclR5cGVWYWxpZGF0b3IgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgUHJlY2lzZU1hdGggfSBmcm9tICcuL3ByZWNpc2UtbWF0aCc7XG5cblxuZXhwb3J0IGNsYXNzIFByZWNpc2VOdW1iZXIgaW1wbGVtZW50cyBJUHJlY2lzZU51bWJlciB7XG5cdHJlYWRvbmx5IGZhY3RvcjogQmlnO1xuXHRyZWFkb25seSBiaWdJbnRlZ2VyOiBCaWc7XG5cdHJlYWRvbmx5IGRlY2ltYWw6IHN0cmluZztcblxuXHRjb25zdHJ1Y3Rvcihcblx0XHRyZWFkb25seSBwcmVjaXNpb246IG51bWJlcixcblx0XHRpbnRlZ2VyVmFsdWU6IEJpZ1NvdXJjZSA9IDAsXG5cdCkge1xuXHRcdHRoaXMuZmFjdG9yID0gbmV3IEJpZyhcblx0XHRcdE1hdGgucG93KDEwLCBwcmVjaXNpb24pXG5cdFx0KTtcblxuXHRcdHRoaXMuYmlnSW50ZWdlciA9IG5ldyBCaWcoaW50ZWdlclZhbHVlKTtcblxuXHRcdEJpZy5STSA9IFJvdW5kaW5nTW9kZS5Sb3VuZERvd247XG5cdFx0dGhpcy5kZWNpbWFsID0gdGhpcy5iaWdJbnRlZ2VyLmRpdih0aGlzLmZhY3Rvcilcblx0XHRcdC50b0ZpeGVkKFxuXHRcdFx0XHRwcmVjaXNpb25cblx0XHRcdCk7XG5cdH1cblxuXG5cdGlzTGVzc1RoYW4ob3RoZXI6IElQcmVjaXNlTnVtYmVyKSB7XG5cdFx0dGhpcy52YWxpZGF0ZU1hdGNoaW5nQ3VycmVuY3kob3RoZXIpO1xuXG5cdFx0cmV0dXJuIHRoaXMuYmlnSW50ZWdlci5sdChvdGhlci5pbnRlZ2VyKTtcblx0fVxuXHRpc0xlc3NUaGFuRGVjaW1hbChvdGhlcjogQmlnU291cmNlKSB7XG5cdFx0cmV0dXJuIG5ldyBCaWcodGhpcy5kZWNpbWFsKS5sdChvdGhlcik7XG5cdH1cblxuXHRpc0dyZWF0ZXJUaGFuKG90aGVyOiBJUHJlY2lzZU51bWJlcikge1xuXHRcdHRoaXMudmFsaWRhdGVNYXRjaGluZ0N1cnJlbmN5KG90aGVyKTtcblxuXHRcdHJldHVybiB0aGlzLmJpZ0ludGVnZXIuZ3Qob3RoZXIuaW50ZWdlcik7XG5cdH1cblx0aXNHcmVhdGVyVGhhbkRlY2ltYWwob3RoZXI6IEJpZ1NvdXJjZSkge1xuXHRcdHJldHVybiBuZXcgQmlnKHRoaXMuZGVjaW1hbCkuZ3Qob3RoZXIpO1xuXHR9XG5cblx0aXNFcXVhbFRvKG90aGVyOiBJUHJlY2lzZU51bWJlcikge1xuXHRcdHRoaXMudmFsaWRhdGVNYXRjaGluZ0N1cnJlbmN5KG90aGVyKTtcblxuXHRcdHJldHVybiB0aGlzLmJpZ0ludGVnZXIuZXEob3RoZXIuaW50ZWdlcik7XG5cdH1cblx0aXNFcXVhbFRvRGVjaW1hbChvdGhlcjogQmlnU291cmNlKSB7XG5cdFx0cmV0dXJuIG5ldyBCaWcodGhpcy5kZWNpbWFsKS5lcShvdGhlcik7XG5cdH1cblxuXHRwcm90ZWN0ZWQgdmFsaWRhdGVNYXRjaGluZ0N1cnJlbmN5KG90aGVyOiBJUHJlY2lzZU51bWJlcikge1xuXHRcdGlmIChcblx0XHRcdHRoaXMucHJlY2lzaW9uICE9IG90aGVyLnByZWNpc2lvblxuXHRcdCkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdib3RoIGFtb3VudHMgbXVzdCBiZSB0aGUgc2FtZSBwcmVjaXNpb24hJyk7XG5cdFx0fVxuXHR9XG5cblxuXHRnZXQgaW50ZWdlcigpIHtcblx0XHRyZXR1cm4gdGhpcy5iaWdJbnRlZ2VyLnRvRml4ZWQoMCk7XG5cdH1cblxuXHRnZXQgaXNaZXJvKCkge1xuXHRcdHJldHVybiB0aGlzLmJpZ0ludGVnZXIuZXEoMCk7XG5cdH1cblxuXHRnZXQgaXNQb3NpdGl2ZSgpIHtcblx0XHRyZXR1cm4gdGhpcy5iaWdJbnRlZ2VyLmd0KDApO1xuXHR9XG5cblx0Z2V0IGlzTmVnYXRpdmUoKSB7XG5cdFx0cmV0dXJuIHRoaXMuYmlnSW50ZWdlci5sdCgwKTtcblx0fVxufVxuXG5cbmV4cG9ydCBjbGFzcyBQcmVjaXNlRGVjaW1hbCBleHRlbmRzIFByZWNpc2VOdW1iZXIge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRkZWNpbWFsVmFsdWU6IEJpZ1NvdXJjZSxcblx0XHRwcmVjaXNpb246IG51bWJlclxuXHQpIHtcblx0XHRjb25zdCBmYWN0b3IgPSBuZXcgQmlnKCBNYXRoLnBvdygxMCwgcHJlY2lzaW9uKSApO1xuXG5cdFx0Y29uc3QgZGVjaW1hbCA9IG5ldyBCaWcoZGVjaW1hbFZhbHVlKTtcblxuXHRcdHN1cGVyKFxuXHRcdFx0cHJlY2lzaW9uLFxuXHRcdFx0ZGVjaW1hbFxuXHRcdFx0XHQudGltZXMoZmFjdG9yKVxuXHRcdFx0XHQucm91bmQoMCwgUm91bmRpbmdNb2RlLlJvdW5kRG93bilcblx0XHQpO1xuXHR9XG59XG5cblxuY2xhc3MgTWF0Y2hpbmdQcmVjaXNpb25WYWxpZGF0b3IgaW1wbGVtZW50cyBJTWF0Y2hpbmdOdW1iZXJUeXBlVmFsaWRhdG9yPElQcmVjaXNlTnVtYmVyPiB7XG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcmVhZG9ubHkgcHJlY2lzaW9uOiBudW1iZXIpIHtcblx0fVxuXG5cdGlzTWF0Y2hpbmdUeXBlKG90aGVyOiBJUHJlY2lzZU51bWJlcik6IGJvb2xlYW4ge1xuXHRcdGlmIChcblx0XHRcdHRoaXMucHJlY2lzaW9uICE9IG90aGVyLnByZWNpc2lvblxuXHRcdCkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdib3RoIGFtb3VudHMgbXVzdCBiZSB0aGUgc2FtZSBwcmVjaXNpb24hJyk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH1cblx0fVxufVxuXG5jbGFzcyBQcmVjaXNlTnVtYmVyRmFjdG9yeSBpbXBsZW1lbnRzIElQcmVjaXNlTnVtYmVyRmFjdG9yeTxJUHJlY2lzZU51bWJlcj4ge1xuXHRyZWFkb25seSB2YWxpZGF0b3I6IElNYXRjaGluZ051bWJlclR5cGVWYWxpZGF0b3I8SVByZWNpc2VOdW1iZXI+O1xuXG5cdGNvbnN0cnVjdG9yKHByZWNpc2lvbjogbnVtYmVyKSB7XG5cdFx0dGhpcy52YWxpZGF0b3IgPSBuZXcgTWF0Y2hpbmdQcmVjaXNpb25WYWxpZGF0b3IocHJlY2lzaW9uKTtcblx0fVxuXG5cdG5ld0Ftb3VudEZyb21JbnRlZ2VyKGludGVnZXI6IEJpZ1NvdXJjZSwgcHJlY2lzaW9uOiBudW1iZXIpOiBJUHJlY2lzZU51bWJlciB7XG5cdFx0Y29uc3QgbnVtYmVyID0gbmV3IFByZWNpc2VOdW1iZXIocHJlY2lzaW9uLCBpbnRlZ2VyKTtcblxuXHRcdHRoaXMudmFsaWRhdG9yLmlzTWF0Y2hpbmdUeXBlKG51bWJlcik7XG5cblx0XHRyZXR1cm4gbnVtYmVyO1xuXHR9XG5cblx0bmV3QW1vdW50RnJvbURlY2ltYWwoZGVjaW1hbDogQmlnU291cmNlLCBwcmVjaXNpb246IG51bWJlcik6IElQcmVjaXNlTnVtYmVyIHtcblx0XHRjb25zdCBudW1iZXIgPSBuZXcgUHJlY2lzZURlY2ltYWwoZGVjaW1hbCwgcHJlY2lzaW9uKTtcblxuXHRcdHRoaXMudmFsaWRhdG9yLmlzTWF0Y2hpbmdUeXBlKG51bWJlcik7XG5cblx0XHRyZXR1cm4gbnVtYmVyO1xuXHR9XG59XG5cblxuZXhwb3J0IGNsYXNzIE51bWJlckZvclByZWNpc2VNYXRoQmFzZTxUIGV4dGVuZHMgSVByZWNpc2VOdW1iZXI+XG5cdGltcGxlbWVudHMgSVByZWNpc2VOdW1iZXIsIElQcmVjaXNlTWF0aDxUPiB7XG5cdHJlYWRvbmx5IG1hdGg6IFByZWNpc2VNYXRoPFQ+O1xuXG5cdGNvbnN0cnVjdG9yKFxuXHRcdHByZWNpc2lvbjogbnVtYmVyLFxuXHRcdGludGVnZXJWYWx1ZTogQmlnU291cmNlLFxuXHRcdHJlYWRvbmx5IGZhY3Rvcnk6IElQcmVjaXNlTnVtYmVyRmFjdG9yeTxUPlxuXHQpIHtcblx0XHRjb25zdCBudW1iZXIgPSBmYWN0b3J5Lm5ld0Ftb3VudEZyb21JbnRlZ2VyKFxuXHRcdFx0aW50ZWdlclZhbHVlLCBwcmVjaXNpb25cblx0XHQpO1xuXG5cdFx0dGhpcy5tYXRoID0gbmV3IFByZWNpc2VNYXRoKFxuXHRcdFx0bnVtYmVyLFxuXHRcdFx0ZmFjdG9yeVxuXHRcdCk7XG5cdH1cblxuXHRhZGREZWNpbWFsKGRlY2ltYWw6IEJpZ1NvdXJjZSkge1xuXHRcdHJldHVybiB0aGlzLm1hdGguYWRkRGVjaW1hbChkZWNpbWFsKTtcblx0fVxuXHRhZGQoYW1vdW50OiBUKSB7XG5cdFx0cmV0dXJuIHRoaXMubWF0aC5hZGQoYW1vdW50KTtcblx0fVxuXG5cdHN1YnRyYWN0RGVjaW1hbChkZWNpbWFsOiBCaWdTb3VyY2UpIHtcblx0XHRyZXR1cm4gdGhpcy5tYXRoLnN1YnRyYWN0RGVjaW1hbChkZWNpbWFsKTtcblx0fVxuXHRzdWJ0cmFjdChhbW91bnQ6IFQpIHtcblx0XHRyZXR1cm4gdGhpcy5tYXRoLnN1YnRyYWN0KGFtb3VudCk7XG5cdH1cblxuXHRtdWx0aXBseURlY2ltYWwoZGVjaW1hbDogQmlnU291cmNlKSB7XG5cdFx0cmV0dXJuIHRoaXMubWF0aC5tdWx0aXBseURlY2ltYWwoZGVjaW1hbCk7XG5cdH1cblx0ZGl2aWRlRGVjaW1hbChkZWNpbWFsOiBCaWdTb3VyY2UpIHtcblx0XHRyZXR1cm4gdGhpcy5tYXRoLmRpdmlkZURlY2ltYWwoZGVjaW1hbCk7XG5cdH1cblxuXHRhYnNvbHV0ZVZhbHVlKCkge1xuXHRcdHJldHVybiB0aGlzLm1hdGguYWJzb2x1dGVWYWx1ZSgpO1xuXHR9XG5cblx0aXNMZXNzVGhhbihvdGhlcjogVCk6IGJvb2xlYW4ge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5pc0xlc3NUaGFuKG90aGVyKTtcblx0fVxuXHRpc0xlc3NUaGFuRGVjaW1hbChvdGhlcjogQmlnU291cmNlKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmlzTGVzc1RoYW5EZWNpbWFsKG90aGVyKTtcblx0fVxuXG5cdGlzR3JlYXRlclRoYW4ob3RoZXI6IFQpOiBib29sZWFuIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuaXNHcmVhdGVyVGhhbihvdGhlcik7XG5cdH1cblx0aXNHcmVhdGVyVGhhbkRlY2ltYWwob3RoZXI6IEJpZ1NvdXJjZSkge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5pc0dyZWF0ZXJUaGFuRGVjaW1hbChvdGhlcik7XG5cdH1cblxuXHRpc0VxdWFsVG8ob3RoZXI6IFQpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuaXNFcXVhbFRvKG90aGVyKTtcblx0fVxuXHRpc0VxdWFsVG9EZWNpbWFsKG90aGVyOiBCaWdTb3VyY2UpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuaXNFcXVhbFRvRGVjaW1hbChvdGhlcik7XG5cdH1cblxuXG5cdGdldCBkZWNpbWFsKCkge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5kZWNpbWFsO1xuXHR9XG5cblx0Z2V0IGludGVnZXIoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmludGVnZXI7XG5cdH1cblxuXHRnZXQgaXNaZXJvKCkge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5pc1plcm87XG5cdH1cblx0Z2V0IGlzUG9zaXRpdmUoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmlzUG9zaXRpdmU7XG5cdH1cblx0Z2V0IGlzTmVnYXRpdmUoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmlzTmVnYXRpdmU7XG5cdH1cblxuXHRnZXQgYmlnSW50ZWdlcigpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuYmlnSW50ZWdlcjtcblx0fVxuXG5cdGdldCBwcmVjaXNpb24oKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLnByZWNpc2lvbjtcblx0fVxuXG5cdGdldCBmYWN0b3IoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmZhY3Rvcjtcblx0fVxuXG5cdGdldCBudW1iZXIoKSB7XG5cdFx0cmV0dXJuIHRoaXMuc3RhcnRpbmdWYWx1ZTtcblx0fVxuXG5cdGdldCBzdGFydGluZ1ZhbHVlKCkge1xuXHRcdHJldHVybiB0aGlzLm1hdGguc3RhcnRpbmdWYWx1ZTtcblx0fVxufVxuXG5leHBvcnQgY2xhc3MgTnVtYmVyRm9yUHJlY2lzZU1hdGggZXh0ZW5kcyBOdW1iZXJGb3JQcmVjaXNlTWF0aEJhc2U8SVByZWNpc2VOdW1iZXI+IHtcblx0Y29uc3RydWN0b3IoXG5cdFx0cHJlY2lzaW9uOiBudW1iZXIsXG5cdFx0aW50ZWdlclZhbHVlOiBCaWdTb3VyY2UgPSAwLFxuXHQpIHtcblx0XHRzdXBlcihcblx0XHRcdHByZWNpc2lvbixcblx0XHRcdGludGVnZXJWYWx1ZSxcblx0XHRcdG5ldyBQcmVjaXNlTnVtYmVyRmFjdG9yeShwcmVjaXNpb24pXG5cdFx0KTtcblx0fVxufVxuXG5cbmV4cG9ydCBjbGFzcyBEZWNpbWFsRm9yUHJlY2lzZU1hdGggZXh0ZW5kcyBOdW1iZXJGb3JQcmVjaXNlTWF0aCB7XG5cdGNvbnN0cnVjdG9yKFxuXHRcdGRlY2ltYWxWYWx1ZTogQmlnU291cmNlLFxuXHRcdHByZWNpc2lvbjogbnVtYmVyXG5cdCkge1xuXHRcdGNvbnN0IGZhY3RvciA9IG5ldyBCaWcoIE1hdGgucG93KDEwLCBwcmVjaXNpb24pICk7XG5cblx0XHRjb25zdCBkZWNpbWFsID0gbmV3IEJpZyhkZWNpbWFsVmFsdWUpO1xuXG5cdFx0c3VwZXIoXG5cdFx0XHRwcmVjaXNpb24sXG5cdFx0XHRkZWNpbWFsXG5cdFx0XHRcdC50aW1lcyhmYWN0b3IpXG5cdFx0XHRcdC5yb3VuZCgwLCBSb3VuZGluZ01vZGUuUm91bmREb3duKVxuXHRcdCk7XG5cdH1cbn1cbiJdfQ==