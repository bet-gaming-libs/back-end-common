/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/currency-amount/precise-math.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Big } from 'big.js';
/**
 * @template T
 */
var /**
 * @template T
 */
PreciseMath = /** @class */ (function () {
    function PreciseMath(startingValue, factory) {
        this.startingValue = startingValue;
        this.factory = factory;
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    PreciseMath.prototype.addDecimal = /**
     * @param {?} decimal
     * @return {?}
     */
    function (decimal) {
        /** @type {?} */
        var other = this.factory.newAmountFromDecimal(decimal, this.precision);
        return this.add(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseMath.prototype.add = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        this.validator.isMatchingType(other);
        /** @type {?} */
        var newInteger = this.startingValue.bigInteger.plus(other.integer);
        /** @type {?} */
        var newNumber = this.factory.newAmountFromInteger(newInteger, this.precision);
        return new PreciseMath(newNumber, this.factory);
    };
    /**
     * @param {?} decimal
     * @return {?}
     */
    PreciseMath.prototype.subtractDecimal = /**
     * @param {?} decimal
     * @return {?}
     */
    function (decimal) {
        /** @type {?} */
        var other = this.factory.newAmountFromDecimal(decimal, this.precision);
        return this.subtract(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseMath.prototype.subtract = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        this.validator.isMatchingType(other);
        /** @type {?} */
        var newInteger = this.startingValue.bigInteger.minus(other.integer);
        /** @type {?} */
        var newNumber = this.factory.newAmountFromInteger(newInteger, this.precision);
        return new PreciseMath(newNumber, this.factory);
    };
    /**
     * @param {?} decimal
     * @return {?}
     */
    PreciseMath.prototype.divideDecimal = /**
     * @param {?} decimal
     * @return {?}
     */
    function (decimal) {
        return this.multiplyDecimal(new Big(1).div(decimal));
    };
    /**
     * @param {?} decimal
     * @return {?}
     */
    PreciseMath.prototype.multiplyDecimal = /**
     * @param {?} decimal
     * @return {?}
     */
    function (decimal) {
        /** @type {?} */
        var product = new Big(this.startingValue.decimal).times(decimal);
        /** @type {?} */
        var newInteger = product
            .times(this.factor)
            .round(0, 0 /* RoundDown */);
        /** @type {?} */
        var newNumber = this.factory.newAmountFromInteger(newInteger, this.precision);
        return new PreciseMath(newNumber, this.factory);
    };
    /**
     * @return {?}
     */
    PreciseMath.prototype.absoluteValue = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var newNumber = this.factory.newAmountFromInteger(new Big(this.bigInteger).abs(), this.precision);
        return new PreciseMath(newNumber, this.factory);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseMath.prototype.isLessThan = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return this.number.isLessThan(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseMath.prototype.isLessThanDecimal = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return this.number.isLessThanDecimal(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseMath.prototype.isGreaterThan = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return this.number.isGreaterThan(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseMath.prototype.isGreaterThanDecimal = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return this.number.isGreaterThanDecimal(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseMath.prototype.isEqualTo = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return this.number.isEqualTo(other);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    PreciseMath.prototype.isEqualToDecimal = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return this.number.isEqualToDecimal(other);
    };
    Object.defineProperty(PreciseMath.prototype, "decimal", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.decimal;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreciseMath.prototype, "integer", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.integer;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreciseMath.prototype, "isZero", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.isZero;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreciseMath.prototype, "isPositive", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.isPositive;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreciseMath.prototype, "isNegative", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.isNegative;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreciseMath.prototype, "bigInteger", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.bigInteger;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreciseMath.prototype, "precision", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.precision;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreciseMath.prototype, "factor", {
        get: /**
         * @return {?}
         */
        function () {
            return this.number.factor;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreciseMath.prototype, "number", {
        get: /**
         * @return {?}
         */
        function () {
            return this.startingValue;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreciseMath.prototype, "validator", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.factory.validator;
        },
        enumerable: true,
        configurable: true
    });
    return PreciseMath;
}());
/**
 * @template T
 */
export { PreciseMath };
if (false) {
    /** @type {?} */
    PreciseMath.prototype.startingValue;
    /** @type {?} */
    PreciseMath.prototype.factory;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJlY2lzZS1tYXRoLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvY3VycmVuY3ktYW1vdW50L3ByZWNpc2UtbWF0aC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxHQUFHLEVBQTJCLE1BQU0sUUFBUSxDQUFDOzs7O0FBS3REOzs7O0lBRUMscUJBQ1UsYUFBZ0IsRUFDaEIsT0FBaUM7UUFEakMsa0JBQWEsR0FBYixhQUFhLENBQUc7UUFDaEIsWUFBTyxHQUFQLE9BQU8sQ0FBMEI7SUFFM0MsQ0FBQzs7Ozs7SUFFRCxnQ0FBVTs7OztJQUFWLFVBQVcsT0FBa0I7O1lBQ3RCLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLG9CQUFvQixDQUM5QyxPQUFPLEVBQ1AsSUFBSSxDQUFDLFNBQVMsQ0FDZDtRQUVELE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN4QixDQUFDOzs7OztJQUNELHlCQUFHOzs7O0lBQUgsVUFBSSxLQUFRO1FBQ1gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7O1lBRS9CLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQ3BELEtBQUssQ0FBQyxPQUFPLENBQ2I7O1lBRUssU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQ2xELFVBQVUsRUFDVixJQUFJLENBQUMsU0FBUyxDQUNkO1FBRUQsT0FBTyxJQUFJLFdBQVcsQ0FDckIsU0FBUyxFQUNULElBQUksQ0FBQyxPQUFPLENBQ1osQ0FBQztJQUNILENBQUM7Ozs7O0lBRUQscUNBQWU7Ozs7SUFBZixVQUFnQixPQUFrQjs7WUFDM0IsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQzlDLE9BQU8sRUFDUCxJQUFJLENBQUMsU0FBUyxDQUNkO1FBRUQsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzdCLENBQUM7Ozs7O0lBQ0QsOEJBQVE7Ozs7SUFBUixVQUFTLEtBQVE7UUFDaEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7O1lBRS9CLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQ3JELEtBQUssQ0FBQyxPQUFPLENBQ2I7O1lBRUssU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQ2xELFVBQVUsRUFDVixJQUFJLENBQUMsU0FBUyxDQUNkO1FBRUQsT0FBTyxJQUFJLFdBQVcsQ0FDckIsU0FBUyxFQUNULElBQUksQ0FBQyxPQUFPLENBQ1osQ0FBQztJQUNILENBQUM7Ozs7O0lBRUQsbUNBQWE7Ozs7SUFBYixVQUFjLE9BQWtCO1FBQy9CLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FDMUIsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUN2QixDQUFDO0lBQ0gsQ0FBQzs7Ozs7SUFDRCxxQ0FBZTs7OztJQUFmLFVBQWdCLE9BQWtCOztZQUMzQixPQUFPLEdBQUcsSUFBSSxHQUFHLENBQ3RCLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUMxQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7O1lBRVYsVUFBVSxHQUFHLE9BQU87YUFDckIsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7YUFDbEIsS0FBSyxDQUFDLENBQUMsb0JBQXlCOztZQUUvQixTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FDbEQsVUFBVSxFQUNWLElBQUksQ0FBQyxTQUFTLENBQ2Q7UUFFRCxPQUFPLElBQUksV0FBVyxDQUNyQixTQUFTLEVBQ1QsSUFBSSxDQUFDLE9BQU8sQ0FDWixDQUFDO0lBQ0gsQ0FBQzs7OztJQUVELG1DQUFhOzs7SUFBYjs7WUFDTyxTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FDbEQsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUM5QixJQUFJLENBQUMsU0FBUyxDQUNkO1FBRUQsT0FBTyxJQUFJLFdBQVcsQ0FDckIsU0FBUyxFQUNULElBQUksQ0FBQyxPQUFPLENBQ1osQ0FBQztJQUNILENBQUM7Ozs7O0lBR0QsZ0NBQVU7Ozs7SUFBVixVQUFXLEtBQVE7UUFDbEIsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN0QyxDQUFDOzs7OztJQUNELHVDQUFpQjs7OztJQUFqQixVQUFrQixLQUFnQjtRQUNqQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDN0MsQ0FBQzs7Ozs7SUFFRCxtQ0FBYTs7OztJQUFiLFVBQWMsS0FBUTtRQUNyQixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pDLENBQUM7Ozs7O0lBQ0QsMENBQW9COzs7O0lBQXBCLFVBQXFCLEtBQWdCO1FBQ3BDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoRCxDQUFDOzs7OztJQUVELCtCQUFTOzs7O0lBQVQsVUFBVSxLQUFRO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckMsQ0FBQzs7Ozs7SUFDRCxzQ0FBZ0I7Ozs7SUFBaEIsVUFBaUIsS0FBZ0I7UUFDaEMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFHRCxzQkFBSSxnQ0FBTzs7OztRQUFYO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUM1QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGdDQUFPOzs7O1FBQVg7WUFDQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO1FBQzVCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksK0JBQU07Ozs7UUFBVjtZQUNDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDM0IsQ0FBQzs7O09BQUE7SUFDRCxzQkFBSSxtQ0FBVTs7OztRQUFkO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztRQUMvQixDQUFDOzs7T0FBQTtJQUNELHNCQUFJLG1DQUFVOzs7O1FBQWQ7WUFDQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO1FBQy9CLENBQUM7OztPQUFBO0lBRUQsc0JBQUksbUNBQVU7Ozs7UUFBZDtZQUNDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFDL0IsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxrQ0FBUzs7OztRQUFiO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUM5QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLCtCQUFNOzs7O1FBQVY7WUFDQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQzNCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksK0JBQU07Ozs7UUFBVjtZQUNDLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUMzQixDQUFDOzs7T0FBQTtJQUdELHNCQUFZLGtDQUFTOzs7OztRQUFyQjtZQUNDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUM7UUFDL0IsQ0FBQzs7O09BQUE7SUFDRixrQkFBQztBQUFELENBQUMsQUE5SkQsSUE4SkM7Ozs7Ozs7SUEzSkMsb0NBQXlCOztJQUN6Qiw4QkFBMEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCaWcsIFJvdW5kaW5nTW9kZSwgQmlnU291cmNlIH0gZnJvbSAnYmlnLmpzJztcblxuaW1wb3J0IHsgSVByZWNpc2VOdW1iZXJGYWN0b3J5LCBJUHJlY2lzZU51bWJlciwgSVByZWNpc2VNYXRoIH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcblxuXG5leHBvcnQgY2xhc3MgUHJlY2lzZU1hdGg8VCBleHRlbmRzIElQcmVjaXNlTnVtYmVyPlxuXHRpbXBsZW1lbnRzIElQcmVjaXNlTWF0aDxUPiB7XG5cdGNvbnN0cnVjdG9yKFxuXHRcdHJlYWRvbmx5IHN0YXJ0aW5nVmFsdWU6IFQsXG5cdFx0cmVhZG9ubHkgZmFjdG9yeTogSVByZWNpc2VOdW1iZXJGYWN0b3J5PFQ+XG5cdCkge1xuXHR9XG5cblx0YWRkRGVjaW1hbChkZWNpbWFsOiBCaWdTb3VyY2UpIHtcblx0XHRjb25zdCBvdGhlciA9IHRoaXMuZmFjdG9yeS5uZXdBbW91bnRGcm9tRGVjaW1hbChcblx0XHRcdGRlY2ltYWwsXG5cdFx0XHR0aGlzLnByZWNpc2lvblxuXHRcdCk7XG5cblx0XHRyZXR1cm4gdGhpcy5hZGQob3RoZXIpO1xuXHR9XG5cdGFkZChvdGhlcjogVCkge1xuXHRcdHRoaXMudmFsaWRhdG9yLmlzTWF0Y2hpbmdUeXBlKG90aGVyKTtcblxuXHRcdGNvbnN0IG5ld0ludGVnZXIgPSB0aGlzLnN0YXJ0aW5nVmFsdWUuYmlnSW50ZWdlci5wbHVzKFxuXHRcdFx0b3RoZXIuaW50ZWdlclxuXHRcdCk7XG5cblx0XHRjb25zdCBuZXdOdW1iZXIgPSB0aGlzLmZhY3RvcnkubmV3QW1vdW50RnJvbUludGVnZXIoXG5cdFx0XHRuZXdJbnRlZ2VyLFxuXHRcdFx0dGhpcy5wcmVjaXNpb25cblx0XHQpO1xuXG5cdFx0cmV0dXJuIG5ldyBQcmVjaXNlTWF0aChcblx0XHRcdG5ld051bWJlcixcblx0XHRcdHRoaXMuZmFjdG9yeVxuXHRcdCk7XG5cdH1cblxuXHRzdWJ0cmFjdERlY2ltYWwoZGVjaW1hbDogQmlnU291cmNlKSB7XG5cdFx0Y29uc3Qgb3RoZXIgPSB0aGlzLmZhY3RvcnkubmV3QW1vdW50RnJvbURlY2ltYWwoXG5cdFx0XHRkZWNpbWFsLFxuXHRcdFx0dGhpcy5wcmVjaXNpb25cblx0XHQpO1xuXG5cdFx0cmV0dXJuIHRoaXMuc3VidHJhY3Qob3RoZXIpO1xuXHR9XG5cdHN1YnRyYWN0KG90aGVyOiBUKSB7XG5cdFx0dGhpcy52YWxpZGF0b3IuaXNNYXRjaGluZ1R5cGUob3RoZXIpO1xuXG5cdFx0Y29uc3QgbmV3SW50ZWdlciA9IHRoaXMuc3RhcnRpbmdWYWx1ZS5iaWdJbnRlZ2VyLm1pbnVzKFxuXHRcdFx0b3RoZXIuaW50ZWdlclxuXHRcdCk7XG5cblx0XHRjb25zdCBuZXdOdW1iZXIgPSB0aGlzLmZhY3RvcnkubmV3QW1vdW50RnJvbUludGVnZXIoXG5cdFx0XHRuZXdJbnRlZ2VyLFxuXHRcdFx0dGhpcy5wcmVjaXNpb25cblx0XHQpO1xuXG5cdFx0cmV0dXJuIG5ldyBQcmVjaXNlTWF0aChcblx0XHRcdG5ld051bWJlcixcblx0XHRcdHRoaXMuZmFjdG9yeVxuXHRcdCk7XG5cdH1cblxuXHRkaXZpZGVEZWNpbWFsKGRlY2ltYWw6IEJpZ1NvdXJjZSkge1xuXHRcdHJldHVybiB0aGlzLm11bHRpcGx5RGVjaW1hbChcblx0XHRcdG5ldyBCaWcoMSkuZGl2KGRlY2ltYWwpXG5cdFx0KTtcblx0fVxuXHRtdWx0aXBseURlY2ltYWwoZGVjaW1hbDogQmlnU291cmNlKSB7XG5cdFx0Y29uc3QgcHJvZHVjdCA9IG5ldyBCaWcoXG5cdFx0XHR0aGlzLnN0YXJ0aW5nVmFsdWUuZGVjaW1hbFxuXHRcdCkudGltZXMoZGVjaW1hbCk7XG5cblx0XHRjb25zdCBuZXdJbnRlZ2VyID0gcHJvZHVjdFxuXHRcdFx0XHRcdFx0LnRpbWVzKHRoaXMuZmFjdG9yKVxuXHRcdFx0XHRcdFx0LnJvdW5kKDAsIFJvdW5kaW5nTW9kZS5Sb3VuZERvd24pO1xuXG5cdFx0Y29uc3QgbmV3TnVtYmVyID0gdGhpcy5mYWN0b3J5Lm5ld0Ftb3VudEZyb21JbnRlZ2VyKFxuXHRcdFx0bmV3SW50ZWdlcixcblx0XHRcdHRoaXMucHJlY2lzaW9uXG5cdFx0KTtcblxuXHRcdHJldHVybiBuZXcgUHJlY2lzZU1hdGgoXG5cdFx0XHRuZXdOdW1iZXIsXG5cdFx0XHR0aGlzLmZhY3Rvcnlcblx0XHQpO1xuXHR9XG5cblx0YWJzb2x1dGVWYWx1ZSgpIHtcblx0XHRjb25zdCBuZXdOdW1iZXIgPSB0aGlzLmZhY3RvcnkubmV3QW1vdW50RnJvbUludGVnZXIoXG5cdFx0XHRuZXcgQmlnKHRoaXMuYmlnSW50ZWdlcikuYWJzKCksXG5cdFx0XHR0aGlzLnByZWNpc2lvblxuXHRcdCk7XG5cblx0XHRyZXR1cm4gbmV3IFByZWNpc2VNYXRoKFxuXHRcdFx0bmV3TnVtYmVyLFxuXHRcdFx0dGhpcy5mYWN0b3J5XG5cdFx0KTtcblx0fVxuXG5cblx0aXNMZXNzVGhhbihvdGhlcjogVCk6IGJvb2xlYW4ge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5pc0xlc3NUaGFuKG90aGVyKTtcblx0fVxuXHRpc0xlc3NUaGFuRGVjaW1hbChvdGhlcjogQmlnU291cmNlKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmlzTGVzc1RoYW5EZWNpbWFsKG90aGVyKTtcblx0fVxuXG5cdGlzR3JlYXRlclRoYW4ob3RoZXI6IFQpOiBib29sZWFuIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuaXNHcmVhdGVyVGhhbihvdGhlcik7XG5cdH1cblx0aXNHcmVhdGVyVGhhbkRlY2ltYWwob3RoZXI6IEJpZ1NvdXJjZSkge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5pc0dyZWF0ZXJUaGFuRGVjaW1hbChvdGhlcik7XG5cdH1cblxuXHRpc0VxdWFsVG8ob3RoZXI6IFQpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuaXNFcXVhbFRvKG90aGVyKTtcblx0fVxuXHRpc0VxdWFsVG9EZWNpbWFsKG90aGVyOiBCaWdTb3VyY2UpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuaXNFcXVhbFRvRGVjaW1hbChvdGhlcik7XG5cdH1cblxuXG5cdGdldCBkZWNpbWFsKCkge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5kZWNpbWFsO1xuXHR9XG5cblx0Z2V0IGludGVnZXIoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmludGVnZXI7XG5cdH1cblxuXHRnZXQgaXNaZXJvKCkge1xuXHRcdHJldHVybiB0aGlzLm51bWJlci5pc1plcm87XG5cdH1cblx0Z2V0IGlzUG9zaXRpdmUoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmlzUG9zaXRpdmU7XG5cdH1cblx0Z2V0IGlzTmVnYXRpdmUoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmlzTmVnYXRpdmU7XG5cdH1cblxuXHRnZXQgYmlnSW50ZWdlcigpIHtcblx0XHRyZXR1cm4gdGhpcy5udW1iZXIuYmlnSW50ZWdlcjtcblx0fVxuXG5cdGdldCBwcmVjaXNpb24oKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLnByZWNpc2lvbjtcblx0fVxuXG5cdGdldCBmYWN0b3IoKSB7XG5cdFx0cmV0dXJuIHRoaXMubnVtYmVyLmZhY3Rvcjtcblx0fVxuXG5cdGdldCBudW1iZXIoKSB7XG5cdFx0cmV0dXJuIHRoaXMuc3RhcnRpbmdWYWx1ZTtcblx0fVxuXG5cblx0cHJpdmF0ZSBnZXQgdmFsaWRhdG9yKCkge1xuXHRcdHJldHVybiB0aGlzLmZhY3RvcnkudmFsaWRhdG9yO1xuXHR9XG59XG4iXX0=