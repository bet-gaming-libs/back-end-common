/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/coin-data-provider.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Coin } from './models/coin';
var CoinDataProvider = /** @class */ (function () {
    function CoinDataProvider(database) {
        this.database = database;
        this.isInit = false;
        this.ids = {};
        this.data = {};
        this.allCoins = [];
    }
    /**
     * @return {?}
     */
    CoinDataProvider.prototype.init = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, _b, _c, row;
            var e_1, _d;
            return tslib_1.__generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.database.getAllCoins()];
                    case 1:
                        _a.allCoins = (_e.sent()).map((/**
                         * @param {?} row
                         * @return {?}
                         */
                        function (row) { return new Coin(row); }));
                        try {
                            for (_b = tslib_1.__values(this.allCoins), _c = _b.next(); !_c.done; _c = _b.next()) {
                                row = _c.value;
                                // map symbol to ID
                                this.ids[row.symbol] = row.id;
                                // map ID to data
                                this.data[row.id] = row;
                            }
                        }
                        catch (e_1_1) { e_1 = { error: e_1_1 }; }
                        finally {
                            try {
                                if (_c && !_c.done && (_d = _b.return)) _d.call(_b);
                            }
                            finally { if (e_1) throw e_1.error; }
                        }
                        this.isInit = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} symbol
     * @return {?}
     */
    CoinDataProvider.prototype.getCoinDataBySymbol = /**
     * @param {?} symbol
     * @return {?}
     */
    function (symbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var id;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getCoinId(symbol)];
                    case 1:
                        id = _a.sent();
                        return [2 /*return*/, this.getCoinData(id)];
                }
            });
        });
    };
    /**
     * @param {?} symbol
     * @return {?}
     */
    CoinDataProvider.prototype.getCoinId = /**
     * @param {?} symbol
     * @return {?}
     */
    function (symbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var id;
            return tslib_1.__generator(this, function (_a) {
                if (!this.isInit) {
                    throw new Error('CoinDataProvider is NOT Initialized!');
                }
                symbol = symbol.toUpperCase();
                id = this.ids[symbol];
                if (id === undefined) {
                    throw new Error('CoinId NOT FOUND: ' + symbol);
                }
                return [2 /*return*/, id];
            });
        });
    };
    /**
     * @param {?} coinId
     * @return {?}
     */
    CoinDataProvider.prototype.getCoinSymbol = /**
     * @param {?} coinId
     * @return {?}
     */
    function (coinId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var data;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getCoinData(coinId)];
                    case 1:
                        data = _a.sent();
                        return [2 /*return*/, data.symbol];
                }
            });
        });
    };
    /**
     * @param {?} coinId
     * @return {?}
     */
    CoinDataProvider.prototype.getCoinData = /**
     * @param {?} coinId
     * @return {?}
     */
    function (coinId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var data;
            return tslib_1.__generator(this, function (_a) {
                if (!this.isInit) {
                    throw new Error('CoinDataProvider is NOT Initialized!');
                }
                data = this.data[coinId];
                if (!data) {
                    throw new Error('Coin NOT FOUND: ' + coinId);
                }
                return [2 /*return*/, data];
            });
        });
    };
    /**
     * @return {?}
     */
    CoinDataProvider.prototype.getAllCoins = /**
     * @return {?}
     */
    function () {
        return this.allCoins.slice();
    };
    return CoinDataProvider;
}());
export { CoinDataProvider };
if (false) {
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.isInit;
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.ids;
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.data;
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.allCoins;
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.database;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29pbi1kYXRhLXByb3ZpZGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvZGF0YWJhc2UvY29pbi1kYXRhLXByb3ZpZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUVBLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHckM7SUFRQywwQkFBNkIsUUFBOEI7UUFBOUIsYUFBUSxHQUFSLFFBQVEsQ0FBc0I7UUFQbkQsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUVmLFFBQUcsR0FBK0IsRUFBRSxDQUFDO1FBQ3JDLFNBQUksR0FBeUIsRUFBRSxDQUFDO1FBQ2hDLGFBQVEsR0FBVyxFQUFFLENBQUM7SUFJOUIsQ0FBQzs7OztJQUVLLCtCQUFJOzs7SUFBVjs7Ozs7Ozt3QkFDQyxLQUFBLElBQUksQ0FBQTt3QkFDSCxxQkFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxFQUFBOzt3QkFEbEMsR0FBSyxRQUFRLEdBQUcsQ0FDZixTQUFpQyxDQUNqQyxDQUFDLEdBQUc7Ozs7d0JBQUUsVUFBQSxHQUFHLElBQUksT0FBQSxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBYixDQUFhLEVBQUUsQ0FBQzs7NEJBRTlCLEtBQWtCLEtBQUEsaUJBQUEsSUFBSSxDQUFDLFFBQVEsQ0FBQSw0Q0FBRTtnQ0FBdEIsR0FBRztnQ0FDYixtQkFBbUI7Z0NBQ25CLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQyxFQUFFLENBQUM7Z0NBQzlCLGlCQUFpQjtnQ0FDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDOzZCQUN4Qjs7Ozs7Ozs7O3dCQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDOzs7OztLQUNuQjs7Ozs7SUFFSyw4Q0FBbUI7Ozs7SUFBekIsVUFBMEIsTUFBYzs7Ozs7NEJBQzVCLHFCQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUFqQyxFQUFFLEdBQUcsU0FBNEI7d0JBRXZDLHNCQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEVBQUM7Ozs7S0FDNUI7Ozs7O0lBRUssb0NBQVM7Ozs7SUFBZixVQUFnQixNQUFjOzs7O2dCQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtvQkFDakIsTUFBTSxJQUFJLEtBQUssQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO2lCQUN4RDtnQkFFRCxNQUFNLEdBQUcsTUFBTSxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUV4QixFQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUM7Z0JBRTNCLElBQUksRUFBRSxLQUFLLFNBQVMsRUFBRTtvQkFDckIsTUFBTSxJQUFJLEtBQUssQ0FBQyxvQkFBb0IsR0FBRyxNQUFNLENBQUMsQ0FBQztpQkFDL0M7Z0JBRUQsc0JBQU8sRUFBRSxFQUFDOzs7S0FDVjs7Ozs7SUFFSyx3Q0FBYTs7OztJQUFuQixVQUFvQixNQUFjOzs7Ozs0QkFDcEIscUJBQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQXJDLElBQUksR0FBRyxTQUE4Qjt3QkFFM0Msc0JBQU8sSUFBSSxDQUFDLE1BQU0sRUFBQzs7OztLQUNuQjs7Ozs7SUFFSyxzQ0FBVzs7OztJQUFqQixVQUFrQixNQUFjOzs7O2dCQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtvQkFDakIsTUFBTSxJQUFJLEtBQUssQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO2lCQUN4RDtnQkFFSyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7Z0JBRTlCLElBQUksQ0FBQyxJQUFJLEVBQUU7b0JBQ1YsTUFBTSxJQUFJLEtBQUssQ0FBQyxrQkFBa0IsR0FBRyxNQUFNLENBQUMsQ0FBQztpQkFDN0M7Z0JBRUQsc0JBQU8sSUFBSSxFQUFDOzs7S0FDWjs7OztJQUVELHNDQUFXOzs7SUFBWDtRQUNDLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUM5QixDQUFDO0lBQ0YsdUJBQUM7QUFBRCxDQUFDLEFBdkVELElBdUVDOzs7Ozs7O0lBdEVBLGtDQUF1Qjs7Ozs7SUFFdkIsK0JBQTZDOzs7OztJQUM3QyxnQ0FBd0M7Ozs7O0lBQ3hDLG9DQUE4Qjs7Ozs7SUFHbEIsb0NBQStDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSUNvaW5EYXRhUHJvdmlkZXIsIElDb2luRGF0YWJhc2VTZXJ2aWNlIH0gZnJvbSAnLi4vY3VycmVuY3ktYW1vdW50L2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgQ29pbklkIH0gZnJvbSAnLi9jb2lucyc7XG5pbXBvcnQgeyBDb2luIH0gZnJvbSAnLi9tb2RlbHMvY29pbic7XG5cblxuZXhwb3J0IGNsYXNzIENvaW5EYXRhUHJvdmlkZXIgaW1wbGVtZW50cyBJQ29pbkRhdGFQcm92aWRlciB7XG5cdHByaXZhdGUgaXNJbml0ID0gZmFsc2U7XG5cblx0cHJpdmF0ZSBpZHM6IHtbc3ltYm9sOiBzdHJpbmddOiBDb2luSWR9ID0ge307XG5cdHByaXZhdGUgZGF0YToge1tpZDogbnVtYmVyXTogQ29pbn0gPSB7fTtcblx0cHJpdmF0ZSBhbGxDb2luczogQ29pbltdID0gW107XG5cblxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIHJlYWRvbmx5IGRhdGFiYXNlOiBJQ29pbkRhdGFiYXNlU2VydmljZSkge1xuXHR9XG5cblx0YXN5bmMgaW5pdCgpIHtcblx0XHR0aGlzLmFsbENvaW5zID0gKFxuXHRcdFx0YXdhaXQgdGhpcy5kYXRhYmFzZS5nZXRBbGxDb2lucygpXG5cdFx0KS5tYXAoIHJvdyA9PiBuZXcgQ29pbihyb3cpICk7XG5cblx0XHRmb3IgKGNvbnN0IHJvdyBvZiB0aGlzLmFsbENvaW5zKSB7XG5cdFx0XHQvLyBtYXAgc3ltYm9sIHRvIElEXG5cdFx0XHR0aGlzLmlkc1tyb3cuc3ltYm9sXSA9IHJvdy5pZDtcblx0XHRcdC8vIG1hcCBJRCB0byBkYXRhXG5cdFx0XHR0aGlzLmRhdGFbcm93LmlkXSA9IHJvdztcblx0XHR9XG5cblx0XHR0aGlzLmlzSW5pdCA9IHRydWU7XG5cdH1cblxuXHRhc3luYyBnZXRDb2luRGF0YUJ5U3ltYm9sKHN5bWJvbDogc3RyaW5nKTogUHJvbWlzZTxDb2luPiB7XG5cdFx0Y29uc3QgaWQgPSBhd2FpdCB0aGlzLmdldENvaW5JZChzeW1ib2wpO1xuXG5cdFx0cmV0dXJuIHRoaXMuZ2V0Q29pbkRhdGEoaWQpO1xuXHR9XG5cblx0YXN5bmMgZ2V0Q29pbklkKHN5bWJvbDogc3RyaW5nKTogUHJvbWlzZTxDb2luSWQ+IHtcblx0XHRpZiAoIXRoaXMuaXNJbml0KSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ0NvaW5EYXRhUHJvdmlkZXIgaXMgTk9UIEluaXRpYWxpemVkIScpO1xuXHRcdH1cblxuXHRcdHN5bWJvbCA9IHN5bWJvbC50b1VwcGVyQ2FzZSgpO1xuXG5cdFx0Y29uc3QgaWQgPSB0aGlzLmlkc1tzeW1ib2xdO1xuXG5cdFx0aWYgKGlkID09PSB1bmRlZmluZWQpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignQ29pbklkIE5PVCBGT1VORDogJyArIHN5bWJvbCk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIGlkO1xuXHR9XG5cblx0YXN5bmMgZ2V0Q29pblN5bWJvbChjb2luSWQ6IENvaW5JZCk6IFByb21pc2U8c3RyaW5nPiB7XG5cdFx0Y29uc3QgZGF0YSA9IGF3YWl0IHRoaXMuZ2V0Q29pbkRhdGEoY29pbklkKTtcblxuXHRcdHJldHVybiBkYXRhLnN5bWJvbDtcblx0fVxuXG5cdGFzeW5jIGdldENvaW5EYXRhKGNvaW5JZDogQ29pbklkKTogUHJvbWlzZTxDb2luPiB7XG5cdFx0aWYgKCF0aGlzLmlzSW5pdCkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdDb2luRGF0YVByb3ZpZGVyIGlzIE5PVCBJbml0aWFsaXplZCEnKTtcblx0XHR9XG5cblx0XHRjb25zdCBkYXRhID0gdGhpcy5kYXRhW2NvaW5JZF07XG5cblx0XHRpZiAoIWRhdGEpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignQ29pbiBOT1QgRk9VTkQ6ICcgKyBjb2luSWQpO1xuXHRcdH1cblxuXHRcdHJldHVybiBkYXRhO1xuXHR9XG5cblx0Z2V0QWxsQ29pbnMoKSB7XG5cdFx0cmV0dXJuIHRoaXMuYWxsQ29pbnMuc2xpY2UoKTtcblx0fVxufVxuIl19