/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/coin-database-service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { CoinRepository } from './repositories/coin-repository';
var CoinDatabaseService = /** @class */ (function () {
    function CoinDatabaseService(database) {
        this.coins = new CoinRepository(database);
    }
    /**
     * @return {?}
     */
    CoinDatabaseService.prototype.getAllCoins = /**
     * @return {?}
     */
    function () {
        return this.coins.select({
            orderBy: 'id'
        });
    };
    /**
     * @param {?} coinId
     * @return {?}
     */
    CoinDatabaseService.prototype.getCoinData = /**
     * @param {?} coinId
     * @return {?}
     */
    function (coinId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var result;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.coins.selectOneByPrimaryKey(coinId)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    return CoinDatabaseService;
}());
export { CoinDatabaseService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    CoinDatabaseService.prototype.coins;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29pbi1kYXRhYmFzZS1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvZGF0YWJhc2UvY29pbi1kYXRhYmFzZS1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQU1oRTtJQUdDLDZCQUFZLFFBQXdCO1FBQ25DLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDM0MsQ0FBQzs7OztJQUVELHlDQUFXOzs7SUFBWDtRQUNDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDeEIsT0FBTyxFQUFFLElBQUk7U0FDYixDQUFDLENBQUM7SUFDSixDQUFDOzs7OztJQUVLLHlDQUFXOzs7O0lBQWpCLFVBQWtCLE1BQWM7Ozs7OzRCQUNoQixxQkFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBdkQsTUFBTSxHQUFHLFNBQThDO3dCQUU3RCxzQkFBTyxNQUFNLEVBQUM7Ozs7S0FDZDtJQUNGLDBCQUFDO0FBQUQsQ0FBQyxBQWxCRCxJQWtCQzs7Ozs7OztJQWpCQSxvQ0FBdUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb2luUmVwb3NpdG9yeSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL2NvaW4tcmVwb3NpdG9yeSc7XG5pbXBvcnQgeyBJQ29pbkRhdGFiYXNlU2VydmljZSB9IGZyb20gJy4uL2N1cnJlbmN5LWFtb3VudC9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElNeVNRTERhdGFiYXNlIH0gZnJvbSAnLi4vLi4vZGF0YWJhc2UvZW5naW5lL215c3FsL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSVNhdmVkQ29pblJvdyB9IGZyb20gJy4vbW9kZWxzL2NvaW4nO1xuXG5cbmV4cG9ydCBjbGFzcyBDb2luRGF0YWJhc2VTZXJ2aWNlIGltcGxlbWVudHMgSUNvaW5EYXRhYmFzZVNlcnZpY2Uge1xuXHRwcml2YXRlIHJlYWRvbmx5IGNvaW5zOiBDb2luUmVwb3NpdG9yeTtcblxuXHRjb25zdHJ1Y3RvcihkYXRhYmFzZTogSU15U1FMRGF0YWJhc2UpIHtcblx0XHR0aGlzLmNvaW5zID0gbmV3IENvaW5SZXBvc2l0b3J5KGRhdGFiYXNlKTtcblx0fVxuXG5cdGdldEFsbENvaW5zKCkge1xuXHRcdHJldHVybiB0aGlzLmNvaW5zLnNlbGVjdCh7XG5cdFx0XHRvcmRlckJ5OiAnaWQnXG5cdFx0fSk7XG5cdH1cblxuXHRhc3luYyBnZXRDb2luRGF0YShjb2luSWQ6IG51bWJlcik6IFByb21pc2U8SVNhdmVkQ29pblJvdz4ge1xuXHRcdGNvbnN0IHJlc3VsdCA9IGF3YWl0IHRoaXMuY29pbnMuc2VsZWN0T25lQnlQcmltYXJ5S2V5KGNvaW5JZCk7XG5cblx0XHRyZXR1cm4gcmVzdWx0O1xuXHR9XG59XG4iXX0=