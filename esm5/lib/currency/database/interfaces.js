/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IUserAccountRow() { }
if (false) {
    /** @type {?} */
    IUserAccountRow.prototype.eos_account_name;
    /** @type {?} */
    IUserAccountRow.prototype.easy_account_public_key;
}
/**
 * @record
 */
export function IUserAccountDatabaseService() { }
if (false) {
    /**
     * @param {?} userId
     * @return {?}
     */
    IUserAccountDatabaseService.prototype.getAccountInfo = function (userId) { };
    /**
     * @param {?} eos_account
     * @return {?}
     */
    IUserAccountDatabaseService.prototype.getUserIdForEosAccount = function (eos_account) { };
    /**
     * @param {?} ezeos_id
     * @return {?}
     */
    IUserAccountDatabaseService.prototype.getUserIdForEasyAccount = function (ezeos_id) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2N1cnJlbmN5L2RhdGFiYXNlL2ludGVyZmFjZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxxQ0FHQzs7O0lBRkEsMkNBQXlCOztJQUN6QixrREFBZ0M7Ozs7O0FBR2pDLGlEQUlDOzs7Ozs7SUFIQSw2RUFBeUQ7Ozs7O0lBQ3pELDBGQUE2RDs7Ozs7SUFDN0Qsd0ZBQTJEIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBJVXNlckFjY291bnRSb3cge1xuXHRlb3NfYWNjb3VudF9uYW1lOiBzdHJpbmc7XG5cdGVhc3lfYWNjb3VudF9wdWJsaWNfa2V5OiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVVzZXJBY2NvdW50RGF0YWJhc2VTZXJ2aWNlIHtcblx0Z2V0QWNjb3VudEluZm8odXNlcklkOiBudW1iZXIpOiBQcm9taXNlPElVc2VyQWNjb3VudFJvdz47XG5cdGdldFVzZXJJZEZvckVvc0FjY291bnQoZW9zX2FjY291bnQ6IHN0cmluZyk6IFByb21pc2U8bnVtYmVyPjtcblx0Z2V0VXNlcklkRm9yRWFzeUFjY291bnQoZXplb3NfaWQ6IHN0cmluZyk6IFByb21pc2U8bnVtYmVyPjtcbn1cbiJdfQ==