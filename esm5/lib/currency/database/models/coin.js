/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/models/coin.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { PreciseDecimal } from '../../currency-amount/precise-math-numbers';
/**
 * @record
 */
export function ISavedCoinRow() { }
if (false) {
    /** @type {?} */
    ISavedCoinRow.prototype.id;
    /** @type {?} */
    ISavedCoinRow.prototype.symbol;
    /** @type {?} */
    ISavedCoinRow.prototype.precision;
    /** @type {?} */
    ISavedCoinRow.prototype.uses_memo_for_deposits;
    /** @type {?} */
    ISavedCoinRow.prototype.minimum_withdrawal_amount;
}
var Coin = /** @class */ (function () {
    function Coin(data) {
        this.id = data.id;
        this.symbol = data.symbol;
        this.precision = data.precision;
        this.usesMemoForDeposits = data.uses_memo_for_deposits == 1;
        this.minimumWithdrawalAmount = new PreciseDecimal(data.minimum_withdrawal_amount, this.precision);
    }
    Object.defineProperty(Coin.prototype, "data", {
        get: /**
         * @return {?}
         */
        function () {
            return {
                id: this.id,
                symbol: this.symbol,
                precision: this.precision,
                usesMemoForDeposits: this.usesMemoForDeposits,
                minimumWithdrawalAmount: this.minimumWithdrawalAmount.decimal
            };
        },
        enumerable: true,
        configurable: true
    });
    return Coin;
}());
export { Coin };
if (false) {
    /** @type {?} */
    Coin.prototype.id;
    /** @type {?} */
    Coin.prototype.symbol;
    /** @type {?} */
    Coin.prototype.precision;
    /** @type {?} */
    Coin.prototype.usesMemoForDeposits;
    /** @type {?} */
    Coin.prototype.minimumWithdrawalAmount;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29pbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2N1cnJlbmN5L2RhdGFiYXNlL21vZGVscy9jb2luLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRDQUE0QyxDQUFDOzs7O0FBSzVFLG1DQU1DOzs7SUFMQSwyQkFBVzs7SUFDWCwrQkFBZTs7SUFDZixrQ0FBa0I7O0lBQ2xCLCtDQUErQjs7SUFDL0Isa0RBQWtDOztBQUduQztJQU9DLGNBQVksSUFBbUI7UUFDOUIsSUFBSSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDaEMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxzQkFBc0IsSUFBSSxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksY0FBYyxDQUNoRCxJQUFJLENBQUMseUJBQXlCLEVBQzlCLElBQUksQ0FBQyxTQUFTLENBQ2QsQ0FBQztJQUNILENBQUM7SUFFRCxzQkFBSSxzQkFBSTs7OztRQUFSO1lBQ0MsT0FBTztnQkFDTixFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUU7Z0JBQ1gsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO2dCQUNuQixTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7Z0JBQ3pCLG1CQUFtQixFQUFFLElBQUksQ0FBQyxtQkFBbUI7Z0JBQzdDLHVCQUF1QixFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPO2FBQzdELENBQUM7UUFDSCxDQUFDOzs7T0FBQTtJQUNGLFdBQUM7QUFBRCxDQUFDLEFBM0JELElBMkJDOzs7O0lBMUJBLGtCQUFvQjs7SUFDcEIsc0JBQXdCOztJQUN4Qix5QkFBMkI7O0lBQzNCLG1DQUFzQzs7SUFDdEMsdUNBQWlEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUHJlY2lzZURlY2ltYWwgfSBmcm9tICcuLi8uLi9jdXJyZW5jeS1hbW91bnQvcHJlY2lzZS1tYXRoLW51bWJlcnMnO1xuaW1wb3J0IHsgQ29pbklkIH0gZnJvbSAnLi4vY29pbnMnO1xuaW1wb3J0IHsgSUN1cnJlbmN5IH0gZnJvbSAnLi4vLi4vY3VycmVuY3ktYW1vdW50L2ludGVyZmFjZXMnO1xuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSVNhdmVkQ29pblJvdyB7XG5cdGlkOiBudW1iZXI7XG5cdHN5bWJvbDogc3RyaW5nO1xuXHRwcmVjaXNpb246IG51bWJlcjtcblx0dXNlc19tZW1vX2Zvcl9kZXBvc2l0czogbnVtYmVyO1xuXHRtaW5pbXVtX3dpdGhkcmF3YWxfYW1vdW50OiBzdHJpbmc7XG59XG5cbmV4cG9ydCBjbGFzcyBDb2luIGltcGxlbWVudHMgSUN1cnJlbmN5IHtcblx0cmVhZG9ubHkgaWQ6IENvaW5JZDtcblx0cmVhZG9ubHkgc3ltYm9sOiBzdHJpbmc7XG5cdHJlYWRvbmx5IHByZWNpc2lvbjogbnVtYmVyO1xuXHRyZWFkb25seSB1c2VzTWVtb0ZvckRlcG9zaXRzOiBib29sZWFuO1xuXHRyZWFkb25seSBtaW5pbXVtV2l0aGRyYXdhbEFtb3VudDogUHJlY2lzZURlY2ltYWw7XG5cblx0Y29uc3RydWN0b3IoZGF0YTogSVNhdmVkQ29pblJvdykge1xuXHRcdHRoaXMuaWQgPSBkYXRhLmlkO1xuXHRcdHRoaXMuc3ltYm9sID0gZGF0YS5zeW1ib2w7XG5cdFx0dGhpcy5wcmVjaXNpb24gPSBkYXRhLnByZWNpc2lvbjtcblx0XHR0aGlzLnVzZXNNZW1vRm9yRGVwb3NpdHMgPSBkYXRhLnVzZXNfbWVtb19mb3JfZGVwb3NpdHMgPT0gMTtcblx0XHR0aGlzLm1pbmltdW1XaXRoZHJhd2FsQW1vdW50ID0gbmV3IFByZWNpc2VEZWNpbWFsKFxuXHRcdFx0ZGF0YS5taW5pbXVtX3dpdGhkcmF3YWxfYW1vdW50LFxuXHRcdFx0dGhpcy5wcmVjaXNpb25cblx0XHQpO1xuXHR9XG5cblx0Z2V0IGRhdGEoKSB7XG5cdFx0cmV0dXJuIHtcblx0XHRcdGlkOiB0aGlzLmlkLFxuXHRcdFx0c3ltYm9sOiB0aGlzLnN5bWJvbCxcblx0XHRcdHByZWNpc2lvbjogdGhpcy5wcmVjaXNpb24sXG5cdFx0XHR1c2VzTWVtb0ZvckRlcG9zaXRzOiB0aGlzLnVzZXNNZW1vRm9yRGVwb3NpdHMsXG5cdFx0XHRtaW5pbXVtV2l0aGRyYXdhbEFtb3VudDogdGhpcy5taW5pbXVtV2l0aGRyYXdhbEFtb3VudC5kZWNpbWFsXG5cdFx0fTtcblx0fVxufVxuIl19