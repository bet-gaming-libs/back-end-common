/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/models/withdrawal-request.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * @record
 */
export function INewWithdrawalRequestRow() { }
if (false) {
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.id;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.memo;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.coin_id;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.tx_hash;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.is_processed;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.easy_account_id;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.eos_account_name;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.withdraw_address;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.coin_decimal_amount;
}
/**
 * @record
 */
export function IFailedWithdrawalRequestRow() { }
if (false) {
    /** @type {?} */
    IFailedWithdrawalRequestRow.prototype.error_message;
}
/**
 * @record
 */
export function ISucceededWithdrawalRequest() { }
if (false) {
    /** @type {?} */
    ISucceededWithdrawalRequest.prototype.processed_at;
    /** @type {?} */
    ISucceededWithdrawalRequest.prototype.withdraw_transaction_id;
}
/**
 * @record
 */
export function IWithdrawalRequestInsufficientFunds() { }
if (false) {
    /** @type {?} */
    IWithdrawalRequestInsufficientFunds.prototype.insufficient_funds;
}
/**
 * @record
 */
export function ISavedWithdrawalRequestRow() { }
if (false) {
    /** @type {?} */
    ISavedWithdrawalRequestRow.prototype.requested_at;
}
/**
 * @record
 */
export function IWithdrawalRequest() { }
if (false) {
    /** @type {?} */
    IWithdrawalRequest.prototype.address;
    /** @type {?} */
    IWithdrawalRequest.prototype.memo;
    /**
     * @return {?}
     */
    IWithdrawalRequest.prototype.getAmount = function () { };
}
/**
 * @record
 */
export function ISavedWithdrawalRequest() { }
if (false) {
    /**
     * @return {?}
     */
    ISavedWithdrawalRequest.prototype.getRefundParams = function () { };
}
var SavedWithdrawalRequest = /** @class */ (function () {
    function SavedWithdrawalRequest(data, account, amountFactory) {
        this.account = account;
        this.amountFactory = amountFactory;
        this.userId = undefined;
        this.amount = undefined;
        this.id = data.id;
        this.coinId = data.coin_id;
        this.decimalAmount = data.coin_decimal_amount;
        this.transactionHash = data.tx_hash;
        this.address = data.withdraw_address;
        this.memo = data.memo ? data.memo : '';
        this.eosAccountName = data.eos_account_name;
        this.easyAccountId = data.easy_account_id;
        this.isEosAccount = Number(this.easyAccountId) == 0;
        this.errorMessage = data.error_message;
    }
    /**
     * @return {?}
     */
    SavedWithdrawalRequest.prototype.getDataForApi = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var amount;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getAmount()];
                    case 1:
                        amount = _a.sent();
                        return [2 /*return*/, {
                                id: this.id,
                                memo: this.memo,
                                address: this.address,
                                errorMessage: this.errorMessage,
                                coin: amount.currency.symbol,
                                amount: amount.decimal,
                            }];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    SavedWithdrawalRequest.prototype.getAmount = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!(this.amount == undefined)) return [3 /*break*/, 2];
                        _a = this;
                        return [4 /*yield*/, this.amountFactory.newAmountFromDecimalAndCoinId(this.decimalAmount, this.coinId)];
                    case 1:
                        _a.amount = _b.sent();
                        _b.label = 2;
                    case 2: return [2 /*return*/, this.amount];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    SavedWithdrawalRequest.prototype.getRefundParams = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var userId, info;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getUserId()];
                    case 1:
                        userId = _a.sent();
                        return [4 /*yield*/, this.account.getAccountInfo(userId)];
                    case 2:
                        info = _a.sent();
                        return [2 /*return*/, {
                                requestId: this.id,
                                easyAccountPublicKey: info.easy_account_public_key ?
                                    info.easy_account_public_key :
                                    '',
                                reason: this.errorMessage
                            }];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    SavedWithdrawalRequest.prototype.getUserId = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        if (!(this.userId == undefined)) return [3 /*break*/, 5];
                        _a = this;
                        if (!this.isEosAccount) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.account.getUserIdForEosAccount(this.eosAccountName)];
                    case 1:
                        _b = _c.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.account.getUserIdForEasyAccount(this.easyAccountId)];
                    case 3:
                        _b = _c.sent();
                        _c.label = 4;
                    case 4:
                        _a.userId = _b;
                        _c.label = 5;
                    case 5: return [2 /*return*/, this.userId];
                }
            });
        });
    };
    return SavedWithdrawalRequest;
}());
export { SavedWithdrawalRequest };
if (false) {
    /** @type {?} */
    SavedWithdrawalRequest.prototype.address;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.id;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.transactionHash;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.memo;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.coinId;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.decimalAmount;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.errorMessage;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.isEosAccount;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.eosAccountName;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.easyAccountId;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.userId;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.amount;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.account;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.amountFactory;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2l0aGRyYXdhbC1yZXF1ZXN0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvZGF0YWJhc2UvbW9kZWxzL3dpdGhkcmF3YWwtcmVxdWVzdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFNQSw4Q0FVQzs7O0lBVEEsc0NBQVc7O0lBQ1gsd0NBQWE7O0lBQ2IsMkNBQWdCOztJQUNoQiwyQ0FBZ0I7O0lBQ2hCLGdEQUFxQjs7SUFDckIsbURBQXdCOztJQUN4QixvREFBeUI7O0lBQ3pCLG9EQUF5Qjs7SUFDekIsdURBQTRCOzs7OztBQUc3QixpREFFQzs7O0lBREEsb0RBQXNCOzs7OztBQUd2QixpREFHQzs7O0lBRkEsbURBQW1COztJQUNuQiw4REFBZ0M7Ozs7O0FBR2pDLHlEQUVDOzs7SUFEQSxpRUFBMkI7Ozs7O0FBRzVCLGdEQUVDOzs7SUFEQSxrREFBbUI7Ozs7O0FBS3BCLHdDQUtDOzs7SUFKQSxxQ0FBZ0I7O0lBQ2hCLGtDQUFhOzs7O0lBRWIseURBQXNDOzs7OztBQUd2Qyw2Q0FFQzs7Ozs7SUFEQSxvRUFBMkQ7O0FBSTVEO0lBZ0JDLGdDQUNDLElBQWdDLEVBQ2YsT0FBb0MsRUFDcEMsYUFBcUM7UUFEckMsWUFBTyxHQUFQLE9BQU8sQ0FBNkI7UUFDcEMsa0JBQWEsR0FBYixhQUFhLENBQXdCO1FBTi9DLFdBQU0sR0FBVyxTQUFTLENBQUM7UUFDM0IsV0FBTSxHQUFvQixTQUFTLENBQUM7UUFPM0MsSUFBSSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUMzQixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztRQUU5QyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDcEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDckMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFFdkMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDNUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1FBQzFDLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQ3hDLENBQUM7Ozs7SUFFSyw4Q0FBYTs7O0lBQW5COzs7Ozs0QkFDZ0IscUJBQU0sSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFBOzt3QkFBL0IsTUFBTSxHQUFHLFNBQXNCO3dCQUVyQyxzQkFBTztnQ0FDTixFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUU7Z0NBQ1gsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO2dDQUNmLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTztnQ0FDckIsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZO2dDQUMvQixJQUFJLEVBQUUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNO2dDQUM1QixNQUFNLEVBQUUsTUFBTSxDQUFDLE9BQU87NkJBQ3RCLEVBQUM7Ozs7S0FDRjs7OztJQUVLLDBDQUFTOzs7SUFBZjs7Ozs7OzZCQUNLLENBQUEsSUFBSSxDQUFDLE1BQU0sSUFBSSxTQUFTLENBQUEsRUFBeEIsd0JBQXdCO3dCQUMzQixLQUFBLElBQUksQ0FBQTt3QkFBVSxxQkFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLDZCQUE2QixDQUNuRSxJQUFJLENBQUMsYUFBYSxFQUNsQixJQUFJLENBQUMsTUFBTSxDQUNYLEVBQUE7O3dCQUhELEdBQUssTUFBTSxHQUFHLFNBR2IsQ0FBQzs7NEJBR0gsc0JBQU8sSUFBSSxDQUFDLE1BQU0sRUFBQzs7OztLQUNuQjs7OztJQUVLLGdEQUFlOzs7SUFBckI7Ozs7OzRCQUNnQixxQkFBTSxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUE7O3dCQUEvQixNQUFNLEdBQUcsU0FBc0I7d0JBRXhCLHFCQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBaEQsSUFBSSxHQUFHLFNBQXlDO3dCQUV0RCxzQkFBTztnQ0FDTixTQUFTLEVBQUUsSUFBSSxDQUFDLEVBQUU7Z0NBQ2xCLG9CQUFvQixFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO29DQUM5QyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQztvQ0FDOUIsRUFBRTtnQ0FDUixNQUFNLEVBQUUsSUFBSSxDQUFDLFlBQVk7NkJBQ3pCLEVBQUM7Ozs7S0FDRjs7OztJQUVLLDBDQUFTOzs7SUFBZjs7Ozs7OzZCQUNLLENBQUEsSUFBSSxDQUFDLE1BQU0sSUFBSSxTQUFTLENBQUEsRUFBeEIsd0JBQXdCO3dCQUMzQixLQUFBLElBQUksQ0FBQTs2QkFDSCxJQUFJLENBQUMsWUFBWSxFQUFqQix3QkFBaUI7d0JBQ2hCLHFCQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQ3hDLElBQUksQ0FBQyxjQUFjLENBQ25CLEVBQUE7O3dCQUZELEtBQUEsU0FFQyxDQUFBOzs0QkFDRCxxQkFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUN6QyxJQUFJLENBQUMsYUFBYSxDQUNsQixFQUFBOzt3QkFGRCxLQUFBLFNBRUMsQ0FBQTs7O3dCQVBILEdBQUssTUFBTSxLQU9SLENBQUM7OzRCQUdMLHNCQUFPLElBQUksQ0FBQyxNQUFNLEVBQUM7Ozs7S0FDbkI7SUFDRiw2QkFBQztBQUFELENBQUMsQUF2RkQsSUF1RkM7Ozs7SUF0RkEseUNBQXlCOztJQUN6QixvQ0FBb0I7O0lBQ3BCLGlEQUFpQzs7SUFDakMsc0NBQXNCOztJQUN0Qix3Q0FBd0I7O0lBQ3hCLCtDQUErQjs7SUFDL0IsOENBQThCOzs7OztJQUU5Qiw4Q0FBdUM7Ozs7O0lBQ3ZDLGdEQUF3Qzs7Ozs7SUFDeEMsK0NBQXVDOzs7OztJQUV2Qyx3Q0FBbUM7Ozs7O0lBQ25DLHdDQUE0Qzs7Ozs7SUFJM0MseUNBQXFEOzs7OztJQUNyRCwrQ0FBc0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJUmVmdW5kV2l0aGRyYXdhbFJlcXVlc3RQYXJhbXMgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElVc2VyQWNjb3VudERhdGFiYXNlU2VydmljZSB9IGZyb20gJy4uL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSUN1cnJlbmN5QW1vdW50LCBJQ3VycmVuY3lBbW91bnRGYWN0b3J5IH0gZnJvbSAnLi4vLi4vY3VycmVuY3ktYW1vdW50L2ludGVyZmFjZXMnO1xuXG5cblxuZXhwb3J0IGludGVyZmFjZSBJTmV3V2l0aGRyYXdhbFJlcXVlc3RSb3cge1xuXHRpZDogc3RyaW5nO1xuXHRtZW1vOiBzdHJpbmc7XG5cdGNvaW5faWQ6IG51bWJlcjtcblx0dHhfaGFzaDogc3RyaW5nO1xuXHRpc19wcm9jZXNzZWQ6IG51bWJlcjtcblx0ZWFzeV9hY2NvdW50X2lkOiBzdHJpbmc7XG5cdGVvc19hY2NvdW50X25hbWU6IHN0cmluZztcblx0d2l0aGRyYXdfYWRkcmVzczogc3RyaW5nO1xuXHRjb2luX2RlY2ltYWxfYW1vdW50OiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUZhaWxlZFdpdGhkcmF3YWxSZXF1ZXN0Um93IGV4dGVuZHMgSU5ld1dpdGhkcmF3YWxSZXF1ZXN0Um93IHtcblx0ZXJyb3JfbWVzc2FnZTogc3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElTdWNjZWVkZWRXaXRoZHJhd2FsUmVxdWVzdCBleHRlbmRzIElOZXdXaXRoZHJhd2FsUmVxdWVzdFJvdyAge1xuXHRwcm9jZXNzZWRfYXQ6IERhdGU7XG5cdHdpdGhkcmF3X3RyYW5zYWN0aW9uX2lkOiBudW1iZXI7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVdpdGhkcmF3YWxSZXF1ZXN0SW5zdWZmaWNpZW50RnVuZHMgZXh0ZW5kcyBJTmV3V2l0aGRyYXdhbFJlcXVlc3RSb3cge1xuXHRpbnN1ZmZpY2llbnRfZnVuZHM6IG51bWJlcjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJU2F2ZWRXaXRoZHJhd2FsUmVxdWVzdFJvdyBleHRlbmRzIElGYWlsZWRXaXRoZHJhd2FsUmVxdWVzdFJvdywgSVN1Y2NlZWRlZFdpdGhkcmF3YWxSZXF1ZXN0LCBJV2l0aGRyYXdhbFJlcXVlc3RJbnN1ZmZpY2llbnRGdW5kcyB7XG5cdHJlcXVlc3RlZF9hdDogRGF0ZTtcbn1cblxuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSVdpdGhkcmF3YWxSZXF1ZXN0IHtcblx0YWRkcmVzczogc3RyaW5nO1xuXHRtZW1vOiBzdHJpbmc7XG5cblx0Z2V0QW1vdW50KCk6IFByb21pc2U8SUN1cnJlbmN5QW1vdW50Pjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJU2F2ZWRXaXRoZHJhd2FsUmVxdWVzdCBleHRlbmRzIElXaXRoZHJhd2FsUmVxdWVzdCB7XG5cdGdldFJlZnVuZFBhcmFtcygpOiBQcm9taXNlPElSZWZ1bmRXaXRoZHJhd2FsUmVxdWVzdFBhcmFtcz47XG59XG5cblxuZXhwb3J0IGNsYXNzIFNhdmVkV2l0aGRyYXdhbFJlcXVlc3QgaW1wbGVtZW50cyBJU2F2ZWRXaXRoZHJhd2FsUmVxdWVzdCB7XG5cdHJlYWRvbmx5IGFkZHJlc3M6IHN0cmluZztcblx0cmVhZG9ubHkgaWQ6IHN0cmluZztcblx0cmVhZG9ubHkgdHJhbnNhY3Rpb25IYXNoOiBzdHJpbmc7XG5cdHJlYWRvbmx5IG1lbW86IHN0cmluZztcblx0cmVhZG9ubHkgY29pbklkOiBudW1iZXI7XG5cdHJlYWRvbmx5IGRlY2ltYWxBbW91bnQ6IHN0cmluZztcblx0cmVhZG9ubHkgZXJyb3JNZXNzYWdlOiBzdHJpbmc7XG5cblx0cHJpdmF0ZSByZWFkb25seSBpc0Vvc0FjY291bnQ6IGJvb2xlYW47XG5cdHByaXZhdGUgcmVhZG9ubHkgZW9zQWNjb3VudE5hbWU6IHN0cmluZztcblx0cHJpdmF0ZSByZWFkb25seSBlYXN5QWNjb3VudElkOiBzdHJpbmc7XG5cblx0cHJpdmF0ZSB1c2VySWQ6IG51bWJlciA9IHVuZGVmaW5lZDtcblx0cHJpdmF0ZSBhbW91bnQ6IElDdXJyZW5jeUFtb3VudCA9IHVuZGVmaW5lZDtcblxuXHRjb25zdHJ1Y3Rvcihcblx0XHRkYXRhOiBJU2F2ZWRXaXRoZHJhd2FsUmVxdWVzdFJvdyxcblx0XHRwcml2YXRlIHJlYWRvbmx5IGFjY291bnQ6IElVc2VyQWNjb3VudERhdGFiYXNlU2VydmljZSxcblx0XHRwcml2YXRlIHJlYWRvbmx5IGFtb3VudEZhY3Rvcnk6IElDdXJyZW5jeUFtb3VudEZhY3Rvcnlcblx0KSB7XG5cdFx0dGhpcy5pZCA9IGRhdGEuaWQ7XG5cdFx0dGhpcy5jb2luSWQgPSBkYXRhLmNvaW5faWQ7XG5cdFx0dGhpcy5kZWNpbWFsQW1vdW50ID0gZGF0YS5jb2luX2RlY2ltYWxfYW1vdW50O1xuXG5cdFx0dGhpcy50cmFuc2FjdGlvbkhhc2ggPSBkYXRhLnR4X2hhc2g7XG5cdFx0dGhpcy5hZGRyZXNzID0gZGF0YS53aXRoZHJhd19hZGRyZXNzO1xuXHRcdHRoaXMubWVtbyA9IGRhdGEubWVtbyA/IGRhdGEubWVtbyA6ICcnO1xuXG5cdFx0dGhpcy5lb3NBY2NvdW50TmFtZSA9IGRhdGEuZW9zX2FjY291bnRfbmFtZTtcblx0XHR0aGlzLmVhc3lBY2NvdW50SWQgPSBkYXRhLmVhc3lfYWNjb3VudF9pZDtcblx0XHR0aGlzLmlzRW9zQWNjb3VudCA9IE51bWJlcih0aGlzLmVhc3lBY2NvdW50SWQpID09IDA7XG5cdFx0dGhpcy5lcnJvck1lc3NhZ2UgPSBkYXRhLmVycm9yX21lc3NhZ2U7XG5cdH1cblxuXHRhc3luYyBnZXREYXRhRm9yQXBpKCkge1xuXHRcdGNvbnN0IGFtb3VudCA9IGF3YWl0IHRoaXMuZ2V0QW1vdW50KCk7XG5cblx0XHRyZXR1cm4ge1xuXHRcdFx0aWQ6IHRoaXMuaWQsXG5cdFx0XHRtZW1vOiB0aGlzLm1lbW8sXG5cdFx0XHRhZGRyZXNzOiB0aGlzLmFkZHJlc3MsXG5cdFx0XHRlcnJvck1lc3NhZ2U6IHRoaXMuZXJyb3JNZXNzYWdlLFxuXHRcdFx0Y29pbjogYW1vdW50LmN1cnJlbmN5LnN5bWJvbCxcblx0XHRcdGFtb3VudDogYW1vdW50LmRlY2ltYWwsXG5cdFx0fTtcblx0fVxuXG5cdGFzeW5jIGdldEFtb3VudCgpOiBQcm9taXNlPElDdXJyZW5jeUFtb3VudD4ge1xuXHRcdGlmICh0aGlzLmFtb3VudCA9PSB1bmRlZmluZWQpIHtcblx0XHRcdHRoaXMuYW1vdW50ID0gYXdhaXQgdGhpcy5hbW91bnRGYWN0b3J5Lm5ld0Ftb3VudEZyb21EZWNpbWFsQW5kQ29pbklkKFxuXHRcdFx0XHR0aGlzLmRlY2ltYWxBbW91bnQsXG5cdFx0XHRcdHRoaXMuY29pbklkXG5cdFx0XHQpO1xuXHRcdH1cblxuXHRcdHJldHVybiB0aGlzLmFtb3VudDtcblx0fVxuXG5cdGFzeW5jIGdldFJlZnVuZFBhcmFtcygpOiBQcm9taXNlPElSZWZ1bmRXaXRoZHJhd2FsUmVxdWVzdFBhcmFtcz4ge1xuXHRcdGNvbnN0IHVzZXJJZCA9IGF3YWl0IHRoaXMuZ2V0VXNlcklkKCk7XG5cblx0XHRjb25zdCBpbmZvID0gYXdhaXQgdGhpcy5hY2NvdW50LmdldEFjY291bnRJbmZvKHVzZXJJZCk7XG5cblx0XHRyZXR1cm4ge1xuXHRcdFx0cmVxdWVzdElkOiB0aGlzLmlkLFxuXHRcdFx0ZWFzeUFjY291bnRQdWJsaWNLZXk6IGluZm8uZWFzeV9hY2NvdW50X3B1YmxpY19rZXkgP1xuXHRcdFx0XHRcdFx0XHRcdFx0aW5mby5lYXN5X2FjY291bnRfcHVibGljX2tleSA6XG5cdFx0XHRcdFx0XHRcdFx0XHQnJyxcblx0XHRcdHJlYXNvbjogdGhpcy5lcnJvck1lc3NhZ2Vcblx0XHR9O1xuXHR9XG5cblx0YXN5bmMgZ2V0VXNlcklkKCk6IFByb21pc2U8bnVtYmVyPiB7XG5cdFx0aWYgKHRoaXMudXNlcklkID09IHVuZGVmaW5lZCkge1xuXHRcdFx0dGhpcy51c2VySWQgPVxuXHRcdFx0XHR0aGlzLmlzRW9zQWNjb3VudCA/XG5cdFx0XHRcdFx0YXdhaXQgdGhpcy5hY2NvdW50LmdldFVzZXJJZEZvckVvc0FjY291bnQoXG5cdFx0XHRcdFx0XHR0aGlzLmVvc0FjY291bnROYW1lXG5cdFx0XHRcdFx0KSA6XG5cdFx0XHRcdFx0YXdhaXQgdGhpcy5hY2NvdW50LmdldFVzZXJJZEZvckVhc3lBY2NvdW50KFxuXHRcdFx0XHRcdFx0dGhpcy5lYXN5QWNjb3VudElkXG5cdFx0XHRcdFx0KTtcblx0XHR9XG5cblx0XHRyZXR1cm4gdGhpcy51c2VySWQ7XG5cdH1cbn1cbiJdfQ==