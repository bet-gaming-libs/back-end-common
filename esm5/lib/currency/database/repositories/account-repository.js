/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/repositories/account-repository.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { MySQLRepository } from '../../../database/engine/mysql/mysql-repository';
/** @type {?} */
var FieldNames = {
    ID: 'id',
    EOS_ACCOUNT: 'eos_account',
    EZEOS_ID: 'ezeos_id',
};
var AccountRepository = /** @class */ (function (_super) {
    tslib_1.__extends(AccountRepository, _super);
    function AccountRepository(db) {
        return _super.call(this, db, 'account', [
            FieldNames.ID,
            FieldNames.EOS_ACCOUNT,
            FieldNames.EZEOS_ID
        ]) || this;
    }
    return AccountRepository;
}(MySQLRepository));
export { AccountRepository };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC1yZXBvc2l0b3J5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvZGF0YWJhc2UvcmVwb3NpdG9yaWVzL2FjY291bnQtcmVwb3NpdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saURBQWlELENBQUM7O0lBTTVFLFVBQVUsR0FBRztJQUNsQixFQUFFLEVBQUUsSUFBSTtJQUNSLFdBQVcsRUFBRSxhQUFhO0lBQzFCLFFBQVEsRUFBRSxVQUFVO0NBQ3BCO0FBSUQ7SUFBdUMsNkNBQWlDO0lBQ3ZFLDJCQUFZLEVBQWtCO2VBQzdCLGtCQUNDLEVBQUUsRUFDRixTQUFTLEVBQ1Q7WUFDQyxVQUFVLENBQUMsRUFBRTtZQUNiLFVBQVUsQ0FBQyxXQUFXO1lBQ3RCLFVBQVUsQ0FBQyxRQUFRO1NBQ25CLENBQ0Q7SUFDRixDQUFDO0lBQ0Ysd0JBQUM7QUFBRCxDQUFDLEFBWkQsQ0FBdUMsZUFBZSxHQVlyRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE15U1FMUmVwb3NpdG9yeSB9IGZyb20gJy4uLy4uLy4uL2RhdGFiYXNlL2VuZ2luZS9teXNxbC9teXNxbC1yZXBvc2l0b3J5JztcbmltcG9ydCB7IElNeVNRTERhdGFiYXNlIH0gZnJvbSAnLi4vLi4vLi4vZGF0YWJhc2UvZW5naW5lL215c3FsL2ludGVyZmFjZXMnO1xuaW1wb3J0IHtJU2F2ZWRBY2NvdW50Um93fSBmcm9tICcuLi9tb2RlbHMvYWNjb3VudCc7XG5cblxuXG5jb25zdCBGaWVsZE5hbWVzID0ge1xuXHRJRDogJ2lkJyxcblx0RU9TX0FDQ09VTlQ6ICdlb3NfYWNjb3VudCcsXG5cdEVaRU9TX0lEOiAnZXplb3NfaWQnLFxufTtcblxuXG5cbmV4cG9ydCBjbGFzcyBBY2NvdW50UmVwb3NpdG9yeSBleHRlbmRzIE15U1FMUmVwb3NpdG9yeTxJU2F2ZWRBY2NvdW50Um93PiB7XG5cdGNvbnN0cnVjdG9yKGRiOiBJTXlTUUxEYXRhYmFzZSkge1xuXHRcdHN1cGVyKFxuXHRcdFx0ZGIsXG5cdFx0XHQnYWNjb3VudCcsXG5cdFx0XHRbXG5cdFx0XHRcdEZpZWxkTmFtZXMuSUQsXG5cdFx0XHRcdEZpZWxkTmFtZXMuRU9TX0FDQ09VTlQsXG5cdFx0XHRcdEZpZWxkTmFtZXMuRVpFT1NfSURcblx0XHRcdF1cblx0XHQpO1xuXHR9XG59XG4iXX0=