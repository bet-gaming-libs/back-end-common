/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/repositories/coin-repository.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { CommonFieldNames } from '../models/common';
import { MySQLRepository } from '../../../database/engine/mysql/mysql-repository';
/** @type {?} */
var FieldNames = {
    SYMBOL: 'symbol',
    PRECISION: 'precision',
    USES_MEMO_FOR_DEPOSITS: 'uses_memo_for_deposits',
    MINIMUM_WITHDRAWAL_AMOUNT: 'minimum_withdrawal_amount'
};
var CoinRepository = /** @class */ (function (_super) {
    tslib_1.__extends(CoinRepository, _super);
    function CoinRepository(database) {
        return _super.call(this, database, 'coin', [
            CommonFieldNames.ID,
            FieldNames.SYMBOL,
            FieldNames.PRECISION,
            FieldNames.USES_MEMO_FOR_DEPOSITS,
            FieldNames.MINIMUM_WITHDRAWAL_AMOUNT
        ]) || this;
    }
    return CoinRepository;
}(MySQLRepository));
export { CoinRepository };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29pbi1yZXBvc2l0b3J5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvZGF0YWJhc2UvcmVwb3NpdG9yaWVzL2NvaW4tcmVwb3NpdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNwRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saURBQWlELENBQUM7O0lBSzVFLFVBQVUsR0FBRztJQUNsQixNQUFNLEVBQUUsUUFBUTtJQUNoQixTQUFTLEVBQUUsV0FBVztJQUN0QixzQkFBc0IsRUFBRSx3QkFBd0I7SUFDaEQseUJBQXlCLEVBQUUsMkJBQTJCO0NBQ3REO0FBR0Q7SUFBb0MsMENBQThCO0lBQ2pFLHdCQUFZLFFBQXdCO2VBQ25DLGtCQUNDLFFBQVEsRUFDUixNQUFNLEVBQ047WUFDQyxnQkFBZ0IsQ0FBQyxFQUFFO1lBQ25CLFVBQVUsQ0FBQyxNQUFNO1lBQ2pCLFVBQVUsQ0FBQyxTQUFTO1lBQ3BCLFVBQVUsQ0FBQyxzQkFBc0I7WUFDakMsVUFBVSxDQUFDLHlCQUF5QjtTQUNwQyxDQUNEO0lBQ0YsQ0FBQztJQUNGLHFCQUFDO0FBQUQsQ0FBQyxBQWRELENBQW9DLGVBQWUsR0FjbEQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21tb25GaWVsZE5hbWVzIH0gZnJvbSAnLi4vbW9kZWxzL2NvbW1vbic7XG5pbXBvcnQgeyBNeVNRTFJlcG9zaXRvcnkgfSBmcm9tICcuLi8uLi8uLi9kYXRhYmFzZS9lbmdpbmUvbXlzcWwvbXlzcWwtcmVwb3NpdG9yeSc7XG5pbXBvcnQgeyBJTXlTUUxEYXRhYmFzZSB9IGZyb20gJy4uLy4uLy4uL2RhdGFiYXNlL2VuZ2luZS9teXNxbC9pbnRlcmZhY2VzJztcbmltcG9ydCB7IElTYXZlZENvaW5Sb3cgfSBmcm9tICcuLi9tb2RlbHMvY29pbic7XG5cblxuY29uc3QgRmllbGROYW1lcyA9IHtcblx0U1lNQk9MOiAnc3ltYm9sJyxcblx0UFJFQ0lTSU9OOiAncHJlY2lzaW9uJyxcblx0VVNFU19NRU1PX0ZPUl9ERVBPU0lUUzogJ3VzZXNfbWVtb19mb3JfZGVwb3NpdHMnLFxuXHRNSU5JTVVNX1dJVEhEUkFXQUxfQU1PVU5UOiAnbWluaW11bV93aXRoZHJhd2FsX2Ftb3VudCdcbn07XG5cblxuZXhwb3J0IGNsYXNzIENvaW5SZXBvc2l0b3J5IGV4dGVuZHMgTXlTUUxSZXBvc2l0b3J5PElTYXZlZENvaW5Sb3c+IHtcblx0Y29uc3RydWN0b3IoZGF0YWJhc2U6IElNeVNRTERhdGFiYXNlKSB7XG5cdFx0c3VwZXIoXG5cdFx0XHRkYXRhYmFzZSxcblx0XHRcdCdjb2luJyxcblx0XHRcdFtcblx0XHRcdFx0Q29tbW9uRmllbGROYW1lcy5JRCxcblx0XHRcdFx0RmllbGROYW1lcy5TWU1CT0wsXG5cdFx0XHRcdEZpZWxkTmFtZXMuUFJFQ0lTSU9OLFxuXHRcdFx0XHRGaWVsZE5hbWVzLlVTRVNfTUVNT19GT1JfREVQT1NJVFMsXG5cdFx0XHRcdEZpZWxkTmFtZXMuTUlOSU1VTV9XSVRIRFJBV0FMX0FNT1VOVFxuXHRcdFx0XSxcblx0XHQpO1xuXHR9XG59XG4iXX0=