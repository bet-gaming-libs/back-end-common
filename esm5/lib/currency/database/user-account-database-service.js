/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/user-account-database-service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { AccountRepository } from './repositories/account-repository';
var UserAccountDatabaseService = /** @class */ (function () {
    function UserAccountDatabaseService(database) {
        this.database = database;
        this.accountRepository = new AccountRepository(this.database);
    }
    /**
     * @param {?} userId
     * @return {?}
     */
    UserAccountDatabaseService.prototype.getAccountInfo = /**
     * @param {?} userId
     * @return {?}
     */
    function (userId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var rows;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.database.builder.rawQuery("\n            SELECT\n                eos_account AS eos_account_name,\n                public_key AS easy_account_public_key\n\n            FROM\n                account\n                LEFT JOIN account_ezeos ON account.id = account_id\n\n            WHERE\n                account.id = " + userId + ";\n        ").execute()];
                    case 1:
                        rows = _a.sent();
                        return [2 /*return*/, rows[0]];
                }
            });
        });
    };
    /**
     * @param {?} eos_account
     * @return {?}
     */
    UserAccountDatabaseService.prototype.getUserIdForEosAccount = /**
     * @param {?} eos_account
     * @return {?}
     */
    function (eos_account) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var row, insertId;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.accountRepository.selectOne({
                            whereMap: { eos_account: eos_account },
                        })];
                    case 1:
                        row = _a.sent();
                        if (!row) return [3 /*break*/, 2];
                        return [2 /*return*/, row.id];
                    case 2: return [4 /*yield*/, this.accountRepository.insert({ eos_account: eos_account })];
                    case 3:
                        insertId = (_a.sent()).insertId;
                        return [2 /*return*/, insertId];
                }
            });
        });
    };
    /**
     * @param {?} ezeos_id
     * @return {?}
     */
    UserAccountDatabaseService.prototype.getUserIdForEasyAccount = /**
     * @param {?} ezeos_id
     * @return {?}
     */
    function (ezeos_id) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var rows;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.accountRepository.select({
                            whereMap: { ezeos_id: ezeos_id },
                        })];
                    case 1:
                        rows = _a.sent();
                        if (rows.length == 0) {
                            throw new Error('User ID NOT FOUND for Easy Account ID: ' + ezeos_id);
                        }
                        return [2 /*return*/, rows[0].id];
                }
            });
        });
    };
    return UserAccountDatabaseService;
}());
export { UserAccountDatabaseService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    UserAccountDatabaseService.prototype.accountRepository;
    /**
     * @type {?}
     * @protected
     */
    UserAccountDatabaseService.prototype.database;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1hY2NvdW50LWRhdGFiYXNlLXNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9jdXJyZW5jeS9kYXRhYmFzZS91c2VyLWFjY291bnQtZGF0YWJhc2Utc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFFQSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUd0RTtJQUdDLG9DQUErQixRQUF1QjtRQUF2QixhQUFRLEdBQVIsUUFBUSxDQUFlO1FBQ3JELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMvRCxDQUFDOzs7OztJQUVLLG1EQUFjOzs7O0lBQXBCLFVBQXFCLE1BQWM7Ozs7OzRCQUNyQixxQkFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQW9CLHVTQVV4QyxNQUFNLGdCQUM1QixDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUE7O3dCQVhaLElBQUksR0FBRyxTQVdLO3dCQUVsQixzQkFBTyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUM7Ozs7S0FDZjs7Ozs7SUFFSywyREFBc0I7Ozs7SUFBNUIsVUFBNkIsV0FBbUI7Ozs7OzRCQUNuQyxxQkFBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDOzRCQUNsRCxRQUFRLEVBQUUsRUFBRSxXQUFXLGFBQUEsRUFBRTt5QkFDekIsQ0FBQyxFQUFBOzt3QkFGSSxHQUFHLEdBQUcsU0FFVjs2QkFFRSxHQUFHLEVBQUgsd0JBQUc7d0JBQ04sc0JBQU8sR0FBRyxDQUFDLEVBQUUsRUFBQzs0QkFFTyxxQkFBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLEVBQUUsV0FBVyxhQUFBLEVBQUMsQ0FBQyxFQUFBOzt3QkFBaEUsUUFBUSxHQUFLLENBQUEsU0FBbUQsQ0FBQSxTQUF4RDt3QkFFaEIsc0JBQU8sUUFBUSxFQUFDOzs7O0tBRWpCOzs7OztJQUVLLDREQUF1Qjs7OztJQUE3QixVQUE4QixRQUFnQjs7Ozs7NEJBQ2hDLHFCQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUM7NEJBQ2hELFFBQVEsRUFBRSxFQUFFLFFBQVEsVUFBQSxFQUFFO3lCQUN0QixDQUFDLEVBQUE7O3dCQUZJLElBQUksR0FBRyxTQUVYO3dCQUVGLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7NEJBQ3JCLE1BQU0sSUFBSSxLQUFLLENBQUMseUNBQXlDLEdBQUcsUUFBUSxDQUFDLENBQUM7eUJBQ3RFO3dCQUVELHNCQUFPLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUM7Ozs7S0FDbEI7SUFDRixpQ0FBQztBQUFELENBQUMsQUFqREQsSUFpREM7Ozs7Ozs7SUFoREEsdURBQXNEOzs7OztJQUUxQyw4Q0FBMEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNeVNRTERhdGFiYXNlIH0gZnJvbSAnLi4vLi4vZGF0YWJhc2UvZW5naW5lL215c3FsL215c3FsLWRhdGFiYXNlJztcbmltcG9ydCB7IElVc2VyQWNjb3VudERhdGFiYXNlU2VydmljZSwgSVVzZXJBY2NvdW50Um93IH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7IEFjY291bnRSZXBvc2l0b3J5IH0gZnJvbSAnLi9yZXBvc2l0b3JpZXMvYWNjb3VudC1yZXBvc2l0b3J5JztcblxuXG5leHBvcnQgY2xhc3MgVXNlckFjY291bnREYXRhYmFzZVNlcnZpY2UgaW1wbGVtZW50cyBJVXNlckFjY291bnREYXRhYmFzZVNlcnZpY2Uge1xuXHRwcml2YXRlIHJlYWRvbmx5IGFjY291bnRSZXBvc2l0b3J5OiBBY2NvdW50UmVwb3NpdG9yeTtcblxuXHRjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgcmVhZG9ubHkgZGF0YWJhc2U6IE15U1FMRGF0YWJhc2UpIHtcblx0XHR0aGlzLmFjY291bnRSZXBvc2l0b3J5ID0gbmV3IEFjY291bnRSZXBvc2l0b3J5KHRoaXMuZGF0YWJhc2UpO1xuXHR9XG5cblx0YXN5bmMgZ2V0QWNjb3VudEluZm8odXNlcklkOiBudW1iZXIpIHtcblx0XHRjb25zdCByb3dzID0gYXdhaXQgdGhpcy5kYXRhYmFzZS5idWlsZGVyLnJhd1F1ZXJ5PElVc2VyQWNjb3VudFJvd1tdPihgXG4gICAgICAgICAgICBTRUxFQ1RcbiAgICAgICAgICAgICAgICBlb3NfYWNjb3VudCBBUyBlb3NfYWNjb3VudF9uYW1lLFxuICAgICAgICAgICAgICAgIHB1YmxpY19rZXkgQVMgZWFzeV9hY2NvdW50X3B1YmxpY19rZXlcblxuICAgICAgICAgICAgRlJPTVxuICAgICAgICAgICAgICAgIGFjY291bnRcbiAgICAgICAgICAgICAgICBMRUZUIEpPSU4gYWNjb3VudF9lemVvcyBPTiBhY2NvdW50LmlkID0gYWNjb3VudF9pZFxuXG4gICAgICAgICAgICBXSEVSRVxuICAgICAgICAgICAgICAgIGFjY291bnQuaWQgPSAke3VzZXJJZH07XG4gICAgICAgIGApLmV4ZWN1dGUoKTtcblxuXHRcdHJldHVybiByb3dzWzBdO1xuXHR9XG5cblx0YXN5bmMgZ2V0VXNlcklkRm9yRW9zQWNjb3VudChlb3NfYWNjb3VudDogc3RyaW5nKTogUHJvbWlzZTxudW1iZXI+IHtcblx0XHRjb25zdCByb3cgPSBhd2FpdCB0aGlzLmFjY291bnRSZXBvc2l0b3J5LnNlbGVjdE9uZSh7XG5cdFx0XHR3aGVyZU1hcDogeyBlb3NfYWNjb3VudCB9LFxuXHRcdH0pO1xuXG5cdFx0aWYgKHJvdykge1xuXHRcdFx0cmV0dXJuIHJvdy5pZDtcblx0XHR9IGVsc2Uge1xuXHRcdFx0Y29uc3QgeyBpbnNlcnRJZCB9ID0gYXdhaXQgdGhpcy5hY2NvdW50UmVwb3NpdG9yeS5pbnNlcnQoeyBlb3NfYWNjb3VudH0pO1xuXG5cdFx0XHRyZXR1cm4gaW5zZXJ0SWQ7XG5cdFx0fVxuXHR9XG5cblx0YXN5bmMgZ2V0VXNlcklkRm9yRWFzeUFjY291bnQoZXplb3NfaWQ6IHN0cmluZyk6IFByb21pc2U8bnVtYmVyPiB7XG5cdFx0Y29uc3Qgcm93cyA9IGF3YWl0IHRoaXMuYWNjb3VudFJlcG9zaXRvcnkuc2VsZWN0KHtcblx0XHRcdHdoZXJlTWFwOiB7IGV6ZW9zX2lkIH0sXG5cdFx0fSk7XG5cblx0XHRpZiAocm93cy5sZW5ndGggPT0gMCkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdVc2VyIElEIE5PVCBGT1VORCBmb3IgRWFzeSBBY2NvdW50IElEOiAnICsgZXplb3NfaWQpO1xuXHRcdH1cblxuXHRcdHJldHVybiByb3dzWzBdLmlkO1xuXHR9XG59XG4iXX0=