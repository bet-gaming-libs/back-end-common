/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/mnemonic/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var MemoPrefix = {
    EOSBET: "EOSBET_",
    EARNBET: "EARNBET_",
    XRP: "EARNBET_XRP_",
};
export { MemoPrefix };
/**
 * @record
 */
export function IMnemonicUtility() { }
if (false) {
    /** @type {?} */
    IMnemonicUtility.prototype.words;
    /**
     * @param {?} userId
     * @param {?} coinId
     * @param {?} prefix
     * @return {?}
     */
    IMnemonicUtility.prototype.getDepositMemo = function (userId, coinId, prefix) { };
    /**
     * @param {?} coinId
     * @return {?}
     */
    IMnemonicUtility.prototype.getNodeForChangeAddress = function (coinId) { };
    /**
     * @param {?} coinId
     * @param {?} userId
     * @return {?}
     */
    IMnemonicUtility.prototype.getNodeForDepositAddress = function (coinId, userId) { };
}
/**
 * @record
 */
export function IMnemonicUtilityConfig() { }
if (false) {
    /** @type {?} */
    IMnemonicUtilityConfig.prototype.mnemonicPhrase;
    /** @type {?} */
    IMnemonicUtilityConfig.prototype.blockchainNetwork;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2N1cnJlbmN5L21uZW1vbmljL2ludGVyZmFjZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBTUEsSUFBWSxVQUFVO0lBQ3JCLE1BQU0sV0FBWTtJQUNsQixPQUFPLFlBQWE7SUFDcEIsR0FBRyxnQkFBaUI7RUFDcEI7Ozs7O0FBRUQsc0NBT0M7OztJQU5BLGlDQUFjOzs7Ozs7O0lBRWQsa0ZBQTJFOzs7OztJQUUzRSwyRUFBOEQ7Ozs7OztJQUM5RCxvRkFBK0U7Ozs7O0FBR2hGLDRDQUdDOzs7SUFGQSxnREFBdUI7O0lBQ3ZCLG1EQUEwQiIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IHsgYmlwMzIgfSBmcm9tICdiaXRjb2luanMtbGliJztcblxuaW1wb3J0IHsgQ29pbklkIH0gZnJvbSAnLi4vZGF0YWJhc2UvY29pbnMnO1xuXG5cbmV4cG9ydCBlbnVtIE1lbW9QcmVmaXgge1xuXHRFT1NCRVQgPSAnRU9TQkVUXycsXG5cdEVBUk5CRVQgPSAnRUFSTkJFVF8nLFxuXHRYUlAgPSAnRUFSTkJFVF9YUlBfJ1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElNbmVtb25pY1V0aWxpdHkge1xuXHR3b3Jkczogc3RyaW5nO1xuXG5cdGdldERlcG9zaXRNZW1vKHVzZXJJZDogbnVtYmVyLCBjb2luSWQ6IENvaW5JZCwgcHJlZml4OiBNZW1vUHJlZml4KTogc3RyaW5nO1xuXG5cdGdldE5vZGVGb3JDaGFuZ2VBZGRyZXNzKGNvaW5JZDogQ29pbklkKTogYmlwMzIuQklQMzJJbnRlcmZhY2U7XG5cdGdldE5vZGVGb3JEZXBvc2l0QWRkcmVzcyhjb2luSWQ6IENvaW5JZCwgdXNlcklkOiBudW1iZXIpOiBiaXAzMi5CSVAzMkludGVyZmFjZTtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJTW5lbW9uaWNVdGlsaXR5Q29uZmlnIHtcblx0bW5lbW9uaWNQaHJhc2U6IHN0cmluZztcblx0YmxvY2tjaGFpbk5ldHdvcms6IHN0cmluZztcbn1cbiJdfQ==