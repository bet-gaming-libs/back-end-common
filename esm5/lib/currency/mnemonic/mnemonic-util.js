/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/mnemonic/mnemonic-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import * as bs58 from 'bs58';
import * as bip39 from 'bip39';
import * as bip32 from 'bip32';
import * as bitcoin from 'bitcoinjs-lib';
import { CoinId } from '../database/coins';
var MnemonicUtility = /** @class */ (function () {
    function MnemonicUtility(_words, network) {
        this._words = _words;
        this.network = network;
        this.isInit = false;
        this.root = undefined;
    }
    Object.defineProperty(MnemonicUtility.prototype, "words", {
        get: /**
         * @return {?}
         */
        function () {
            return this._words;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    MnemonicUtility.prototype.init = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var seed;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.isInit) {
                            return [2 /*return*/];
                        }
                        this.isInit = true;
                        return [4 /*yield*/, bip39.mnemonicToSeed(this.words)];
                    case 1:
                        seed = _a.sent();
                        this.root = bip32.fromSeed(seed, this.network);
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} coinId
     * @return {?}
     */
    MnemonicUtility.prototype.getNodeForChangeAddress = /**
     * @param {?} coinId
     * @return {?}
     */
    function (coinId) {
        /** @type {?} */
        var basePath = getBasePath(coinId);
        // console.log(basePath);
        switch (coinId) {
            case CoinId.BTC:
            case CoinId.LTC:
            case CoinId.BCH:
                basePath = basePath.substr(0, basePath.length - 2) + '1/';
                break;
        }
        /** @type {?} */
        var path = basePath + '0';
        // console.log(path);
        /** @type {?} */
        var node = this.root.derivePath(path);
        return node;
    };
    /**
     * @param {?} coinId
     * @param {?} userId
     * @return {?}
     */
    MnemonicUtility.prototype.getNodeForDepositAddress = /**
     * @param {?} coinId
     * @param {?} userId
     * @return {?}
     */
    function (coinId, userId) {
        /** @type {?} */
        var path = getBasePath(coinId) + userId;
        return this.root.derivePath(path);
    };
    /**
     * @param {?} userId
     * @param {?} coinId
     * @param {?} prefix
     * @return {?}
     */
    MnemonicUtility.prototype.getDepositMemo = /**
     * @param {?} userId
     * @param {?} coinId
     * @param {?} prefix
     * @return {?}
     */
    function (userId, coinId, prefix) {
        /** @type {?} */
        var node = this.getNodeForDepositAddress(coinId, userId);
        /** @type {?} */
        var memo = bs58.encode(node.publicKey.slice(0, 12)).toLowerCase();
        return "" + prefix + memo;
    };
    return MnemonicUtility;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    MnemonicUtility.prototype.isInit;
    /**
     * @type {?}
     * @private
     */
    MnemonicUtility.prototype.root;
    /**
     * @type {?}
     * @private
     */
    MnemonicUtility.prototype._words;
    /**
     * @type {?}
     * @private
     */
    MnemonicUtility.prototype.network;
}
/**
 * the MnemonicUtility class should be a singleton: **
 * @type {?}
 */
var mnemonicUtility;
/** @type {?} */
var allowedNetworkNames = ['mainnet', 'testnet', 'regtest'];
/**
 * @param {?} config
 * @return {?}
 * @this {*}
 */
export function getMnemonicUtil(config) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var words, networkName;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!!mnemonicUtility) return [3 /*break*/, 2];
                    words = config.mnemonicPhrase;
                    networkName = config.blockchainNetwork;
                    if (allowedNetworkNames.indexOf(networkName) < 0) {
                        throw new Error(networkName + " network not supported.");
                    }
                    mnemonicUtility = new MnemonicUtility(words, bitcoin.networks[networkName]);
                    return [4 /*yield*/, mnemonicUtility.init()];
                case 1:
                    _a.sent();
                    _a.label = 2;
                case 2: return [2 /*return*/, mnemonicUtility];
            }
        });
    });
}
/**
 *
 * @param {?} coinId
 * @return {?}
 */
function getBasePath(coinId) {
    switch (coinId) {
        case CoinId.BTC:
            return 'm/49\'/0\'/0\'/0/';
        case CoinId.LTC:
            return 'm/49\'/2\'/0\'/0/';
        case CoinId.BCH:
            return 'm/44\'/145\'/0\'/0/';
        case CoinId.ETH:
            return 'm/44\'/60\'/0\'/0/';
        case CoinId.BNB:
        case CoinId.BET_BINANCE:
            // https://docs.binance.org/blockchain.html
            // derive the private key using BIP32 / BIP44 with HD prefix as "44'/714'/", which is reserved at SLIP 44.
            // 714 comes from Binance's birthday, July 14th
            return "m/44'/714'/0'/0/";
        case CoinId.XRP:
            return "m/44'/144'/0'/0/";
        case CoinId.TRX:
            return "m/44'/195'/0'/0/";
        case CoinId.EOS:
            return "m/44'/194'/0'/0/";
        default:
            throw new Error('CoinId NOT SUPPORTED: ' + coinId);
    }
}
/**
 * @return {?}
 */
export function generateMnemonicPhrase() {
    return bip39.generateMnemonic(256);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW5lbW9uaWMtdXRpbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2N1cnJlbmN5L21uZW1vbmljL21uZW1vbmljLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxLQUFLLElBQUksTUFBTSxNQUFNLENBQUM7QUFDN0IsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE9BQU8sTUFBTSxlQUFlLENBQUM7QUFJekMsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRzNDO0lBSUMseUJBQ1MsTUFBYyxFQUNkLE9BQXdCO1FBRHhCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxZQUFPLEdBQVAsT0FBTyxDQUFpQjtRQUx6QixXQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2YsU0FBSSxHQUF5QixTQUFTLENBQUM7SUFNL0MsQ0FBQztJQUVELHNCQUFJLGtDQUFLOzs7O1FBQVQ7WUFDQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDcEIsQ0FBQzs7O09BQUE7Ozs7SUFFSyw4QkFBSTs7O0lBQVY7Ozs7Ozt3QkFDQyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7NEJBQ2hCLHNCQUFPO3lCQUNQO3dCQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO3dCQUVOLHFCQUFNLEtBQUssQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFBOzt3QkFBN0MsSUFBSSxHQUFHLFNBQXNDO3dCQUVuRCxJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzs7Ozs7S0FDL0M7Ozs7O0lBR0QsaURBQXVCOzs7O0lBQXZCLFVBQXdCLE1BQWM7O1lBQ2pDLFFBQVEsR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDO1FBQ2xDLHlCQUF5QjtRQUV6QixRQUFRLE1BQU0sRUFBRTtZQUNmLEtBQUssTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUNoQixLQUFLLE1BQU0sQ0FBQyxHQUFHLENBQUM7WUFDaEIsS0FBSyxNQUFNLENBQUMsR0FBRztnQkFDZCxRQUFRLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUM7Z0JBQzFELE1BQU07U0FDUDs7WUFFSyxJQUFJLEdBQUcsUUFBUSxHQUFHLEdBQUc7OztZQUlyQixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1FBRXZDLE9BQU8sSUFBSSxDQUFDO0lBQ2IsQ0FBQzs7Ozs7O0lBRUQsa0RBQXdCOzs7OztJQUF4QixVQUF5QixNQUFjLEVBQUUsTUFBYzs7WUFDaEQsSUFBSSxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUMsR0FBRyxNQUFNO1FBRXpDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7Ozs7OztJQUVELHdDQUFjOzs7Ozs7SUFBZCxVQUFlLE1BQWMsRUFBRSxNQUFjLEVBQUUsTUFBa0I7O1lBQzFELElBQUksR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQzs7WUFFcEQsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFO1FBRW5FLE9BQU8sS0FBRyxNQUFNLEdBQUcsSUFBTSxDQUFDO0lBQzNCLENBQUM7SUFDRixzQkFBQztBQUFELENBQUMsQUE3REQsSUE2REM7Ozs7OztJQTVEQSxpQ0FBdUI7Ozs7O0lBQ3ZCLCtCQUErQzs7Ozs7SUFHOUMsaUNBQXNCOzs7OztJQUN0QixrQ0FBZ0M7Ozs7OztJQTJEOUIsZUFBZ0M7O0lBRTlCLG1CQUFtQixHQUFHLENBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLENBQUM7Ozs7OztBQUU3RCxNQUFNLFVBQWdCLGVBQWUsQ0FBQyxNQUE4Qjs7Ozs7O3lCQUMvRCxDQUFDLGVBQWUsRUFBaEIsd0JBQWdCO29CQUNiLEtBQUssR0FBRyxNQUFNLENBQUMsY0FBYztvQkFDN0IsV0FBVyxHQUFHLE1BQU0sQ0FBQyxpQkFBaUI7b0JBRTVDLElBQUksbUJBQW1CLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDakQsTUFBTSxJQUFJLEtBQUssQ0FBSSxXQUFXLDRCQUF5QixDQUFDLENBQUM7cUJBQ3pEO29CQUVELGVBQWUsR0FBRyxJQUFJLGVBQWUsQ0FDcEMsS0FBSyxFQUNMLE9BQU8sQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQzdCLENBQUM7b0JBRUYscUJBQU0sZUFBZSxDQUFDLElBQUksRUFBRSxFQUFBOztvQkFBNUIsU0FBNEIsQ0FBQzs7d0JBRzlCLHNCQUFPLGVBQWUsRUFBQzs7OztDQUN2Qjs7Ozs7O0FBS0QsU0FBUyxXQUFXLENBQUMsTUFBYztJQUNsQyxRQUFRLE1BQU0sRUFBRTtRQUNmLEtBQUssTUFBTSxDQUFDLEdBQUc7WUFDZCxPQUFPLG1CQUFtQixDQUFDO1FBRTVCLEtBQUssTUFBTSxDQUFDLEdBQUc7WUFDZCxPQUFPLG1CQUFtQixDQUFDO1FBRTVCLEtBQUssTUFBTSxDQUFDLEdBQUc7WUFDZCxPQUFPLHFCQUFxQixDQUFDO1FBRTlCLEtBQUssTUFBTSxDQUFDLEdBQUc7WUFDZCxPQUFPLG9CQUFvQixDQUFDO1FBRTdCLEtBQUssTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNoQixLQUFLLE1BQU0sQ0FBQyxXQUFXO1lBQ3RCLDJDQUEyQztZQUMzQywwR0FBMEc7WUFDMUcsK0NBQStDO1lBQy9DLE9BQU8sa0JBQWtCLENBQUM7UUFFM0IsS0FBSyxNQUFNLENBQUMsR0FBRztZQUNkLE9BQU8sa0JBQWtCLENBQUM7UUFFM0IsS0FBSyxNQUFNLENBQUMsR0FBRztZQUNkLE9BQU8sa0JBQWtCLENBQUM7UUFFM0IsS0FBSyxNQUFNLENBQUMsR0FBRztZQUNkLE9BQU8sa0JBQWtCLENBQUM7UUFFM0I7WUFDQyxNQUFNLElBQUksS0FBSyxDQUFDLHdCQUF3QixHQUFHLE1BQU0sQ0FBQyxDQUFDO0tBQ3BEO0FBQ0YsQ0FBQzs7OztBQUdELE1BQU0sVUFBVSxzQkFBc0I7SUFDckMsT0FBTyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDcEMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIGJzNTggZnJvbSAnYnM1OCc7XG5pbXBvcnQgKiBhcyBiaXAzOSBmcm9tICdiaXAzOSc7XG5pbXBvcnQgKiBhcyBiaXAzMiBmcm9tICdiaXAzMic7XG5pbXBvcnQgKiBhcyBiaXRjb2luIGZyb20gJ2JpdGNvaW5qcy1saWInO1xuXG5cbmltcG9ydCB7IElNbmVtb25pY1V0aWxpdHksIE1lbW9QcmVmaXgsIElNbmVtb25pY1V0aWxpdHlDb25maWcgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgQ29pbklkIH0gZnJvbSAnLi4vZGF0YWJhc2UvY29pbnMnO1xuXG5cbmNsYXNzIE1uZW1vbmljVXRpbGl0eSBpbXBsZW1lbnRzIElNbmVtb25pY1V0aWxpdHkge1xuXHRwcml2YXRlIGlzSW5pdCA9IGZhbHNlO1xuXHRwcml2YXRlIHJvb3Q6IGJpcDMyLkJJUDMySW50ZXJmYWNlID0gdW5kZWZpbmVkO1xuXG5cdGNvbnN0cnVjdG9yKFxuXHRcdHByaXZhdGUgX3dvcmRzOiBzdHJpbmcsXG5cdFx0cHJpdmF0ZSBuZXR3b3JrOiBiaXRjb2luLk5ldHdvcmtcblx0KSB7XG5cdH1cblxuXHRnZXQgd29yZHMoKSB7XG5cdFx0cmV0dXJuIHRoaXMuX3dvcmRzO1xuXHR9XG5cblx0YXN5bmMgaW5pdCgpIHtcblx0XHRpZiAodGhpcy5pc0luaXQpIHtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cblx0XHR0aGlzLmlzSW5pdCA9IHRydWU7XG5cblx0XHRjb25zdCBzZWVkID0gYXdhaXQgYmlwMzkubW5lbW9uaWNUb1NlZWQodGhpcy53b3Jkcyk7XG5cblx0XHR0aGlzLnJvb3QgPSBiaXAzMi5mcm9tU2VlZChzZWVkLCB0aGlzLm5ldHdvcmspO1xuXHR9XG5cblxuXHRnZXROb2RlRm9yQ2hhbmdlQWRkcmVzcyhjb2luSWQ6IENvaW5JZCk6IGJpcDMyLkJJUDMySW50ZXJmYWNlIHtcblx0XHRsZXQgYmFzZVBhdGggPSBnZXRCYXNlUGF0aChjb2luSWQpO1xuXHRcdC8vIGNvbnNvbGUubG9nKGJhc2VQYXRoKTtcblxuXHRcdHN3aXRjaCAoY29pbklkKSB7XG5cdFx0XHRjYXNlIENvaW5JZC5CVEM6XG5cdFx0XHRjYXNlIENvaW5JZC5MVEM6XG5cdFx0XHRjYXNlIENvaW5JZC5CQ0g6XG5cdFx0XHRcdGJhc2VQYXRoID0gYmFzZVBhdGguc3Vic3RyKDAsIGJhc2VQYXRoLmxlbmd0aCAtIDIpICsgJzEvJztcblx0XHRcdFx0YnJlYWs7XG5cdFx0fVxuXG5cdFx0Y29uc3QgcGF0aCA9IGJhc2VQYXRoICsgJzAnO1xuXHRcdC8vIGNvbnNvbGUubG9nKHBhdGgpO1xuXG5cblx0XHRjb25zdCBub2RlID0gdGhpcy5yb290LmRlcml2ZVBhdGgocGF0aCk7XG5cblx0XHRyZXR1cm4gbm9kZTtcblx0fVxuXG5cdGdldE5vZGVGb3JEZXBvc2l0QWRkcmVzcyhjb2luSWQ6IENvaW5JZCwgdXNlcklkOiBudW1iZXIpOiBiaXAzMi5CSVAzMkludGVyZmFjZSB7XG5cdFx0Y29uc3QgcGF0aCA9IGdldEJhc2VQYXRoKGNvaW5JZCkgKyB1c2VySWQ7XG5cblx0XHRyZXR1cm4gdGhpcy5yb290LmRlcml2ZVBhdGgocGF0aCk7XG5cdH1cblxuXHRnZXREZXBvc2l0TWVtbyh1c2VySWQ6IG51bWJlciwgY29pbklkOiBDb2luSWQsIHByZWZpeDogTWVtb1ByZWZpeCk6IHN0cmluZyB7XG5cdFx0Y29uc3Qgbm9kZSA9IHRoaXMuZ2V0Tm9kZUZvckRlcG9zaXRBZGRyZXNzKGNvaW5JZCwgdXNlcklkKTtcblxuXHRcdGNvbnN0IG1lbW8gPSBiczU4LmVuY29kZShub2RlLnB1YmxpY0tleS5zbGljZSgwLCAxMikpLnRvTG93ZXJDYXNlKCk7XG5cblx0XHRyZXR1cm4gYCR7cHJlZml4fSR7bWVtb31gO1xuXHR9XG59XG5cblxuLyoqKiB0aGUgTW5lbW9uaWNVdGlsaXR5IGNsYXNzIHNob3VsZCBiZSBhIHNpbmdsZXRvbjogKioqL1xubGV0IG1uZW1vbmljVXRpbGl0eTogTW5lbW9uaWNVdGlsaXR5O1xuXG5jb25zdCBhbGxvd2VkTmV0d29ya05hbWVzID0gWydtYWlubmV0JywgJ3Rlc3RuZXQnLCAncmVndGVzdCddO1xuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0TW5lbW9uaWNVdGlsKGNvbmZpZzogSU1uZW1vbmljVXRpbGl0eUNvbmZpZyk6IFByb21pc2U8SU1uZW1vbmljVXRpbGl0eT4ge1xuXHRpZiAoIW1uZW1vbmljVXRpbGl0eSkge1xuXHRcdGNvbnN0IHdvcmRzID0gY29uZmlnLm1uZW1vbmljUGhyYXNlO1xuXHRcdGNvbnN0IG5ldHdvcmtOYW1lID0gY29uZmlnLmJsb2NrY2hhaW5OZXR3b3JrO1xuXG5cdFx0aWYgKGFsbG93ZWROZXR3b3JrTmFtZXMuaW5kZXhPZihuZXR3b3JrTmFtZSkgPCAwKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoYCR7bmV0d29ya05hbWV9IG5ldHdvcmsgbm90IHN1cHBvcnRlZC5gKTtcblx0XHR9XG5cblx0XHRtbmVtb25pY1V0aWxpdHkgPSBuZXcgTW5lbW9uaWNVdGlsaXR5KFxuXHRcdFx0d29yZHMsXG5cdFx0XHRiaXRjb2luLm5ldHdvcmtzW25ldHdvcmtOYW1lXVxuXHRcdCk7XG5cblx0XHRhd2FpdCBtbmVtb25pY1V0aWxpdHkuaW5pdCgpO1xuXHR9XG5cblx0cmV0dXJuIG1uZW1vbmljVXRpbGl0eTtcbn1cbi8qKiovXG5cblxuXG5mdW5jdGlvbiBnZXRCYXNlUGF0aChjb2luSWQ6IENvaW5JZCk6IHN0cmluZyB7XG5cdHN3aXRjaCAoY29pbklkKSB7XG5cdFx0Y2FzZSBDb2luSWQuQlRDOlxuXHRcdFx0cmV0dXJuICdtLzQ5XFwnLzBcXCcvMFxcJy8wLyc7XG5cblx0XHRjYXNlIENvaW5JZC5MVEM6XG5cdFx0XHRyZXR1cm4gJ20vNDlcXCcvMlxcJy8wXFwnLzAvJztcblxuXHRcdGNhc2UgQ29pbklkLkJDSDpcblx0XHRcdHJldHVybiAnbS80NFxcJy8xNDVcXCcvMFxcJy8wLyc7XG5cblx0XHRjYXNlIENvaW5JZC5FVEg6XG5cdFx0XHRyZXR1cm4gJ20vNDRcXCcvNjBcXCcvMFxcJy8wLyc7XG5cblx0XHRjYXNlIENvaW5JZC5CTkI6XG5cdFx0Y2FzZSBDb2luSWQuQkVUX0JJTkFOQ0U6XG5cdFx0XHQvLyBodHRwczovL2RvY3MuYmluYW5jZS5vcmcvYmxvY2tjaGFpbi5odG1sXG5cdFx0XHQvLyBkZXJpdmUgdGhlIHByaXZhdGUga2V5IHVzaW5nIEJJUDMyIC8gQklQNDQgd2l0aCBIRCBwcmVmaXggYXMgXCI0NCcvNzE0Jy9cIiwgd2hpY2ggaXMgcmVzZXJ2ZWQgYXQgU0xJUCA0NC5cblx0XHRcdC8vIDcxNCBjb21lcyBmcm9tIEJpbmFuY2UncyBiaXJ0aGRheSwgSnVseSAxNHRoXG5cdFx0XHRyZXR1cm4gYG0vNDQnLzcxNCcvMCcvMC9gO1xuXG5cdFx0Y2FzZSBDb2luSWQuWFJQOlxuXHRcdFx0cmV0dXJuIGBtLzQ0Jy8xNDQnLzAnLzAvYDtcblxuXHRcdGNhc2UgQ29pbklkLlRSWDpcblx0XHRcdHJldHVybiBgbS80NCcvMTk1Jy8wJy8wL2A7XG5cblx0XHRjYXNlIENvaW5JZC5FT1M6XG5cdFx0XHRyZXR1cm4gYG0vNDQnLzE5NCcvMCcvMC9gO1xuXG5cdFx0ZGVmYXVsdDpcblx0XHRcdHRocm93IG5ldyBFcnJvcignQ29pbklkIE5PVCBTVVBQT1JURUQ6ICcgKyBjb2luSWQpO1xuXHR9XG59XG5cblxuZXhwb3J0IGZ1bmN0aW9uIGdlbmVyYXRlTW5lbW9uaWNQaHJhc2UoKSB7XG5cdHJldHVybiBiaXAzOS5nZW5lcmF0ZU1uZW1vbmljKDI1Nik7XG59XG5cblxuXG4vKlxuXG5cblxuUEFUSFM6XG5cbkJUQ1xuXG5MVENcblwibS80NCcvMicvMCcvMC9cIlxuXG5cblxuQkNIXG5gbS80NCcvMTQ1Jy8wJy8wL2BcblxuXG5FVEhcbmBtLzQ0Jy82MCcvMCcvMC9gXG5cblxuXG5cblhSUFxuYG0vNDQnLzE0NCcvMCcvMC9gXG5cbkJFVCAoQmluYW5jZSlcbmBtLzQ0Jy83MTQnLzAnLzAvYFxuXG5cbiovXG4iXX0=