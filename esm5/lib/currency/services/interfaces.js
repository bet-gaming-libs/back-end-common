/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/services/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IWithdrawalRequestData() { }
if (false) {
    /** @type {?} */
    IWithdrawalRequestData.prototype.user;
    /** @type {?} */
    IWithdrawalRequestData.prototype.tx_hash;
    /** @type {?} */
    IWithdrawalRequestData.prototype.extra_data;
    /** @type {?} */
    IWithdrawalRequestData.prototype.withdraw_id;
    /** @type {?} */
    IWithdrawalRequestData.prototype.withdraw_memo;
    /** @type {?} */
    IWithdrawalRequestData.prototype.withdraw_address;
    /** @type {?} */
    IWithdrawalRequestData.prototype.withdraw_quantity;
}
/**
 * @record
 */
export function IRefundWithdrawalRequestParams() { }
if (false) {
    /** @type {?} */
    IRefundWithdrawalRequestParams.prototype.requestId;
    /** @type {?} */
    IRefundWithdrawalRequestParams.prototype.easyAccountPublicKey;
    /** @type {?} */
    IRefundWithdrawalRequestParams.prototype.reason;
}
/**
 * @record
 */
export function IWithdrawalContractService() { }
if (false) {
    /**
     * @return {?}
     */
    IWithdrawalContractService.prototype.getPendingRequests = function () { };
    /**
     * @param {?} requestId
     * @param {?} txnId
     * @return {?}
     */
    IWithdrawalContractService.prototype.processRequest = function (requestId, txnId) { };
    /**
     * @param {?} request
     * @return {?}
     */
    IWithdrawalContractService.prototype.refundRequest = function (request) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2N1cnJlbmN5L3NlcnZpY2VzL2ludGVyZmFjZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFHQSw0Q0FRQzs7O0lBUEEsc0NBQWE7O0lBQ2IseUNBQWdCOztJQUNoQiw0Q0FBbUI7O0lBQ25CLDZDQUFvQjs7SUFDcEIsK0NBQXNCOztJQUN0QixrREFBeUI7O0lBQ3pCLG1EQUEwQjs7Ozs7QUFHM0Isb0RBSUM7OztJQUhBLG1EQUFrQjs7SUFDbEIsOERBQTZCOztJQUM3QixnREFBZTs7Ozs7QUFHaEIsZ0RBSUM7Ozs7O0lBSEEsMEVBQXdEOzs7Ozs7SUFDeEQsc0ZBQWtFOzs7OztJQUNsRSw0RUFBaUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJU2F2ZWRXaXRoZHJhd2FsUmVxdWVzdCB9IGZyb20gJy4uL2RhdGFiYXNlL21vZGVscy93aXRoZHJhd2FsLXJlcXVlc3QnO1xuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSVdpdGhkcmF3YWxSZXF1ZXN0RGF0YSB7XG5cdHVzZXI6IHN0cmluZztcblx0dHhfaGFzaDogc3RyaW5nO1xuXHRleHRyYV9kYXRhOiBzdHJpbmc7XG5cdHdpdGhkcmF3X2lkOiBzdHJpbmc7XG5cdHdpdGhkcmF3X21lbW86IHN0cmluZztcblx0d2l0aGRyYXdfYWRkcmVzczogc3RyaW5nO1xuXHR3aXRoZHJhd19xdWFudGl0eTogc3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElSZWZ1bmRXaXRoZHJhd2FsUmVxdWVzdFBhcmFtcyB7XG5cdHJlcXVlc3RJZDogc3RyaW5nO1xuXHRlYXN5QWNjb3VudFB1YmxpY0tleTogc3RyaW5nO1xuXHRyZWFzb246IHN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJV2l0aGRyYXdhbENvbnRyYWN0U2VydmljZSB7XG5cdGdldFBlbmRpbmdSZXF1ZXN0cygpOiBQcm9taXNlPElXaXRoZHJhd2FsUmVxdWVzdERhdGFbXT47XG5cdHByb2Nlc3NSZXF1ZXN0KHJlcXVlc3RJZDogc3RyaW5nLCB0eG5JZDogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+O1xuXHRyZWZ1bmRSZXF1ZXN0KHJlcXVlc3Q6IElTYXZlZFdpdGhkcmF3YWxSZXF1ZXN0KTogUHJvbWlzZTxzdHJpbmc+O1xufVxuIl19