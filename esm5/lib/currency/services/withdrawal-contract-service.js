/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/services/withdrawal-contract-service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { EosTransactionExecutor } from '../../eos/eos-txn-executor';
import { isWaxTxnIrreversible } from '../../eos/irreversible';
/**
 * @template T
 */
var /**
 * @template T
 */
WithdrawalContractService = /** @class */ (function () {
    function WithdrawalContractService(eosUtility) {
        this.eosUtility = eosUtility;
        this.contractName = this.contracts.withdrawal;
    }
    /**
     * @return {?}
     */
    WithdrawalContractService.prototype.getPendingRequests = /**
     * @return {?}
     */
    function () {
        return this.eosUtility.getTableRows({
            code: this.contractName,
            scope: this.contractName,
            table: 'withdraws',
            limit: 1000
        });
    };
    /**
     * @param {?} requestId
     * @param {?} txnId
     * @return {?}
     */
    WithdrawalContractService.prototype.processRequest = /**
     * @param {?} requestId
     * @param {?} txnId
     * @return {?}
     */
    function (requestId, txnId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var executor, broadcastedTxnId, confirmedTxnId;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        executor = new EosTransactionExecutor([{
                                account: this.contractName,
                                name: 'process',
                                data: {
                                    withdraw_id: requestId,
                                    memo: txnId
                                }
                            }], this.eosUtility, isWaxTxnIrreversible);
                        return [4 /*yield*/, executor.execute()];
                    case 1:
                        broadcastedTxnId = _a.sent();
                        return [4 /*yield*/, executor.confirm()];
                    case 2:
                        confirmedTxnId = _a.sent();
                        /*** save this in DB? ***/
                        return [2 /*return*/, confirmedTxnId];
                }
            });
        });
    };
    /**
     * @param {?} request
     * @return {?}
     */
    WithdrawalContractService.prototype.refundRequest = /**
     * @param {?} request
     * @return {?}
     */
    function (request) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var params, requestId, easyAccountPublicKey, reason, data, executor, broadcastedTxnId, confirmedTxnId;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, request.getRefundParams()];
                    case 1:
                        params = _a.sent();
                        requestId = params.requestId, easyAccountPublicKey = params.easyAccountPublicKey, reason = params.reason;
                        data = {
                            withdraw_id: requestId,
                            ezacct_deposit_memo: easyAccountPublicKey,
                            memo: reason
                        };
                        executor = new EosTransactionExecutor([{
                                account: this.contractName,
                                name: 'refund',
                                data: data
                            }], this.eosUtility, isWaxTxnIrreversible);
                        return [4 /*yield*/, executor.execute()];
                    case 2:
                        broadcastedTxnId = _a.sent();
                        return [4 /*yield*/, executor.confirm()];
                    case 3:
                        confirmedTxnId = _a.sent();
                        /*** save this in DB? ***/
                        return [2 /*return*/, confirmedTxnId];
                }
            });
        });
    };
    Object.defineProperty(WithdrawalContractService.prototype, "contracts", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.eosUtility.contracts;
        },
        enumerable: true,
        configurable: true
    });
    return WithdrawalContractService;
}());
/**
 * @template T
 */
export { WithdrawalContractService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    WithdrawalContractService.prototype.contractName;
    /**
     * @type {?}
     * @private
     */
    WithdrawalContractService.prototype.eosUtility;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2l0aGRyYXdhbC1jb250cmFjdC1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvY3VycmVuY3kvc2VydmljZXMvd2l0aGRyYXdhbC1jb250cmFjdC1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUVBLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDOzs7O0FBSzlEOzs7O0lBR0MsbUNBQTZCLFVBQTBCO1FBQTFCLGVBQVUsR0FBVixVQUFVLENBQWdCO1FBQ3RELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUM7SUFDL0MsQ0FBQzs7OztJQUVELHNEQUFrQjs7O0lBQWxCO1FBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBeUI7WUFDM0QsSUFBSSxFQUFFLElBQUksQ0FBQyxZQUFZO1lBQ3ZCLEtBQUssRUFBRSxJQUFJLENBQUMsWUFBWTtZQUN4QixLQUFLLEVBQUUsV0FBVztZQUNsQixLQUFLLEVBQUUsSUFBSTtTQUNYLENBQUMsQ0FBQztJQUNKLENBQUM7Ozs7OztJQUVLLGtEQUFjOzs7OztJQUFwQixVQUFxQixTQUFpQixFQUFFLEtBQWE7Ozs7Ozt3QkFDOUMsUUFBUSxHQUFHLElBQUksc0JBQXNCLENBQzFDLENBQUM7Z0NBQ0EsT0FBTyxFQUFFLElBQUksQ0FBQyxZQUFZO2dDQUMxQixJQUFJLEVBQUUsU0FBUztnQ0FDZixJQUFJLEVBQUU7b0NBQ0wsV0FBVyxFQUFFLFNBQVM7b0NBQ3RCLElBQUksRUFBRSxLQUFLO2lDQUNYOzZCQUNELENBQUMsRUFDRixJQUFJLENBQUMsVUFBVSxFQUNmLG9CQUFvQixDQUNwQjt3QkFHd0IscUJBQU0sUUFBUSxDQUFDLE9BQU8sRUFBRSxFQUFBOzt3QkFBM0MsZ0JBQWdCLEdBQUcsU0FBd0I7d0JBRTFCLHFCQUFNLFFBQVEsQ0FBQyxPQUFPLEVBQUUsRUFBQTs7d0JBQXpDLGNBQWMsR0FBRyxTQUF3Qjt3QkFFL0MsMEJBQTBCO3dCQUMxQixzQkFBTyxjQUFjLEVBQUM7Ozs7S0FDdEI7Ozs7O0lBRUssaURBQWE7Ozs7SUFBbkIsVUFBb0IsT0FBZ0M7Ozs7OzRCQUNwQyxxQkFBTSxPQUFPLENBQUMsZUFBZSxFQUFFLEVBQUE7O3dCQUF4QyxNQUFNLEdBQUcsU0FBK0I7d0JBRXZDLFNBQVMsR0FBa0MsTUFBTSxVQUF4QyxFQUFFLG9CQUFvQixHQUFZLE1BQU0scUJBQWxCLEVBQUUsTUFBTSxHQUFJLE1BQU0sT0FBVjt3QkFFeEMsSUFBSSxHQUFHOzRCQUNaLFdBQVcsRUFBRSxTQUFTOzRCQUN0QixtQkFBbUIsRUFBRSxvQkFBb0I7NEJBQ3pDLElBQUksRUFBRSxNQUFNO3lCQUNaO3dCQUVLLFFBQVEsR0FBRyxJQUFJLHNCQUFzQixDQUMxQyxDQUFDO2dDQUNBLE9BQU8sRUFBRSxJQUFJLENBQUMsWUFBWTtnQ0FDMUIsSUFBSSxFQUFFLFFBQVE7Z0NBQ2QsSUFBSSxNQUFBOzZCQUNKLENBQUMsRUFDRixJQUFJLENBQUMsVUFBVSxFQUNmLG9CQUFvQixDQUNwQjt3QkFHd0IscUJBQU0sUUFBUSxDQUFDLE9BQU8sRUFBRSxFQUFBOzt3QkFBM0MsZ0JBQWdCLEdBQUcsU0FBd0I7d0JBRTFCLHFCQUFNLFFBQVEsQ0FBQyxPQUFPLEVBQUUsRUFBQTs7d0JBQXpDLGNBQWMsR0FBRyxTQUF3Qjt3QkFFL0MsMEJBQTBCO3dCQUMxQixzQkFBTyxjQUFjLEVBQUM7Ozs7S0FDdEI7SUFHRCxzQkFBWSxnREFBUzs7Ozs7UUFBckI7WUFDQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDO1FBQ2xDLENBQUM7OztPQUFBO0lBQ0YsZ0NBQUM7QUFBRCxDQUFDLEFBekVELElBeUVDOzs7Ozs7Ozs7O0lBeEVBLGlEQUFzQzs7Ozs7SUFFMUIsK0NBQTJDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSVdpdGhkcmF3YWxDb250cmFjdFNlcnZpY2UsIElXaXRoZHJhd2FsUmVxdWVzdERhdGEgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuaW1wb3J0IHtJRW9zVXRpbGl0eX0gZnJvbSAnLi4vLi4vZW9zL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgRW9zVHJhbnNhY3Rpb25FeGVjdXRvciB9IGZyb20gJy4uLy4uL2Vvcy9lb3MtdHhuLWV4ZWN1dG9yJztcbmltcG9ydCB7IGlzV2F4VHhuSXJyZXZlcnNpYmxlIH0gZnJvbSAnLi4vLi4vZW9zL2lycmV2ZXJzaWJsZSc7XG5pbXBvcnQgeyBJRW9zQWNjb3VudE5hbWVzQmFzZSB9IGZyb20gJy4uLy4uL2Vvcy9lb3Mtc2V0dGluZ3MnO1xuaW1wb3J0IHsgSVNhdmVkV2l0aGRyYXdhbFJlcXVlc3QgfSBmcm9tICcuLi9kYXRhYmFzZS9tb2RlbHMvd2l0aGRyYXdhbC1yZXF1ZXN0JztcblxuXG5leHBvcnQgY2xhc3MgV2l0aGRyYXdhbENvbnRyYWN0U2VydmljZTxUIGV4dGVuZHMgSUVvc0FjY291bnROYW1lc0Jhc2U+IGltcGxlbWVudHMgSVdpdGhkcmF3YWxDb250cmFjdFNlcnZpY2Uge1xuXHRwcml2YXRlIHJlYWRvbmx5IGNvbnRyYWN0TmFtZTogc3RyaW5nO1xuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcmVhZG9ubHkgZW9zVXRpbGl0eTogSUVvc1V0aWxpdHk8VD4pIHtcblx0XHR0aGlzLmNvbnRyYWN0TmFtZSA9IHRoaXMuY29udHJhY3RzLndpdGhkcmF3YWw7XG5cdH1cblxuXHRnZXRQZW5kaW5nUmVxdWVzdHMoKTogUHJvbWlzZTxJV2l0aGRyYXdhbFJlcXVlc3REYXRhW10+IHtcblx0XHRyZXR1cm4gdGhpcy5lb3NVdGlsaXR5LmdldFRhYmxlUm93czxJV2l0aGRyYXdhbFJlcXVlc3REYXRhPih7XG5cdFx0XHRjb2RlOiB0aGlzLmNvbnRyYWN0TmFtZSxcblx0XHRcdHNjb3BlOiB0aGlzLmNvbnRyYWN0TmFtZSxcblx0XHRcdHRhYmxlOiAnd2l0aGRyYXdzJyxcblx0XHRcdGxpbWl0OiAxMDAwXG5cdFx0fSk7XG5cdH1cblxuXHRhc3luYyBwcm9jZXNzUmVxdWVzdChyZXF1ZXN0SWQ6IHN0cmluZywgdHhuSWQ6IHN0cmluZykge1xuXHRcdGNvbnN0IGV4ZWN1dG9yID0gbmV3IEVvc1RyYW5zYWN0aW9uRXhlY3V0b3IoXG5cdFx0XHRbe1xuXHRcdFx0XHRhY2NvdW50OiB0aGlzLmNvbnRyYWN0TmFtZSxcblx0XHRcdFx0bmFtZTogJ3Byb2Nlc3MnLFxuXHRcdFx0XHRkYXRhOiB7XG5cdFx0XHRcdFx0d2l0aGRyYXdfaWQ6IHJlcXVlc3RJZCxcblx0XHRcdFx0XHRtZW1vOiB0eG5JZFxuXHRcdFx0XHR9XG5cdFx0XHR9XSxcblx0XHRcdHRoaXMuZW9zVXRpbGl0eSxcblx0XHRcdGlzV2F4VHhuSXJyZXZlcnNpYmxlXG5cdFx0KTtcblxuXHRcdC8qKiogc2F2ZSB0aGlzIGluIERCPyAqKiovXG5cdFx0Y29uc3QgYnJvYWRjYXN0ZWRUeG5JZCA9IGF3YWl0IGV4ZWN1dG9yLmV4ZWN1dGUoKTtcblxuXHRcdGNvbnN0IGNvbmZpcm1lZFR4bklkID0gYXdhaXQgZXhlY3V0b3IuY29uZmlybSgpO1xuXG5cdFx0LyoqKiBzYXZlIHRoaXMgaW4gREI/ICoqKi9cblx0XHRyZXR1cm4gY29uZmlybWVkVHhuSWQ7XG5cdH1cblxuXHRhc3luYyByZWZ1bmRSZXF1ZXN0KHJlcXVlc3Q6IElTYXZlZFdpdGhkcmF3YWxSZXF1ZXN0KTogUHJvbWlzZTxzdHJpbmc+IHtcblx0XHRjb25zdCBwYXJhbXMgPSBhd2FpdCByZXF1ZXN0LmdldFJlZnVuZFBhcmFtcygpO1xuXG5cdFx0Y29uc3Qge3JlcXVlc3RJZCwgZWFzeUFjY291bnRQdWJsaWNLZXksIHJlYXNvbn0gPSBwYXJhbXM7XG5cblx0XHRjb25zdCBkYXRhID0ge1xuXHRcdFx0d2l0aGRyYXdfaWQ6IHJlcXVlc3RJZCxcblx0XHRcdGV6YWNjdF9kZXBvc2l0X21lbW86IGVhc3lBY2NvdW50UHVibGljS2V5LFxuXHRcdFx0bWVtbzogcmVhc29uXG5cdFx0fTtcblxuXHRcdGNvbnN0IGV4ZWN1dG9yID0gbmV3IEVvc1RyYW5zYWN0aW9uRXhlY3V0b3IoXG5cdFx0XHRbe1xuXHRcdFx0XHRhY2NvdW50OiB0aGlzLmNvbnRyYWN0TmFtZSxcblx0XHRcdFx0bmFtZTogJ3JlZnVuZCcsXG5cdFx0XHRcdGRhdGFcblx0XHRcdH1dLFxuXHRcdFx0dGhpcy5lb3NVdGlsaXR5LFxuXHRcdFx0aXNXYXhUeG5JcnJldmVyc2libGVcblx0XHQpO1xuXG5cdFx0LyoqKiBzYXZlIHRoaXMgaW4gREI/ICoqKi9cblx0XHRjb25zdCBicm9hZGNhc3RlZFR4bklkID0gYXdhaXQgZXhlY3V0b3IuZXhlY3V0ZSgpO1xuXG5cdFx0Y29uc3QgY29uZmlybWVkVHhuSWQgPSBhd2FpdCBleGVjdXRvci5jb25maXJtKCk7XG5cblx0XHQvKioqIHNhdmUgdGhpcyBpbiBEQj8gKioqL1xuXHRcdHJldHVybiBjb25maXJtZWRUeG5JZDtcblx0fVxuXG5cblx0cHJpdmF0ZSBnZXQgY29udHJhY3RzKCkge1xuXHRcdHJldHVybiB0aGlzLmVvc1V0aWxpdHkuY29udHJhY3RzO1xuXHR9XG59XG4iXX0=