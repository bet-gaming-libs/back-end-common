/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-connection-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import * as mysql from 'mysql';
import { sleep, logWithTime } from 'earnbet-common';
/** @type {?} */
var logSqlQueries = process.env.LOG_SQL_QUERIES === 'true';
var MySQLConnectionManager = /** @class */ (function () {
    function MySQLConnectionManager(config) {
        this.config = config;
        this.isConnecting = false;
        this.connection = undefined;
        this.connect();
    }
    /**
     * @return {?}
     */
    MySQLConnectionManager.prototype.getConnection = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.reconnect()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, this.connection];
                }
            });
        });
    };
    /**
     * @private
     * @return {?}
     */
    MySQLConnectionManager.prototype.reconnect = /**
     * @private
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var isConnected;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.isConnected) return [3 /*break*/, 3];
                        logWithTime('NOT CONNECTED:', this.connection.threadId);
                        this.printConnectionState();
                        return [4 /*yield*/, this.connect()];
                    case 1:
                        isConnected = _a.sent();
                        if (isConnected) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, sleep(1000)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 0];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @private
     * @return {?}
     */
    MySQLConnectionManager.prototype.connect = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        function (resolve) {
            if (_this.isConnected) {
                resolve(true);
                return;
            }
            if (_this.isConnecting) {
                resolve(false);
                return;
            }
            _this.isConnecting = true;
            // end existing connection
            if (_this.connection) {
                _this.connection.end();
            }
            // connection parameters
            _this.connection = mysql.createConnection(_this.config);
            // logWithTime('MySQLConnection State:',this.connection.state);
            // establish connection
            _this.connection.connect((/**
             * @param {?} err
             * @return {?}
             */
            function (err) {
                logWithTime(_this.config);
                _this.isConnecting = false;
                if (err) {
                    console.error('error connecting: ' + err.stack);
                    resolve(false);
                }
                else {
                    logWithTime('connected as id ' + _this.connection.threadId);
                    logWithTime('MySQLConnection State:', _this.connection.state);
                    // set query format
                    _this.connection.config.queryFormat = (/**
                     * @param {?} query
                     * @param {?} values
                     * @return {?}
                     */
                    function (query, values) {
                        if (!values) {
                            return query;
                        }
                        /** @type {?} */
                        var escapedQuery = query.replace(/\:(\w+)/g, (/**
                         * @param {?} txt
                         * @param {?} key
                         * @return {?}
                         */
                        function (txt, key) {
                            if (values.hasOwnProperty(key)) {
                                return this.escape(values[key]);
                            }
                            return txt;
                        }).bind(this));
                        if (logSqlQueries) {
                            console.log({ escapedQuery: escapedQuery });
                        }
                        return escapedQuery;
                    });
                    resolve(true);
                }
            }));
            _this.connection.on('error', (/**
             * @param {?} err
             * @return {?}
             */
            function (err) {
                logWithTime('******************* MySQL Connection onError EVENT: *******************');
                console.error(err);
                _this.printConnectionState();
            }));
        }));
    };
    /**
     * @private
     * @return {?}
     */
    MySQLConnectionManager.prototype.printConnectionState = /**
     * @private
     * @return {?}
     */
    function () {
        logWithTime('MySQLConnection State:', this.connection.state);
    };
    Object.defineProperty(MySQLConnectionManager.prototype, "isConnected", {
        /*
        private get isDisconnected():boolean {
            return this.connection == undefined ||
                    this.connection.state === 'disconnected';
        }
        */
        get: /*
            private get isDisconnected():boolean {
                return this.connection == undefined ||
                        this.connection.state === 'disconnected';
            }
            */
        /**
         * @private
         * @return {?}
         */
        function () {
            return this.connection !== undefined && (this.connection.state === 'connected' ||
                this.connection.state === 'authenticated');
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    MySQLConnectionManager.prototype.endConnection = /**
     * @return {?}
     */
    function () {
        this.connection.end();
    };
    return MySQLConnectionManager;
}());
export { MySQLConnectionManager };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.isConnecting;
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.connection;
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.config;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvZGF0YWJhc2UvZW5naW5lL215c3FsL215c3FsLWNvbm5lY3Rpb24tbWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUUvQixPQUFPLEVBQUMsS0FBSyxFQUFFLFdBQVcsRUFBQyxNQUFNLGdCQUFnQixDQUFDOztJQUk1QyxhQUFhLEdBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxLQUFLLE1BQU07QUFHdkM7SUFLQyxnQ0FBb0IsTUFBb0I7UUFBcEIsV0FBTSxHQUFOLE1BQU0sQ0FBYztRQUpoQyxpQkFBWSxHQUFHLEtBQUssQ0FBQztRQUVyQixlQUFVLEdBQXFCLFNBQVMsQ0FBQztRQUdoRCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDaEIsQ0FBQzs7OztJQUVLLDhDQUFhOzs7SUFBbkI7Ozs7NEJBQ0MscUJBQU0sSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFBOzt3QkFBdEIsU0FBc0IsQ0FBQzt3QkFFdkIsc0JBQU8sSUFBSSxDQUFDLFVBQVUsRUFBQzs7OztLQUN2Qjs7Ozs7SUFFYSwwQ0FBUzs7OztJQUF2Qjs7Ozs7OzZCQUNRLENBQUMsSUFBSSxDQUFDLFdBQVc7d0JBQ3ZCLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUV4RCxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQzt3QkFHUixxQkFBTSxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUE7O3dCQUFsQyxXQUFXLEdBQUcsU0FBb0I7d0JBRXhDLElBQUksV0FBVyxFQUFFOzRCQUNoQixzQkFBTzt5QkFDUDt3QkFFRCxxQkFBTSxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUE7O3dCQUFqQixTQUFpQixDQUFDOzs7Ozs7S0FFbkI7Ozs7O0lBRU8sd0NBQU87Ozs7SUFBZjtRQUFBLGlCQTRFQztRQTNFQSxPQUFPLElBQUksT0FBTzs7OztRQUFXLFVBQUMsT0FBTztZQUNwQyxJQUFJLEtBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ3JCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDZCxPQUFPO2FBQ1A7WUFHRCxJQUFJLEtBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ3RCLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDZixPQUFPO2FBQ1A7WUFHRCxLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUd6QiwwQkFBMEI7WUFDMUIsSUFBSSxLQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNwQixLQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDO2FBQ3RCO1lBR0Qsd0JBQXdCO1lBQ3hCLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLGdCQUFnQixDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUV0RCwrREFBK0Q7WUFFL0QsdUJBQXVCO1lBQ3ZCLEtBQUksQ0FBQyxVQUFVLENBQUMsT0FBTzs7OztZQUFDLFVBQUMsR0FBRztnQkFDM0IsV0FBVyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFFekIsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7Z0JBRTFCLElBQUksR0FBRyxFQUFFO29CQUNSLE9BQU8sQ0FBQyxLQUFLLENBQUMsb0JBQW9CLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUVoRCxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ2Y7cUJBQU07b0JBQ04sV0FBVyxDQUFDLGtCQUFrQixHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQzNELFdBQVcsQ0FBQyx3QkFBd0IsRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUc3RCxtQkFBbUI7b0JBQ25CLEtBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLFdBQVc7Ozs7O29CQUFHLFVBQVUsS0FBSyxFQUFFLE1BQU07d0JBQzNELElBQUksQ0FBQyxNQUFNLEVBQUU7NEJBQUMsT0FBTyxLQUFLLENBQUM7eUJBQUU7OzRCQUV2QixZQUFZLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUU7Ozs7O3dCQUFBLFVBQVUsR0FBRyxFQUFFLEdBQUc7NEJBQ2hFLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQ0FDL0IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDOzZCQUNoQzs0QkFDRCxPQUFPLEdBQUcsQ0FBQzt3QkFDWCxDQUFDLEVBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUdkLElBQUksYUFBYSxFQUFFOzRCQUNsQixPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUMsWUFBWSxjQUFBLEVBQUMsQ0FBQyxDQUFDO3lCQUM1Qjt3QkFFRCxPQUFPLFlBQVksQ0FBQztvQkFDckIsQ0FBQyxDQUFBLENBQUM7b0JBR0YsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNkO1lBQ0YsQ0FBQyxFQUFDLENBQUM7WUFFSCxLQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxPQUFPOzs7O1lBQUUsVUFBQyxHQUFHO2dCQUMvQixXQUFXLENBQUMseUVBQXlFLENBQUMsQ0FBQztnQkFFdkYsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFHbkIsS0FBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7WUFDN0IsQ0FBQyxFQUFDLENBQUM7UUFDSixDQUFDLEVBQUUsQ0FBQztJQUNMLENBQUM7Ozs7O0lBR08scURBQW9COzs7O0lBQTVCO1FBQ0MsV0FBVyxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQVFELHNCQUFZLCtDQUFXO1FBTnZCOzs7OztVQUtLOzs7Ozs7Ozs7OztRQUNMO1lBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxLQUFLLFNBQVMsSUFBSSxDQUN2QyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssS0FBSyxXQUFXO2dCQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssS0FBSyxlQUFlLENBQ3pDLENBQUM7UUFDSCxDQUFDOzs7T0FBQTs7OztJQUdELDhDQUFhOzs7SUFBYjtRQUNDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUNGLDZCQUFDO0FBQUQsQ0FBQyxBQXBJRCxJQW9JQzs7Ozs7OztJQW5JQSw4Q0FBNkI7Ozs7O0lBRTdCLDRDQUFpRDs7Ozs7SUFFckMsd0NBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgbXlzcWwgZnJvbSAnbXlzcWwnO1xuXG5pbXBvcnQge3NsZWVwLCBsb2dXaXRoVGltZX0gZnJvbSAnZWFybmJldC1jb21tb24nO1xuaW1wb3J0IHsgSU15U1FMQ29uZmlnIH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcblxuXG5jb25zdCBsb2dTcWxRdWVyaWVzID1cblx0cHJvY2Vzcy5lbnYuTE9HX1NRTF9RVUVSSUVTID09PSAndHJ1ZSc7XG5cblxuZXhwb3J0IGNsYXNzIE15U1FMQ29ubmVjdGlvbk1hbmFnZXIge1xuXHRwcml2YXRlIGlzQ29ubmVjdGluZyA9IGZhbHNlO1xuXG5cdHByaXZhdGUgY29ubmVjdGlvbjogbXlzcWwuQ29ubmVjdGlvbiA9IHVuZGVmaW5lZDtcblxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIGNvbmZpZzogSU15U1FMQ29uZmlnKSB7XG5cdFx0dGhpcy5jb25uZWN0KCk7XG5cdH1cblxuXHRhc3luYyBnZXRDb25uZWN0aW9uKCk6IFByb21pc2U8bXlzcWwuQ29ubmVjdGlvbj4ge1xuXHRcdGF3YWl0IHRoaXMucmVjb25uZWN0KCk7XG5cblx0XHRyZXR1cm4gdGhpcy5jb25uZWN0aW9uO1xuXHR9XG5cblx0cHJpdmF0ZSBhc3luYyByZWNvbm5lY3QoKSB7XG5cdFx0d2hpbGUgKCF0aGlzLmlzQ29ubmVjdGVkKSB7XG5cdFx0XHRsb2dXaXRoVGltZSgnTk9UIENPTk5FQ1RFRDonLCB0aGlzLmNvbm5lY3Rpb24udGhyZWFkSWQpO1xuXG5cdFx0XHR0aGlzLnByaW50Q29ubmVjdGlvblN0YXRlKCk7XG5cblxuXHRcdFx0Y29uc3QgaXNDb25uZWN0ZWQgPSBhd2FpdCB0aGlzLmNvbm5lY3QoKTtcblxuXHRcdFx0aWYgKGlzQ29ubmVjdGVkKSB7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblxuXHRcdFx0YXdhaXQgc2xlZXAoMTAwMCk7XG5cdFx0fVxuXHR9XG5cblx0cHJpdmF0ZSBjb25uZWN0KCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuXHRcdHJldHVybiBuZXcgUHJvbWlzZTxib29sZWFuPiggKHJlc29sdmUpID0+IHtcblx0XHRcdGlmICh0aGlzLmlzQ29ubmVjdGVkKSB7XG5cdFx0XHRcdHJlc29sdmUodHJ1ZSk7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblxuXG5cdFx0XHRpZiAodGhpcy5pc0Nvbm5lY3RpbmcpIHtcblx0XHRcdFx0cmVzb2x2ZShmYWxzZSk7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblxuXG5cdFx0XHR0aGlzLmlzQ29ubmVjdGluZyA9IHRydWU7XG5cblxuXHRcdFx0Ly8gZW5kIGV4aXN0aW5nIGNvbm5lY3Rpb25cblx0XHRcdGlmICh0aGlzLmNvbm5lY3Rpb24pIHtcblx0XHRcdFx0dGhpcy5jb25uZWN0aW9uLmVuZCgpO1xuXHRcdFx0fVxuXG5cblx0XHRcdC8vIGNvbm5lY3Rpb24gcGFyYW1ldGVyc1xuXHRcdFx0dGhpcy5jb25uZWN0aW9uID0gbXlzcWwuY3JlYXRlQ29ubmVjdGlvbih0aGlzLmNvbmZpZyk7XG5cblx0XHRcdC8vIGxvZ1dpdGhUaW1lKCdNeVNRTENvbm5lY3Rpb24gU3RhdGU6Jyx0aGlzLmNvbm5lY3Rpb24uc3RhdGUpO1xuXG5cdFx0XHQvLyBlc3RhYmxpc2ggY29ubmVjdGlvblxuXHRcdFx0dGhpcy5jb25uZWN0aW9uLmNvbm5lY3QoKGVycikgPT4ge1xuXHRcdFx0XHRsb2dXaXRoVGltZSh0aGlzLmNvbmZpZyk7XG5cblx0XHRcdFx0dGhpcy5pc0Nvbm5lY3RpbmcgPSBmYWxzZTtcblxuXHRcdFx0XHRpZiAoZXJyKSB7XG5cdFx0XHRcdFx0Y29uc29sZS5lcnJvcignZXJyb3IgY29ubmVjdGluZzogJyArIGVyci5zdGFjayk7XG5cblx0XHRcdFx0XHRyZXNvbHZlKGZhbHNlKTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRsb2dXaXRoVGltZSgnY29ubmVjdGVkIGFzIGlkICcgKyB0aGlzLmNvbm5lY3Rpb24udGhyZWFkSWQpO1xuXHRcdFx0XHRcdGxvZ1dpdGhUaW1lKCdNeVNRTENvbm5lY3Rpb24gU3RhdGU6JywgdGhpcy5jb25uZWN0aW9uLnN0YXRlKTtcblxuXG5cdFx0XHRcdFx0Ly8gc2V0IHF1ZXJ5IGZvcm1hdFxuXHRcdFx0XHRcdHRoaXMuY29ubmVjdGlvbi5jb25maWcucXVlcnlGb3JtYXQgPSBmdW5jdGlvbiAocXVlcnksIHZhbHVlcykge1xuXHRcdFx0XHRcdFx0aWYgKCF2YWx1ZXMpIHtyZXR1cm4gcXVlcnk7IH1cblxuXHRcdFx0XHRcdFx0Y29uc3QgZXNjYXBlZFF1ZXJ5ID0gcXVlcnkucmVwbGFjZSgvXFw6KFxcdyspL2csIGZ1bmN0aW9uICh0eHQsIGtleSkge1xuXHRcdFx0XHRcdFx0XHRpZiAodmFsdWVzLmhhc093blByb3BlcnR5KGtleSkpIHtcblx0XHRcdFx0XHRcdFx0XHRyZXR1cm4gdGhpcy5lc2NhcGUodmFsdWVzW2tleV0pO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdHJldHVybiB0eHQ7XG5cdFx0XHRcdFx0XHRcdH0uYmluZCh0aGlzKSk7XG5cblxuXHRcdFx0XHRcdFx0aWYgKGxvZ1NxbFF1ZXJpZXMpIHtcblx0XHRcdFx0XHRcdFx0Y29uc29sZS5sb2coe2VzY2FwZWRRdWVyeX0pO1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHRyZXR1cm4gZXNjYXBlZFF1ZXJ5O1xuXHRcdFx0XHRcdH07XG5cblxuXHRcdFx0XHRcdHJlc29sdmUodHJ1ZSk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXG5cdFx0XHR0aGlzLmNvbm5lY3Rpb24ub24oJ2Vycm9yJywgKGVycikgPT4ge1xuXHRcdFx0XHRsb2dXaXRoVGltZSgnKioqKioqKioqKioqKioqKioqKiBNeVNRTCBDb25uZWN0aW9uIG9uRXJyb3IgRVZFTlQ6ICoqKioqKioqKioqKioqKioqKionKTtcblxuXHRcdFx0XHRjb25zb2xlLmVycm9yKGVycik7XG5cblxuXHRcdFx0XHR0aGlzLnByaW50Q29ubmVjdGlvblN0YXRlKCk7XG5cdFx0XHR9KTtcblx0XHR9ICk7XG5cdH1cblxuXG5cdHByaXZhdGUgcHJpbnRDb25uZWN0aW9uU3RhdGUoKSB7XG5cdFx0bG9nV2l0aFRpbWUoJ015U1FMQ29ubmVjdGlvbiBTdGF0ZTonLCB0aGlzLmNvbm5lY3Rpb24uc3RhdGUpO1xuXHR9XG5cblx0LypcbiAgICBwcml2YXRlIGdldCBpc0Rpc2Nvbm5lY3RlZCgpOmJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5jb25uZWN0aW9uID09IHVuZGVmaW5lZCB8fFxuICAgICAgICAgICAgICAgIHRoaXMuY29ubmVjdGlvbi5zdGF0ZSA9PT0gJ2Rpc2Nvbm5lY3RlZCc7XG4gICAgfVxuICAgICovXG5cdHByaXZhdGUgZ2V0IGlzQ29ubmVjdGVkKCk6IGJvb2xlYW4ge1xuXHRcdHJldHVybiB0aGlzLmNvbm5lY3Rpb24gIT09IHVuZGVmaW5lZCAmJiAoXG5cdFx0XHR0aGlzLmNvbm5lY3Rpb24uc3RhdGUgPT09ICdjb25uZWN0ZWQnIHx8XG5cdFx0XHR0aGlzLmNvbm5lY3Rpb24uc3RhdGUgPT09ICdhdXRoZW50aWNhdGVkJ1xuXHRcdCk7XG5cdH1cblxuXG5cdGVuZENvbm5lY3Rpb24oKSB7XG5cdFx0dGhpcy5jb25uZWN0aW9uLmVuZCgpO1xuXHR9XG59XG4iXX0=