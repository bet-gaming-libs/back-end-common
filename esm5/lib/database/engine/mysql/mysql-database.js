/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-database.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { MySQLQueryBuilder } from './mysql-query-builder';
import { MySQLTransactionManager } from './mysql-transaction-manager';
var MySQLDatabase = /** @class */ (function () {
    function MySQLDatabase(config) {
        this.builder = new MySQLQueryBuilder(config);
        this.transactionManager = new MySQLTransactionManager(config);
    }
    return MySQLDatabase;
}());
export { MySQLDatabase };
if (false) {
    /** @type {?} */
    MySQLDatabase.prototype.builder;
    /** @type {?} */
    MySQLDatabase.prototype.transactionManager;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtZGF0YWJhc2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9kYXRhYmFzZS9lbmdpbmUvbXlzcWwvbXlzcWwtZGF0YWJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFDQSxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSx1QkFBdUIsQ0FBQztBQUN4RCxPQUFPLEVBQUMsdUJBQXVCLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQUdwRTtJQUlDLHVCQUFZLE1BQW9CO1FBQy9CLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSx1QkFBdUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBQ0Ysb0JBQUM7QUFBRCxDQUFDLEFBUkQsSUFRQzs7OztJQVBBLGdDQUFvQzs7SUFDcEMsMkNBQXFEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSU15U1FMRGF0YWJhc2UsIElNeVNRTENvbmZpZyB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5pbXBvcnQge015U1FMUXVlcnlCdWlsZGVyfSBmcm9tICcuL215c3FsLXF1ZXJ5LWJ1aWxkZXInO1xuaW1wb3J0IHtNeVNRTFRyYW5zYWN0aW9uTWFuYWdlcn0gZnJvbSAnLi9teXNxbC10cmFuc2FjdGlvbi1tYW5hZ2VyJztcblxuXG5leHBvcnQgY2xhc3MgTXlTUUxEYXRhYmFzZSBpbXBsZW1lbnRzIElNeVNRTERhdGFiYXNlIHtcblx0cmVhZG9ubHkgYnVpbGRlcjogTXlTUUxRdWVyeUJ1aWxkZXI7XG5cdHJlYWRvbmx5IHRyYW5zYWN0aW9uTWFuYWdlcjogTXlTUUxUcmFuc2FjdGlvbk1hbmFnZXI7XG5cblx0Y29uc3RydWN0b3IoY29uZmlnOiBJTXlTUUxDb25maWcpIHtcblx0XHR0aGlzLmJ1aWxkZXIgPSBuZXcgTXlTUUxRdWVyeUJ1aWxkZXIoY29uZmlnKTtcblx0XHR0aGlzLnRyYW5zYWN0aW9uTWFuYWdlciA9IG5ldyBNeVNRTFRyYW5zYWN0aW9uTWFuYWdlcihjb25maWcpO1xuXHR9XG59XG4iXX0=