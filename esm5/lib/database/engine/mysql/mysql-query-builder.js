/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-query-builder.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { MySQLConnectionManager } from './mysql-connection-manager';
import { MySQLExpression, SingleExpression, StringStatement, OrExpressionGroup, UpdateStatements, createExpressionGroup, ExpressionGroupJoiner } from './mysql-statements';
import { MySQLQuery } from './mysql-query';
var MySQLQueryBuilder = /** @class */ (function () {
    function MySQLQueryBuilder(config) {
        this.connectionManager = new MySQLConnectionManager(config);
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    MySQLQueryBuilder.prototype.insert = /**
     * @template T
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    function (repository, entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // construct query
                        return [4 /*yield*/, this.constructInsertQuery(repository, entities)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, new MySQLQuery(query, this.connectionManager)];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    MySQLQueryBuilder.prototype.constructInsertQuery = /**
     * @private
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    function (repository, entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var fields, fieldName, rows, entities_1, entities_1_1, entity, _a, _b, e_1_1;
            var e_1, _c;
            return tslib_1.__generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        fields = [];
                        for (fieldName in entities[0]) {
                            fields.push(fieldName);
                        }
                        rows = [];
                        _d.label = 1;
                    case 1:
                        _d.trys.push([1, 6, 7, 8]);
                        entities_1 = tslib_1.__values(entities), entities_1_1 = entities_1.next();
                        _d.label = 2;
                    case 2:
                        if (!!entities_1_1.done) return [3 /*break*/, 5];
                        entity = entities_1_1.value;
                        _b = (_a = rows).push;
                        return [4 /*yield*/, this.constructRowForInsert(entity)];
                    case 3:
                        _b.apply(_a, [_d.sent()]);
                        _d.label = 4;
                    case 4:
                        entities_1_1 = entities_1.next();
                        return [3 /*break*/, 2];
                    case 5: return [3 /*break*/, 8];
                    case 6:
                        e_1_1 = _d.sent();
                        e_1 = { error: e_1_1 };
                        return [3 /*break*/, 8];
                    case 7:
                        try {
                            if (entities_1_1 && !entities_1_1.done && (_c = entities_1.return)) _c.call(entities_1);
                        }
                        finally { if (e_1) throw e_1.error; }
                        return [7 /*endfinally*/];
                    case 8: return [2 /*return*/, 'INSERT INTO ' + repository.tableName +
                            ' (' + this.prepareFieldList(fields) + ')' +
                            ' VALUES ' + rows.join(', ') + ';'];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} entity
     * @return {?}
     */
    MySQLQueryBuilder.prototype.constructRowForInsert = /**
     * @private
     * @param {?} entity
     * @return {?}
     */
    function (entity) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var values, _a, _b, _i, propertyName, rawValue, preparedValue;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        values = [];
                        _a = [];
                        for (_b in entity)
                            _a.push(_b);
                        _i = 0;
                        _c.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3 /*break*/, 4];
                        propertyName = _a[_i];
                        rawValue = entity[propertyName];
                        return [4 /*yield*/, this.prepareValueForInsert(rawValue)];
                    case 2:
                        preparedValue = _c.sent();
                        values.push(preparedValue);
                        _c.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/, '(' + values.join(',') + ')'];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareValueForInsert = /**
     * @private
     * @param {?} value
     * @return {?}
     */
    function (value) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = typeof value;
                        switch (_a) {
                            case 'string': return [3 /*break*/, 1];
                        }
                        return [3 /*break*/, 6];
                    case 1:
                        _b = value;
                        switch (_b) {
                            case MySQLExpression.NULL: return [3 /*break*/, 2];
                            case MySQLExpression.NOW: return [3 /*break*/, 2];
                        }
                        return [3 /*break*/, 3];
                    case 2: return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, this.escape(value)];
                    case 4:
                        value = _c.sent();
                        return [3 /*break*/, 5];
                    case 5: return [3 /*break*/, 8];
                    case 6: return [4 /*yield*/, this.escape(value)];
                    case 7:
                        value = _c.sent();
                        return [3 /*break*/, 8];
                    case 8: return [2 /*return*/, '' + value];
                }
            });
        });
    };
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @param {?} primaryKeyValues
     * @return {?}
     */
    MySQLQueryBuilder.prototype.selectByPrimaryKeys = /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @param {?} primaryKeyValues
     * @return {?}
     */
    function (repository, fields, primaryKeyValues) {
        /** @type {?} */
        var query = this.prepareFieldsForSelect(fields, repository.tableName) +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    };
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @return {?}
     */
    MySQLQueryBuilder.prototype.selectAll = /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @return {?}
     */
    function (repository, fields) {
        /** @type {?} */
        var query = this.prepareFieldsForSelect(fields, repository.tableName) + ';';
        return new MySQLQuery(query, this.connectionManager);
    };
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    MySQLQueryBuilder.prototype.select = /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.prepareSelectStatement(params)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, new MySQLQuery(query + ";", this.connectionManager)];
                }
            });
        });
    };
    /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareSelectStatement = /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var fields, repository, query, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        fields = params.fields, repository = params.repository;
                        query = this.prepareFieldsForSelect(fields, repository.tableName);
                        _a = query;
                        return [4 /*yield*/, this.prepareWhereOrderAndLimit(params)];
                    case 1:
                        query = _a + _b.sent();
                        return [2 /*return*/, query];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} fields
     * @param {?} tableName
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareFieldsForSelect = /**
     * @private
     * @param {?} fields
     * @param {?} tableName
     * @return {?}
     */
    function (fields, tableName) {
        /** @type {?} */
        var fieldsList = this.prepareFieldList(fields);
        return "SELECT " + fieldsList + " FROM " + tableName;
    };
    /**
     * @private
     * @param {?} fields
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareFieldList = /**
     * @private
     * @param {?} fields
     * @return {?}
     */
    function (fields) {
        var e_2, _a;
        /** @type {?} */
        var prepared = [];
        try {
            for (var fields_1 = tslib_1.__values(fields), fields_1_1 = fields_1.next(); !fields_1_1.done; fields_1_1 = fields_1.next()) {
                var field = fields_1_1.value;
                /** @type {?} */
                var isAlias = field.indexOf(' AS ') > -1;
                prepared.push(isAlias ?
                    field :
                    '`' + field + '`');
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (fields_1_1 && !fields_1_1.done && (_a = fields_1.return)) _a.call(fields_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
        return prepared.join(',');
    };
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    MySQLQueryBuilder.prototype.update = /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var statements, repository, updateStatements, remainingQuery, query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (params.singleStatement) {
                            params.statements = [
                                params.singleStatement
                            ];
                        }
                        statements = params.statements, repository = params.repository;
                        if (!statements ||
                            statements.length < 1) {
                            throw new Error('there must be at least 1 update statement!');
                        }
                        return [4 /*yield*/, this.prepareExpressionGroup(new UpdateStatements(statements))];
                    case 1:
                        updateStatements = _a.sent();
                        return [4 /*yield*/, this.prepareWhereOrderAndLimit(params)];
                    case 2:
                        remainingQuery = _a.sent();
                        query = 'UPDATE ' + repository.tableName +
                            ' SET ' + updateStatements +
                            remainingQuery + ';';
                        return [2 /*return*/, new MySQLQuery(query, this.connectionManager)];
                }
            });
        });
    };
    /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareWhereOrderAndLimit = /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var expressions, whereGroup, orderBy, limit, query, whereClause, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (params.whereMap) {
                            params.whereAndMap = params.whereMap;
                        }
                        if (params.whereAndMap ||
                            params.whereOrMap) {
                            params.whereGroup = createExpressionGroup(params.whereAndMap ?
                                params.whereAndMap :
                                params.whereOrMap, params.whereAndMap ?
                                ExpressionGroupJoiner.AND :
                                ExpressionGroupJoiner.OR);
                        }
                        else if (params.wherePrimaryKeys) {
                            expressions = params.wherePrimaryKeys.map((/**
                             * @param {?} primaryKeyValue
                             * @return {?}
                             */
                            function (primaryKeyValue) { return new StringStatement(params.repository.primaryKey, '' + primaryKeyValue); }));
                            params.whereGroup = new OrExpressionGroup(expressions);
                        }
                        else if (params.whereClause) {
                            params.whereGroup = new SingleExpression(params.whereClause);
                        }
                        whereGroup = params.whereGroup, orderBy = params.orderBy, limit = params.limit;
                        query = '';
                        _a = whereGroup;
                        if (!_a) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.prepareExpressionGroup(whereGroup)];
                    case 1:
                        _a = (_b.sent());
                        _b.label = 2;
                    case 2:
                        whereClause = _a;
                        if (whereClause) {
                            query += "\n                WHERE " + whereClause;
                        }
                        if (orderBy) {
                            query += "\n                ORDER BY " + orderBy;
                        }
                        if (limit) {
                            query += "\n                LIMIT " + limit;
                        }
                        return [2 /*return*/, query];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} group
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareExpressionGroup = /**
     * @private
     * @param {?} group
     * @return {?}
     */
    function (group) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var statements, useBrackets, _a, _b, expression, leftSide, operator, rightSide, rightSideValue, _c, _d, e_3_1;
            var e_3, _e;
            return tslib_1.__generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        statements = [];
                        useBrackets = group.surroundExpressionWithBrackets;
                        _f.label = 1;
                    case 1:
                        _f.trys.push([1, 8, 9, 10]);
                        _a = tslib_1.__values(group.expressions), _b = _a.next();
                        _f.label = 2;
                    case 2:
                        if (!!_b.done) return [3 /*break*/, 7];
                        expression = _b.value;
                        leftSide = expression.leftSide, operator = expression.operator, rightSide = expression.rightSide;
                        if (!rightSide.isString) return [3 /*break*/, 4];
                        _d = "";
                        return [4 /*yield*/, this.escape((/** @type {?} */ (rightSide.data)))];
                    case 3:
                        _c = _d + (_f.sent());
                        return [3 /*break*/, 5];
                    case 4:
                        _c = rightSide.data;
                        _f.label = 5;
                    case 5:
                        rightSideValue = _c;
                        statements.push((useBrackets ? '(' : '') + " `" + leftSide + "` " + operator + " " + rightSideValue + " " + (useBrackets ? ')' : ''));
                        _f.label = 6;
                    case 6:
                        _b = _a.next();
                        return [3 /*break*/, 2];
                    case 7: return [3 /*break*/, 10];
                    case 8:
                        e_3_1 = _f.sent();
                        e_3 = { error: e_3_1 };
                        return [3 /*break*/, 10];
                    case 9:
                        try {
                            if (_b && !_b.done && (_e = _a.return)) _e.call(_a);
                        }
                        finally { if (e_3) throw e_3.error; }
                        return [7 /*endfinally*/];
                    case 10: return [2 /*return*/, statements.join(" " + group.joiner + " ")];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    MySQLQueryBuilder.prototype.escape = /**
     * @private
     * @param {?} value
     * @return {?}
     */
    function (value) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var connection;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connectionManager.getConnection()];
                    case 1:
                        connection = _a.sent();
                        return [2 /*return*/, connection.escape(value)];
                }
            });
        });
    };
    /**
     * @param {?} repository
     * @param {?} primaryKeyValues
     * @return {?}
     */
    MySQLQueryBuilder.prototype.delete = /**
     * @param {?} repository
     * @param {?} primaryKeyValues
     * @return {?}
     */
    function (repository, primaryKeyValues) {
        if (primaryKeyValues.length < 1) {
            throw new Error('Please specify which records to delete!');
        }
        /** @type {?} */
        var query = 'DELETE FROM ' + repository.tableName +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    };
    /**
     * @template T
     * @param {?} sql
     * @param {?=} argumentsToPrepare
     * @return {?}
     */
    MySQLQueryBuilder.prototype.rawQuery = /**
     * @template T
     * @param {?} sql
     * @param {?=} argumentsToPrepare
     * @return {?}
     */
    function (sql, argumentsToPrepare) {
        return new MySQLQuery(sql, this.connectionManager, argumentsToPrepare);
    };
    return MySQLQueryBuilder;
}());
export { MySQLQueryBuilder };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLQueryBuilder.prototype.connectionManager;
}
/**
 * @param {?} repository
 * @param {?} primaryKeyValues
 * @return {?}
 */
function constructWhereWithPrimaryKeys(repository, primaryKeyValues) {
    var e_4, _a;
    /** @type {?} */
    var conditions = [];
    try {
        for (var primaryKeyValues_1 = tslib_1.__values(primaryKeyValues), primaryKeyValues_1_1 = primaryKeyValues_1.next(); !primaryKeyValues_1_1.done; primaryKeyValues_1_1 = primaryKeyValues_1.next()) {
            var primaryKeyValue = primaryKeyValues_1_1.value;
            if (repository.isPrimaryKeyAString) {
                primaryKeyValue = '\'' + primaryKeyValue + '\'';
            }
            /** @type {?} */
            var condition = repository.primaryKey + ' = ' + primaryKeyValue;
            conditions.push(condition);
        }
    }
    catch (e_4_1) { e_4 = { error: e_4_1 }; }
    finally {
        try {
            if (primaryKeyValues_1_1 && !primaryKeyValues_1_1.done && (_a = primaryKeyValues_1.return)) _a.call(primaryKeyValues_1);
        }
        finally { if (e_4) throw e_4.error; }
    }
    return ' WHERE ' + conditions.join(' OR ');
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcXVlcnktYnVpbGRlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RhdGFiYXNlL2VuZ2luZS9teXNxbC9teXNxbC1xdWVyeS1idWlsZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBbUIsZUFBZSxFQUFFLGdCQUFnQixFQUFFLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxnQkFBZ0IsRUFBRSxxQkFBcUIsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQzVMLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFHekM7SUFHQywyQkFBWSxNQUFvQjtRQUMvQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM3RCxDQUFDOzs7Ozs7O0lBRVksa0NBQU07Ozs7OztJQUFuQixVQUNDLFVBQTRCLEVBQzVCLFFBQWE7Ozs7Ozs7d0JBR0MscUJBQU0sSUFBSSxDQUFDLG9CQUFvQixDQUM1QyxVQUFVLEVBQUUsUUFBUSxDQUNwQixFQUFBOzt3QkFGSyxLQUFLLEdBQUcsU0FFYjt3QkFFRCxzQkFBTyxJQUFJLFVBQVUsQ0FBb0IsS0FBSyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFDOzs7O0tBQ3hFOzs7Ozs7O0lBQ2EsZ0RBQW9COzs7Ozs7SUFBbEMsVUFDQyxVQUE0QixFQUM1QixRQUFlOzs7Ozs7O3dCQUVULE1BQU0sR0FBYSxFQUFFO3dCQUMzQixLQUFXLFNBQVMsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUU7NEJBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7eUJBQ3ZCO3dCQUVLLElBQUksR0FBYSxFQUFFOzs7O3dCQUNKLGFBQUEsaUJBQUEsUUFBUSxDQUFBOzs7O3dCQUFsQixNQUFNO3dCQUNoQixLQUFBLENBQUEsS0FBQSxJQUFJLENBQUEsQ0FBQyxJQUFJLENBQUE7d0JBQ1IscUJBQU0sSUFBSSxDQUFDLHFCQUFxQixDQUMvQixNQUFNLENBQ04sRUFBQTs7d0JBSEYsY0FDQyxTQUVDLEVBQ0QsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs0QkFHSCxzQkFBTyxjQUFjLEdBQUcsVUFBVSxDQUFDLFNBQVM7NEJBQzNDLElBQUksR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLEdBQUcsR0FBRzs0QkFDMUMsVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFDOzs7O0tBRXBDOzs7Ozs7SUFDYSxpREFBcUI7Ozs7O0lBQW5DLFVBQ0MsTUFBVzs7Ozs7O3dCQUVMLE1BQU0sR0FBYSxFQUFFOzttQ0FFQSxNQUFNOzs7Ozs7O3dCQUMxQixRQUFRLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQzt3QkFHcEMscUJBQU0sSUFBSSxDQUFDLHFCQUFxQixDQUFDLFFBQVEsQ0FBQyxFQUFBOzt3QkFEckMsYUFBYSxHQUNsQixTQUEwQzt3QkFFM0MsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzs7Ozs7NEJBRzVCLHNCQUFPLEdBQUcsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsRUFBQzs7OztLQUNwQzs7Ozs7O0lBQ2EsaURBQXFCOzs7OztJQUFuQyxVQUFvQyxLQUFVOzs7Ozs7d0JBQ3JDLEtBQUEsT0FBTyxLQUFLLENBQUE7O2lDQUNkLFFBQVEsQ0FBQyxDQUFULHdCQUFROzs7O3dCQUNKLEtBQUEsS0FBSyxDQUFBOztpQ0FDUCxlQUFlLENBQUMsSUFBSSxDQUFDLENBQXJCLHdCQUFvQjtpQ0FDcEIsZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFwQix3QkFBbUI7Ozs0QkFDeEIsd0JBQU07NEJBR0cscUJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBQTs7d0JBQWhDLEtBQUssR0FBRyxTQUF3QixDQUFDO3dCQUNsQyx3QkFBTTs0QkFFUix3QkFBTTs0QkFHRyxxQkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFBOzt3QkFBaEMsS0FBSyxHQUFHLFNBQXdCLENBQUM7d0JBQ2xDLHdCQUFNOzRCQUdQLHNCQUFPLEVBQUUsR0FBRyxLQUFLLEVBQUM7Ozs7S0FDbEI7Ozs7Ozs7O0lBR00sK0NBQW1COzs7Ozs7O0lBQTFCLFVBQ0MsVUFBNEIsRUFDNUIsTUFBZ0IsRUFDaEIsZ0JBQW1DOztZQUU3QixLQUFLLEdBQ1YsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDO1lBQ3pELDZCQUE2QixDQUFDLFVBQVUsRUFBRSxnQkFBZ0IsQ0FBQyxHQUFHLEdBQUc7UUFFbEUsT0FBTyxJQUFJLFVBQVUsQ0FBTSxLQUFLLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDM0QsQ0FBQzs7Ozs7OztJQUVNLHFDQUFTOzs7Ozs7SUFBaEIsVUFDQyxVQUE0QixFQUM1QixNQUFnQjs7WUFFVixLQUFLLEdBQ1YsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsR0FBRztRQUVoRSxPQUFPLElBQUksVUFBVSxDQUFNLEtBQUssRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUMzRCxDQUFDOzs7Ozs7SUFHWSxrQ0FBTTs7Ozs7SUFBbkIsVUFBdUIsTUFBb0M7Ozs7OzRCQUM1QyxxQkFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUFqRCxLQUFLLEdBQUcsU0FBeUM7d0JBRXZELHNCQUFPLElBQUksVUFBVSxDQUFTLEtBQUssTUFBRyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFDOzs7O0tBQ2hFOzs7Ozs7O0lBRWEsa0RBQXNCOzs7Ozs7SUFBcEMsVUFBd0MsTUFBb0M7Ozs7Ozt3QkFDcEUsTUFBTSxHQUFnQixNQUFNLE9BQXRCLEVBQUUsVUFBVSxHQUFJLE1BQU0sV0FBVjt3QkFFckIsS0FBSyxHQUFXLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQzt3QkFFN0UsS0FBQSxLQUFLLENBQUE7d0JBQUkscUJBQU0sSUFBSSxDQUFDLHlCQUF5QixDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBckQsS0FBSyxHQUFMLEtBQVMsU0FBNEMsQ0FBQzt3QkFFdEQsc0JBQU8sS0FBSyxFQUFDOzs7O0tBQ2I7Ozs7Ozs7SUFFTyxrREFBc0I7Ozs7OztJQUE5QixVQUErQixNQUFnQixFQUFFLFNBQWlCOztZQUMzRCxVQUFVLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQztRQUVoRCxPQUFPLFlBQVUsVUFBVSxjQUFTLFNBQVcsQ0FBQztJQUNqRCxDQUFDOzs7Ozs7SUFHTyw0Q0FBZ0I7Ozs7O0lBQXhCLFVBQXlCLE1BQWdCOzs7WUFDbEMsUUFBUSxHQUFhLEVBQUU7O1lBRTdCLEtBQW9CLElBQUEsV0FBQSxpQkFBQSxNQUFNLENBQUEsOEJBQUEsa0RBQUU7Z0JBQXZCLElBQU0sS0FBSyxtQkFBQTs7b0JBQ1QsT0FBTyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUUxQyxRQUFRLENBQUMsSUFBSSxDQUNaLE9BQU8sQ0FBQyxDQUFDO29CQUNULEtBQUssQ0FBQyxDQUFDO29CQUNQLEdBQUcsR0FBRyxLQUFLLEdBQUcsR0FBRyxDQUNqQixDQUFDO2FBQ0Y7Ozs7Ozs7OztRQUVELE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUMzQixDQUFDOzs7Ozs7SUFFWSxrQ0FBTTs7Ozs7SUFBbkIsVUFBdUIsTUFBb0M7Ozs7Ozt3QkFDMUQsSUFBSSxNQUFNLENBQUMsZUFBZSxFQUFFOzRCQUMzQixNQUFNLENBQUMsVUFBVSxHQUFHO2dDQUNuQixNQUFNLENBQUMsZUFBZTs2QkFDdEIsQ0FBQzt5QkFDRjt3QkFHTSxVQUFVLEdBQWdCLE1BQU0sV0FBdEIsRUFBRSxVQUFVLEdBQUksTUFBTSxXQUFWO3dCQUU3QixJQUNDLENBQUMsVUFBVTs0QkFDWCxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFDcEI7NEJBQ0QsTUFBTSxJQUFJLEtBQUssQ0FBQyw0Q0FBNEMsQ0FBQyxDQUFDO3lCQUM5RDt3QkFHZ0MscUJBQU0sSUFBSSxDQUFDLHNCQUFzQixDQUNqRSxJQUFJLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxDQUNoQyxFQUFBOzt3QkFGSyxnQkFBZ0IsR0FBVyxTQUVoQzt3QkFFOEIscUJBQU0sSUFBSSxDQUFDLHlCQUF5QixDQUNsRSxNQUFNLENBQ04sRUFBQTs7d0JBRkssY0FBYyxHQUFXLFNBRTlCO3dCQUVLLEtBQUssR0FDVixTQUFTLEdBQUcsVUFBVSxDQUFDLFNBQVM7NEJBQ2hDLE9BQU8sR0FBRyxnQkFBZ0I7NEJBQ3pCLGNBQWMsR0FBRyxHQUFHO3dCQUV0QixzQkFBTyxJQUFJLFVBQVUsQ0FBb0IsS0FBSyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFDOzs7O0tBQ3hFOzs7Ozs7O0lBRWEscURBQXlCOzs7Ozs7SUFBdkMsVUFBMkMsTUFBOEI7Ozs7Ozt3QkFDeEUsSUFBSSxNQUFNLENBQUMsUUFBUSxFQUFFOzRCQUNwQixNQUFNLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUM7eUJBQ3JDO3dCQUVELElBQ0MsTUFBTSxDQUFDLFdBQVc7NEJBQ2xCLE1BQU0sQ0FBQyxVQUFVLEVBQ2hCOzRCQUNELE1BQU0sQ0FBQyxVQUFVLEdBQUcscUJBQXFCLENBQ3hDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztnQ0FDbkIsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dDQUNwQixNQUFNLENBQUMsVUFBVSxFQUNsQixNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7Z0NBQ25CLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxDQUFDO2dDQUMzQixxQkFBcUIsQ0FBQyxFQUFFLENBQ3pCLENBQUM7eUJBQ0Y7NkJBQU0sSUFBSSxNQUFNLENBQUMsZ0JBQWdCLEVBQUU7NEJBQzdCLFdBQVcsR0FBc0IsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEdBQUc7Ozs7NEJBQ2pFLFVBQUMsZUFBZSxJQUFLLE9BQUEsSUFBSSxlQUFlLENBQ3ZDLE1BQU0sQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUM1QixFQUFFLEdBQUcsZUFBZSxDQUNwQixFQUhvQixDQUdwQixFQUNEOzRCQUVELE1BQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsQ0FBQzt5QkFDdkQ7NkJBQU0sSUFBSSxNQUFNLENBQUMsV0FBVyxFQUFFOzRCQUM5QixNQUFNLENBQUMsVUFBVSxHQUFHLElBQUksZ0JBQWdCLENBQ3ZDLE1BQU0sQ0FBQyxXQUFXLENBQ2xCLENBQUM7eUJBQ0Y7d0JBR00sVUFBVSxHQUFvQixNQUFNLFdBQTFCLEVBQUUsT0FBTyxHQUFXLE1BQU0sUUFBakIsRUFBRSxLQUFLLEdBQUksTUFBTSxNQUFWO3dCQUU3QixLQUFLLEdBQUcsRUFBRTt3QkFHYixLQUFBLFVBQVUsQ0FBQTtpQ0FBVix3QkFBVTt3QkFDVixxQkFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxDQUFDLEVBQUE7OzhCQUE3QyxTQUE2Qzs7O3dCQUZ4QyxXQUFXLEtBRTZCO3dCQUU5QyxJQUFJLFdBQVcsRUFBRTs0QkFDaEIsS0FBSyxJQUFJLDZCQUNZLFdBQWEsQ0FBQzt5QkFDbkM7d0JBRUQsSUFBSSxPQUFPLEVBQUU7NEJBQ1osS0FBSyxJQUFJLGdDQUNlLE9BQVMsQ0FBQzt5QkFDbEM7d0JBRUQsSUFBSSxLQUFLLEVBQUU7NEJBQ1YsS0FBSyxJQUFJLDZCQUNZLEtBQU8sQ0FBQzt5QkFDN0I7d0JBRUQsc0JBQU8sS0FBSyxFQUFDOzs7O0tBQ2I7Ozs7OztJQUVhLGtEQUFzQjs7Ozs7SUFBcEMsVUFBcUMsS0FBc0I7Ozs7Ozs7d0JBQ3BELFVBQVUsR0FBYSxFQUFFO3dCQUV6QixXQUFXLEdBQUcsS0FBSyxDQUFDLDhCQUE4Qjs7Ozt3QkFFL0IsS0FBQSxpQkFBQSxLQUFLLENBQUMsV0FBVyxDQUFBOzs7O3dCQUEvQixVQUFVO3dCQUNiLFFBQVEsR0FBeUIsVUFBVSxTQUFuQyxFQUFFLFFBQVEsR0FBZSxVQUFVLFNBQXpCLEVBQUUsU0FBUyxHQUFJLFVBQVUsVUFBZDs2QkFHbkMsU0FBUyxDQUFDLFFBQVEsRUFBbEIsd0JBQWtCOzt3QkFDYixxQkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFBLFNBQVMsQ0FBQyxJQUFJLEVBQVUsQ0FBQyxFQUFBOzt3QkFBL0MsS0FBQSxNQUFJLFNBQTJDLENBQUcsQ0FBQTs7O3dCQUNsRCxLQUFBLFNBQVMsQ0FBQyxJQUFJLENBQUE7Ozt3QkFIVixjQUFjLEtBR0o7d0JBRWhCLFVBQVUsQ0FBQyxJQUFJLENBQ2QsQ0FDQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxXQUNqQixRQUFRLFVBQU0sUUFBUSxTQUFJLGNBQWMsVUFDN0MsV0FBVyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FDckIsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7OzZCQUdOLHNCQUFPLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBSSxLQUFLLENBQUMsTUFBTSxNQUFHLENBQUMsRUFBQzs7OztLQUM1Qzs7Ozs7O0lBR2Esa0NBQU07Ozs7O0lBQXBCLFVBQXFCLEtBQWE7Ozs7OzRCQUNkLHFCQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsRUFBQTs7d0JBQXpELFVBQVUsR0FBRyxTQUE0Qzt3QkFFL0Qsc0JBQU8sVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBQzs7OztLQUNoQzs7Ozs7O0lBRU0sa0NBQU07Ozs7O0lBQWIsVUFDQyxVQUE0QixFQUM1QixnQkFBdUI7UUFFdkIsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ2hDLE1BQU0sSUFBSSxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQztTQUMzRDs7WUFHSyxLQUFLLEdBQUcsY0FBYyxHQUFHLFVBQVUsQ0FBQyxTQUFTO1lBQ2hELDZCQUE2QixDQUFDLFVBQVUsRUFBRSxnQkFBZ0IsQ0FBQyxHQUFHLEdBQUc7UUFFcEUsT0FBTyxJQUFJLFVBQVUsQ0FBb0IsS0FBSyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7Ozs7Ozs7SUFFRCxvQ0FBUTs7Ozs7O0lBQVIsVUFBWSxHQUFXLEVBQUUsa0JBQXdCO1FBQ2hELE9BQU8sSUFBSSxVQUFVLENBQUksR0FBRyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO0lBQzNFLENBQUM7SUFDRix3QkFBQztBQUFELENBQUMsQUE1UkQsSUE0UkM7Ozs7Ozs7SUEzUkEsOENBQWtEOzs7Ozs7O0FBOFJuRCxTQUFTLDZCQUE2QixDQUNyQyxVQUE0QixFQUM1QixnQkFBdUI7OztRQUVqQixVQUFVLEdBQWEsRUFBRTs7UUFFL0IsS0FBNEIsSUFBQSxxQkFBQSxpQkFBQSxnQkFBZ0IsQ0FBQSxrREFBQSxnRkFBRTtZQUF6QyxJQUFJLGVBQWUsNkJBQUE7WUFDdkIsSUFBSSxVQUFVLENBQUMsbUJBQW1CLEVBQUU7Z0JBQ25DLGVBQWUsR0FBRyxJQUFJLEdBQUcsZUFBZSxHQUFHLElBQUksQ0FBQzthQUNoRDs7Z0JBRUssU0FBUyxHQUFHLFVBQVUsQ0FBQyxVQUFVLEdBQUcsS0FBSyxHQUFHLGVBQWU7WUFDakUsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUMzQjs7Ozs7Ozs7O0lBRUQsT0FBTyxTQUFTLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUM1QyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSU15U1FMUmVwb3NpdG9yeSwgUHJpbWFyeUtleVZhbHVlLCBJTXlTUUxRdWVyeVJlc3VsdCwgSU15U1FMQ29uZmlnLCBJU2VsZWN0UXVlcnlCdWlsZGVyUGFyYW1zLCBJVXBkYXRlUXVlcnlCdWlsZGVyUGFyYW1zLCBJUXVlcnlCdWlsZGVyUGFyYW1zIH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIgfSBmcm9tICcuL215c3FsLWNvbm5lY3Rpb24tbWFuYWdlcic7XG5pbXBvcnQgeyBFeHByZXNzaW9uR3JvdXAsIE15U1FMRXhwcmVzc2lvbiwgU2luZ2xlRXhwcmVzc2lvbiwgU3RyaW5nU3RhdGVtZW50LCBPckV4cHJlc3Npb25Hcm91cCwgVXBkYXRlU3RhdGVtZW50cywgY3JlYXRlRXhwcmVzc2lvbkdyb3VwLCBFeHByZXNzaW9uR3JvdXBKb2luZXIgfSBmcm9tICcuL215c3FsLXN0YXRlbWVudHMnO1xuaW1wb3J0IHtNeVNRTFF1ZXJ5fSBmcm9tICcuL215c3FsLXF1ZXJ5JztcblxuXG5leHBvcnQgY2xhc3MgTXlTUUxRdWVyeUJ1aWxkZXIge1xuXHRwcml2YXRlIGNvbm5lY3Rpb25NYW5hZ2VyOiBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyO1xuXG5cdGNvbnN0cnVjdG9yKGNvbmZpZzogSU15U1FMQ29uZmlnKSB7XG5cdFx0dGhpcy5jb25uZWN0aW9uTWFuYWdlciA9IG5ldyBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyKGNvbmZpZyk7XG5cdH1cblxuXHRwdWJsaWMgYXN5bmMgaW5zZXJ0PFQ+KFxuXHRcdHJlcG9zaXRvcnk6IElNeVNRTFJlcG9zaXRvcnksXG5cdFx0ZW50aXRpZXM6IFRbXVxuXHQpIHtcblx0XHQvLyBjb25zdHJ1Y3QgcXVlcnlcblx0XHRjb25zdCBxdWVyeSA9IGF3YWl0IHRoaXMuY29uc3RydWN0SW5zZXJ0UXVlcnkoXG5cdFx0XHRyZXBvc2l0b3J5LCBlbnRpdGllc1xuXHRcdCk7XG5cblx0XHRyZXR1cm4gbmV3IE15U1FMUXVlcnk8SU15U1FMUXVlcnlSZXN1bHQ+KHF1ZXJ5LCB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyKTtcblx0fVxuXHRwcml2YXRlIGFzeW5jIGNvbnN0cnVjdEluc2VydFF1ZXJ5KFxuXHRcdHJlcG9zaXRvcnk6IElNeVNRTFJlcG9zaXRvcnksXG5cdFx0ZW50aXRpZXM6IGFueVtdXG5cdCk6IFByb21pc2U8c3RyaW5nPiB7XG5cdFx0Y29uc3QgZmllbGRzOiBzdHJpbmdbXSA9IFtdO1xuXHRcdGZvciAoY29uc3QgZmllbGROYW1lIGluIGVudGl0aWVzWzBdKSB7XG5cdFx0XHRmaWVsZHMucHVzaChmaWVsZE5hbWUpO1xuXHRcdH1cblxuXHRcdGNvbnN0IHJvd3M6IHN0cmluZ1tdID0gW107XG5cdFx0Zm9yIChjb25zdCBlbnRpdHkgb2YgZW50aXRpZXMpIHtcblx0XHRcdHJvd3MucHVzaChcblx0XHRcdFx0YXdhaXQgdGhpcy5jb25zdHJ1Y3RSb3dGb3JJbnNlcnQoXG5cdFx0XHRcdFx0ZW50aXR5XG5cdFx0XHRcdClcblx0XHRcdCk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuICdJTlNFUlQgSU5UTyAnICsgcmVwb3NpdG9yeS50YWJsZU5hbWUgK1xuXHRcdFx0JyAoJyArIHRoaXMucHJlcGFyZUZpZWxkTGlzdChmaWVsZHMpICsgJyknICtcblx0XHRcdCcgVkFMVUVTICcgKyByb3dzLmpvaW4oJywgJykgKyAnOyc7XG5cblx0fVxuXHRwcml2YXRlIGFzeW5jIGNvbnN0cnVjdFJvd0Zvckluc2VydChcblx0XHRlbnRpdHk6IGFueVxuXHQpOiBQcm9taXNlPHN0cmluZz4ge1xuXHRcdGNvbnN0IHZhbHVlczogc3RyaW5nW10gPSBbXTtcblxuXHRcdGZvciAoY29uc3QgcHJvcGVydHlOYW1lIGluIGVudGl0eSkge1xuXHRcdFx0Y29uc3QgcmF3VmFsdWUgPSBlbnRpdHlbcHJvcGVydHlOYW1lXTtcblxuXHRcdFx0Y29uc3QgcHJlcGFyZWRWYWx1ZTogc3RyaW5nID1cblx0XHRcdFx0YXdhaXQgdGhpcy5wcmVwYXJlVmFsdWVGb3JJbnNlcnQocmF3VmFsdWUpO1xuXG5cdFx0XHR2YWx1ZXMucHVzaChwcmVwYXJlZFZhbHVlKTtcblx0XHR9XG5cblx0XHRyZXR1cm4gJygnICsgdmFsdWVzLmpvaW4oJywnKSArICcpJztcblx0fVxuXHRwcml2YXRlIGFzeW5jIHByZXBhcmVWYWx1ZUZvckluc2VydCh2YWx1ZTogYW55KTogUHJvbWlzZTxzdHJpbmc+IHtcblx0XHRzd2l0Y2ggKHR5cGVvZiB2YWx1ZSkge1xuXHRcdFx0Y2FzZSAnc3RyaW5nJzpcblx0XHRcdFx0c3dpdGNoICh2YWx1ZSkge1xuXHRcdFx0XHRcdGNhc2UgTXlTUUxFeHByZXNzaW9uLk5VTEw6XG5cdFx0XHRcdFx0Y2FzZSBNeVNRTEV4cHJlc3Npb24uTk9XOlxuXHRcdFx0XHRcdGJyZWFrO1xuXG5cdFx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRcdHZhbHVlID0gYXdhaXQgdGhpcy5lc2NhcGUodmFsdWUpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHR9XG5cdFx0XHRicmVhaztcblxuXHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0dmFsdWUgPSBhd2FpdCB0aGlzLmVzY2FwZSh2YWx1ZSk7XG5cdFx0XHRicmVhaztcblx0XHR9XG5cblx0XHRyZXR1cm4gJycgKyB2YWx1ZTtcblx0fVxuXG5cblx0cHVibGljIHNlbGVjdEJ5UHJpbWFyeUtleXM8VD4oXG5cdFx0cmVwb3NpdG9yeTogSU15U1FMUmVwb3NpdG9yeSxcblx0XHRmaWVsZHM6IHN0cmluZ1tdLFxuXHRcdHByaW1hcnlLZXlWYWx1ZXM6IFByaW1hcnlLZXlWYWx1ZVtdXG5cdCkge1xuXHRcdGNvbnN0IHF1ZXJ5ID1cblx0XHRcdHRoaXMucHJlcGFyZUZpZWxkc0ZvclNlbGVjdChmaWVsZHMsIHJlcG9zaXRvcnkudGFibGVOYW1lKSArXG5cdFx0XHRjb25zdHJ1Y3RXaGVyZVdpdGhQcmltYXJ5S2V5cyhyZXBvc2l0b3J5LCBwcmltYXJ5S2V5VmFsdWVzKSArICc7JztcblxuXHRcdHJldHVybiBuZXcgTXlTUUxRdWVyeTxUW10+KHF1ZXJ5LCB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyKTtcblx0fVxuXG5cdHB1YmxpYyBzZWxlY3RBbGw8VD4oXG5cdFx0cmVwb3NpdG9yeTogSU15U1FMUmVwb3NpdG9yeSxcblx0XHRmaWVsZHM6IHN0cmluZ1tdLFxuXHQpIHtcblx0XHRjb25zdCBxdWVyeSA9XG5cdFx0XHR0aGlzLnByZXBhcmVGaWVsZHNGb3JTZWxlY3QoZmllbGRzLCByZXBvc2l0b3J5LnRhYmxlTmFtZSkgKyAnOyc7XG5cblx0XHRyZXR1cm4gbmV3IE15U1FMUXVlcnk8VFtdPihxdWVyeSwgdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG5cdH1cblxuXG5cdHB1YmxpYyBhc3luYyBzZWxlY3Q8VD4ocGFyYW1zOiBJU2VsZWN0UXVlcnlCdWlsZGVyUGFyYW1zPFQ+KSB7XG5cdFx0Y29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLnByZXBhcmVTZWxlY3RTdGF0ZW1lbnQocGFyYW1zKTtcblxuXHRcdHJldHVybiBuZXcgTXlTUUxRdWVyeTxUW10+KGAke3F1ZXJ5fTtgLCB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyKTtcblx0fVxuXG5cdHByaXZhdGUgYXN5bmMgcHJlcGFyZVNlbGVjdFN0YXRlbWVudDxUPihwYXJhbXM6IElTZWxlY3RRdWVyeUJ1aWxkZXJQYXJhbXM8VD4pOiBQcm9taXNlPHN0cmluZz4ge1xuXHRcdGNvbnN0IHtmaWVsZHMsIHJlcG9zaXRvcnl9ID0gcGFyYW1zO1xuXG5cdFx0bGV0IHF1ZXJ5OiBzdHJpbmcgPSB0aGlzLnByZXBhcmVGaWVsZHNGb3JTZWxlY3QoZmllbGRzLCByZXBvc2l0b3J5LnRhYmxlTmFtZSk7XG5cblx0XHRxdWVyeSArPSBhd2FpdCB0aGlzLnByZXBhcmVXaGVyZU9yZGVyQW5kTGltaXQocGFyYW1zKTtcblxuXHRcdHJldHVybiBxdWVyeTtcblx0fVxuXG5cdHByaXZhdGUgcHJlcGFyZUZpZWxkc0ZvclNlbGVjdChmaWVsZHM6IHN0cmluZ1tdLCB0YWJsZU5hbWU6IHN0cmluZyk6IHN0cmluZyB7XG5cdFx0Y29uc3QgZmllbGRzTGlzdCA9IHRoaXMucHJlcGFyZUZpZWxkTGlzdChmaWVsZHMpO1xuXG5cdFx0cmV0dXJuIGBTRUxFQ1QgJHtmaWVsZHNMaXN0fSBGUk9NICR7dGFibGVOYW1lfWA7XG5cdH1cblxuXG5cdHByaXZhdGUgcHJlcGFyZUZpZWxkTGlzdChmaWVsZHM6IHN0cmluZ1tdKTogc3RyaW5nIHtcblx0XHRjb25zdCBwcmVwYXJlZDogc3RyaW5nW10gPSBbXTtcblxuXHRcdGZvciAoY29uc3QgZmllbGQgb2YgZmllbGRzKSB7XG5cdFx0XHRjb25zdCBpc0FsaWFzID0gZmllbGQuaW5kZXhPZignIEFTICcpID4gLTE7XG5cblx0XHRcdHByZXBhcmVkLnB1c2goXG5cdFx0XHRcdGlzQWxpYXMgP1xuXHRcdFx0XHRmaWVsZCA6XG5cdFx0XHRcdCdgJyArIGZpZWxkICsgJ2AnXG5cdFx0XHQpO1xuXHRcdH1cblxuXHRcdHJldHVybiBwcmVwYXJlZC5qb2luKCcsJyk7XG5cdH1cblxuXHRwdWJsaWMgYXN5bmMgdXBkYXRlPFQ+KHBhcmFtczogSVVwZGF0ZVF1ZXJ5QnVpbGRlclBhcmFtczxUPikge1xuXHRcdGlmIChwYXJhbXMuc2luZ2xlU3RhdGVtZW50KSB7XG5cdFx0XHRwYXJhbXMuc3RhdGVtZW50cyA9IFtcblx0XHRcdFx0cGFyYW1zLnNpbmdsZVN0YXRlbWVudFxuXHRcdFx0XTtcblx0XHR9XG5cblxuXHRcdGNvbnN0IHtzdGF0ZW1lbnRzLCByZXBvc2l0b3J5fSA9IHBhcmFtcztcblxuXHRcdGlmIChcblx0XHRcdCFzdGF0ZW1lbnRzIHx8XG5cdFx0XHRzdGF0ZW1lbnRzLmxlbmd0aCA8IDFcblx0XHQpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcigndGhlcmUgbXVzdCBiZSBhdCBsZWFzdCAxIHVwZGF0ZSBzdGF0ZW1lbnQhJyk7XG5cdFx0fVxuXG5cblx0XHRjb25zdCB1cGRhdGVTdGF0ZW1lbnRzOiBzdHJpbmcgPSBhd2FpdCB0aGlzLnByZXBhcmVFeHByZXNzaW9uR3JvdXAoXG5cdFx0XHRuZXcgVXBkYXRlU3RhdGVtZW50cyhzdGF0ZW1lbnRzKVxuXHRcdCk7XG5cblx0XHRjb25zdCByZW1haW5pbmdRdWVyeTogc3RyaW5nID0gYXdhaXQgdGhpcy5wcmVwYXJlV2hlcmVPcmRlckFuZExpbWl0KFxuXHRcdFx0cGFyYW1zXG5cdFx0KTtcblxuXHRcdGNvbnN0IHF1ZXJ5OiBzdHJpbmcgPVxuXHRcdFx0J1VQREFURSAnICsgcmVwb3NpdG9yeS50YWJsZU5hbWUgK1xuXHRcdFx0JyBTRVQgJyArIHVwZGF0ZVN0YXRlbWVudHMgK1xuXHRcdFx0XHRyZW1haW5pbmdRdWVyeSArICc7JztcblxuXHRcdHJldHVybiBuZXcgTXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4ocXVlcnksIHRoaXMuY29ubmVjdGlvbk1hbmFnZXIpO1xuXHR9XG5cblx0cHJpdmF0ZSBhc3luYyBwcmVwYXJlV2hlcmVPcmRlckFuZExpbWl0PFQ+KHBhcmFtczogSVF1ZXJ5QnVpbGRlclBhcmFtczxUPikge1xuXHRcdGlmIChwYXJhbXMud2hlcmVNYXApIHtcblx0XHRcdHBhcmFtcy53aGVyZUFuZE1hcCA9IHBhcmFtcy53aGVyZU1hcDtcblx0XHR9XG5cblx0XHRpZiAoXG5cdFx0XHRwYXJhbXMud2hlcmVBbmRNYXAgfHxcblx0XHRcdHBhcmFtcy53aGVyZU9yTWFwXG5cdFx0KSB7XG5cdFx0XHRwYXJhbXMud2hlcmVHcm91cCA9IGNyZWF0ZUV4cHJlc3Npb25Hcm91cChcblx0XHRcdFx0cGFyYW1zLndoZXJlQW5kTWFwID9cblx0XHRcdFx0XHRwYXJhbXMud2hlcmVBbmRNYXAgOlxuXHRcdFx0XHRcdHBhcmFtcy53aGVyZU9yTWFwLFxuXHRcdFx0XHRwYXJhbXMud2hlcmVBbmRNYXAgP1xuXHRcdFx0XHRcdEV4cHJlc3Npb25Hcm91cEpvaW5lci5BTkQgOlxuXHRcdFx0XHRcdEV4cHJlc3Npb25Hcm91cEpvaW5lci5PUlxuXHRcdFx0KTtcblx0XHR9IGVsc2UgaWYgKHBhcmFtcy53aGVyZVByaW1hcnlLZXlzKSB7XG5cdFx0XHRjb25zdCBleHByZXNzaW9uczogU3RyaW5nU3RhdGVtZW50W10gPSBwYXJhbXMud2hlcmVQcmltYXJ5S2V5cy5tYXAoXG5cdFx0XHRcdChwcmltYXJ5S2V5VmFsdWUpID0+IG5ldyBTdHJpbmdTdGF0ZW1lbnQoXG5cdFx0XHRcdFx0cGFyYW1zLnJlcG9zaXRvcnkucHJpbWFyeUtleSxcblx0XHRcdFx0XHQnJyArIHByaW1hcnlLZXlWYWx1ZVxuXHRcdFx0XHQpXG5cdFx0XHQpO1xuXG5cdFx0XHRwYXJhbXMud2hlcmVHcm91cCA9IG5ldyBPckV4cHJlc3Npb25Hcm91cChleHByZXNzaW9ucyk7XG5cdFx0fSBlbHNlIGlmIChwYXJhbXMud2hlcmVDbGF1c2UpIHtcblx0XHRcdHBhcmFtcy53aGVyZUdyb3VwID0gbmV3IFNpbmdsZUV4cHJlc3Npb24oXG5cdFx0XHRcdHBhcmFtcy53aGVyZUNsYXVzZVxuXHRcdFx0KTtcblx0XHR9XG5cblxuXHRcdGNvbnN0IHt3aGVyZUdyb3VwLCBvcmRlckJ5LCBsaW1pdH0gPSBwYXJhbXM7XG5cblx0XHRsZXQgcXVlcnkgPSAnJztcblxuXHRcdGNvbnN0IHdoZXJlQ2xhdXNlID1cblx0XHRcdHdoZXJlR3JvdXAgJiZcblx0XHRcdGF3YWl0IHRoaXMucHJlcGFyZUV4cHJlc3Npb25Hcm91cCh3aGVyZUdyb3VwKTtcblxuXHRcdGlmICh3aGVyZUNsYXVzZSkge1xuXHRcdFx0cXVlcnkgKz0gYFxuICAgICAgICAgICAgICAgIFdIRVJFICR7d2hlcmVDbGF1c2V9YDtcblx0XHR9XG5cblx0XHRpZiAob3JkZXJCeSkge1xuXHRcdFx0cXVlcnkgKz0gYFxuICAgICAgICAgICAgICAgIE9SREVSIEJZICR7b3JkZXJCeX1gO1xuXHRcdH1cblxuXHRcdGlmIChsaW1pdCkge1xuXHRcdFx0cXVlcnkgKz0gYFxuICAgICAgICAgICAgICAgIExJTUlUICR7bGltaXR9YDtcblx0XHR9XG5cblx0XHRyZXR1cm4gcXVlcnk7XG5cdH1cblxuXHRwcml2YXRlIGFzeW5jIHByZXBhcmVFeHByZXNzaW9uR3JvdXAoZ3JvdXA6IEV4cHJlc3Npb25Hcm91cCk6IFByb21pc2U8c3RyaW5nPiB7XG5cdFx0Y29uc3Qgc3RhdGVtZW50czogc3RyaW5nW10gPSBbXTtcblxuXHRcdGNvbnN0IHVzZUJyYWNrZXRzID0gZ3JvdXAuc3Vycm91bmRFeHByZXNzaW9uV2l0aEJyYWNrZXRzO1xuXG5cdFx0Zm9yIChjb25zdCBleHByZXNzaW9uIG9mIGdyb3VwLmV4cHJlc3Npb25zKSB7XG5cdFx0XHRjb25zdCB7bGVmdFNpZGUsIG9wZXJhdG9yLCByaWdodFNpZGV9ID0gZXhwcmVzc2lvbjtcblxuXHRcdFx0Y29uc3QgcmlnaHRTaWRlVmFsdWUgPVxuXHRcdFx0XHRyaWdodFNpZGUuaXNTdHJpbmcgP1xuXHRcdFx0XHRcdGAkeyBhd2FpdCB0aGlzLmVzY2FwZShyaWdodFNpZGUuZGF0YSBhcyBzdHJpbmcpIH1gIDpcblx0XHRcdFx0XHRyaWdodFNpZGUuZGF0YTtcblxuXHRcdFx0c3RhdGVtZW50cy5wdXNoKFxuXHRcdFx0XHRgJHtcblx0XHRcdFx0XHR1c2VCcmFja2V0cyA/ICcoJyA6ICcnXG5cdFx0XHRcdH0gXFxgJHtsZWZ0U2lkZX1cXGAgJHtvcGVyYXRvcn0gJHtyaWdodFNpZGVWYWx1ZX0gJHtcblx0XHRcdFx0XHR1c2VCcmFja2V0cyA/ICcpJyA6ICcnXG5cdFx0XHRcdH1gKTtcblx0XHR9XG5cblx0XHRyZXR1cm4gc3RhdGVtZW50cy5qb2luKGAgJHtncm91cC5qb2luZXJ9IGApO1xuXHR9XG5cblxuXHRwcml2YXRlIGFzeW5jIGVzY2FwZSh2YWx1ZTogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcblx0XHRjb25zdCBjb25uZWN0aW9uID0gYXdhaXQgdGhpcy5jb25uZWN0aW9uTWFuYWdlci5nZXRDb25uZWN0aW9uKCk7XG5cblx0XHRyZXR1cm4gY29ubmVjdGlvbi5lc2NhcGUodmFsdWUpO1xuXHR9XG5cblx0cHVibGljIGRlbGV0ZShcblx0XHRyZXBvc2l0b3J5OiBJTXlTUUxSZXBvc2l0b3J5LFxuXHRcdHByaW1hcnlLZXlWYWx1ZXM6IGFueVtdXG5cdCkge1xuXHRcdGlmIChwcmltYXJ5S2V5VmFsdWVzLmxlbmd0aCA8IDEpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignUGxlYXNlIHNwZWNpZnkgd2hpY2ggcmVjb3JkcyB0byBkZWxldGUhJyk7XG5cdFx0fVxuXG5cblx0XHRjb25zdCBxdWVyeSA9ICdERUxFVEUgRlJPTSAnICsgcmVwb3NpdG9yeS50YWJsZU5hbWUgK1xuXHRcdFx0XHRcdGNvbnN0cnVjdFdoZXJlV2l0aFByaW1hcnlLZXlzKHJlcG9zaXRvcnksIHByaW1hcnlLZXlWYWx1ZXMpICsgJzsnO1xuXG5cdFx0cmV0dXJuIG5ldyBNeVNRTFF1ZXJ5PElNeVNRTFF1ZXJ5UmVzdWx0PihxdWVyeSwgdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG5cdH1cblxuXHRyYXdRdWVyeTxUPihzcWw6IHN0cmluZywgYXJndW1lbnRzVG9QcmVwYXJlPzogYW55KTogTXlTUUxRdWVyeTxUPiB7XG5cdFx0cmV0dXJuIG5ldyBNeVNRTFF1ZXJ5PFQ+KHNxbCwgdGhpcy5jb25uZWN0aW9uTWFuYWdlciwgYXJndW1lbnRzVG9QcmVwYXJlKTtcblx0fVxufVxuXG5cbmZ1bmN0aW9uIGNvbnN0cnVjdFdoZXJlV2l0aFByaW1hcnlLZXlzKFxuXHRyZXBvc2l0b3J5OiBJTXlTUUxSZXBvc2l0b3J5LFxuXHRwcmltYXJ5S2V5VmFsdWVzOiBhbnlbXVxuKTogc3RyaW5nIHtcblx0Y29uc3QgY29uZGl0aW9uczogc3RyaW5nW10gPSBbXTtcblxuXHRmb3IgKGxldCBwcmltYXJ5S2V5VmFsdWUgb2YgcHJpbWFyeUtleVZhbHVlcykge1xuXHRcdGlmIChyZXBvc2l0b3J5LmlzUHJpbWFyeUtleUFTdHJpbmcpIHtcblx0XHRcdHByaW1hcnlLZXlWYWx1ZSA9ICdcXCcnICsgcHJpbWFyeUtleVZhbHVlICsgJ1xcJyc7XG5cdFx0fVxuXG5cdFx0Y29uc3QgY29uZGl0aW9uID0gcmVwb3NpdG9yeS5wcmltYXJ5S2V5ICsgJyA9ICcgKyBwcmltYXJ5S2V5VmFsdWU7XG5cdFx0Y29uZGl0aW9ucy5wdXNoKGNvbmRpdGlvbik7XG5cdH1cblxuXHRyZXR1cm4gJyBXSEVSRSAnICsgY29uZGl0aW9ucy5qb2luKCcgT1IgJyk7XG59XG4iXX0=