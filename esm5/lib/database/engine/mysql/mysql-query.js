/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-query.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { logWithTime } from 'earnbet-common';
/** @type {?} */
var logSqlQueries = process.env.LOG_SQL_QUERIES === 'true';
/**
 * @template T
 */
var /**
 * @template T
 */
MySQLQuery = /** @class */ (function () {
    function MySQLQuery(sql, connectionManager, argumentsToPrepare) {
        this.sql = sql;
        this.connectionManager = connectionManager;
        this.argumentsToPrepare = argumentsToPrepare;
    }
    /**
     * @return {?}
     */
    MySQLQuery.prototype.execute = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var connection;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // When execute is called directly,
                        // by default, it will use the connectionManager used to construct the query...
                        return [4 /*yield*/, this.connectionManager.getConnection()];
                    case 1:
                        connection = _a.sent();
                        return [2 /*return*/, executeQuery(this.sql, connection, this.argumentsToPrepare)];
                }
            });
        });
    };
    return MySQLQuery;
}());
/**
 * @template T
 */
export { MySQLQuery };
if (false) {
    /** @type {?} */
    MySQLQuery.prototype.sql;
    /**
     * @type {?}
     * @private
     */
    MySQLQuery.prototype.connectionManager;
    /** @type {?} */
    MySQLQuery.prototype.argumentsToPrepare;
}
/**
 * @template T
 * @param {?} sql
 * @param {?} connection
 * @param {?} argumentsToPrepare
 * @return {?}
 */
export function executeQuery(sql, connection, argumentsToPrepare) {
    return new Promise((/**
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    function (resolve, reject) {
        if (logSqlQueries) {
            logWithTime(sql, argumentsToPrepare);
        }
        connection.query(sql, argumentsToPrepare, (/**
         * @param {?} error
         * @param {?} result
         * @param {?} fields
         * @return {?}
         */
        function (error, result, fields) {
            if (error) {
                logWithTime('*** MySQL Error while executing Query: ', sql, argumentsToPrepare);
                console.error(error);
                reject(error);
            }
            else {
                resolve(result);
            }
        }));
    }));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcXVlcnkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9kYXRhYmFzZS9lbmdpbmUvbXlzcWwvbXlzcWwtcXVlcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBRUEsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLGdCQUFnQixDQUFDOztJQUtyQyxhQUFhLEdBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxLQUFLLE1BQU07Ozs7QUFHdkM7Ozs7SUFDQyxvQkFDVSxHQUFXLEVBQ1osaUJBQXlDLEVBQ3hDLGtCQUF3QjtRQUZ4QixRQUFHLEdBQUgsR0FBRyxDQUFRO1FBQ1osc0JBQWlCLEdBQWpCLGlCQUFpQixDQUF3QjtRQUN4Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQU07SUFFbEMsQ0FBQzs7OztJQUVLLDRCQUFPOzs7SUFBYjs7Ozs7Ozs7d0JBR29CLHFCQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsRUFBQTs7d0JBQXpELFVBQVUsR0FBRyxTQUE0Qzt3QkFFL0Qsc0JBQU8sWUFBWSxDQUFJLElBQUksQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxFQUFDOzs7O0tBQ3RFO0lBQ0YsaUJBQUM7QUFBRCxDQUFDLEFBZkQsSUFlQzs7Ozs7OztJQWJDLHlCQUFvQjs7Ozs7SUFDcEIsdUNBQWlEOztJQUNqRCx3Q0FBaUM7Ozs7Ozs7OztBQWNuQyxNQUFNLFVBQVUsWUFBWSxDQUMzQixHQUFXLEVBQ1gsVUFBc0IsRUFDdEIsa0JBQXVCO0lBRXZCLE9BQU8sSUFBSSxPQUFPOzs7OztJQUFLLFVBQUMsT0FBTyxFQUFFLE1BQU07UUFDdEMsSUFBSSxhQUFhLEVBQUU7WUFDbEIsV0FBVyxDQUFDLEdBQUcsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1NBQ3JDO1FBRUQsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsa0JBQWtCOzs7Ozs7UUFBRSxVQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTTtZQUMvRCxJQUFJLEtBQUssRUFBRTtnQkFDVixXQUFXLENBQUMseUNBQXlDLEVBQUUsR0FBRyxFQUFFLGtCQUFrQixDQUFDLENBQUM7Z0JBQ2hGLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBRXJCLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNkO2lCQUFNO2dCQUNOLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNoQjtRQUNGLENBQUMsRUFBQyxDQUFDO0lBQ0osQ0FBQyxFQUFDLENBQUM7QUFDSixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29ubmVjdGlvbiB9IGZyb20gJ215c3FsJztcblxuaW1wb3J0IHtsb2dXaXRoVGltZX0gZnJvbSAnZWFybmJldC1jb21tb24nO1xuXG5pbXBvcnQgeyBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyIH0gZnJvbSAnLi9teXNxbC1jb25uZWN0aW9uLW1hbmFnZXInO1xuXG5cbmNvbnN0IGxvZ1NxbFF1ZXJpZXMgPVxuXHRwcm9jZXNzLmVudi5MT0dfU1FMX1FVRVJJRVMgPT09ICd0cnVlJztcblxuXG5leHBvcnQgY2xhc3MgTXlTUUxRdWVyeTxUPiB7XG5cdGNvbnN0cnVjdG9yKFxuXHRcdHJlYWRvbmx5IHNxbDogc3RyaW5nLFxuXHRcdHByaXZhdGUgY29ubmVjdGlvbk1hbmFnZXI6IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIsXG5cdFx0cmVhZG9ubHkgYXJndW1lbnRzVG9QcmVwYXJlPzogYW55LFxuXHQpIHtcblx0fVxuXG5cdGFzeW5jIGV4ZWN1dGUoKTogUHJvbWlzZTxUPiB7XG5cdFx0Ly8gV2hlbiBleGVjdXRlIGlzIGNhbGxlZCBkaXJlY3RseSxcblx0XHQvLyBieSBkZWZhdWx0LCBpdCB3aWxsIHVzZSB0aGUgY29ubmVjdGlvbk1hbmFnZXIgdXNlZCB0byBjb25zdHJ1Y3QgdGhlIHF1ZXJ5Li4uXG5cdFx0Y29uc3QgY29ubmVjdGlvbiA9IGF3YWl0IHRoaXMuY29ubmVjdGlvbk1hbmFnZXIuZ2V0Q29ubmVjdGlvbigpO1xuXG5cdFx0cmV0dXJuIGV4ZWN1dGVRdWVyeTxUPih0aGlzLnNxbCwgY29ubmVjdGlvbiwgdGhpcy5hcmd1bWVudHNUb1ByZXBhcmUpO1xuXHR9XG59XG5cblxuZXhwb3J0IGZ1bmN0aW9uIGV4ZWN1dGVRdWVyeTxUPihcblx0c3FsOiBzdHJpbmcsXG5cdGNvbm5lY3Rpb246IENvbm5lY3Rpb24sXG5cdGFyZ3VtZW50c1RvUHJlcGFyZTogYW55LFxuKTogUHJvbWlzZTxUPiB7XG5cdHJldHVybiBuZXcgUHJvbWlzZTxUPiggKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdGlmIChsb2dTcWxRdWVyaWVzKSB7XG5cdFx0XHRsb2dXaXRoVGltZShzcWwsIGFyZ3VtZW50c1RvUHJlcGFyZSk7XG5cdFx0fVxuXG5cdFx0Y29ubmVjdGlvbi5xdWVyeShzcWwsIGFyZ3VtZW50c1RvUHJlcGFyZSwgKGVycm9yLCByZXN1bHQsIGZpZWxkcykgPT4ge1xuXHRcdFx0aWYgKGVycm9yKSB7XG5cdFx0XHRcdGxvZ1dpdGhUaW1lKCcqKiogTXlTUUwgRXJyb3Igd2hpbGUgZXhlY3V0aW5nIFF1ZXJ5OiAnLCBzcWwsIGFyZ3VtZW50c1RvUHJlcGFyZSk7XG5cdFx0XHRcdGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG5cdFx0XHRcdHJlamVjdChlcnJvcik7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRyZXNvbHZlKHJlc3VsdCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdH0pO1xufVxuIl19