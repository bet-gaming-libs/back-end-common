/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-repository.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * @template TypeForSelect
 */
var /**
 * @template TypeForSelect
 */
MySQLRepository = /** @class */ (function () {
    function MySQLRepository(db, _tableName, defaultFieldsToSelect, _primaryKey, isPrimaryKeyAString) {
        if (defaultFieldsToSelect === void 0) { defaultFieldsToSelect = []; }
        if (_primaryKey === void 0) { _primaryKey = 'id'; }
        if (isPrimaryKeyAString === void 0) { isPrimaryKeyAString = false; }
        this.db = db;
        this._tableName = _tableName;
        this.defaultFieldsToSelect = defaultFieldsToSelect;
        this._primaryKey = _primaryKey;
        this.isPrimaryKeyAString = isPrimaryKeyAString;
    }
    /**
     * @template T
     * @param {?} entity
     * @return {?}
     */
    MySQLRepository.prototype.insert = /**
     * @template T
     * @param {?} entity
     * @return {?}
     */
    function (entity) {
        return this.insertMany([entity]);
    };
    /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    MySQLRepository.prototype.insertMany = /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    function (entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildInsert(entities)];
                    case 1:
                        query = _a.sent();
                        return [4 /*yield*/, query.execute()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    MySQLRepository.prototype.buildInsert = /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    function (entities) {
        return this.db.builder.insert(this, entities);
    };
    /**
     * @param {?} primaryKeyValue
     * @param {?=} fields
     * @return {?}
     */
    MySQLRepository.prototype.selectOneByPrimaryKey = /**
     * @param {?} primaryKeyValue
     * @param {?=} fields
     * @return {?}
     */
    function (primaryKeyValue, fields) {
        if (fields === void 0) { fields = this.defaultFieldsToSelect; }
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var rows;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.selectManyByPrimaryKeys([primaryKeyValue], fields)];
                    case 1:
                        rows = _a.sent();
                        return [2 /*return*/, rows.length > 0 ?
                                rows[0] :
                                null];
                }
            });
        });
    };
    /**
     * @param {?} primaryKeyValues
     * @param {?=} fields
     * @return {?}
     */
    MySQLRepository.prototype.selectManyByPrimaryKeys = /**
     * @param {?} primaryKeyValues
     * @param {?=} fields
     * @return {?}
     */
    function (primaryKeyValues, fields) {
        if (fields === void 0) { fields = this.defaultFieldsToSelect; }
        return this.db.builder.selectByPrimaryKeys(this, fields, primaryKeyValues).execute();
    };
    /**
     * @param {?=} fields
     * @return {?}
     */
    MySQLRepository.prototype.selectAll = /**
     * @param {?=} fields
     * @return {?}
     */
    function (fields) {
        if (fields === void 0) { fields = this.defaultFieldsToSelect; }
        return this.db.builder.selectAll(this, fields).execute();
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.selectOne = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var rows;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.select(tslib_1.__assign({}, params, { limit: 1 }))];
                    case 1:
                        rows = _a.sent();
                        return [2 /*return*/, rows.length > 0 ?
                                rows[0] :
                                null];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.select = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildSelect(params)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, query.execute()];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.buildSelect = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var fields = params.fields ?
            params.fields :
            this.defaultFieldsToSelect;
        /** @type {?} */
        var newParams = tslib_1.__assign({}, params, { fields: fields, repository: this });
        return this.db.builder.select(newParams);
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.updateOne = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildUpdateOne(params)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, query.execute()];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.buildUpdateOne = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return this.buildUpdate(tslib_1.__assign({}, params, { limit: 1 }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.update = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildUpdate(params)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, query.execute()];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.buildUpdate = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var builderParams = tslib_1.__assign({}, params, { repository: this });
        return this.db.builder.update(builderParams);
    };
    /**
     * @param {?} primaryKeyValues
     * @return {?}
     */
    MySQLRepository.prototype.delete = /**
     * @param {?} primaryKeyValues
     * @return {?}
     */
    function (primaryKeyValues) {
        return this.db.builder.delete(this, primaryKeyValues).execute();
    };
    Object.defineProperty(MySQLRepository.prototype, "tableName", {
        get: /**
         * @return {?}
         */
        function () {
            return this._tableName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MySQLRepository.prototype, "primaryKey", {
        get: /**
         * @return {?}
         */
        function () {
            return this._primaryKey;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MySQLRepository.prototype, "primaryKeyFieldList", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return [this.primaryKey];
        },
        enumerable: true,
        configurable: true
    });
    return MySQLRepository;
}());
/**
 * @template TypeForSelect
 */
export { MySQLRepository };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    MySQLRepository.prototype.db;
    /**
     * @type {?}
     * @private
     */
    MySQLRepository.prototype._tableName;
    /**
     * @type {?}
     * @protected
     */
    MySQLRepository.prototype.defaultFieldsToSelect;
    /**
     * @type {?}
     * @private
     */
    MySQLRepository.prototype._primaryKey;
    /** @type {?} */
    MySQLRepository.prototype.isPrimaryKeyAString;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcmVwb3NpdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RhdGFiYXNlL2VuZ2luZS9teXNxbC9teXNxbC1yZXBvc2l0b3J5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQWtCQTs7OztJQUNDLHlCQUNXLEVBQWtCLEVBQ3BCLFVBQWtCLEVBQ2hCLHFCQUFvQyxFQUN0QyxXQUEwQixFQUN6QixtQkFBMkI7UUFGMUIsc0NBQUEsRUFBQSwwQkFBb0M7UUFDdEMsNEJBQUEsRUFBQSxrQkFBMEI7UUFDekIsb0NBQUEsRUFBQSwyQkFBMkI7UUFKMUIsT0FBRSxHQUFGLEVBQUUsQ0FBZ0I7UUFDcEIsZUFBVSxHQUFWLFVBQVUsQ0FBUTtRQUNoQiwwQkFBcUIsR0FBckIscUJBQXFCLENBQWU7UUFDdEMsZ0JBQVcsR0FBWCxXQUFXLENBQWU7UUFDekIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFRO0lBRXJDLENBQUM7Ozs7OztJQUVNLGdDQUFNOzs7OztJQUFiLFVBQWlCLE1BQVM7UUFDekIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUNsQyxDQUFDOzs7Ozs7SUFDWSxvQ0FBVTs7Ozs7SUFBdkIsVUFBMkIsUUFBYTs7Ozs7NEJBQ3pCLHFCQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEVBQUE7O3dCQUF4QyxLQUFLLEdBQUcsU0FBZ0M7d0JBQ3ZDLHFCQUFNLEtBQUssQ0FBQyxPQUFPLEVBQUUsRUFBQTs0QkFBNUIsc0JBQU8sU0FBcUIsRUFBQzs7OztLQUM3Qjs7Ozs7O0lBRU0scUNBQVc7Ozs7O0lBQWxCLFVBQXNCLFFBQWE7UUFDbEMsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUksSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ2xELENBQUM7Ozs7OztJQUdZLCtDQUFxQjs7Ozs7SUFBbEMsVUFDQyxlQUFnQyxFQUNoQyxNQUE2QztRQUE3Qyx1QkFBQSxFQUFBLFNBQW1CLElBQUksQ0FBQyxxQkFBcUI7Ozs7OzRCQUVoQyxxQkFBTSxJQUFJLENBQUMsdUJBQXVCLENBQzlDLENBQUMsZUFBZSxDQUFDLEVBQ2pCLE1BQU0sQ0FDTixFQUFBOzt3QkFISyxJQUFJLEdBQUcsU0FHWjt3QkFFRCxzQkFBTyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dDQUN0QixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQ0FDVCxJQUFJLEVBQUM7Ozs7S0FDUDs7Ozs7O0lBRU0saURBQXVCOzs7OztJQUE5QixVQUNDLGdCQUFtQyxFQUNuQyxNQUE2QztRQUE3Qyx1QkFBQSxFQUFBLFNBQW1CLElBQUksQ0FBQyxxQkFBcUI7UUFFN0MsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FDekMsSUFBSSxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsQ0FDOUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNiLENBQUM7Ozs7O0lBRU0sbUNBQVM7Ozs7SUFBaEIsVUFBaUIsTUFBNkM7UUFBN0MsdUJBQUEsRUFBQSxTQUFtQixJQUFJLENBQUMscUJBQXFCO1FBQzdELE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUMvQixJQUFJLEVBQ0osTUFBTSxDQUNOLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDYixDQUFDOzs7OztJQUdZLG1DQUFTOzs7O0lBQXRCLFVBQXVCLE1BQXlDOzs7Ozs0QkFDbEQscUJBQU0sSUFBSSxDQUFDLE1BQU0sc0JBQzFCLE1BQU0sSUFDVCxLQUFLLEVBQUUsQ0FBQyxJQUNQLEVBQUE7O3dCQUhJLElBQUksR0FBRyxTQUdYO3dCQUVGLHNCQUFPLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0NBQ3RCLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dDQUNULElBQUksRUFBQzs7OztLQUNQOzs7OztJQUVZLGdDQUFNOzs7O0lBQW5CLFVBQW9CLE1BQXlDOzs7Ozs0QkFDOUMscUJBQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQXRDLEtBQUssR0FBRyxTQUE4Qjt3QkFFNUMsc0JBQU8sS0FBSyxDQUFDLE9BQU8sRUFBRSxFQUFDOzs7O0tBQ3ZCOzs7OztJQUVNLHFDQUFXOzs7O0lBQWxCLFVBQW1CLE1BQXlDOztZQUNyRCxNQUFNLEdBQ1gsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2QsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2YsSUFBSSxDQUFDLHFCQUFxQjs7WUFFdEIsU0FBUyx3QkFDWCxNQUFNLElBQ1QsTUFBTSxRQUFBLEVBQ04sVUFBVSxFQUFFLElBQUksR0FDaEI7UUFFRCxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBZ0IsU0FBUyxDQUFDLENBQUM7SUFDekQsQ0FBQzs7Ozs7SUFHWSxtQ0FBUzs7OztJQUF0QixVQUF1QixNQUF5Qzs7Ozs7NEJBQ2pELHFCQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUF6QyxLQUFLLEdBQUcsU0FBaUM7d0JBRS9DLHNCQUFPLEtBQUssQ0FBQyxPQUFPLEVBQUUsRUFBQzs7OztLQUN2Qjs7Ozs7SUFDTSx3Q0FBYzs7OztJQUFyQixVQUFzQixNQUF5QztRQUM5RCxPQUFPLElBQUksQ0FBQyxXQUFXLHNCQUNuQixNQUFNLElBQ1QsS0FBSyxFQUFFLENBQUMsSUFDUCxDQUFDO0lBQ0osQ0FBQzs7Ozs7SUFFWSxnQ0FBTTs7OztJQUFuQixVQUFvQixNQUF5Qzs7Ozs7NEJBQzlDLHFCQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUF0QyxLQUFLLEdBQUcsU0FBOEI7d0JBQzVDLHNCQUFPLEtBQUssQ0FBQyxPQUFPLEVBQUUsRUFBQzs7OztLQUN2Qjs7Ozs7SUFDTSxxQ0FBVzs7OztJQUFsQixVQUFtQixNQUF5Qzs7WUFDckQsYUFBYSx3QkFDZixNQUFNLElBQ1QsVUFBVSxFQUFFLElBQUksR0FDaEI7UUFFRCxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUM5QyxDQUFDOzs7OztJQUdNLGdDQUFNOzs7O0lBQWIsVUFBYyxnQkFBbUM7UUFDaEQsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDakUsQ0FBQztJQUdELHNCQUFXLHNDQUFTOzs7O1FBQXBCO1lBQ0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ3hCLENBQUM7OztPQUFBO0lBRUQsc0JBQVcsdUNBQVU7Ozs7UUFBckI7WUFDQyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDekIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBWSxnREFBbUI7Ozs7O1FBQS9CO1lBQ0MsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMxQixDQUFDOzs7T0FBQTtJQUNGLHNCQUFDO0FBQUQsQ0FBQyxBQWpJRCxJQWlJQzs7Ozs7Ozs7OztJQS9IQyw2QkFBNEI7Ozs7O0lBQzVCLHFDQUEwQjs7Ozs7SUFDMUIsZ0RBQThDOzs7OztJQUM5QyxzQ0FBa0M7O0lBQ2xDLDhDQUFvQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG5cdElNeVNRTERhdGFiYXNlLFxuXHRJTXlTUUxSZXBvc2l0b3J5LFxuXHRQcmltYXJ5S2V5VmFsdWUsXG5cdElNeVNRTFF1ZXJ5UmVzdWx0LFxuXHRJU2VsZWN0UXVlcnlCdWlsZGVyUGFyYW1zLFxuXHRJU2VsZWN0UXVlcnlQYXJhbXMsXG5cdElVcGRhdGVRdWVyeVBhcmFtcyxcblx0SVVwZGF0ZVF1ZXJ5QnVpbGRlclBhcmFtcyxcbn0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7IE15U1FMUXVlcnkgfSBmcm9tICcuL215c3FsLXF1ZXJ5JztcblxuXG5cbmV4cG9ydCB0eXBlIFNlbGVjdE9uZUhhbmRsZXI8VD4gPSAocm93czogVFtdKSA9PiB2b2lkO1xuXG5cblxuZXhwb3J0IGNsYXNzIE15U1FMUmVwb3NpdG9yeTxUeXBlRm9yU2VsZWN0PiBpbXBsZW1lbnRzIElNeVNRTFJlcG9zaXRvcnkge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRwcm90ZWN0ZWQgZGI6IElNeVNRTERhdGFiYXNlLFxuXHRcdHByaXZhdGUgX3RhYmxlTmFtZTogc3RyaW5nLFxuXHRcdHByb3RlY3RlZCBkZWZhdWx0RmllbGRzVG9TZWxlY3Q6IHN0cmluZ1tdID0gW10sXG5cdFx0cHJpdmF0ZSBfcHJpbWFyeUtleTogc3RyaW5nID0gJ2lkJyxcblx0XHRyZWFkb25seSBpc1ByaW1hcnlLZXlBU3RyaW5nID0gZmFsc2Vcblx0KSB7XG5cdH1cblxuXHRwdWJsaWMgaW5zZXJ0PFQ+KGVudGl0eTogVCkge1xuXHRcdHJldHVybiB0aGlzLmluc2VydE1hbnkoW2VudGl0eV0pO1xuXHR9XG5cdHB1YmxpYyBhc3luYyBpbnNlcnRNYW55PFQ+KGVudGl0aWVzOiBUW10pIHtcblx0XHRjb25zdCBxdWVyeSA9IGF3YWl0IHRoaXMuYnVpbGRJbnNlcnQoZW50aXRpZXMpO1xuXHRcdHJldHVybiBhd2FpdCBxdWVyeS5leGVjdXRlKCk7XG5cdH1cblxuXHRwdWJsaWMgYnVpbGRJbnNlcnQ8VD4oZW50aXRpZXM6IFRbXSk6IFByb21pc2U8TXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4+IHtcblx0XHRyZXR1cm4gdGhpcy5kYi5idWlsZGVyLmluc2VydDxUPih0aGlzLCBlbnRpdGllcyk7XG5cdH1cblxuXG5cdHB1YmxpYyBhc3luYyBzZWxlY3RPbmVCeVByaW1hcnlLZXkoXG5cdFx0cHJpbWFyeUtleVZhbHVlOiBQcmltYXJ5S2V5VmFsdWUsXG5cdFx0ZmllbGRzOiBzdHJpbmdbXSA9IHRoaXMuZGVmYXVsdEZpZWxkc1RvU2VsZWN0XG5cdCk6IFByb21pc2U8VHlwZUZvclNlbGVjdD4ge1xuXHRcdGNvbnN0IHJvd3MgPSBhd2FpdCB0aGlzLnNlbGVjdE1hbnlCeVByaW1hcnlLZXlzKFxuXHRcdFx0W3ByaW1hcnlLZXlWYWx1ZV0sXG5cdFx0XHRmaWVsZHNcblx0XHQpO1xuXG5cdFx0cmV0dXJuIHJvd3MubGVuZ3RoID4gMCA/XG5cdFx0XHRcdHJvd3NbMF0gOlxuXHRcdFx0XHRudWxsO1xuXHR9XG5cblx0cHVibGljIHNlbGVjdE1hbnlCeVByaW1hcnlLZXlzKFxuXHRcdHByaW1hcnlLZXlWYWx1ZXM6IFByaW1hcnlLZXlWYWx1ZVtdLFxuXHRcdGZpZWxkczogc3RyaW5nW10gPSB0aGlzLmRlZmF1bHRGaWVsZHNUb1NlbGVjdFxuXHQpIHtcblx0XHRyZXR1cm4gdGhpcy5kYi5idWlsZGVyLnNlbGVjdEJ5UHJpbWFyeUtleXM8VHlwZUZvclNlbGVjdD4oXG5cdFx0XHR0aGlzLCBmaWVsZHMsIHByaW1hcnlLZXlWYWx1ZXNcblx0XHQpLmV4ZWN1dGUoKTtcblx0fVxuXG5cdHB1YmxpYyBzZWxlY3RBbGwoZmllbGRzOiBzdHJpbmdbXSA9IHRoaXMuZGVmYXVsdEZpZWxkc1RvU2VsZWN0KSB7XG5cdFx0cmV0dXJuIHRoaXMuZGIuYnVpbGRlci5zZWxlY3RBbGw8VHlwZUZvclNlbGVjdD4oXG5cdFx0XHR0aGlzLFxuXHRcdFx0ZmllbGRzXG5cdFx0KS5leGVjdXRlKCk7XG5cdH1cblxuXG5cdHB1YmxpYyBhc3luYyBzZWxlY3RPbmUocGFyYW1zOiBJU2VsZWN0UXVlcnlQYXJhbXM8VHlwZUZvclNlbGVjdD4pIHtcblx0XHRjb25zdCByb3dzID0gYXdhaXQgdGhpcy5zZWxlY3Qoe1xuXHRcdFx0Li4ucGFyYW1zLFxuXHRcdFx0bGltaXQ6IDFcblx0XHR9KTtcblxuXHRcdHJldHVybiByb3dzLmxlbmd0aCA+IDAgP1xuXHRcdFx0XHRyb3dzWzBdIDpcblx0XHRcdFx0bnVsbDtcblx0fVxuXG5cdHB1YmxpYyBhc3luYyBzZWxlY3QocGFyYW1zOiBJU2VsZWN0UXVlcnlQYXJhbXM8VHlwZUZvclNlbGVjdD4pIHtcblx0XHRjb25zdCBxdWVyeSA9IGF3YWl0IHRoaXMuYnVpbGRTZWxlY3QocGFyYW1zKTtcblxuXHRcdHJldHVybiBxdWVyeS5leGVjdXRlKCk7XG5cdH1cblxuXHRwdWJsaWMgYnVpbGRTZWxlY3QocGFyYW1zOiBJU2VsZWN0UXVlcnlQYXJhbXM8VHlwZUZvclNlbGVjdD4pIHtcblx0XHRjb25zdCBmaWVsZHMgPVxuXHRcdFx0cGFyYW1zLmZpZWxkcyA/XG5cdFx0XHRcdHBhcmFtcy5maWVsZHMgOlxuXHRcdFx0XHR0aGlzLmRlZmF1bHRGaWVsZHNUb1NlbGVjdDtcblxuXHRcdGNvbnN0IG5ld1BhcmFtczogSVNlbGVjdFF1ZXJ5QnVpbGRlclBhcmFtczxUeXBlRm9yU2VsZWN0PiA9IHtcblx0XHRcdC4uLnBhcmFtcyxcblx0XHRcdGZpZWxkcyxcblx0XHRcdHJlcG9zaXRvcnk6IHRoaXNcblx0XHR9O1xuXG5cdFx0cmV0dXJuIHRoaXMuZGIuYnVpbGRlci5zZWxlY3Q8VHlwZUZvclNlbGVjdD4obmV3UGFyYW1zKTtcblx0fVxuXG5cblx0cHVibGljIGFzeW5jIHVwZGF0ZU9uZShwYXJhbXM6IElVcGRhdGVRdWVyeVBhcmFtczxUeXBlRm9yU2VsZWN0Pikge1xuXHRcdGNvbnN0IHF1ZXJ5ID0gYXdhaXQgdGhpcy5idWlsZFVwZGF0ZU9uZShwYXJhbXMpO1xuXG5cdFx0cmV0dXJuIHF1ZXJ5LmV4ZWN1dGUoKTtcblx0fVxuXHRwdWJsaWMgYnVpbGRVcGRhdGVPbmUocGFyYW1zOiBJVXBkYXRlUXVlcnlQYXJhbXM8VHlwZUZvclNlbGVjdD4pIHtcblx0XHRyZXR1cm4gdGhpcy5idWlsZFVwZGF0ZSh7XG5cdFx0XHQuLi5wYXJhbXMsXG5cdFx0XHRsaW1pdDogMVxuXHRcdH0pO1xuXHR9XG5cblx0cHVibGljIGFzeW5jIHVwZGF0ZShwYXJhbXM6IElVcGRhdGVRdWVyeVBhcmFtczxUeXBlRm9yU2VsZWN0Pikge1xuXHRcdGNvbnN0IHF1ZXJ5ID0gYXdhaXQgdGhpcy5idWlsZFVwZGF0ZShwYXJhbXMpO1xuXHRcdHJldHVybiBxdWVyeS5leGVjdXRlKCk7XG5cdH1cblx0cHVibGljIGJ1aWxkVXBkYXRlKHBhcmFtczogSVVwZGF0ZVF1ZXJ5UGFyYW1zPFR5cGVGb3JTZWxlY3Q+KSB7XG5cdFx0Y29uc3QgYnVpbGRlclBhcmFtczogSVVwZGF0ZVF1ZXJ5QnVpbGRlclBhcmFtczxUeXBlRm9yU2VsZWN0PiA9IHtcblx0XHRcdC4uLnBhcmFtcyxcblx0XHRcdHJlcG9zaXRvcnk6IHRoaXNcblx0XHR9O1xuXG5cdFx0cmV0dXJuIHRoaXMuZGIuYnVpbGRlci51cGRhdGUoYnVpbGRlclBhcmFtcyk7XG5cdH1cblxuXG5cdHB1YmxpYyBkZWxldGUocHJpbWFyeUtleVZhbHVlczogUHJpbWFyeUtleVZhbHVlW10pIHtcblx0XHRyZXR1cm4gdGhpcy5kYi5idWlsZGVyLmRlbGV0ZSh0aGlzLCBwcmltYXJ5S2V5VmFsdWVzKS5leGVjdXRlKCk7XG5cdH1cblxuXG5cdHB1YmxpYyBnZXQgdGFibGVOYW1lKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX3RhYmxlTmFtZTtcblx0fVxuXG5cdHB1YmxpYyBnZXQgcHJpbWFyeUtleSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9wcmltYXJ5S2V5O1xuXHR9XG5cblx0cHJpdmF0ZSBnZXQgcHJpbWFyeUtleUZpZWxkTGlzdCgpOiBzdHJpbmdbXSB7XG5cdFx0cmV0dXJuIFt0aGlzLnByaW1hcnlLZXldO1xuXHR9XG59XG4iXX0=