/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-statements.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/** @enum {number} */
var MySQLValueType = {
    EXPRESSION: 0,
    STRING: 1,
};
export { MySQLValueType };
MySQLValueType[MySQLValueType.EXPRESSION] = 'EXPRESSION';
MySQLValueType[MySQLValueType.STRING] = 'STRING';
/** @enum {string} */
var ExpressionOperator = {
    EQUAL: "=",
    NOT_EQUAL: "!=",
    LESS_THAN: "<",
    LESS_THAN_OR_EQUAL: "<=",
    GREATER_THAN: ">",
    GREATER_THAN_OR_EQUAL: ">=",
    IS: "IS",
    IS_NOT: "IS NOT",
};
export { ExpressionOperator };
/** @enum {string} */
var ExpressionGroupJoiner = {
    AND: "AND",
    OR: "OR",
    COMMA: ",",
};
export { ExpressionGroupJoiner };
/**
 * @record
 */
export function IMySQLValue() { }
if (false) {
    /** @type {?} */
    IMySQLValue.prototype.data;
    /** @type {?} */
    IMySQLValue.prototype.isString;
}
var MySQLValue = /** @class */ (function () {
    function MySQLValue(data, type) {
        this.data = data;
        this.type = type;
        this.isString =
            type == MySQLValueType.STRING;
    }
    return MySQLValue;
}());
export { MySQLValue };
if (false) {
    /** @type {?} */
    MySQLValue.prototype.isString;
    /** @type {?} */
    MySQLValue.prototype.data;
    /** @type {?} */
    MySQLValue.prototype.type;
}
var MySQLNumberValue = /** @class */ (function (_super) {
    tslib_1.__extends(MySQLNumberValue, _super);
    function MySQLNumberValue(data) {
        return _super.call(this, data, MySQLValueType.EXPRESSION) || this;
    }
    return MySQLNumberValue;
}(MySQLValue));
var MySQLStringValue = /** @class */ (function (_super) {
    tslib_1.__extends(MySQLStringValue, _super);
    function MySQLStringValue(data) {
        return _super.call(this, data, MySQLValueType.STRING) || this;
    }
    return MySQLStringValue;
}(MySQLValue));
var MySQLStringExpressionValue = /** @class */ (function (_super) {
    tslib_1.__extends(MySQLStringExpressionValue, _super);
    function MySQLStringExpressionValue(data) {
        return _super.call(this, data, MySQLValueType.EXPRESSION) || this;
    }
    return MySQLStringExpressionValue;
}(MySQLValue));
/** @enum {string} */
var MySQLExpression = {
    NOW: "NOW()",
    NULL: "NULL",
};
export { MySQLExpression };
/** @type {?} */
var nowValue = new MySQLStringExpressionValue(MySQLExpression.NOW);
/** @type {?} */
var nullValue = new MySQLStringExpressionValue(MySQLExpression.NULL);
/*
class MySQLValue
{
    static booleanAsInt(bool:boolean):number
    {
        return bool ? 1 : 0;
    }
}
*/
/**
 * @abstract
 */
var /*
class MySQLValue
{
    static booleanAsInt(bool:boolean):number
    {
        return bool ? 1 : 0;
    }
}
*/
/**
 * @abstract
 */
Expression = /** @class */ (function () {
    function Expression(leftSide, operator, rightSide) {
        this.leftSide = leftSide;
        this.operator = operator;
        this.rightSide = rightSide;
    }
    return Expression;
}());
/*
class MySQLValue
{
    static booleanAsInt(bool:boolean):number
    {
        return bool ? 1 : 0;
    }
}
*/
/**
 * @abstract
 */
export { Expression };
if (false) {
    /** @type {?} */
    Expression.prototype.leftSide;
    /** @type {?} */
    Expression.prototype.operator;
    /** @type {?} */
    Expression.prototype.rightSide;
}
var ExpressionGroup = /** @class */ (function () {
    function ExpressionGroup(expressions, joiner, surroundExpressionWithBrackets) {
        if (surroundExpressionWithBrackets === void 0) { surroundExpressionWithBrackets = true; }
        this.expressions = expressions;
        this.joiner = joiner;
        this.surroundExpressionWithBrackets = surroundExpressionWithBrackets;
    }
    return ExpressionGroup;
}());
export { ExpressionGroup };
if (false) {
    /** @type {?} */
    ExpressionGroup.prototype.expressions;
    /** @type {?} */
    ExpressionGroup.prototype.joiner;
    /** @type {?} */
    ExpressionGroup.prototype.surroundExpressionWithBrackets;
}
/**
 * @param {?} map
 * @param {?} joiner
 * @return {?}
 */
export function createExpressionGroup(map, joiner) {
    /** @type {?} */
    var expressions = [];
    // should we use Object.keys()?
    for (var fieldName in map) {
        /** @type {?} */
        var fieldValue = map[fieldName];
        expressions.push(typeof fieldValue == 'string' ?
            new StringStatement(fieldName, (/** @type {?} */ (fieldValue))) :
            new NumberStatement(fieldName, fieldValue));
    }
    return new ExpressionGroup(expressions, joiner);
}
var SingleExpression = /** @class */ (function (_super) {
    tslib_1.__extends(SingleExpression, _super);
    function SingleExpression(expression) {
        return _super.call(this, [expression], undefined) || this;
    }
    return SingleExpression;
}(ExpressionGroup));
export { SingleExpression };
var AndExpressionGroup = /** @class */ (function (_super) {
    tslib_1.__extends(AndExpressionGroup, _super);
    function AndExpressionGroup(expressions) {
        return _super.call(this, expressions, ExpressionGroupJoiner.AND) || this;
    }
    return AndExpressionGroup;
}(ExpressionGroup));
export { AndExpressionGroup };
var OrExpressionGroup = /** @class */ (function (_super) {
    tslib_1.__extends(OrExpressionGroup, _super);
    function OrExpressionGroup(expressions) {
        return _super.call(this, expressions, ExpressionGroupJoiner.OR) || this;
    }
    return OrExpressionGroup;
}(ExpressionGroup));
export { OrExpressionGroup };
var UpdateStatements = /** @class */ (function (_super) {
    tslib_1.__extends(UpdateStatements, _super);
    function UpdateStatements(expressions) {
        return _super.call(this, expressions, ExpressionGroupJoiner.COMMA, false) || this;
    }
    return UpdateStatements;
}(ExpressionGroup));
export { UpdateStatements };
/**
 * @abstract
 */
var /**
 * @abstract
 */
FieldStatement = /** @class */ (function (_super) {
    tslib_1.__extends(FieldStatement, _super);
    function FieldStatement(fieldName, value, operator) {
        return _super.call(this, fieldName, operator, value) || this;
    }
    return FieldStatement;
}(Expression));
/**
 * @abstract
 */
export { FieldStatement };
var NumberStatement = /** @class */ (function (_super) {
    tslib_1.__extends(NumberStatement, _super);
    function NumberStatement(fieldName, data, operator) {
        if (operator === void 0) { operator = ExpressionOperator.EQUAL; }
        return _super.call(this, fieldName, new MySQLNumberValue(data), operator) || this;
    }
    return NumberStatement;
}(FieldStatement));
export { NumberStatement };
var StringStatement = /** @class */ (function (_super) {
    tslib_1.__extends(StringStatement, _super);
    function StringStatement(fieldName, data, operator) {
        if (operator === void 0) { operator = ExpressionOperator.EQUAL; }
        return _super.call(this, fieldName, new MySQLStringValue(data), operator) || this;
    }
    return StringStatement;
}(FieldStatement));
export { StringStatement };
var StringExpressionStatement = /** @class */ (function (_super) {
    tslib_1.__extends(StringExpressionStatement, _super);
    function StringExpressionStatement(fieldName, data, operator) {
        if (operator === void 0) { operator = ExpressionOperator.EQUAL; }
        return _super.call(this, fieldName, new MySQLStringExpressionValue(data), operator) || this;
    }
    return StringExpressionStatement;
}(FieldStatement));
export { StringExpressionStatement };
var NullUpdateStatement = /** @class */ (function (_super) {
    tslib_1.__extends(NullUpdateStatement, _super);
    function NullUpdateStatement(fieldName) {
        return _super.call(this, fieldName, nullValue, ExpressionOperator.EQUAL) || this;
    }
    return NullUpdateStatement;
}(FieldStatement));
export { NullUpdateStatement };
var NowUpdateStatement = /** @class */ (function (_super) {
    tslib_1.__extends(NowUpdateStatement, _super);
    function NowUpdateStatement(fieldName) {
        return _super.call(this, fieldName, nowValue, ExpressionOperator.EQUAL) || this;
    }
    return NowUpdateStatement;
}(FieldStatement));
export { NowUpdateStatement };
var FieldIsNullStatement = /** @class */ (function (_super) {
    tslib_1.__extends(FieldIsNullStatement, _super);
    function FieldIsNullStatement(fieldName) {
        return _super.call(this, fieldName, nullValue, ExpressionOperator.IS) || this;
    }
    return FieldIsNullStatement;
}(FieldStatement));
export { FieldIsNullStatement };
var FieldIsNotNullStatement = /** @class */ (function (_super) {
    tslib_1.__extends(FieldIsNotNullStatement, _super);
    function FieldIsNotNullStatement(fieldName) {
        return _super.call(this, fieldName, nullValue, ExpressionOperator.IS_NOT) || this;
    }
    return FieldIsNotNullStatement;
}(FieldStatement));
export { FieldIsNotNullStatement };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtc3RhdGVtZW50cy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RhdGFiYXNlL2VuZ2luZS9teXNxbC9teXNxbC1zdGF0ZW1lbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQSxJQUFZLGNBQWM7SUFDekIsVUFBVSxHQUFBO0lBQ1YsTUFBTSxHQUFBO0VBQ047Ozs7O0FBRUQsSUFBWSxrQkFBa0I7SUFDN0IsS0FBSyxLQUFzQjtJQUMzQixTQUFTLE1BQW1CO0lBRTVCLFNBQVMsS0FBa0I7SUFDM0Isa0JBQWtCLE1BQVU7SUFDNUIsWUFBWSxLQUFlO0lBQzNCLHFCQUFxQixNQUFPO0lBRTVCLEVBQUUsTUFBMEI7SUFDNUIsTUFBTSxVQUEwQjtFQUNoQzs7O0FBRUQsSUFBWSxxQkFBcUI7SUFDaEMsR0FBRyxPQUFVO0lBQ2IsRUFBRSxNQUFVO0lBQ1osS0FBSyxLQUFNO0VBQ1g7Ozs7O0FBZUQsaUNBR0M7OztJQUZBLDJCQUEwQjs7SUFDMUIsK0JBQWtCOztBQUtuQjtJQUdDLG9CQUNVLElBQXlCLEVBQ3pCLElBQW9CO1FBRHBCLFNBQUksR0FBSixJQUFJLENBQXFCO1FBQ3pCLFNBQUksR0FBSixJQUFJLENBQWdCO1FBRTdCLElBQUksQ0FBQyxRQUFRO1lBQ1osSUFBSSxJQUFJLGNBQWMsQ0FBQyxNQUFNLENBQUM7SUFDaEMsQ0FBQztJQUNGLGlCQUFDO0FBQUQsQ0FBQyxBQVZELElBVUM7Ozs7SUFUQSw4QkFBMkI7O0lBRzFCLDBCQUFrQzs7SUFDbEMsMEJBQTZCOztBQU8vQjtJQUErQiw0Q0FBVTtJQUN4QywwQkFBWSxJQUFZO2VBQ3ZCLGtCQUFNLElBQUksRUFBRSxjQUFjLENBQUMsVUFBVSxDQUFDO0lBQ3ZDLENBQUM7SUFDRix1QkFBQztBQUFELENBQUMsQUFKRCxDQUErQixVQUFVLEdBSXhDO0FBRUQ7SUFBK0IsNENBQVU7SUFDeEMsMEJBQVksSUFBWTtlQUN2QixrQkFBTSxJQUFJLEVBQUUsY0FBYyxDQUFDLE1BQU0sQ0FBQztJQUNuQyxDQUFDO0lBQ0YsdUJBQUM7QUFBRCxDQUFDLEFBSkQsQ0FBK0IsVUFBVSxHQUl4QztBQUVEO0lBQXlDLHNEQUFVO0lBQ2xELG9DQUFZLElBQVk7ZUFDdkIsa0JBQU0sSUFBSSxFQUFFLGNBQWMsQ0FBQyxVQUFVLENBQUM7SUFDdkMsQ0FBQztJQUNGLGlDQUFDO0FBQUQsQ0FBQyxBQUpELENBQXlDLFVBQVUsR0FJbEQ7O0FBR0QsSUFBWSxlQUFlO0lBQzFCLEdBQUcsU0FBVTtJQUNiLElBQUksUUFBUztFQUNiOzs7SUFFSyxRQUFRLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDOztJQUM5RCxTQUFTLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDOzs7Ozs7Ozs7Ozs7O0FBZXRFOzs7Ozs7Ozs7Ozs7O0lBQ0Msb0JBQ1UsUUFBZ0IsRUFDaEIsUUFBZ0IsRUFDaEIsU0FBcUI7UUFGckIsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUNoQixhQUFRLEdBQVIsUUFBUSxDQUFRO1FBQ2hCLGNBQVMsR0FBVCxTQUFTLENBQVk7SUFFL0IsQ0FBQztJQUNGLGlCQUFDO0FBQUQsQ0FBQyxBQVBELElBT0M7Ozs7Ozs7Ozs7Ozs7Ozs7SUFMQyw4QkFBeUI7O0lBQ3pCLDhCQUF5Qjs7SUFDekIsK0JBQThCOztBQU1oQztJQUNDLHlCQUNVLFdBQXlCLEVBQ3pCLE1BQTZCLEVBQzdCLDhCQUE4QztRQUE5QywrQ0FBQSxFQUFBLHFDQUE4QztRQUY5QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYztRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUF1QjtRQUM3QixtQ0FBOEIsR0FBOUIsOEJBQThCLENBQWdCO0lBRXhELENBQUM7SUFDRixzQkFBQztBQUFELENBQUMsQUFQRCxJQU9DOzs7O0lBTEMsc0NBQWtDOztJQUNsQyxpQ0FBc0M7O0lBQ3RDLHlEQUF1RDs7Ozs7OztBQU16RCxNQUFNLFVBQVUscUJBQXFCLENBQ3BDLEdBQVEsRUFBRSxNQUE2Qjs7UUFFakMsV0FBVyxHQUFpQixFQUFFO0lBRXBDLCtCQUErQjtJQUMvQixLQUFLLElBQU0sU0FBUyxJQUFJLEdBQUcsRUFBRTs7WUFDdEIsVUFBVSxHQUFHLEdBQUcsQ0FBQyxTQUFTLENBQUM7UUFFakMsV0FBVyxDQUFDLElBQUksQ0FDZixPQUFPLFVBQVUsSUFBSSxRQUFRLENBQUMsQ0FBQztZQUM5QixJQUFJLGVBQWUsQ0FDbEIsU0FBUyxFQUNULG1CQUFBLFVBQVUsRUFBVSxDQUNwQixDQUFDLENBQUM7WUFDSCxJQUFJLGVBQWUsQ0FDbEIsU0FBUyxFQUNULFVBQVUsQ0FDVixDQUNGLENBQUM7S0FDRjtJQUVELE9BQU8sSUFBSSxlQUFlLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQ2pELENBQUM7QUFHRDtJQUFzQyw0Q0FBZTtJQUNwRCwwQkFBWSxVQUFzQjtlQUNqQyxrQkFBTSxDQUFDLFVBQVUsQ0FBQyxFQUFFLFNBQVMsQ0FBQztJQUMvQixDQUFDO0lBQ0YsdUJBQUM7QUFBRCxDQUFDLEFBSkQsQ0FBc0MsZUFBZSxHQUlwRDs7QUFDRDtJQUF3Qyw4Q0FBZTtJQUN0RCw0QkFBWSxXQUF5QjtlQUNwQyxrQkFBTSxXQUFXLEVBQUUscUJBQXFCLENBQUMsR0FBRyxDQUFDO0lBQzlDLENBQUM7SUFDRix5QkFBQztBQUFELENBQUMsQUFKRCxDQUF3QyxlQUFlLEdBSXREOztBQUNEO0lBQXVDLDZDQUFlO0lBQ3JELDJCQUFZLFdBQXlCO2VBQ3BDLGtCQUFNLFdBQVcsRUFBRSxxQkFBcUIsQ0FBQyxFQUFFLENBQUM7SUFDN0MsQ0FBQztJQUNGLHdCQUFDO0FBQUQsQ0FBQyxBQUpELENBQXVDLGVBQWUsR0FJckQ7O0FBRUQ7SUFBc0MsNENBQWU7SUFDcEQsMEJBQVksV0FBeUI7ZUFDcEMsa0JBQU0sV0FBVyxFQUFFLHFCQUFxQixDQUFDLEtBQUssRUFBRSxLQUFLLENBQUM7SUFDdkQsQ0FBQztJQUNGLHVCQUFDO0FBQUQsQ0FBQyxBQUpELENBQXNDLGVBQWUsR0FJcEQ7Ozs7O0FBSUQ7Ozs7SUFBNkMsMENBQVU7SUFDdEQsd0JBQ0MsU0FBaUIsRUFDakIsS0FBaUIsRUFDakIsUUFBNEI7ZUFFNUIsa0JBQU0sU0FBUyxFQUFFLFFBQVEsRUFBRSxLQUFLLENBQUM7SUFDbEMsQ0FBQztJQUNGLHFCQUFDO0FBQUQsQ0FBQyxBQVJELENBQTZDLFVBQVUsR0FRdEQ7Ozs7O0FBR0Q7SUFBcUMsMkNBQWM7SUFDbEQseUJBQ0MsU0FBaUIsRUFDakIsSUFBWSxFQUNaLFFBQXVEO1FBQXZELHlCQUFBLEVBQUEsV0FBK0Isa0JBQWtCLENBQUMsS0FBSztlQUV2RCxrQkFDQyxTQUFTLEVBQ1QsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFDMUIsUUFBUSxDQUNSO0lBQ0YsQ0FBQztJQUNGLHNCQUFDO0FBQUQsQ0FBQyxBQVpELENBQXFDLGNBQWMsR0FZbEQ7O0FBRUQ7SUFBcUMsMkNBQWM7SUFDbEQseUJBQ0MsU0FBaUIsRUFDakIsSUFBWSxFQUNaLFFBQXVEO1FBQXZELHlCQUFBLEVBQUEsV0FBK0Isa0JBQWtCLENBQUMsS0FBSztlQUV2RCxrQkFDQyxTQUFTLEVBQ1QsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFDMUIsUUFBUSxDQUNSO0lBQ0YsQ0FBQztJQUNGLHNCQUFDO0FBQUQsQ0FBQyxBQVpELENBQXFDLGNBQWMsR0FZbEQ7O0FBRUQ7SUFBK0MscURBQWM7SUFDNUQsbUNBQ0MsU0FBaUIsRUFDakIsSUFBWSxFQUNaLFFBQXVEO1FBQXZELHlCQUFBLEVBQUEsV0FBK0Isa0JBQWtCLENBQUMsS0FBSztlQUV2RCxrQkFDQyxTQUFTLEVBQ1QsSUFBSSwwQkFBMEIsQ0FBQyxJQUFJLENBQUMsRUFDcEMsUUFBUSxDQUNSO0lBQ0YsQ0FBQztJQUNGLGdDQUFDO0FBQUQsQ0FBQyxBQVpELENBQStDLGNBQWMsR0FZNUQ7O0FBR0Q7SUFBeUMsK0NBQWM7SUFDdEQsNkJBQVksU0FBaUI7ZUFDNUIsa0JBQU0sU0FBUyxFQUFFLFNBQVMsRUFBRSxrQkFBa0IsQ0FBQyxLQUFLLENBQUM7SUFDdEQsQ0FBQztJQUNGLDBCQUFDO0FBQUQsQ0FBQyxBQUpELENBQXlDLGNBQWMsR0FJdEQ7O0FBRUQ7SUFBd0MsOENBQWM7SUFDckQsNEJBQVksU0FBaUI7ZUFDNUIsa0JBQU0sU0FBUyxFQUFFLFFBQVEsRUFBRSxrQkFBa0IsQ0FBQyxLQUFLLENBQUM7SUFDckQsQ0FBQztJQUNGLHlCQUFDO0FBQUQsQ0FBQyxBQUpELENBQXdDLGNBQWMsR0FJckQ7O0FBR0Q7SUFBMEMsZ0RBQWM7SUFDdkQsOEJBQVksU0FBaUI7ZUFDNUIsa0JBQU0sU0FBUyxFQUFFLFNBQVMsRUFBRSxrQkFBa0IsQ0FBQyxFQUFFLENBQUM7SUFDbkQsQ0FBQztJQUNGLDJCQUFDO0FBQUQsQ0FBQyxBQUpELENBQTBDLGNBQWMsR0FJdkQ7O0FBRUQ7SUFBNkMsbURBQWM7SUFDMUQsaUNBQVksU0FBaUI7ZUFDNUIsa0JBQU0sU0FBUyxFQUFFLFNBQVMsRUFBRSxrQkFBa0IsQ0FBQyxNQUFNLENBQUM7SUFDdkQsQ0FBQztJQUNGLDhCQUFDO0FBQUQsQ0FBQyxBQUpELENBQTZDLGNBQWMsR0FJMUQiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBNeVNRTFZhbHVlVHlwZSB7XG5cdEVYUFJFU1NJT04sXG5cdFNUUklOR1xufVxuXG5leHBvcnQgZW51bSBFeHByZXNzaW9uT3BlcmF0b3Ige1xuXHRFUVVBTCAgICAgICAgICAgICAgICAgPSAnPScsXG5cdE5PVF9FUVVBTCAgICAgICAgICAgICA9ICchPScsXG5cblx0TEVTU19USEFOICAgICAgICAgICAgID0gJzwnLFxuXHRMRVNTX1RIQU5fT1JfRVFVQUwgICAgPSAnPD0nLFxuXHRHUkVBVEVSX1RIQU4gICAgICAgICAgPSAnPicsXG5cdEdSRUFURVJfVEhBTl9PUl9FUVVBTCA9ICc+PScsXG5cblx0SVMgICAgICAgICAgICAgICAgICAgID0gJ0lTJyxcblx0SVNfTk9UICAgICAgICAgICAgICAgID0gJ0lTIE5PVCcsXG59XG5cbmV4cG9ydCBlbnVtIEV4cHJlc3Npb25Hcm91cEpvaW5lciB7XG5cdEFORCAgID0gJ0FORCcsXG5cdE9SICAgID0gJ09SJyxcblx0Q09NTUEgPSAnLCdcbn1cblxuXG5cbmV4cG9ydCB0eXBlIEV4cHJlc3Npb25WYWx1ZVR5cGUgPSBzdHJpbmd8bnVtYmVyO1xuXG5cblxuLypcbmludGVyZmFjZSBJTXlTUUxWYWx1ZTxUIGV4dGVuZHMgRXhwcmVzc2lvblZhbHVlVHlwZT5cbntcbiAgICBkYXRhOlQsXG4gICAgLy90eXBlOk15U1FMVmFsdWVUeXBlXG59XG4qL1xuZXhwb3J0IGludGVyZmFjZSBJTXlTUUxWYWx1ZSB7XG5cdGRhdGE6IEV4cHJlc3Npb25WYWx1ZVR5cGU7XG5cdGlzU3RyaW5nOiBib29sZWFuO1xufVxuXG5cblxuZXhwb3J0IGNsYXNzIE15U1FMVmFsdWUgaW1wbGVtZW50cyBJTXlTUUxWYWx1ZSB7XG5cdHJlYWRvbmx5IGlzU3RyaW5nOiBib29sZWFuO1xuXG5cdGNvbnN0cnVjdG9yKFxuXHRcdHJlYWRvbmx5IGRhdGE6IEV4cHJlc3Npb25WYWx1ZVR5cGUsXG5cdFx0cmVhZG9ubHkgdHlwZTogTXlTUUxWYWx1ZVR5cGVcblx0KSB7XG5cdFx0dGhpcy5pc1N0cmluZyA9XG5cdFx0XHR0eXBlID09IE15U1FMVmFsdWVUeXBlLlNUUklORztcblx0fVxufVxuXG5jbGFzcyBNeVNRTE51bWJlclZhbHVlIGV4dGVuZHMgTXlTUUxWYWx1ZSB7XG5cdGNvbnN0cnVjdG9yKGRhdGE6IG51bWJlcikge1xuXHRcdHN1cGVyKGRhdGEsIE15U1FMVmFsdWVUeXBlLkVYUFJFU1NJT04pO1xuXHR9XG59XG5cbmNsYXNzIE15U1FMU3RyaW5nVmFsdWUgZXh0ZW5kcyBNeVNRTFZhbHVlIHtcblx0Y29uc3RydWN0b3IoZGF0YTogc3RyaW5nKSB7XG5cdFx0c3VwZXIoZGF0YSwgTXlTUUxWYWx1ZVR5cGUuU1RSSU5HKTtcblx0fVxufVxuXG5jbGFzcyBNeVNRTFN0cmluZ0V4cHJlc3Npb25WYWx1ZSBleHRlbmRzIE15U1FMVmFsdWUge1xuXHRjb25zdHJ1Y3RvcihkYXRhOiBzdHJpbmcpIHtcblx0XHRzdXBlcihkYXRhLCBNeVNRTFZhbHVlVHlwZS5FWFBSRVNTSU9OKTtcblx0fVxufVxuXG5cbmV4cG9ydCBlbnVtIE15U1FMRXhwcmVzc2lvbiB7XG5cdE5PVyA9ICdOT1coKScsXG5cdE5VTEwgPSAnTlVMTCcsXG59XG5cbmNvbnN0IG5vd1ZhbHVlID0gbmV3IE15U1FMU3RyaW5nRXhwcmVzc2lvblZhbHVlKE15U1FMRXhwcmVzc2lvbi5OT1cpO1xuY29uc3QgbnVsbFZhbHVlID0gbmV3IE15U1FMU3RyaW5nRXhwcmVzc2lvblZhbHVlKE15U1FMRXhwcmVzc2lvbi5OVUxMKTtcblxuXG4vKlxuY2xhc3MgTXlTUUxWYWx1ZVxue1xuICAgIHN0YXRpYyBib29sZWFuQXNJbnQoYm9vbDpib29sZWFuKTpudW1iZXJcbiAgICB7XG4gICAgICAgIHJldHVybiBib29sID8gMSA6IDA7XG4gICAgfVxufVxuKi9cblxuXG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBFeHByZXNzaW9uIHtcblx0Y29uc3RydWN0b3IoXG5cdFx0cmVhZG9ubHkgbGVmdFNpZGU6IHN0cmluZyxcblx0XHRyZWFkb25seSBvcGVyYXRvcjogc3RyaW5nLFxuXHRcdHJlYWRvbmx5IHJpZ2h0U2lkZTogTXlTUUxWYWx1ZVxuXHQpIHtcblx0fVxufVxuXG5cbmV4cG9ydCBjbGFzcyBFeHByZXNzaW9uR3JvdXAge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRyZWFkb25seSBleHByZXNzaW9uczogRXhwcmVzc2lvbltdLFxuXHRcdHJlYWRvbmx5IGpvaW5lcjogRXhwcmVzc2lvbkdyb3VwSm9pbmVyLFxuXHRcdHJlYWRvbmx5IHN1cnJvdW5kRXhwcmVzc2lvbldpdGhCcmFja2V0czogYm9vbGVhbiA9IHRydWVcblx0KSB7XG5cdH1cbn1cblxuXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlRXhwcmVzc2lvbkdyb3VwKFxuXHRtYXA6IGFueSwgam9pbmVyOiBFeHByZXNzaW9uR3JvdXBKb2luZXJcbikge1xuXHRjb25zdCBleHByZXNzaW9uczogRXhwcmVzc2lvbltdID0gW107XG5cblx0Ly8gc2hvdWxkIHdlIHVzZSBPYmplY3Qua2V5cygpP1xuXHRmb3IgKGNvbnN0IGZpZWxkTmFtZSBpbiBtYXApIHtcblx0XHRjb25zdCBmaWVsZFZhbHVlID0gbWFwW2ZpZWxkTmFtZV07XG5cblx0XHRleHByZXNzaW9ucy5wdXNoKFxuXHRcdFx0dHlwZW9mIGZpZWxkVmFsdWUgPT0gJ3N0cmluZycgP1xuXHRcdFx0XHRuZXcgU3RyaW5nU3RhdGVtZW50KFxuXHRcdFx0XHRcdGZpZWxkTmFtZSxcblx0XHRcdFx0XHRmaWVsZFZhbHVlIGFzIHN0cmluZ1xuXHRcdFx0XHQpIDpcblx0XHRcdFx0bmV3IE51bWJlclN0YXRlbWVudChcblx0XHRcdFx0XHRmaWVsZE5hbWUsXG5cdFx0XHRcdFx0ZmllbGRWYWx1ZVxuXHRcdFx0XHQpXG5cdFx0KTtcblx0fVxuXG5cdHJldHVybiBuZXcgRXhwcmVzc2lvbkdyb3VwKGV4cHJlc3Npb25zLCBqb2luZXIpO1xufVxuXG5cbmV4cG9ydCBjbGFzcyBTaW5nbGVFeHByZXNzaW9uIGV4dGVuZHMgRXhwcmVzc2lvbkdyb3VwIHtcblx0Y29uc3RydWN0b3IoZXhwcmVzc2lvbjogRXhwcmVzc2lvbikge1xuXHRcdHN1cGVyKFtleHByZXNzaW9uXSwgdW5kZWZpbmVkKTtcblx0fVxufVxuZXhwb3J0IGNsYXNzIEFuZEV4cHJlc3Npb25Hcm91cCBleHRlbmRzIEV4cHJlc3Npb25Hcm91cCB7XG5cdGNvbnN0cnVjdG9yKGV4cHJlc3Npb25zOiBFeHByZXNzaW9uW10pIHtcblx0XHRzdXBlcihleHByZXNzaW9ucywgRXhwcmVzc2lvbkdyb3VwSm9pbmVyLkFORCk7XG5cdH1cbn1cbmV4cG9ydCBjbGFzcyBPckV4cHJlc3Npb25Hcm91cCBleHRlbmRzIEV4cHJlc3Npb25Hcm91cCB7XG5cdGNvbnN0cnVjdG9yKGV4cHJlc3Npb25zOiBFeHByZXNzaW9uW10pIHtcblx0XHRzdXBlcihleHByZXNzaW9ucywgRXhwcmVzc2lvbkdyb3VwSm9pbmVyLk9SKTtcblx0fVxufVxuXG5leHBvcnQgY2xhc3MgVXBkYXRlU3RhdGVtZW50cyBleHRlbmRzIEV4cHJlc3Npb25Hcm91cCB7XG5cdGNvbnN0cnVjdG9yKGV4cHJlc3Npb25zOiBFeHByZXNzaW9uW10pIHtcblx0XHRzdXBlcihleHByZXNzaW9ucywgRXhwcmVzc2lvbkdyb3VwSm9pbmVyLkNPTU1BLCBmYWxzZSk7XG5cdH1cbn1cblxuXG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBGaWVsZFN0YXRlbWVudCBleHRlbmRzIEV4cHJlc3Npb24ge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRmaWVsZE5hbWU6IHN0cmluZyxcblx0XHR2YWx1ZTogTXlTUUxWYWx1ZSxcblx0XHRvcGVyYXRvcjogRXhwcmVzc2lvbk9wZXJhdG9yXG5cdCkge1xuXHRcdHN1cGVyKGZpZWxkTmFtZSwgb3BlcmF0b3IsIHZhbHVlKTtcblx0fVxufVxuXG5cbmV4cG9ydCBjbGFzcyBOdW1iZXJTdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudCB7XG5cdGNvbnN0cnVjdG9yKFxuXHRcdGZpZWxkTmFtZTogc3RyaW5nLFxuXHRcdGRhdGE6IG51bWJlcixcblx0XHRvcGVyYXRvcjogRXhwcmVzc2lvbk9wZXJhdG9yID0gRXhwcmVzc2lvbk9wZXJhdG9yLkVRVUFMXG5cdCkge1xuXHRcdHN1cGVyKFxuXHRcdFx0ZmllbGROYW1lLFxuXHRcdFx0bmV3IE15U1FMTnVtYmVyVmFsdWUoZGF0YSksXG5cdFx0XHRvcGVyYXRvclxuXHRcdCk7XG5cdH1cbn1cblxuZXhwb3J0IGNsYXNzIFN0cmluZ1N0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50IHtcblx0Y29uc3RydWN0b3IoXG5cdFx0ZmllbGROYW1lOiBzdHJpbmcsXG5cdFx0ZGF0YTogc3RyaW5nLFxuXHRcdG9wZXJhdG9yOiBFeHByZXNzaW9uT3BlcmF0b3IgPSBFeHByZXNzaW9uT3BlcmF0b3IuRVFVQUxcblx0KSB7XG5cdFx0c3VwZXIoXG5cdFx0XHRmaWVsZE5hbWUsXG5cdFx0XHRuZXcgTXlTUUxTdHJpbmdWYWx1ZShkYXRhKSxcblx0XHRcdG9wZXJhdG9yXG5cdFx0KTtcblx0fVxufVxuXG5leHBvcnQgY2xhc3MgU3RyaW5nRXhwcmVzc2lvblN0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50IHtcblx0Y29uc3RydWN0b3IoXG5cdFx0ZmllbGROYW1lOiBzdHJpbmcsXG5cdFx0ZGF0YTogc3RyaW5nLFxuXHRcdG9wZXJhdG9yOiBFeHByZXNzaW9uT3BlcmF0b3IgPSBFeHByZXNzaW9uT3BlcmF0b3IuRVFVQUxcblx0KSB7XG5cdFx0c3VwZXIoXG5cdFx0XHRmaWVsZE5hbWUsXG5cdFx0XHRuZXcgTXlTUUxTdHJpbmdFeHByZXNzaW9uVmFsdWUoZGF0YSksXG5cdFx0XHRvcGVyYXRvclxuXHRcdCk7XG5cdH1cbn1cblxuXG5leHBvcnQgY2xhc3MgTnVsbFVwZGF0ZVN0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50IHtcblx0Y29uc3RydWN0b3IoZmllbGROYW1lOiBzdHJpbmcpIHtcblx0XHRzdXBlcihmaWVsZE5hbWUsIG51bGxWYWx1ZSwgRXhwcmVzc2lvbk9wZXJhdG9yLkVRVUFMKTtcblx0fVxufVxuXG5leHBvcnQgY2xhc3MgTm93VXBkYXRlU3RhdGVtZW50IGV4dGVuZHMgRmllbGRTdGF0ZW1lbnQge1xuXHRjb25zdHJ1Y3RvcihmaWVsZE5hbWU6IHN0cmluZykge1xuXHRcdHN1cGVyKGZpZWxkTmFtZSwgbm93VmFsdWUsIEV4cHJlc3Npb25PcGVyYXRvci5FUVVBTCk7XG5cdH1cbn1cblxuXG5leHBvcnQgY2xhc3MgRmllbGRJc051bGxTdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudCB7XG5cdGNvbnN0cnVjdG9yKGZpZWxkTmFtZTogc3RyaW5nKSB7XG5cdFx0c3VwZXIoZmllbGROYW1lLCBudWxsVmFsdWUsIEV4cHJlc3Npb25PcGVyYXRvci5JUyk7XG5cdH1cbn1cblxuZXhwb3J0IGNsYXNzIEZpZWxkSXNOb3ROdWxsU3RhdGVtZW50IGV4dGVuZHMgRmllbGRTdGF0ZW1lbnQge1xuXHRjb25zdHJ1Y3RvcihmaWVsZE5hbWU6IHN0cmluZykge1xuXHRcdHN1cGVyKGZpZWxkTmFtZSwgbnVsbFZhbHVlLCBFeHByZXNzaW9uT3BlcmF0b3IuSVNfTk9UKTtcblx0fVxufVxuIl19