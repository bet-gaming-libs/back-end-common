/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-transaction-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { logWithTime } from 'earnbet-common';
import { MySQLConnectionManager } from './mysql-connection-manager';
import { executeQuery } from './mysql-query';
var MySQLTransactionManager = /** @class */ (function () {
    function MySQLTransactionManager(config) {
        var _this = this;
        this.queue = [];
        this.isExecutingTransaction = false;
        /**
         * we need to manage a queue of transactions
         * to be executed in order
         * once the transaction either is successful or fails
         * then move on to the next
         */
        this.performNextTransaction = (/**
         * @return {?}
         */
        function () {
            if (_this.isExecutingTransaction ||
                _this.queue.length < 1) {
                return;
            }
            logWithTime('perform next transaction!');
            _this.isExecutingTransaction = true;
            // remove first element from queue and return it
            /** @type {?} */
            var executor = _this.queue.shift();
            executor.execute();
        });
        this.onTransactionCompleted = (/**
         * @return {?}
         */
        function () {
            logWithTime('onTransactionCompleted');
            _this.isExecutingTransaction = false;
            _this.performNextTransaction();
        });
        this.connectionManager = new MySQLConnectionManager(config);
    }
    /**
     * @param {?} queries
     * @return {?}
     */
    MySQLTransactionManager.prototype.performTransaction = /**
     * @param {?} queries
     * @return {?}
     */
    function (queries) {
        return this.performPreparedTransaction(queries.map((/**
         * @param {?} sql
         * @return {?}
         */
        function (sql) { return ({ sql: sql, argumentsToPrepare: undefined }); })));
    };
    /**
     * @param {?} data
     * @return {?}
     */
    MySQLTransactionManager.prototype.performPreparedTransaction = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        /*
                    we need to promise the result of each query executed
                    so that they can be inspected by consumer for Ids, etc
                    */
        /** @type {?} */
        var executor = new MySQLTransactionExecuter(data, this.performNextTransaction, this.onTransactionCompleted, this.connectionManager);
        // add executor to queue
        this.queue.push(executor);
        return new Promise(executor.init);
    };
    return MySQLTransactionManager;
}());
export { MySQLTransactionManager };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.queue;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.isExecutingTransaction;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.connectionManager;
    /**
     * we need to manage a queue of transactions
     * to be executed in order
     * once the transaction either is successful or fails
     * then move on to the next
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.performNextTransaction;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.onTransactionCompleted;
}
var MySQLTransactionExecuter = /** @class */ (function () {
    function MySQLTransactionExecuter(queries, onInitHandler, onCompleteHandler, connectionManager) {
        var _this = this;
        this.queries = queries;
        this.onInitHandler = onInitHandler;
        this.onCompleteHandler = onCompleteHandler;
        this.connectionManager = connectionManager;
        this.init = (/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            _this.resolve = resolve;
            _this.reject = reject;
            _this.onInitHandler();
        });
    }
    /**
     * @return {?}
     */
    MySQLTransactionExecuter.prototype.execute = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        // only need to get connection once?
                        _a = this;
                        return [4 /*yield*/, this.connectionManager.getConnection()];
                    case 1:
                        // only need to get connection once?
                        _a.connection = _b.sent();
                        this.connection.beginTransaction((/**
                         * @return {?}
                         */
                        function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var results, _a, _b, _c, sql, argumentsToPrepare, result, e_1_1, e_2;
                            var e_1, _d;
                            return tslib_1.__generator(this, function (_e) {
                                switch (_e.label) {
                                    case 0:
                                        _e.trys.push([0, 10, , 12]);
                                        results = [];
                                        _e.label = 1;
                                    case 1:
                                        _e.trys.push([1, 6, 7, 8]);
                                        _a = tslib_1.__values(this.queries), _b = _a.next();
                                        _e.label = 2;
                                    case 2:
                                        if (!!_b.done) return [3 /*break*/, 5];
                                        _c = _b.value, sql = _c.sql, argumentsToPrepare = _c.argumentsToPrepare;
                                        return [4 /*yield*/, executeQuery(sql, this.connection, argumentsToPrepare)];
                                    case 3:
                                        result = _e.sent();
                                        results.push(result);
                                        _e.label = 4;
                                    case 4:
                                        _b = _a.next();
                                        return [3 /*break*/, 2];
                                    case 5: return [3 /*break*/, 8];
                                    case 6:
                                        e_1_1 = _e.sent();
                                        e_1 = { error: e_1_1 };
                                        return [3 /*break*/, 8];
                                    case 7:
                                        try {
                                            if (_b && !_b.done && (_d = _a.return)) _d.call(_a);
                                        }
                                        finally { if (e_1) throw e_1.error; }
                                        return [7 /*endfinally*/];
                                    case 8: return [4 /*yield*/, this.commit()];
                                    case 9:
                                        _e.sent();
                                        this.resolve(results);
                                        return [3 /*break*/, 12];
                                    case 10:
                                        e_2 = _e.sent();
                                        return [4 /*yield*/, this.rollback()];
                                    case 11:
                                        _e.sent();
                                        this.reject(e_2);
                                        return [3 /*break*/, 12];
                                    case 12:
                                        this.onCompleteHandler();
                                        return [2 /*return*/];
                                }
                            });
                        }); }));
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @private
     * @return {?}
     */
    MySQLTransactionExecuter.prototype.commit = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            _this.connection.commit((/**
             * @param {?} error
             * @return {?}
             */
            function (error) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                return tslib_1.__generator(this, function (_a) {
                    if (error) {
                        logWithTime('ERROR in committing queries', this.queries);
                        return [2 /*return*/, reject(error)];
                    }
                    resolve();
                    return [2 /*return*/];
                });
            }); }));
        }));
    };
    /**
     * @private
     * @return {?}
     */
    MySQLTransactionExecuter.prototype.rollback = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        function (resolve) {
            _this.connection.rollback(resolve);
        }));
    };
    return MySQLTransactionExecuter;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.resolve;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.reject;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.connection;
    /** @type {?} */
    MySQLTransactionExecuter.prototype.init;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.queries;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.onInitHandler;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.onCompleteHandler;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.connectionManager;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtdHJhbnNhY3Rpb24tbWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RhdGFiYXNlL2VuZ2luZS9teXNxbC9teXNxbC10cmFuc2FjdGlvbi1tYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUVBLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUczQyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRzdDO0lBTUMsaUNBQVksTUFBb0I7UUFBaEMsaUJBRUM7UUFQTyxVQUFLLEdBQStCLEVBQUUsQ0FBQztRQUN2QywyQkFBc0IsR0FBRyxLQUFLLENBQUM7Ozs7Ozs7UUF5Qy9CLDJCQUFzQjs7O1FBQUc7WUFDaEMsSUFDQyxLQUFJLENBQUMsc0JBQXNCO2dCQUMzQixLQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQ3BCO2dCQUNELE9BQU87YUFDUDtZQUVELFdBQVcsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1lBRXpDLEtBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7OztnQkFHN0IsUUFBUSxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFO1lBRW5DLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNwQixDQUFDLEVBQUE7UUFFTywyQkFBc0I7OztRQUFHO1lBQ2hDLFdBQVcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBRXRDLEtBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7WUFFcEMsS0FBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDL0IsQ0FBQyxFQUFBO1FBNURBLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzdELENBQUM7Ozs7O0lBRUQsb0RBQWtCOzs7O0lBQWxCLFVBQW1CLE9BQWlCO1FBRW5DLE9BQU8sSUFBSSxDQUFDLDBCQUEwQixDQUNyQyxPQUFPLENBQUMsR0FBRzs7OztRQUNWLFVBQUEsR0FBRyxJQUFJLE9BQUEsQ0FBQyxFQUFDLEdBQUcsS0FBQSxFQUFFLGtCQUFrQixFQUFFLFNBQVMsRUFBQyxDQUFDLEVBQXRDLENBQXNDLEVBQzdDLENBQ0QsQ0FBQztJQUNILENBQUM7Ozs7O0lBRUQsNERBQTBCOzs7O0lBQTFCLFVBQTJCLElBQWtCOzs7Ozs7WUFLdEMsUUFBUSxHQUFHLElBQUksd0JBQXdCLENBQzVDLElBQUksRUFDSixJQUFJLENBQUMsc0JBQXNCLEVBQzNCLElBQUksQ0FBQyxzQkFBc0IsRUFDM0IsSUFBSSxDQUFDLGlCQUFpQixDQUN0QjtRQUVELHdCQUF3QjtRQUN4QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUUxQixPQUFPLElBQUksT0FBTyxDQUFzQixRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7SUFFeEQsQ0FBQztJQWdDRiw4QkFBQztBQUFELENBQUMsQUFwRUQsSUFvRUM7Ozs7Ozs7SUFuRUEsd0NBQStDOzs7OztJQUMvQyx5REFBdUM7Ozs7O0lBRXZDLG9EQUEyRDs7Ozs7Ozs7O0lBdUMzRCx5REFnQkM7Ozs7O0lBRUQseURBTUM7O0FBS0Y7SUFLQyxrQ0FDUyxPQUFxQixFQUNyQixhQUF1QixFQUN2QixpQkFBMkIsRUFDM0IsaUJBQXlDO1FBSmxELGlCQU1DO1FBTFEsWUFBTyxHQUFQLE9BQU8sQ0FBYztRQUNyQixrQkFBYSxHQUFiLGFBQWEsQ0FBVTtRQUN2QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQVU7UUFDM0Isc0JBQWlCLEdBQWpCLGlCQUFpQixDQUF3QjtRQUlsRCxTQUFJOzs7OztRQUFHLFVBQUMsT0FBd0IsRUFBRSxNQUFnQjtZQUNqRCxLQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztZQUN2QixLQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztZQUVyQixLQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDdEIsQ0FBQyxFQUFBO0lBUEQsQ0FBQzs7OztJQVNLLDBDQUFPOzs7SUFBYjs7Ozs7Ozt3QkFDQyxvQ0FBb0M7d0JBQ3BDLEtBQUEsSUFBSSxDQUFBO3dCQUFjLHFCQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsRUFBQTs7d0JBRDlELG9DQUFvQzt3QkFDcEMsR0FBSyxVQUFVLEdBQUcsU0FBNEMsQ0FBQzt3QkFFL0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0I7Ozt3QkFBQzs7Ozs7Ozt3Q0FFekIsT0FBTyxHQUF3QixFQUFFOzs7O3dDQUVDLEtBQUEsaUJBQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQTs7Ozt3Q0FBekMsYUFBeUIsRUFBeEIsR0FBRyxTQUFBLEVBQUUsa0JBQWtCLHdCQUFBO3dDQUNuQixxQkFBTSxZQUFZLENBQ2hDLEdBQUcsRUFDSCxJQUFJLENBQUMsVUFBVSxFQUNmLGtCQUFrQixDQUNsQixFQUFBOzt3Q0FKSyxNQUFNLEdBQUcsU0FJZDt3Q0FFRCxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7OzRDQUd0QixxQkFBTSxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUE7O3dDQUFuQixTQUFtQixDQUFDO3dDQUVwQixJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDOzs7O3dDQUV0QixxQkFBTSxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUE7O3dDQUFyQixTQUFxQixDQUFDO3dDQUV0QixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUMsQ0FBQyxDQUFDOzs7d0NBR2hCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDOzs7OzZCQUN6QixFQUFDLENBQUM7Ozs7O0tBQ0g7Ozs7O0lBRU8seUNBQU07Ozs7SUFBZDtRQUFBLGlCQVlDO1FBWEEsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUNsQyxLQUFJLENBQUMsVUFBVSxDQUFDLE1BQU07Ozs7WUFBQyxVQUFPLEtBQUs7O29CQUNsQyxJQUFJLEtBQUssRUFBRTt3QkFDVixXQUFXLENBQUMsNkJBQTZCLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3dCQUV6RCxzQkFBTyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUM7cUJBQ3JCO29CQUVELE9BQU8sRUFBRSxDQUFDOzs7aUJBQ1YsRUFBQyxDQUFDO1FBQ0osQ0FBQyxFQUFDLENBQUM7SUFDSixDQUFDOzs7OztJQUVPLDJDQUFROzs7O0lBQWhCO1FBQUEsaUJBSUM7UUFIQSxPQUFPLElBQUksT0FBTzs7OztRQUFDLFVBQUMsT0FBTztZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNuQyxDQUFDLEVBQUMsQ0FBQztJQUNKLENBQUM7SUFDRiwrQkFBQztBQUFELENBQUMsQUF0RUQsSUFzRUM7Ozs7OztJQXJFQSwyQ0FBaUM7Ozs7O0lBQ2pDLDBDQUF5Qjs7Ozs7SUFDekIsOENBQStCOztJQVUvQix3Q0FLQzs7Ozs7SUFaQSwyQ0FBNkI7Ozs7O0lBQzdCLGlEQUErQjs7Ozs7SUFDL0IscURBQW1DOzs7OztJQUNuQyxxREFBaUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb25uZWN0aW9uIH0gZnJvbSAnbXlzcWwnO1xuXG5pbXBvcnQge2xvZ1dpdGhUaW1lfSBmcm9tICdlYXJuYmV0LWNvbW1vbic7XG5cbmltcG9ydCB7IElNeVNRTENvbmZpZywgSU15U1FMUXVlcnlSZXN1bHQsIElRdWVyeURhdGEgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgTXlTUUxDb25uZWN0aW9uTWFuYWdlciB9IGZyb20gJy4vbXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyJztcbmltcG9ydCB7IGV4ZWN1dGVRdWVyeSB9IGZyb20gJy4vbXlzcWwtcXVlcnknO1xuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTFRyYW5zYWN0aW9uTWFuYWdlciB7XG5cdHByaXZhdGUgcXVldWU6IE15U1FMVHJhbnNhY3Rpb25FeGVjdXRlcltdID0gW107XG5cdHByaXZhdGUgaXNFeGVjdXRpbmdUcmFuc2FjdGlvbiA9IGZhbHNlO1xuXG5cdHByaXZhdGUgcmVhZG9ubHkgY29ubmVjdGlvbk1hbmFnZXI6IE15U1FMQ29ubmVjdGlvbk1hbmFnZXI7XG5cblx0Y29uc3RydWN0b3IoY29uZmlnOiBJTXlTUUxDb25maWcpIHtcblx0XHR0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyID0gbmV3IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIoY29uZmlnKTtcblx0fVxuXG5cdHBlcmZvcm1UcmFuc2FjdGlvbihxdWVyaWVzOiBzdHJpbmdbXSkge1xuXG5cdFx0cmV0dXJuIHRoaXMucGVyZm9ybVByZXBhcmVkVHJhbnNhY3Rpb24oXG5cdFx0XHRxdWVyaWVzLm1hcChcblx0XHRcdFx0c3FsID0+ICh7c3FsLCBhcmd1bWVudHNUb1ByZXBhcmU6IHVuZGVmaW5lZH0pXG5cdFx0XHQpXG5cdFx0KTtcblx0fVxuXG5cdHBlcmZvcm1QcmVwYXJlZFRyYW5zYWN0aW9uKGRhdGE6IElRdWVyeURhdGFbXSkge1xuXHRcdFx0LypcbiAgICAgICAgICAgIHdlIG5lZWQgdG8gcHJvbWlzZSB0aGUgcmVzdWx0IG9mIGVhY2ggcXVlcnkgZXhlY3V0ZWRcbiAgICAgICAgICAgIHNvIHRoYXQgdGhleSBjYW4gYmUgaW5zcGVjdGVkIGJ5IGNvbnN1bWVyIGZvciBJZHMsIGV0Y1xuICAgICAgICAgICAgKi9cblx0XHRjb25zdCBleGVjdXRvciA9IG5ldyBNeVNRTFRyYW5zYWN0aW9uRXhlY3V0ZXIoXG5cdFx0XHRkYXRhLFxuXHRcdFx0dGhpcy5wZXJmb3JtTmV4dFRyYW5zYWN0aW9uLFxuXHRcdFx0dGhpcy5vblRyYW5zYWN0aW9uQ29tcGxldGVkLFxuXHRcdFx0dGhpcy5jb25uZWN0aW9uTWFuYWdlclxuXHRcdCk7XG5cblx0XHQvLyBhZGQgZXhlY3V0b3IgdG8gcXVldWVcblx0XHR0aGlzLnF1ZXVlLnB1c2goZXhlY3V0b3IpO1xuXG5cdFx0cmV0dXJuIG5ldyBQcm9taXNlPElNeVNRTFF1ZXJ5UmVzdWx0W10+KGV4ZWN1dG9yLmluaXQpO1xuXG5cdH1cblxuXHQvKioqIHdlIG5lZWQgdG8gbWFuYWdlIGEgcXVldWUgb2YgdHJhbnNhY3Rpb25zXG4gICAgICogdG8gYmUgZXhlY3V0ZWQgaW4gb3JkZXJcbiAgICAgKiBvbmNlIHRoZSB0cmFuc2FjdGlvbiBlaXRoZXIgaXMgc3VjY2Vzc2Z1bCBvciBmYWlsc1xuICAgICAqIHRoZW4gbW92ZSBvbiB0byB0aGUgbmV4dFxuICAgICAqL1xuXHRwcml2YXRlIHBlcmZvcm1OZXh0VHJhbnNhY3Rpb24gPSAoKSA9PiB7XG5cdFx0aWYgKFxuXHRcdFx0dGhpcy5pc0V4ZWN1dGluZ1RyYW5zYWN0aW9uIHx8XG5cdFx0XHR0aGlzLnF1ZXVlLmxlbmd0aCA8IDFcblx0XHQpIHtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cblx0XHRsb2dXaXRoVGltZSgncGVyZm9ybSBuZXh0IHRyYW5zYWN0aW9uIScpO1xuXG5cdFx0dGhpcy5pc0V4ZWN1dGluZ1RyYW5zYWN0aW9uID0gdHJ1ZTtcblxuXHRcdC8vIHJlbW92ZSBmaXJzdCBlbGVtZW50IGZyb20gcXVldWUgYW5kIHJldHVybiBpdFxuXHRcdGNvbnN0IGV4ZWN1dG9yID0gdGhpcy5xdWV1ZS5zaGlmdCgpO1xuXG5cdFx0ZXhlY3V0b3IuZXhlY3V0ZSgpO1xuXHR9XG5cblx0cHJpdmF0ZSBvblRyYW5zYWN0aW9uQ29tcGxldGVkID0gKCkgPT4ge1xuXHRcdGxvZ1dpdGhUaW1lKCdvblRyYW5zYWN0aW9uQ29tcGxldGVkJyk7XG5cblx0XHR0aGlzLmlzRXhlY3V0aW5nVHJhbnNhY3Rpb24gPSBmYWxzZTtcblxuXHRcdHRoaXMucGVyZm9ybU5leHRUcmFuc2FjdGlvbigpO1xuXHR9XG59XG5cbnR5cGUgUmVzb2x2ZUZ1bmN0aW9uID0gKHJlc3VsdHM6IElNeVNRTFF1ZXJ5UmVzdWx0W10pID0+IHZvaWQ7XG5cbmNsYXNzIE15U1FMVHJhbnNhY3Rpb25FeGVjdXRlciB7XG5cdHByaXZhdGUgcmVzb2x2ZTogUmVzb2x2ZUZ1bmN0aW9uO1xuXHRwcml2YXRlIHJlamVjdDogRnVuY3Rpb247XG5cdHByaXZhdGUgY29ubmVjdGlvbjogQ29ubmVjdGlvbjtcblxuXHRjb25zdHJ1Y3Rvcihcblx0XHRwcml2YXRlIHF1ZXJpZXM6IElRdWVyeURhdGFbXSxcblx0XHRwcml2YXRlIG9uSW5pdEhhbmRsZXI6IEZ1bmN0aW9uLFxuXHRcdHByaXZhdGUgb25Db21wbGV0ZUhhbmRsZXI6IEZ1bmN0aW9uLFxuXHRcdHByaXZhdGUgY29ubmVjdGlvbk1hbmFnZXI6IE15U1FMQ29ubmVjdGlvbk1hbmFnZXJcblx0KSB7XG5cdH1cblxuXHRpbml0ID0gKHJlc29sdmU6IFJlc29sdmVGdW5jdGlvbiwgcmVqZWN0OiBGdW5jdGlvbikgPT4ge1xuXHRcdHRoaXMucmVzb2x2ZSA9IHJlc29sdmU7XG5cdFx0dGhpcy5yZWplY3QgPSByZWplY3Q7XG5cblx0XHR0aGlzLm9uSW5pdEhhbmRsZXIoKTtcblx0fVxuXG5cdGFzeW5jIGV4ZWN1dGUoKSB7XG5cdFx0Ly8gb25seSBuZWVkIHRvIGdldCBjb25uZWN0aW9uIG9uY2U/XG5cdFx0dGhpcy5jb25uZWN0aW9uID0gYXdhaXQgdGhpcy5jb25uZWN0aW9uTWFuYWdlci5nZXRDb25uZWN0aW9uKCk7XG5cblx0XHR0aGlzLmNvbm5lY3Rpb24uYmVnaW5UcmFuc2FjdGlvbihhc3luYyAoKSA9PiB7XG5cdFx0XHR0cnkge1xuXHRcdFx0XHRjb25zdCByZXN1bHRzOiBJTXlTUUxRdWVyeVJlc3VsdFtdID0gW107XG5cblx0XHRcdFx0Zm9yIChjb25zdCB7c3FsLCBhcmd1bWVudHNUb1ByZXBhcmV9IG9mIHRoaXMucXVlcmllcykge1xuXHRcdFx0XHRcdGNvbnN0IHJlc3VsdCA9IGF3YWl0IGV4ZWN1dGVRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4oXG5cdFx0XHRcdFx0XHRzcWwsXG5cdFx0XHRcdFx0XHR0aGlzLmNvbm5lY3Rpb24sXG5cdFx0XHRcdFx0XHRhcmd1bWVudHNUb1ByZXBhcmVcblx0XHRcdFx0XHQpO1xuXG5cdFx0XHRcdFx0cmVzdWx0cy5wdXNoKHJlc3VsdCk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRhd2FpdCB0aGlzLmNvbW1pdCgpO1xuXG5cdFx0XHRcdHRoaXMucmVzb2x2ZShyZXN1bHRzKTtcblx0XHRcdH0gY2F0Y2ggKGUpIHtcblx0XHRcdFx0YXdhaXQgdGhpcy5yb2xsYmFjaygpO1xuXG5cdFx0XHRcdHRoaXMucmVqZWN0KGUpO1xuXHRcdFx0fVxuXG5cdFx0XHR0aGlzLm9uQ29tcGxldGVIYW5kbGVyKCk7XG5cdFx0fSk7XG5cdH1cblxuXHRwcml2YXRlIGNvbW1pdCgpIHtcblx0XHRyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdFx0dGhpcy5jb25uZWN0aW9uLmNvbW1pdChhc3luYyAoZXJyb3IpID0+IHtcblx0XHRcdFx0aWYgKGVycm9yKSB7XG5cdFx0XHRcdFx0bG9nV2l0aFRpbWUoJ0VSUk9SIGluIGNvbW1pdHRpbmcgcXVlcmllcycsIHRoaXMucXVlcmllcyk7XG5cblx0XHRcdFx0XHRyZXR1cm4gcmVqZWN0KGVycm9yKTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdHJlc29sdmUoKTtcblx0XHRcdH0pO1xuXHRcdH0pO1xuXHR9XG5cblx0cHJpdmF0ZSByb2xsYmFjaygpIHtcblx0XHRyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcblx0XHRcdHRoaXMuY29ubmVjdGlvbi5yb2xsYmFjayhyZXNvbHZlKTtcblx0XHR9KTtcblx0fVxufVxuIl19