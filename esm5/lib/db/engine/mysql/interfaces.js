/**
 * @fileoverview added by tsickle
 * Generated from: lib/db/engine/mysql/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IMySQLConfig() { }
if (false) {
    /** @type {?} */
    IMySQLConfig.prototype.host;
    /** @type {?|undefined} */
    IMySQLConfig.prototype.port;
    /** @type {?} */
    IMySQLConfig.prototype.user;
    /** @type {?} */
    IMySQLConfig.prototype.password;
    /** @type {?} */
    IMySQLConfig.prototype.database;
    /** @type {?} */
    IMySQLConfig.prototype.charset;
}
/**
 * @record
 */
export function IMySQLRepository() { }
if (false) {
    /** @type {?} */
    IMySQLRepository.prototype.tableName;
    /** @type {?} */
    IMySQLRepository.prototype.primaryKey;
    /** @type {?} */
    IMySQLRepository.prototype.isPrimaryKeyAString;
}
/**
 * @record
 */
export function IMySQLQueryResult() { }
if (false) {
    /** @type {?} */
    IMySQLQueryResult.prototype.fieldCount;
    /** @type {?} */
    IMySQLQueryResult.prototype.affectedRows;
    /** @type {?} */
    IMySQLQueryResult.prototype.insertId;
    /** @type {?} */
    IMySQLQueryResult.prototype.serverStatus;
    /** @type {?} */
    IMySQLQueryResult.prototype.warningCount;
    /** @type {?} */
    IMySQLQueryResult.prototype.message;
    /** @type {?} */
    IMySQLQueryResult.prototype.changedRows;
}
/**
 * @record
 */
export function IMySQLDatabase() { }
if (false) {
    /** @type {?} */
    IMySQLDatabase.prototype.builder;
    /** @type {?} */
    IMySQLDatabase.prototype.transactionManager;
}
/**
 * @record
 * @template T
 */
export function IQueryParams() { }
if (false) {
    /** @type {?|undefined} */
    IQueryParams.prototype.whereMap;
    /** @type {?|undefined} */
    IQueryParams.prototype.whereAndMap;
    /** @type {?|undefined} */
    IQueryParams.prototype.whereOrMap;
    /** @type {?|undefined} */
    IQueryParams.prototype.whereClause;
    /** @type {?|undefined} */
    IQueryParams.prototype.wherePrimaryKeys;
    /** @type {?|undefined} */
    IQueryParams.prototype.whereGroup;
    /** @type {?|undefined} */
    IQueryParams.prototype.orderBy;
    /** @type {?|undefined} */
    IQueryParams.prototype.limit;
}
/**
 * @record
 * @template T
 */
export function ISelectQueryParams() { }
if (false) {
    /** @type {?|undefined} */
    ISelectQueryParams.prototype.fields;
}
/**
 * @record
 * @template T
 */
export function IQueryBuilderParams() { }
if (false) {
    /** @type {?} */
    IQueryBuilderParams.prototype.repository;
}
/**
 * @record
 * @template T
 */
export function ISelectQueryBuilderParams() { }
if (false) {
    /** @type {?} */
    ISelectQueryBuilderParams.prototype.fields;
}
/**
 * @record
 * @template T
 */
export function IUpdateQueryParams() { }
if (false) {
    /** @type {?|undefined} */
    IUpdateQueryParams.prototype.singleStatement;
    /** @type {?|undefined} */
    IUpdateQueryParams.prototype.statements;
}
/**
 * @record
 * @template T
 */
export function IUpdateQueryBuilderParams() { }
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL2VuZ2luZS9teXNxbC9pbnRlcmZhY2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBS0Esa0NBUUM7OztJQU5HLDRCQUFZOztJQUNaLDRCQUFhOztJQUNiLDRCQUFZOztJQUNaLGdDQUFnQjs7SUFDaEIsZ0NBQWdCOztJQUNoQiwrQkFBZTs7Ozs7QUFtQm5CLHNDQU1DOzs7SUFKRyxxQ0FBaUI7O0lBQ2pCLHNDQUFrQjs7SUFFbEIsK0NBQTRCOzs7OztBQUloQyx1Q0FTQzs7O0lBUEcsdUNBQWtCOztJQUNsQix5Q0FBb0I7O0lBQ3BCLHFDQUFnQjs7SUFDaEIseUNBQW9COztJQUNwQix5Q0FBb0I7O0lBQ3BCLG9DQUFlOztJQUNmLHdDQUFtQjs7Ozs7QUFpQnZCLG9DQUlDOzs7SUFGRyxpQ0FBbUM7O0lBQ25DLDRDQUFvRDs7Ozs7O0FBSXhELGtDQWFDOzs7SUFYRyxnQ0FBcUI7O0lBQ3JCLG1DQUF3Qjs7SUFDeEIsa0NBQXVCOztJQUV2QixtQ0FBd0I7O0lBQ3hCLHdDQUFvQzs7SUFFcEMsa0NBQTRCOztJQUU1QiwrQkFBZ0I7O0lBQ2hCLDZCQUFxQjs7Ozs7O0FBR3pCLHdDQUdDOzs7SUFERyxvQ0FBaUI7Ozs7OztBQUdyQix5Q0FHQzs7O0lBREcseUNBQTRCOzs7Ozs7QUFHaEMsK0NBSUM7OztJQURHLDJDQUFnQjs7Ozs7O0FBR3BCLHdDQUlDOzs7SUFGRyw2Q0FBNEI7O0lBQzVCLHdDQUF5Qjs7Ozs7O0FBRzdCLCtDQUdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTXlTUUxRdWVyeUJ1aWxkZXIgfSBmcm9tICcuL215c3FsLXF1ZXJ5LWJ1aWxkZXInO1xuaW1wb3J0IHsgTXlTUUxUcmFuc2FjdGlvbk1hbmFnZXIgfSBmcm9tICcuL215c3FsLXRyYW5zYWN0aW9uLW1hbmFnZXInO1xuaW1wb3J0IHsgRXhwcmVzc2lvbkdyb3VwLCBFeHByZXNzaW9uIH0gZnJvbSAnLi9teXNxbC1zdGF0ZW1lbnRzJztcblxuXG5leHBvcnQgaW50ZXJmYWNlIElNeVNRTENvbmZpZ1xue1xuICAgIGhvc3Q6c3RyaW5nO1xuICAgIHBvcnQ/Om51bWJlcjtcbiAgICB1c2VyOnN0cmluZztcbiAgICBwYXNzd29yZDpzdHJpbmc7XG4gICAgZGF0YWJhc2U6c3RyaW5nO1xuICAgIGNoYXJzZXQ6c3RyaW5nO1xufVxuXG4vKlxuZXhwb3J0IGludGVyZmFjZSBJVXBkYXRlU3RhdGVtZW50RGF0YVxue1xuICAgIGZpZWxkTmFtZTpzdHJpbmc7XG5cbiAgICB2YWx1ZToge1xuICAgICAgICB0eXBlOm51bWJlcjtcbiAgICAgICAgZGF0YTphbnk7XG4gICAgfVxufVxuXG5leHBvcnQgdHlwZSBGaWVsZE1hcCA9IHtcbiAgICBbY29sdW1uTmFtZTpzdHJpbmddIDogc3RyaW5nO1xufTtcbiovXG5cbmV4cG9ydCBpbnRlcmZhY2UgSU15U1FMUmVwb3NpdG9yeVxue1xuICAgIHRhYmxlTmFtZTpzdHJpbmc7XG4gICAgcHJpbWFyeUtleTpzdHJpbmc7XG5cbiAgICBpc1ByaW1hcnlLZXlBU3RyaW5nOmJvb2xlYW47XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJTXlTUUxRdWVyeVJlc3VsdFxue1xuICAgIGZpZWxkQ291bnQ6bnVtYmVyO1xuICAgIGFmZmVjdGVkUm93czpudW1iZXI7XG4gICAgaW5zZXJ0SWQ6bnVtYmVyO1xuICAgIHNlcnZlclN0YXR1czpudW1iZXI7XG4gICAgd2FybmluZ0NvdW50Om51bWJlcjtcbiAgICBtZXNzYWdlOnN0cmluZztcbiAgICBjaGFuZ2VkUm93czpudW1iZXI7XG59XG5cbi8qXG5leHBvcnQgaW50ZXJmYWNlIElXaGVyZUZpbHRlciB7XG4gICAgW2tleTogc3RyaW5nXTogc3RyaW5nfG51bWJlcjtcbn1cbiovXG5cbmV4cG9ydCB0eXBlIFByaW1hcnlLZXlWYWx1ZSA9IG51bWJlcnxzdHJpbmc7XG5cbmV4cG9ydCB0eXBlIFF1ZXJ5RXhlY3V0ZXI8VD4gPSAoKSA9PiBQcm9taXNlPFQ+O1xuXG5cbi8vIElNeVNRTCBkYXRhYmFzZWQgaXMgbm93IENPTVBPU0VEIG9mOlxuICAgIC8vIDEuIGEgcXVlcnkgYnVpbGRlclxuICAgIC8vIDIuIGEgdHJhbnNhY3Rpb24gTWFuYWdlclxuZXhwb3J0IGludGVyZmFjZSBJTXlTUUxEYXRhYmFzZVxue1xuICAgIHJlYWRvbmx5IGJ1aWxkZXI6TXlTUUxRdWVyeUJ1aWxkZXI7XG4gICAgcmVhZG9ubHkgdHJhbnNhY3Rpb25NYW5hZ2VyOk15U1FMVHJhbnNhY3Rpb25NYW5hZ2VyO1xufVxuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSVF1ZXJ5UGFyYW1zPFQ+XG57XG4gICAgd2hlcmVNYXA/OlBhcnRpYWw8VD47XG4gICAgd2hlcmVBbmRNYXA/OlBhcnRpYWw8VD47XG4gICAgd2hlcmVPck1hcD86UGFydGlhbDxUPjtcblxuICAgIHdoZXJlQ2xhdXNlPzpFeHByZXNzaW9uO1xuICAgIHdoZXJlUHJpbWFyeUtleXM/OihudW1iZXJ8c3RyaW5nKVtdO1xuXG4gICAgd2hlcmVHcm91cD86RXhwcmVzc2lvbkdyb3VwO1xuXG4gICAgb3JkZXJCeT86c3RyaW5nO1xuICAgIGxpbWl0PzpudW1iZXJ8c3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElTZWxlY3RRdWVyeVBhcmFtczxUPiBleHRlbmRzIElRdWVyeVBhcmFtczxUPlxue1xuICAgIGZpZWxkcz86c3RyaW5nW107XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVF1ZXJ5QnVpbGRlclBhcmFtczxUPiBleHRlbmRzIElRdWVyeVBhcmFtczxUPlxue1xuICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeTtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJU2VsZWN0UXVlcnlCdWlsZGVyUGFyYW1zPFQ+IFxuICAgIGV4dGVuZHMgSVNlbGVjdFF1ZXJ5UGFyYW1zPFQ+LElRdWVyeUJ1aWxkZXJQYXJhbXM8VD5cbntcbiAgICBmaWVsZHM6c3RyaW5nW107XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVVwZGF0ZVF1ZXJ5UGFyYW1zPFQ+IGV4dGVuZHMgSVF1ZXJ5UGFyYW1zPFQ+XG57XG4gICAgc2luZ2xlU3RhdGVtZW50PzpFeHByZXNzaW9uO1xuICAgIHN0YXRlbWVudHM/OkV4cHJlc3Npb25bXTtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJVXBkYXRlUXVlcnlCdWlsZGVyUGFyYW1zPFQ+IFxuICAgIGV4dGVuZHMgSVVwZGF0ZVF1ZXJ5UGFyYW1zPFQ+LElRdWVyeUJ1aWxkZXJQYXJhbXM8VD5cbntcbn0iXX0=