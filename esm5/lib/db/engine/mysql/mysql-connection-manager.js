/**
 * @fileoverview added by tsickle
 * Generated from: lib/db/engine/mysql/mysql-connection-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import * as mysql from "mysql";
import { sleep } from 'earnbet-common';
var MySQLConnectionManager = /** @class */ (function () {
    function MySQLConnectionManager(config) {
        this.config = config;
        this.isConnecting = false;
        this.connect();
    }
    /**
     * @return {?}
     */
    MySQLConnectionManager.prototype.getConnection = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.reconnect()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, this.connection];
                }
            });
        });
    };
    /**
     * @private
     * @return {?}
     */
    MySQLConnectionManager.prototype.reconnect = /**
     * @private
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var isConnected;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.isDisconnected) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.connect()];
                    case 1:
                        isConnected = _a.sent();
                        if (isConnected) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, sleep(1000)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 0];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @private
     * @return {?}
     */
    MySQLConnectionManager.prototype.connect = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        function (resolve) {
            if (!_this.isDisconnected) {
                resolve(true);
                return;
            }
            if (_this.isConnecting) {
                resolve(false);
                return;
            }
            _this.isConnecting = true;
            // end existing connection
            if (_this.connection) {
                _this.connection.end();
            }
            // connection parameters
            _this.connection = mysql.createConnection(_this.config);
            // establish connection
            _this.connection.connect((/**
             * @param {?} err
             * @return {?}
             */
            function (err) {
                console.log(_this.config);
                if (err) {
                    console.error('error connecting: ' + err.stack);
                    resolve(false);
                }
                else {
                    console.log('connected as id ' + _this.connection.threadId);
                    resolve(true);
                }
                _this.isConnecting = false;
            }));
            _this.connection.on('error', (/**
             * @param {?} err
             * @return {?}
             */
            function (err) {
                console.error(err);
            }));
        }));
    };
    Object.defineProperty(MySQLConnectionManager.prototype, "isDisconnected", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.connection == undefined ||
                this.connection.state === 'disconnected';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    MySQLConnectionManager.prototype.endConnection = /**
     * @return {?}
     */
    function () {
        this.connection.end();
    };
    return MySQLConnectionManager;
}());
export { MySQLConnectionManager };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.isConnecting;
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.connection;
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.config;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvZGIvZW5naW5lL215c3FsL215c3FsLWNvbm5lY3Rpb24tbWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUUvQixPQUFPLEVBQUMsS0FBSyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFJckM7SUFNSSxnQ0FBb0IsTUFBbUI7UUFBbkIsV0FBTSxHQUFOLE1BQU0sQ0FBYTtRQUovQixpQkFBWSxHQUFHLEtBQUssQ0FBQztRQUt6QixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbkIsQ0FBQzs7OztJQUVLLDhDQUFhOzs7SUFBbkI7Ozs7NEJBQ0kscUJBQU0sSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFBOzt3QkFBdEIsU0FBc0IsQ0FBQzt3QkFFdkIsc0JBQU8sSUFBSSxDQUFDLFVBQVUsRUFBQzs7OztLQUMxQjs7Ozs7SUFFYSwwQ0FBUzs7OztJQUF2Qjs7Ozs7OzZCQUNXLElBQUksQ0FBQyxjQUFjO3dCQUNGLHFCQUFNLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBQTs7d0JBQWxDLFdBQVcsR0FBRyxTQUFvQjt3QkFFeEMsSUFBSSxXQUFXLEVBQUU7NEJBQ2Isc0JBQU87eUJBQ1Y7d0JBRUQscUJBQU0sS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFBOzt3QkFBakIsU0FBaUIsQ0FBQzs7Ozs7O0tBRXpCOzs7OztJQUVPLHdDQUFPOzs7O0lBQWY7UUFBQSxpQkFpREM7UUEvQ0csT0FBTyxJQUFJLE9BQU87Ozs7UUFBVyxVQUFDLE9BQU87WUFDakMsSUFBSSxDQUFDLEtBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ3RCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDZCxPQUFPO2FBQ1Y7WUFHRCxJQUFJLEtBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ25CLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDZixPQUFPO2FBQ1Y7WUFHRCxLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUd6QiwwQkFBMEI7WUFDMUIsSUFBSSxLQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNqQixLQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDO2FBQ3pCO1lBR0Qsd0JBQXdCO1lBQ3hCLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLGdCQUFnQixDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUV0RCx1QkFBdUI7WUFDdkIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPOzs7O1lBQUMsVUFBQyxHQUFHO2dCQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFHekIsSUFBSSxHQUFHLEVBQUU7b0JBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBRWhELE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDbEI7cUJBQU07b0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUUzRCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2pCO2dCQUVELEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzlCLENBQUMsRUFBQyxDQUFDO1lBRUgsS0FBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsT0FBTzs7OztZQUFDLFVBQUMsR0FBRztnQkFDM0IsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN2QixDQUFDLEVBQUMsQ0FBQztRQUNQLENBQUMsRUFBRSxDQUFDO0lBQ1IsQ0FBQztJQUVELHNCQUFZLGtEQUFjOzs7OztRQUExQjtZQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsSUFBSSxTQUFTO2dCQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssS0FBSyxjQUFjLENBQUM7UUFDckQsQ0FBQzs7O09BQUE7Ozs7SUFFRCw4Q0FBYTs7O0lBQWI7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFDTCw2QkFBQztBQUFELENBQUMsQUF2RkQsSUF1RkM7Ozs7Ozs7SUFyRkcsOENBQTZCOzs7OztJQUU3Qiw0Q0FBb0M7Ozs7O0lBRXhCLHdDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIG15c3FsIGZyb20gXCJteXNxbFwiO1xuXG5pbXBvcnQge3NsZWVwfSBmcm9tICdlYXJuYmV0LWNvbW1vbic7XG5pbXBvcnQgeyBJTXlTUUxDb25maWcgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyXG57XG4gICAgcHJpdmF0ZSBpc0Nvbm5lY3RpbmcgPSBmYWxzZTtcblxuICAgIHByaXZhdGUgY29ubmVjdGlvbjpteXNxbC5Db25uZWN0aW9uO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjb25maWc6SU15U1FMQ29uZmlnKSB7XG4gICAgICAgIHRoaXMuY29ubmVjdCgpO1xuICAgIH1cblxuICAgIGFzeW5jIGdldENvbm5lY3Rpb24oKTpQcm9taXNlPG15c3FsLkNvbm5lY3Rpb24+IHtcbiAgICAgICAgYXdhaXQgdGhpcy5yZWNvbm5lY3QoKTtcblxuICAgICAgICByZXR1cm4gdGhpcy5jb25uZWN0aW9uO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgcmVjb25uZWN0KCkge1xuICAgICAgICB3aGlsZSAodGhpcy5pc0Rpc2Nvbm5lY3RlZCkge1xuICAgICAgICAgICAgY29uc3QgaXNDb25uZWN0ZWQgPSBhd2FpdCB0aGlzLmNvbm5lY3QoKTtcblxuICAgICAgICAgICAgaWYgKGlzQ29ubmVjdGVkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBhd2FpdCBzbGVlcCgxMDAwKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgY29ubmVjdCgpOlByb21pc2U8Ym9vbGVhbj5cbiAgICB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxib29sZWFuPiggKHJlc29sdmUpPT57XG4gICAgICAgICAgICBpZiAoIXRoaXMuaXNEaXNjb25uZWN0ZWQpIHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKHRydWUpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgICBpZiAodGhpcy5pc0Nvbm5lY3RpbmcpIHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKGZhbHNlKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgdGhpcy5pc0Nvbm5lY3RpbmcgPSB0cnVlO1xuXG5cbiAgICAgICAgICAgIC8vIGVuZCBleGlzdGluZyBjb25uZWN0aW9uXG4gICAgICAgICAgICBpZiAodGhpcy5jb25uZWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uLmVuZCgpO1xuICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgIC8vIGNvbm5lY3Rpb24gcGFyYW1ldGVyc1xuICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uID0gbXlzcWwuY3JlYXRlQ29ubmVjdGlvbih0aGlzLmNvbmZpZyk7XG5cbiAgICAgICAgICAgIC8vIGVzdGFibGlzaCBjb25uZWN0aW9uXG4gICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb24uY29ubmVjdCgoZXJyKT0+e1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMuY29uZmlnKTtcblxuXG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdlcnJvciBjb25uZWN0aW5nOiAnICsgZXJyLnN0YWNrKTtcblxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKGZhbHNlKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnY29ubmVjdGVkIGFzIGlkICcgKyB0aGlzLmNvbm5lY3Rpb24udGhyZWFkSWQpO1xuXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUodHJ1ZSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdGhpcy5pc0Nvbm5lY3RpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb24ub24oJ2Vycm9yJywoZXJyKT0+e1xuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9ICk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXQgaXNEaXNjb25uZWN0ZWQoKTpib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29ubmVjdGlvbiA9PSB1bmRlZmluZWQgfHxcbiAgICAgICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb24uc3RhdGUgPT09ICdkaXNjb25uZWN0ZWQnO1xuICAgIH1cblxuICAgIGVuZENvbm5lY3Rpb24oKSB7XG4gICAgICAgIHRoaXMuY29ubmVjdGlvbi5lbmQoKTtcbiAgICB9XG59Il19