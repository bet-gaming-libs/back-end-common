/**
 * @fileoverview added by tsickle
 * Generated from: lib/db/engine/mysql/mysql-query-builder.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { MySQLConnectionManager } from "./mysql-connection-manager";
import { MySQLExpression, SingleExpression, StringStatement, OrExpressionGroup, UpdateStatements, createExpressionGroup, ExpressionGroupJoiner } from "./mysql-statements";
import { MySQLQuery } from './mysql-query';
var MySQLQueryBuilder = /** @class */ (function () {
    function MySQLQueryBuilder(config) {
        this.connectionManager = new MySQLConnectionManager(config);
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    MySQLQueryBuilder.prototype.insert = /**
     * @template T
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    function (repository, entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // construct query
                        return [4 /*yield*/, this.constructInsertQuery(repository, entities)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, new MySQLQuery(query, this.connectionManager)];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    MySQLQueryBuilder.prototype.constructInsertQuery = /**
     * @private
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    function (repository, entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var fields, fieldName, rows, entities_1, entities_1_1, entity, _a, _b, e_1_1;
            var e_1, _c;
            return tslib_1.__generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        fields = [];
                        for (fieldName in entities[0]) {
                            fields.push(fieldName);
                        }
                        rows = [];
                        _d.label = 1;
                    case 1:
                        _d.trys.push([1, 6, 7, 8]);
                        entities_1 = tslib_1.__values(entities), entities_1_1 = entities_1.next();
                        _d.label = 2;
                    case 2:
                        if (!!entities_1_1.done) return [3 /*break*/, 5];
                        entity = entities_1_1.value;
                        _b = (_a = rows).push;
                        return [4 /*yield*/, this.constructRowForInsert(entity)];
                    case 3:
                        _b.apply(_a, [_d.sent()]);
                        _d.label = 4;
                    case 4:
                        entities_1_1 = entities_1.next();
                        return [3 /*break*/, 2];
                    case 5: return [3 /*break*/, 8];
                    case 6:
                        e_1_1 = _d.sent();
                        e_1 = { error: e_1_1 };
                        return [3 /*break*/, 8];
                    case 7:
                        try {
                            if (entities_1_1 && !entities_1_1.done && (_c = entities_1.return)) _c.call(entities_1);
                        }
                        finally { if (e_1) throw e_1.error; }
                        return [7 /*endfinally*/];
                    case 8: return [2 /*return*/, "INSERT INTO " + repository.tableName +
                            ' (' + this.prepareFieldList(fields) + ')' +
                            ' VALUES ' + rows.join(', ') + ';'];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} entity
     * @return {?}
     */
    MySQLQueryBuilder.prototype.constructRowForInsert = /**
     * @private
     * @param {?} entity
     * @return {?}
     */
    function (entity) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var values, _a, _b, _i, propertyName, rawValue, preparedValue;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        values = [];
                        _a = [];
                        for (_b in entity)
                            _a.push(_b);
                        _i = 0;
                        _c.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3 /*break*/, 4];
                        propertyName = _a[_i];
                        rawValue = entity[propertyName];
                        return [4 /*yield*/, this.prepareValueForInsert(rawValue)];
                    case 2:
                        preparedValue = _c.sent();
                        values.push(preparedValue);
                        _c.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/, '(' + values.join(',') + ')'];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareValueForInsert = /**
     * @private
     * @param {?} value
     * @return {?}
     */
    function (value) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = typeof value;
                        switch (_a) {
                            case "string": return [3 /*break*/, 1];
                        }
                        return [3 /*break*/, 6];
                    case 1:
                        _b = value;
                        switch (_b) {
                            case MySQLExpression.NULL: return [3 /*break*/, 2];
                            case MySQLExpression.NOW: return [3 /*break*/, 2];
                        }
                        return [3 /*break*/, 3];
                    case 2: return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, this.escape(value)];
                    case 4:
                        value = _c.sent();
                        return [3 /*break*/, 5];
                    case 5: return [3 /*break*/, 6];
                    case 6: return [2 /*return*/, "" + value];
                }
            });
        });
    };
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @param {?} primaryKeyValues
     * @return {?}
     */
    MySQLQueryBuilder.prototype.selectByPrimaryKeys = /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @param {?} primaryKeyValues
     * @return {?}
     */
    function (repository, fields, primaryKeyValues) {
        /** @type {?} */
        var query = this.prepareFieldsForSelect(fields, repository.tableName) +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    };
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @return {?}
     */
    MySQLQueryBuilder.prototype.selectAll = /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @return {?}
     */
    function (repository, fields) {
        /** @type {?} */
        var query = this.prepareFieldsForSelect(fields, repository.tableName) + ';';
        return new MySQLQuery(query, this.connectionManager);
    };
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    MySQLQueryBuilder.prototype.select = /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.prepareSelectStatement(params)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, new MySQLQuery(query + ";", this.connectionManager)];
                }
            });
        });
    };
    /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareSelectStatement = /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var fields, repository, query, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        fields = params.fields, repository = params.repository;
                        query = this.prepareFieldsForSelect(fields, repository.tableName);
                        _a = query;
                        return [4 /*yield*/, this.prepareWhereOrderAndLimit(params)];
                    case 1:
                        query = _a + _b.sent();
                        return [2 /*return*/, query];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} fields
     * @param {?} tableName
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareFieldsForSelect = /**
     * @private
     * @param {?} fields
     * @param {?} tableName
     * @return {?}
     */
    function (fields, tableName) {
        /** @type {?} */
        var fieldsList = this.prepareFieldList(fields);
        return "SELECT " + fieldsList + " FROM " + tableName;
    };
    /**
     * @private
     * @param {?} fields
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareFieldList = /**
     * @private
     * @param {?} fields
     * @return {?}
     */
    function (fields) {
        var e_2, _a;
        /** @type {?} */
        var prepared = [];
        try {
            for (var fields_1 = tslib_1.__values(fields), fields_1_1 = fields_1.next(); !fields_1_1.done; fields_1_1 = fields_1.next()) {
                var field = fields_1_1.value;
                /** @type {?} */
                var isAlias = field.indexOf(' AS ') > -1;
                prepared.push(isAlias ?
                    field :
                    '`' + field + '`');
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (fields_1_1 && !fields_1_1.done && (_a = fields_1.return)) _a.call(fields_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
        return prepared.join(',');
    };
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    MySQLQueryBuilder.prototype.update = /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var statements, repository, updateStatements, remainingQuery, query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (params.singleStatement) {
                            params.statements = [
                                params.singleStatement
                            ];
                        }
                        statements = params.statements, repository = params.repository;
                        if (!statements ||
                            statements.length < 1) {
                            throw new Error('there must be at least 1 update statement!');
                        }
                        return [4 /*yield*/, this.prepareExpressionGroup(new UpdateStatements(statements))];
                    case 1:
                        updateStatements = _a.sent();
                        return [4 /*yield*/, this.prepareWhereOrderAndLimit(params)];
                    case 2:
                        remainingQuery = _a.sent();
                        query = 'UPDATE ' + repository.tableName +
                            ' SET ' + updateStatements +
                            remainingQuery + ';';
                        return [2 /*return*/, new MySQLQuery(query, this.connectionManager)];
                }
            });
        });
    };
    /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareWhereOrderAndLimit = /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var expressions, whereGroup, orderBy, limit, query, whereClause, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (params.whereMap) {
                            params.whereAndMap = params.whereMap;
                        }
                        if (params.whereAndMap ||
                            params.whereOrMap) {
                            params.whereGroup = createExpressionGroup(params.whereAndMap ?
                                params.whereAndMap :
                                params.whereOrMap, params.whereAndMap ?
                                ExpressionGroupJoiner.AND :
                                ExpressionGroupJoiner.OR);
                        }
                        else if (params.wherePrimaryKeys) {
                            expressions = params.wherePrimaryKeys.map((/**
                             * @param {?} primaryKeyValue
                             * @return {?}
                             */
                            function (primaryKeyValue) { return new StringStatement(params.repository.primaryKey, '' + primaryKeyValue); }));
                            params.whereGroup = new OrExpressionGroup(expressions);
                        }
                        else if (params.whereClause) {
                            params.whereGroup = new SingleExpression(params.whereClause);
                        }
                        whereGroup = params.whereGroup, orderBy = params.orderBy, limit = params.limit;
                        query = '';
                        _a = whereGroup;
                        if (!_a) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.prepareExpressionGroup(whereGroup)];
                    case 1:
                        _a = (_b.sent());
                        _b.label = 2;
                    case 2:
                        whereClause = _a;
                        if (whereClause) {
                            query += " \n                WHERE " + whereClause;
                        }
                        if (orderBy) {
                            query += " \n                ORDER BY " + orderBy;
                        }
                        if (limit) {
                            query += " \n                LIMIT " + limit;
                        }
                        return [2 /*return*/, query];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} group
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareExpressionGroup = /**
     * @private
     * @param {?} group
     * @return {?}
     */
    function (group) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var statements, useBrackets, _a, _b, expression, leftSide, operator, rightSide, rightSideValue, _c, _d, e_3_1;
            var e_3, _e;
            return tslib_1.__generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        statements = [];
                        useBrackets = group.surroundExpressionWithBrackets;
                        _f.label = 1;
                    case 1:
                        _f.trys.push([1, 8, 9, 10]);
                        _a = tslib_1.__values(group.expressions), _b = _a.next();
                        _f.label = 2;
                    case 2:
                        if (!!_b.done) return [3 /*break*/, 7];
                        expression = _b.value;
                        leftSide = expression.leftSide, operator = expression.operator, rightSide = expression.rightSide;
                        if (!rightSide.isString) return [3 /*break*/, 4];
                        _d = "";
                        return [4 /*yield*/, this.escape((/** @type {?} */ (rightSide.data)))];
                    case 3:
                        _c = _d + (_f.sent());
                        return [3 /*break*/, 5];
                    case 4:
                        _c = rightSide.data;
                        _f.label = 5;
                    case 5:
                        rightSideValue = _c;
                        statements.push((useBrackets ? '(' : '') + " `" + leftSide + "` " + operator + " " + rightSideValue + " " + (useBrackets ? ')' : ''));
                        _f.label = 6;
                    case 6:
                        _b = _a.next();
                        return [3 /*break*/, 2];
                    case 7: return [3 /*break*/, 10];
                    case 8:
                        e_3_1 = _f.sent();
                        e_3 = { error: e_3_1 };
                        return [3 /*break*/, 10];
                    case 9:
                        try {
                            if (_b && !_b.done && (_e = _a.return)) _e.call(_a);
                        }
                        finally { if (e_3) throw e_3.error; }
                        return [7 /*endfinally*/];
                    case 10: return [2 /*return*/, statements.join(" " + group.joiner + " ")];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    MySQLQueryBuilder.prototype.escape = /**
     * @private
     * @param {?} value
     * @return {?}
     */
    function (value) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var connection;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connectionManager.getConnection()];
                    case 1:
                        connection = _a.sent();
                        return [2 /*return*/, connection.escape(value)];
                }
            });
        });
    };
    /**
     * @param {?} repository
     * @param {?} primaryKeyValues
     * @return {?}
     */
    MySQLQueryBuilder.prototype.delete = /**
     * @param {?} repository
     * @param {?} primaryKeyValues
     * @return {?}
     */
    function (repository, primaryKeyValues) {
        if (primaryKeyValues.length < 1) {
            throw new Error('Please specify which records to delete!');
        }
        /** @type {?} */
        var query = 'DELETE FROM ' + repository.tableName +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    };
    /**
     * @template T
     * @param {?} sql
     * @return {?}
     */
    MySQLQueryBuilder.prototype.rawQuery = /**
     * @template T
     * @param {?} sql
     * @return {?}
     */
    function (sql) {
        return new MySQLQuery(sql, this.connectionManager);
    };
    return MySQLQueryBuilder;
}());
export { MySQLQueryBuilder };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLQueryBuilder.prototype.connectionManager;
}
/**
 * @param {?} repository
 * @param {?} primaryKeyValues
 * @return {?}
 */
function constructWhereWithPrimaryKeys(repository, primaryKeyValues) {
    var e_4, _a;
    /** @type {?} */
    var conditions = [];
    try {
        for (var primaryKeyValues_1 = tslib_1.__values(primaryKeyValues), primaryKeyValues_1_1 = primaryKeyValues_1.next(); !primaryKeyValues_1_1.done; primaryKeyValues_1_1 = primaryKeyValues_1.next()) {
            var primaryKeyValue = primaryKeyValues_1_1.value;
            if (repository.isPrimaryKeyAString) {
                primaryKeyValue = "'" + primaryKeyValue + "'";
            }
            /** @type {?} */
            var condition = repository.primaryKey + ' = ' + primaryKeyValue;
            conditions.push(condition);
        }
    }
    catch (e_4_1) { e_4 = { error: e_4_1 }; }
    finally {
        try {
            if (primaryKeyValues_1_1 && !primaryKeyValues_1_1.done && (_a = primaryKeyValues_1.return)) _a.call(primaryKeyValues_1);
        }
        finally { if (e_4) throw e_4.error; }
    }
    return ' WHERE ' + conditions.join(' OR ');
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcXVlcnktYnVpbGRlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL2VuZ2luZS9teXNxbC9teXNxbC1xdWVyeS1idWlsZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBbUIsZUFBZSxFQUFFLGdCQUFnQixFQUFFLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxnQkFBZ0IsRUFBRSxxQkFBcUIsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQzVMLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFHekM7SUFJSSwyQkFBWSxNQUFtQjtRQUMzQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNoRSxDQUFDOzs7Ozs7O0lBRVksa0NBQU07Ozs7OztJQUFuQixVQUNJLFVBQTJCLEVBQzNCLFFBQVk7Ozs7Ozs7d0JBR0UscUJBQU0sSUFBSSxDQUFDLG9CQUFvQixDQUN6QyxVQUFVLEVBQUMsUUFBUSxDQUN0QixFQUFBOzt3QkFGSyxLQUFLLEdBQUcsU0FFYjt3QkFFRCxzQkFBTyxJQUFJLFVBQVUsQ0FBb0IsS0FBSyxFQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFDOzs7O0tBQzFFOzs7Ozs7O0lBQ2EsZ0RBQW9COzs7Ozs7SUFBbEMsVUFDSSxVQUEyQixFQUMzQixRQUFjOzs7Ozs7O3dCQUdSLE1BQU0sR0FBWSxFQUFFO3dCQUMxQixLQUFXLFNBQVMsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUU7NEJBQ2pDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7eUJBQzFCO3dCQUVLLElBQUksR0FBWSxFQUFFOzs7O3dCQUNILGFBQUEsaUJBQUEsUUFBUSxDQUFBOzs7O3dCQUFsQixNQUFNO3dCQUNiLEtBQUEsQ0FBQSxLQUFBLElBQUksQ0FBQSxDQUFDLElBQUksQ0FBQTt3QkFDTCxxQkFBTSxJQUFJLENBQUMscUJBQXFCLENBQzVCLE1BQU0sQ0FDVCxFQUFBOzt3QkFITCxjQUNJLFNBRUMsRUFDSixDQUFDOzs7Ozs7Ozs7Ozs7Ozs7OzRCQUdOLHNCQUFPLGNBQWMsR0FBRyxVQUFVLENBQUMsU0FBUzs0QkFDeEMsSUFBSSxHQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsR0FBRSxHQUFHOzRCQUN4QyxVQUFVLEdBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBQyxHQUFHLEVBQUE7Ozs7S0FFckM7Ozs7OztJQUNhLGlEQUFxQjs7Ozs7SUFBbkMsVUFDSSxNQUFVOzs7Ozs7d0JBR0osTUFBTSxHQUFZLEVBQUU7O21DQUVDLE1BQU07Ozs7Ozs7d0JBQ3ZCLFFBQVEsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDO3dCQUdqQyxxQkFBTSxJQUFJLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLEVBQUE7O3dCQUR4QyxhQUFhLEdBQ2YsU0FBMEM7d0JBRTlDLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7Ozs7OzRCQUcvQixzQkFBTyxHQUFHLEdBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRSxHQUFHLEVBQUM7Ozs7S0FDckM7Ozs7OztJQUNhLGlEQUFxQjs7Ozs7SUFBbkMsVUFBb0MsS0FBUzs7Ozs7O3dCQUVqQyxLQUFBLE9BQU8sS0FBSyxDQUFBOztpQ0FDWCxRQUFRLENBQUMsQ0FBVCx3QkFBUTs7Ozt3QkFDRCxLQUFBLEtBQUssQ0FBQTs7aUNBQ0osZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFyQix3QkFBb0I7aUNBQ3BCLGVBQWUsQ0FBQyxHQUFHLENBQUMsQ0FBcEIsd0JBQW1COzs7NEJBQ3hCLHdCQUFNOzRCQUdNLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUE7O3dCQUFoQyxLQUFLLEdBQUcsU0FBd0IsQ0FBQzt3QkFDckMsd0JBQU07NEJBRWQsd0JBQU07NEJBR1Ysc0JBQU8sRUFBRSxHQUFDLEtBQUssRUFBQzs7OztLQUNuQjs7Ozs7Ozs7SUFHTSwrQ0FBbUI7Ozs7Ozs7SUFBMUIsVUFDSSxVQUEyQixFQUMzQixNQUFlLEVBQ2YsZ0JBQWtDOztZQUc1QixLQUFLLEdBQ1AsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sRUFBQyxVQUFVLENBQUMsU0FBUyxDQUFDO1lBQ3hELDZCQUE2QixDQUFDLFVBQVUsRUFBQyxnQkFBZ0IsQ0FBQyxHQUFFLEdBQUc7UUFFbkUsT0FBTyxJQUFJLFVBQVUsQ0FBTSxLQUFLLEVBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDN0QsQ0FBQzs7Ozs7OztJQUVNLHFDQUFTOzs7Ozs7SUFBaEIsVUFDSSxVQUEyQixFQUMzQixNQUFlOztZQUVULEtBQUssR0FDUCxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxFQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxHQUFHO1FBRWxFLE9BQU8sSUFBSSxVQUFVLENBQU0sS0FBSyxFQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQzdELENBQUM7Ozs7OztJQUdZLGtDQUFNOzs7OztJQUFuQixVQUF1QixNQUFtQzs7Ozs7NEJBQ3hDLHFCQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQWpELEtBQUssR0FBRyxTQUF5Qzt3QkFFdkQsc0JBQU8sSUFBSSxVQUFVLENBQVMsS0FBSyxNQUFHLEVBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUM7Ozs7S0FDbEU7Ozs7Ozs7SUFFYSxrREFBc0I7Ozs7OztJQUFwQyxVQUF3QyxNQUFtQzs7Ozs7O3dCQUVoRSxNQUFNLEdBQWUsTUFBTSxPQUFyQixFQUFDLFVBQVUsR0FBSSxNQUFNLFdBQVY7d0JBRXBCLEtBQUssR0FBVSxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxFQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUM7d0JBRTNFLEtBQUEsS0FBSyxDQUFBO3dCQUFJLHFCQUFNLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQXJELEtBQUssR0FBTCxLQUFTLFNBQTRDLENBQUM7d0JBRXRELHNCQUFPLEtBQUssRUFBQzs7OztLQUNoQjs7Ozs7OztJQUVPLGtEQUFzQjs7Ozs7O0lBQTlCLFVBQStCLE1BQWUsRUFBQyxTQUFnQjs7WUFDckQsVUFBVSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUM7UUFFaEQsT0FBTyxZQUFVLFVBQVUsY0FBUyxTQUFXLENBQUM7SUFDcEQsQ0FBQzs7Ozs7O0lBR08sNENBQWdCOzs7OztJQUF4QixVQUF5QixNQUFlOzs7WUFFOUIsUUFBUSxHQUFZLEVBQUU7O1lBRTVCLEtBQW9CLElBQUEsV0FBQSxpQkFBQSxNQUFNLENBQUEsOEJBQUEsa0RBQUU7Z0JBQXZCLElBQU0sS0FBSyxtQkFBQTs7b0JBQ04sT0FBTyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUUxQyxRQUFRLENBQUMsSUFBSSxDQUNULE9BQU8sQ0FBQyxDQUFDO29CQUNULEtBQUssQ0FBQyxDQUFDO29CQUNQLEdBQUcsR0FBQyxLQUFLLEdBQUMsR0FBRyxDQUNoQixDQUFDO2FBQ0w7Ozs7Ozs7OztRQUVELE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUM5QixDQUFDOzs7Ozs7SUFFWSxrQ0FBTTs7Ozs7SUFBbkIsVUFBdUIsTUFBbUM7Ozs7Ozt3QkFDdEQsSUFBSSxNQUFNLENBQUMsZUFBZSxFQUFFOzRCQUN4QixNQUFNLENBQUMsVUFBVSxHQUFHO2dDQUNoQixNQUFNLENBQUMsZUFBZTs2QkFDekIsQ0FBQzt5QkFDTDt3QkFHTSxVQUFVLEdBQWUsTUFBTSxXQUFyQixFQUFDLFVBQVUsR0FBSSxNQUFNLFdBQVY7d0JBRTVCLElBQ0ksQ0FBQyxVQUFVOzRCQUNYLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUN2Qjs0QkFDRSxNQUFNLElBQUksS0FBSyxDQUFDLDRDQUE0QyxDQUFDLENBQUM7eUJBQ2pFO3dCQUcrQixxQkFBTSxJQUFJLENBQUMsc0JBQXNCLENBQzdELElBQUksZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQ25DLEVBQUE7O3dCQUZLLGdCQUFnQixHQUFVLFNBRS9CO3dCQUU2QixxQkFBTSxJQUFJLENBQUMseUJBQXlCLENBQzlELE1BQU0sQ0FDVCxFQUFBOzt3QkFGSyxjQUFjLEdBQVUsU0FFN0I7d0JBRUssS0FBSyxHQUNQLFNBQVMsR0FBQyxVQUFVLENBQUMsU0FBUzs0QkFDOUIsT0FBTyxHQUFFLGdCQUFnQjs0QkFDeEIsY0FBYyxHQUFFLEdBQUc7d0JBRXhCLHNCQUFPLElBQUksVUFBVSxDQUFvQixLQUFLLEVBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUM7Ozs7S0FDMUU7Ozs7Ozs7SUFFYSxxREFBeUI7Ozs7OztJQUF2QyxVQUEyQyxNQUE2Qjs7Ozs7O3dCQUNwRSxJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7NEJBQ2pCLE1BQU0sQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQzt5QkFDeEM7d0JBRUQsSUFDSSxNQUFNLENBQUMsV0FBVzs0QkFDbEIsTUFBTSxDQUFDLFVBQVUsRUFDbkI7NEJBQ0UsTUFBTSxDQUFDLFVBQVUsR0FBRyxxQkFBcUIsQ0FDckMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dDQUNoQixNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7Z0NBQ3BCLE1BQU0sQ0FBQyxVQUFVLEVBQ3JCLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztnQ0FDaEIscUJBQXFCLENBQUMsR0FBRyxDQUFDLENBQUM7Z0NBQzNCLHFCQUFxQixDQUFDLEVBQUUsQ0FDL0IsQ0FBQzt5QkFDTDs2QkFDSSxJQUFJLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRTs0QkFDeEIsV0FBVyxHQUFxQixNQUFNLENBQUMsZ0JBQWdCLENBQUMsR0FBRzs7Ozs0QkFDN0QsVUFBQyxlQUFlLElBQUssT0FBQSxJQUFJLGVBQWUsQ0FDcEMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQzVCLEVBQUUsR0FBQyxlQUFlLENBQ3JCLEVBSG9CLENBR3BCLEVBQ0o7NEJBRUQsTUFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDO3lCQUMxRDs2QkFDSSxJQUFJLE1BQU0sQ0FBQyxXQUFXLEVBQUU7NEJBQ3pCLE1BQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxnQkFBZ0IsQ0FDcEMsTUFBTSxDQUFDLFdBQVcsQ0FDckIsQ0FBQzt5QkFDTDt3QkFHTSxVQUFVLEdBQWtCLE1BQU0sV0FBeEIsRUFBQyxPQUFPLEdBQVUsTUFBTSxRQUFoQixFQUFDLEtBQUssR0FBSSxNQUFNLE1BQVY7d0JBRTNCLEtBQUssR0FBRyxFQUFFO3dCQUdWLEtBQUEsVUFBVSxDQUFBO2lDQUFWLHdCQUFVO3dCQUNWLHFCQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsRUFBQTs7OEJBQTdDLFNBQTZDOzs7d0JBRjNDLFdBQVcsS0FFZ0M7d0JBRWpELElBQUksV0FBVyxFQUFFOzRCQUNiLEtBQUssSUFBSSw4QkFDRyxXQUFhLENBQUM7eUJBQzdCO3dCQUVELElBQUksT0FBTyxFQUFFOzRCQUNULEtBQUssSUFBSSxpQ0FDTSxPQUFTLENBQUM7eUJBQzVCO3dCQUVELElBQUksS0FBSyxFQUFFOzRCQUNQLEtBQUssSUFBSSw4QkFDRyxLQUFPLENBQUM7eUJBQ3ZCO3dCQUVELHNCQUFPLEtBQUssRUFBQzs7OztLQUNoQjs7Ozs7O0lBRWEsa0RBQXNCOzs7OztJQUFwQyxVQUFxQyxLQUFxQjs7Ozs7Ozt3QkFDaEQsVUFBVSxHQUFZLEVBQUU7d0JBRXhCLFdBQVcsR0FBRyxLQUFLLENBQUMsOEJBQThCOzs7O3dCQUUvQixLQUFBLGlCQUFBLEtBQUssQ0FBQyxXQUFXLENBQUE7Ozs7d0JBQS9CLFVBQVU7d0JBQ1YsUUFBUSxHQUF1QixVQUFVLFNBQWpDLEVBQUMsUUFBUSxHQUFjLFVBQVUsU0FBeEIsRUFBQyxTQUFTLEdBQUksVUFBVSxVQUFkOzZCQUc5QixTQUFTLENBQUMsUUFBUSxFQUFsQix3QkFBa0I7O3dCQUNWLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQUEsU0FBUyxDQUFDLElBQUksRUFBVSxDQUFDLEVBQUE7O3dCQUEvQyxLQUFBLE1BQUksU0FBMkMsQ0FBRyxDQUFBOzs7d0JBQ2xELEtBQUEsU0FBUyxDQUFDLElBQUksQ0FBQTs7O3dCQUhoQixjQUFjLEtBR0U7d0JBRXRCLFVBQVUsQ0FBQyxJQUFJLENBQ1gsQ0FDSSxXQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxXQUNwQixRQUFRLFVBQU0sUUFBUSxTQUFJLGNBQWMsVUFDMUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FDeEIsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7OzZCQUdaLHNCQUFPLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBSSxLQUFLLENBQUMsTUFBTSxNQUFHLENBQUMsRUFBQzs7OztLQUMvQzs7Ozs7O0lBR2Esa0NBQU07Ozs7O0lBQXBCLFVBQXFCLEtBQVk7Ozs7OzRCQUVWLHFCQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsRUFBQTs7d0JBQXpELFVBQVUsR0FBRyxTQUE0Qzt3QkFFL0Qsc0JBQU8sVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBQzs7OztLQUNuQzs7Ozs7O0lBRU0sa0NBQU07Ozs7O0lBQWIsVUFDSSxVQUEyQixFQUMzQixnQkFBc0I7UUFHdEIsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sSUFBSSxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQztTQUM5RDs7WUFHSyxLQUFLLEdBQUcsY0FBYyxHQUFDLFVBQVUsQ0FBQyxTQUFTO1lBQ3JDLDZCQUE2QixDQUFDLFVBQVUsRUFBQyxnQkFBZ0IsQ0FBQyxHQUFDLEdBQUc7UUFFMUUsT0FBTyxJQUFJLFVBQVUsQ0FBb0IsS0FBSyxFQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQzNFLENBQUM7Ozs7OztJQUVELG9DQUFROzs7OztJQUFSLFVBQVksR0FBVztRQUNuQixPQUFPLElBQUksVUFBVSxDQUFJLEdBQUcsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBQ0wsd0JBQUM7QUFBRCxDQUFDLEFBblNELElBbVNDOzs7Ozs7O0lBalNHLDhDQUFpRDs7Ozs7OztBQW9TckQsU0FBUyw2QkFBNkIsQ0FDbEMsVUFBMkIsRUFDM0IsZ0JBQXNCOzs7UUFHaEIsVUFBVSxHQUFZLEVBQUU7O1FBRTlCLEtBQTRCLElBQUEscUJBQUEsaUJBQUEsZ0JBQWdCLENBQUEsa0RBQUEsZ0ZBQUU7WUFBekMsSUFBSSxlQUFlLDZCQUFBO1lBQ3BCLElBQUksVUFBVSxDQUFDLG1CQUFtQixFQUFFO2dCQUNoQyxlQUFlLEdBQUcsR0FBRyxHQUFDLGVBQWUsR0FBQyxHQUFHLENBQUM7YUFDN0M7O2dCQUVLLFNBQVMsR0FBRyxVQUFVLENBQUMsVUFBVSxHQUFHLEtBQUssR0FBRyxlQUFlO1lBQ2pFLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDOUI7Ozs7Ozs7OztJQUVELE9BQU8sU0FBUyxHQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDN0MsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElNeVNRTFJlcG9zaXRvcnksIFByaW1hcnlLZXlWYWx1ZSwgSU15U1FMUXVlcnlSZXN1bHQsIElNeVNRTENvbmZpZywgSVNlbGVjdFF1ZXJ5QnVpbGRlclBhcmFtcywgSVVwZGF0ZVF1ZXJ5QnVpbGRlclBhcmFtcywgSVF1ZXJ5QnVpbGRlclBhcmFtcyB9IGZyb20gXCIuL2ludGVyZmFjZXNcIjtcbmltcG9ydCB7IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIgfSBmcm9tIFwiLi9teXNxbC1jb25uZWN0aW9uLW1hbmFnZXJcIjtcbmltcG9ydCB7IEV4cHJlc3Npb25Hcm91cCwgTXlTUUxFeHByZXNzaW9uLCBTaW5nbGVFeHByZXNzaW9uLCBTdHJpbmdTdGF0ZW1lbnQsIE9yRXhwcmVzc2lvbkdyb3VwLCBVcGRhdGVTdGF0ZW1lbnRzLCBjcmVhdGVFeHByZXNzaW9uR3JvdXAsIEV4cHJlc3Npb25Hcm91cEpvaW5lciB9IGZyb20gXCIuL215c3FsLXN0YXRlbWVudHNcIjtcbmltcG9ydCB7TXlTUUxRdWVyeX0gZnJvbSAnLi9teXNxbC1xdWVyeSc7XG5cblxuZXhwb3J0IGNsYXNzIE15U1FMUXVlcnlCdWlsZGVyXG57XG4gICAgcHJpdmF0ZSBjb25uZWN0aW9uTWFuYWdlcjpNeVNRTENvbm5lY3Rpb25NYW5hZ2VyO1xuXG4gICAgY29uc3RydWN0b3IoY29uZmlnOklNeVNRTENvbmZpZykge1xuICAgICAgICB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyID0gbmV3IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIoY29uZmlnKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgYXN5bmMgaW5zZXJ0PFQ+KFxuICAgICAgICByZXBvc2l0b3J5OklNeVNRTFJlcG9zaXRvcnksXG4gICAgICAgIGVudGl0aWVzOlRbXVxuICAgICkge1xuICAgICAgICAvLyBjb25zdHJ1Y3QgcXVlcnlcbiAgICAgICAgY29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLmNvbnN0cnVjdEluc2VydFF1ZXJ5KFxuICAgICAgICAgICAgcmVwb3NpdG9yeSxlbnRpdGllc1xuICAgICAgICApO1xuXG4gICAgICAgIHJldHVybiBuZXcgTXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4ocXVlcnksdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxuICAgIHByaXZhdGUgYXN5bmMgY29uc3RydWN0SW5zZXJ0UXVlcnkoXG4gICAgICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeSxcbiAgICAgICAgZW50aXRpZXM6YW55W11cbiAgICApOlByb21pc2U8c3RyaW5nPlxuICAgIHtcbiAgICAgICAgY29uc3QgZmllbGRzOnN0cmluZ1tdID0gW107XG4gICAgICAgIGZvciAoY29uc3QgZmllbGROYW1lIGluIGVudGl0aWVzWzBdKSB7XG4gICAgICAgICAgICBmaWVsZHMucHVzaChmaWVsZE5hbWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3Qgcm93czpzdHJpbmdbXSA9IFtdO1xuICAgICAgICBmb3IgKGNvbnN0IGVudGl0eSBvZiBlbnRpdGllcykge1xuICAgICAgICAgICAgcm93cy5wdXNoKFxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuY29uc3RydWN0Um93Rm9ySW5zZXJ0KFxuICAgICAgICAgICAgICAgICAgICBlbnRpdHlcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIFwiSU5TRVJUIElOVE8gXCIgKyByZXBvc2l0b3J5LnRhYmxlTmFtZSArXG4gICAgICAgICAgICAnICgnKyB0aGlzLnByZXBhcmVGaWVsZExpc3QoZmllbGRzKSArJyknICtcbiAgICAgICAgICAgICcgVkFMVUVTICcrcm93cy5qb2luKCcsICcpKyc7J1xuXG4gICAgfVxuICAgIHByaXZhdGUgYXN5bmMgY29uc3RydWN0Um93Rm9ySW5zZXJ0KFxuICAgICAgICBlbnRpdHk6YW55XG4gICAgKTpQcm9taXNlPHN0cmluZz5cbiAgICB7XG4gICAgICAgIGNvbnN0IHZhbHVlczpzdHJpbmdbXSA9IFtdO1xuXG4gICAgICAgIGZvciAoY29uc3QgcHJvcGVydHlOYW1lIGluIGVudGl0eSkge1xuICAgICAgICAgICAgY29uc3QgcmF3VmFsdWUgPSBlbnRpdHlbcHJvcGVydHlOYW1lXTtcblxuICAgICAgICAgICAgY29uc3QgcHJlcGFyZWRWYWx1ZTpzdHJpbmcgPVxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMucHJlcGFyZVZhbHVlRm9ySW5zZXJ0KHJhd1ZhbHVlKTtcblxuICAgICAgICAgICAgdmFsdWVzLnB1c2gocHJlcGFyZWRWYWx1ZSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gJygnKyB2YWx1ZXMuam9pbignLCcpICsnKSc7XG4gICAgfVxuICAgIHByaXZhdGUgYXN5bmMgcHJlcGFyZVZhbHVlRm9ySW5zZXJ0KHZhbHVlOmFueSk6UHJvbWlzZTxzdHJpbmc+XG4gICAge1xuICAgICAgICBzd2l0Y2ggKHR5cGVvZiB2YWx1ZSkge1xuICAgICAgICAgICAgY2FzZSBcInN0cmluZ1wiOlxuICAgICAgICAgICAgICAgIHN3aXRjaCAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBNeVNRTEV4cHJlc3Npb24uTlVMTDpcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBNeVNRTEV4cHJlc3Npb24uTk9XOlxuICAgICAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSBhd2FpdCB0aGlzLmVzY2FwZSh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIFwiXCIrdmFsdWU7XG4gICAgfVxuXG5cbiAgICBwdWJsaWMgc2VsZWN0QnlQcmltYXJ5S2V5czxUPihcbiAgICAgICAgcmVwb3NpdG9yeTpJTXlTUUxSZXBvc2l0b3J5LFxuICAgICAgICBmaWVsZHM6c3RyaW5nW10sXG4gICAgICAgIHByaW1hcnlLZXlWYWx1ZXM6UHJpbWFyeUtleVZhbHVlW11cbiAgICApXG4gICAge1xuICAgICAgICBjb25zdCBxdWVyeSA9IFxuICAgICAgICAgICAgdGhpcy5wcmVwYXJlRmllbGRzRm9yU2VsZWN0KGZpZWxkcyxyZXBvc2l0b3J5LnRhYmxlTmFtZSkgK1xuICAgICAgICAgICAgY29uc3RydWN0V2hlcmVXaXRoUHJpbWFyeUtleXMocmVwb3NpdG9yeSxwcmltYXJ5S2V5VmFsdWVzKSArJzsnO1xuXG4gICAgICAgIHJldHVybiBuZXcgTXlTUUxRdWVyeTxUW10+KHF1ZXJ5LHRoaXMuY29ubmVjdGlvbk1hbmFnZXIpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzZWxlY3RBbGw8VD4oXG4gICAgICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeSxcbiAgICAgICAgZmllbGRzOnN0cmluZ1tdLFxuICAgICkge1xuICAgICAgICBjb25zdCBxdWVyeSA9XG4gICAgICAgICAgICB0aGlzLnByZXBhcmVGaWVsZHNGb3JTZWxlY3QoZmllbGRzLHJlcG9zaXRvcnkudGFibGVOYW1lKSArICc7JztcblxuICAgICAgICByZXR1cm4gbmV3IE15U1FMUXVlcnk8VFtdPihxdWVyeSx0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyKTtcbiAgICB9XG5cblxuICAgIHB1YmxpYyBhc3luYyBzZWxlY3Q8VD4ocGFyYW1zOklTZWxlY3RRdWVyeUJ1aWxkZXJQYXJhbXM8VD4pIHtcbiAgICAgICAgY29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLnByZXBhcmVTZWxlY3RTdGF0ZW1lbnQocGFyYW1zKTtcblxuICAgICAgICByZXR1cm4gbmV3IE15U1FMUXVlcnk8VFtdPihgJHtxdWVyeX07YCx0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGFzeW5jIHByZXBhcmVTZWxlY3RTdGF0ZW1lbnQ8VD4ocGFyYW1zOklTZWxlY3RRdWVyeUJ1aWxkZXJQYXJhbXM8VD4pOlByb21pc2U8c3RyaW5nPlxuICAgIHtcbiAgICAgICAgY29uc3Qge2ZpZWxkcyxyZXBvc2l0b3J5fSA9IHBhcmFtcztcblxuICAgICAgICBsZXQgcXVlcnk6c3RyaW5nID0gdGhpcy5wcmVwYXJlRmllbGRzRm9yU2VsZWN0KGZpZWxkcyxyZXBvc2l0b3J5LnRhYmxlTmFtZSk7XG5cbiAgICAgICAgcXVlcnkgKz0gYXdhaXQgdGhpcy5wcmVwYXJlV2hlcmVPcmRlckFuZExpbWl0KHBhcmFtcyk7XG5cbiAgICAgICAgcmV0dXJuIHF1ZXJ5O1xuICAgIH1cblxuICAgIHByaXZhdGUgcHJlcGFyZUZpZWxkc0ZvclNlbGVjdChmaWVsZHM6c3RyaW5nW10sdGFibGVOYW1lOnN0cmluZyk6c3RyaW5nIHtcbiAgICAgICAgY29uc3QgZmllbGRzTGlzdCA9IHRoaXMucHJlcGFyZUZpZWxkTGlzdChmaWVsZHMpO1xuXG4gICAgICAgIHJldHVybiBgU0VMRUNUICR7ZmllbGRzTGlzdH0gRlJPTSAke3RhYmxlTmFtZX1gO1xuICAgIH1cblxuXG4gICAgcHJpdmF0ZSBwcmVwYXJlRmllbGRMaXN0KGZpZWxkczpzdHJpbmdbXSk6c3RyaW5nXG4gICAge1xuICAgICAgICBjb25zdCBwcmVwYXJlZDpzdHJpbmdbXSA9IFtdO1xuXG4gICAgICAgIGZvciAoY29uc3QgZmllbGQgb2YgZmllbGRzKSB7XG4gICAgICAgICAgICBjb25zdCBpc0FsaWFzID0gZmllbGQuaW5kZXhPZignIEFTICcpID4gLTE7XG5cbiAgICAgICAgICAgIHByZXBhcmVkLnB1c2goXG4gICAgICAgICAgICAgICAgaXNBbGlhcyA/XG4gICAgICAgICAgICAgICAgZmllbGQgOlxuICAgICAgICAgICAgICAgICdgJytmaWVsZCsnYCdcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gcHJlcGFyZWQuam9pbignLCcpO1xuICAgIH1cblxuICAgIHB1YmxpYyBhc3luYyB1cGRhdGU8VD4ocGFyYW1zOklVcGRhdGVRdWVyeUJ1aWxkZXJQYXJhbXM8VD4pIHtcbiAgICAgICAgaWYgKHBhcmFtcy5zaW5nbGVTdGF0ZW1lbnQpIHtcbiAgICAgICAgICAgIHBhcmFtcy5zdGF0ZW1lbnRzID0gW1xuICAgICAgICAgICAgICAgIHBhcmFtcy5zaW5nbGVTdGF0ZW1lbnRcbiAgICAgICAgICAgIF07XG4gICAgICAgIH1cblxuXG4gICAgICAgIGNvbnN0IHtzdGF0ZW1lbnRzLHJlcG9zaXRvcnl9ID0gcGFyYW1zO1xuXG4gICAgICAgIGlmIChcbiAgICAgICAgICAgICFzdGF0ZW1lbnRzIHx8XG4gICAgICAgICAgICBzdGF0ZW1lbnRzLmxlbmd0aCA8IDFcbiAgICAgICAgKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ3RoZXJlIG11c3QgYmUgYXQgbGVhc3QgMSB1cGRhdGUgc3RhdGVtZW50IScpO1xuICAgICAgICB9XG5cblxuICAgICAgICBjb25zdCB1cGRhdGVTdGF0ZW1lbnRzOnN0cmluZyA9IGF3YWl0IHRoaXMucHJlcGFyZUV4cHJlc3Npb25Hcm91cChcbiAgICAgICAgICAgIG5ldyBVcGRhdGVTdGF0ZW1lbnRzKHN0YXRlbWVudHMpXG4gICAgICAgICk7XG5cbiAgICAgICAgY29uc3QgcmVtYWluaW5nUXVlcnk6c3RyaW5nID0gYXdhaXQgdGhpcy5wcmVwYXJlV2hlcmVPcmRlckFuZExpbWl0KFxuICAgICAgICAgICAgcGFyYW1zXG4gICAgICAgICk7XG5cbiAgICAgICAgY29uc3QgcXVlcnk6c3RyaW5nID0gXG4gICAgICAgICAgICAnVVBEQVRFICcrcmVwb3NpdG9yeS50YWJsZU5hbWUrXG4gICAgICAgICAgICAnIFNFVCAnKyB1cGRhdGVTdGF0ZW1lbnRzICtcbiAgICAgICAgICAgICByZW1haW5pbmdRdWVyeSArJzsnO1xuXG4gICAgICAgIHJldHVybiBuZXcgTXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4ocXVlcnksdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhc3luYyBwcmVwYXJlV2hlcmVPcmRlckFuZExpbWl0PFQ+KHBhcmFtczpJUXVlcnlCdWlsZGVyUGFyYW1zPFQ+KSB7XG4gICAgICAgIGlmIChwYXJhbXMud2hlcmVNYXApIHtcbiAgICAgICAgICAgIHBhcmFtcy53aGVyZUFuZE1hcCA9IHBhcmFtcy53aGVyZU1hcDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIHBhcmFtcy53aGVyZUFuZE1hcCB8fFxuICAgICAgICAgICAgcGFyYW1zLndoZXJlT3JNYXBcbiAgICAgICAgKSB7XG4gICAgICAgICAgICBwYXJhbXMud2hlcmVHcm91cCA9IGNyZWF0ZUV4cHJlc3Npb25Hcm91cChcbiAgICAgICAgICAgICAgICBwYXJhbXMud2hlcmVBbmRNYXAgP1xuICAgICAgICAgICAgICAgICAgICBwYXJhbXMud2hlcmVBbmRNYXAgOlxuICAgICAgICAgICAgICAgICAgICBwYXJhbXMud2hlcmVPck1hcCxcbiAgICAgICAgICAgICAgICBwYXJhbXMud2hlcmVBbmRNYXAgP1xuICAgICAgICAgICAgICAgICAgICBFeHByZXNzaW9uR3JvdXBKb2luZXIuQU5EIDpcbiAgICAgICAgICAgICAgICAgICAgRXhwcmVzc2lvbkdyb3VwSm9pbmVyLk9SXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHBhcmFtcy53aGVyZVByaW1hcnlLZXlzKSB7XG4gICAgICAgICAgICBjb25zdCBleHByZXNzaW9uczpTdHJpbmdTdGF0ZW1lbnRbXSA9IHBhcmFtcy53aGVyZVByaW1hcnlLZXlzLm1hcChcbiAgICAgICAgICAgICAgICAocHJpbWFyeUtleVZhbHVlKSA9PiBuZXcgU3RyaW5nU3RhdGVtZW50KFxuICAgICAgICAgICAgICAgICAgICBwYXJhbXMucmVwb3NpdG9yeS5wcmltYXJ5S2V5LFxuICAgICAgICAgICAgICAgICAgICAnJytwcmltYXJ5S2V5VmFsdWVcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApO1xuXG4gICAgICAgICAgICBwYXJhbXMud2hlcmVHcm91cCA9IG5ldyBPckV4cHJlc3Npb25Hcm91cChleHByZXNzaW9ucyk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAocGFyYW1zLndoZXJlQ2xhdXNlKSB7XG4gICAgICAgICAgICBwYXJhbXMud2hlcmVHcm91cCA9IG5ldyBTaW5nbGVFeHByZXNzaW9uKFxuICAgICAgICAgICAgICAgIHBhcmFtcy53aGVyZUNsYXVzZVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgY29uc3Qge3doZXJlR3JvdXAsb3JkZXJCeSxsaW1pdH0gPSBwYXJhbXM7XG5cbiAgICAgICAgbGV0IHF1ZXJ5ID0gJyc7XG5cbiAgICAgICAgY29uc3Qgd2hlcmVDbGF1c2UgPSBcbiAgICAgICAgICAgIHdoZXJlR3JvdXAgJiYgXG4gICAgICAgICAgICBhd2FpdCB0aGlzLnByZXBhcmVFeHByZXNzaW9uR3JvdXAod2hlcmVHcm91cCk7XG5cbiAgICAgICAgaWYgKHdoZXJlQ2xhdXNlKSB7XG4gICAgICAgICAgICBxdWVyeSArPSBgIFxuICAgICAgICAgICAgICAgIFdIRVJFICR7d2hlcmVDbGF1c2V9YDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChvcmRlckJ5KSB7XG4gICAgICAgICAgICBxdWVyeSArPSBgIFxuICAgICAgICAgICAgICAgIE9SREVSIEJZICR7b3JkZXJCeX1gO1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBpZiAobGltaXQpIHtcbiAgICAgICAgICAgIHF1ZXJ5ICs9IGAgXG4gICAgICAgICAgICAgICAgTElNSVQgJHtsaW1pdH1gO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHF1ZXJ5O1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgcHJlcGFyZUV4cHJlc3Npb25Hcm91cChncm91cDpFeHByZXNzaW9uR3JvdXApOlByb21pc2U8c3RyaW5nPiB7XG4gICAgICAgIGNvbnN0IHN0YXRlbWVudHM6c3RyaW5nW10gPSBbXTtcblxuICAgICAgICBjb25zdCB1c2VCcmFja2V0cyA9IGdyb3VwLnN1cnJvdW5kRXhwcmVzc2lvbldpdGhCcmFja2V0cztcblxuICAgICAgICBmb3IgKGNvbnN0IGV4cHJlc3Npb24gb2YgZ3JvdXAuZXhwcmVzc2lvbnMpIHtcbiAgICAgICAgICAgIGNvbnN0IHtsZWZ0U2lkZSxvcGVyYXRvcixyaWdodFNpZGV9ID0gZXhwcmVzc2lvbjtcblxuICAgICAgICAgICAgY29uc3QgcmlnaHRTaWRlVmFsdWUgPVxuICAgICAgICAgICAgICAgIHJpZ2h0U2lkZS5pc1N0cmluZyA/XG4gICAgICAgICAgICAgICAgICAgIGAkeyBhd2FpdCB0aGlzLmVzY2FwZShyaWdodFNpZGUuZGF0YSBhcyBzdHJpbmcpIH1gIDpcbiAgICAgICAgICAgICAgICAgICAgcmlnaHRTaWRlLmRhdGE7XG5cbiAgICAgICAgICAgIHN0YXRlbWVudHMucHVzaChcbiAgICAgICAgICAgICAgICBgJHsgXG4gICAgICAgICAgICAgICAgICAgIHVzZUJyYWNrZXRzID8gJygnIDogJydcbiAgICAgICAgICAgICAgICB9IFxcYCR7bGVmdFNpZGV9XFxgICR7b3BlcmF0b3J9ICR7cmlnaHRTaWRlVmFsdWV9ICR7XG4gICAgICAgICAgICAgICAgICAgIHVzZUJyYWNrZXRzID8gJyknIDogJydcbiAgICAgICAgICAgICAgICB9YCk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gc3RhdGVtZW50cy5qb2luKGAgJHtncm91cC5qb2luZXJ9IGApO1xuICAgIH1cblxuXG4gICAgcHJpdmF0ZSBhc3luYyBlc2NhcGUodmFsdWU6c3RyaW5nKTpQcm9taXNlPHN0cmluZz5cbiAgICB7XG4gICAgICAgIGNvbnN0IGNvbm5lY3Rpb24gPSBhd2FpdCB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyLmdldENvbm5lY3Rpb24oKTtcblxuICAgICAgICByZXR1cm4gY29ubmVjdGlvbi5lc2NhcGUodmFsdWUpO1xuICAgIH1cblxuICAgIHB1YmxpYyBkZWxldGUoXG4gICAgICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeSxcbiAgICAgICAgcHJpbWFyeUtleVZhbHVlczphbnlbXVxuICAgIClcbiAgICB7XG4gICAgICAgIGlmIChwcmltYXJ5S2V5VmFsdWVzLmxlbmd0aCA8IDEpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignUGxlYXNlIHNwZWNpZnkgd2hpY2ggcmVjb3JkcyB0byBkZWxldGUhJyk7XG4gICAgICAgIH1cblxuXG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gJ0RFTEVURSBGUk9NICcrcmVwb3NpdG9yeS50YWJsZU5hbWUrXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0cnVjdFdoZXJlV2l0aFByaW1hcnlLZXlzKHJlcG9zaXRvcnkscHJpbWFyeUtleVZhbHVlcykrJzsnO1xuXG4gICAgICAgIHJldHVybiBuZXcgTXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4ocXVlcnksdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxuXG4gICAgcmF3UXVlcnk8VD4oc3FsOiBzdHJpbmcpOiBNeVNRTFF1ZXJ5PFQ+IHtcbiAgICAgICAgcmV0dXJuIG5ldyBNeVNRTFF1ZXJ5PFQ+KHNxbCwgdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxufVxuXG5cbmZ1bmN0aW9uIGNvbnN0cnVjdFdoZXJlV2l0aFByaW1hcnlLZXlzKFxuICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeSxcbiAgICBwcmltYXJ5S2V5VmFsdWVzOmFueVtdXG4pOnN0cmluZ1xue1xuICAgIGNvbnN0IGNvbmRpdGlvbnM6c3RyaW5nW10gPSBbXTtcblxuICAgIGZvciAobGV0IHByaW1hcnlLZXlWYWx1ZSBvZiBwcmltYXJ5S2V5VmFsdWVzKSB7XG4gICAgICAgIGlmIChyZXBvc2l0b3J5LmlzUHJpbWFyeUtleUFTdHJpbmcpIHtcbiAgICAgICAgICAgIHByaW1hcnlLZXlWYWx1ZSA9IFwiJ1wiK3ByaW1hcnlLZXlWYWx1ZStcIidcIjtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGNvbmRpdGlvbiA9IHJlcG9zaXRvcnkucHJpbWFyeUtleSArICcgPSAnICsgcHJpbWFyeUtleVZhbHVlO1xuICAgICAgICBjb25kaXRpb25zLnB1c2goY29uZGl0aW9uKTtcbiAgICB9XG5cbiAgICByZXR1cm4gJyBXSEVSRSAnK2NvbmRpdGlvbnMuam9pbignIE9SICcpO1xufSJdfQ==