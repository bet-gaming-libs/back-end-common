/**
 * @fileoverview added by tsickle
 * Generated from: lib/db/engine/mysql/mysql-query.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/** @type {?} */
var logSqlQueries = process.env.LOG_SQL_QUERIES === 'true';
/**
 * @template T
 */
var /**
 * @template T
 */
MySQLQuery = /** @class */ (function () {
    function MySQLQuery(sql, connectionManager) {
        this.sql = sql;
        this.connectionManager = connectionManager;
    }
    /**
     * @return {?}
     */
    MySQLQuery.prototype.execute = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var connection;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // When execute is called directly,
                        // by default, it will use the connectionManager used to construct the query...
                        return [4 /*yield*/, this.connectionManager.getConnection()];
                    case 1:
                        connection = _a.sent();
                        return [2 /*return*/, new Promise((/**
                             * @param {?} resolve
                             * @param {?} reject
                             * @return {?}
                             */
                            function (resolve, reject) {
                                if (logSqlQueries) {
                                    console.log(_this.sql);
                                }
                                connection.query(_this.sql, (/**
                                 * @param {?} error
                                 * @param {?} result
                                 * @param {?} fields
                                 * @return {?}
                                 */
                                function (error, result, fields) {
                                    if (error) {
                                        //console.log('*** MySQL Error: ***');
                                        //console.log(err);
                                        //throw error;
                                        reject(error);
                                    }
                                    else {
                                        resolve(result);
                                    }
                                }));
                            }))];
                }
            });
        });
    };
    return MySQLQuery;
}());
/**
 * @template T
 */
export { MySQLQuery };
if (false) {
    /** @type {?} */
    MySQLQuery.prototype.sql;
    /**
     * @type {?}
     * @private
     */
    MySQLQuery.prototype.connectionManager;
}
var MySQLTransactionQuery = /** @class */ (function (_super) {
    tslib_1.__extends(MySQLTransactionQuery, _super);
    function MySQLTransactionQuery() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return MySQLTransactionQuery;
}(MySQLQuery));
export { MySQLTransactionQuery };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcXVlcnkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9kYi9lbmdpbmUvbXlzcWwvbXlzcWwtcXVlcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztJQUlNLGFBQWEsR0FDZixPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsS0FBSyxNQUFNOzs7O0FBRzFDOzs7O0lBRUksb0JBQ2EsR0FBVSxFQUNYLGlCQUF3QztRQUR2QyxRQUFHLEdBQUgsR0FBRyxDQUFPO1FBQ1gsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUF1QjtJQUVwRCxDQUFDOzs7O0lBRUssNEJBQU87OztJQUFiOzs7Ozs7Ozs7d0JBR3VCLHFCQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsRUFBQTs7d0JBQXpELFVBQVUsR0FBRyxTQUE0Qzt3QkFFL0Qsc0JBQU8sSUFBSSxPQUFPOzs7Ozs0QkFBSyxVQUFDLE9BQU8sRUFBQyxNQUFNO2dDQUNsQyxJQUFJLGFBQWEsRUFBRTtvQ0FDZixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQ0FDekI7Z0NBRUQsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsR0FBRzs7Ozs7O2dDQUFDLFVBQVMsS0FBSyxFQUFDLE1BQU0sRUFBQyxNQUFNO29DQUNsRCxJQUFJLEtBQUssRUFBRTt3Q0FDUCxzQ0FBc0M7d0NBQ3RDLG1CQUFtQjt3Q0FDbkIsY0FBYzt3Q0FDZCxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7cUNBQ2pCO3lDQUFNO3dDQUNILE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztxQ0FDbkI7Z0NBQ0wsQ0FBQyxFQUFDLENBQUM7NEJBQ1AsQ0FBQyxFQUFDLEVBQUM7Ozs7S0FDTjtJQUNMLGlCQUFDO0FBQUQsQ0FBQyxBQTlCRCxJQThCQzs7Ozs7OztJQTNCTyx5QkFBbUI7Ozs7O0lBQ25CLHVDQUFnRDs7QUE0QnhEO0lBQTJDLGlEQUE2QjtJQUF4RTs7SUFFQSxDQUFDO0lBQUQsNEJBQUM7QUFBRCxDQUFDLEFBRkQsQ0FBMkMsVUFBVSxHQUVwRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIgfSBmcm9tIFwiLi9teXNxbC1jb25uZWN0aW9uLW1hbmFnZXJcIjtcbmltcG9ydCB7IElNeVNRTFF1ZXJ5UmVzdWx0IH0gZnJvbSBcIi4vaW50ZXJmYWNlc1wiO1xuXG5cbmNvbnN0IGxvZ1NxbFF1ZXJpZXMgPSBcbiAgICBwcm9jZXNzLmVudi5MT0dfU1FMX1FVRVJJRVMgPT09ICd0cnVlJztcblxuXG5leHBvcnQgY2xhc3MgTXlTUUxRdWVyeTxUPlxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICByZWFkb25seSBzcWw6c3RyaW5nLFxuICAgICAgICBwcml2YXRlIGNvbm5lY3Rpb25NYW5hZ2VyOk15U1FMQ29ubmVjdGlvbk1hbmFnZXJcbiAgICApIHtcbiAgICB9XG5cbiAgICBhc3luYyBleGVjdXRlKCk6UHJvbWlzZTxUPiB7XG4gICAgICAgIC8vIFdoZW4gZXhlY3V0ZSBpcyBjYWxsZWQgZGlyZWN0bHksXG4gICAgICAgIC8vIGJ5IGRlZmF1bHQsIGl0IHdpbGwgdXNlIHRoZSBjb25uZWN0aW9uTWFuYWdlciB1c2VkIHRvIGNvbnN0cnVjdCB0aGUgcXVlcnkuLi5cbiAgICAgICAgY29uc3QgY29ubmVjdGlvbiA9IGF3YWl0IHRoaXMuY29ubmVjdGlvbk1hbmFnZXIuZ2V0Q29ubmVjdGlvbigpO1xuXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxUPiggKHJlc29sdmUscmVqZWN0KT0+e1xuICAgICAgICAgICAgaWYgKGxvZ1NxbFF1ZXJpZXMpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnNxbCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGNvbm5lY3Rpb24ucXVlcnkodGhpcy5zcWwsZnVuY3Rpb24oZXJyb3IscmVzdWx0LGZpZWxkcyl7XG4gICAgICAgICAgICAgICAgaWYgKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coJyoqKiBNeVNRTCBFcnJvcjogKioqJyk7XG4gICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coZXJyKTtcbiAgICAgICAgICAgICAgICAgICAgLy90aHJvdyBlcnJvcjtcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycm9yKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHJlc3VsdCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIE15U1FMVHJhbnNhY3Rpb25RdWVyeSBleHRlbmRzIE15U1FMUXVlcnk8SU15U1FMUXVlcnlSZXN1bHQ+XG57XG59Il19