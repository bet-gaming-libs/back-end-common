/**
 * @fileoverview added by tsickle
 * Generated from: lib/db/engine/mysql/mysql-repository.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * @template TypeForSelect
 */
var /**
 * @template TypeForSelect
 */
MySQLRepository = /** @class */ (function () {
    function MySQLRepository(db, _tableName, defaultFieldsToSelect, _primaryKey, isPrimaryKeyAString) {
        if (defaultFieldsToSelect === void 0) { defaultFieldsToSelect = []; }
        if (_primaryKey === void 0) { _primaryKey = 'id'; }
        if (isPrimaryKeyAString === void 0) { isPrimaryKeyAString = false; }
        this.db = db;
        this._tableName = _tableName;
        this.defaultFieldsToSelect = defaultFieldsToSelect;
        this._primaryKey = _primaryKey;
        this.isPrimaryKeyAString = isPrimaryKeyAString;
    }
    /**
     * @template T
     * @param {?} entity
     * @return {?}
     */
    MySQLRepository.prototype.insert = /**
     * @template T
     * @param {?} entity
     * @return {?}
     */
    function (entity) {
        return this.insertMany([entity]);
    };
    /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    MySQLRepository.prototype.insertMany = /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    function (entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildInsert(entities)];
                    case 1:
                        query = _a.sent();
                        return [4 /*yield*/, query.execute()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    MySQLRepository.prototype.buildInsert = /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    function (entities) {
        return this.db.builder.insert(this, entities);
    };
    /**
     * @param {?} primaryKeyValue
     * @param {?=} fields
     * @return {?}
     */
    MySQLRepository.prototype.selectOneByPrimaryKey = /**
     * @param {?} primaryKeyValue
     * @param {?=} fields
     * @return {?}
     */
    function (primaryKeyValue, fields) {
        if (fields === void 0) { fields = this.defaultFieldsToSelect; }
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var rows;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.selectManyByPrimaryKeys([primaryKeyValue], fields)];
                    case 1:
                        rows = _a.sent();
                        return [2 /*return*/, rows.length > 0 ?
                                rows[0] :
                                null];
                }
            });
        });
    };
    /**
     * @param {?} primaryKeyValues
     * @param {?=} fields
     * @return {?}
     */
    MySQLRepository.prototype.selectManyByPrimaryKeys = /**
     * @param {?} primaryKeyValues
     * @param {?=} fields
     * @return {?}
     */
    function (primaryKeyValues, fields) {
        if (fields === void 0) { fields = this.defaultFieldsToSelect; }
        return this.db.builder.selectByPrimaryKeys(this, fields, primaryKeyValues).execute();
    };
    /**
     * @param {?=} fields
     * @return {?}
     */
    MySQLRepository.prototype.selectAll = /**
     * @param {?=} fields
     * @return {?}
     */
    function (fields) {
        if (fields === void 0) { fields = this.defaultFieldsToSelect; }
        return this.db.builder.selectAll(this, fields).execute();
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.selectOne = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var rows;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.select(tslib_1.__assign({}, params, { limit: 1 }))];
                    case 1:
                        rows = _a.sent();
                        return [2 /*return*/, rows.length > 0 ?
                                rows[0] :
                                null];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.select = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildSelect(params)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, query.execute()];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.buildSelect = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var fields = params.fields ?
            params.fields :
            this.defaultFieldsToSelect;
        /** @type {?} */
        var newParams = tslib_1.__assign({}, params, { fields: fields, repository: this });
        return this.db.builder.select(newParams);
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.updateOne = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildUpdateOne(params)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, query.execute()];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.buildUpdateOne = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return this.buildUpdate(tslib_1.__assign({}, params, { limit: 1 }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.update = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildUpdate(params)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, query.execute()];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.buildUpdate = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var builderParams = tslib_1.__assign({}, params, { repository: this });
        return this.db.builder.update(builderParams);
    };
    /**
     * @param {?} primaryKeyValues
     * @return {?}
     */
    MySQLRepository.prototype.delete = /**
     * @param {?} primaryKeyValues
     * @return {?}
     */
    function (primaryKeyValues) {
        return this.db.builder.delete(this, primaryKeyValues).execute();
    };
    Object.defineProperty(MySQLRepository.prototype, "tableName", {
        get: /**
         * @return {?}
         */
        function () {
            return this._tableName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MySQLRepository.prototype, "primaryKey", {
        get: /**
         * @return {?}
         */
        function () {
            return this._primaryKey;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MySQLRepository.prototype, "primaryKeyFieldList", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return [this.primaryKey];
        },
        enumerable: true,
        configurable: true
    });
    return MySQLRepository;
}());
/**
 * @template TypeForSelect
 */
export { MySQLRepository };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    MySQLRepository.prototype.db;
    /**
     * @type {?}
     * @private
     */
    MySQLRepository.prototype._tableName;
    /**
     * @type {?}
     * @protected
     */
    MySQLRepository.prototype.defaultFieldsToSelect;
    /**
     * @type {?}
     * @private
     */
    MySQLRepository.prototype._primaryKey;
    /** @type {?} */
    MySQLRepository.prototype.isPrimaryKeyAString;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcmVwb3NpdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL2VuZ2luZS9teXNxbC9teXNxbC1yZXBvc2l0b3J5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQWtCQTs7OztJQUVJLHlCQUNjLEVBQWlCLEVBQ25CLFVBQWlCLEVBQ2YscUJBQW1DLEVBQ3JDLFdBQXlCLEVBQ3hCLG1CQUEyQjtRQUYxQixzQ0FBQSxFQUFBLDBCQUFtQztRQUNyQyw0QkFBQSxFQUFBLGtCQUF5QjtRQUN4QixvQ0FBQSxFQUFBLDJCQUEyQjtRQUoxQixPQUFFLEdBQUYsRUFBRSxDQUFlO1FBQ25CLGVBQVUsR0FBVixVQUFVLENBQU87UUFDZiwwQkFBcUIsR0FBckIscUJBQXFCLENBQWM7UUFDckMsZ0JBQVcsR0FBWCxXQUFXLENBQWM7UUFDeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFRO0lBRXhDLENBQUM7Ozs7OztJQUVNLGdDQUFNOzs7OztJQUFiLFVBQWlCLE1BQVE7UUFFckIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUNyQyxDQUFDOzs7Ozs7SUFDWSxvQ0FBVTs7Ozs7SUFBdkIsVUFBMkIsUUFBWTs7Ozs7NEJBRXJCLHFCQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEVBQUE7O3dCQUF4QyxLQUFLLEdBQUcsU0FBZ0M7d0JBQ3ZDLHFCQUFNLEtBQUssQ0FBQyxPQUFPLEVBQUUsRUFBQTs0QkFBNUIsc0JBQU8sU0FBcUIsRUFBQzs7OztLQUNoQzs7Ozs7O0lBRU0scUNBQVc7Ozs7O0lBQWxCLFVBQXNCLFFBQVk7UUFFOUIsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUksSUFBSSxFQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3BELENBQUM7Ozs7OztJQUdZLCtDQUFxQjs7Ozs7SUFBbEMsVUFDSSxlQUErQixFQUMvQixNQUE0QztRQUE1Qyx1QkFBQSxFQUFBLFNBQWtCLElBQUksQ0FBQyxxQkFBcUI7Ozs7OzRCQUUvQixxQkFBTSxJQUFJLENBQUMsdUJBQXVCLENBQzNDLENBQUMsZUFBZSxDQUFDLEVBQ2pCLE1BQU0sQ0FDVCxFQUFBOzt3QkFISyxJQUFJLEdBQUcsU0FHWjt3QkFFRCxzQkFBTyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dDQUNoQixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQ0FDVCxJQUFJLEVBQUM7Ozs7S0FDaEI7Ozs7OztJQUVNLGlEQUF1Qjs7Ozs7SUFBOUIsVUFDSSxnQkFBa0MsRUFDbEMsTUFBNEM7UUFBNUMsdUJBQUEsRUFBQSxTQUFrQixJQUFJLENBQUMscUJBQXFCO1FBRTVDLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQ3RDLElBQUksRUFBQyxNQUFNLEVBQUMsZ0JBQWdCLENBQy9CLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDaEIsQ0FBQzs7Ozs7SUFFTSxtQ0FBUzs7OztJQUFoQixVQUFpQixNQUE0QztRQUE1Qyx1QkFBQSxFQUFBLFNBQWtCLElBQUksQ0FBQyxxQkFBcUI7UUFDekQsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQzVCLElBQUksRUFDSixNQUFNLENBQ1QsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNoQixDQUFDOzs7OztJQUdZLG1DQUFTOzs7O0lBQXRCLFVBQXVCLE1BQXdDOzs7Ozs0QkFDOUMscUJBQU0sSUFBSSxDQUFDLE1BQU0sc0JBQ3ZCLE1BQU0sSUFDVCxLQUFLLEVBQUUsQ0FBQyxJQUNWLEVBQUE7O3dCQUhJLElBQUksR0FBRyxTQUdYO3dCQUVGLHNCQUFPLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0NBQ2hCLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dDQUNULElBQUksRUFBQzs7OztLQUNoQjs7Ozs7SUFFWSxnQ0FBTTs7OztJQUFuQixVQUFvQixNQUF3Qzs7Ozs7NEJBQzFDLHFCQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUF0QyxLQUFLLEdBQUcsU0FBOEI7d0JBRTVDLHNCQUFPLEtBQUssQ0FBQyxPQUFPLEVBQUUsRUFBQzs7OztLQUMxQjs7Ozs7SUFFTSxxQ0FBVzs7OztJQUFsQixVQUFtQixNQUF3Qzs7WUFDakQsTUFBTSxHQUNSLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNYLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxxQkFBcUI7O1lBRTVCLFNBQVMsd0JBQ1IsTUFBTSxJQUNULE1BQU0sUUFBQSxFQUNOLFVBQVUsRUFBRSxJQUFJLEdBQ25CO1FBRUQsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQWdCLFNBQVMsQ0FBQyxDQUFDO0lBQzVELENBQUM7Ozs7O0lBR1ksbUNBQVM7Ozs7SUFBdEIsVUFBdUIsTUFBd0M7Ozs7OzRCQUM3QyxxQkFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBekMsS0FBSyxHQUFHLFNBQWlDO3dCQUUvQyxzQkFBTyxLQUFLLENBQUMsT0FBTyxFQUFFLEVBQUM7Ozs7S0FDMUI7Ozs7O0lBQ00sd0NBQWM7Ozs7SUFBckIsVUFBc0IsTUFBd0M7UUFDMUQsT0FBTyxJQUFJLENBQUMsV0FBVyxzQkFDaEIsTUFBTSxJQUNULEtBQUssRUFBRSxDQUFDLElBQ1YsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRVksZ0NBQU07Ozs7SUFBbkIsVUFBb0IsTUFBd0M7Ozs7OzRCQUMxQyxxQkFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBdEMsS0FBSyxHQUFHLFNBQThCO3dCQUM1QyxzQkFBTyxLQUFLLENBQUMsT0FBTyxFQUFFLEVBQUM7Ozs7S0FDMUI7Ozs7O0lBQ00scUNBQVc7Ozs7SUFBbEIsVUFBbUIsTUFBd0M7O1lBQ2pELGFBQWEsd0JBQ1osTUFBTSxJQUNULFVBQVUsRUFBRSxJQUFJLEdBQ25CO1FBRUQsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDakQsQ0FBQzs7Ozs7SUFHTSxnQ0FBTTs7OztJQUFiLFVBQWMsZ0JBQWtDO1FBRTVDLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksRUFBQyxnQkFBZ0IsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ25FLENBQUM7SUFHRCxzQkFBVyxzQ0FBUzs7OztRQUFwQjtZQUVJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUMzQixDQUFDOzs7T0FBQTtJQUVELHNCQUFXLHVDQUFVOzs7O1FBQXJCO1lBRUksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzVCLENBQUM7OztPQUFBO0lBRUQsc0JBQVksZ0RBQW1COzs7OztRQUEvQjtZQUVJLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDN0IsQ0FBQzs7O09BQUE7SUFDTCxzQkFBQztBQUFELENBQUMsQUF6SUQsSUF5SUM7Ozs7Ozs7Ozs7SUF0SU8sNkJBQTJCOzs7OztJQUMzQixxQ0FBeUI7Ozs7O0lBQ3pCLGdEQUE2Qzs7Ozs7SUFDN0Msc0NBQWlDOztJQUNqQyw4Q0FBb0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICAgIElNeVNRTERhdGFiYXNlLFxuICAgIElNeVNRTFJlcG9zaXRvcnksXG4gICAgUHJpbWFyeUtleVZhbHVlLFxuICAgIElNeVNRTFF1ZXJ5UmVzdWx0LFxuICAgIElTZWxlY3RRdWVyeUJ1aWxkZXJQYXJhbXMsXG4gICAgSVNlbGVjdFF1ZXJ5UGFyYW1zLFxuICAgIElVcGRhdGVRdWVyeVBhcmFtcyxcbiAgICBJVXBkYXRlUXVlcnlCdWlsZGVyUGFyYW1zLFxufSBmcm9tICcuL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgTXlTUUxRdWVyeSB9IGZyb20gJy4vbXlzcWwtcXVlcnknO1xuXG5cblxuZXhwb3J0IHR5cGUgU2VsZWN0T25lSGFuZGxlcjxUPiA9IChyb3dzOlRbXSkgPT4gdm9pZDtcblxuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTFJlcG9zaXRvcnk8VHlwZUZvclNlbGVjdD4gaW1wbGVtZW50cyBJTXlTUUxSZXBvc2l0b3J5XG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByb3RlY3RlZCBkYjpJTXlTUUxEYXRhYmFzZSxcbiAgICAgICAgcHJpdmF0ZSBfdGFibGVOYW1lOnN0cmluZyxcbiAgICAgICAgcHJvdGVjdGVkIGRlZmF1bHRGaWVsZHNUb1NlbGVjdDpzdHJpbmdbXSA9IFtdLFxuICAgICAgICBwcml2YXRlIF9wcmltYXJ5S2V5OnN0cmluZyA9ICdpZCcsXG4gICAgICAgIHJlYWRvbmx5IGlzUHJpbWFyeUtleUFTdHJpbmcgPSBmYWxzZVxuICAgICkge1xuICAgIH1cblxuICAgIHB1YmxpYyBpbnNlcnQ8VD4oZW50aXR5OlQpXG4gICAge1xuICAgICAgICByZXR1cm4gdGhpcy5pbnNlcnRNYW55KFtlbnRpdHldKTtcbiAgICB9XG4gICAgcHVibGljIGFzeW5jIGluc2VydE1hbnk8VD4oZW50aXRpZXM6VFtdKVxuICAgIHtcbiAgICAgICAgY29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLmJ1aWxkSW5zZXJ0KGVudGl0aWVzKTtcbiAgICAgICAgcmV0dXJuIGF3YWl0IHF1ZXJ5LmV4ZWN1dGUoKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgYnVpbGRJbnNlcnQ8VD4oZW50aXRpZXM6VFtdKTpQcm9taXNlPE15U1FMUXVlcnk8SU15U1FMUXVlcnlSZXN1bHQ+PlxuICAgIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGIuYnVpbGRlci5pbnNlcnQ8VD4odGhpcyxlbnRpdGllcyk7XG4gICAgfVxuXG5cbiAgICBwdWJsaWMgYXN5bmMgc2VsZWN0T25lQnlQcmltYXJ5S2V5KFxuICAgICAgICBwcmltYXJ5S2V5VmFsdWU6UHJpbWFyeUtleVZhbHVlLFxuICAgICAgICBmaWVsZHM6c3RyaW5nW10gPSB0aGlzLmRlZmF1bHRGaWVsZHNUb1NlbGVjdFxuICAgICk6UHJvbWlzZTxUeXBlRm9yU2VsZWN0PiB7XG4gICAgICAgIGNvbnN0IHJvd3MgPSBhd2FpdCB0aGlzLnNlbGVjdE1hbnlCeVByaW1hcnlLZXlzKFxuICAgICAgICAgICAgW3ByaW1hcnlLZXlWYWx1ZV0sXG4gICAgICAgICAgICBmaWVsZHNcbiAgICAgICAgKTtcblxuICAgICAgICByZXR1cm4gcm93cy5sZW5ndGggPiAwID9cbiAgICAgICAgICAgICAgICByb3dzWzBdIDpcbiAgICAgICAgICAgICAgICBudWxsO1xuICAgIH1cblxuICAgIHB1YmxpYyBzZWxlY3RNYW55QnlQcmltYXJ5S2V5cyhcbiAgICAgICAgcHJpbWFyeUtleVZhbHVlczpQcmltYXJ5S2V5VmFsdWVbXSxcbiAgICAgICAgZmllbGRzOnN0cmluZ1tdID0gdGhpcy5kZWZhdWx0RmllbGRzVG9TZWxlY3RcbiAgICApIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGIuYnVpbGRlci5zZWxlY3RCeVByaW1hcnlLZXlzPFR5cGVGb3JTZWxlY3Q+KFxuICAgICAgICAgICAgdGhpcyxmaWVsZHMscHJpbWFyeUtleVZhbHVlc1xuICAgICAgICApLmV4ZWN1dGUoKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc2VsZWN0QWxsKGZpZWxkczpzdHJpbmdbXSA9IHRoaXMuZGVmYXVsdEZpZWxkc1RvU2VsZWN0KSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRiLmJ1aWxkZXIuc2VsZWN0QWxsPFR5cGVGb3JTZWxlY3Q+KFxuICAgICAgICAgICAgdGhpcyxcbiAgICAgICAgICAgIGZpZWxkc1xuICAgICAgICApLmV4ZWN1dGUoKTtcbiAgICB9XG5cbiAgIFxuICAgIHB1YmxpYyBhc3luYyBzZWxlY3RPbmUocGFyYW1zOklTZWxlY3RRdWVyeVBhcmFtczxUeXBlRm9yU2VsZWN0Pikge1xuICAgICAgICBjb25zdCByb3dzID0gYXdhaXQgdGhpcy5zZWxlY3Qoe1xuICAgICAgICAgICAgLi4ucGFyYW1zLFxuICAgICAgICAgICAgbGltaXQ6IDFcbiAgICAgICAgfSk7XG5cbiAgICAgICAgcmV0dXJuIHJvd3MubGVuZ3RoID4gMCA/XG4gICAgICAgICAgICAgICAgcm93c1swXSA6XG4gICAgICAgICAgICAgICAgbnVsbDtcbiAgICB9XG5cbiAgICBwdWJsaWMgYXN5bmMgc2VsZWN0KHBhcmFtczpJU2VsZWN0UXVlcnlQYXJhbXM8VHlwZUZvclNlbGVjdD4pIHtcbiAgICAgICAgY29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLmJ1aWxkU2VsZWN0KHBhcmFtcyk7XG5cbiAgICAgICAgcmV0dXJuIHF1ZXJ5LmV4ZWN1dGUoKTtcbiAgICB9XG4gICAgXG4gICAgcHVibGljIGJ1aWxkU2VsZWN0KHBhcmFtczpJU2VsZWN0UXVlcnlQYXJhbXM8VHlwZUZvclNlbGVjdD4pIHtcbiAgICAgICAgY29uc3QgZmllbGRzID1cbiAgICAgICAgICAgIHBhcmFtcy5maWVsZHMgP1xuICAgICAgICAgICAgICAgIHBhcmFtcy5maWVsZHMgOlxuICAgICAgICAgICAgICAgIHRoaXMuZGVmYXVsdEZpZWxkc1RvU2VsZWN0O1xuXG4gICAgICAgIGNvbnN0IG5ld1BhcmFtczpJU2VsZWN0UXVlcnlCdWlsZGVyUGFyYW1zPFR5cGVGb3JTZWxlY3Q+ID0ge1xuICAgICAgICAgICAgLi4ucGFyYW1zLFxuICAgICAgICAgICAgZmllbGRzLFxuICAgICAgICAgICAgcmVwb3NpdG9yeTogdGhpc1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuZGIuYnVpbGRlci5zZWxlY3Q8VHlwZUZvclNlbGVjdD4obmV3UGFyYW1zKTtcbiAgICB9XG5cblxuICAgIHB1YmxpYyBhc3luYyB1cGRhdGVPbmUocGFyYW1zOklVcGRhdGVRdWVyeVBhcmFtczxUeXBlRm9yU2VsZWN0Pikge1xuICAgICAgICBjb25zdCBxdWVyeSA9IGF3YWl0IHRoaXMuYnVpbGRVcGRhdGVPbmUocGFyYW1zKTtcblxuICAgICAgICByZXR1cm4gcXVlcnkuZXhlY3V0ZSgpO1xuICAgIH1cbiAgICBwdWJsaWMgYnVpbGRVcGRhdGVPbmUocGFyYW1zOklVcGRhdGVRdWVyeVBhcmFtczxUeXBlRm9yU2VsZWN0Pikge1xuICAgICAgICByZXR1cm4gdGhpcy5idWlsZFVwZGF0ZSh7XG4gICAgICAgICAgICAuLi5wYXJhbXMsXG4gICAgICAgICAgICBsaW1pdDogMVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgYXN5bmMgdXBkYXRlKHBhcmFtczpJVXBkYXRlUXVlcnlQYXJhbXM8VHlwZUZvclNlbGVjdD4pIHtcbiAgICAgICAgY29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLmJ1aWxkVXBkYXRlKHBhcmFtcyk7XG4gICAgICAgIHJldHVybiBxdWVyeS5leGVjdXRlKCk7XG4gICAgfVxuICAgIHB1YmxpYyBidWlsZFVwZGF0ZShwYXJhbXM6SVVwZGF0ZVF1ZXJ5UGFyYW1zPFR5cGVGb3JTZWxlY3Q+KSB7XG4gICAgICAgIGNvbnN0IGJ1aWxkZXJQYXJhbXM6SVVwZGF0ZVF1ZXJ5QnVpbGRlclBhcmFtczxUeXBlRm9yU2VsZWN0PiA9IHtcbiAgICAgICAgICAgIC4uLnBhcmFtcyxcbiAgICAgICAgICAgIHJlcG9zaXRvcnk6IHRoaXNcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLmRiLmJ1aWxkZXIudXBkYXRlKGJ1aWxkZXJQYXJhbXMpO1xuICAgIH1cblxuXG4gICAgcHVibGljIGRlbGV0ZShwcmltYXJ5S2V5VmFsdWVzOlByaW1hcnlLZXlWYWx1ZVtdKVxuICAgIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGIuYnVpbGRlci5kZWxldGUodGhpcyxwcmltYXJ5S2V5VmFsdWVzKS5leGVjdXRlKCk7XG4gICAgfVxuXG5cbiAgICBwdWJsaWMgZ2V0IHRhYmxlTmFtZSgpOnN0cmluZ1xuICAgIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX3RhYmxlTmFtZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IHByaW1hcnlLZXkoKTpzdHJpbmdcbiAgICB7XG4gICAgICAgIHJldHVybiB0aGlzLl9wcmltYXJ5S2V5O1xuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0IHByaW1hcnlLZXlGaWVsZExpc3QoKTpzdHJpbmdbXVxuICAgIHtcbiAgICAgICAgcmV0dXJuIFt0aGlzLnByaW1hcnlLZXldO1xuICAgIH1cbn0iXX0=