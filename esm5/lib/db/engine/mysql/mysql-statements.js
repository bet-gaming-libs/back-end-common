/**
 * @fileoverview added by tsickle
 * Generated from: lib/db/engine/mysql/mysql-statements.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/** @enum {number} */
var MySQLValueType = {
    EXPRESSION: 0,
    STRING: 1,
};
export { MySQLValueType };
MySQLValueType[MySQLValueType.EXPRESSION] = 'EXPRESSION';
MySQLValueType[MySQLValueType.STRING] = 'STRING';
/** @enum {string} */
var ExpressionOperator = {
    EQUAL: "=",
    NOT_EQUAL: "!=",
    LESS_THAN: "<",
    LESS_THAN_OR_EQUAL: "<=",
    GREATER_THAN: ">",
    GREATER_THAN_OR_EQUAL: ">=",
    IS: "IS",
    IS_NOT: "IS NOT",
};
export { ExpressionOperator };
/** @enum {string} */
var ExpressionGroupJoiner = {
    AND: "AND",
    OR: "OR",
    COMMA: ",",
};
export { ExpressionGroupJoiner };
/**
 * @record
 */
export function IMySQLValue() { }
if (false) {
    /** @type {?} */
    IMySQLValue.prototype.data;
    /** @type {?} */
    IMySQLValue.prototype.isString;
}
var MySQLValue = /** @class */ (function () {
    function MySQLValue(data, type) {
        this.data = data;
        this.type = type;
        this.isString =
            type == MySQLValueType.STRING;
    }
    return MySQLValue;
}());
export { MySQLValue };
if (false) {
    /** @type {?} */
    MySQLValue.prototype.isString;
    /** @type {?} */
    MySQLValue.prototype.data;
    /** @type {?} */
    MySQLValue.prototype.type;
}
var MySQLNumberValue = /** @class */ (function (_super) {
    tslib_1.__extends(MySQLNumberValue, _super);
    function MySQLNumberValue(data) {
        return _super.call(this, data, MySQLValueType.EXPRESSION) || this;
    }
    return MySQLNumberValue;
}(MySQLValue));
var MySQLStringValue = /** @class */ (function (_super) {
    tslib_1.__extends(MySQLStringValue, _super);
    function MySQLStringValue(data) {
        return _super.call(this, data, MySQLValueType.STRING) || this;
    }
    return MySQLStringValue;
}(MySQLValue));
var MySQLStringExpressionValue = /** @class */ (function (_super) {
    tslib_1.__extends(MySQLStringExpressionValue, _super);
    function MySQLStringExpressionValue(data) {
        return _super.call(this, data, MySQLValueType.EXPRESSION) || this;
    }
    return MySQLStringExpressionValue;
}(MySQLValue));
/** @enum {string} */
var MySQLExpression = {
    NOW: "NOW()",
    NULL: "NULL",
};
export { MySQLExpression };
/** @type {?} */
var nowValue = new MySQLStringExpressionValue(MySQLExpression.NOW);
/** @type {?} */
var nullValue = new MySQLStringExpressionValue(MySQLExpression.NULL);
/*
class MySQLValue
{
    static booleanAsInt(bool:boolean):number
    {
        return bool ? 1 : 0;
    }
}
*/
/**
 * @abstract
 */
var /*
class MySQLValue
{
    static booleanAsInt(bool:boolean):number
    {
        return bool ? 1 : 0;
    }
}
*/
/**
 * @abstract
 */
Expression = /** @class */ (function () {
    function Expression(leftSide, operator, rightSide) {
        this.leftSide = leftSide;
        this.operator = operator;
        this.rightSide = rightSide;
    }
    return Expression;
}());
/*
class MySQLValue
{
    static booleanAsInt(bool:boolean):number
    {
        return bool ? 1 : 0;
    }
}
*/
/**
 * @abstract
 */
export { Expression };
if (false) {
    /** @type {?} */
    Expression.prototype.leftSide;
    /** @type {?} */
    Expression.prototype.operator;
    /** @type {?} */
    Expression.prototype.rightSide;
}
var ExpressionGroup = /** @class */ (function () {
    function ExpressionGroup(expressions, joiner, surroundExpressionWithBrackets) {
        if (surroundExpressionWithBrackets === void 0) { surroundExpressionWithBrackets = true; }
        this.expressions = expressions;
        this.joiner = joiner;
        this.surroundExpressionWithBrackets = surroundExpressionWithBrackets;
    }
    return ExpressionGroup;
}());
export { ExpressionGroup };
if (false) {
    /** @type {?} */
    ExpressionGroup.prototype.expressions;
    /** @type {?} */
    ExpressionGroup.prototype.joiner;
    /** @type {?} */
    ExpressionGroup.prototype.surroundExpressionWithBrackets;
}
/**
 * @param {?} map
 * @param {?} joiner
 * @return {?}
 */
export function createExpressionGroup(map, joiner) {
    /** @type {?} */
    var expressions = [];
    // should we use Object.keys()?
    for (var fieldName in map) {
        /** @type {?} */
        var fieldValue = map[fieldName];
        expressions.push(typeof fieldValue == 'string' ?
            new StringStatement(fieldName, (/** @type {?} */ (fieldValue))) :
            new NumberStatement(fieldName, fieldValue));
    }
    return new ExpressionGroup(expressions, joiner);
}
var SingleExpression = /** @class */ (function (_super) {
    tslib_1.__extends(SingleExpression, _super);
    function SingleExpression(expression) {
        return _super.call(this, [expression], undefined) || this;
    }
    return SingleExpression;
}(ExpressionGroup));
export { SingleExpression };
var AndExpressionGroup = /** @class */ (function (_super) {
    tslib_1.__extends(AndExpressionGroup, _super);
    function AndExpressionGroup(expressions) {
        return _super.call(this, expressions, ExpressionGroupJoiner.AND) || this;
    }
    return AndExpressionGroup;
}(ExpressionGroup));
export { AndExpressionGroup };
var OrExpressionGroup = /** @class */ (function (_super) {
    tslib_1.__extends(OrExpressionGroup, _super);
    function OrExpressionGroup(expressions) {
        return _super.call(this, expressions, ExpressionGroupJoiner.OR) || this;
    }
    return OrExpressionGroup;
}(ExpressionGroup));
export { OrExpressionGroup };
var UpdateStatements = /** @class */ (function (_super) {
    tslib_1.__extends(UpdateStatements, _super);
    function UpdateStatements(expressions) {
        return _super.call(this, expressions, ExpressionGroupJoiner.COMMA, false) || this;
    }
    return UpdateStatements;
}(ExpressionGroup));
export { UpdateStatements };
/**
 * @abstract
 */
var /**
 * @abstract
 */
FieldStatement = /** @class */ (function (_super) {
    tslib_1.__extends(FieldStatement, _super);
    function FieldStatement(fieldName, value, operator) {
        return _super.call(this, fieldName, operator, value) || this;
    }
    return FieldStatement;
}(Expression));
/**
 * @abstract
 */
export { FieldStatement };
var NumberStatement = /** @class */ (function (_super) {
    tslib_1.__extends(NumberStatement, _super);
    function NumberStatement(fieldName, data, operator) {
        if (operator === void 0) { operator = ExpressionOperator.EQUAL; }
        return _super.call(this, fieldName, new MySQLNumberValue(data), operator) || this;
    }
    return NumberStatement;
}(FieldStatement));
export { NumberStatement };
var StringStatement = /** @class */ (function (_super) {
    tslib_1.__extends(StringStatement, _super);
    function StringStatement(fieldName, data, operator) {
        if (operator === void 0) { operator = ExpressionOperator.EQUAL; }
        return _super.call(this, fieldName, new MySQLStringValue(data), operator) || this;
    }
    return StringStatement;
}(FieldStatement));
export { StringStatement };
var StringExpressionStatement = /** @class */ (function (_super) {
    tslib_1.__extends(StringExpressionStatement, _super);
    function StringExpressionStatement(fieldName, data, operator) {
        if (operator === void 0) { operator = ExpressionOperator.EQUAL; }
        return _super.call(this, fieldName, new MySQLStringExpressionValue(data), operator) || this;
    }
    return StringExpressionStatement;
}(FieldStatement));
export { StringExpressionStatement };
var NullUpdateStatement = /** @class */ (function (_super) {
    tslib_1.__extends(NullUpdateStatement, _super);
    function NullUpdateStatement(fieldName) {
        return _super.call(this, fieldName, nullValue, ExpressionOperator.EQUAL) || this;
    }
    return NullUpdateStatement;
}(FieldStatement));
export { NullUpdateStatement };
var NowUpdateStatement = /** @class */ (function (_super) {
    tslib_1.__extends(NowUpdateStatement, _super);
    function NowUpdateStatement(fieldName) {
        return _super.call(this, fieldName, nowValue, ExpressionOperator.EQUAL) || this;
    }
    return NowUpdateStatement;
}(FieldStatement));
export { NowUpdateStatement };
var FieldIsNullStatement = /** @class */ (function (_super) {
    tslib_1.__extends(FieldIsNullStatement, _super);
    function FieldIsNullStatement(fieldName) {
        return _super.call(this, fieldName, nullValue, ExpressionOperator.IS) || this;
    }
    return FieldIsNullStatement;
}(FieldStatement));
export { FieldIsNullStatement };
var FieldIsNotNullStatement = /** @class */ (function (_super) {
    tslib_1.__extends(FieldIsNotNullStatement, _super);
    function FieldIsNotNullStatement(fieldName) {
        return _super.call(this, fieldName, nullValue, ExpressionOperator.IS_NOT) || this;
    }
    return FieldIsNotNullStatement;
}(FieldStatement));
export { FieldIsNotNullStatement };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtc3RhdGVtZW50cy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL2VuZ2luZS9teXNxbC9teXNxbC1zdGF0ZW1lbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQSxJQUFZLGNBQWM7SUFFdEIsVUFBVSxHQUFBO0lBQ1YsTUFBTSxHQUFBO0VBQ1Q7Ozs7O0FBRUQsSUFBWSxrQkFBa0I7SUFFMUIsS0FBSyxLQUFzQjtJQUMzQixTQUFTLE1BQW1CO0lBRTVCLFNBQVMsS0FBa0I7SUFDM0Isa0JBQWtCLE1BQVU7SUFDNUIsWUFBWSxLQUFlO0lBQzNCLHFCQUFxQixNQUFPO0lBRTVCLEVBQUUsTUFBMEI7SUFDNUIsTUFBTSxVQUEwQjtFQUNuQzs7O0FBRUQsSUFBWSxxQkFBcUI7SUFFN0IsR0FBRyxPQUFVO0lBQ2IsRUFBRSxNQUFVO0lBQ1osS0FBSyxLQUFNO0VBQ2Q7Ozs7O0FBZUQsaUNBSUM7OztJQUZHLDJCQUF5Qjs7SUFDekIsK0JBQWlCOztBQUtyQjtJQUlJLG9CQUNhLElBQXdCLEVBQ3hCLElBQW1CO1FBRG5CLFNBQUksR0FBSixJQUFJLENBQW9CO1FBQ3hCLFNBQUksR0FBSixJQUFJLENBQWU7UUFFNUIsSUFBSSxDQUFDLFFBQVE7WUFDVCxJQUFJLElBQUksY0FBYyxDQUFDLE1BQU0sQ0FBQztJQUN0QyxDQUFDO0lBQ0wsaUJBQUM7QUFBRCxDQUFDLEFBWEQsSUFXQzs7OztJQVRHLDhCQUEwQjs7SUFHdEIsMEJBQWlDOztJQUNqQywwQkFBNEI7O0FBT3BDO0lBQStCLDRDQUFVO0lBRXJDLDBCQUFZLElBQVc7ZUFDbkIsa0JBQU0sSUFBSSxFQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUM7SUFDekMsQ0FBQztJQUNMLHVCQUFDO0FBQUQsQ0FBQyxBQUxELENBQStCLFVBQVUsR0FLeEM7QUFFRDtJQUErQiw0Q0FBVTtJQUVyQywwQkFBWSxJQUFXO2VBQ25CLGtCQUFNLElBQUksRUFBQyxjQUFjLENBQUMsTUFBTSxDQUFDO0lBQ3JDLENBQUM7SUFDTCx1QkFBQztBQUFELENBQUMsQUFMRCxDQUErQixVQUFVLEdBS3hDO0FBRUQ7SUFBeUMsc0RBQVU7SUFFL0Msb0NBQVksSUFBVztlQUNuQixrQkFBTSxJQUFJLEVBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQztJQUN6QyxDQUFDO0lBQ0wsaUNBQUM7QUFBRCxDQUFDLEFBTEQsQ0FBeUMsVUFBVSxHQUtsRDs7QUFHRCxJQUFZLGVBQWU7SUFDdkIsR0FBRyxTQUFVO0lBQ2IsSUFBSSxRQUFTO0VBQ2hCOzs7SUFFSyxRQUFRLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDOztJQUM5RCxTQUFTLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDOzs7Ozs7Ozs7Ozs7O0FBZXRFOzs7Ozs7Ozs7Ozs7O0lBRUksb0JBQ2EsUUFBZSxFQUNmLFFBQWUsRUFDZixTQUFvQjtRQUZwQixhQUFRLEdBQVIsUUFBUSxDQUFPO1FBQ2YsYUFBUSxHQUFSLFFBQVEsQ0FBTztRQUNmLGNBQVMsR0FBVCxTQUFTLENBQVc7SUFFakMsQ0FBQztJQUNMLGlCQUFDO0FBQUQsQ0FBQyxBQVJELElBUUM7Ozs7Ozs7Ozs7Ozs7Ozs7SUFMTyw4QkFBd0I7O0lBQ3hCLDhCQUF3Qjs7SUFDeEIsK0JBQTZCOztBQU1yQztJQUVJLHlCQUNhLFdBQXdCLEVBQ3hCLE1BQTRCLEVBQzVCLDhCQUE2QztRQUE3QywrQ0FBQSxFQUFBLHFDQUE2QztRQUY3QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixXQUFNLEdBQU4sTUFBTSxDQUFzQjtRQUM1QixtQ0FBOEIsR0FBOUIsOEJBQThCLENBQWU7SUFFMUQsQ0FBQztJQUNMLHNCQUFDO0FBQUQsQ0FBQyxBQVJELElBUUM7Ozs7SUFMTyxzQ0FBaUM7O0lBQ2pDLGlDQUFxQzs7SUFDckMseURBQXNEOzs7Ozs7O0FBTTlELE1BQU0sVUFBVSxxQkFBcUIsQ0FDakMsR0FBTyxFQUFDLE1BQTRCOztRQUU5QixXQUFXLEdBQWdCLEVBQUU7SUFFbkMsK0JBQStCO0lBQy9CLEtBQUssSUFBTSxTQUFTLElBQUksR0FBRyxFQUFFOztZQUNuQixVQUFVLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQztRQUVqQyxXQUFXLENBQUMsSUFBSSxDQUNaLE9BQU8sVUFBVSxJQUFJLFFBQVEsQ0FBQyxDQUFDO1lBQzNCLElBQUksZUFBZSxDQUNmLFNBQVMsRUFDVCxtQkFBQSxVQUFVLEVBQVUsQ0FDdkIsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxlQUFlLENBQ2YsU0FBUyxFQUNULFVBQVUsQ0FDYixDQUNSLENBQUM7S0FDTDtJQUVELE9BQU8sSUFBSSxlQUFlLENBQUMsV0FBVyxFQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ25ELENBQUM7QUFHRDtJQUFzQyw0Q0FBZTtJQUVqRCwwQkFBWSxVQUFxQjtlQUM3QixrQkFBTSxDQUFDLFVBQVUsQ0FBQyxFQUFDLFNBQVMsQ0FBQztJQUNqQyxDQUFDO0lBQ0wsdUJBQUM7QUFBRCxDQUFDLEFBTEQsQ0FBc0MsZUFBZSxHQUtwRDs7QUFDRDtJQUF3Qyw4Q0FBZTtJQUVuRCw0QkFBWSxXQUF3QjtlQUNoQyxrQkFBTSxXQUFXLEVBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDO0lBQ2hELENBQUM7SUFDTCx5QkFBQztBQUFELENBQUMsQUFMRCxDQUF3QyxlQUFlLEdBS3REOztBQUNEO0lBQXVDLDZDQUFlO0lBRWxELDJCQUFZLFdBQXdCO2VBQ2hDLGtCQUFNLFdBQVcsRUFBQyxxQkFBcUIsQ0FBQyxFQUFFLENBQUM7SUFDL0MsQ0FBQztJQUNMLHdCQUFDO0FBQUQsQ0FBQyxBQUxELENBQXVDLGVBQWUsR0FLckQ7O0FBRUQ7SUFBc0MsNENBQWU7SUFFakQsMEJBQVksV0FBd0I7ZUFDaEMsa0JBQU0sV0FBVyxFQUFDLHFCQUFxQixDQUFDLEtBQUssRUFBQyxLQUFLLENBQUM7SUFDeEQsQ0FBQztJQUNMLHVCQUFDO0FBQUQsQ0FBQyxBQUxELENBQXNDLGVBQWUsR0FLcEQ7Ozs7O0FBSUQ7Ozs7SUFBNkMsMENBQVU7SUFFbkQsd0JBQ0ksU0FBZ0IsRUFDaEIsS0FBZ0IsRUFDaEIsUUFBMkI7ZUFFM0Isa0JBQU0sU0FBUyxFQUFDLFFBQVEsRUFBQyxLQUFLLENBQUM7SUFDbkMsQ0FBQztJQUNMLHFCQUFDO0FBQUQsQ0FBQyxBQVRELENBQTZDLFVBQVUsR0FTdEQ7Ozs7O0FBR0Q7SUFBcUMsMkNBQWM7SUFFL0MseUJBQ0ksU0FBZ0IsRUFDaEIsSUFBVyxFQUNYLFFBQXNEO1FBQXRELHlCQUFBLEVBQUEsV0FBOEIsa0JBQWtCLENBQUMsS0FBSztlQUV0RCxrQkFDSSxTQUFTLEVBQ1QsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFDMUIsUUFBUSxDQUNYO0lBQ0wsQ0FBQztJQUNMLHNCQUFDO0FBQUQsQ0FBQyxBQWJELENBQXFDLGNBQWMsR0FhbEQ7O0FBRUQ7SUFBcUMsMkNBQWM7SUFFL0MseUJBQ0ksU0FBZ0IsRUFDaEIsSUFBVyxFQUNYLFFBQXNEO1FBQXRELHlCQUFBLEVBQUEsV0FBOEIsa0JBQWtCLENBQUMsS0FBSztlQUV0RCxrQkFDSSxTQUFTLEVBQ1QsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFDMUIsUUFBUSxDQUNYO0lBQ0wsQ0FBQztJQUNMLHNCQUFDO0FBQUQsQ0FBQyxBQWJELENBQXFDLGNBQWMsR0FhbEQ7O0FBRUQ7SUFBK0MscURBQWM7SUFFekQsbUNBQ0ksU0FBZ0IsRUFDaEIsSUFBVyxFQUNYLFFBQXNEO1FBQXRELHlCQUFBLEVBQUEsV0FBOEIsa0JBQWtCLENBQUMsS0FBSztlQUV0RCxrQkFDSSxTQUFTLEVBQ1QsSUFBSSwwQkFBMEIsQ0FBQyxJQUFJLENBQUMsRUFDcEMsUUFBUSxDQUNYO0lBQ0wsQ0FBQztJQUNMLGdDQUFDO0FBQUQsQ0FBQyxBQWJELENBQStDLGNBQWMsR0FhNUQ7O0FBR0Q7SUFBeUMsK0NBQWM7SUFFbkQsNkJBQVksU0FBZ0I7ZUFDeEIsa0JBQU0sU0FBUyxFQUFDLFNBQVMsRUFBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUM7SUFDdkQsQ0FBQztJQUNMLDBCQUFDO0FBQUQsQ0FBQyxBQUxELENBQXlDLGNBQWMsR0FLdEQ7O0FBRUQ7SUFBd0MsOENBQWM7SUFFbEQsNEJBQVksU0FBZ0I7ZUFDeEIsa0JBQU0sU0FBUyxFQUFDLFFBQVEsRUFBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUM7SUFDdEQsQ0FBQztJQUNMLHlCQUFDO0FBQUQsQ0FBQyxBQUxELENBQXdDLGNBQWMsR0FLckQ7O0FBR0Q7SUFBMEMsZ0RBQWM7SUFFcEQsOEJBQVksU0FBZ0I7ZUFDeEIsa0JBQU0sU0FBUyxFQUFDLFNBQVMsRUFBQyxrQkFBa0IsQ0FBQyxFQUFFLENBQUM7SUFDcEQsQ0FBQztJQUNMLDJCQUFDO0FBQUQsQ0FBQyxBQUxELENBQTBDLGNBQWMsR0FLdkQ7O0FBRUQ7SUFBNkMsbURBQWM7SUFFdkQsaUNBQVksU0FBZ0I7ZUFDeEIsa0JBQU0sU0FBUyxFQUFDLFNBQVMsRUFBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUM7SUFDeEQsQ0FBQztJQUNMLDhCQUFDO0FBQUQsQ0FBQyxBQUxELENBQTZDLGNBQWMsR0FLMUQiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBNeVNRTFZhbHVlVHlwZVxue1xuICAgIEVYUFJFU1NJT04sXG4gICAgU1RSSU5HXG59XG5cbmV4cG9ydCBlbnVtIEV4cHJlc3Npb25PcGVyYXRvclxue1xuICAgIEVRVUFMICAgICAgICAgICAgICAgICA9ICc9JyxcbiAgICBOT1RfRVFVQUwgICAgICAgICAgICAgPSAnIT0nLFxuXG4gICAgTEVTU19USEFOICAgICAgICAgICAgID0gJzwnLFxuICAgIExFU1NfVEhBTl9PUl9FUVVBTCAgICA9ICc8PScsXG4gICAgR1JFQVRFUl9USEFOICAgICAgICAgID0gJz4nLFxuICAgIEdSRUFURVJfVEhBTl9PUl9FUVVBTCA9ICc+PScsXG5cbiAgICBJUyAgICAgICAgICAgICAgICAgICAgPSAnSVMnLFxuICAgIElTX05PVCAgICAgICAgICAgICAgICA9ICdJUyBOT1QnLFxufVxuXG5leHBvcnQgZW51bSBFeHByZXNzaW9uR3JvdXBKb2luZXJcbntcbiAgICBBTkQgICA9ICdBTkQnLFxuICAgIE9SICAgID0gJ09SJyxcbiAgICBDT01NQSA9ICcsJ1xufVxuXG5cblxuZXhwb3J0IHR5cGUgRXhwcmVzc2lvblZhbHVlVHlwZSA9IHN0cmluZ3xudW1iZXI7XG5cblxuXG4vKlxuaW50ZXJmYWNlIElNeVNRTFZhbHVlPFQgZXh0ZW5kcyBFeHByZXNzaW9uVmFsdWVUeXBlPlxue1xuICAgIGRhdGE6VCxcbiAgICAvL3R5cGU6TXlTUUxWYWx1ZVR5cGVcbn1cbiovXG5leHBvcnQgaW50ZXJmYWNlIElNeVNRTFZhbHVlXG57XG4gICAgZGF0YTpFeHByZXNzaW9uVmFsdWVUeXBlO1xuICAgIGlzU3RyaW5nOmJvb2xlYW47XG59XG5cblxuXG5leHBvcnQgY2xhc3MgTXlTUUxWYWx1ZSBpbXBsZW1lbnRzIElNeVNRTFZhbHVlXG57XG4gICAgcmVhZG9ubHkgaXNTdHJpbmc6Ym9vbGVhbjtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICByZWFkb25seSBkYXRhOkV4cHJlc3Npb25WYWx1ZVR5cGUsXG4gICAgICAgIHJlYWRvbmx5IHR5cGU6TXlTUUxWYWx1ZVR5cGVcbiAgICApIHtcbiAgICAgICAgdGhpcy5pc1N0cmluZyA9IFxuICAgICAgICAgICAgdHlwZSA9PSBNeVNRTFZhbHVlVHlwZS5TVFJJTkc7XG4gICAgfVxufVxuXG5jbGFzcyBNeVNRTE51bWJlclZhbHVlIGV4dGVuZHMgTXlTUUxWYWx1ZVxue1xuICAgIGNvbnN0cnVjdG9yKGRhdGE6bnVtYmVyKSB7XG4gICAgICAgIHN1cGVyKGRhdGEsTXlTUUxWYWx1ZVR5cGUuRVhQUkVTU0lPTik7XG4gICAgfVxufVxuXG5jbGFzcyBNeVNRTFN0cmluZ1ZhbHVlIGV4dGVuZHMgTXlTUUxWYWx1ZVxue1xuICAgIGNvbnN0cnVjdG9yKGRhdGE6c3RyaW5nKSB7XG4gICAgICAgIHN1cGVyKGRhdGEsTXlTUUxWYWx1ZVR5cGUuU1RSSU5HKTtcbiAgICB9XG59XG5cbmNsYXNzIE15U1FMU3RyaW5nRXhwcmVzc2lvblZhbHVlIGV4dGVuZHMgTXlTUUxWYWx1ZVxue1xuICAgIGNvbnN0cnVjdG9yKGRhdGE6c3RyaW5nKSB7XG4gICAgICAgIHN1cGVyKGRhdGEsTXlTUUxWYWx1ZVR5cGUuRVhQUkVTU0lPTik7XG4gICAgfVxufVxuXG5cbmV4cG9ydCBlbnVtIE15U1FMRXhwcmVzc2lvbiB7XG4gICAgTk9XID0gJ05PVygpJyxcbiAgICBOVUxMID0gJ05VTEwnLFxufVxuXG5jb25zdCBub3dWYWx1ZSA9IG5ldyBNeVNRTFN0cmluZ0V4cHJlc3Npb25WYWx1ZShNeVNRTEV4cHJlc3Npb24uTk9XKTtcbmNvbnN0IG51bGxWYWx1ZSA9IG5ldyBNeVNRTFN0cmluZ0V4cHJlc3Npb25WYWx1ZShNeVNRTEV4cHJlc3Npb24uTlVMTCk7XG5cblxuLypcbmNsYXNzIE15U1FMVmFsdWVcbntcbiAgICBzdGF0aWMgYm9vbGVhbkFzSW50KGJvb2w6Ym9vbGVhbik6bnVtYmVyXG4gICAge1xuICAgICAgICByZXR1cm4gYm9vbCA/IDEgOiAwO1xuICAgIH1cbn1cbiovXG5cblxuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRXhwcmVzc2lvblxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICByZWFkb25seSBsZWZ0U2lkZTpzdHJpbmcsXG4gICAgICAgIHJlYWRvbmx5IG9wZXJhdG9yOnN0cmluZyxcbiAgICAgICAgcmVhZG9ubHkgcmlnaHRTaWRlOk15U1FMVmFsdWVcbiAgICApIHtcbiAgICB9XG59XG5cblxuZXhwb3J0IGNsYXNzIEV4cHJlc3Npb25Hcm91cFxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICByZWFkb25seSBleHByZXNzaW9uczpFeHByZXNzaW9uW10sXG4gICAgICAgIHJlYWRvbmx5IGpvaW5lcjpFeHByZXNzaW9uR3JvdXBKb2luZXIsXG4gICAgICAgIHJlYWRvbmx5IHN1cnJvdW5kRXhwcmVzc2lvbldpdGhCcmFja2V0czpib29sZWFuID0gdHJ1ZVxuICAgICkge1xuICAgIH1cbn1cblxuXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlRXhwcmVzc2lvbkdyb3VwKFxuICAgIG1hcDphbnksam9pbmVyOkV4cHJlc3Npb25Hcm91cEpvaW5lclxuKSB7XG4gICAgY29uc3QgZXhwcmVzc2lvbnM6RXhwcmVzc2lvbltdID0gW107XG5cbiAgICAvLyBzaG91bGQgd2UgdXNlIE9iamVjdC5rZXlzKCk/XG4gICAgZm9yIChjb25zdCBmaWVsZE5hbWUgaW4gbWFwKSB7XG4gICAgICAgIGNvbnN0IGZpZWxkVmFsdWUgPSBtYXBbZmllbGROYW1lXTtcblxuICAgICAgICBleHByZXNzaW9ucy5wdXNoKFxuICAgICAgICAgICAgdHlwZW9mIGZpZWxkVmFsdWUgPT0gJ3N0cmluZycgP1xuICAgICAgICAgICAgICAgIG5ldyBTdHJpbmdTdGF0ZW1lbnQoXG4gICAgICAgICAgICAgICAgICAgIGZpZWxkTmFtZSxcbiAgICAgICAgICAgICAgICAgICAgZmllbGRWYWx1ZSBhcyBzdHJpbmdcbiAgICAgICAgICAgICAgICApIDpcbiAgICAgICAgICAgICAgICBuZXcgTnVtYmVyU3RhdGVtZW50KFxuICAgICAgICAgICAgICAgICAgICBmaWVsZE5hbWUsXG4gICAgICAgICAgICAgICAgICAgIGZpZWxkVmFsdWVcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICk7XG4gICAgfVxuICAgIFxuICAgIHJldHVybiBuZXcgRXhwcmVzc2lvbkdyb3VwKGV4cHJlc3Npb25zLGpvaW5lcik7XG59XG5cblxuZXhwb3J0IGNsYXNzIFNpbmdsZUV4cHJlc3Npb24gZXh0ZW5kcyBFeHByZXNzaW9uR3JvdXBcbntcbiAgICBjb25zdHJ1Y3RvcihleHByZXNzaW9uOkV4cHJlc3Npb24pIHtcbiAgICAgICAgc3VwZXIoW2V4cHJlc3Npb25dLHVuZGVmaW5lZCk7XG4gICAgfVxufVxuZXhwb3J0IGNsYXNzIEFuZEV4cHJlc3Npb25Hcm91cCBleHRlbmRzIEV4cHJlc3Npb25Hcm91cFxue1xuICAgIGNvbnN0cnVjdG9yKGV4cHJlc3Npb25zOkV4cHJlc3Npb25bXSkge1xuICAgICAgICBzdXBlcihleHByZXNzaW9ucyxFeHByZXNzaW9uR3JvdXBKb2luZXIuQU5EKTtcbiAgICB9XG59XG5leHBvcnQgY2xhc3MgT3JFeHByZXNzaW9uR3JvdXAgZXh0ZW5kcyBFeHByZXNzaW9uR3JvdXBcbntcbiAgICBjb25zdHJ1Y3RvcihleHByZXNzaW9uczpFeHByZXNzaW9uW10pIHtcbiAgICAgICAgc3VwZXIoZXhwcmVzc2lvbnMsRXhwcmVzc2lvbkdyb3VwSm9pbmVyLk9SKTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBVcGRhdGVTdGF0ZW1lbnRzIGV4dGVuZHMgRXhwcmVzc2lvbkdyb3VwXG57XG4gICAgY29uc3RydWN0b3IoZXhwcmVzc2lvbnM6RXhwcmVzc2lvbltdKSB7XG4gICAgICAgIHN1cGVyKGV4cHJlc3Npb25zLEV4cHJlc3Npb25Hcm91cEpvaW5lci5DT01NQSxmYWxzZSk7XG4gICAgfVxufVxuXG5cblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEZpZWxkU3RhdGVtZW50IGV4dGVuZHMgRXhwcmVzc2lvblxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBmaWVsZE5hbWU6c3RyaW5nLFxuICAgICAgICB2YWx1ZTpNeVNRTFZhbHVlLFxuICAgICAgICBvcGVyYXRvcjpFeHByZXNzaW9uT3BlcmF0b3JcbiAgICApIHtcbiAgICAgICAgc3VwZXIoZmllbGROYW1lLG9wZXJhdG9yLHZhbHVlKTtcbiAgICB9XG59XG5cblxuZXhwb3J0IGNsYXNzIE51bWJlclN0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50XG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIGZpZWxkTmFtZTpzdHJpbmcsXG4gICAgICAgIGRhdGE6bnVtYmVyLFxuICAgICAgICBvcGVyYXRvcjpFeHByZXNzaW9uT3BlcmF0b3IgPSBFeHByZXNzaW9uT3BlcmF0b3IuRVFVQUxcbiAgICApIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICBmaWVsZE5hbWUsXG4gICAgICAgICAgICBuZXcgTXlTUUxOdW1iZXJWYWx1ZShkYXRhKSxcbiAgICAgICAgICAgIG9wZXJhdG9yXG4gICAgICAgICk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgU3RyaW5nU3RhdGVtZW50IGV4dGVuZHMgRmllbGRTdGF0ZW1lbnRcbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgZmllbGROYW1lOnN0cmluZyxcbiAgICAgICAgZGF0YTpzdHJpbmcsXG4gICAgICAgIG9wZXJhdG9yOkV4cHJlc3Npb25PcGVyYXRvciA9IEV4cHJlc3Npb25PcGVyYXRvci5FUVVBTFxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIGZpZWxkTmFtZSxcbiAgICAgICAgICAgIG5ldyBNeVNRTFN0cmluZ1ZhbHVlKGRhdGEpLFxuICAgICAgICAgICAgb3BlcmF0b3JcbiAgICAgICAgKTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBTdHJpbmdFeHByZXNzaW9uU3RhdGVtZW50IGV4dGVuZHMgRmllbGRTdGF0ZW1lbnRcbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgZmllbGROYW1lOnN0cmluZyxcbiAgICAgICAgZGF0YTpzdHJpbmcsXG4gICAgICAgIG9wZXJhdG9yOkV4cHJlc3Npb25PcGVyYXRvciA9IEV4cHJlc3Npb25PcGVyYXRvci5FUVVBTFxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIGZpZWxkTmFtZSxcbiAgICAgICAgICAgIG5ldyBNeVNRTFN0cmluZ0V4cHJlc3Npb25WYWx1ZShkYXRhKSxcbiAgICAgICAgICAgIG9wZXJhdG9yXG4gICAgICAgICk7XG4gICAgfVxufVxuXG5cbmV4cG9ydCBjbGFzcyBOdWxsVXBkYXRlU3RhdGVtZW50IGV4dGVuZHMgRmllbGRTdGF0ZW1lbnRcbntcbiAgICBjb25zdHJ1Y3RvcihmaWVsZE5hbWU6c3RyaW5nKSB7XG4gICAgICAgIHN1cGVyKGZpZWxkTmFtZSxudWxsVmFsdWUsRXhwcmVzc2lvbk9wZXJhdG9yLkVRVUFMKTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBOb3dVcGRhdGVTdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudFxue1xuICAgIGNvbnN0cnVjdG9yKGZpZWxkTmFtZTpzdHJpbmcpIHtcbiAgICAgICAgc3VwZXIoZmllbGROYW1lLG5vd1ZhbHVlLEV4cHJlc3Npb25PcGVyYXRvci5FUVVBTCk7XG4gICAgfVxufVxuXG5cbmV4cG9ydCBjbGFzcyBGaWVsZElzTnVsbFN0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50XG57XG4gICAgY29uc3RydWN0b3IoZmllbGROYW1lOnN0cmluZykge1xuICAgICAgICBzdXBlcihmaWVsZE5hbWUsbnVsbFZhbHVlLEV4cHJlc3Npb25PcGVyYXRvci5JUyk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgRmllbGRJc05vdE51bGxTdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudFxue1xuICAgIGNvbnN0cnVjdG9yKGZpZWxkTmFtZTpzdHJpbmcpIHtcbiAgICAgICAgc3VwZXIoZmllbGROYW1lLG51bGxWYWx1ZSxFeHByZXNzaW9uT3BlcmF0b3IuSVNfTk9UKTtcbiAgICB9XG59Il19