/**
 * @fileoverview added by tsickle
 * Generated from: lib/db/engine/mysql/mysql-transaction-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { MySQLConnectionManager } from './mysql-connection-manager';
import { MySQLTransactionQuery } from './mysql-query';
var MySQLTransactionManager = /** @class */ (function () {
    function MySQLTransactionManager(config) {
        var _this = this;
        this.queue = [];
        this.isExecutingTransaction = false;
        /**
         * we need to manage a queue of transactions
         * to be executed in order
         * once the transaction either is successful or fails
         * then move on to the next
         */
        this.performNextTransaction = (/**
         * @return {?}
         */
        function () {
            if (_this.isExecutingTransaction ||
                _this.queue.length < 1) {
                return;
            }
            console.log('perform next transaction!');
            _this.isExecutingTransaction = true;
            // remove first element from queue and return it
            /** @type {?} */
            var executor = _this.queue.shift();
            executor.execute();
        });
        this.onTransactionCompleted = (/**
         * @return {?}
         */
        function () {
            console.log('onTransactionCompleted');
            _this.isExecutingTransaction = false;
            _this.performNextTransaction();
        });
        this.connectionManager = new MySQLConnectionManager(config);
    }
    /**
     * @param {?} queries
     * @return {?}
     */
    MySQLTransactionManager.prototype.performTransaction = /**
     * @param {?} queries
     * @return {?}
     */
    function (queries) {
        /*
                we need to promise the result of each query executed
                so that they can be inspected by consumer for Ids, etc
                */
        /** @type {?} */
        var executor = new MySQLTransactionExecuter(queries, this.performNextTransaction, this.onTransactionCompleted, this.connectionManager);
        // add executor to queue
        this.queue.push(executor);
        return new Promise(executor.init);
    };
    return MySQLTransactionManager;
}());
export { MySQLTransactionManager };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.queue;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.isExecutingTransaction;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.connectionManager;
    /**
     * we need to manage a queue of transactions
     * to be executed in order
     * once the transaction either is successful or fails
     * then move on to the next
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.performNextTransaction;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.onTransactionCompleted;
}
var MySQLTransactionExecuter = /** @class */ (function () {
    function MySQLTransactionExecuter(queries, onInitHandler, onCompleteHandler, connectionManager) {
        var _this = this;
        this.queries = queries;
        this.onInitHandler = onInitHandler;
        this.onCompleteHandler = onCompleteHandler;
        this.connectionManager = connectionManager;
        this.init = (/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            _this.resolve = resolve;
            _this.reject = reject;
            _this.onInitHandler();
        });
    }
    /**
     * @return {?}
     */
    MySQLTransactionExecuter.prototype.execute = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.connectionManager.getConnection()];
                    case 1:
                        _a.connection = _b.sent();
                        this.connection.beginTransaction((/**
                         * @return {?}
                         */
                        function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var results, _a, _b, queryString, query, result, e_1_1, e_2;
                            var e_1, _c;
                            return tslib_1.__generator(this, function (_d) {
                                switch (_d.label) {
                                    case 0:
                                        _d.trys.push([0, 10, , 12]);
                                        results = [];
                                        _d.label = 1;
                                    case 1:
                                        _d.trys.push([1, 6, 7, 8]);
                                        _a = tslib_1.__values(this.queries), _b = _a.next();
                                        _d.label = 2;
                                    case 2:
                                        if (!!_b.done) return [3 /*break*/, 5];
                                        queryString = _b.value;
                                        query = new MySQLTransactionQuery(queryString, this.connectionManager);
                                        return [4 /*yield*/, query.execute()];
                                    case 3:
                                        result = _d.sent();
                                        results.push(result);
                                        _d.label = 4;
                                    case 4:
                                        _b = _a.next();
                                        return [3 /*break*/, 2];
                                    case 5: return [3 /*break*/, 8];
                                    case 6:
                                        e_1_1 = _d.sent();
                                        e_1 = { error: e_1_1 };
                                        return [3 /*break*/, 8];
                                    case 7:
                                        try {
                                            if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                                        }
                                        finally { if (e_1) throw e_1.error; }
                                        return [7 /*endfinally*/];
                                    case 8: return [4 /*yield*/, this.commit()];
                                    case 9:
                                        _d.sent();
                                        this.resolve(results);
                                        return [3 /*break*/, 12];
                                    case 10:
                                        e_2 = _d.sent();
                                        return [4 /*yield*/, this.rollback()];
                                    case 11:
                                        _d.sent();
                                        this.reject(e_2);
                                        return [3 /*break*/, 12];
                                    case 12:
                                        this.onCompleteHandler();
                                        return [2 /*return*/];
                                }
                            });
                        }); }));
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @private
     * @return {?}
     */
    MySQLTransactionExecuter.prototype.commit = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            _this.connection.commit((/**
             * @param {?} error
             * @return {?}
             */
            function (error) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                return tslib_1.__generator(this, function (_a) {
                    if (error) {
                        return [2 /*return*/, reject(error)];
                    }
                    resolve();
                    return [2 /*return*/];
                });
            }); }));
        }));
    };
    /**
     * @private
     * @return {?}
     */
    MySQLTransactionExecuter.prototype.rollback = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        function (resolve) {
            _this.connection.rollback(resolve);
        }));
    };
    return MySQLTransactionExecuter;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.resolve;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.reject;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.connection;
    /** @type {?} */
    MySQLTransactionExecuter.prototype.init;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.queries;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.onInitHandler;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.onCompleteHandler;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.connectionManager;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtdHJhbnNhY3Rpb24tbWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL2VuZ2luZS9teXNxbC9teXNxbC10cmFuc2FjdGlvbi1tYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUdBLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUd0RDtJQU9JLGlDQUFZLE1BQW1CO1FBQS9CLGlCQUVDO1FBUE8sVUFBSyxHQUE4QixFQUFFLENBQUM7UUFDdEMsMkJBQXNCLEdBQUcsS0FBSyxDQUFDOzs7Ozs7O1FBK0IvQiwyQkFBc0I7OztRQUFHO1lBQzdCLElBQ0ksS0FBSSxDQUFDLHNCQUFzQjtnQkFDM0IsS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUN2QjtnQkFDRSxPQUFPO2FBQ1Y7WUFFRCxPQUFPLENBQUMsR0FBRyxDQUFDLDJCQUEyQixDQUFDLENBQUM7WUFFekMsS0FBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQzs7O2dCQUc3QixRQUFRLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUU7WUFFbkMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3ZCLENBQUMsRUFBQztRQUVNLDJCQUFzQjs7O1FBQUc7WUFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBRXRDLEtBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7WUFFcEMsS0FBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDbEMsQ0FBQyxFQUFBO1FBbERHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Ozs7O0lBRUQsb0RBQWtCOzs7O0lBQWxCLFVBQW1CLE9BQWdCOzs7Ozs7WUFLekIsUUFBUSxHQUFHLElBQUksd0JBQXdCLENBQ3pDLE9BQU8sRUFDUCxJQUFJLENBQUMsc0JBQXNCLEVBQzNCLElBQUksQ0FBQyxzQkFBc0IsRUFDM0IsSUFBSSxDQUFDLGlCQUFpQixDQUN6QjtRQUVELHdCQUF3QjtRQUN4QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUUxQixPQUFPLElBQUksT0FBTyxDQUFzQixRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQWdDTCw4QkFBQztBQUFELENBQUMsQUEzREQsSUEyREM7Ozs7Ozs7SUF6REcsd0NBQThDOzs7OztJQUM5Qyx5REFBdUM7Ozs7O0lBRXZDLG9EQUEwRDs7Ozs7Ozs7O0lBNkIxRCx5REFnQkU7Ozs7O0lBRUYseURBTUM7O0FBS0w7SUFNSSxrQ0FDWSxPQUFnQixFQUNoQixhQUFzQixFQUN0QixpQkFBMEIsRUFDMUIsaUJBQXdDO1FBSnBELGlCQU1DO1FBTFcsWUFBTyxHQUFQLE9BQU8sQ0FBUztRQUNoQixrQkFBYSxHQUFiLGFBQWEsQ0FBUztRQUN0QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQVM7UUFDMUIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUF1QjtRQUlwRCxTQUFJOzs7OztRQUFHLFVBQUMsT0FBdUIsRUFBQyxNQUFlO1lBQzNDLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1lBQ3ZCLEtBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1lBRXJCLEtBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN6QixDQUFDLEVBQUE7SUFQRCxDQUFDOzs7O0lBU0ssMENBQU87OztJQUFiOzs7Ozs7O3dCQUNJLEtBQUEsSUFBSSxDQUFBO3dCQUFjLHFCQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsRUFBQTs7d0JBQTlELEdBQUssVUFBVSxHQUFHLFNBQTRDLENBQUM7d0JBRS9ELElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCOzs7d0JBQUM7Ozs7Ozs7d0NBRW5CLE9BQU8sR0FBdUIsRUFBRTs7Ozt3Q0FFWixLQUFBLGlCQUFBLElBQUksQ0FBQyxPQUFPLENBQUE7Ozs7d0NBQTNCLFdBQVc7d0NBQ1osS0FBSyxHQUFHLElBQUkscUJBQXFCLENBQ25DLFdBQVcsRUFDWCxJQUFJLENBQUMsaUJBQWlCLENBQ3pCO3dDQUVjLHFCQUFNLEtBQUssQ0FBQyxPQUFPLEVBQUUsRUFBQTs7d0NBQTlCLE1BQU0sR0FBRyxTQUFxQjt3Q0FFcEMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs0Q0FHekIscUJBQU0sSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFBOzt3Q0FBbkIsU0FBbUIsQ0FBQzt3Q0FFcEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQzs7Ozt3Q0FFdEIscUJBQU0sSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFBOzt3Q0FBckIsU0FBcUIsQ0FBQzt3Q0FFdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFDLENBQUMsQ0FBQzs7O3dDQUduQixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQzs7Ozs2QkFDNUIsRUFBQyxDQUFDOzs7OztLQUNOOzs7OztJQUVPLHlDQUFNOzs7O0lBQWQ7UUFBQSxpQkFVQztRQVRHLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDL0IsS0FBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNOzs7O1lBQUMsVUFBTyxLQUFLOztvQkFDL0IsSUFBSSxLQUFLLEVBQUU7d0JBQ1Asc0JBQU8sTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFDO3FCQUN4QjtvQkFFRCxPQUFPLEVBQUUsQ0FBQzs7O2lCQUNiLEVBQUMsQ0FBQztRQUNQLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFTywyQ0FBUTs7OztJQUFoQjtRQUFBLGlCQUlDO1FBSEcsT0FBTyxJQUFJLE9BQU87Ozs7UUFBQyxVQUFDLE9BQU87WUFDdkIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdEMsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0wsK0JBQUM7QUFBRCxDQUFDLEFBckVELElBcUVDOzs7Ozs7SUFuRUcsMkNBQWdDOzs7OztJQUNoQywwQ0FBd0I7Ozs7O0lBQ3hCLDhDQUE4Qjs7SUFVOUIsd0NBS0M7Ozs7O0lBWkcsMkNBQXdCOzs7OztJQUN4QixpREFBOEI7Ozs7O0lBQzlCLHFEQUFrQzs7Ozs7SUFDbEMscURBQWdEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29ubmVjdGlvbiB9IGZyb20gXCJteXNxbFwiO1xuXG5pbXBvcnQgeyBJTXlTUUxDb25maWcsIElNeVNRTFF1ZXJ5UmVzdWx0IH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIgfSBmcm9tICcuL215c3FsLWNvbm5lY3Rpb24tbWFuYWdlcic7XG5pbXBvcnQgeyBNeVNRTFRyYW5zYWN0aW9uUXVlcnkgfSBmcm9tICcuL215c3FsLXF1ZXJ5JztcblxuXG5leHBvcnQgY2xhc3MgTXlTUUxUcmFuc2FjdGlvbk1hbmFnZXJcbntcbiAgICBwcml2YXRlIHF1ZXVlOk15U1FMVHJhbnNhY3Rpb25FeGVjdXRlcltdID0gW107XG4gICAgcHJpdmF0ZSBpc0V4ZWN1dGluZ1RyYW5zYWN0aW9uID0gZmFsc2U7XG5cbiAgICBwcml2YXRlIHJlYWRvbmx5IGNvbm5lY3Rpb25NYW5hZ2VyOk15U1FMQ29ubmVjdGlvbk1hbmFnZXI7XG5cbiAgICBjb25zdHJ1Y3Rvcihjb25maWc6SU15U1FMQ29uZmlnKSB7XG4gICAgICAgIHRoaXMuY29ubmVjdGlvbk1hbmFnZXIgPSBuZXcgTXlTUUxDb25uZWN0aW9uTWFuYWdlcihjb25maWcpO1xuICAgIH1cblxuICAgIHBlcmZvcm1UcmFuc2FjdGlvbihxdWVyaWVzOnN0cmluZ1tdKSB7XG4gICAgICAgIC8qXG4gICAgICAgIHdlIG5lZWQgdG8gcHJvbWlzZSB0aGUgcmVzdWx0IG9mIGVhY2ggcXVlcnkgZXhlY3V0ZWRcbiAgICAgICAgc28gdGhhdCB0aGV5IGNhbiBiZSBpbnNwZWN0ZWQgYnkgY29uc3VtZXIgZm9yIElkcywgZXRjXG4gICAgICAgICovXG4gICAgICAgIGNvbnN0IGV4ZWN1dG9yID0gbmV3IE15U1FMVHJhbnNhY3Rpb25FeGVjdXRlcihcbiAgICAgICAgICAgIHF1ZXJpZXMsXG4gICAgICAgICAgICB0aGlzLnBlcmZvcm1OZXh0VHJhbnNhY3Rpb24sXG4gICAgICAgICAgICB0aGlzLm9uVHJhbnNhY3Rpb25Db21wbGV0ZWQsXG4gICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyXG4gICAgICAgICk7XG5cbiAgICAgICAgLy8gYWRkIGV4ZWN1dG9yIHRvIHF1ZXVlXG4gICAgICAgIHRoaXMucXVldWUucHVzaChleGVjdXRvcik7XG5cbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlPElNeVNRTFF1ZXJ5UmVzdWx0W10+KGV4ZWN1dG9yLmluaXQpO1xuICAgIH1cblxuICAgIC8qKiogd2UgbmVlZCB0byBtYW5hZ2UgYSBxdWV1ZSBvZiB0cmFuc2FjdGlvbnNcbiAgICAgKiB0byBiZSBleGVjdXRlZCBpbiBvcmRlclxuICAgICAqIG9uY2UgdGhlIHRyYW5zYWN0aW9uIGVpdGhlciBpcyBzdWNjZXNzZnVsIG9yIGZhaWxzXG4gICAgICogdGhlbiBtb3ZlIG9uIHRvIHRoZSBuZXh0XG4gICAgICovXG4gICAgcHJpdmF0ZSBwZXJmb3JtTmV4dFRyYW5zYWN0aW9uID0gKCkgPT4ge1xuICAgICAgICBpZiAoXG4gICAgICAgICAgICB0aGlzLmlzRXhlY3V0aW5nVHJhbnNhY3Rpb24gfHxcbiAgICAgICAgICAgIHRoaXMucXVldWUubGVuZ3RoIDwgMVxuICAgICAgICApIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnNvbGUubG9nKCdwZXJmb3JtIG5leHQgdHJhbnNhY3Rpb24hJyk7XG5cbiAgICAgICAgdGhpcy5pc0V4ZWN1dGluZ1RyYW5zYWN0aW9uID0gdHJ1ZTtcblxuICAgICAgICAvLyByZW1vdmUgZmlyc3QgZWxlbWVudCBmcm9tIHF1ZXVlIGFuZCByZXR1cm4gaXRcbiAgICAgICAgY29uc3QgZXhlY3V0b3IgPSB0aGlzLnF1ZXVlLnNoaWZ0KCk7XG5cbiAgICAgICAgZXhlY3V0b3IuZXhlY3V0ZSgpO1xuICAgIH07XG5cbiAgICBwcml2YXRlIG9uVHJhbnNhY3Rpb25Db21wbGV0ZWQgPSAoKSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdvblRyYW5zYWN0aW9uQ29tcGxldGVkJyk7XG5cbiAgICAgICAgdGhpcy5pc0V4ZWN1dGluZ1RyYW5zYWN0aW9uID0gZmFsc2U7XG5cbiAgICAgICAgdGhpcy5wZXJmb3JtTmV4dFRyYW5zYWN0aW9uKCk7XG4gICAgfVxufVxuXG50eXBlIFJlc29sdmVGdW5jdGlvbiA9IChyZXN1bHRzOklNeVNRTFF1ZXJ5UmVzdWx0W10pID0+IHZvaWQ7XG5cbmNsYXNzIE15U1FMVHJhbnNhY3Rpb25FeGVjdXRlclxue1xuICAgIHByaXZhdGUgcmVzb2x2ZTpSZXNvbHZlRnVuY3Rpb247XG4gICAgcHJpdmF0ZSByZWplY3Q6RnVuY3Rpb247XG4gICAgcHJpdmF0ZSBjb25uZWN0aW9uOkNvbm5lY3Rpb247XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSBxdWVyaWVzOnN0cmluZ1tdLFxuICAgICAgICBwcml2YXRlIG9uSW5pdEhhbmRsZXI6RnVuY3Rpb24sXG4gICAgICAgIHByaXZhdGUgb25Db21wbGV0ZUhhbmRsZXI6RnVuY3Rpb24sXG4gICAgICAgIHByaXZhdGUgY29ubmVjdGlvbk1hbmFnZXI6TXlTUUxDb25uZWN0aW9uTWFuYWdlclxuICAgICkge1xuICAgIH1cblxuICAgIGluaXQgPSAocmVzb2x2ZTpSZXNvbHZlRnVuY3Rpb24scmVqZWN0OkZ1bmN0aW9uKSA9PiB7XG4gICAgICAgIHRoaXMucmVzb2x2ZSA9IHJlc29sdmU7XG4gICAgICAgIHRoaXMucmVqZWN0ID0gcmVqZWN0O1xuXG4gICAgICAgIHRoaXMub25Jbml0SGFuZGxlcigpO1xuICAgIH1cblxuICAgIGFzeW5jIGV4ZWN1dGUoKSB7XG4gICAgICAgIHRoaXMuY29ubmVjdGlvbiA9IGF3YWl0IHRoaXMuY29ubmVjdGlvbk1hbmFnZXIuZ2V0Q29ubmVjdGlvbigpO1xuXG4gICAgICAgIHRoaXMuY29ubmVjdGlvbi5iZWdpblRyYW5zYWN0aW9uKGFzeW5jICgpID0+IHtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgY29uc3QgcmVzdWx0czpJTXlTUUxRdWVyeVJlc3VsdFtdID0gW107XG5cbiAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IHF1ZXJ5U3RyaW5nIG9mIHRoaXMucXVlcmllcykge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBxdWVyeSA9IG5ldyBNeVNRTFRyYW5zYWN0aW9uUXVlcnkoXG4gICAgICAgICAgICAgICAgICAgICAgICBxdWVyeVN0cmluZyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29ubmVjdGlvbk1hbmFnZXJcbiAgICAgICAgICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBxdWVyeS5leGVjdXRlKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0cy5wdXNoKHJlc3VsdCk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5jb21taXQoKTtcblxuICAgICAgICAgICAgICAgIHRoaXMucmVzb2x2ZShyZXN1bHRzKTtcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLnJvbGxiYWNrKCk7XG5cbiAgICAgICAgICAgICAgICB0aGlzLnJlamVjdChlKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5vbkNvbXBsZXRlSGFuZGxlcigpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGNvbW1pdCgpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIHRoaXMuY29ubmVjdGlvbi5jb21taXQoYXN5bmMgKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZWplY3QoZXJyb3IpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHJvbGxiYWNrKCkge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcbiAgICAgICAgICAgIHRoaXMuY29ubmVjdGlvbi5yb2xsYmFjayhyZXNvbHZlKTtcbiAgICAgICAgfSk7XG4gICAgfVxufSJdfQ==