/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import * as mysql from "mysql";
import { sleep } from 'earnbet-common';
var MySQLConnectionManager = /** @class */ (function () {
    function MySQLConnectionManager(config) {
        this.config = config;
        this.isConnecting = false;
        this.connect();
    }
    /**
     * @return {?}
     */
    MySQLConnectionManager.prototype.getConnection = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.reconnect()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, this.connection];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    MySQLConnectionManager.prototype.reconnect = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var isConnected;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.isDisconnected) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.connect()];
                    case 1:
                        isConnected = _a.sent();
                        if (isConnected) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, sleep(1000)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 0];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    MySQLConnectionManager.prototype.connect = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (!_this.isDisconnected) {
                resolve(true);
                return;
            }
            if (_this.isConnecting) {
                resolve(false);
                return;
            }
            _this.isConnecting = true;
            // end existing connection
            if (_this.connection) {
                _this.connection.end();
            }
            // connection parameters
            // connection parameters
            _this.connection = mysql.createConnection(_this.config);
            // establish connection
            // establish connection
            _this.connection.connect(function (err) {
                if (err) {
                    console.error('error connecting: ' + err.stack);
                    resolve(false);
                    return;
                }
                console.log('connected as id ' + _this.connection.threadId);
                resolve(true);
                _this.isConnecting = false;
            });
            _this.connection.on('error', function (err) {
                console.error(err);
            });
        });
    };
    Object.defineProperty(MySQLConnectionManager.prototype, "isDisconnected", {
        get: /**
         * @return {?}
         */
        function () {
            return this.connection == undefined ||
                this.connection.state === 'disconnected';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    MySQLConnectionManager.prototype.endConnection = /**
     * @return {?}
     */
    function () {
        this.connection.end();
    };
    return MySQLConnectionManager;
}());
export { MySQLConnectionManager };
if (false) {
    /** @type {?} */
    MySQLConnectionManager.prototype.isConnecting;
    /** @type {?} */
    MySQLConnectionManager.prototype.connection;
    /** @type {?} */
    MySQLConnectionManager.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvZGIvbXlzcWwvbXlzcWwtY29ubmVjdGlvbi1tYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxFQUFDLEtBQUssRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBSXJDLElBQUE7SUFNSSxnQ0FBb0IsTUFBbUI7UUFBbkIsV0FBTSxHQUFOLE1BQU0sQ0FBYTs0QkFKaEIsS0FBSztRQUt4QixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7S0FDbEI7Ozs7SUFFSyw4Q0FBYTs7O0lBQW5COzs7OzRCQUNJLHFCQUFNLElBQUksQ0FBQyxTQUFTLEVBQUUsRUFBQTs7d0JBQXRCLFNBQXNCLENBQUM7d0JBRXZCLHNCQUFPLElBQUksQ0FBQyxVQUFVLEVBQUM7Ozs7S0FDMUI7Ozs7SUFFYSwwQ0FBUzs7Ozs7Ozs7OzZCQUNaLElBQUksQ0FBQyxjQUFjO3dCQUNGLHFCQUFNLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBQTs7d0JBQWxDLFdBQVcsR0FBRyxTQUFvQjt3QkFFeEMsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQzs0QkFDZCxNQUFNLGdCQUFDO3lCQUNWO3dCQUVELHFCQUFNLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBQTs7d0JBQWpCLFNBQWlCLENBQUM7Ozs7Ozs7Ozs7SUFJbEIsd0NBQU87Ozs7O1FBRVgsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFXLFVBQUMsT0FBTztZQUNqQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2QsTUFBTSxDQUFDO2FBQ1Y7WUFHRCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDcEIsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNmLE1BQU0sQ0FBQzthQUNWO1lBR0QsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7O1lBSXpCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUNsQixLQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDO2FBQ3pCOztZQUlELEFBREEsd0JBQXdCO1lBQ3hCLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLGdCQUFnQixDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzs7WUFHdEQsQUFEQSx1QkFBdUI7WUFDdkIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFHO2dCQUN4QixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUNOLE9BQU8sQ0FBQyxLQUFLLENBQUMsb0JBQW9CLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUVoRCxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ2YsTUFBTSxDQUFDO2lCQUNWO2dCQUdELE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDM0QsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUdkLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO2FBQzdCLENBQUMsQ0FBQztZQUVILEtBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBQyxVQUFDLEdBQUc7Z0JBQzNCLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDdEIsQ0FBQyxDQUFDO1NBQ04sQ0FBRSxDQUFDOzswQkFHSSxrREFBYzs7Ozs7WUFDdEIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksU0FBUztnQkFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEtBQUssY0FBYyxDQUFDOzs7Ozs7OztJQUdyRCw4Q0FBYTs7O0lBQWI7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDO0tBQ3pCO2lDQTNGTDtJQTRGQyxDQUFBO0FBdEZELGtDQXNGQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIG15c3FsIGZyb20gXCJteXNxbFwiO1xuXG5pbXBvcnQge3NsZWVwfSBmcm9tICdlYXJuYmV0LWNvbW1vbic7XG5pbXBvcnQgeyBJTXlTUUxDb25maWcgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTENvbm5lY3Rpb25NYW5hZ2VyXG57XG4gICAgcHJpdmF0ZSBpc0Nvbm5lY3RpbmcgPSBmYWxzZTtcblxuICAgIHByaXZhdGUgY29ubmVjdGlvbjpteXNxbC5Db25uZWN0aW9uO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjb25maWc6SU15U1FMQ29uZmlnKSB7XG4gICAgICAgIHRoaXMuY29ubmVjdCgpO1xuICAgIH1cblxuICAgIGFzeW5jIGdldENvbm5lY3Rpb24oKTpQcm9taXNlPG15c3FsLkNvbm5lY3Rpb24+IHtcbiAgICAgICAgYXdhaXQgdGhpcy5yZWNvbm5lY3QoKTtcblxuICAgICAgICByZXR1cm4gdGhpcy5jb25uZWN0aW9uO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgcmVjb25uZWN0KCkge1xuICAgICAgICB3aGlsZSAodGhpcy5pc0Rpc2Nvbm5lY3RlZCkge1xuICAgICAgICAgICAgY29uc3QgaXNDb25uZWN0ZWQgPSBhd2FpdCB0aGlzLmNvbm5lY3QoKTtcblxuICAgICAgICAgICAgaWYgKGlzQ29ubmVjdGVkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBhd2FpdCBzbGVlcCgxMDAwKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgY29ubmVjdCgpOlByb21pc2U8Ym9vbGVhbj5cbiAgICB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxib29sZWFuPiggKHJlc29sdmUpPT57XG4gICAgICAgICAgICBpZiAoIXRoaXMuaXNEaXNjb25uZWN0ZWQpIHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKHRydWUpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgICBpZiAodGhpcy5pc0Nvbm5lY3RpbmcpIHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKGZhbHNlKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgdGhpcy5pc0Nvbm5lY3RpbmcgPSB0cnVlO1xuXG5cbiAgICAgICAgICAgIC8vIGVuZCBleGlzdGluZyBjb25uZWN0aW9uXG4gICAgICAgICAgICBpZiAodGhpcy5jb25uZWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uLmVuZCgpO1xuICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgIC8vIGNvbm5lY3Rpb24gcGFyYW1ldGVyc1xuICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uID0gbXlzcWwuY3JlYXRlQ29ubmVjdGlvbih0aGlzLmNvbmZpZyk7XG5cbiAgICAgICAgICAgIC8vIGVzdGFibGlzaCBjb25uZWN0aW9uXG4gICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb24uY29ubmVjdCgoZXJyKT0+e1xuICAgICAgICAgICAgICAgIGlmIChlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignZXJyb3IgY29ubmVjdGluZzogJyArIGVyci5zdGFjayk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjb25uZWN0ZWQgYXMgaWQgJyArIHRoaXMuY29ubmVjdGlvbi50aHJlYWRJZCk7XG4gICAgICAgICAgICAgICAgcmVzb2x2ZSh0cnVlKTtcblxuXG4gICAgICAgICAgICAgICAgdGhpcy5pc0Nvbm5lY3RpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb24ub24oJ2Vycm9yJywoZXJyKT0+e1xuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9ICk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXQgaXNEaXNjb25uZWN0ZWQoKTpib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29ubmVjdGlvbiA9PSB1bmRlZmluZWQgfHxcbiAgICAgICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb24uc3RhdGUgPT09ICdkaXNjb25uZWN0ZWQnO1xuICAgIH1cblxuICAgIGVuZENvbm5lY3Rpb24oKSB7XG4gICAgICAgIHRoaXMuY29ubmVjdGlvbi5lbmQoKTtcbiAgICB9XG59Il19