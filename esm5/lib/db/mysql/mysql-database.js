/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { MySQLQueryBuilder } from './mysql-query-builder';
import { MySQLTransactionManager } from './mysql-transaction-manager';
var MySQLDatabase = /** @class */ (function () {
    function MySQLDatabase(config) {
        this.builder = new MySQLQueryBuilder(config);
        this.transactionManager = new MySQLTransactionManager(config);
    }
    return MySQLDatabase;
}());
export { MySQLDatabase };
if (false) {
    /** @type {?} */
    MySQLDatabase.prototype.builder;
    /** @type {?} */
    MySQLDatabase.prototype.transactionManager;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtZGF0YWJhc2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9kYi9teXNxbC9teXNxbC1kYXRhYmFzZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDeEQsT0FBTyxFQUFDLHVCQUF1QixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFHcEUsSUFBQTtJQUtJLHVCQUFZLE1BQW1CO1FBQzNCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSx1QkFBdUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztLQUNqRTt3QkFiTDtJQWNDLENBQUE7QUFURCx5QkFTQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElNeVNRTERhdGFiYXNlLCBJTXlTUUxDb25maWcgfSBmcm9tIFwiLi9pbnRlcmZhY2VzXCI7XG5pbXBvcnQge015U1FMUXVlcnlCdWlsZGVyfSBmcm9tICcuL215c3FsLXF1ZXJ5LWJ1aWxkZXInO1xuaW1wb3J0IHtNeVNRTFRyYW5zYWN0aW9uTWFuYWdlcn0gZnJvbSAnLi9teXNxbC10cmFuc2FjdGlvbi1tYW5hZ2VyJztcblxuXG5leHBvcnQgY2xhc3MgTXlTUUxEYXRhYmFzZSBpbXBsZW1lbnRzIElNeVNRTERhdGFiYXNlXG57XG4gICAgcmVhZG9ubHkgYnVpbGRlcjpNeVNRTFF1ZXJ5QnVpbGRlcjtcbiAgICByZWFkb25seSB0cmFuc2FjdGlvbk1hbmFnZXI6TXlTUUxUcmFuc2FjdGlvbk1hbmFnZXI7XG5cbiAgICBjb25zdHJ1Y3Rvcihjb25maWc6SU15U1FMQ29uZmlnKSB7XG4gICAgICAgIHRoaXMuYnVpbGRlciA9IG5ldyBNeVNRTFF1ZXJ5QnVpbGRlcihjb25maWcpO1xuICAgICAgICB0aGlzLnRyYW5zYWN0aW9uTWFuYWdlciA9IG5ldyBNeVNRTFRyYW5zYWN0aW9uTWFuYWdlcihjb25maWcpO1xuICAgIH1cbn0iXX0=