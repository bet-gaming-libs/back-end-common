/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { MySQLConnectionManager } from "./mysql-connection-manager";
import { MySQLExpression, SingleExpression, StringStatement, OrExpressionGroup, UpdateStatements, createExpressionGroup, ExpressionGroupJoiner } from "./mysql-statements";
import { MySQLQuery } from './mysql-query';
var MySQLQueryBuilder = /** @class */ (function () {
    function MySQLQueryBuilder(config) {
        this.connectionManager = new MySQLConnectionManager(config);
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    MySQLQueryBuilder.prototype.insert = /**
     * @template T
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    function (repository, entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.constructInsertQuery(repository, entities)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, new MySQLQuery(query, this.connectionManager)];
                }
            });
        });
    };
    /**
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    MySQLQueryBuilder.prototype.constructInsertQuery = /**
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    function (repository, entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var fields, fieldName, rows, entities_1, entities_1_1, entity, _a, _b, e_1_1, e_1, _c;
            return tslib_1.__generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        fields = [];
                        for (fieldName in entities[0]) {
                            fields.push(fieldName);
                        }
                        rows = [];
                        _d.label = 1;
                    case 1:
                        _d.trys.push([1, 6, 7, 8]);
                        entities_1 = tslib_1.__values(entities), entities_1_1 = entities_1.next();
                        _d.label = 2;
                    case 2:
                        if (!!entities_1_1.done) return [3 /*break*/, 5];
                        entity = entities_1_1.value;
                        _b = (_a = rows).push;
                        return [4 /*yield*/, this.constructRowForInsert(entity)];
                    case 3:
                        _b.apply(_a, [_d.sent()]);
                        _d.label = 4;
                    case 4:
                        entities_1_1 = entities_1.next();
                        return [3 /*break*/, 2];
                    case 5: return [3 /*break*/, 8];
                    case 6:
                        e_1_1 = _d.sent();
                        e_1 = { error: e_1_1 };
                        return [3 /*break*/, 8];
                    case 7:
                        try {
                            if (entities_1_1 && !entities_1_1.done && (_c = entities_1.return)) _c.call(entities_1);
                        }
                        finally { if (e_1) throw e_1.error; }
                        return [7 /*endfinally*/];
                    case 8: return [2 /*return*/, "INSERT INTO " + repository.tableName +
                            ' (' + this.prepareFieldList(fields) + ')' +
                            ' VALUES ' + rows.join(', ') + ';'];
                }
            });
        });
    };
    /**
     * @param {?} entity
     * @return {?}
     */
    MySQLQueryBuilder.prototype.constructRowForInsert = /**
     * @param {?} entity
     * @return {?}
     */
    function (entity) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var values, _a, _b, _i, propertyName, rawValue, preparedValue;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        values = [];
                        _a = [];
                        for (_b in entity)
                            _a.push(_b);
                        _i = 0;
                        _c.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3 /*break*/, 4];
                        propertyName = _a[_i];
                        rawValue = entity[propertyName];
                        return [4 /*yield*/, this.prepareValueForInsert(rawValue)];
                    case 2:
                        preparedValue = _c.sent();
                        values.push(preparedValue);
                        _c.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/, '(' + values.join(',') + ')'];
                }
            });
        });
    };
    /**
     * @param {?} value
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareValueForInsert = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = typeof value;
                        switch (_a) {
                            case "string": return [3 /*break*/, 1];
                        }
                        return [3 /*break*/, 6];
                    case 1:
                        _b = value;
                        switch (_b) {
                            case MySQLExpression.NULL: return [3 /*break*/, 2];
                            case MySQLExpression.NOW: return [3 /*break*/, 2];
                        }
                        return [3 /*break*/, 3];
                    case 2: return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, this.escape(value)];
                    case 4:
                        value = _c.sent();
                        return [3 /*break*/, 5];
                    case 5: return [3 /*break*/, 6];
                    case 6: return [2 /*return*/, "" + value];
                }
            });
        });
    };
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @param {?} primaryKeyValues
     * @return {?}
     */
    MySQLQueryBuilder.prototype.selectByPrimaryKeys = /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @param {?} primaryKeyValues
     * @return {?}
     */
    function (repository, fields, primaryKeyValues) {
        /** @type {?} */
        var query = this.prepareFieldsForSelect(fields, repository.tableName) +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    };
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @return {?}
     */
    MySQLQueryBuilder.prototype.selectAll = /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @return {?}
     */
    function (repository, fields) {
        /** @type {?} */
        var query = this.prepareFieldsForSelect(fields, repository.tableName) + ';';
        return new MySQLQuery(query, this.connectionManager);
    };
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    MySQLQueryBuilder.prototype.select = /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.prepareSelectStatement(params)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, new MySQLQuery(query + ";", this.connectionManager)];
                }
            });
        });
    };
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareSelectStatement = /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var fields, repository, query, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        fields = params.fields, repository = params.repository;
                        query = this.prepareFieldsForSelect(fields, repository.tableName);
                        _a = query;
                        return [4 /*yield*/, this.prepareWhereOrderAndLimit(params)];
                    case 1:
                        query = _a + _b.sent();
                        return [2 /*return*/, query];
                }
            });
        });
    };
    /**
     * @param {?} fields
     * @param {?} tableName
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareFieldsForSelect = /**
     * @param {?} fields
     * @param {?} tableName
     * @return {?}
     */
    function (fields, tableName) {
        /** @type {?} */
        var fieldsList = this.prepareFieldList(fields);
        return "SELECT " + fieldsList + " FROM " + tableName;
    };
    /**
     * @param {?} fields
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareFieldList = /**
     * @param {?} fields
     * @return {?}
     */
    function (fields) {
        /** @type {?} */
        var prepared = [];
        try {
            for (var fields_1 = tslib_1.__values(fields), fields_1_1 = fields_1.next(); !fields_1_1.done; fields_1_1 = fields_1.next()) {
                var field = fields_1_1.value;
                /** @type {?} */
                var isAlias = field.indexOf(' AS ') > -1;
                prepared.push(isAlias ?
                    field :
                    '`' + field + '`');
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (fields_1_1 && !fields_1_1.done && (_a = fields_1.return)) _a.call(fields_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
        return prepared.join(',');
        var e_2, _a;
    };
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    MySQLQueryBuilder.prototype.update = /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var statements, repository, updateStatements, remainingQuery, query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (params.singleStatement) {
                            params.statements = [
                                params.singleStatement
                            ];
                        }
                        statements = params.statements, repository = params.repository;
                        if (!statements ||
                            statements.length < 1) {
                            throw new Error('there must be at least 1 update statement!');
                        }
                        return [4 /*yield*/, this.prepareExpressionGroup(new UpdateStatements(statements))];
                    case 1:
                        updateStatements = _a.sent();
                        return [4 /*yield*/, this.prepareWhereOrderAndLimit(params)];
                    case 2:
                        remainingQuery = _a.sent();
                        query = 'UPDATE ' + repository.tableName +
                            ' SET ' + updateStatements +
                            remainingQuery + ';';
                        return [2 /*return*/, new MySQLQuery(query, this.connectionManager)];
                }
            });
        });
    };
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareWhereOrderAndLimit = /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var expressions, whereGroup, orderBy, limit, query, whereClause, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (params.whereMap) {
                            params.whereAndMap = params.whereMap;
                        }
                        if (params.whereAndMap ||
                            params.whereOrMap) {
                            params.whereGroup = createExpressionGroup(params.whereAndMap ?
                                params.whereAndMap :
                                params.whereOrMap, params.whereAndMap ?
                                ExpressionGroupJoiner.AND :
                                ExpressionGroupJoiner.OR);
                        }
                        else if (params.wherePrimaryKeys) {
                            expressions = params.wherePrimaryKeys.map(function (primaryKeyValue) { return new StringStatement(params.repository.primaryKey, '' + primaryKeyValue); });
                            params.whereGroup = new OrExpressionGroup(expressions);
                        }
                        else if (params.whereClause) {
                            params.whereGroup = new SingleExpression(params.whereClause);
                        }
                        whereGroup = params.whereGroup, orderBy = params.orderBy, limit = params.limit;
                        query = '';
                        _a = whereGroup;
                        if (!_a) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.prepareExpressionGroup(whereGroup)];
                    case 1:
                        _a = (_b.sent());
                        _b.label = 2;
                    case 2:
                        whereClause = _a;
                        if (whereClause) {
                            query += " \n                WHERE " + whereClause;
                        }
                        if (orderBy) {
                            query += " \n                ORDER BY " + orderBy;
                        }
                        if (limit) {
                            query += " \n                LIMIT " + limit;
                        }
                        return [2 /*return*/, query];
                }
            });
        });
    };
    /**
     * @param {?} group
     * @return {?}
     */
    MySQLQueryBuilder.prototype.prepareExpressionGroup = /**
     * @param {?} group
     * @return {?}
     */
    function (group) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var statements, useBrackets, _a, _b, expression, leftSide, operator, rightSide, rightSideValue, _c, _d, e_3_1, e_3, _e;
            return tslib_1.__generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        statements = [];
                        useBrackets = group.surroundExpressionWithBrackets;
                        _f.label = 1;
                    case 1:
                        _f.trys.push([1, 8, 9, 10]);
                        _a = tslib_1.__values(group.expressions), _b = _a.next();
                        _f.label = 2;
                    case 2:
                        if (!!_b.done) return [3 /*break*/, 7];
                        expression = _b.value;
                        leftSide = expression.leftSide, operator = expression.operator, rightSide = expression.rightSide;
                        if (!rightSide.isString) return [3 /*break*/, 4];
                        _d = "";
                        return [4 /*yield*/, this.escape((/** @type {?} */ (rightSide.data)))];
                    case 3:
                        _c = _d + (_f.sent());
                        return [3 /*break*/, 5];
                    case 4:
                        _c = rightSide.data;
                        _f.label = 5;
                    case 5:
                        rightSideValue = _c;
                        statements.push((useBrackets ? '(' : '') + " `" + leftSide + "` " + operator + " " + rightSideValue + " " + (useBrackets ? ')' : ''));
                        _f.label = 6;
                    case 6:
                        _b = _a.next();
                        return [3 /*break*/, 2];
                    case 7: return [3 /*break*/, 10];
                    case 8:
                        e_3_1 = _f.sent();
                        e_3 = { error: e_3_1 };
                        return [3 /*break*/, 10];
                    case 9:
                        try {
                            if (_b && !_b.done && (_e = _a.return)) _e.call(_a);
                        }
                        finally { if (e_3) throw e_3.error; }
                        return [7 /*endfinally*/];
                    case 10: return [2 /*return*/, statements.join(" " + group.joiner + " ")];
                }
            });
        });
    };
    /**
     * @param {?} value
     * @return {?}
     */
    MySQLQueryBuilder.prototype.escape = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var connection;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connectionManager.getConnection()];
                    case 1:
                        connection = _a.sent();
                        return [2 /*return*/, connection.escape(value)];
                }
            });
        });
    };
    /**
     * @param {?} repository
     * @param {?} primaryKeyValues
     * @return {?}
     */
    MySQLQueryBuilder.prototype.delete = /**
     * @param {?} repository
     * @param {?} primaryKeyValues
     * @return {?}
     */
    function (repository, primaryKeyValues) {
        if (primaryKeyValues.length < 1) {
            throw new Error('Please specify which records to delete!');
        }
        /** @type {?} */
        var query = 'DELETE FROM ' + repository.tableName +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    };
    /**
     * @template T
     * @param {?} sql
     * @return {?}
     */
    MySQLQueryBuilder.prototype.rawQuery = /**
     * @template T
     * @param {?} sql
     * @return {?}
     */
    function (sql) {
        return new MySQLQuery(sql, this.connectionManager);
    };
    return MySQLQueryBuilder;
}());
export { MySQLQueryBuilder };
if (false) {
    /** @type {?} */
    MySQLQueryBuilder.prototype.connectionManager;
}
/**
 * @param {?} repository
 * @param {?} primaryKeyValues
 * @return {?}
 */
function constructWhereWithPrimaryKeys(repository, primaryKeyValues) {
    /** @type {?} */
    var conditions = [];
    try {
        for (var primaryKeyValues_1 = tslib_1.__values(primaryKeyValues), primaryKeyValues_1_1 = primaryKeyValues_1.next(); !primaryKeyValues_1_1.done; primaryKeyValues_1_1 = primaryKeyValues_1.next()) {
            var primaryKeyValue = primaryKeyValues_1_1.value;
            if (repository.isPrimaryKeyAString) {
                primaryKeyValue = "'" + primaryKeyValue + "'";
            }
            /** @type {?} */
            var condition = repository.primaryKey + ' = ' + primaryKeyValue;
            conditions.push(condition);
        }
    }
    catch (e_4_1) { e_4 = { error: e_4_1 }; }
    finally {
        try {
            if (primaryKeyValues_1_1 && !primaryKeyValues_1_1.done && (_a = primaryKeyValues_1.return)) _a.call(primaryKeyValues_1);
        }
        finally { if (e_4) throw e_4.error; }
    }
    return ' WHERE ' + conditions.join(' OR ');
    var e_4, _a;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcXVlcnktYnVpbGRlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL215c3FsL215c3FsLXF1ZXJ5LWJ1aWxkZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFDQSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEVBQW1CLGVBQWUsRUFBRSxnQkFBZ0IsRUFBRSxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsZ0JBQWdCLEVBQUUscUJBQXFCLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUM1TCxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBR3pDLElBQUE7SUFJSSwyQkFBWSxNQUFtQjtRQUMzQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztLQUMvRDs7Ozs7OztJQUVZLGtDQUFNOzs7Ozs7Y0FDZixVQUEyQixFQUMzQixRQUFZOzs7Ozs0QkFHRSxxQkFBTSxJQUFJLENBQUMsb0JBQW9CLENBQ3pDLFVBQVUsRUFBQyxRQUFRLENBQ3RCLEVBQUE7O3dCQUZLLEtBQUssR0FBRyxTQUViO3dCQUVELHNCQUFPLElBQUksVUFBVSxDQUFvQixLQUFLLEVBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUM7Ozs7Ozs7Ozs7SUFFN0QsZ0RBQW9COzs7OztjQUM5QixVQUEyQixFQUMzQixRQUFjOzs7Ozs7d0JBR1IsTUFBTSxHQUFZLEVBQUUsQ0FBQzt3QkFDM0IsR0FBRyxDQUFDLENBQU8sU0FBUyxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ2xDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7eUJBQzFCO3dCQUVLLElBQUksR0FBWSxFQUFFLENBQUM7Ozs7d0JBQ0osYUFBQSxpQkFBQSxRQUFRLENBQUE7Ozs7d0JBQWxCLE1BQU07d0JBQ2IsS0FBQSxDQUFBLEtBQUEsSUFBSSxDQUFBLENBQUMsSUFBSSxDQUFBO3dCQUNMLHFCQUFNLElBQUksQ0FBQyxxQkFBcUIsQ0FDNUIsTUFBTSxDQUNULEVBQUE7O3dCQUhMLGNBQ0ksU0FFQyxFQUNKLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7NEJBR04sc0JBQU8sY0FBYyxHQUFHLFVBQVUsQ0FBQyxTQUFTOzRCQUN4QyxJQUFJLEdBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxHQUFFLEdBQUc7NEJBQ3hDLFVBQVUsR0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFDLEdBQUcsRUFBQTs7Ozs7Ozs7O0lBR3hCLGlEQUFxQjs7OztjQUMvQixNQUFVOzs7Ozs7d0JBR0osTUFBTSxHQUFZLEVBQUUsQ0FBQzs7bUNBRUEsTUFBTSxDQUFDOzs7Ozs7O3dCQUN4QixRQUFRLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUdsQyxxQkFBTSxJQUFJLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLEVBQUE7O3dCQUR4QyxhQUFhLEdBQ2YsU0FBMEM7d0JBRTlDLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7Ozs7OzRCQUcvQixzQkFBTyxHQUFHLEdBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRSxHQUFHLEVBQUM7Ozs7Ozs7OztJQUV4QixpREFBcUI7Ozs7Y0FBQyxLQUFTOzs7Ozs7d0JBRWpDLEtBQUEsT0FBTyxLQUFLLENBQUE7O2lDQUNYLFFBQVEsRUFBUixNQUFNLGtCQUFFOzs7O3dCQUNELEtBQUEsS0FBSyxDQUFBOztpQ0FDSixlQUFlLENBQUMsSUFBSSxFQUFwQixNQUFNLGtCQUFjO2lDQUNwQixlQUFlLENBQUMsR0FBRyxFQUFuQixNQUFNLGtCQUFhOzs7NEJBQ3hCLHdCQUFNOzRCQUdNLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUE7O3dCQUFoQyxLQUFLLEdBQUcsU0FBd0IsQ0FBQzt3QkFDckMsd0JBQU07NEJBRWQsd0JBQU07NEJBR1Ysc0JBQU8sRUFBRSxHQUFDLEtBQUssRUFBQzs7Ozs7Ozs7Ozs7O0lBSWIsK0NBQW1COzs7Ozs7O2NBQ3RCLFVBQTJCLEVBQzNCLE1BQWUsRUFDZixnQkFBa0M7O1FBR2xDLElBQU0sS0FBSyxHQUNQLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQztZQUN4RCw2QkFBNkIsQ0FBQyxVQUFVLEVBQUMsZ0JBQWdCLENBQUMsR0FBRSxHQUFHLENBQUM7UUFFcEUsTUFBTSxDQUFDLElBQUksVUFBVSxDQUFNLEtBQUssRUFBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzs7Ozs7Ozs7SUFHdEQscUNBQVM7Ozs7OztjQUNaLFVBQTJCLEVBQzNCLE1BQWU7O1FBRWYsSUFBTSxLQUFLLEdBQ1AsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sRUFBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsR0FBRyxDQUFDO1FBRW5FLE1BQU0sQ0FBQyxJQUFJLFVBQVUsQ0FBTSxLQUFLLEVBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7Ozs7Ozs7SUFJaEQsa0NBQU07Ozs7O2NBQUksTUFBbUM7Ozs7OzRCQUN4QyxxQkFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUFqRCxLQUFLLEdBQUcsU0FBeUM7d0JBRXZELHNCQUFPLElBQUksVUFBVSxDQUFTLEtBQUssTUFBRyxFQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFDOzs7Ozs7Ozs7O0lBR3JELGtEQUFzQjs7Ozs7Y0FBSSxNQUFtQzs7Ozs7O3dCQUVoRSxNQUFNLEdBQWUsTUFBTSxPQUFyQixFQUFDLFVBQVUsR0FBSSxNQUFNLFdBQVYsQ0FBVzt3QkFFL0IsS0FBSyxHQUFVLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dCQUU1RSxLQUFBLEtBQUssQ0FBQTt3QkFBSSxxQkFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUFyRCxLQUFLLEdBQUwsS0FBUyxTQUE0QyxDQUFDO3dCQUV0RCxzQkFBTyxLQUFLLEVBQUM7Ozs7Ozs7Ozs7SUFHVCxrREFBc0I7Ozs7O2NBQUMsTUFBZSxFQUFDLFNBQWdCOztRQUMzRCxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFakQsTUFBTSxDQUFDLFlBQVUsVUFBVSxjQUFTLFNBQVcsQ0FBQzs7Ozs7O0lBSTVDLDRDQUFnQjs7OztjQUFDLE1BQWU7O1FBRXBDLElBQU0sUUFBUSxHQUFZLEVBQUUsQ0FBQzs7WUFFN0IsR0FBRyxDQUFDLENBQWdCLElBQUEsV0FBQSxpQkFBQSxNQUFNLENBQUEsOEJBQUE7Z0JBQXJCLElBQU0sS0FBSyxtQkFBQTs7Z0JBQ1osSUFBTSxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFFM0MsUUFBUSxDQUFDLElBQUksQ0FDVCxPQUFPLENBQUMsQ0FBQztvQkFDVCxLQUFLLENBQUMsQ0FBQztvQkFDUCxHQUFHLEdBQUMsS0FBSyxHQUFDLEdBQUcsQ0FDaEIsQ0FBQzthQUNMOzs7Ozs7Ozs7UUFFRCxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzs7Ozs7Ozs7SUFHakIsa0NBQU07Ozs7O2NBQUksTUFBbUM7Ozs7Ozt3QkFDdEQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7NEJBQ3pCLE1BQU0sQ0FBQyxVQUFVLEdBQUc7Z0NBQ2hCLE1BQU0sQ0FBQyxlQUFlOzZCQUN6QixDQUFDO3lCQUNMO3dCQUdNLFVBQVUsR0FBZSxNQUFNLFdBQXJCLEVBQUMsVUFBVSxHQUFJLE1BQU0sV0FBVixDQUFXO3dCQUV2QyxFQUFFLENBQUMsQ0FDQyxDQUFDLFVBQVU7NEJBQ1gsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUN4QixDQUFDLENBQUMsQ0FBQzs0QkFDQyxNQUFNLElBQUksS0FBSyxDQUFDLDRDQUE0QyxDQUFDLENBQUM7eUJBQ2pFO3dCQUcrQixxQkFBTSxJQUFJLENBQUMsc0JBQXNCLENBQzdELElBQUksZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQ25DLEVBQUE7O3dCQUZLLGdCQUFnQixHQUFVLFNBRS9CO3dCQUU2QixxQkFBTSxJQUFJLENBQUMseUJBQXlCLENBQzlELE1BQU0sQ0FDVCxFQUFBOzt3QkFGSyxjQUFjLEdBQVUsU0FFN0I7d0JBRUssS0FBSyxHQUNQLFNBQVMsR0FBQyxVQUFVLENBQUMsU0FBUzs0QkFDOUIsT0FBTyxHQUFFLGdCQUFnQjs0QkFDeEIsY0FBYyxHQUFFLEdBQUcsQ0FBQzt3QkFFekIsc0JBQU8sSUFBSSxVQUFVLENBQW9CLEtBQUssRUFBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsRUFBQzs7Ozs7Ozs7OztJQUc3RCxxREFBeUI7Ozs7O2NBQUksTUFBNkI7Ozs7Ozt3QkFDcEUsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7NEJBQ2xCLE1BQU0sQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQzt5QkFDeEM7d0JBRUQsRUFBRSxDQUFDLENBQ0MsTUFBTSxDQUFDLFdBQVc7NEJBQ2xCLE1BQU0sQ0FBQyxVQUNYLENBQUMsQ0FBQyxDQUFDOzRCQUNDLE1BQU0sQ0FBQyxVQUFVLEdBQUcscUJBQXFCLENBQ3JDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztnQ0FDaEIsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dDQUNwQixNQUFNLENBQUMsVUFBVSxFQUNyQixNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7Z0NBQ2hCLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxDQUFDO2dDQUMzQixxQkFBcUIsQ0FBQyxFQUFFLENBQy9CLENBQUM7eUJBQ0w7d0JBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7NEJBQ3pCLFdBQVcsR0FBcUIsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FDN0QsVUFBQyxlQUFlLElBQUssT0FBQSxJQUFJLGVBQWUsQ0FDcEMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQzVCLEVBQUUsR0FBQyxlQUFlLENBQ3JCLEVBSG9CLENBR3BCLENBQ0osQ0FBQzs0QkFFRixNQUFNLENBQUMsVUFBVSxHQUFHLElBQUksaUJBQWlCLENBQUMsV0FBVyxDQUFDLENBQUM7eUJBQzFEO3dCQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQzs0QkFDMUIsTUFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLGdCQUFnQixDQUNwQyxNQUFNLENBQUMsV0FBVyxDQUNyQixDQUFDO3lCQUNMO3dCQUdNLFVBQVUsR0FBa0IsTUFBTSxXQUF4QixFQUFDLE9BQU8sR0FBVSxNQUFNLFFBQWhCLEVBQUMsS0FBSyxHQUFJLE1BQU0sTUFBVixDQUFXO3dCQUV0QyxLQUFLLEdBQUcsRUFBRSxDQUFDO3dCQUdYLEtBQUEsVUFBVSxDQUFBO2lDQUFWLHdCQUFVO3dCQUNWLHFCQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsRUFBQTs7OEJBQTdDLFNBQTZDOzs7d0JBRjNDLFdBQVc7d0JBSWpCLEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7NEJBQ2QsS0FBSyxJQUFJLDhCQUNHLFdBQWEsQ0FBQzt5QkFDN0I7d0JBRUQsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzs0QkFDVixLQUFLLElBQUksaUNBQ00sT0FBUyxDQUFDO3lCQUM1Qjt3QkFFRCxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDOzRCQUNSLEtBQUssSUFBSSw4QkFDRyxLQUFPLENBQUM7eUJBQ3ZCO3dCQUVELHNCQUFPLEtBQUssRUFBQzs7Ozs7Ozs7O0lBR0gsa0RBQXNCOzs7O2NBQUMsS0FBcUI7Ozs7Ozt3QkFDaEQsVUFBVSxHQUFZLEVBQUUsQ0FBQzt3QkFFekIsV0FBVyxHQUFHLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQzs7Ozt3QkFFaEMsS0FBQSxpQkFBQSxLQUFLLENBQUMsV0FBVyxDQUFBOzs7O3dCQUEvQixVQUFVO3dCQUNWLFFBQVEsR0FBdUIsVUFBVSxTQUFqQyxFQUFDLFFBQVEsR0FBYyxVQUFVLFNBQXhCLEVBQUMsU0FBUyxHQUFJLFVBQVUsVUFBZCxDQUFlOzZCQUc3QyxTQUFTLENBQUMsUUFBUSxFQUFsQix3QkFBa0I7O3dCQUNWLHFCQUFNLElBQUksQ0FBQyxNQUFNLG9CQUFDLFNBQVMsQ0FBQyxJQUFjLEdBQUMsRUFBQTs7d0JBQS9DLEtBQUEsTUFBSSxTQUEyQyxDQUFHLENBQUE7Ozt3QkFDbEQsS0FBQSxTQUFTLENBQUMsSUFBSSxDQUFBOzs7d0JBSGhCLGNBQWM7d0JBS3BCLFVBQVUsQ0FBQyxJQUFJLENBQ1gsQ0FDSSxXQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxXQUNwQixRQUFRLFVBQU0sUUFBUSxTQUFJLGNBQWMsVUFDMUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FDeEIsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7OzZCQUdaLHNCQUFPLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBSSxLQUFLLENBQUMsTUFBTSxNQUFHLENBQUMsRUFBQzs7Ozs7Ozs7O0lBSWxDLGtDQUFNOzs7O2NBQUMsS0FBWTs7Ozs7NEJBRVYscUJBQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxFQUFBOzt3QkFBekQsVUFBVSxHQUFHLFNBQTRDO3dCQUUvRCxzQkFBTyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFDOzs7Ozs7Ozs7O0lBRzdCLGtDQUFNOzs7OztjQUNULFVBQTJCLEVBQzNCLGdCQUFzQjtRQUd0QixFQUFFLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM5QixNQUFNLElBQUksS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUM7U0FDOUQ7O1FBR0QsSUFBTSxLQUFLLEdBQUcsY0FBYyxHQUFDLFVBQVUsQ0FBQyxTQUFTO1lBQ3JDLDZCQUE2QixDQUFDLFVBQVUsRUFBQyxnQkFBZ0IsQ0FBQyxHQUFDLEdBQUcsQ0FBQztRQUUzRSxNQUFNLENBQUMsSUFBSSxVQUFVLENBQW9CLEtBQUssRUFBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzs7Ozs7OztJQUczRSxvQ0FBUTs7Ozs7SUFBUixVQUFZLEdBQVc7UUFDbkIsTUFBTSxDQUFDLElBQUksVUFBVSxDQUFJLEdBQUcsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztLQUN6RDs0QkF4U0w7SUF5U0MsQ0FBQTtBQW5TRCw2QkFtU0M7Ozs7Ozs7Ozs7QUFHRCx1Q0FDSSxVQUEyQixFQUMzQixnQkFBc0I7O0lBR3RCLElBQU0sVUFBVSxHQUFZLEVBQUUsQ0FBQzs7UUFFL0IsR0FBRyxDQUFDLENBQXdCLElBQUEscUJBQUEsaUJBQUEsZ0JBQWdCLENBQUEsa0RBQUE7WUFBdkMsSUFBSSxlQUFlLDZCQUFBO1lBQ3BCLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLGVBQWUsR0FBRyxHQUFHLEdBQUMsZUFBZSxHQUFDLEdBQUcsQ0FBQzthQUM3Qzs7WUFFRCxJQUFNLFNBQVMsR0FBRyxVQUFVLENBQUMsVUFBVSxHQUFHLEtBQUssR0FBRyxlQUFlLENBQUM7WUFDbEUsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUM5Qjs7Ozs7Ozs7O0lBRUQsTUFBTSxDQUFDLFNBQVMsR0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDOztDQUM1QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElNeVNRTFJlcG9zaXRvcnksIFByaW1hcnlLZXlWYWx1ZSwgSU15U1FMUXVlcnlSZXN1bHQsIElNeVNRTENvbmZpZywgSVNlbGVjdFF1ZXJ5QnVpbGRlclBhcmFtcywgSVVwZGF0ZVF1ZXJ5QnVpbGRlclBhcmFtcywgSVF1ZXJ5QnVpbGRlclBhcmFtcyB9IGZyb20gXCIuL2ludGVyZmFjZXNcIjtcbmltcG9ydCB7IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIgfSBmcm9tIFwiLi9teXNxbC1jb25uZWN0aW9uLW1hbmFnZXJcIjtcbmltcG9ydCB7IEV4cHJlc3Npb25Hcm91cCwgTXlTUUxFeHByZXNzaW9uLCBTaW5nbGVFeHByZXNzaW9uLCBTdHJpbmdTdGF0ZW1lbnQsIE9yRXhwcmVzc2lvbkdyb3VwLCBVcGRhdGVTdGF0ZW1lbnRzLCBjcmVhdGVFeHByZXNzaW9uR3JvdXAsIEV4cHJlc3Npb25Hcm91cEpvaW5lciB9IGZyb20gXCIuL215c3FsLXN0YXRlbWVudHNcIjtcbmltcG9ydCB7TXlTUUxRdWVyeX0gZnJvbSAnLi9teXNxbC1xdWVyeSc7XG5cblxuZXhwb3J0IGNsYXNzIE15U1FMUXVlcnlCdWlsZGVyXG57XG4gICAgcHJpdmF0ZSBjb25uZWN0aW9uTWFuYWdlcjpNeVNRTENvbm5lY3Rpb25NYW5hZ2VyO1xuXG4gICAgY29uc3RydWN0b3IoY29uZmlnOklNeVNRTENvbmZpZykge1xuICAgICAgICB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyID0gbmV3IE15U1FMQ29ubmVjdGlvbk1hbmFnZXIoY29uZmlnKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgYXN5bmMgaW5zZXJ0PFQ+KFxuICAgICAgICByZXBvc2l0b3J5OklNeVNRTFJlcG9zaXRvcnksXG4gICAgICAgIGVudGl0aWVzOlRbXVxuICAgICkge1xuICAgICAgICAvLyBjb25zdHJ1Y3QgcXVlcnlcbiAgICAgICAgY29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLmNvbnN0cnVjdEluc2VydFF1ZXJ5KFxuICAgICAgICAgICAgcmVwb3NpdG9yeSxlbnRpdGllc1xuICAgICAgICApO1xuXG4gICAgICAgIHJldHVybiBuZXcgTXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4ocXVlcnksdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxuICAgIHByaXZhdGUgYXN5bmMgY29uc3RydWN0SW5zZXJ0UXVlcnkoXG4gICAgICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeSxcbiAgICAgICAgZW50aXRpZXM6YW55W11cbiAgICApOlByb21pc2U8c3RyaW5nPlxuICAgIHtcbiAgICAgICAgY29uc3QgZmllbGRzOnN0cmluZ1tdID0gW107XG4gICAgICAgIGZvciAoY29uc3QgZmllbGROYW1lIGluIGVudGl0aWVzWzBdKSB7XG4gICAgICAgICAgICBmaWVsZHMucHVzaChmaWVsZE5hbWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3Qgcm93czpzdHJpbmdbXSA9IFtdO1xuICAgICAgICBmb3IgKGNvbnN0IGVudGl0eSBvZiBlbnRpdGllcykge1xuICAgICAgICAgICAgcm93cy5wdXNoKFxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuY29uc3RydWN0Um93Rm9ySW5zZXJ0KFxuICAgICAgICAgICAgICAgICAgICBlbnRpdHlcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIFwiSU5TRVJUIElOVE8gXCIgKyByZXBvc2l0b3J5LnRhYmxlTmFtZSArXG4gICAgICAgICAgICAnICgnKyB0aGlzLnByZXBhcmVGaWVsZExpc3QoZmllbGRzKSArJyknICtcbiAgICAgICAgICAgICcgVkFMVUVTICcrcm93cy5qb2luKCcsICcpKyc7J1xuXG4gICAgfVxuICAgIHByaXZhdGUgYXN5bmMgY29uc3RydWN0Um93Rm9ySW5zZXJ0KFxuICAgICAgICBlbnRpdHk6YW55XG4gICAgKTpQcm9taXNlPHN0cmluZz5cbiAgICB7XG4gICAgICAgIGNvbnN0IHZhbHVlczpzdHJpbmdbXSA9IFtdO1xuXG4gICAgICAgIGZvciAoY29uc3QgcHJvcGVydHlOYW1lIGluIGVudGl0eSkge1xuICAgICAgICAgICAgY29uc3QgcmF3VmFsdWUgPSBlbnRpdHlbcHJvcGVydHlOYW1lXTtcblxuICAgICAgICAgICAgY29uc3QgcHJlcGFyZWRWYWx1ZTpzdHJpbmcgPVxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMucHJlcGFyZVZhbHVlRm9ySW5zZXJ0KHJhd1ZhbHVlKTtcblxuICAgICAgICAgICAgdmFsdWVzLnB1c2gocHJlcGFyZWRWYWx1ZSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gJygnKyB2YWx1ZXMuam9pbignLCcpICsnKSc7XG4gICAgfVxuICAgIHByaXZhdGUgYXN5bmMgcHJlcGFyZVZhbHVlRm9ySW5zZXJ0KHZhbHVlOmFueSk6UHJvbWlzZTxzdHJpbmc+XG4gICAge1xuICAgICAgICBzd2l0Y2ggKHR5cGVvZiB2YWx1ZSkge1xuICAgICAgICAgICAgY2FzZSBcInN0cmluZ1wiOlxuICAgICAgICAgICAgICAgIHN3aXRjaCAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBNeVNRTEV4cHJlc3Npb24uTlVMTDpcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBNeVNRTEV4cHJlc3Npb24uTk9XOlxuICAgICAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSBhd2FpdCB0aGlzLmVzY2FwZSh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIFwiXCIrdmFsdWU7XG4gICAgfVxuXG5cbiAgICBwdWJsaWMgc2VsZWN0QnlQcmltYXJ5S2V5czxUPihcbiAgICAgICAgcmVwb3NpdG9yeTpJTXlTUUxSZXBvc2l0b3J5LFxuICAgICAgICBmaWVsZHM6c3RyaW5nW10sXG4gICAgICAgIHByaW1hcnlLZXlWYWx1ZXM6UHJpbWFyeUtleVZhbHVlW11cbiAgICApXG4gICAge1xuICAgICAgICBjb25zdCBxdWVyeSA9IFxuICAgICAgICAgICAgdGhpcy5wcmVwYXJlRmllbGRzRm9yU2VsZWN0KGZpZWxkcyxyZXBvc2l0b3J5LnRhYmxlTmFtZSkgK1xuICAgICAgICAgICAgY29uc3RydWN0V2hlcmVXaXRoUHJpbWFyeUtleXMocmVwb3NpdG9yeSxwcmltYXJ5S2V5VmFsdWVzKSArJzsnO1xuXG4gICAgICAgIHJldHVybiBuZXcgTXlTUUxRdWVyeTxUW10+KHF1ZXJ5LHRoaXMuY29ubmVjdGlvbk1hbmFnZXIpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzZWxlY3RBbGw8VD4oXG4gICAgICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeSxcbiAgICAgICAgZmllbGRzOnN0cmluZ1tdLFxuICAgICkge1xuICAgICAgICBjb25zdCBxdWVyeSA9XG4gICAgICAgICAgICB0aGlzLnByZXBhcmVGaWVsZHNGb3JTZWxlY3QoZmllbGRzLHJlcG9zaXRvcnkudGFibGVOYW1lKSArICc7JztcblxuICAgICAgICByZXR1cm4gbmV3IE15U1FMUXVlcnk8VFtdPihxdWVyeSx0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyKTtcbiAgICB9XG5cblxuICAgIHB1YmxpYyBhc3luYyBzZWxlY3Q8VD4ocGFyYW1zOklTZWxlY3RRdWVyeUJ1aWxkZXJQYXJhbXM8VD4pIHtcbiAgICAgICAgY29uc3QgcXVlcnkgPSBhd2FpdCB0aGlzLnByZXBhcmVTZWxlY3RTdGF0ZW1lbnQocGFyYW1zKTtcblxuICAgICAgICByZXR1cm4gbmV3IE15U1FMUXVlcnk8VFtdPihgJHtxdWVyeX07YCx0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGFzeW5jIHByZXBhcmVTZWxlY3RTdGF0ZW1lbnQ8VD4ocGFyYW1zOklTZWxlY3RRdWVyeUJ1aWxkZXJQYXJhbXM8VD4pOlByb21pc2U8c3RyaW5nPlxuICAgIHtcbiAgICAgICAgY29uc3Qge2ZpZWxkcyxyZXBvc2l0b3J5fSA9IHBhcmFtcztcblxuICAgICAgICBsZXQgcXVlcnk6c3RyaW5nID0gdGhpcy5wcmVwYXJlRmllbGRzRm9yU2VsZWN0KGZpZWxkcyxyZXBvc2l0b3J5LnRhYmxlTmFtZSk7XG5cbiAgICAgICAgcXVlcnkgKz0gYXdhaXQgdGhpcy5wcmVwYXJlV2hlcmVPcmRlckFuZExpbWl0KHBhcmFtcyk7XG5cbiAgICAgICAgcmV0dXJuIHF1ZXJ5O1xuICAgIH1cblxuICAgIHByaXZhdGUgcHJlcGFyZUZpZWxkc0ZvclNlbGVjdChmaWVsZHM6c3RyaW5nW10sdGFibGVOYW1lOnN0cmluZyk6c3RyaW5nIHtcbiAgICAgICAgY29uc3QgZmllbGRzTGlzdCA9IHRoaXMucHJlcGFyZUZpZWxkTGlzdChmaWVsZHMpO1xuXG4gICAgICAgIHJldHVybiBgU0VMRUNUICR7ZmllbGRzTGlzdH0gRlJPTSAke3RhYmxlTmFtZX1gO1xuICAgIH1cblxuXG4gICAgcHJpdmF0ZSBwcmVwYXJlRmllbGRMaXN0KGZpZWxkczpzdHJpbmdbXSk6c3RyaW5nXG4gICAge1xuICAgICAgICBjb25zdCBwcmVwYXJlZDpzdHJpbmdbXSA9IFtdO1xuXG4gICAgICAgIGZvciAoY29uc3QgZmllbGQgb2YgZmllbGRzKSB7XG4gICAgICAgICAgICBjb25zdCBpc0FsaWFzID0gZmllbGQuaW5kZXhPZignIEFTICcpID4gLTE7XG5cbiAgICAgICAgICAgIHByZXBhcmVkLnB1c2goXG4gICAgICAgICAgICAgICAgaXNBbGlhcyA/XG4gICAgICAgICAgICAgICAgZmllbGQgOlxuICAgICAgICAgICAgICAgICdgJytmaWVsZCsnYCdcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gcHJlcGFyZWQuam9pbignLCcpO1xuICAgIH1cblxuICAgIHB1YmxpYyBhc3luYyB1cGRhdGU8VD4ocGFyYW1zOklVcGRhdGVRdWVyeUJ1aWxkZXJQYXJhbXM8VD4pIHtcbiAgICAgICAgaWYgKHBhcmFtcy5zaW5nbGVTdGF0ZW1lbnQpIHtcbiAgICAgICAgICAgIHBhcmFtcy5zdGF0ZW1lbnRzID0gW1xuICAgICAgICAgICAgICAgIHBhcmFtcy5zaW5nbGVTdGF0ZW1lbnRcbiAgICAgICAgICAgIF07XG4gICAgICAgIH1cblxuXG4gICAgICAgIGNvbnN0IHtzdGF0ZW1lbnRzLHJlcG9zaXRvcnl9ID0gcGFyYW1zO1xuXG4gICAgICAgIGlmIChcbiAgICAgICAgICAgICFzdGF0ZW1lbnRzIHx8XG4gICAgICAgICAgICBzdGF0ZW1lbnRzLmxlbmd0aCA8IDFcbiAgICAgICAgKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ3RoZXJlIG11c3QgYmUgYXQgbGVhc3QgMSB1cGRhdGUgc3RhdGVtZW50IScpO1xuICAgICAgICB9XG5cblxuICAgICAgICBjb25zdCB1cGRhdGVTdGF0ZW1lbnRzOnN0cmluZyA9IGF3YWl0IHRoaXMucHJlcGFyZUV4cHJlc3Npb25Hcm91cChcbiAgICAgICAgICAgIG5ldyBVcGRhdGVTdGF0ZW1lbnRzKHN0YXRlbWVudHMpXG4gICAgICAgICk7XG5cbiAgICAgICAgY29uc3QgcmVtYWluaW5nUXVlcnk6c3RyaW5nID0gYXdhaXQgdGhpcy5wcmVwYXJlV2hlcmVPcmRlckFuZExpbWl0KFxuICAgICAgICAgICAgcGFyYW1zXG4gICAgICAgICk7XG5cbiAgICAgICAgY29uc3QgcXVlcnk6c3RyaW5nID0gXG4gICAgICAgICAgICAnVVBEQVRFICcrcmVwb3NpdG9yeS50YWJsZU5hbWUrXG4gICAgICAgICAgICAnIFNFVCAnKyB1cGRhdGVTdGF0ZW1lbnRzICtcbiAgICAgICAgICAgICByZW1haW5pbmdRdWVyeSArJzsnO1xuXG4gICAgICAgIHJldHVybiBuZXcgTXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4ocXVlcnksdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhc3luYyBwcmVwYXJlV2hlcmVPcmRlckFuZExpbWl0PFQ+KHBhcmFtczpJUXVlcnlCdWlsZGVyUGFyYW1zPFQ+KSB7XG4gICAgICAgIGlmIChwYXJhbXMud2hlcmVNYXApIHtcbiAgICAgICAgICAgIHBhcmFtcy53aGVyZUFuZE1hcCA9IHBhcmFtcy53aGVyZU1hcDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIHBhcmFtcy53aGVyZUFuZE1hcCB8fFxuICAgICAgICAgICAgcGFyYW1zLndoZXJlT3JNYXBcbiAgICAgICAgKSB7XG4gICAgICAgICAgICBwYXJhbXMud2hlcmVHcm91cCA9IGNyZWF0ZUV4cHJlc3Npb25Hcm91cChcbiAgICAgICAgICAgICAgICBwYXJhbXMud2hlcmVBbmRNYXAgP1xuICAgICAgICAgICAgICAgICAgICBwYXJhbXMud2hlcmVBbmRNYXAgOlxuICAgICAgICAgICAgICAgICAgICBwYXJhbXMud2hlcmVPck1hcCxcbiAgICAgICAgICAgICAgICBwYXJhbXMud2hlcmVBbmRNYXAgP1xuICAgICAgICAgICAgICAgICAgICBFeHByZXNzaW9uR3JvdXBKb2luZXIuQU5EIDpcbiAgICAgICAgICAgICAgICAgICAgRXhwcmVzc2lvbkdyb3VwSm9pbmVyLk9SXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHBhcmFtcy53aGVyZVByaW1hcnlLZXlzKSB7XG4gICAgICAgICAgICBjb25zdCBleHByZXNzaW9uczpTdHJpbmdTdGF0ZW1lbnRbXSA9IHBhcmFtcy53aGVyZVByaW1hcnlLZXlzLm1hcChcbiAgICAgICAgICAgICAgICAocHJpbWFyeUtleVZhbHVlKSA9PiBuZXcgU3RyaW5nU3RhdGVtZW50KFxuICAgICAgICAgICAgICAgICAgICBwYXJhbXMucmVwb3NpdG9yeS5wcmltYXJ5S2V5LFxuICAgICAgICAgICAgICAgICAgICAnJytwcmltYXJ5S2V5VmFsdWVcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApO1xuXG4gICAgICAgICAgICBwYXJhbXMud2hlcmVHcm91cCA9IG5ldyBPckV4cHJlc3Npb25Hcm91cChleHByZXNzaW9ucyk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAocGFyYW1zLndoZXJlQ2xhdXNlKSB7XG4gICAgICAgICAgICBwYXJhbXMud2hlcmVHcm91cCA9IG5ldyBTaW5nbGVFeHByZXNzaW9uKFxuICAgICAgICAgICAgICAgIHBhcmFtcy53aGVyZUNsYXVzZVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgY29uc3Qge3doZXJlR3JvdXAsb3JkZXJCeSxsaW1pdH0gPSBwYXJhbXM7XG5cbiAgICAgICAgbGV0IHF1ZXJ5ID0gJyc7XG5cbiAgICAgICAgY29uc3Qgd2hlcmVDbGF1c2UgPSBcbiAgICAgICAgICAgIHdoZXJlR3JvdXAgJiYgXG4gICAgICAgICAgICBhd2FpdCB0aGlzLnByZXBhcmVFeHByZXNzaW9uR3JvdXAod2hlcmVHcm91cCk7XG5cbiAgICAgICAgaWYgKHdoZXJlQ2xhdXNlKSB7XG4gICAgICAgICAgICBxdWVyeSArPSBgIFxuICAgICAgICAgICAgICAgIFdIRVJFICR7d2hlcmVDbGF1c2V9YDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChvcmRlckJ5KSB7XG4gICAgICAgICAgICBxdWVyeSArPSBgIFxuICAgICAgICAgICAgICAgIE9SREVSIEJZICR7b3JkZXJCeX1gO1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBpZiAobGltaXQpIHtcbiAgICAgICAgICAgIHF1ZXJ5ICs9IGAgXG4gICAgICAgICAgICAgICAgTElNSVQgJHtsaW1pdH1gO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHF1ZXJ5O1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgcHJlcGFyZUV4cHJlc3Npb25Hcm91cChncm91cDpFeHByZXNzaW9uR3JvdXApOlByb21pc2U8c3RyaW5nPiB7XG4gICAgICAgIGNvbnN0IHN0YXRlbWVudHM6c3RyaW5nW10gPSBbXTtcblxuICAgICAgICBjb25zdCB1c2VCcmFja2V0cyA9IGdyb3VwLnN1cnJvdW5kRXhwcmVzc2lvbldpdGhCcmFja2V0cztcblxuICAgICAgICBmb3IgKGNvbnN0IGV4cHJlc3Npb24gb2YgZ3JvdXAuZXhwcmVzc2lvbnMpIHtcbiAgICAgICAgICAgIGNvbnN0IHtsZWZ0U2lkZSxvcGVyYXRvcixyaWdodFNpZGV9ID0gZXhwcmVzc2lvbjtcblxuICAgICAgICAgICAgY29uc3QgcmlnaHRTaWRlVmFsdWUgPVxuICAgICAgICAgICAgICAgIHJpZ2h0U2lkZS5pc1N0cmluZyA/XG4gICAgICAgICAgICAgICAgICAgIGAkeyBhd2FpdCB0aGlzLmVzY2FwZShyaWdodFNpZGUuZGF0YSBhcyBzdHJpbmcpIH1gIDpcbiAgICAgICAgICAgICAgICAgICAgcmlnaHRTaWRlLmRhdGE7XG5cbiAgICAgICAgICAgIHN0YXRlbWVudHMucHVzaChcbiAgICAgICAgICAgICAgICBgJHsgXG4gICAgICAgICAgICAgICAgICAgIHVzZUJyYWNrZXRzID8gJygnIDogJydcbiAgICAgICAgICAgICAgICB9IFxcYCR7bGVmdFNpZGV9XFxgICR7b3BlcmF0b3J9ICR7cmlnaHRTaWRlVmFsdWV9ICR7XG4gICAgICAgICAgICAgICAgICAgIHVzZUJyYWNrZXRzID8gJyknIDogJydcbiAgICAgICAgICAgICAgICB9YCk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gc3RhdGVtZW50cy5qb2luKGAgJHtncm91cC5qb2luZXJ9IGApO1xuICAgIH1cblxuXG4gICAgcHJpdmF0ZSBhc3luYyBlc2NhcGUodmFsdWU6c3RyaW5nKTpQcm9taXNlPHN0cmluZz5cbiAgICB7XG4gICAgICAgIGNvbnN0IGNvbm5lY3Rpb24gPSBhd2FpdCB0aGlzLmNvbm5lY3Rpb25NYW5hZ2VyLmdldENvbm5lY3Rpb24oKTtcblxuICAgICAgICByZXR1cm4gY29ubmVjdGlvbi5lc2NhcGUodmFsdWUpO1xuICAgIH1cblxuICAgIHB1YmxpYyBkZWxldGUoXG4gICAgICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeSxcbiAgICAgICAgcHJpbWFyeUtleVZhbHVlczphbnlbXVxuICAgIClcbiAgICB7XG4gICAgICAgIGlmIChwcmltYXJ5S2V5VmFsdWVzLmxlbmd0aCA8IDEpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignUGxlYXNlIHNwZWNpZnkgd2hpY2ggcmVjb3JkcyB0byBkZWxldGUhJyk7XG4gICAgICAgIH1cblxuXG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gJ0RFTEVURSBGUk9NICcrcmVwb3NpdG9yeS50YWJsZU5hbWUrXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0cnVjdFdoZXJlV2l0aFByaW1hcnlLZXlzKHJlcG9zaXRvcnkscHJpbWFyeUtleVZhbHVlcykrJzsnO1xuXG4gICAgICAgIHJldHVybiBuZXcgTXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4ocXVlcnksdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxuXG4gICAgcmF3UXVlcnk8VD4oc3FsOiBzdHJpbmcpOiBNeVNRTFF1ZXJ5PFQ+IHtcbiAgICAgICAgcmV0dXJuIG5ldyBNeVNRTFF1ZXJ5PFQ+KHNxbCwgdGhpcy5jb25uZWN0aW9uTWFuYWdlcik7XG4gICAgfVxufVxuXG5cbmZ1bmN0aW9uIGNvbnN0cnVjdFdoZXJlV2l0aFByaW1hcnlLZXlzKFxuICAgIHJlcG9zaXRvcnk6SU15U1FMUmVwb3NpdG9yeSxcbiAgICBwcmltYXJ5S2V5VmFsdWVzOmFueVtdXG4pOnN0cmluZ1xue1xuICAgIGNvbnN0IGNvbmRpdGlvbnM6c3RyaW5nW10gPSBbXTtcblxuICAgIGZvciAobGV0IHByaW1hcnlLZXlWYWx1ZSBvZiBwcmltYXJ5S2V5VmFsdWVzKSB7XG4gICAgICAgIGlmIChyZXBvc2l0b3J5LmlzUHJpbWFyeUtleUFTdHJpbmcpIHtcbiAgICAgICAgICAgIHByaW1hcnlLZXlWYWx1ZSA9IFwiJ1wiK3ByaW1hcnlLZXlWYWx1ZStcIidcIjtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGNvbmRpdGlvbiA9IHJlcG9zaXRvcnkucHJpbWFyeUtleSArICcgPSAnICsgcHJpbWFyeUtleVZhbHVlO1xuICAgICAgICBjb25kaXRpb25zLnB1c2goY29uZGl0aW9uKTtcbiAgICB9XG5cbiAgICByZXR1cm4gJyBXSEVSRSAnK2NvbmRpdGlvbnMuam9pbignIE9SICcpO1xufSJdfQ==