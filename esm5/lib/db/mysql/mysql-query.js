/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/** @type {?} */
var logSqlQueries = process.env["LOG_SQL_QUERIES"] === 'true';
/**
 * @template T
 */
var /**
 * @template T
 */
MySQLQuery = /** @class */ (function () {
    function MySQLQuery(sql, connectionManager) {
        this.sql = sql;
        this.connectionManager = connectionManager;
    }
    /**
     * @return {?}
     */
    MySQLQuery.prototype.execute = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            var connection;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connectionManager.getConnection()];
                    case 1:
                        connection = _a.sent();
                        return [2 /*return*/, new Promise(function (resolve, reject) {
                                if (logSqlQueries) {
                                    console.log(_this.sql);
                                }
                                connection.query(_this.sql, function (error, result, fields) {
                                    if (error) {
                                        //console.log('*** MySQL Error: ***');
                                        //console.log(err);
                                        //throw error;
                                        reject(error);
                                    }
                                    else {
                                        resolve(result);
                                    }
                                });
                            })];
                }
            });
        });
    };
    return MySQLQuery;
}());
/**
 * @template T
 */
export { MySQLQuery };
if (false) {
    /** @type {?} */
    MySQLQuery.prototype.sql;
    /** @type {?} */
    MySQLQuery.prototype.connectionManager;
}
var MySQLTransactionQuery = /** @class */ (function (_super) {
    tslib_1.__extends(MySQLTransactionQuery, _super);
    function MySQLTransactionQuery() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return MySQLTransactionQuery;
}(MySQLQuery));
export { MySQLTransactionQuery };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcXVlcnkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9kYi9teXNxbC9teXNxbC1xdWVyeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFJQSxJQUFNLGFBQWEsR0FDZixPQUFPLENBQUMsR0FBRyx3QkFBcUIsTUFBTSxDQUFDOzs7O0FBRzNDOzs7QUFBQTtJQUVJLG9CQUNhLEdBQVUsRUFDWDtRQURDLFFBQUcsR0FBSCxHQUFHLENBQU87UUFDWCxzQkFBaUIsR0FBakIsaUJBQWlCO0tBRTVCOzs7O0lBRUssNEJBQU87OztJQUFiOzs7Ozs7NEJBR3VCLHFCQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsRUFBQTs7d0JBQXpELFVBQVUsR0FBRyxTQUE0Qzt3QkFFL0Qsc0JBQU8sSUFBSSxPQUFPLENBQUssVUFBQyxPQUFPLEVBQUMsTUFBTTtnQ0FDbEMsRUFBRSxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztvQ0FDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7aUNBQ3pCO2dDQUVELFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSSxDQUFDLEdBQUcsRUFBQyxVQUFTLEtBQUssRUFBQyxNQUFNLEVBQUMsTUFBTTtvQ0FDbEQsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzs7Ozt3Q0FJUixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7cUNBQ2pCO29DQUFDLElBQUksQ0FBQyxDQUFDO3dDQUNKLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztxQ0FDbkI7aUNBQ0osQ0FBQyxDQUFDOzZCQUNOLENBQUMsRUFBQzs7OztLQUNOO3FCQXJDTDtJQXNDQyxDQUFBOzs7O0FBOUJELHNCQThCQzs7Ozs7OztBQUVELElBQUE7SUFBMkMsaURBQTZCOzs7O2dDQXhDeEU7RUF3QzJDLFVBQVUsRUFFcEQsQ0FBQTtBQUZELGlDQUVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTXlTUUxDb25uZWN0aW9uTWFuYWdlciB9IGZyb20gXCIuL215c3FsLWNvbm5lY3Rpb24tbWFuYWdlclwiO1xuaW1wb3J0IHsgSU15U1FMUXVlcnlSZXN1bHQgfSBmcm9tIFwiLi9pbnRlcmZhY2VzXCI7XG5cblxuY29uc3QgbG9nU3FsUXVlcmllcyA9IFxuICAgIHByb2Nlc3MuZW52LkxPR19TUUxfUVVFUklFUyA9PT0gJ3RydWUnO1xuXG5cbmV4cG9ydCBjbGFzcyBNeVNRTFF1ZXJ5PFQ+XG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHJlYWRvbmx5IHNxbDpzdHJpbmcsXG4gICAgICAgIHByaXZhdGUgY29ubmVjdGlvbk1hbmFnZXI6TXlTUUxDb25uZWN0aW9uTWFuYWdlclxuICAgICkge1xuICAgIH1cblxuICAgIGFzeW5jIGV4ZWN1dGUoKTpQcm9taXNlPFQ+IHtcbiAgICAgICAgLy8gV2hlbiBleGVjdXRlIGlzIGNhbGxlZCBkaXJlY3RseSxcbiAgICAgICAgLy8gYnkgZGVmYXVsdCwgaXQgd2lsbCB1c2UgdGhlIGNvbm5lY3Rpb25NYW5hZ2VyIHVzZWQgdG8gY29uc3RydWN0IHRoZSBxdWVyeS4uLlxuICAgICAgICBjb25zdCBjb25uZWN0aW9uID0gYXdhaXQgdGhpcy5jb25uZWN0aW9uTWFuYWdlci5nZXRDb25uZWN0aW9uKCk7XG5cbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlPFQ+KCAocmVzb2x2ZSxyZWplY3QpPT57XG4gICAgICAgICAgICBpZiAobG9nU3FsUXVlcmllcykge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMuc3FsKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29ubmVjdGlvbi5xdWVyeSh0aGlzLnNxbCxmdW5jdGlvbihlcnJvcixyZXN1bHQsZmllbGRzKXtcbiAgICAgICAgICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZygnKioqIE15U1FMIEVycm9yOiAqKionKTtcbiAgICAgICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhlcnIpO1xuICAgICAgICAgICAgICAgICAgICAvL3Rocm93IGVycm9yO1xuICAgICAgICAgICAgICAgICAgICByZWplY3QoZXJyb3IpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUocmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgTXlTUUxUcmFuc2FjdGlvblF1ZXJ5IGV4dGVuZHMgTXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD5cbntcbn0iXX0=