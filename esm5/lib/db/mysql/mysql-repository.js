/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/** @typedef {?} */
var SelectOneHandler;
export { SelectOneHandler };
/**
 * @template TypeForSelect
 */
var /**
 * @template TypeForSelect
 */
MySQLRepository = /** @class */ (function () {
    function MySQLRepository(db, _tableName, defaultFieldsToSelect, _primaryKey, isPrimaryKeyAString) {
        if (defaultFieldsToSelect === void 0) { defaultFieldsToSelect = []; }
        if (_primaryKey === void 0) { _primaryKey = 'id'; }
        if (isPrimaryKeyAString === void 0) { isPrimaryKeyAString = false; }
        this.db = db;
        this._tableName = _tableName;
        this.defaultFieldsToSelect = defaultFieldsToSelect;
        this._primaryKey = _primaryKey;
        this.isPrimaryKeyAString = isPrimaryKeyAString;
    }
    /**
     * @template T
     * @param {?} entity
     * @return {?}
     */
    MySQLRepository.prototype.insert = /**
     * @template T
     * @param {?} entity
     * @return {?}
     */
    function (entity) {
        return this.insertMany([entity]);
    };
    /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    MySQLRepository.prototype.insertMany = /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    function (entities) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildInsert(entities)];
                    case 1:
                        query = _a.sent();
                        return [4 /*yield*/, query.execute()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    MySQLRepository.prototype.buildInsert = /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    function (entities) {
        return this.db.builder.insert(this, entities);
    };
    /**
     * @param {?} primaryKeyValue
     * @param {?=} fields
     * @return {?}
     */
    MySQLRepository.prototype.selectOneByPrimaryKey = /**
     * @param {?} primaryKeyValue
     * @param {?=} fields
     * @return {?}
     */
    function (primaryKeyValue, fields) {
        if (fields === void 0) { fields = this.defaultFieldsToSelect; }
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var rows;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.selectManyByPrimaryKeys([primaryKeyValue], fields)];
                    case 1:
                        rows = _a.sent();
                        return [2 /*return*/, rows.length > 0 ?
                                rows[0] :
                                null];
                }
            });
        });
    };
    /**
     * @param {?} primaryKeyValues
     * @param {?=} fields
     * @return {?}
     */
    MySQLRepository.prototype.selectManyByPrimaryKeys = /**
     * @param {?} primaryKeyValues
     * @param {?=} fields
     * @return {?}
     */
    function (primaryKeyValues, fields) {
        if (fields === void 0) { fields = this.defaultFieldsToSelect; }
        return this.db.builder.selectByPrimaryKeys(this, fields, primaryKeyValues).execute();
    };
    /**
     * @param {?=} fields
     * @return {?}
     */
    MySQLRepository.prototype.selectAll = /**
     * @param {?=} fields
     * @return {?}
     */
    function (fields) {
        if (fields === void 0) { fields = this.defaultFieldsToSelect; }
        return this.db.builder.selectAll(this, fields).execute();
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.selectOne = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var rows;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.select(tslib_1.__assign({}, params, { limit: 1 }))];
                    case 1:
                        rows = _a.sent();
                        return [2 /*return*/, rows.length > 0 ?
                                rows[0] :
                                null];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.select = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildSelect(params)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, query.execute()];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.buildSelect = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var fields = params.fields ?
            params.fields :
            this.defaultFieldsToSelect;
        /** @type {?} */
        var newParams = tslib_1.__assign({}, params, { fields: fields, repository: this });
        return this.db.builder.select(newParams);
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.updateOne = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildUpdateOne(params)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, query.execute()];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.buildUpdateOne = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return this.buildUpdate(tslib_1.__assign({}, params, { limit: 1 }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.update = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var query;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildUpdate(params)];
                    case 1:
                        query = _a.sent();
                        return [2 /*return*/, query.execute()];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MySQLRepository.prototype.buildUpdate = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var builderParams = tslib_1.__assign({}, params, { repository: this });
        return this.db.builder.update(builderParams);
    };
    /**
     * @param {?} primaryKeyValues
     * @return {?}
     */
    MySQLRepository.prototype.delete = /**
     * @param {?} primaryKeyValues
     * @return {?}
     */
    function (primaryKeyValues) {
        return this.db.builder.delete(this, primaryKeyValues).execute();
    };
    Object.defineProperty(MySQLRepository.prototype, "tableName", {
        get: /**
         * @return {?}
         */
        function () {
            return this._tableName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MySQLRepository.prototype, "primaryKey", {
        get: /**
         * @return {?}
         */
        function () {
            return this._primaryKey;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MySQLRepository.prototype, "primaryKeyFieldList", {
        get: /**
         * @return {?}
         */
        function () {
            return [this.primaryKey];
        },
        enumerable: true,
        configurable: true
    });
    return MySQLRepository;
}());
/**
 * @template TypeForSelect
 */
export { MySQLRepository };
if (false) {
    /** @type {?} */
    MySQLRepository.prototype.db;
    /** @type {?} */
    MySQLRepository.prototype._tableName;
    /** @type {?} */
    MySQLRepository.prototype.defaultFieldsToSelect;
    /** @type {?} */
    MySQLRepository.prototype._primaryKey;
    /** @type {?} */
    MySQLRepository.prototype.isPrimaryKeyAString;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtcmVwb3NpdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL215c3FsL215c3FsLXJlcG9zaXRvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFrQkE7OztBQUFBO0lBRUkseUJBQ2MsRUFBaUIsRUFDbkIsWUFDRSxxQkFBbUMsRUFDckMsYUFDQyxtQkFBMkI7MEVBRlM7O3lFQUVUO1FBSjFCLE9BQUUsR0FBRixFQUFFLENBQWU7UUFDbkIsZUFBVSxHQUFWLFVBQVU7UUFDUiwwQkFBcUIsR0FBckIscUJBQXFCLENBQWM7UUFDckMsZ0JBQVcsR0FBWCxXQUFXO1FBQ1Ysd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFRO0tBRXZDOzs7Ozs7SUFFTSxnQ0FBTTs7Ozs7Y0FBSSxNQUFRO1FBRXJCLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzs7Ozs7OztJQUV4QixvQ0FBVTs7Ozs7Y0FBSSxRQUFZOzs7Ozs0QkFFckIscUJBQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsRUFBQTs7d0JBQXhDLEtBQUssR0FBRyxTQUFnQzt3QkFDdkMscUJBQU0sS0FBSyxDQUFDLE9BQU8sRUFBRSxFQUFBOzRCQUE1QixzQkFBTyxTQUFxQixFQUFDOzs7Ozs7Ozs7O0lBRzFCLHFDQUFXOzs7OztjQUFJLFFBQVk7UUFFOUIsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBSSxJQUFJLEVBQUMsUUFBUSxDQUFDLENBQUM7Ozs7Ozs7SUFJdkMsK0NBQXFCOzs7OztjQUM5QixlQUErQixFQUMvQixNQUE0QztRQUE1Qyx1QkFBQSxFQUFBLFNBQWtCLElBQUksQ0FBQyxxQkFBcUI7Ozs7OzRCQUUvQixxQkFBTSxJQUFJLENBQUMsdUJBQXVCLENBQzNDLENBQUMsZUFBZSxDQUFDLEVBQ2pCLE1BQU0sQ0FDVCxFQUFBOzt3QkFISyxJQUFJLEdBQUcsU0FHWjt3QkFFRCxzQkFBTyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dDQUNoQixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQ0FDVCxJQUFJLEVBQUM7Ozs7Ozs7Ozs7SUFHVixpREFBdUI7Ozs7O2NBQzFCLGdCQUFrQyxFQUNsQyxNQUE0QztRQUE1Qyx1QkFBQSxFQUFBLFNBQWtCLElBQUksQ0FBQyxxQkFBcUI7UUFFNUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUN0QyxJQUFJLEVBQUMsTUFBTSxFQUFDLGdCQUFnQixDQUMvQixDQUFDLE9BQU8sRUFBRSxDQUFDOzs7Ozs7SUFHVCxtQ0FBUzs7OztjQUFDLE1BQTRDO1FBQTVDLHVCQUFBLEVBQUEsU0FBa0IsSUFBSSxDQUFDLHFCQUFxQjtRQUN6RCxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUM1QixJQUFJLEVBQ0osTUFBTSxDQUNULENBQUMsT0FBTyxFQUFFLENBQUM7Ozs7OztJQUlILG1DQUFTOzs7O2NBQUMsTUFBd0M7Ozs7OzRCQUM5QyxxQkFBTSxJQUFJLENBQUMsTUFBTSxzQkFDdkIsTUFBTSxJQUNULEtBQUssRUFBRSxDQUFDLElBQ1YsRUFBQTs7d0JBSEksSUFBSSxHQUFHLFNBR1g7d0JBRUYsc0JBQU8sSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztnQ0FDaEIsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBQ1QsSUFBSSxFQUFDOzs7Ozs7Ozs7SUFHSixnQ0FBTTs7OztjQUFDLE1BQXdDOzs7Ozs0QkFDMUMscUJBQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQXRDLEtBQUssR0FBRyxTQUE4Qjt3QkFFNUMsc0JBQU8sS0FBSyxDQUFDLE9BQU8sRUFBRSxFQUFDOzs7Ozs7Ozs7SUFHcEIscUNBQVc7Ozs7Y0FBQyxNQUF3Qzs7UUFDdkQsSUFBTSxNQUFNLEdBQ1IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ1gsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2YsSUFBSSxDQUFDLHFCQUFxQixDQUFDOztRQUVuQyxJQUFNLFNBQVMsd0JBQ1IsTUFBTSxJQUNULE1BQU0sUUFBQSxFQUNOLFVBQVUsRUFBRSxJQUFJLElBQ25CO1FBRUQsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBZ0IsU0FBUyxDQUFDLENBQUM7Ozs7OztJQUkvQyxtQ0FBUzs7OztjQUFDLE1BQXdDOzs7Ozs0QkFDN0MscUJBQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQXpDLEtBQUssR0FBRyxTQUFpQzt3QkFFL0Msc0JBQU8sS0FBSyxDQUFDLE9BQU8sRUFBRSxFQUFDOzs7Ozs7Ozs7SUFFcEIsd0NBQWM7Ozs7Y0FBQyxNQUF3QztRQUMxRCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsc0JBQ2hCLE1BQU0sSUFDVCxLQUFLLEVBQUUsQ0FBQyxJQUNWLENBQUM7Ozs7OztJQUdNLGdDQUFNOzs7O2NBQUMsTUFBd0M7Ozs7OzRCQUMxQyxxQkFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBdEMsS0FBSyxHQUFHLFNBQThCO3dCQUM1QyxzQkFBTyxLQUFLLENBQUMsT0FBTyxFQUFFLEVBQUM7Ozs7Ozs7OztJQUVwQixxQ0FBVzs7OztjQUFDLE1BQXdDOztRQUN2RCxJQUFNLGFBQWEsd0JBQ1osTUFBTSxJQUNULFVBQVUsRUFBRSxJQUFJLElBQ25CO1FBRUQsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQzs7Ozs7O0lBSTFDLGdDQUFNOzs7O2NBQUMsZ0JBQWtDO1FBRTVDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFDLGdCQUFnQixDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7OzBCQUl4RCxzQ0FBUzs7Ozs7WUFFaEIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7Ozs7OzBCQUdoQix1Q0FBVTs7Ozs7WUFFakIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7Ozs7OzBCQUdoQixnREFBbUI7Ozs7O1lBRTNCLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs7Ozs7MEJBekpqQztJQTJKQyxDQUFBOzs7O0FBeklELDJCQXlJQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gICAgSU15U1FMRGF0YWJhc2UsXG4gICAgSU15U1FMUmVwb3NpdG9yeSxcbiAgICBQcmltYXJ5S2V5VmFsdWUsXG4gICAgSU15U1FMUXVlcnlSZXN1bHQsXG4gICAgSVNlbGVjdFF1ZXJ5QnVpbGRlclBhcmFtcyxcbiAgICBJU2VsZWN0UXVlcnlQYXJhbXMsXG4gICAgSVVwZGF0ZVF1ZXJ5UGFyYW1zLFxuICAgIElVcGRhdGVRdWVyeUJ1aWxkZXJQYXJhbXMsXG59IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBNeVNRTFF1ZXJ5IH0gZnJvbSAnLi9teXNxbC1xdWVyeSc7XG5cblxuXG5leHBvcnQgdHlwZSBTZWxlY3RPbmVIYW5kbGVyPFQ+ID0gKHJvd3M6VFtdKSA9PiB2b2lkO1xuXG5cblxuZXhwb3J0IGNsYXNzIE15U1FMUmVwb3NpdG9yeTxUeXBlRm9yU2VsZWN0PiBpbXBsZW1lbnRzIElNeVNRTFJlcG9zaXRvcnlcbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIGRiOklNeVNRTERhdGFiYXNlLFxuICAgICAgICBwcml2YXRlIF90YWJsZU5hbWU6c3RyaW5nLFxuICAgICAgICBwcm90ZWN0ZWQgZGVmYXVsdEZpZWxkc1RvU2VsZWN0OnN0cmluZ1tdID0gW10sXG4gICAgICAgIHByaXZhdGUgX3ByaW1hcnlLZXk6c3RyaW5nID0gJ2lkJyxcbiAgICAgICAgcmVhZG9ubHkgaXNQcmltYXJ5S2V5QVN0cmluZyA9IGZhbHNlXG4gICAgKSB7XG4gICAgfVxuXG4gICAgcHVibGljIGluc2VydDxUPihlbnRpdHk6VClcbiAgICB7XG4gICAgICAgIHJldHVybiB0aGlzLmluc2VydE1hbnkoW2VudGl0eV0pO1xuICAgIH1cbiAgICBwdWJsaWMgYXN5bmMgaW5zZXJ0TWFueTxUPihlbnRpdGllczpUW10pXG4gICAge1xuICAgICAgICBjb25zdCBxdWVyeSA9IGF3YWl0IHRoaXMuYnVpbGRJbnNlcnQoZW50aXRpZXMpO1xuICAgICAgICByZXR1cm4gYXdhaXQgcXVlcnkuZXhlY3V0ZSgpO1xuICAgIH1cblxuICAgIHB1YmxpYyBidWlsZEluc2VydDxUPihlbnRpdGllczpUW10pOlByb21pc2U8TXlTUUxRdWVyeTxJTXlTUUxRdWVyeVJlc3VsdD4+XG4gICAge1xuICAgICAgICByZXR1cm4gdGhpcy5kYi5idWlsZGVyLmluc2VydDxUPih0aGlzLGVudGl0aWVzKTtcbiAgICB9XG5cblxuICAgIHB1YmxpYyBhc3luYyBzZWxlY3RPbmVCeVByaW1hcnlLZXkoXG4gICAgICAgIHByaW1hcnlLZXlWYWx1ZTpQcmltYXJ5S2V5VmFsdWUsXG4gICAgICAgIGZpZWxkczpzdHJpbmdbXSA9IHRoaXMuZGVmYXVsdEZpZWxkc1RvU2VsZWN0XG4gICAgKTpQcm9taXNlPFR5cGVGb3JTZWxlY3Q+IHtcbiAgICAgICAgY29uc3Qgcm93cyA9IGF3YWl0IHRoaXMuc2VsZWN0TWFueUJ5UHJpbWFyeUtleXMoXG4gICAgICAgICAgICBbcHJpbWFyeUtleVZhbHVlXSxcbiAgICAgICAgICAgIGZpZWxkc1xuICAgICAgICApO1xuXG4gICAgICAgIHJldHVybiByb3dzLmxlbmd0aCA+IDAgP1xuICAgICAgICAgICAgICAgIHJvd3NbMF0gOlxuICAgICAgICAgICAgICAgIG51bGw7XG4gICAgfVxuXG4gICAgcHVibGljIHNlbGVjdE1hbnlCeVByaW1hcnlLZXlzKFxuICAgICAgICBwcmltYXJ5S2V5VmFsdWVzOlByaW1hcnlLZXlWYWx1ZVtdLFxuICAgICAgICBmaWVsZHM6c3RyaW5nW10gPSB0aGlzLmRlZmF1bHRGaWVsZHNUb1NlbGVjdFxuICAgICkge1xuICAgICAgICByZXR1cm4gdGhpcy5kYi5idWlsZGVyLnNlbGVjdEJ5UHJpbWFyeUtleXM8VHlwZUZvclNlbGVjdD4oXG4gICAgICAgICAgICB0aGlzLGZpZWxkcyxwcmltYXJ5S2V5VmFsdWVzXG4gICAgICAgICkuZXhlY3V0ZSgpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzZWxlY3RBbGwoZmllbGRzOnN0cmluZ1tdID0gdGhpcy5kZWZhdWx0RmllbGRzVG9TZWxlY3QpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGIuYnVpbGRlci5zZWxlY3RBbGw8VHlwZUZvclNlbGVjdD4oXG4gICAgICAgICAgICB0aGlzLFxuICAgICAgICAgICAgZmllbGRzXG4gICAgICAgICkuZXhlY3V0ZSgpO1xuICAgIH1cblxuICAgXG4gICAgcHVibGljIGFzeW5jIHNlbGVjdE9uZShwYXJhbXM6SVNlbGVjdFF1ZXJ5UGFyYW1zPFR5cGVGb3JTZWxlY3Q+KSB7XG4gICAgICAgIGNvbnN0IHJvd3MgPSBhd2FpdCB0aGlzLnNlbGVjdCh7XG4gICAgICAgICAgICAuLi5wYXJhbXMsXG4gICAgICAgICAgICBsaW1pdDogMVxuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gcm93cy5sZW5ndGggPiAwID9cbiAgICAgICAgICAgICAgICByb3dzWzBdIDpcbiAgICAgICAgICAgICAgICBudWxsO1xuICAgIH1cblxuICAgIHB1YmxpYyBhc3luYyBzZWxlY3QocGFyYW1zOklTZWxlY3RRdWVyeVBhcmFtczxUeXBlRm9yU2VsZWN0Pikge1xuICAgICAgICBjb25zdCBxdWVyeSA9IGF3YWl0IHRoaXMuYnVpbGRTZWxlY3QocGFyYW1zKTtcblxuICAgICAgICByZXR1cm4gcXVlcnkuZXhlY3V0ZSgpO1xuICAgIH1cbiAgICBcbiAgICBwdWJsaWMgYnVpbGRTZWxlY3QocGFyYW1zOklTZWxlY3RRdWVyeVBhcmFtczxUeXBlRm9yU2VsZWN0Pikge1xuICAgICAgICBjb25zdCBmaWVsZHMgPVxuICAgICAgICAgICAgcGFyYW1zLmZpZWxkcyA/XG4gICAgICAgICAgICAgICAgcGFyYW1zLmZpZWxkcyA6XG4gICAgICAgICAgICAgICAgdGhpcy5kZWZhdWx0RmllbGRzVG9TZWxlY3Q7XG5cbiAgICAgICAgY29uc3QgbmV3UGFyYW1zOklTZWxlY3RRdWVyeUJ1aWxkZXJQYXJhbXM8VHlwZUZvclNlbGVjdD4gPSB7XG4gICAgICAgICAgICAuLi5wYXJhbXMsXG4gICAgICAgICAgICBmaWVsZHMsXG4gICAgICAgICAgICByZXBvc2l0b3J5OiB0aGlzXG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gdGhpcy5kYi5idWlsZGVyLnNlbGVjdDxUeXBlRm9yU2VsZWN0PihuZXdQYXJhbXMpO1xuICAgIH1cblxuXG4gICAgcHVibGljIGFzeW5jIHVwZGF0ZU9uZShwYXJhbXM6SVVwZGF0ZVF1ZXJ5UGFyYW1zPFR5cGVGb3JTZWxlY3Q+KSB7XG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gYXdhaXQgdGhpcy5idWlsZFVwZGF0ZU9uZShwYXJhbXMpO1xuXG4gICAgICAgIHJldHVybiBxdWVyeS5leGVjdXRlKCk7XG4gICAgfVxuICAgIHB1YmxpYyBidWlsZFVwZGF0ZU9uZShwYXJhbXM6SVVwZGF0ZVF1ZXJ5UGFyYW1zPFR5cGVGb3JTZWxlY3Q+KSB7XG4gICAgICAgIHJldHVybiB0aGlzLmJ1aWxkVXBkYXRlKHtcbiAgICAgICAgICAgIC4uLnBhcmFtcyxcbiAgICAgICAgICAgIGxpbWl0OiAxXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHB1YmxpYyBhc3luYyB1cGRhdGUocGFyYW1zOklVcGRhdGVRdWVyeVBhcmFtczxUeXBlRm9yU2VsZWN0Pikge1xuICAgICAgICBjb25zdCBxdWVyeSA9IGF3YWl0IHRoaXMuYnVpbGRVcGRhdGUocGFyYW1zKTtcbiAgICAgICAgcmV0dXJuIHF1ZXJ5LmV4ZWN1dGUoKTtcbiAgICB9XG4gICAgcHVibGljIGJ1aWxkVXBkYXRlKHBhcmFtczpJVXBkYXRlUXVlcnlQYXJhbXM8VHlwZUZvclNlbGVjdD4pIHtcbiAgICAgICAgY29uc3QgYnVpbGRlclBhcmFtczpJVXBkYXRlUXVlcnlCdWlsZGVyUGFyYW1zPFR5cGVGb3JTZWxlY3Q+ID0ge1xuICAgICAgICAgICAgLi4ucGFyYW1zLFxuICAgICAgICAgICAgcmVwb3NpdG9yeTogdGhpc1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuZGIuYnVpbGRlci51cGRhdGUoYnVpbGRlclBhcmFtcyk7XG4gICAgfVxuXG5cbiAgICBwdWJsaWMgZGVsZXRlKHByaW1hcnlLZXlWYWx1ZXM6UHJpbWFyeUtleVZhbHVlW10pXG4gICAge1xuICAgICAgICByZXR1cm4gdGhpcy5kYi5idWlsZGVyLmRlbGV0ZSh0aGlzLHByaW1hcnlLZXlWYWx1ZXMpLmV4ZWN1dGUoKTtcbiAgICB9XG5cblxuICAgIHB1YmxpYyBnZXQgdGFibGVOYW1lKCk6c3RyaW5nXG4gICAge1xuICAgICAgICByZXR1cm4gdGhpcy5fdGFibGVOYW1lO1xuICAgIH1cblxuICAgIHB1YmxpYyBnZXQgcHJpbWFyeUtleSgpOnN0cmluZ1xuICAgIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX3ByaW1hcnlLZXk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXQgcHJpbWFyeUtleUZpZWxkTGlzdCgpOnN0cmluZ1tdXG4gICAge1xuICAgICAgICByZXR1cm4gW3RoaXMucHJpbWFyeUtleV07XG4gICAgfVxufSJdfQ==