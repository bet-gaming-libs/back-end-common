/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/** @enum {number} */
var MySQLValueType = {
    EXPRESSION: 0,
    STRING: 1,
};
export { MySQLValueType };
MySQLValueType[MySQLValueType.EXPRESSION] = 'EXPRESSION';
MySQLValueType[MySQLValueType.STRING] = 'STRING';
/** @enum {string} */
var ExpressionOperator = {
    EQUAL: '=',
    LESS_THAN: '<',
    LESS_THAN_OR_EQUAL: '<=',
    GREATER_THAN: '>',
    GREATER_THAN_OR_EQUAL: '>=',
    IS: 'IS',
    IS_NOT: 'IS NOT',
};
export { ExpressionOperator };
/** @enum {string} */
var ExpressionGroupJoiner = {
    AND: 'AND',
    OR: 'OR',
    COMMA: ',',
};
export { ExpressionGroupJoiner };
/** @typedef {?} */
var ExpressionValueType;
export { ExpressionValueType };
/**
 * @record
 */
export function IMySQLValue() { }
/** @type {?} */
IMySQLValue.prototype.data;
/** @type {?} */
IMySQLValue.prototype.isString;
var MySQLValue = /** @class */ (function () {
    function MySQLValue(data, type) {
        this.data = data;
        this.type = type;
        this.isString =
            type == MySQLValueType.STRING;
    }
    return MySQLValue;
}());
export { MySQLValue };
if (false) {
    /** @type {?} */
    MySQLValue.prototype.isString;
    /** @type {?} */
    MySQLValue.prototype.data;
    /** @type {?} */
    MySQLValue.prototype.type;
}
var MySQLNumberValue = /** @class */ (function (_super) {
    tslib_1.__extends(MySQLNumberValue, _super);
    function MySQLNumberValue(data) {
        return _super.call(this, data, MySQLValueType.EXPRESSION) || this;
    }
    return MySQLNumberValue;
}(MySQLValue));
var MySQLStringValue = /** @class */ (function (_super) {
    tslib_1.__extends(MySQLStringValue, _super);
    function MySQLStringValue(data) {
        return _super.call(this, data, MySQLValueType.STRING) || this;
    }
    return MySQLStringValue;
}(MySQLValue));
var MySQLStringExpressionValue = /** @class */ (function (_super) {
    tslib_1.__extends(MySQLStringExpressionValue, _super);
    function MySQLStringExpressionValue(data) {
        return _super.call(this, data, MySQLValueType.EXPRESSION) || this;
    }
    return MySQLStringExpressionValue;
}(MySQLValue));
/** @enum {string} */
var MySQLExpression = {
    NOW: 'NOW()',
    NULL: 'NULL',
};
export { MySQLExpression };
/** @type {?} */
var nowValue = new MySQLStringExpressionValue(MySQLExpression.NOW);
/** @type {?} */
var nullValue = new MySQLStringExpressionValue(MySQLExpression.NULL);
/**
 * @abstract
 */
var /**
 * @abstract
 */
Expression = /** @class */ (function () {
    function Expression(leftSide, operator, rightSide) {
        this.leftSide = leftSide;
        this.operator = operator;
        this.rightSide = rightSide;
    }
    return Expression;
}());
/**
 * @abstract
 */
export { Expression };
if (false) {
    /** @type {?} */
    Expression.prototype.leftSide;
    /** @type {?} */
    Expression.prototype.operator;
    /** @type {?} */
    Expression.prototype.rightSide;
}
var ExpressionGroup = /** @class */ (function () {
    function ExpressionGroup(expressions, joiner, surroundExpressionWithBrackets) {
        if (surroundExpressionWithBrackets === void 0) { surroundExpressionWithBrackets = true; }
        this.expressions = expressions;
        this.joiner = joiner;
        this.surroundExpressionWithBrackets = surroundExpressionWithBrackets;
    }
    return ExpressionGroup;
}());
export { ExpressionGroup };
if (false) {
    /** @type {?} */
    ExpressionGroup.prototype.expressions;
    /** @type {?} */
    ExpressionGroup.prototype.joiner;
    /** @type {?} */
    ExpressionGroup.prototype.surroundExpressionWithBrackets;
}
/**
 * @param {?} map
 * @param {?} joiner
 * @return {?}
 */
export function createExpressionGroup(map, joiner) {
    /** @type {?} */
    var expressions = [];
    // should we use Object.keys()?
    for (var fieldName in map) {
        /** @type {?} */
        var fieldValue = map[fieldName];
        expressions.push(typeof fieldValue == 'string' ?
            new StringStatement(fieldName, /** @type {?} */ (fieldValue)) :
            new NumberStatement(fieldName, fieldValue));
    }
    return new ExpressionGroup(expressions, joiner);
}
var SingleExpression = /** @class */ (function (_super) {
    tslib_1.__extends(SingleExpression, _super);
    function SingleExpression(expression) {
        return _super.call(this, [expression], undefined) || this;
    }
    return SingleExpression;
}(ExpressionGroup));
export { SingleExpression };
var AndExpressionGroup = /** @class */ (function (_super) {
    tslib_1.__extends(AndExpressionGroup, _super);
    function AndExpressionGroup(expressions) {
        return _super.call(this, expressions, ExpressionGroupJoiner.AND) || this;
    }
    return AndExpressionGroup;
}(ExpressionGroup));
export { AndExpressionGroup };
var OrExpressionGroup = /** @class */ (function (_super) {
    tslib_1.__extends(OrExpressionGroup, _super);
    function OrExpressionGroup(expressions) {
        return _super.call(this, expressions, ExpressionGroupJoiner.OR) || this;
    }
    return OrExpressionGroup;
}(ExpressionGroup));
export { OrExpressionGroup };
var UpdateStatements = /** @class */ (function (_super) {
    tslib_1.__extends(UpdateStatements, _super);
    function UpdateStatements(expressions) {
        return _super.call(this, expressions, ExpressionGroupJoiner.COMMA, false) || this;
    }
    return UpdateStatements;
}(ExpressionGroup));
export { UpdateStatements };
/**
 * @abstract
 */
var /**
 * @abstract
 */
FieldStatement = /** @class */ (function (_super) {
    tslib_1.__extends(FieldStatement, _super);
    function FieldStatement(fieldName, value, operator) {
        return _super.call(this, fieldName, operator, value) || this;
    }
    return FieldStatement;
}(Expression));
/**
 * @abstract
 */
export { FieldStatement };
var NumberStatement = /** @class */ (function (_super) {
    tslib_1.__extends(NumberStatement, _super);
    function NumberStatement(fieldName, data, operator) {
        if (operator === void 0) { operator = ExpressionOperator.EQUAL; }
        return _super.call(this, fieldName, new MySQLNumberValue(data), operator) || this;
    }
    return NumberStatement;
}(FieldStatement));
export { NumberStatement };
var StringStatement = /** @class */ (function (_super) {
    tslib_1.__extends(StringStatement, _super);
    function StringStatement(fieldName, data, operator) {
        if (operator === void 0) { operator = ExpressionOperator.EQUAL; }
        return _super.call(this, fieldName, new MySQLStringValue(data), operator) || this;
    }
    return StringStatement;
}(FieldStatement));
export { StringStatement };
var StringExpressionStatement = /** @class */ (function (_super) {
    tslib_1.__extends(StringExpressionStatement, _super);
    function StringExpressionStatement(fieldName, data, operator) {
        if (operator === void 0) { operator = ExpressionOperator.EQUAL; }
        return _super.call(this, fieldName, new MySQLStringExpressionValue(data), operator) || this;
    }
    return StringExpressionStatement;
}(FieldStatement));
export { StringExpressionStatement };
var NullUpdateStatement = /** @class */ (function (_super) {
    tslib_1.__extends(NullUpdateStatement, _super);
    function NullUpdateStatement(fieldName) {
        return _super.call(this, fieldName, nullValue, ExpressionOperator.EQUAL) || this;
    }
    return NullUpdateStatement;
}(FieldStatement));
export { NullUpdateStatement };
var NowUpdateStatement = /** @class */ (function (_super) {
    tslib_1.__extends(NowUpdateStatement, _super);
    function NowUpdateStatement(fieldName) {
        return _super.call(this, fieldName, nowValue, ExpressionOperator.EQUAL) || this;
    }
    return NowUpdateStatement;
}(FieldStatement));
export { NowUpdateStatement };
var FieldIsNullStatement = /** @class */ (function (_super) {
    tslib_1.__extends(FieldIsNullStatement, _super);
    function FieldIsNullStatement(fieldName) {
        return _super.call(this, fieldName, nullValue, ExpressionOperator.IS) || this;
    }
    return FieldIsNullStatement;
}(FieldStatement));
export { FieldIsNullStatement };
var FieldIsNotNullStatement = /** @class */ (function (_super) {
    tslib_1.__extends(FieldIsNotNullStatement, _super);
    function FieldIsNotNullStatement(fieldName) {
        return _super.call(this, fieldName, nullValue, ExpressionOperator.IS_NOT) || this;
    }
    return FieldIsNotNullStatement;
}(FieldStatement));
export { FieldIsNotNullStatement };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtc3RhdGVtZW50cy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL215c3FsL215c3FsLXN0YXRlbWVudHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztJQUVJLGFBQVU7SUFDVixTQUFNOzs7OEJBRE4sVUFBVTs4QkFDVixNQUFNOzs7SUFLTixPQUF3QixHQUFHO0lBQzNCLFdBQXdCLEdBQUc7SUFDM0Isb0JBQXdCLElBQUk7SUFDNUIsY0FBd0IsR0FBRztJQUMzQix1QkFBd0IsSUFBSTtJQUU1QixJQUF3QixJQUFJO0lBQzVCLFFBQXdCLFFBQVE7Ozs7O0lBS2hDLEtBQVEsS0FBSztJQUNiLElBQVEsSUFBSTtJQUNaLE9BQVEsR0FBRzs7Ozs7Ozs7Ozs7Ozs7QUF3QmYsSUFBQTtJQUlJLG9CQUNhLElBQXdCLEVBQ3hCLElBQW1CO1FBRG5CLFNBQUksR0FBSixJQUFJLENBQW9CO1FBQ3hCLFNBQUksR0FBSixJQUFJLENBQWU7UUFFNUIsSUFBSSxDQUFDLFFBQVE7WUFDVCxJQUFJLElBQUksY0FBYyxDQUFDLE1BQU0sQ0FBQztLQUNyQztxQkF4REw7SUF5REMsQ0FBQTtBQVhELHNCQVdDOzs7Ozs7Ozs7QUFFRCxJQUFBO0lBQStCLDRDQUFVO0lBRXJDLDBCQUFZLElBQVc7ZUFDbkIsa0JBQU0sSUFBSSxFQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUM7S0FDeEM7MkJBL0RMO0VBMkQrQixVQUFVLEVBS3hDLENBQUE7QUFFRCxJQUFBO0lBQStCLDRDQUFVO0lBRXJDLDBCQUFZLElBQVc7ZUFDbkIsa0JBQU0sSUFBSSxFQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7S0FDcEM7MkJBdEVMO0VBa0UrQixVQUFVLEVBS3hDLENBQUE7QUFFRCxJQUFBO0lBQXlDLHNEQUFVO0lBRS9DLG9DQUFZLElBQVc7ZUFDbkIsa0JBQU0sSUFBSSxFQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUM7S0FDeEM7cUNBN0VMO0VBeUV5QyxVQUFVLEVBS2xELENBQUE7OztJQUlHLEtBQU0sT0FBTztJQUNiLE1BQU8sTUFBTTs7OztBQUdqQixJQUFNLFFBQVEsR0FBRyxJQUFJLDBCQUEwQixDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsQ0FBQzs7QUFDckUsSUFBTSxTQUFTLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7Ozs7QUFldkU7OztBQUFBO0lBRUksb0JBQ2EsUUFBZSxFQUNmLFFBQWUsRUFDZixTQUFvQjtRQUZwQixhQUFRLEdBQVIsUUFBUSxDQUFPO1FBQ2YsYUFBUSxHQUFSLFFBQVEsQ0FBTztRQUNmLGNBQVMsR0FBVCxTQUFTLENBQVc7S0FFaEM7cUJBN0dMO0lBOEdDLENBQUE7Ozs7QUFSRCxzQkFRQzs7Ozs7Ozs7O0FBR0QsSUFBQTtJQUVJLHlCQUNhLFdBQXdCLEVBQ3hCLE1BQTRCLEVBQzVCLDhCQUE2Qzs4RkFBQTtRQUY3QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixXQUFNLEdBQU4sTUFBTSxDQUFzQjtRQUM1QixtQ0FBOEIsR0FBOUIsOEJBQThCLENBQWU7S0FFekQ7MEJBeEhMO0lBeUhDLENBQUE7QUFSRCwyQkFRQzs7Ozs7Ozs7Ozs7Ozs7QUFHRCxNQUFNLGdDQUNGLEdBQU8sRUFBQyxNQUE0Qjs7SUFFcEMsSUFBTSxXQUFXLEdBQWdCLEVBQUUsQ0FBQzs7SUFHcEMsR0FBRyxDQUFDLENBQUMsSUFBTSxTQUFTLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQzs7UUFDMUIsSUFBTSxVQUFVLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRWxDLFdBQVcsQ0FBQyxJQUFJLENBQ1osT0FBTyxVQUFVLElBQUksUUFBUSxDQUFDLENBQUM7WUFDM0IsSUFBSSxlQUFlLENBQ2YsU0FBUyxvQkFDVCxVQUFvQixFQUN2QixDQUFDLENBQUM7WUFDSCxJQUFJLGVBQWUsQ0FDZixTQUFTLEVBQ1QsVUFBVSxDQUNiLENBQ1IsQ0FBQztLQUNMO0lBRUQsTUFBTSxDQUFDLElBQUksZUFBZSxDQUFDLFdBQVcsRUFBQyxNQUFNLENBQUMsQ0FBQztDQUNsRDtBQUdELElBQUE7SUFBc0MsNENBQWU7SUFFakQsMEJBQVksVUFBcUI7ZUFDN0Isa0JBQU0sQ0FBQyxVQUFVLENBQUMsRUFBQyxTQUFTLENBQUM7S0FDaEM7MkJBMUpMO0VBc0pzQyxlQUFlLEVBS3BELENBQUE7QUFMRCw0QkFLQztBQUNELElBQUE7SUFBd0MsOENBQWU7SUFFbkQsNEJBQVksV0FBd0I7ZUFDaEMsa0JBQU0sV0FBVyxFQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQztLQUMvQzs2QkFoS0w7RUE0SndDLGVBQWUsRUFLdEQsQ0FBQTtBQUxELDhCQUtDO0FBQ0QsSUFBQTtJQUF1Qyw2Q0FBZTtJQUVsRCwyQkFBWSxXQUF3QjtlQUNoQyxrQkFBTSxXQUFXLEVBQUMscUJBQXFCLENBQUMsRUFBRSxDQUFDO0tBQzlDOzRCQXRLTDtFQWtLdUMsZUFBZSxFQUtyRCxDQUFBO0FBTEQsNkJBS0M7QUFFRCxJQUFBO0lBQXNDLDRDQUFlO0lBRWpELDBCQUFZLFdBQXdCO2VBQ2hDLGtCQUFNLFdBQVcsRUFBQyxxQkFBcUIsQ0FBQyxLQUFLLEVBQUMsS0FBSyxDQUFDO0tBQ3ZEOzJCQTdLTDtFQXlLc0MsZUFBZSxFQUtwRCxDQUFBO0FBTEQsNEJBS0M7Ozs7QUFJRDs7O0FBQUE7SUFBNkMsMENBQVU7SUFFbkQsd0JBQ0ksU0FBZ0IsRUFDaEIsS0FBZ0IsRUFDaEIsUUFBMkI7ZUFFM0Isa0JBQU0sU0FBUyxFQUFDLFFBQVEsRUFBQyxLQUFLLENBQUM7S0FDbEM7eUJBMUxMO0VBa0w2QyxVQUFVLEVBU3RELENBQUE7Ozs7QUFURCwwQkFTQztBQUdELElBQUE7SUFBcUMsMkNBQWM7SUFFL0MseUJBQ0ksU0FBZ0IsRUFDaEIsSUFBVyxFQUNYLFFBQXNEO1FBQXRELHlCQUFBLEVBQUEsV0FBOEIsa0JBQWtCLENBQUMsS0FBSztlQUV0RCxrQkFDSSxTQUFTLEVBQ1QsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFDMUIsUUFBUSxDQUNYO0tBQ0o7MEJBMU1MO0VBOExxQyxjQUFjLEVBYWxELENBQUE7QUFiRCwyQkFhQztBQUVELElBQUE7SUFBcUMsMkNBQWM7SUFFL0MseUJBQ0ksU0FBZ0IsRUFDaEIsSUFBVyxFQUNYLFFBQXNEO1FBQXRELHlCQUFBLEVBQUEsV0FBOEIsa0JBQWtCLENBQUMsS0FBSztlQUV0RCxrQkFDSSxTQUFTLEVBQ1QsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFDMUIsUUFBUSxDQUNYO0tBQ0o7MEJBek5MO0VBNk1xQyxjQUFjLEVBYWxELENBQUE7QUFiRCwyQkFhQztBQUVELElBQUE7SUFBK0MscURBQWM7SUFFekQsbUNBQ0ksU0FBZ0IsRUFDaEIsSUFBVyxFQUNYLFFBQXNEO1FBQXRELHlCQUFBLEVBQUEsV0FBOEIsa0JBQWtCLENBQUMsS0FBSztlQUV0RCxrQkFDSSxTQUFTLEVBQ1QsSUFBSSwwQkFBMEIsQ0FBQyxJQUFJLENBQUMsRUFDcEMsUUFBUSxDQUNYO0tBQ0o7b0NBeE9MO0VBNE4rQyxjQUFjLEVBYTVELENBQUE7QUFiRCxxQ0FhQztBQUdELElBQUE7SUFBeUMsK0NBQWM7SUFFbkQsNkJBQVksU0FBZ0I7ZUFDeEIsa0JBQU0sU0FBUyxFQUFDLFNBQVMsRUFBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUM7S0FDdEQ7OEJBaFBMO0VBNE95QyxjQUFjLEVBS3RELENBQUE7QUFMRCwrQkFLQztBQUVELElBQUE7SUFBd0MsOENBQWM7SUFFbEQsNEJBQVksU0FBZ0I7ZUFDeEIsa0JBQU0sU0FBUyxFQUFDLFFBQVEsRUFBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUM7S0FDckQ7NkJBdlBMO0VBbVB3QyxjQUFjLEVBS3JELENBQUE7QUFMRCw4QkFLQztBQUdELElBQUE7SUFBMEMsZ0RBQWM7SUFFcEQsOEJBQVksU0FBZ0I7ZUFDeEIsa0JBQU0sU0FBUyxFQUFDLFNBQVMsRUFBQyxrQkFBa0IsQ0FBQyxFQUFFLENBQUM7S0FDbkQ7K0JBL1BMO0VBMlAwQyxjQUFjLEVBS3ZELENBQUE7QUFMRCxnQ0FLQztBQUVELElBQUE7SUFBNkMsbURBQWM7SUFFdkQsaUNBQVksU0FBZ0I7ZUFDeEIsa0JBQU0sU0FBUyxFQUFDLFNBQVMsRUFBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUM7S0FDdkQ7a0NBdFFMO0VBa1E2QyxjQUFjLEVBSzFELENBQUE7QUFMRCxtQ0FLQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBlbnVtIE15U1FMVmFsdWVUeXBlXG57XG4gICAgRVhQUkVTU0lPTixcbiAgICBTVFJJTkdcbn1cblxuZXhwb3J0IGVudW0gRXhwcmVzc2lvbk9wZXJhdG9yXG57XG4gICAgRVFVQUwgICAgICAgICAgICAgICAgID0gJz0nLFxuICAgIExFU1NfVEhBTiAgICAgICAgICAgICA9ICc8JyxcbiAgICBMRVNTX1RIQU5fT1JfRVFVQUwgICAgPSAnPD0nLFxuICAgIEdSRUFURVJfVEhBTiAgICAgICAgICA9ICc+JyxcbiAgICBHUkVBVEVSX1RIQU5fT1JfRVFVQUwgPSAnPj0nLFxuXG4gICAgSVMgICAgICAgICAgICAgICAgICAgID0gJ0lTJyxcbiAgICBJU19OT1QgICAgICAgICAgICAgICAgPSAnSVMgTk9UJyxcbn1cblxuZXhwb3J0IGVudW0gRXhwcmVzc2lvbkdyb3VwSm9pbmVyXG57XG4gICAgQU5EICAgPSAnQU5EJyxcbiAgICBPUiAgICA9ICdPUicsXG4gICAgQ09NTUEgPSAnLCdcbn1cblxuXG5cbmV4cG9ydCB0eXBlIEV4cHJlc3Npb25WYWx1ZVR5cGUgPSBzdHJpbmd8bnVtYmVyO1xuXG5cblxuLypcbmludGVyZmFjZSBJTXlTUUxWYWx1ZTxUIGV4dGVuZHMgRXhwcmVzc2lvblZhbHVlVHlwZT5cbntcbiAgICBkYXRhOlQsXG4gICAgLy90eXBlOk15U1FMVmFsdWVUeXBlXG59XG4qL1xuZXhwb3J0IGludGVyZmFjZSBJTXlTUUxWYWx1ZVxue1xuICAgIGRhdGE6RXhwcmVzc2lvblZhbHVlVHlwZTtcbiAgICBpc1N0cmluZzpib29sZWFuO1xufVxuXG5cblxuZXhwb3J0IGNsYXNzIE15U1FMVmFsdWUgaW1wbGVtZW50cyBJTXlTUUxWYWx1ZVxue1xuICAgIHJlYWRvbmx5IGlzU3RyaW5nOmJvb2xlYW47XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcmVhZG9ubHkgZGF0YTpFeHByZXNzaW9uVmFsdWVUeXBlLFxuICAgICAgICByZWFkb25seSB0eXBlOk15U1FMVmFsdWVUeXBlXG4gICAgKSB7XG4gICAgICAgIHRoaXMuaXNTdHJpbmcgPSBcbiAgICAgICAgICAgIHR5cGUgPT0gTXlTUUxWYWx1ZVR5cGUuU1RSSU5HO1xuICAgIH1cbn1cblxuY2xhc3MgTXlTUUxOdW1iZXJWYWx1ZSBleHRlbmRzIE15U1FMVmFsdWVcbntcbiAgICBjb25zdHJ1Y3RvcihkYXRhOm51bWJlcikge1xuICAgICAgICBzdXBlcihkYXRhLE15U1FMVmFsdWVUeXBlLkVYUFJFU1NJT04pO1xuICAgIH1cbn1cblxuY2xhc3MgTXlTUUxTdHJpbmdWYWx1ZSBleHRlbmRzIE15U1FMVmFsdWVcbntcbiAgICBjb25zdHJ1Y3RvcihkYXRhOnN0cmluZykge1xuICAgICAgICBzdXBlcihkYXRhLE15U1FMVmFsdWVUeXBlLlNUUklORyk7XG4gICAgfVxufVxuXG5jbGFzcyBNeVNRTFN0cmluZ0V4cHJlc3Npb25WYWx1ZSBleHRlbmRzIE15U1FMVmFsdWVcbntcbiAgICBjb25zdHJ1Y3RvcihkYXRhOnN0cmluZykge1xuICAgICAgICBzdXBlcihkYXRhLE15U1FMVmFsdWVUeXBlLkVYUFJFU1NJT04pO1xuICAgIH1cbn1cblxuXG5leHBvcnQgZW51bSBNeVNRTEV4cHJlc3Npb24ge1xuICAgIE5PVyA9ICdOT1coKScsXG4gICAgTlVMTCA9ICdOVUxMJyxcbn1cblxuY29uc3Qgbm93VmFsdWUgPSBuZXcgTXlTUUxTdHJpbmdFeHByZXNzaW9uVmFsdWUoTXlTUUxFeHByZXNzaW9uLk5PVyk7XG5jb25zdCBudWxsVmFsdWUgPSBuZXcgTXlTUUxTdHJpbmdFeHByZXNzaW9uVmFsdWUoTXlTUUxFeHByZXNzaW9uLk5VTEwpO1xuXG5cbi8qXG5jbGFzcyBNeVNRTFZhbHVlXG57XG4gICAgc3RhdGljIGJvb2xlYW5Bc0ludChib29sOmJvb2xlYW4pOm51bWJlclxuICAgIHtcbiAgICAgICAgcmV0dXJuIGJvb2wgPyAxIDogMDtcbiAgICB9XG59XG4qL1xuXG5cblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEV4cHJlc3Npb25cbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcmVhZG9ubHkgbGVmdFNpZGU6c3RyaW5nLFxuICAgICAgICByZWFkb25seSBvcGVyYXRvcjpzdHJpbmcsXG4gICAgICAgIHJlYWRvbmx5IHJpZ2h0U2lkZTpNeVNRTFZhbHVlXG4gICAgKSB7XG4gICAgfVxufVxuXG5cbmV4cG9ydCBjbGFzcyBFeHByZXNzaW9uR3JvdXBcbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcmVhZG9ubHkgZXhwcmVzc2lvbnM6RXhwcmVzc2lvbltdLFxuICAgICAgICByZWFkb25seSBqb2luZXI6RXhwcmVzc2lvbkdyb3VwSm9pbmVyLFxuICAgICAgICByZWFkb25seSBzdXJyb3VuZEV4cHJlc3Npb25XaXRoQnJhY2tldHM6Ym9vbGVhbiA9IHRydWVcbiAgICApIHtcbiAgICB9XG59XG5cblxuZXhwb3J0IGZ1bmN0aW9uIGNyZWF0ZUV4cHJlc3Npb25Hcm91cChcbiAgICBtYXA6YW55LGpvaW5lcjpFeHByZXNzaW9uR3JvdXBKb2luZXJcbikge1xuICAgIGNvbnN0IGV4cHJlc3Npb25zOkV4cHJlc3Npb25bXSA9IFtdO1xuXG4gICAgLy8gc2hvdWxkIHdlIHVzZSBPYmplY3Qua2V5cygpP1xuICAgIGZvciAoY29uc3QgZmllbGROYW1lIGluIG1hcCkge1xuICAgICAgICBjb25zdCBmaWVsZFZhbHVlID0gbWFwW2ZpZWxkTmFtZV07XG5cbiAgICAgICAgZXhwcmVzc2lvbnMucHVzaChcbiAgICAgICAgICAgIHR5cGVvZiBmaWVsZFZhbHVlID09ICdzdHJpbmcnID9cbiAgICAgICAgICAgICAgICBuZXcgU3RyaW5nU3RhdGVtZW50KFxuICAgICAgICAgICAgICAgICAgICBmaWVsZE5hbWUsXG4gICAgICAgICAgICAgICAgICAgIGZpZWxkVmFsdWUgYXMgc3RyaW5nXG4gICAgICAgICAgICAgICAgKSA6XG4gICAgICAgICAgICAgICAgbmV3IE51bWJlclN0YXRlbWVudChcbiAgICAgICAgICAgICAgICAgICAgZmllbGROYW1lLFxuICAgICAgICAgICAgICAgICAgICBmaWVsZFZhbHVlXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICApO1xuICAgIH1cbiAgICBcbiAgICByZXR1cm4gbmV3IEV4cHJlc3Npb25Hcm91cChleHByZXNzaW9ucyxqb2luZXIpO1xufVxuXG5cbmV4cG9ydCBjbGFzcyBTaW5nbGVFeHByZXNzaW9uIGV4dGVuZHMgRXhwcmVzc2lvbkdyb3VwXG57XG4gICAgY29uc3RydWN0b3IoZXhwcmVzc2lvbjpFeHByZXNzaW9uKSB7XG4gICAgICAgIHN1cGVyKFtleHByZXNzaW9uXSx1bmRlZmluZWQpO1xuICAgIH1cbn1cbmV4cG9ydCBjbGFzcyBBbmRFeHByZXNzaW9uR3JvdXAgZXh0ZW5kcyBFeHByZXNzaW9uR3JvdXBcbntcbiAgICBjb25zdHJ1Y3RvcihleHByZXNzaW9uczpFeHByZXNzaW9uW10pIHtcbiAgICAgICAgc3VwZXIoZXhwcmVzc2lvbnMsRXhwcmVzc2lvbkdyb3VwSm9pbmVyLkFORCk7XG4gICAgfVxufVxuZXhwb3J0IGNsYXNzIE9yRXhwcmVzc2lvbkdyb3VwIGV4dGVuZHMgRXhwcmVzc2lvbkdyb3VwXG57XG4gICAgY29uc3RydWN0b3IoZXhwcmVzc2lvbnM6RXhwcmVzc2lvbltdKSB7XG4gICAgICAgIHN1cGVyKGV4cHJlc3Npb25zLEV4cHJlc3Npb25Hcm91cEpvaW5lci5PUik7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgVXBkYXRlU3RhdGVtZW50cyBleHRlbmRzIEV4cHJlc3Npb25Hcm91cFxue1xuICAgIGNvbnN0cnVjdG9yKGV4cHJlc3Npb25zOkV4cHJlc3Npb25bXSkge1xuICAgICAgICBzdXBlcihleHByZXNzaW9ucyxFeHByZXNzaW9uR3JvdXBKb2luZXIuQ09NTUEsZmFsc2UpO1xuICAgIH1cbn1cblxuXG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBGaWVsZFN0YXRlbWVudCBleHRlbmRzIEV4cHJlc3Npb25cbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgZmllbGROYW1lOnN0cmluZyxcbiAgICAgICAgdmFsdWU6TXlTUUxWYWx1ZSxcbiAgICAgICAgb3BlcmF0b3I6RXhwcmVzc2lvbk9wZXJhdG9yXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKGZpZWxkTmFtZSxvcGVyYXRvcix2YWx1ZSk7XG4gICAgfVxufVxuXG5cbmV4cG9ydCBjbGFzcyBOdW1iZXJTdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudFxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBmaWVsZE5hbWU6c3RyaW5nLFxuICAgICAgICBkYXRhOm51bWJlcixcbiAgICAgICAgb3BlcmF0b3I6RXhwcmVzc2lvbk9wZXJhdG9yID0gRXhwcmVzc2lvbk9wZXJhdG9yLkVRVUFMXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKFxuICAgICAgICAgICAgZmllbGROYW1lLFxuICAgICAgICAgICAgbmV3IE15U1FMTnVtYmVyVmFsdWUoZGF0YSksXG4gICAgICAgICAgICBvcGVyYXRvclxuICAgICAgICApO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIFN0cmluZ1N0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50XG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIGZpZWxkTmFtZTpzdHJpbmcsXG4gICAgICAgIGRhdGE6c3RyaW5nLFxuICAgICAgICBvcGVyYXRvcjpFeHByZXNzaW9uT3BlcmF0b3IgPSBFeHByZXNzaW9uT3BlcmF0b3IuRVFVQUxcbiAgICApIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICBmaWVsZE5hbWUsXG4gICAgICAgICAgICBuZXcgTXlTUUxTdHJpbmdWYWx1ZShkYXRhKSxcbiAgICAgICAgICAgIG9wZXJhdG9yXG4gICAgICAgICk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgU3RyaW5nRXhwcmVzc2lvblN0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50XG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIGZpZWxkTmFtZTpzdHJpbmcsXG4gICAgICAgIGRhdGE6c3RyaW5nLFxuICAgICAgICBvcGVyYXRvcjpFeHByZXNzaW9uT3BlcmF0b3IgPSBFeHByZXNzaW9uT3BlcmF0b3IuRVFVQUxcbiAgICApIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICBmaWVsZE5hbWUsXG4gICAgICAgICAgICBuZXcgTXlTUUxTdHJpbmdFeHByZXNzaW9uVmFsdWUoZGF0YSksXG4gICAgICAgICAgICBvcGVyYXRvclxuICAgICAgICApO1xuICAgIH1cbn1cblxuXG5leHBvcnQgY2xhc3MgTnVsbFVwZGF0ZVN0YXRlbWVudCBleHRlbmRzIEZpZWxkU3RhdGVtZW50XG57XG4gICAgY29uc3RydWN0b3IoZmllbGROYW1lOnN0cmluZykge1xuICAgICAgICBzdXBlcihmaWVsZE5hbWUsbnVsbFZhbHVlLEV4cHJlc3Npb25PcGVyYXRvci5FUVVBTCk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgTm93VXBkYXRlU3RhdGVtZW50IGV4dGVuZHMgRmllbGRTdGF0ZW1lbnRcbntcbiAgICBjb25zdHJ1Y3RvcihmaWVsZE5hbWU6c3RyaW5nKSB7XG4gICAgICAgIHN1cGVyKGZpZWxkTmFtZSxub3dWYWx1ZSxFeHByZXNzaW9uT3BlcmF0b3IuRVFVQUwpO1xuICAgIH1cbn1cblxuXG5leHBvcnQgY2xhc3MgRmllbGRJc051bGxTdGF0ZW1lbnQgZXh0ZW5kcyBGaWVsZFN0YXRlbWVudFxue1xuICAgIGNvbnN0cnVjdG9yKGZpZWxkTmFtZTpzdHJpbmcpIHtcbiAgICAgICAgc3VwZXIoZmllbGROYW1lLG51bGxWYWx1ZSxFeHByZXNzaW9uT3BlcmF0b3IuSVMpO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIEZpZWxkSXNOb3ROdWxsU3RhdGVtZW50IGV4dGVuZHMgRmllbGRTdGF0ZW1lbnRcbntcbiAgICBjb25zdHJ1Y3RvcihmaWVsZE5hbWU6c3RyaW5nKSB7XG4gICAgICAgIHN1cGVyKGZpZWxkTmFtZSxudWxsVmFsdWUsRXhwcmVzc2lvbk9wZXJhdG9yLklTX05PVCk7XG4gICAgfVxufSJdfQ==