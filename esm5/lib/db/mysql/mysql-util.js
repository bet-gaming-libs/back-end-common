/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
var MySQLExpression = /** @class */ (function () {
    function MySQLExpression() {
    }
    /**
     * @param {?} bool
     * @return {?}
     */
    MySQLExpression.booleanAsInt = /**
     * @param {?} bool
     * @return {?}
     */
    function (bool) {
        return bool ? 1 : 0;
    };
    MySQLExpression.NOW = 'NOW()';
    MySQLExpression.NULL = 'NULL';
    return MySQLExpression;
}());
export { MySQLExpression };
if (false) {
    /** @type {?} */
    MySQLExpression.NOW;
    /** @type {?} */
    MySQLExpression.NULL;
}
var MySQLValueType = /** @class */ (function () {
    function MySQLValueType() {
    }
    MySQLValueType.EXPRESSION = 0;
    MySQLValueType.STRING = 1;
    return MySQLValueType;
}());
export { MySQLValueType };
if (false) {
    /** @type {?} */
    MySQLValueType.EXPRESSION;
    /** @type {?} */
    MySQLValueType.STRING;
}
// unsupported: template constraints.
/**
 * @template T
 */
var 
// unsupported: template constraints.
/**
 * @template T
 */
UpdateStatement = /** @class */ (function () {
    function UpdateStatement(fieldName, type, data) {
        this.fieldName = fieldName;
        this.type = type;
        this.data = data;
    }
    Object.defineProperty(UpdateStatement.prototype, "value", {
        get: /**
         * @return {?}
         */
        function () {
            return {
                type: this.type,
                data: this.data
            };
        },
        enumerable: true,
        configurable: true
    });
    return UpdateStatement;
}());
// unsupported: template constraints.
/**
 * @template T
 */
export { UpdateStatement };
if (false) {
    /** @type {?} */
    UpdateStatement.prototype.fieldName;
    /** @type {?} */
    UpdateStatement.prototype.type;
    /** @type {?} */
    UpdateStatement.prototype.data;
}
var StringUpdateStatement = /** @class */ (function (_super) {
    tslib_1.__extends(StringUpdateStatement, _super);
    function StringUpdateStatement(fieldName, fieldValue) {
        return _super.call(this, fieldName, MySQLValueType.STRING, fieldValue) || this;
    }
    return StringUpdateStatement;
}(UpdateStatement));
export { StringUpdateStatement };
// unsupported: template constraints.
/**
 * @template T
 */
var 
// unsupported: template constraints.
/**
 * @template T
 */
ExpressionUpdateStatement = /** @class */ (function (_super) {
    tslib_1.__extends(ExpressionUpdateStatement, _super);
    function ExpressionUpdateStatement(fieldName, fieldValue) {
        return _super.call(this, fieldName, MySQLValueType.EXPRESSION, fieldValue) || this;
    }
    return ExpressionUpdateStatement;
}(UpdateStatement));
// unsupported: template constraints.
/**
 * @template T
 */
export { ExpressionUpdateStatement };
var NumberUpdateStatement = /** @class */ (function (_super) {
    tslib_1.__extends(NumberUpdateStatement, _super);
    function NumberUpdateStatement() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NumberUpdateStatement;
}(ExpressionUpdateStatement));
export { NumberUpdateStatement };
var CalculationUpdateStatement = /** @class */ (function (_super) {
    tslib_1.__extends(CalculationUpdateStatement, _super);
    function CalculationUpdateStatement() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return CalculationUpdateStatement;
}(ExpressionUpdateStatement));
export { CalculationUpdateStatement };
var NowUpdateStatement = /** @class */ (function (_super) {
    tslib_1.__extends(NowUpdateStatement, _super);
    function NowUpdateStatement(fieldName) {
        return _super.call(this, fieldName, MySQLExpression.NOW) || this;
    }
    return NowUpdateStatement;
}(CalculationUpdateStatement));
export { NowUpdateStatement };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlzcWwtdXRpbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2RiL215c3FsL215c3FsLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0lBUVcsNEJBQVk7Ozs7SUFBbkIsVUFBb0IsSUFBWTtRQUU1QixNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUN2QjswQkFOWSxPQUFPOzJCQUNOLE1BQU07MEJBTnhCOztTQUdhLGVBQWU7Ozs7Ozs7Ozs7Z0NBYUosQ0FBQzs0QkFDTCxDQUFDO3lCQWpCckI7O1NBY2EsY0FBYzs7Ozs7Ozs7Ozs7QUFNM0I7Ozs7O0FBQUE7SUFFSSx5QkFDYSxTQUFnQixFQUNqQixNQUNBO1FBRkMsY0FBUyxHQUFULFNBQVMsQ0FBTztRQUNqQixTQUFJLEdBQUosSUFBSTtRQUNKLFNBQUksR0FBSixJQUFJO0tBR2Y7SUFFRCxzQkFBSSxrQ0FBSzs7OztRQUFUO1lBQ0ksTUFBTSxDQUFDO2dCQUNILElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtnQkFDZixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7YUFDbEIsQ0FBQTtTQUNKOzs7T0FBQTswQkFuQ0w7SUFvQ0MsQ0FBQTs7Ozs7QUFoQkQsMkJBZ0JDOzs7Ozs7Ozs7QUFFRCxJQUFBO0lBQTJDLGlEQUF1QjtJQUU5RCwrQkFBWSxTQUFnQixFQUFDLFVBQWlCO2VBQzFDLGtCQUFNLFNBQVMsRUFBQyxjQUFjLENBQUMsTUFBTSxFQUFDLFVBQVUsQ0FBQztLQUNwRDtnQ0ExQ0w7RUFzQzJDLGVBQWUsRUFLekQsQ0FBQTtBQUxELGlDQUtDOzs7OztBQUVEOzs7OztBQUFBO0lBQXdFLHFEQUFrQjtJQUV0RixtQ0FBWSxTQUFnQixFQUFDLFVBQVk7ZUFDckMsa0JBQU0sU0FBUyxFQUFDLGNBQWMsQ0FBQyxVQUFVLEVBQUMsVUFBVSxDQUFDO0tBQ3hEO29DQWpETDtFQTZDd0UsZUFBZSxFQUt0RixDQUFBOzs7OztBQUxELHFDQUtDO0FBRUQsSUFBQTtJQUEyQyxpREFBaUM7Ozs7Z0NBcEQ1RTtFQW9EMkMseUJBQXlCLEVBRW5FLENBQUE7QUFGRCxpQ0FFQztBQUVELElBQUE7SUFBZ0Qsc0RBQWlDOzs7O3FDQXhEakY7RUF3RGdELHlCQUF5QixFQUV4RSxDQUFBO0FBRkQsc0NBRUM7QUFFRCxJQUFBO0lBQXdDLDhDQUEwQjtJQUU5RCw0QkFBWSxTQUFnQjtlQUN4QixrQkFBTSxTQUFTLEVBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQztLQUN2Qzs2QkFoRUw7RUE0RHdDLDBCQUEwQixFQUtqRSxDQUFBO0FBTEQsOEJBS0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJVXBkYXRlU3RhdGVtZW50RGF0YSB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5cblxuZXhwb3J0IGNsYXNzIE15U1FMRXhwcmVzc2lvblxue1xuICAgIHN0YXRpYyBOT1cgPSAnTk9XKCknO1xuICAgIHN0YXRpYyBOVUxMID0gJ05VTEwnO1xuXG4gICAgc3RhdGljIGJvb2xlYW5Bc0ludChib29sOmJvb2xlYW4pOm51bWJlclxuICAgIHtcbiAgICAgICAgcmV0dXJuIGJvb2wgPyAxIDogMDtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBNeVNRTFZhbHVlVHlwZVxue1xuICAgIHN0YXRpYyBFWFBSRVNTSU9OID0gMDtcbiAgICBzdGF0aWMgU1RSSU5HID0gMTtcbn1cblxuZXhwb3J0IGNsYXNzIFVwZGF0ZVN0YXRlbWVudDxUIGV4dGVuZHMgc3RyaW5nfG51bWJlcj4gaW1wbGVtZW50cyBJVXBkYXRlU3RhdGVtZW50RGF0YVxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICByZWFkb25seSBmaWVsZE5hbWU6c3RyaW5nLFxuICAgICAgICBwcml2YXRlIHR5cGU6bnVtYmVyLFxuICAgICAgICBwcml2YXRlIGRhdGE6VFxuICAgICkge1xuXG4gICAgfVxuXG4gICAgZ2V0IHZhbHVlKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgdHlwZTogdGhpcy50eXBlLFxuICAgICAgICAgICAgZGF0YTogdGhpcy5kYXRhXG4gICAgICAgIH1cbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBTdHJpbmdVcGRhdGVTdGF0ZW1lbnQgZXh0ZW5kcyBVcGRhdGVTdGF0ZW1lbnQ8c3RyaW5nPlxue1xuICAgIGNvbnN0cnVjdG9yKGZpZWxkTmFtZTpzdHJpbmcsZmllbGRWYWx1ZTpzdHJpbmcpIHtcbiAgICAgICAgc3VwZXIoZmllbGROYW1lLE15U1FMVmFsdWVUeXBlLlNUUklORyxmaWVsZFZhbHVlKTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBFeHByZXNzaW9uVXBkYXRlU3RhdGVtZW50PFQgZXh0ZW5kcyBzdHJpbmd8bnVtYmVyPiBleHRlbmRzIFVwZGF0ZVN0YXRlbWVudDxUPlxue1xuICAgIGNvbnN0cnVjdG9yKGZpZWxkTmFtZTpzdHJpbmcsZmllbGRWYWx1ZTpUKSB7XG4gICAgICAgIHN1cGVyKGZpZWxkTmFtZSxNeVNRTFZhbHVlVHlwZS5FWFBSRVNTSU9OLGZpZWxkVmFsdWUpO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIE51bWJlclVwZGF0ZVN0YXRlbWVudCBleHRlbmRzIEV4cHJlc3Npb25VcGRhdGVTdGF0ZW1lbnQ8bnVtYmVyPlxue1xufVxuXG5leHBvcnQgY2xhc3MgQ2FsY3VsYXRpb25VcGRhdGVTdGF0ZW1lbnQgZXh0ZW5kcyBFeHByZXNzaW9uVXBkYXRlU3RhdGVtZW50PHN0cmluZz5cbntcbn1cblxuZXhwb3J0IGNsYXNzIE5vd1VwZGF0ZVN0YXRlbWVudCBleHRlbmRzIENhbGN1bGF0aW9uVXBkYXRlU3RhdGVtZW50XG57XG4gICAgY29uc3RydWN0b3IoZmllbGROYW1lOnN0cmluZykge1xuICAgICAgICBzdXBlcihmaWVsZE5hbWUsTXlTUUxFeHByZXNzaW9uLk5PVyk7XG4gICAgfVxufSJdfQ==