/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-txn-executor.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { sleep, debugMessage } from 'earnbet-common';
/** @type {?} */
var _1HoursInSeconds = 60 * 60;
var EosTransactionExecutorBase = /** @class */ (function () {
    function EosTransactionExecutorBase(actions, eos, txnChecker, retryOnError, secondsBeforeRetry, expireInSeconds) {
        if (retryOnError === void 0) { retryOnError = true; }
        if (secondsBeforeRetry === void 0) { secondsBeforeRetry = 10; }
        if (expireInSeconds === void 0) { expireInSeconds = _1HoursInSeconds; }
        this.actions = actions;
        this.eos = eos;
        this.txnChecker = txnChecker;
        this.retryOnError = retryOnError;
        this.secondsBeforeRetry = secondsBeforeRetry;
        this.expireInSeconds = expireInSeconds;
        this.numOfRetries = 0;
        this.signedTransaction = undefined;
        // include first action for now
        // include other actions as well?
        var _a = this.actions[0], account = _a.account, name = _a.name;
        this.actionId = account + ' - ' + name;
    }
    /**
     * @return {?}
     */
    EosTransactionExecutorBase.prototype.execute = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        debugMessage('Attempting to Execute Transaction: ' + this.actionId);
                        console.log(this.actions);
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 4, , 5]);
                        return [4 /*yield*/, this.sign()];
                    case 2:
                        _b.sent();
                        // attempt to broadcast
                        _a = this;
                        return [4 /*yield*/, broadcastSignedTransaction(this.eos, this.signedTransaction.transaction)];
                    case 3:
                        // attempt to broadcast
                        _a.broadcastedTxnId = _b.sent();
                        this.broadcastedTime = Date.now();
                        debugMessage(this.actionId + ' Broadcasted: ' + this.broadcastedTxnId);
                        return [2 /*return*/, this.broadcastedTxnId];
                    case 4:
                        error_1 = _b.sent();
                        return [2 /*return*/, this.onBroadcastError(error_1)];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    EosTransactionExecutorBase.prototype.sign = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, error_2;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 3, , 5]);
                        if (!(this.signedTransaction == undefined)) return [3 /*break*/, 2];
                        _a = this;
                        return [4 /*yield*/, signTransaction(this.eos, this.actions, this.expireInSeconds)];
                    case 1:
                        _a.signedTransaction = _b.sent();
                        _b.label = 2;
                    case 2:
                        debugMessage('Transaction Signed: ' + this.signedTransaction.transaction_id);
                        return [2 /*return*/, this.signedTransaction];
                    case 3:
                        error_2 = _b.sent();
                        console.error(error_2);
                        return [4 /*yield*/, sleep(1000)];
                    case 4:
                        _b.sent();
                        return [2 /*return*/, this.sign()];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @protected
     * @param {?} error
     * @return {?}
     */
    EosTransactionExecutorBase.prototype.onBroadcastError = /**
     * @protected
     * @param {?} error
     * @return {?}
     */
    function (error) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var isDuplicateTxn, maxNumOfRetries;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.error(error);
                        isDuplicateTxn = isDuplicateTxnError(error);
                        // if duplicate error, then stop broadcasting process
                        // return originally broadcasted txnId
                        if (isDuplicateTxn) {
                            return [2 /*return*/, this.broadcastedTxnId];
                        }
                        if (!this.retryOnError) return [3 /*break*/, 2];
                        maxNumOfRetries = (_1HoursInSeconds / this.secondsBeforeRetry) - 1;
                        if (!(this.numOfRetries++ < maxNumOfRetries)) return [3 /*break*/, 2];
                        // wait and then retry
                        return [4 /*yield*/, sleep(1000 * this.secondsBeforeRetry)];
                    case 1:
                        // wait and then retry
                        _a.sent();
                        return [2 /*return*/, this.execute()];
                    case 2: throw error;
                }
            });
        });
    };
    /**
     * @return {?}
     */
    EosTransactionExecutorBase.prototype.confirm = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var timeToWait, remainingTimeToWait, txnId, isConfirmed, error_3;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.broadcastedTxnId) {
                            throw new Error('Transaction has not yet been succesfully broadcasted!');
                        }
                        if (!this.txnChecker) {
                            throw new Error('no Transaction Checker function defined!');
                        }
                        // wait 5 minutes before checking irreversibility
                        timeToWait = 5 * 60 * 1000;
                        remainingTimeToWait = timeToWait -
                            (Date.now() - this.broadcastedTime);
                        console.log({ remainingTimeToWait: remainingTimeToWait });
                        if (!(remainingTimeToWait > 0)) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep(remainingTimeToWait)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        txnId = this.broadcastedTxnId;
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 5, , 7]);
                        return [4 /*yield*/, this.txnChecker(txnId)];
                    case 4:
                        isConfirmed = _a.sent();
                        if (!isConfirmed) {
                            debugMessage(this.actionId + ' Txn: ' + txnId + ' is NOT CONFIRMED!');
                            return [2 /*return*/, this.execute()];
                        }
                        debugMessage(this.actionId + ' Txn: ' + txnId + ' is IRREVERSIBLE!');
                        return [2 /*return*/, txnId];
                    case 5:
                        error_3 = _a.sent();
                        debugMessage('Cound Not Confirm Txn: ' + txnId);
                        console.error(error_3);
                        // wait
                        return [4 /*yield*/, sleep(5 * 1000)];
                    case 6:
                        // wait
                        _a.sent();
                        return [2 /*return*/, this.execute()];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    return EosTransactionExecutorBase;
}());
export { EosTransactionExecutorBase };
if (false) {
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.numOfRetries;
    /**
     * @type {?}
     * @protected
     */
    EosTransactionExecutorBase.prototype.actionId;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.signedTransaction;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.broadcastedTxnId;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.broadcastedTime;
    /** @type {?} */
    EosTransactionExecutorBase.prototype.actions;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.eos;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.txnChecker;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.retryOnError;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.secondsBeforeRetry;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.expireInSeconds;
}
/**
 * @template T
 */
var /**
 * @template T
 */
EosTransactionExecutor = /** @class */ (function (_super) {
    tslib_1.__extends(EosTransactionExecutor, _super);
    function EosTransactionExecutor(actions, eosUtility, txnChecker, retryOnError, secondsBeforeRetry) {
        if (retryOnError === void 0) { retryOnError = true; }
        if (secondsBeforeRetry === void 0) { secondsBeforeRetry = 10; }
        return _super.call(this, actions.map((/**
         * @param {?} action
         * @return {?}
         */
        function (action) { return (tslib_1.__assign({}, action, { authorization: eosUtility.authorization })); })), eosUtility.transactor, txnChecker, retryOnError, secondsBeforeRetry) || this;
    }
    return EosTransactionExecutor;
}(EosTransactionExecutorBase));
/**
 * @template T
 */
export { EosTransactionExecutor };
/*
Error: {"code":409,"message":"Conflict","error":{"code":3040008,"name":"tx_duplicate","what":"Duplicate transaction","details":[{"message":"duplicate transaction 7be2845618fa965d89ff8f3b368ed1c768dab63303c38c7d8a5e9f031d7a23c6","file":"producer_plugin.cpp","line_number":527,"method":"process_incoming_transaction_async"}]}}
*/
/**
 * @param {?} error
 * @return {?}
 */
function isDuplicateTxnError(error) {
    var e_1, _a;
    /** @type {?} */
    var searchStrings = [
        'tx_duplicate',
        'Duplicate transaction',
        'duplicate transaction'
    ];
    /** @type {?} */
    var errorString = String(error);
    try {
        for (var searchStrings_1 = tslib_1.__values(searchStrings), searchStrings_1_1 = searchStrings_1.next(); !searchStrings_1_1.done; searchStrings_1_1 = searchStrings_1.next()) {
            var searchString = searchStrings_1_1.value;
            if (errorString.indexOf(searchString) > -1) {
                return true;
            }
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (searchStrings_1_1 && !searchStrings_1_1.done && (_a = searchStrings_1.return)) _a.call(searchStrings_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return false;
}
/**
 * @record
 */
function ISignedTransaction() { }
if (false) {
    /** @type {?} */
    ISignedTransaction.prototype.compression;
    /** @type {?} */
    ISignedTransaction.prototype.transaction;
    /** @type {?} */
    ISignedTransaction.prototype.signatures;
}
/**
 * @record
 */
function ISignedTransactionResult() { }
if (false) {
    /** @type {?} */
    ISignedTransactionResult.prototype.transaction_id;
    /** @type {?} */
    ISignedTransactionResult.prototype.transaction;
}
/**
 * @record
 */
function IBroadcastedTransactionResult() { }
if (false) {
    /** @type {?} */
    IBroadcastedTransactionResult.prototype.transaction_id;
}
/**
 * @param {?} transactor
 * @param {?} actions
 * @param {?} expireInSeconds
 * @return {?}
 * @this {*}
 */
function signTransaction(transactor, actions, expireInSeconds) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var result;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, transactor.transaction({
                        actions: actions
                    }, {
                        broadcast: false,
                        sign: true,
                        expireInSeconds: expireInSeconds
                    })];
                case 1:
                    result = _a.sent();
                    return [2 /*return*/, result];
            }
        });
    });
}
/**
 * @param {?} transactor
 * @param {?} transaction
 * @return {?}
 * @this {*}
 */
function broadcastSignedTransaction(transactor, transaction) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var result;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, transactor.pushTransaction(transaction)];
                case 1:
                    result = _a.sent();
                    return [2 /*return*/, result.transaction_id];
            }
        });
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLXR4bi1leGVjdXRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2Vvcy9lb3MtdHhuLWV4ZWN1dG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBc0MsS0FBSyxFQUFFLFlBQVksRUFBd0IsTUFBTSxnQkFBZ0IsQ0FBQzs7SUFPekcsZ0JBQWdCLEdBQUcsRUFBRSxHQUFHLEVBQUU7QUFHaEM7SUFTQyxvQ0FDVSxPQUFnQyxFQUN4QixHQUFHLEVBQ0gsVUFBK0IsRUFDL0IsWUFBNEIsRUFDNUIsa0JBQStCLEVBQy9CLGVBQTBDO1FBRjFDLDZCQUFBLEVBQUEsbUJBQTRCO1FBQzVCLG1DQUFBLEVBQUEsdUJBQStCO1FBQy9CLGdDQUFBLEVBQUEsa0NBQTBDO1FBTGxELFlBQU8sR0FBUCxPQUFPLENBQXlCO1FBQ3hCLFFBQUcsR0FBSCxHQUFHLENBQUE7UUFDSCxlQUFVLEdBQVYsVUFBVSxDQUFxQjtRQUMvQixpQkFBWSxHQUFaLFlBQVksQ0FBZ0I7UUFDNUIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFhO1FBQy9CLG9CQUFlLEdBQWYsZUFBZSxDQUEyQjtRQWRwRCxpQkFBWSxHQUFHLENBQUMsQ0FBQztRQUlqQixzQkFBaUIsR0FBNkIsU0FBUyxDQUFDOzs7UUFjekQsSUFBQSxvQkFBaUMsRUFBaEMsb0JBQU8sRUFBRSxjQUF1QjtRQUV2QyxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sR0FBRyxLQUFLLEdBQUcsSUFBSSxDQUFDO0lBQ3hDLENBQUM7Ozs7SUFJSyw0Q0FBTzs7O0lBQWI7Ozs7Ozt3QkFDQyxZQUFZLENBQ1gscUNBQXFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FDckQsQ0FBQzt3QkFDRixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzs7Ozt3QkFJekIscUJBQU0sSUFBSSxDQUFDLElBQUksRUFBRSxFQUFBOzt3QkFBakIsU0FBaUIsQ0FBQzt3QkFFbEIsdUJBQXVCO3dCQUN2QixLQUFBLElBQUksQ0FBQTt3QkFBb0IscUJBQU0sMEJBQTBCLENBQ3ZELElBQUksQ0FBQyxHQUFHLEVBQ1IsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FDbEMsRUFBQTs7d0JBSkQsdUJBQXVCO3dCQUN2QixHQUFLLGdCQUFnQixHQUFHLFNBR3ZCLENBQUM7d0JBRUYsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7d0JBRWxDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO3dCQUV2RSxzQkFBTyxJQUFJLENBQUMsZ0JBQWdCLEVBQUM7Ozt3QkFFN0Isc0JBQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQUssQ0FBQyxFQUFDOzs7OztLQUVyQzs7OztJQUVLLHlDQUFJOzs7SUFBVjs7Ozs7Ozs2QkFHTSxDQUFBLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxTQUFTLENBQUEsRUFBbkMsd0JBQW1DO3dCQUN0QyxLQUFBLElBQUksQ0FBQTt3QkFBcUIscUJBQU0sZUFBZSxDQUM3QyxJQUFJLENBQUMsR0FBRyxFQUNSLElBQUksQ0FBQyxPQUFPLEVBQ1osSUFBSSxDQUFDLGVBQWUsQ0FDcEIsRUFBQTs7d0JBSkQsR0FBSyxpQkFBaUIsR0FBRyxTQUl4QixDQUFDOzs7d0JBR0gsWUFBWSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsQ0FBQzt3QkFFN0Usc0JBQU8sSUFBSSxDQUFDLGlCQUFpQixFQUFDOzs7d0JBRTlCLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBSyxDQUFDLENBQUM7d0JBRXJCLHFCQUFNLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBQTs7d0JBQWpCLFNBQWlCLENBQUM7d0JBRWxCLHNCQUFPLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBQzs7Ozs7S0FFcEI7Ozs7OztJQUVlLHFEQUFnQjs7Ozs7SUFBaEMsVUFBaUMsS0FBSzs7Ozs7O3dCQUNyQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUVmLGNBQWMsR0FBRyxtQkFBbUIsQ0FBQyxLQUFLLENBQUM7d0JBRWpELHFEQUFxRDt3QkFDckQsc0NBQXNDO3dCQUN0QyxJQUFJLGNBQWMsRUFBRTs0QkFDbkIsc0JBQU8sSUFBSSxDQUFDLGdCQUFnQixFQUFDO3lCQUM3Qjs2QkFHRyxJQUFJLENBQUMsWUFBWSxFQUFqQix3QkFBaUI7d0JBQ2QsZUFBZSxHQUFHLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQzs2QkFFcEUsQ0FBQSxJQUFJLENBQUMsWUFBWSxFQUFFLEdBQUcsZUFBZSxDQUFBLEVBQXJDLHdCQUFxQzt3QkFDeEMsc0JBQXNCO3dCQUN0QixxQkFBTSxLQUFLLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxFQUFBOzt3QkFEM0Msc0JBQXNCO3dCQUN0QixTQUEyQyxDQUFDO3dCQUU1QyxzQkFBTyxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUM7NEJBS3hCLE1BQU0sS0FBSyxDQUFDOzs7O0tBQ1o7Ozs7SUFHSyw0Q0FBTzs7O0lBQWI7Ozs7Ozt3QkFDQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFOzRCQUMzQixNQUFNLElBQUksS0FBSyxDQUFDLHVEQUF1RCxDQUFDLENBQUM7eUJBQ3pFO3dCQUVELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFOzRCQUNyQixNQUFNLElBQUksS0FBSyxDQUFDLDBDQUEwQyxDQUFDLENBQUM7eUJBQzVEOzt3QkFJSyxVQUFVLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJO3dCQUUxQixtQkFBbUIsR0FDeEIsVUFBVTs0QkFDVixDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO3dCQUVwQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUMsbUJBQW1CLHFCQUFBLEVBQUMsQ0FBQyxDQUFDOzZCQUUvQixDQUFBLG1CQUFtQixHQUFHLENBQUMsQ0FBQSxFQUF2Qix3QkFBdUI7d0JBQzFCLHFCQUFNLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxFQUFBOzt3QkFBaEMsU0FBZ0MsQ0FBQzs7O3dCQUk1QixLQUFLLEdBQUcsSUFBSSxDQUFDLGdCQUFnQjs7Ozt3QkFJZCxxQkFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFBOzt3QkFBMUMsV0FBVyxHQUFHLFNBQTRCO3dCQUVoRCxJQUFJLENBQUMsV0FBVyxFQUFFOzRCQUNqQixZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLEdBQUcsS0FBSyxHQUFHLG9CQUFvQixDQUFDLENBQUM7NEJBRXRFLHNCQUFPLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBQzt5QkFDdEI7d0JBR0QsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxHQUFHLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxDQUFDO3dCQUVyRSxzQkFBTyxLQUFLLEVBQUM7Ozt3QkFFYixZQUFZLENBQUMseUJBQXlCLEdBQUcsS0FBSyxDQUFDLENBQUM7d0JBQ2hELE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBSyxDQUFDLENBQUM7d0JBRXJCLE9BQU87d0JBQ1AscUJBQU0sS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBQTs7d0JBRHJCLE9BQU87d0JBQ1AsU0FBcUIsQ0FBQzt3QkFFdEIsc0JBQU8sSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFDOzs7OztLQUV2QjtJQUNGLGlDQUFDO0FBQUQsQ0FBQyxBQXpKRCxJQXlKQzs7Ozs7OztJQXhKQSxrREFBeUI7Ozs7O0lBRXpCLDhDQUFvQzs7Ozs7SUFFcEMsdURBQWdFOzs7OztJQUNoRSxzREFBaUM7Ozs7O0lBQ2pDLHFEQUFnQzs7SUFHL0IsNkNBQXlDOzs7OztJQUN6Qyx5Q0FBb0I7Ozs7O0lBQ3BCLGdEQUFnRDs7Ozs7SUFDaEQsa0RBQTZDOzs7OztJQUM3Qyx3REFBZ0Q7Ozs7O0lBQ2hELHFEQUEyRDs7Ozs7QUE2STdEOzs7O0lBQTRFLGtEQUEwQjtJQUNyRyxnQ0FDQyxPQUF5QixFQUN6QixVQUEwQixFQUMxQixVQUErQixFQUMvQixZQUFtQixFQUNuQixrQkFBdUI7UUFEdkIsNkJBQUEsRUFBQSxtQkFBbUI7UUFDbkIsbUNBQUEsRUFBQSx1QkFBdUI7ZUFFdkIsa0JBQ0MsT0FBTyxDQUFDLEdBQUc7Ozs7UUFDVixVQUFDLE1BQU0sSUFBSyxPQUFBLHNCQUNSLE1BQU0sSUFDVCxhQUFhLEVBQUUsVUFBVSxDQUFDLGFBQWEsSUFDdEMsRUFIVSxDQUdWLEVBQ0YsRUFDRCxVQUFVLENBQUMsVUFBVSxFQUNyQixVQUFVLEVBQ1YsWUFBWSxFQUNaLGtCQUFrQixDQUNsQjtJQUNGLENBQUM7SUFDRiw2QkFBQztBQUFELENBQUMsQUFyQkQsQ0FBNEUsMEJBQTBCLEdBcUJyRzs7Ozs7Ozs7Ozs7O0FBT0QsU0FBUyxtQkFBbUIsQ0FBQyxLQUFLOzs7UUFDM0IsYUFBYSxHQUFHO1FBQ3JCLGNBQWM7UUFDZCx1QkFBdUI7UUFDdkIsdUJBQXVCO0tBQ3ZCOztRQUVLLFdBQVcsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDOztRQUVqQyxLQUEyQixJQUFBLGtCQUFBLGlCQUFBLGFBQWEsQ0FBQSw0Q0FBQSx1RUFBRTtZQUFyQyxJQUFNLFlBQVksMEJBQUE7WUFDdEIsSUFBSSxXQUFXLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUMzQyxPQUFPLElBQUksQ0FBQzthQUNaO1NBQ0Q7Ozs7Ozs7OztJQUVELE9BQU8sS0FBSyxDQUFDO0FBQ2QsQ0FBQzs7OztBQUlELGlDQWlCQzs7O0lBaEJBLHlDQUFvQjs7SUFDcEIseUNBWUU7O0lBRUYsd0NBQXFCOzs7OztBQUd0Qix1Q0FHQzs7O0lBRkEsa0RBQXVCOztJQUN2QiwrQ0FBZ0M7Ozs7O0FBR2pDLDRDQUVDOzs7SUFEQSx1REFBdUI7Ozs7Ozs7OztBQUt4QixTQUFlLGVBQWUsQ0FDN0IsVUFBVSxFQUNWLE9BQWdDLEVBQ2hDLGVBQXVCOzs7Ozt3QkFFa0IscUJBQU0sVUFBVSxDQUFDLFdBQVcsQ0FDcEU7d0JBQ0MsT0FBTyxTQUFBO3FCQUNQLEVBQ0Q7d0JBQ0MsU0FBUyxFQUFFLEtBQUs7d0JBQ2hCLElBQUksRUFBRSxJQUFJO3dCQUNWLGVBQWUsaUJBQUE7cUJBQ2YsQ0FDRCxFQUFBOztvQkFUSyxNQUFNLEdBQTZCLFNBU3hDO29CQUVELHNCQUFPLE1BQU0sRUFBQzs7OztDQUNkOzs7Ozs7O0FBRUQsU0FBZSwwQkFBMEIsQ0FDeEMsVUFBVSxFQUNWLFdBQStCOzs7Ozt3QkFFZSxxQkFBTSxVQUFVLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxFQUFBOztvQkFBckYsTUFBTSxHQUFrQyxTQUE2QztvQkFFM0Ysc0JBQU8sTUFBTSxDQUFDLGNBQWMsRUFBQzs7OztDQUM3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7VHhuQ29uZmlybWVkQ2hlY2tlciwgSUVvc0FjdGlvbkRhdGEsIHNsZWVwLCBkZWJ1Z01lc3NhZ2UsIElFb3NUcmFuc2FjdGlvbkFjdGlvbn0gZnJvbSAnZWFybmJldC1jb21tb24nO1xuXG5pbXBvcnQgeyBJRW9zVXRpbGl0eSB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBJRW9zQWNjb3VudE5hbWVzQmFzZSB9IGZyb20gJy4vZW9zLXNldHRpbmdzJztcblxuXG5cbmNvbnN0IF8xSG91cnNJblNlY29uZHMgPSA2MCAqIDYwO1xuXG5cbmV4cG9ydCBjbGFzcyBFb3NUcmFuc2FjdGlvbkV4ZWN1dG9yQmFzZSB7XG5cdHByaXZhdGUgbnVtT2ZSZXRyaWVzID0gMDtcblxuXHRwcm90ZWN0ZWQgcmVhZG9ubHkgYWN0aW9uSWQ6IHN0cmluZztcblxuXHRwcml2YXRlIHNpZ25lZFRyYW5zYWN0aW9uOiBJU2lnbmVkVHJhbnNhY3Rpb25SZXN1bHQgPSB1bmRlZmluZWQ7XG5cdHByaXZhdGUgYnJvYWRjYXN0ZWRUeG5JZDogc3RyaW5nO1xuXHRwcml2YXRlIGJyb2FkY2FzdGVkVGltZTogbnVtYmVyO1xuXG5cdGNvbnN0cnVjdG9yKFxuXHRcdHJlYWRvbmx5IGFjdGlvbnM6IElFb3NUcmFuc2FjdGlvbkFjdGlvbltdLFxuXHRcdHByaXZhdGUgcmVhZG9ubHkgZW9zLFxuXHRcdHByaXZhdGUgcmVhZG9ubHkgdHhuQ2hlY2tlcjogVHhuQ29uZmlybWVkQ2hlY2tlcixcblx0XHRwcml2YXRlIHJlYWRvbmx5IHJldHJ5T25FcnJvcjogYm9vbGVhbiA9IHRydWUsXG5cdFx0cHJpdmF0ZSByZWFkb25seSBzZWNvbmRzQmVmb3JlUmV0cnk6IG51bWJlciA9IDEwLFxuXHRcdHByaXZhdGUgcmVhZG9ubHkgZXhwaXJlSW5TZWNvbmRzOiBudW1iZXIgPSBfMUhvdXJzSW5TZWNvbmRzXG5cdCkge1xuXHRcdC8vIGluY2x1ZGUgZmlyc3QgYWN0aW9uIGZvciBub3dcblx0XHQvLyBpbmNsdWRlIG90aGVyIGFjdGlvbnMgYXMgd2VsbD9cblx0XHRjb25zdCB7YWNjb3VudCwgbmFtZX0gPSB0aGlzLmFjdGlvbnNbMF07XG5cblx0XHR0aGlzLmFjdGlvbklkID0gYWNjb3VudCArICcgLSAnICsgbmFtZTtcblx0fVxuXG5cblxuXHRhc3luYyBleGVjdXRlKCk6IFByb21pc2U8c3RyaW5nPiB7XG5cdFx0ZGVidWdNZXNzYWdlKFxuXHRcdFx0J0F0dGVtcHRpbmcgdG8gRXhlY3V0ZSBUcmFuc2FjdGlvbjogJyArIHRoaXMuYWN0aW9uSWRcblx0XHQpO1xuXHRcdGNvbnNvbGUubG9nKHRoaXMuYWN0aW9ucyk7XG5cblxuXHRcdHRyeSB7XG5cdFx0XHRhd2FpdCB0aGlzLnNpZ24oKTtcblxuXHRcdFx0Ly8gYXR0ZW1wdCB0byBicm9hZGNhc3Rcblx0XHRcdHRoaXMuYnJvYWRjYXN0ZWRUeG5JZCA9IGF3YWl0IGJyb2FkY2FzdFNpZ25lZFRyYW5zYWN0aW9uKFxuXHRcdFx0XHR0aGlzLmVvcyxcblx0XHRcdFx0dGhpcy5zaWduZWRUcmFuc2FjdGlvbi50cmFuc2FjdGlvblxuXHRcdFx0KTtcblxuXHRcdFx0dGhpcy5icm9hZGNhc3RlZFRpbWUgPSBEYXRlLm5vdygpO1xuXG5cdFx0XHRkZWJ1Z01lc3NhZ2UodGhpcy5hY3Rpb25JZCArICcgQnJvYWRjYXN0ZWQ6ICcgKyB0aGlzLmJyb2FkY2FzdGVkVHhuSWQpO1xuXG5cdFx0XHRyZXR1cm4gdGhpcy5icm9hZGNhc3RlZFR4bklkO1xuXHRcdH0gY2F0Y2ggKGVycm9yKSB7XG5cdFx0XHRyZXR1cm4gdGhpcy5vbkJyb2FkY2FzdEVycm9yKGVycm9yKTtcblx0XHR9XG5cdH1cblxuXHRhc3luYyBzaWduKCk6IFByb21pc2U8SVNpZ25lZFRyYW5zYWN0aW9uUmVzdWx0PiB7XG5cdFx0dHJ5IHtcblx0XHRcdC8vIHNpZ24gdHJhbnNhY3Rpb24gaWYgbm90IGFscmVhZHkgc2lnbmVkXG5cdFx0XHRpZiAodGhpcy5zaWduZWRUcmFuc2FjdGlvbiA9PSB1bmRlZmluZWQpIHtcblx0XHRcdFx0dGhpcy5zaWduZWRUcmFuc2FjdGlvbiA9IGF3YWl0IHNpZ25UcmFuc2FjdGlvbihcblx0XHRcdFx0XHR0aGlzLmVvcyxcblx0XHRcdFx0XHR0aGlzLmFjdGlvbnMsXG5cdFx0XHRcdFx0dGhpcy5leHBpcmVJblNlY29uZHNcblx0XHRcdFx0KTtcblx0XHRcdH1cblxuXHRcdFx0ZGVidWdNZXNzYWdlKCdUcmFuc2FjdGlvbiBTaWduZWQ6ICcgKyB0aGlzLnNpZ25lZFRyYW5zYWN0aW9uLnRyYW5zYWN0aW9uX2lkKTtcblxuXHRcdFx0cmV0dXJuIHRoaXMuc2lnbmVkVHJhbnNhY3Rpb247XG5cdFx0fSBjYXRjaCAoZXJyb3IpIHtcblx0XHRcdGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG5cdFx0XHRhd2FpdCBzbGVlcCgxMDAwKTtcblxuXHRcdFx0cmV0dXJuIHRoaXMuc2lnbigpO1xuXHRcdH1cblx0fVxuXG5cdHByb3RlY3RlZCBhc3luYyBvbkJyb2FkY2FzdEVycm9yKGVycm9yKTogUHJvbWlzZTxzdHJpbmc+IHtcblx0XHRjb25zb2xlLmVycm9yKGVycm9yKTtcblxuXHRcdGNvbnN0IGlzRHVwbGljYXRlVHhuID0gaXNEdXBsaWNhdGVUeG5FcnJvcihlcnJvcik7XG5cblx0XHQvLyBpZiBkdXBsaWNhdGUgZXJyb3IsIHRoZW4gc3RvcCBicm9hZGNhc3RpbmcgcHJvY2Vzc1xuXHRcdC8vIHJldHVybiBvcmlnaW5hbGx5IGJyb2FkY2FzdGVkIHR4bklkXG5cdFx0aWYgKGlzRHVwbGljYXRlVHhuKSB7XG5cdFx0XHRyZXR1cm4gdGhpcy5icm9hZGNhc3RlZFR4bklkO1xuXHRcdH1cblxuXG5cdFx0aWYgKHRoaXMucmV0cnlPbkVycm9yKSB7XG5cdFx0XHRjb25zdCBtYXhOdW1PZlJldHJpZXMgPSAoXzFIb3Vyc0luU2Vjb25kcyAvIHRoaXMuc2Vjb25kc0JlZm9yZVJldHJ5KSAtIDE7XG5cblx0XHRcdGlmICh0aGlzLm51bU9mUmV0cmllcysrIDwgbWF4TnVtT2ZSZXRyaWVzKSB7XG5cdFx0XHRcdC8vIHdhaXQgYW5kIHRoZW4gcmV0cnlcblx0XHRcdFx0YXdhaXQgc2xlZXAoMTAwMCAqIHRoaXMuc2Vjb25kc0JlZm9yZVJldHJ5KTtcblxuXHRcdFx0XHRyZXR1cm4gdGhpcy5leGVjdXRlKCk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cblx0XHR0aHJvdyBlcnJvcjtcblx0fVxuXG5cblx0YXN5bmMgY29uZmlybSgpOiBQcm9taXNlPHN0cmluZz4ge1xuXHRcdGlmICghdGhpcy5icm9hZGNhc3RlZFR4bklkKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ1RyYW5zYWN0aW9uIGhhcyBub3QgeWV0IGJlZW4gc3VjY2VzZnVsbHkgYnJvYWRjYXN0ZWQhJyk7XG5cdFx0fVxuXG5cdFx0aWYgKCF0aGlzLnR4bkNoZWNrZXIpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignbm8gVHJhbnNhY3Rpb24gQ2hlY2tlciBmdW5jdGlvbiBkZWZpbmVkIScpO1xuXHRcdH1cblxuXG5cdFx0Ly8gd2FpdCA1IG1pbnV0ZXMgYmVmb3JlIGNoZWNraW5nIGlycmV2ZXJzaWJpbGl0eVxuXHRcdGNvbnN0IHRpbWVUb1dhaXQgPSA1ICogNjAgKiAxMDAwO1xuXG5cdFx0Y29uc3QgcmVtYWluaW5nVGltZVRvV2FpdCA9XG5cdFx0XHR0aW1lVG9XYWl0IC1cblx0XHRcdChEYXRlLm5vdygpIC0gdGhpcy5icm9hZGNhc3RlZFRpbWUpO1xuXG5cdFx0Y29uc29sZS5sb2coe3JlbWFpbmluZ1RpbWVUb1dhaXR9KTtcblxuXHRcdGlmIChyZW1haW5pbmdUaW1lVG9XYWl0ID4gMCkge1xuXHRcdFx0YXdhaXQgc2xlZXAocmVtYWluaW5nVGltZVRvV2FpdCk7XG5cdFx0fVxuXG5cblx0XHRjb25zdCB0eG5JZCA9IHRoaXMuYnJvYWRjYXN0ZWRUeG5JZDtcblxuXHRcdC8vIGNvbmZpcm0gaXJyZXZlcnNpYmlsaXR5XG5cdFx0dHJ5IHtcblx0XHRcdGNvbnN0IGlzQ29uZmlybWVkID0gYXdhaXQgdGhpcy50eG5DaGVja2VyKHR4bklkKTtcblxuXHRcdFx0aWYgKCFpc0NvbmZpcm1lZCkge1xuXHRcdFx0XHRkZWJ1Z01lc3NhZ2UodGhpcy5hY3Rpb25JZCArICcgVHhuOiAnICsgdHhuSWQgKyAnIGlzIE5PVCBDT05GSVJNRUQhJyk7XG5cblx0XHRcdFx0cmV0dXJuIHRoaXMuZXhlY3V0ZSgpO1xuXHRcdFx0fVxuXG5cblx0XHRcdGRlYnVnTWVzc2FnZSh0aGlzLmFjdGlvbklkICsgJyBUeG46ICcgKyB0eG5JZCArICcgaXMgSVJSRVZFUlNJQkxFIScpO1xuXG5cdFx0XHRyZXR1cm4gdHhuSWQ7XG5cdFx0fSBjYXRjaCAoZXJyb3IpIHtcblx0XHRcdGRlYnVnTWVzc2FnZSgnQ291bmQgTm90IENvbmZpcm0gVHhuOiAnICsgdHhuSWQpO1xuXHRcdFx0Y29uc29sZS5lcnJvcihlcnJvcik7XG5cblx0XHRcdC8vIHdhaXRcblx0XHRcdGF3YWl0IHNsZWVwKDUgKiAxMDAwKTtcblxuXHRcdFx0cmV0dXJuIHRoaXMuZXhlY3V0ZSgpO1xuXHRcdH1cblx0fVxufVxuXG5cbmV4cG9ydCBjbGFzcyBFb3NUcmFuc2FjdGlvbkV4ZWN1dG9yPFQgZXh0ZW5kcyBJRW9zQWNjb3VudE5hbWVzQmFzZT4gZXh0ZW5kcyBFb3NUcmFuc2FjdGlvbkV4ZWN1dG9yQmFzZSB7XG5cdGNvbnN0cnVjdG9yKFxuXHRcdGFjdGlvbnM6IElFb3NBY3Rpb25EYXRhW10sXG5cdFx0ZW9zVXRpbGl0eTogSUVvc1V0aWxpdHk8VD4sXG5cdFx0dHhuQ2hlY2tlcjogVHhuQ29uZmlybWVkQ2hlY2tlcixcblx0XHRyZXRyeU9uRXJyb3IgPSB0cnVlLFxuXHRcdHNlY29uZHNCZWZvcmVSZXRyeSA9IDEwXG5cdCkge1xuXHRcdHN1cGVyKFxuXHRcdFx0YWN0aW9ucy5tYXAoXG5cdFx0XHRcdChhY3Rpb24pID0+ICh7XG5cdFx0XHRcdFx0Li4uYWN0aW9uLFxuXHRcdFx0XHRcdGF1dGhvcml6YXRpb246IGVvc1V0aWxpdHkuYXV0aG9yaXphdGlvblxuXHRcdFx0XHR9KVxuXHRcdFx0KSxcblx0XHRcdGVvc1V0aWxpdHkudHJhbnNhY3Rvcixcblx0XHRcdHR4bkNoZWNrZXIsXG5cdFx0XHRyZXRyeU9uRXJyb3IsXG5cdFx0XHRzZWNvbmRzQmVmb3JlUmV0cnlcblx0XHQpO1xuXHR9XG59XG5cblxuXG4vKlxuRXJyb3I6IHtcImNvZGVcIjo0MDksXCJtZXNzYWdlXCI6XCJDb25mbGljdFwiLFwiZXJyb3JcIjp7XCJjb2RlXCI6MzA0MDAwOCxcIm5hbWVcIjpcInR4X2R1cGxpY2F0ZVwiLFwid2hhdFwiOlwiRHVwbGljYXRlIHRyYW5zYWN0aW9uXCIsXCJkZXRhaWxzXCI6W3tcIm1lc3NhZ2VcIjpcImR1cGxpY2F0ZSB0cmFuc2FjdGlvbiA3YmUyODQ1NjE4ZmE5NjVkODlmZjhmM2IzNjhlZDFjNzY4ZGFiNjMzMDNjMzhjN2Q4YTVlOWYwMzFkN2EyM2M2XCIsXCJmaWxlXCI6XCJwcm9kdWNlcl9wbHVnaW4uY3BwXCIsXCJsaW5lX251bWJlclwiOjUyNyxcIm1ldGhvZFwiOlwicHJvY2Vzc19pbmNvbWluZ190cmFuc2FjdGlvbl9hc3luY1wifV19fVxuKi9cbmZ1bmN0aW9uIGlzRHVwbGljYXRlVHhuRXJyb3IoZXJyb3IpOiBib29sZWFuIHtcblx0Y29uc3Qgc2VhcmNoU3RyaW5ncyA9IFtcblx0XHQndHhfZHVwbGljYXRlJyxcblx0XHQnRHVwbGljYXRlIHRyYW5zYWN0aW9uJyxcblx0XHQnZHVwbGljYXRlIHRyYW5zYWN0aW9uJ1xuXHRdO1xuXG5cdGNvbnN0IGVycm9yU3RyaW5nID0gU3RyaW5nKGVycm9yKTtcblxuXHRmb3IgKGNvbnN0IHNlYXJjaFN0cmluZyBvZiBzZWFyY2hTdHJpbmdzKSB7XG5cdFx0aWYgKGVycm9yU3RyaW5nLmluZGV4T2Yoc2VhcmNoU3RyaW5nKSA+IC0xKSB7XG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHR9XG5cdH1cblxuXHRyZXR1cm4gZmFsc2U7XG59XG5cblxuXG5pbnRlcmZhY2UgSVNpZ25lZFRyYW5zYWN0aW9uIHtcblx0Y29tcHJlc3Npb246IHN0cmluZztcblx0dHJhbnNhY3Rpb246IHtcblx0XHQvKlxuICAgICAgICBleHBpcmF0aW9uOiAnMjAyMC0wNy0xM1QyMTo0OTozMScsXG4gICAgIHJlZl9ibG9ja19udW06IDM0NDI2LFxuICAgICByZWZfYmxvY2tfcHJlZml4OiAzNDczMjUzODE1LFxuICAgICBtYXhfbmV0X3VzYWdlX3dvcmRzOiAwLFxuICAgICBtYXhfY3B1X3VzYWdlX21zOiAwLFxuICAgICBkZWxheV9zZWM6IDAsXG4gICAgIGNvbnRleHRfZnJlZV9hY3Rpb25zOiBbXSxcbiAgICAgYWN0aW9uczogWyBbT2JqZWN0XSBdLFxuICAgICB0cmFuc2FjdGlvbl9leHRlbnNpb25zOiBbXVxuICAgICAgICAqL1xuXHR9O1xuXG5cdHNpZ25hdHVyZXM6IHN0cmluZ1tdO1xufVxuXG5pbnRlcmZhY2UgSVNpZ25lZFRyYW5zYWN0aW9uUmVzdWx0IHtcblx0dHJhbnNhY3Rpb25faWQ6IHN0cmluZztcblx0dHJhbnNhY3Rpb246IElTaWduZWRUcmFuc2FjdGlvbjtcbn1cblxuaW50ZXJmYWNlIElCcm9hZGNhc3RlZFRyYW5zYWN0aW9uUmVzdWx0IHtcblx0dHJhbnNhY3Rpb25faWQ6IHN0cmluZztcbn1cblxuXG5cbmFzeW5jIGZ1bmN0aW9uIHNpZ25UcmFuc2FjdGlvbihcblx0dHJhbnNhY3Rvcixcblx0YWN0aW9uczogSUVvc1RyYW5zYWN0aW9uQWN0aW9uW10sXG5cdGV4cGlyZUluU2Vjb25kczogbnVtYmVyXG4pOiBQcm9taXNlPElTaWduZWRUcmFuc2FjdGlvblJlc3VsdD4ge1xuXHRjb25zdCByZXN1bHQ6IElTaWduZWRUcmFuc2FjdGlvblJlc3VsdCA9IGF3YWl0IHRyYW5zYWN0b3IudHJhbnNhY3Rpb24oXG5cdFx0e1xuXHRcdFx0YWN0aW9uc1xuXHRcdH0sXG5cdFx0e1xuXHRcdFx0YnJvYWRjYXN0OiBmYWxzZSxcblx0XHRcdHNpZ246IHRydWUsXG5cdFx0XHRleHBpcmVJblNlY29uZHNcblx0XHR9XG5cdCk7XG5cblx0cmV0dXJuIHJlc3VsdDtcbn1cblxuYXN5bmMgZnVuY3Rpb24gYnJvYWRjYXN0U2lnbmVkVHJhbnNhY3Rpb24oXG5cdHRyYW5zYWN0b3IsXG5cdHRyYW5zYWN0aW9uOiBJU2lnbmVkVHJhbnNhY3Rpb25cbik6IFByb21pc2U8c3RyaW5nPiB7XG5cdGNvbnN0IHJlc3VsdDogSUJyb2FkY2FzdGVkVHJhbnNhY3Rpb25SZXN1bHQgPSBhd2FpdCB0cmFuc2FjdG9yLnB1c2hUcmFuc2FjdGlvbih0cmFuc2FjdGlvbik7XG5cblx0cmV0dXJuIHJlc3VsdC50cmFuc2FjdGlvbl9pZDtcbn1cbiJdfQ==