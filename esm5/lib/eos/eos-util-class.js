/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-util-class.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { getUniqueTableRow, getAllTableRows, getTableRows, getTokenBalance } from 'earnbet-common';
import { getEosJs } from './eos-util';
/**
 * @template T
 */
var /**
 * @template T
 */
EosUtility = /** @class */ (function () {
    function EosUtility(params) {
        var config = params.config;
        /** @type {?} */
        var endpoints = config.endpoints;
        this.poller = getEosJs(config, endpoints.poller);
        this.transactor = getEosJs(config, endpoints.transactor);
        this.chainId = params.chainId;
        this.config = config;
        this.contracts = params.contracts;
        this.authorization = params.authorization;
    }
    /**
     * @return {?}
     */
    EosUtility.prototype.getInfo = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/, this.poller.getInfo({})];
            });
        });
    };
    /**
     * @template T
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} rowId
     * @return {?}
     */
    EosUtility.prototype.getUniqueTableRow = /**
     * @template T
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} rowId
     * @return {?}
     */
    function (code, scope, table, rowId) {
        return getUniqueTableRow(this.poller, code, scope, table, rowId);
    };
    /**
     * @template T
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} key
     * @return {?}
     */
    EosUtility.prototype.getAllTableRows = /**
     * @template T
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} key
     * @return {?}
     */
    function (code, scope, table, key) {
        return getAllTableRows(this.poller, code, scope, table, key);
    };
    /**
     * @template T
     * @param {?} parameters
     * @return {?}
     */
    EosUtility.prototype.getTableRows = /**
     * @template T
     * @param {?} parameters
     * @return {?}
     */
    function (parameters) {
        return getTableRows(this.poller, parameters);
    };
    /**
     * @param {?} contract
     * @param {?} account
     * @param {?} symbol
     * @param {?=} table
     * @return {?}
     */
    EosUtility.prototype.getTokenBalance = /**
     * @param {?} contract
     * @param {?} account
     * @param {?} symbol
     * @param {?=} table
     * @return {?}
     */
    function (contract, account, symbol, table) {
        if (table === void 0) { table = 'accounts'; }
        return getTokenBalance(this.poller, contract, account, symbol, table);
    };
    /**
     * @param {?} actions
     * @return {?}
     */
    EosUtility.prototype.executeTransaction = /**
     * @param {?} actions
     * @return {?}
     */
    function (actions) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var result, error_1;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.transactor.transaction({
                                actions: actions.map((/**
                                 * @param {?} action
                                 * @return {?}
                                 */
                                function (action) { return (tslib_1.__assign({}, action, { authorization: _this.authorization })); }))
                            })];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, result.transaction_id];
                    case 2:
                        error_1 = _a.sent();
                        throw error_1;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} eosAccountName
     * @return {?}
     */
    EosUtility.prototype.isAccountSafe = /**
     * @param {?} eosAccountName
     * @return {?}
     */
    function (eosAccountName) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var result, error_2;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.isEasyAccountContract(eosAccountName)) {
                            return [2 /*return*/, true];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.poller.getCodeHash(eosAccountName)];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result.code_hash == '0000000000000000000000000000000000000000000000000000000000000000'];
                    case 3:
                        error_2 = _a.sent();
                        return [2 /*return*/, false];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} name
     * @return {?}
     */
    EosUtility.prototype.isEasyAccountContract = /**
     * @param {?} name
     * @return {?}
     */
    function (name) {
        return this.contracts.easyAccount != undefined &&
            name == this.contracts.easyAccount;
    };
    return EosUtility;
}());
/**
 * @template T
 */
export { EosUtility };
if (false) {
    /** @type {?} */
    EosUtility.prototype.poller;
    /** @type {?} */
    EosUtility.prototype.transactor;
    /** @type {?} */
    EosUtility.prototype.chainId;
    /** @type {?} */
    EosUtility.prototype.config;
    /** @type {?} */
    EosUtility.prototype.contracts;
    /** @type {?} */
    EosUtility.prototype.authorization;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLXV0aWwtY2xhc3MuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9lb3MvZW9zLXV0aWwtY2xhc3MudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUF3QixpQkFBaUIsRUFBRSxlQUFlLEVBQUUsWUFBWSxFQUE0QyxlQUFlLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUluSyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sWUFBWSxDQUFDOzs7O0FBR3RDOzs7O0lBU0Msb0JBQVksTUFBNEI7UUFDaEMsSUFBQSxzQkFBTTs7WUFFUCxTQUFTLEdBQUcsTUFBTSxDQUFDLFNBQVM7UUFFbEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRXpELElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUM5QixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUM7UUFDbEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsYUFBYSxDQUFDO0lBQzNDLENBQUM7Ozs7SUFFSyw0QkFBTzs7O0lBQWI7OztnQkFDQyxzQkFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsRUFBQzs7O0tBQy9COzs7Ozs7Ozs7SUFFRCxzQ0FBaUI7Ozs7Ozs7O0lBQWpCLFVBQXFCLElBQVksRUFBRSxLQUFhLEVBQUUsS0FBYSxFQUFFLEtBQWE7UUFDN0UsT0FBTyxpQkFBaUIsQ0FDdkIsSUFBSSxDQUFDLE1BQU0sRUFDWCxJQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQ3pCLENBQUM7SUFDSCxDQUFDOzs7Ozs7Ozs7SUFFRCxvQ0FBZTs7Ozs7Ozs7SUFBZixVQUFtQixJQUFZLEVBQUUsS0FBYSxFQUFFLEtBQWEsRUFBRSxHQUFXO1FBQ3pFLE9BQU8sZUFBZSxDQUNyQixJQUFJLENBQUMsTUFBTSxFQUNYLElBQUksRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FDdkIsQ0FBQztJQUNILENBQUM7Ozs7OztJQUVELGlDQUFZOzs7OztJQUFaLFVBQWdCLFVBQW9DO1FBQ25ELE9BQU8sWUFBWSxDQUNsQixJQUFJLENBQUMsTUFBTSxFQUNYLFVBQVUsQ0FDVixDQUFDO0lBQ0gsQ0FBQzs7Ozs7Ozs7SUFFRCxvQ0FBZTs7Ozs7OztJQUFmLFVBQ0MsUUFBZ0IsRUFBRSxPQUFlLEVBQUUsTUFBYyxFQUNqRCxLQUFrQjtRQUFsQixzQkFBQSxFQUFBLGtCQUFrQjtRQUVsQixPQUFPLGVBQWUsQ0FDckIsSUFBSSxDQUFDLE1BQU0sRUFDWCxRQUFRLEVBQ1IsT0FBTyxFQUNQLE1BQU0sRUFDTixLQUFLLENBQ0wsQ0FBQztJQUNILENBQUM7Ozs7O0lBRUssdUNBQWtCOzs7O0lBQXhCLFVBQ0MsT0FBeUI7Ozs7Ozs7O3dCQUdULHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDO2dDQUNoRCxPQUFPLEVBQUUsT0FBTyxDQUFDLEdBQUc7Ozs7Z0NBQ25CLFVBQUMsTUFBTSxJQUFLLE9BQUEsc0JBQ1IsTUFBTSxJQUNULGFBQWEsRUFBRSxLQUFJLENBQUMsYUFBYSxJQUNoQyxFQUhVLENBR1YsRUFDRjs2QkFDRCxDQUFDLEVBQUE7O3dCQVBJLE1BQU0sR0FBRyxTQU9iO3dCQUVGLHNCQUFPLE1BQU0sQ0FBQyxjQUFjLEVBQUM7Ozt3QkFFN0IsTUFBTSxPQUFLLENBQUM7Ozs7O0tBRWI7Ozs7O0lBR0ssa0NBQWE7Ozs7SUFBbkIsVUFBb0IsY0FBc0I7Ozs7Ozt3QkFDekMsSUFBSyxJQUFJLENBQUMscUJBQXFCLENBQUMsY0FBYyxDQUFDLEVBQUc7NEJBQ2pELHNCQUFPLElBQUksRUFBQzt5QkFDWjs7Ozt3QkFJZSxxQkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsRUFBQTs7d0JBQXRELE1BQU0sR0FBRyxTQUE2Qzt3QkFFNUQsc0JBQU8sTUFBTSxDQUFDLFNBQVMsSUFBSSxrRUFBa0UsRUFBQzs7O3dCQUU5RixzQkFBTyxLQUFLLEVBQUM7Ozs7O0tBRWQ7Ozs7O0lBRUQsMENBQXFCOzs7O0lBQXJCLFVBQXNCLElBQVk7UUFDakMsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsSUFBSSxTQUFTO1lBQzVDLElBQUksSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQztJQUN0QyxDQUFDO0lBQ0YsaUJBQUM7QUFBRCxDQUFDLEFBcEdELElBb0dDOzs7Ozs7O0lBbkdBLDRCQUFnQjs7SUFDaEIsZ0NBQW9COztJQUVwQiw2QkFBeUI7O0lBQ3pCLDRCQUE0Qjs7SUFDNUIsK0JBQXNCOztJQUN0QixtQ0FBK0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJQWN0aW9uQXV0aG9yaXphdGlvbiwgZ2V0VW5pcXVlVGFibGVSb3csIGdldEFsbFRhYmxlUm93cywgZ2V0VGFibGVSb3dzLCBJVGFibGVSb3dRdWVyeVBhcmFtZXRlcnMsIElFb3NBY3Rpb25EYXRhLCBnZXRUb2tlbkJhbGFuY2UgfSBmcm9tICdlYXJuYmV0LWNvbW1vbic7XG5cbmltcG9ydCB7IElFb3NVdGlsaXR5LCBJRW9zVXRpbGl0eVBhcmFtcywgSUVPU0luZm8gfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuaW1wb3J0IHsgSUVvc0NvbmZpZywgSUVvc0FjY291bnROYW1lc0Jhc2UgfSBmcm9tICcuL2Vvcy1zZXR0aW5ncyc7XG5pbXBvcnQgeyBnZXRFb3NKcyB9IGZyb20gJy4vZW9zLXV0aWwnO1xuXG5cbmV4cG9ydCBjbGFzcyBFb3NVdGlsaXR5PFQgZXh0ZW5kcyBJRW9zQWNjb3VudE5hbWVzQmFzZT4gaW1wbGVtZW50cyBJRW9zVXRpbGl0eTxUPiB7XG5cdHJlYWRvbmx5IHBvbGxlcjtcblx0cmVhZG9ubHkgdHJhbnNhY3RvcjtcblxuXHRyZWFkb25seSBjaGFpbklkOiBudW1iZXI7XG5cdHJlYWRvbmx5IGNvbmZpZzogSUVvc0NvbmZpZztcblx0cmVhZG9ubHkgY29udHJhY3RzOiBUO1xuXHRyZWFkb25seSBhdXRob3JpemF0aW9uOiBJQWN0aW9uQXV0aG9yaXphdGlvbltdO1xuXG5cdGNvbnN0cnVjdG9yKHBhcmFtczogSUVvc1V0aWxpdHlQYXJhbXM8VD4pIHtcblx0XHRjb25zdCB7Y29uZmlnfSA9IHBhcmFtcztcblxuXHRcdGNvbnN0IGVuZHBvaW50cyA9IGNvbmZpZy5lbmRwb2ludHM7XG5cblx0XHR0aGlzLnBvbGxlciA9IGdldEVvc0pzKGNvbmZpZywgZW5kcG9pbnRzLnBvbGxlcik7XG5cdFx0dGhpcy50cmFuc2FjdG9yID0gZ2V0RW9zSnMoY29uZmlnLCBlbmRwb2ludHMudHJhbnNhY3Rvcik7XG5cblx0XHR0aGlzLmNoYWluSWQgPSBwYXJhbXMuY2hhaW5JZDtcblx0XHR0aGlzLmNvbmZpZyA9IGNvbmZpZztcblx0XHR0aGlzLmNvbnRyYWN0cyA9IHBhcmFtcy5jb250cmFjdHM7XG5cdFx0dGhpcy5hdXRob3JpemF0aW9uID0gcGFyYW1zLmF1dGhvcml6YXRpb247XG5cdH1cblxuXHRhc3luYyBnZXRJbmZvKCk6IFByb21pc2U8SUVPU0luZm8+IHtcblx0XHRyZXR1cm4gdGhpcy5wb2xsZXIuZ2V0SW5mbyh7fSk7XG5cdH1cblxuXHRnZXRVbmlxdWVUYWJsZVJvdzxUPihjb2RlOiBzdHJpbmcsIHNjb3BlOiBzdHJpbmcsIHRhYmxlOiBzdHJpbmcsIHJvd0lkOiBzdHJpbmcpIHtcblx0XHRyZXR1cm4gZ2V0VW5pcXVlVGFibGVSb3c8VD4oXG5cdFx0XHR0aGlzLnBvbGxlcixcblx0XHRcdGNvZGUsIHNjb3BlLCB0YWJsZSwgcm93SWRcblx0XHQpO1xuXHR9XG5cblx0Z2V0QWxsVGFibGVSb3dzPFQ+KGNvZGU6IHN0cmluZywgc2NvcGU6IHN0cmluZywgdGFibGU6IHN0cmluZywga2V5OiBzdHJpbmcpIHtcblx0XHRyZXR1cm4gZ2V0QWxsVGFibGVSb3dzPFQ+KFxuXHRcdFx0dGhpcy5wb2xsZXIsXG5cdFx0XHRjb2RlLCBzY29wZSwgdGFibGUsIGtleVxuXHRcdCk7XG5cdH1cblxuXHRnZXRUYWJsZVJvd3M8VD4ocGFyYW1ldGVyczogSVRhYmxlUm93UXVlcnlQYXJhbWV0ZXJzKSB7XG5cdFx0cmV0dXJuIGdldFRhYmxlUm93czxUPihcblx0XHRcdHRoaXMucG9sbGVyLFxuXHRcdFx0cGFyYW1ldGVyc1xuXHRcdCk7XG5cdH1cblxuXHRnZXRUb2tlbkJhbGFuY2UoXG5cdFx0Y29udHJhY3Q6IHN0cmluZywgYWNjb3VudDogc3RyaW5nLCBzeW1ib2w6IHN0cmluZyxcblx0XHR0YWJsZSA9ICdhY2NvdW50cydcblx0KSB7XG5cdFx0cmV0dXJuIGdldFRva2VuQmFsYW5jZShcblx0XHRcdHRoaXMucG9sbGVyLFxuXHRcdFx0Y29udHJhY3QsXG5cdFx0XHRhY2NvdW50LFxuXHRcdFx0c3ltYm9sLFxuXHRcdFx0dGFibGVcblx0XHQpO1xuXHR9XG5cblx0YXN5bmMgZXhlY3V0ZVRyYW5zYWN0aW9uKFxuXHRcdGFjdGlvbnM6IElFb3NBY3Rpb25EYXRhW11cblx0KTogUHJvbWlzZTxzdHJpbmc+IHtcblx0XHR0cnkge1xuXHRcdFx0Y29uc3QgcmVzdWx0ID0gYXdhaXQgdGhpcy50cmFuc2FjdG9yLnRyYW5zYWN0aW9uKHtcblx0XHRcdFx0YWN0aW9uczogYWN0aW9ucy5tYXAoXG5cdFx0XHRcdFx0KGFjdGlvbikgPT4gKHtcblx0XHRcdFx0XHRcdC4uLmFjdGlvbixcblx0XHRcdFx0XHRcdGF1dGhvcml6YXRpb246IHRoaXMuYXV0aG9yaXphdGlvblxuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdClcblx0XHRcdH0pO1xuXG5cdFx0XHRyZXR1cm4gcmVzdWx0LnRyYW5zYWN0aW9uX2lkO1xuXHRcdH0gY2F0Y2ggKGVycm9yKSB7XG5cdFx0XHR0aHJvdyBlcnJvcjtcblx0XHR9XG5cdH1cblxuXG5cdGFzeW5jIGlzQWNjb3VudFNhZmUoZW9zQWNjb3VudE5hbWU6IHN0cmluZyk6IFByb21pc2U8Ym9vbGVhbj4ge1xuXHRcdGlmICggdGhpcy5pc0Vhc3lBY2NvdW50Q29udHJhY3QoZW9zQWNjb3VudE5hbWUpICkge1xuXHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0fVxuXG5cblx0XHR0cnkge1xuXHRcdFx0Y29uc3QgcmVzdWx0ID0gYXdhaXQgdGhpcy5wb2xsZXIuZ2V0Q29kZUhhc2goZW9zQWNjb3VudE5hbWUpO1xuXG5cdFx0XHRyZXR1cm4gcmVzdWx0LmNvZGVfaGFzaCA9PSAnMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMCc7XG5cdFx0fSBjYXRjaCAoZXJyb3IpIHtcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9XG5cdH1cblxuXHRpc0Vhc3lBY2NvdW50Q29udHJhY3QobmFtZTogc3RyaW5nKTogYm9vbGVhbiB7XG5cdFx0cmV0dXJuIHRoaXMuY29udHJhY3RzLmVhc3lBY2NvdW50ICE9IHVuZGVmaW5lZCAmJlxuXHRcdFx0XHRuYW1lID09IHRoaXMuY29udHJhY3RzLmVhc3lBY2NvdW50O1xuXHR9XG59XG4iXX0=