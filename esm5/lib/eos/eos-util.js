/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import Eos from 'eosjs-revised';
import eos_ecc from 'eosjs-ecc';
/**
 * @param {?} eosConfig
 * @param {?=} endpoint
 * @return {?}
 */
export function getEosJs(eosConfig, endpoint) {
    if (endpoint === void 0) { endpoint = undefined; }
    /** @type {?} */
    var cfg = Object.assign({}, eosConfig.eosjs);
    if (endpoint != undefined) {
        cfg.httpEndpoint = endpoint;
    }
    return Eos(cfg);
}
/**
 * @param {?} signedData
 * @param {?} info
 * @return {?}
 */
export function recoverPublicKey(signedData, info) {
    /** @type {?} */
    var recoveredPublicKey;
    try {
        if (!info.isSignedDataHashed) {
            recoveredPublicKey = eos_ecc.recover(info.signature, signedData);
        }
        else {
            /** @type {?} */
            var signedHash = eos_ecc.sha256(eos_ecc.sha256(signedData) +
                eos_ecc.sha256(info.nonce));
            recoveredPublicKey = eos_ecc.recoverHash(info.signature, signedHash);
        }
    }
    catch (error) {
        console.error(error);
    }
    return recoveredPublicKey;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLXV0aWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1iYWNrLWVuZC8iLCJzb3VyY2VzIjpbImxpYi9lb3MvZW9zLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEdBQUcsTUFBTSxlQUFlLENBQUM7QUFDaEMsT0FBTyxPQUFPLE1BQU0sV0FBVyxDQUFDOzs7Ozs7QUFTaEMsTUFBTSxVQUFVLFFBQVEsQ0FBQyxTQUFxQixFQUFFLFFBQTJCO0lBQTNCLHlCQUFBLEVBQUEsb0JBQTJCOztRQUNwRSxHQUFHLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQztJQUU5QyxJQUFJLFFBQVEsSUFBSSxTQUFTLEVBQUU7UUFDMUIsR0FBRyxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUM7S0FDNUI7SUFFRCxPQUFPLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNqQixDQUFDOzs7Ozs7QUFHRCxNQUFNLFVBQVUsZ0JBQWdCLENBQUMsVUFBa0IsRUFBRSxJQUFvQjs7UUFDcEUsa0JBQTBCO0lBRTlCLElBQUk7UUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzdCLGtCQUFrQixHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQ25DLElBQUksQ0FBQyxTQUFTLEVBQ2QsVUFBVSxDQUNWLENBQUM7U0FDRjthQUFNOztnQkFDQSxVQUFVLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FDaEMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUM7Z0JBQzFCLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUMxQjtZQUVELGtCQUFrQixHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsQ0FBQztTQUNyRTtLQUNEO0lBQUMsT0FBTyxLQUFLLEVBQUU7UUFDZixPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ3JCO0lBR0QsT0FBTyxrQkFBa0IsQ0FBQztBQUMzQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEVvcyBmcm9tICdlb3Nqcy1yZXZpc2VkJztcbmltcG9ydCBlb3NfZWNjIGZyb20gJ2Vvc2pzLWVjYyc7XG5cblxuaW1wb3J0IHsgSVNpZ25hdHVyZUluZm8gfSBmcm9tICdlYXJuYmV0LWNvbW1vbic7XG5cbmltcG9ydCB7IElFb3NDb25maWcgfSBmcm9tICcuL2Vvcy1zZXR0aW5ncyc7XG5cblxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0RW9zSnMoZW9zQ29uZmlnOiBJRW9zQ29uZmlnLCBlbmRwb2ludDogc3RyaW5nPSB1bmRlZmluZWQpIHtcblx0Y29uc3QgY2ZnID0gT2JqZWN0LmFzc2lnbih7fSwgZW9zQ29uZmlnLmVvc2pzKTtcblxuXHRpZiAoZW5kcG9pbnQgIT0gdW5kZWZpbmVkKSB7XG5cdFx0Y2ZnLmh0dHBFbmRwb2ludCA9IGVuZHBvaW50O1xuXHR9XG5cblx0cmV0dXJuIEVvcyhjZmcpO1xufVxuXG5cbmV4cG9ydCBmdW5jdGlvbiByZWNvdmVyUHVibGljS2V5KHNpZ25lZERhdGE6IHN0cmluZywgaW5mbzogSVNpZ25hdHVyZUluZm8pOiBzdHJpbmcge1xuXHRsZXQgcmVjb3ZlcmVkUHVibGljS2V5OiBzdHJpbmc7XG5cblx0dHJ5IHtcblx0XHRpZiAoIWluZm8uaXNTaWduZWREYXRhSGFzaGVkKSB7XG5cdFx0XHRyZWNvdmVyZWRQdWJsaWNLZXkgPSBlb3NfZWNjLnJlY292ZXIoXG5cdFx0XHRcdGluZm8uc2lnbmF0dXJlLFxuXHRcdFx0XHRzaWduZWREYXRhXG5cdFx0XHQpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRjb25zdCBzaWduZWRIYXNoID0gZW9zX2VjYy5zaGEyNTYoXG5cdFx0XHRcdGVvc19lY2Muc2hhMjU2KHNpZ25lZERhdGEpICtcblx0XHRcdFx0ZW9zX2VjYy5zaGEyNTYoaW5mby5ub25jZSlcblx0XHRcdCk7XG5cblx0XHRcdHJlY292ZXJlZFB1YmxpY0tleSA9IGVvc19lY2MucmVjb3Zlckhhc2goaW5mby5zaWduYXR1cmUsIHNpZ25lZEhhc2gpO1xuXHRcdH1cblx0fSBjYXRjaCAoZXJyb3IpIHtcblx0XHRjb25zb2xlLmVycm9yKGVycm9yKTtcblx0fVxuXG5cblx0cmV0dXJuIHJlY292ZXJlZFB1YmxpY0tleTtcbn1cbiJdfQ==