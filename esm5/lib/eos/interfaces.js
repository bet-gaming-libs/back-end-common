/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IEOSInfo() { }
if (false) {
    /** @type {?} */
    IEOSInfo.prototype.server_version;
    /** @type {?} */
    IEOSInfo.prototype.chain_id;
    /** @type {?} */
    IEOSInfo.prototype.head_block_num;
    /** @type {?} */
    IEOSInfo.prototype.last_irreversible_block_num;
    /** @type {?} */
    IEOSInfo.prototype.last_irreversible_block_id;
    /** @type {?} */
    IEOSInfo.prototype.head_block_id;
    /** @type {?} */
    IEOSInfo.prototype.head_block_time;
    /** @type {?} */
    IEOSInfo.prototype.head_block_producer;
    /** @type {?} */
    IEOSInfo.prototype.virtual_block_cpu_limit;
    /** @type {?} */
    IEOSInfo.prototype.virtual_block_net_limit;
    /** @type {?} */
    IEOSInfo.prototype.block_cpu_limit;
    /** @type {?} */
    IEOSInfo.prototype.block_net_limit;
    /** @type {?} */
    IEOSInfo.prototype.server_version_string;
}
/**
 * @record
 * @template T
 */
export function IEosUtilityParams() { }
if (false) {
    /** @type {?} */
    IEosUtilityParams.prototype.chainId;
    /** @type {?} */
    IEosUtilityParams.prototype.config;
    /** @type {?} */
    IEosUtilityParams.prototype.contracts;
    /** @type {?} */
    IEosUtilityParams.prototype.authorization;
}
/**
 * @record
 * @template T
 */
export function IEosUtility() { }
if (false) {
    /** @type {?} */
    IEosUtility.prototype.chainId;
    /** @type {?} */
    IEosUtility.prototype.config;
    /** @type {?} */
    IEosUtility.prototype.contracts;
    /** @type {?} */
    IEosUtility.prototype.authorization;
    /** @type {?} */
    IEosUtility.prototype.transactor;
    /**
     * @return {?}
     */
    IEosUtility.prototype.getInfo = function () { };
    /**
     * @param {?} contract
     * @param {?} account
     * @param {?} symbol
     * @param {?=} table
     * @return {?}
     */
    IEosUtility.prototype.getTokenBalance = function (contract, account, symbol, table) { };
    /**
     * @template T
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} rowId
     * @return {?}
     */
    IEosUtility.prototype.getUniqueTableRow = function (code, scope, table, rowId) { };
    /**
     * @template T
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} key
     * @return {?}
     */
    IEosUtility.prototype.getAllTableRows = function (code, scope, table, key) { };
    /**
     * @template T
     * @param {?} parameters
     * @return {?}
     */
    IEosUtility.prototype.getTableRows = function (parameters) { };
    /**
     * @param {?} actions
     * @return {?}
     */
    IEosUtility.prototype.executeTransaction = function (actions) { };
    /**
     * @param {?} eosAccountName
     * @return {?}
     */
    IEosUtility.prototype.isAccountSafe = function (eosAccountName) { };
    /**
     * @param {?} name
     * @return {?}
     */
    IEosUtility.prototype.isEasyAccountContract = function (name) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWJhY2stZW5kLyIsInNvdXJjZXMiOlsibGliL2Vvcy9pbnRlcmZhY2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBS0EsOEJBY0M7OztJQWJBLGtDQUF1Qjs7SUFDdkIsNEJBQWlCOztJQUNqQixrQ0FBdUI7O0lBQ3ZCLCtDQUFvQzs7SUFDcEMsOENBQW1DOztJQUNuQyxpQ0FBc0I7O0lBQ3RCLG1DQUF3Qjs7SUFDeEIsdUNBQTRCOztJQUM1QiwyQ0FBZ0M7O0lBQ2hDLDJDQUFnQzs7SUFDaEMsbUNBQXdCOztJQUN4QixtQ0FBd0I7O0lBQ3hCLHlDQUE4Qjs7Ozs7O0FBRy9CLHVDQUtDOzs7SUFKQSxvQ0FBeUI7O0lBQ3pCLG1DQUE0Qjs7SUFDNUIsc0NBQXNCOztJQUN0QiwwQ0FBK0M7Ozs7OztBQUloRCxpQ0F3QkM7OztJQXZCQSw4QkFBeUI7O0lBQ3pCLDZCQUE0Qjs7SUFDNUIsZ0NBQXNCOztJQUN0QixvQ0FBK0M7O0lBRS9DLGlDQUF5Qjs7OztJQUV6QixnREFBNkI7Ozs7Ozs7O0lBQzdCLHdGQUdtQjs7Ozs7Ozs7O0lBRW5CLG1GQUE4Rjs7Ozs7Ozs7O0lBQzlGLCtFQUEwRjs7Ozs7O0lBQzFGLCtEQUFvRTs7Ozs7SUFFcEUsa0VBRW1COzs7OztJQUVuQixvRUFBd0Q7Ozs7O0lBQ3hELGtFQUE2QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElUYWJsZVJvd1F1ZXJ5UGFyYW1ldGVycywgSUVvc0FjdGlvbkRhdGEsIElBY3Rpb25BdXRob3JpemF0aW9uIH0gZnJvbSAnZWFybmJldC1jb21tb24nO1xuXG5pbXBvcnQge0lFb3NDb25maWcsIElFb3NBY2NvdW50TmFtZXNCYXNlfSBmcm9tICcuL2Vvcy1zZXR0aW5ncyc7XG5cblxuZXhwb3J0IGludGVyZmFjZSBJRU9TSW5mbyB7XG5cdHNlcnZlcl92ZXJzaW9uOiBzdHJpbmc7XG5cdGNoYWluX2lkOiBzdHJpbmc7XG5cdGhlYWRfYmxvY2tfbnVtOiBudW1iZXI7XG5cdGxhc3RfaXJyZXZlcnNpYmxlX2Jsb2NrX251bTogbnVtYmVyO1xuXHRsYXN0X2lycmV2ZXJzaWJsZV9ibG9ja19pZDogc3RyaW5nO1xuXHRoZWFkX2Jsb2NrX2lkOiBzdHJpbmc7XG5cdGhlYWRfYmxvY2tfdGltZTogc3RyaW5nO1xuXHRoZWFkX2Jsb2NrX3Byb2R1Y2VyOiBzdHJpbmc7XG5cdHZpcnR1YWxfYmxvY2tfY3B1X2xpbWl0OiBudW1iZXI7XG5cdHZpcnR1YWxfYmxvY2tfbmV0X2xpbWl0OiBudW1iZXI7XG5cdGJsb2NrX2NwdV9saW1pdDogbnVtYmVyO1xuXHRibG9ja19uZXRfbGltaXQ6IG51bWJlcjtcblx0c2VydmVyX3ZlcnNpb25fc3RyaW5nOiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUVvc1V0aWxpdHlQYXJhbXM8VCBleHRlbmRzIElFb3NBY2NvdW50TmFtZXNCYXNlPiB7XG5cdHJlYWRvbmx5IGNoYWluSWQ6IG51bWJlcjtcblx0cmVhZG9ubHkgY29uZmlnOiBJRW9zQ29uZmlnO1xuXHRyZWFkb25seSBjb250cmFjdHM6IFQ7XG5cdHJlYWRvbmx5IGF1dGhvcml6YXRpb246IElBY3Rpb25BdXRob3JpemF0aW9uW107XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJRW9zVXRpbGl0eTxUIGV4dGVuZHMgSUVvc0FjY291bnROYW1lc0Jhc2U+IHtcblx0cmVhZG9ubHkgY2hhaW5JZDogbnVtYmVyO1xuXHRyZWFkb25seSBjb25maWc6IElFb3NDb25maWc7XG5cdHJlYWRvbmx5IGNvbnRyYWN0czogVDtcblx0cmVhZG9ubHkgYXV0aG9yaXphdGlvbjogSUFjdGlvbkF1dGhvcml6YXRpb25bXTtcblxuXHRyZWFkb25seSB0cmFuc2FjdG9yOiBhbnk7XG5cblx0Z2V0SW5mbygpOiBQcm9taXNlPElFT1NJbmZvPjtcblx0Z2V0VG9rZW5CYWxhbmNlKFxuXHRcdGNvbnRyYWN0OiBzdHJpbmcsIGFjY291bnQ6IHN0cmluZywgc3ltYm9sOiBzdHJpbmcsXG5cdFx0dGFibGU/OiBzdHJpbmdcblx0KTogUHJvbWlzZTxzdHJpbmc+O1xuXG5cdGdldFVuaXF1ZVRhYmxlUm93PFQ+KGNvZGU6IHN0cmluZywgc2NvcGU6IHN0cmluZywgdGFibGU6IHN0cmluZywgcm93SWQ6IHN0cmluZyk6IFByb21pc2U8VFtdPjtcblx0Z2V0QWxsVGFibGVSb3dzPFQ+KGNvZGU6IHN0cmluZywgc2NvcGU6IHN0cmluZywgdGFibGU6IHN0cmluZywga2V5OiBzdHJpbmcpOiBQcm9taXNlPFRbXT47XG5cdGdldFRhYmxlUm93czxUPihwYXJhbWV0ZXJzOiBJVGFibGVSb3dRdWVyeVBhcmFtZXRlcnMpOiBQcm9taXNlPFRbXT47XG5cblx0ZXhlY3V0ZVRyYW5zYWN0aW9uKFxuXHRcdGFjdGlvbnM6IElFb3NBY3Rpb25EYXRhW11cblx0KTogUHJvbWlzZTxzdHJpbmc+O1xuXG5cdGlzQWNjb3VudFNhZmUoZW9zQWNjb3VudE5hbWU6IHN0cmluZyk6IFByb21pc2U8Ym9vbGVhbj47XG5cdGlzRWFzeUFjY291bnRDb250cmFjdChuYW1lOiBzdHJpbmcpOiBib29sZWFuO1xufVxuIl19