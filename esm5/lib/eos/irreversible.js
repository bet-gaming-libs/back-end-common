/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/irreversible.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { MySQLDatabase } from '../database/engine/mysql/mysql-database';
/** @type {?} */
var waxDatabase;
/** @type {?} */
var eosDatabase;
/**
 * @param {?} txnId
 * @return {?}
 * @this {*}
 */
export function isWaxTxnIrreversible(txnId) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var waxDatabase;
        return tslib_1.__generator(this, function (_a) {
            waxDatabase = getWaxDatabase();
            return [2 /*return*/, isTxnIrreversible(waxDatabase, txnId)];
        });
    });
}
/**
 * @param {?} txnId
 * @return {?}
 * @this {*}
 */
export function isEosTxnIrreversible(txnId) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var eosDatabase;
        return tslib_1.__generator(this, function (_a) {
            eosDatabase = getEosDatabase();
            return [2 /*return*/, isTxnIrreversible(eosDatabase, txnId)];
        });
    });
}
/**
 * @return {?}
 */
function getWaxDatabase() {
    if (waxDatabase == undefined) {
        if (process.env.WAX_IRREV_DB_HOSTNAME == undefined ||
            process.env.WAX_IRREV_DB_PORT == undefined ||
            process.env.WAX_IRREV_DB_USERNAME == undefined ||
            process.env.WAX_IRREV_DB_PASSWORD == undefined ||
            process.env.WAX_IRREV_DB_NAME == undefined) {
            throw new Error('must define ENV VARS for WAX_IRREV_DB!');
        }
        waxDatabase = new MySQLDatabase({
            host: process.env.WAX_IRREV_DB_HOSTNAME,
            port: Number(process.env.WAX_IRREV_DB_PORT),
            user: process.env.WAX_IRREV_DB_USERNAME,
            password: process.env.WAX_IRREV_DB_PASSWORD,
            database: process.env.WAX_IRREV_DB_NAME,
            charset: 'utf8',
        });
    }
    return waxDatabase;
}
/**
 * @return {?}
 */
function getEosDatabase() {
    if (eosDatabase == undefined) {
        if (process.env.EOS_IRREV_DB_HOSTNAME == undefined ||
            process.env.EOS_IRREV_DB_PORT == undefined ||
            process.env.EOS_IRREV_DB_USERNAME == undefined ||
            process.env.EOS_IRREV_DB_PASSWORD == undefined ||
            process.env.EOS_IRREV_DB_NAME == undefined) {
            throw new Error('must define ENV VARS for EOS_IRREV_DB!');
        }
        eosDatabase = new MySQLDatabase({
            host: process.env.EOS_IRREV_DB_HOSTNAME,
            port: Number(process.env.EOS_IRREV_DB_PORT),
            user: process.env.EOS_IRREV_DB_USERNAME,
            password: process.env.EOS_IRREV_DB_PASSWORD,
            database: process.env.EOS_IRREV_DB_NAME,
            charset: 'utf8',
        });
    }
    return eosDatabase;
}
/**
 * @param {?} database
 * @param {?} txnId
 * @return {?}
 * @this {*}
 */
function isTxnIrreversible(database, txnId) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var rows;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, database.builder.rawQuery("select block_num from IRREVTX where trx_id = '" + txnId.toLowerCase() + "' LIMIT 1").execute()];
                case 1:
                    rows = _a.sent();
                    console.log(rows);
                    return [2 /*return*/, rows.length > 0];
            }
        });
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXJyZXZlcnNpYmxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvZW9zL2lycmV2ZXJzaWJsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0seUNBQXlDLENBQUM7O0lBR3BFLFdBQTBCOztJQUUxQixXQUEwQjs7Ozs7O0FBRzlCLE1BQU0sVUFBZ0Isb0JBQW9CLENBQUMsS0FBYTs7OztZQUNqRCxXQUFXLEdBQUcsY0FBYyxFQUFFO1lBRXBDLHNCQUFPLGlCQUFpQixDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsRUFBQzs7O0NBQzdDOzs7Ozs7QUFFRCxNQUFNLFVBQWdCLG9CQUFvQixDQUFDLEtBQWE7Ozs7WUFDakQsV0FBVyxHQUFHLGNBQWMsRUFBRTtZQUVwQyxzQkFBTyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLEVBQUM7OztDQUM3Qzs7OztBQUdELFNBQVMsY0FBYztJQUN0QixJQUFJLFdBQVcsSUFBSSxTQUFTLEVBQUU7UUFDN0IsSUFDQyxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixJQUFJLFNBQVM7WUFDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsSUFBSSxTQUFTO1lBQzFDLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLElBQUksU0FBUztZQUM5QyxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixJQUFJLFNBQVM7WUFDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsSUFBSSxTQUFTLEVBQ3pDO1lBQ0QsTUFBTSxJQUFJLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO1NBQzFEO1FBRUQsV0FBVyxHQUFHLElBQUksYUFBYSxDQUFDO1lBQy9CLElBQUksRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQjtZQUN2QyxJQUFJLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUM7WUFDM0MsSUFBSSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCO1lBQ3ZDLFFBQVEsRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQjtZQUMzQyxRQUFRLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUI7WUFDdkMsT0FBTyxFQUFFLE1BQU07U0FDZixDQUFDLENBQUM7S0FDSDtJQUVELE9BQU8sV0FBVyxDQUFDO0FBQ3BCLENBQUM7Ozs7QUFFRCxTQUFTLGNBQWM7SUFDdEIsSUFBSSxXQUFXLElBQUksU0FBUyxFQUFFO1FBQzdCLElBQ0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsSUFBSSxTQUFTO1lBQzlDLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLElBQUksU0FBUztZQUMxQyxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixJQUFJLFNBQVM7WUFDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsSUFBSSxTQUFTO1lBQzlDLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLElBQUksU0FBUyxFQUN6QztZQUNELE1BQU0sSUFBSSxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FBQztTQUMxRDtRQUVELFdBQVcsR0FBRyxJQUFJLGFBQWEsQ0FBQztZQUMvQixJQUFJLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUI7WUFDdkMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDO1lBQzNDLElBQUksRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQjtZQUN2QyxRQUFRLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUI7WUFDM0MsUUFBUSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCO1lBQ3ZDLE9BQU8sRUFBRSxNQUFNO1NBQ2YsQ0FBQyxDQUFDO0tBQ0g7SUFFRCxPQUFPLFdBQVcsQ0FBQztBQUVwQixDQUFDOzs7Ozs7O0FBRUQsU0FBZSxpQkFBaUIsQ0FBQyxRQUF1QixFQUFFLEtBQWE7Ozs7O3dCQUN6RCxxQkFBTSxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FDM0MsbURBQWlELEtBQUssQ0FBQyxXQUFXLEVBQUUsY0FBVyxDQUMvRSxDQUFDLE9BQU8sRUFBRSxFQUFBOztvQkFGTCxJQUFJLEdBQUcsU0FFRjtvQkFFWCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUVsQixzQkFBTyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBQzs7OztDQUN2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE15U1FMRGF0YWJhc2UgfSBmcm9tICcuLi9kYXRhYmFzZS9lbmdpbmUvbXlzcWwvbXlzcWwtZGF0YWJhc2UnO1xuXG5cbmxldCB3YXhEYXRhYmFzZTogTXlTUUxEYXRhYmFzZTtcblxubGV0IGVvc0RhdGFiYXNlOiBNeVNRTERhdGFiYXNlO1xuXG5cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBpc1dheFR4bklycmV2ZXJzaWJsZSh0eG5JZDogc3RyaW5nKTogUHJvbWlzZTxib29sZWFuPiB7XG5cdGNvbnN0IHdheERhdGFiYXNlID0gZ2V0V2F4RGF0YWJhc2UoKTtcblxuXHRyZXR1cm4gaXNUeG5JcnJldmVyc2libGUod2F4RGF0YWJhc2UsIHR4bklkKTtcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGlzRW9zVHhuSXJyZXZlcnNpYmxlKHR4bklkOiBzdHJpbmcpOiBQcm9taXNlPGJvb2xlYW4+IHtcblx0Y29uc3QgZW9zRGF0YWJhc2UgPSBnZXRFb3NEYXRhYmFzZSgpO1xuXG5cdHJldHVybiBpc1R4bklycmV2ZXJzaWJsZShlb3NEYXRhYmFzZSwgdHhuSWQpO1xufVxuXG5cbmZ1bmN0aW9uIGdldFdheERhdGFiYXNlKCkge1xuXHRpZiAod2F4RGF0YWJhc2UgPT0gdW5kZWZpbmVkKSB7XG5cdFx0aWYgKFxuXHRcdFx0cHJvY2Vzcy5lbnYuV0FYX0lSUkVWX0RCX0hPU1ROQU1FID09IHVuZGVmaW5lZCB8fFxuXHRcdFx0cHJvY2Vzcy5lbnYuV0FYX0lSUkVWX0RCX1BPUlQgPT0gdW5kZWZpbmVkIHx8XG5cdFx0XHRwcm9jZXNzLmVudi5XQVhfSVJSRVZfREJfVVNFUk5BTUUgPT0gdW5kZWZpbmVkIHx8XG5cdFx0XHRwcm9jZXNzLmVudi5XQVhfSVJSRVZfREJfUEFTU1dPUkQgPT0gdW5kZWZpbmVkIHx8XG5cdFx0XHRwcm9jZXNzLmVudi5XQVhfSVJSRVZfREJfTkFNRSA9PSB1bmRlZmluZWRcblx0XHQpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignbXVzdCBkZWZpbmUgRU5WIFZBUlMgZm9yIFdBWF9JUlJFVl9EQiEnKTtcblx0XHR9XG5cblx0XHR3YXhEYXRhYmFzZSA9IG5ldyBNeVNRTERhdGFiYXNlKHtcblx0XHRcdGhvc3Q6IHByb2Nlc3MuZW52LldBWF9JUlJFVl9EQl9IT1NUTkFNRSxcblx0XHRcdHBvcnQ6IE51bWJlcihwcm9jZXNzLmVudi5XQVhfSVJSRVZfREJfUE9SVCksXG5cdFx0XHR1c2VyOiBwcm9jZXNzLmVudi5XQVhfSVJSRVZfREJfVVNFUk5BTUUsXG5cdFx0XHRwYXNzd29yZDogcHJvY2Vzcy5lbnYuV0FYX0lSUkVWX0RCX1BBU1NXT1JELFxuXHRcdFx0ZGF0YWJhc2U6IHByb2Nlc3MuZW52LldBWF9JUlJFVl9EQl9OQU1FLFxuXHRcdFx0Y2hhcnNldDogJ3V0ZjgnLFxuXHRcdH0pO1xuXHR9XG5cblx0cmV0dXJuIHdheERhdGFiYXNlO1xufVxuXG5mdW5jdGlvbiBnZXRFb3NEYXRhYmFzZSgpIHtcblx0aWYgKGVvc0RhdGFiYXNlID09IHVuZGVmaW5lZCkge1xuXHRcdGlmIChcblx0XHRcdHByb2Nlc3MuZW52LkVPU19JUlJFVl9EQl9IT1NUTkFNRSA9PSB1bmRlZmluZWQgfHxcblx0XHRcdHByb2Nlc3MuZW52LkVPU19JUlJFVl9EQl9QT1JUID09IHVuZGVmaW5lZCB8fFxuXHRcdFx0cHJvY2Vzcy5lbnYuRU9TX0lSUkVWX0RCX1VTRVJOQU1FID09IHVuZGVmaW5lZCB8fFxuXHRcdFx0cHJvY2Vzcy5lbnYuRU9TX0lSUkVWX0RCX1BBU1NXT1JEID09IHVuZGVmaW5lZCB8fFxuXHRcdFx0cHJvY2Vzcy5lbnYuRU9TX0lSUkVWX0RCX05BTUUgPT0gdW5kZWZpbmVkXG5cdFx0KSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ211c3QgZGVmaW5lIEVOViBWQVJTIGZvciBFT1NfSVJSRVZfREIhJyk7XG5cdFx0fVxuXG5cdFx0ZW9zRGF0YWJhc2UgPSBuZXcgTXlTUUxEYXRhYmFzZSh7XG5cdFx0XHRob3N0OiBwcm9jZXNzLmVudi5FT1NfSVJSRVZfREJfSE9TVE5BTUUsXG5cdFx0XHRwb3J0OiBOdW1iZXIocHJvY2Vzcy5lbnYuRU9TX0lSUkVWX0RCX1BPUlQpLFxuXHRcdFx0dXNlcjogcHJvY2Vzcy5lbnYuRU9TX0lSUkVWX0RCX1VTRVJOQU1FLFxuXHRcdFx0cGFzc3dvcmQ6IHByb2Nlc3MuZW52LkVPU19JUlJFVl9EQl9QQVNTV09SRCxcblx0XHRcdGRhdGFiYXNlOiBwcm9jZXNzLmVudi5FT1NfSVJSRVZfREJfTkFNRSxcblx0XHRcdGNoYXJzZXQ6ICd1dGY4Jyxcblx0XHR9KTtcblx0fVxuXG5cdHJldHVybiBlb3NEYXRhYmFzZTtcblxufVxuXG5hc3luYyBmdW5jdGlvbiBpc1R4bklycmV2ZXJzaWJsZShkYXRhYmFzZTogTXlTUUxEYXRhYmFzZSwgdHhuSWQ6IHN0cmluZyk6IFByb21pc2U8Ym9vbGVhbj4ge1xuXHRjb25zdCByb3dzID0gYXdhaXQgZGF0YWJhc2UuYnVpbGRlci5yYXdRdWVyeTx7fVtdPihcblx0XHRgc2VsZWN0IGJsb2NrX251bSBmcm9tIElSUkVWVFggd2hlcmUgdHJ4X2lkID0gJyR7dHhuSWQudG9Mb3dlckNhc2UoKX0nIExJTUlUIDFgXG5cdCkuZXhlY3V0ZSgpO1xuXG5cdGNvbnNvbGUubG9nKHJvd3MpO1xuXG5cdHJldHVybiByb3dzLmxlbmd0aCA+IDA7XG59XG5cblxuLypcbmltcG9ydCB7IGh0dHBHZXQgfSBmcm9tICdlYXJuYmV0LWNvbW1vbi1iYWNrLWVuZCc7XG5cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBpc1dheFR4bklycmV2ZXJzaWJsZSh0eG5JZDpzdHJpbmcpOlByb21pc2U8Ym9vbGVhbj4ge1xuICAgIGxldCByZXN1bHQgPSBhd2FpdCBodHRwR2V0KFxuICAgICAgICAnd2F4YmV0LXVzLWhpc3RkYi54ZW9zLm1lL2Vhcm5iZXQvaXJyZXZ0eC8nK3R4bklkXG4gICAgKTtcblxuICAgIGNvbnNvbGUubG9nKCdpc1dheFR4bklycmV2ZXJzaWJsZSBSRVNVTFQ6JyxyZXN1bHQpO1xuXG4gICAgcmV0dXJuIE51bWJlcihyZXN1bHQpID09PSAxO1xufVxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gaXNFb3NUeG5JcnJldmVyc2libGUodHhuSWQ6c3RyaW5nKTpQcm9taXNlPGJvb2xlYW4+IHtcbiAgICBsZXQgcmVzdWx0ID0gYXdhaXQgaHR0cEdldChcbiAgICAgICAgJ2Vvc2JldC11cy1oaXN0ZGIueGVvcy5tZS9lb3NiZXQvaXJyZXZ0eC8nK3R4bklkXG4gICAgKTtcblxuICAgIGNvbnNvbGUubG9nKCdpc0Vvc1R4bklycmV2ZXJzaWJsZSBSRVNVTFQ6JyxyZXN1bHQpO1xuXG4gICAgcmV0dXJuIE51bWJlcihyZXN1bHQpID09PSAxO1xufVxuKi9cbiJdfQ==