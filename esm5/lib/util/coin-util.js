/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/coin-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { httpGetJson } from './http-util';
var CoinPriceService = /** @class */ (function () {
    function CoinPriceService() {
        var _this = this;
        this.symbols = [];
        this.prices = {};
        this.updateCoinPrices = (/**
         * @return {?}
         */
        function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var _a, _b, symbol, price, e_1_1;
            var e_1, _c;
            return tslib_1.__generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        _d.trys.push([0, 5, 6, 7]);
                        _a = tslib_1.__values(this.symbols), _b = _a.next();
                        _d.label = 1;
                    case 1:
                        if (!!_b.done) return [3 /*break*/, 4];
                        symbol = _b.value;
                        symbol = symbol.split('_')[0];
                        return [4 /*yield*/, fetchPriceOfCoin(symbol)];
                    case 2:
                        price = _d.sent();
                        if (price != undefined) {
                            this.prices[symbol] = price;
                        }
                        _d.label = 3;
                    case 3:
                        _b = _a.next();
                        return [3 /*break*/, 1];
                    case 4: return [3 /*break*/, 7];
                    case 5:
                        e_1_1 = _d.sent();
                        e_1 = { error: e_1_1 };
                        return [3 /*break*/, 7];
                    case 6:
                        try {
                            if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                        }
                        finally { if (e_1) throw e_1.error; }
                        return [7 /*endfinally*/];
                    case 7: return [2 /*return*/];
                }
            });
        }); });
    }
    /**
     * @param {?} database
     * @return {?}
     */
    CoinPriceService.prototype.init = /**
     * @param {?} database
     * @return {?}
     */
    function (database) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var coins;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, database.getAllCoins()];
                    case 1:
                        coins = _a.sent();
                        this.symbols = coins.map((/**
                         * @param {?} row
                         * @return {?}
                         */
                        function (row) { return row.symbol; }));
                        setInterval(this.updateCoinPrices, 1000 * 60 * 60);
                        return [4 /*yield*/, this.updateCoinPrices()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} symbol
     * @return {?}
     */
    CoinPriceService.prototype.getPriceInUSD = /**
     * @param {?} symbol
     * @return {?}
     */
    function (symbol) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var price;
            return tslib_1.__generator(this, function (_a) {
                symbol = symbol.split('_')[0];
                if (symbol == 'FUN') {
                    return [2 /*return*/, 0];
                }
                if (symbol == 'USD') {
                    return [2 /*return*/, 1];
                }
                price = this.prices[symbol.toUpperCase()];
                if (price == undefined) {
                    console.log(this.prices);
                    throw new Error(symbol + ' Price is UNDEFINED!');
                }
                return [2 /*return*/, price];
            });
        });
    };
    return CoinPriceService;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    CoinPriceService.prototype.symbols;
    /**
     * @type {?}
     * @private
     */
    CoinPriceService.prototype.prices;
    /**
     * @type {?}
     * @private
     */
    CoinPriceService.prototype.updateCoinPrices;
}
/** @type {?} */
var coinPriceService;
/**
 * @param {?} database
 * @return {?}
 * @this {*}
 */
export function getCoinPriceService(database) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!(coinPriceService == undefined)) return [3 /*break*/, 2];
                    coinPriceService = new CoinPriceService();
                    return [4 /*yield*/, coinPriceService.init(database)];
                case 1:
                    _a.sent();
                    _a.label = 2;
                case 2: return [2 /*return*/, coinPriceService];
            }
        });
    });
}
/**
 * @record
 */
function ICoinPriceData() { }
if (false) {
    /** @type {?} */
    ICoinPriceData.prototype.price;
}
/**
 * @param {?} symbol
 * @return {?}
 * @this {*}
 */
function fetchPriceOfCoin(symbol) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var url, data, error_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    url = 'internal.eosbet.io/token_price/' +
                        symbol.toUpperCase();
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, httpGetJson(url)];
                case 2:
                    data = _a.sent();
                    // console.log(data);
                    return [2 /*return*/, Number(data.price)];
                case 3:
                    error_1 = _a.sent();
                    console.error(error_1);
                    return [2 /*return*/, undefined];
                case 4: return [2 /*return*/];
            }
        });
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29pbi11dGlsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tYmFjay1lbmQvIiwic291cmNlcyI6WyJsaWIvdXRpbC9jb2luLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUkxQztJQUlDO1FBQUEsaUJBRUM7UUFMTyxZQUFPLEdBQWEsRUFBRSxDQUFDO1FBQ3ZCLFdBQU0sR0FBNkIsRUFBRSxDQUFDO1FBbUJ0QyxxQkFBZ0I7OztRQUFHOzs7Ozs7O3dCQUNQLEtBQUEsaUJBQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQTs7Ozt3QkFBdEIsTUFBTTt3QkFDZCxNQUFNLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFFaEIscUJBQU0sZ0JBQWdCLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUF0QyxLQUFLLEdBQUcsU0FBOEI7d0JBRTVDLElBQUksS0FBSyxJQUFJLFNBQVMsRUFBRTs0QkFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLENBQUM7eUJBQzVCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O2FBRUYsRUFBQTtJQXpCRCxDQUFDOzs7OztJQUVLLCtCQUFJOzs7O0lBQVYsVUFBVyxRQUE4Qjs7Ozs7NEJBQzFCLHFCQUFNLFFBQVEsQ0FBQyxXQUFXLEVBQUUsRUFBQTs7d0JBQXBDLEtBQUssR0FBRyxTQUE0Qjt3QkFFMUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsR0FBRzs7Ozt3QkFDdkIsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLENBQUMsTUFBTSxFQUFWLENBQVUsRUFDakIsQ0FBQzt3QkFHRixXQUFXLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLElBQUksR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBRW5ELHFCQUFNLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxFQUFBOzt3QkFBN0IsU0FBNkIsQ0FBQzs7Ozs7S0FDOUI7Ozs7O0lBY0ssd0NBQWE7Ozs7SUFBbkIsVUFBb0IsTUFBYzs7OztnQkFDakMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRzlCLElBQUksTUFBTSxJQUFJLEtBQUssRUFBRTtvQkFDcEIsc0JBQU8sQ0FBQyxFQUFDO2lCQUNUO2dCQUVELElBQUksTUFBTSxJQUFJLEtBQUssRUFBRTtvQkFDcEIsc0JBQU8sQ0FBQyxFQUFDO2lCQUNUO2dCQUdLLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFFLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBRTtnQkFFakQsSUFBSSxLQUFLLElBQUksU0FBUyxFQUFFO29CQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFFekIsTUFBTSxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsc0JBQXNCLENBQUMsQ0FBQztpQkFDakQ7Z0JBRUQsc0JBQU8sS0FBSyxFQUFDOzs7S0FDYjtJQUNGLHVCQUFDO0FBQUQsQ0FBQyxBQXhERCxJQXdEQzs7Ozs7O0lBdkRBLG1DQUErQjs7Ozs7SUFDL0Isa0NBQThDOzs7OztJQW1COUMsNENBVUM7OztJQTRCRSxnQkFBa0M7Ozs7OztBQUV0QyxNQUFNLFVBQWdCLG1CQUFtQixDQUFDLFFBQThCOzs7Ozt5QkFDbkUsQ0FBQSxnQkFBZ0IsSUFBSSxTQUFTLENBQUEsRUFBN0Isd0JBQTZCO29CQUNoQyxnQkFBZ0IsR0FBRyxJQUFJLGdCQUFnQixFQUFFLENBQUM7b0JBRTFDLHFCQUFNLGdCQUFnQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBQTs7b0JBQXJDLFNBQXFDLENBQUM7O3dCQUd2QyxzQkFBTyxnQkFBZ0IsRUFBQzs7OztDQUN4Qjs7OztBQUdELDZCQUVDOzs7SUFEQSwrQkFBYzs7Ozs7OztBQUdmLFNBQWUsZ0JBQWdCLENBQUMsTUFBYzs7Ozs7O29CQUN2QyxHQUFHLEdBQ1IsaUNBQWlDO3dCQUNqQyxNQUFNLENBQUMsV0FBVyxFQUFFOzs7O29CQUlQLHFCQUFNLFdBQVcsQ0FBaUIsR0FBRyxDQUFDLEVBQUE7O29CQUE3QyxJQUFJLEdBQUcsU0FBc0M7b0JBQ25ELHFCQUFxQjtvQkFFckIsc0JBQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBQzs7O29CQUUxQixPQUFPLENBQUMsS0FBSyxDQUFDLE9BQUssQ0FBQyxDQUFDO29CQUVyQixzQkFBTyxTQUFTLEVBQUM7Ozs7O0NBRWxCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgaHR0cEdldEpzb24gfSBmcm9tICcuL2h0dHAtdXRpbCc7XG5pbXBvcnQgeyBJQ3VycmVuY3lQcmljZVNlcnZpY2UsIElDb2luRGF0YWJhc2VTZXJ2aWNlIH0gZnJvbSAnLi4vY3VycmVuY3kvY3VycmVuY3ktYW1vdW50L2ludGVyZmFjZXMnO1xuXG5cbmNsYXNzIENvaW5QcmljZVNlcnZpY2UgaW1wbGVtZW50cyBJQ3VycmVuY3lQcmljZVNlcnZpY2Uge1xuXHRwcml2YXRlIHN5bWJvbHM6IHN0cmluZ1tdID0gW107XG5cdHByaXZhdGUgcHJpY2VzOiB7W2NvaW46IHN0cmluZ106IG51bWJlcn0gPSB7fTtcblxuXHRjb25zdHJ1Y3RvcigpIHtcblxuXHR9XG5cblx0YXN5bmMgaW5pdChkYXRhYmFzZTogSUNvaW5EYXRhYmFzZVNlcnZpY2UpIHtcblx0XHRjb25zdCBjb2lucyA9IGF3YWl0IGRhdGFiYXNlLmdldEFsbENvaW5zKCk7XG5cblx0XHR0aGlzLnN5bWJvbHMgPSBjb2lucy5tYXAoXG5cdFx0XHRyb3cgPT4gcm93LnN5bWJvbFxuXHRcdCk7XG5cblxuXHRcdHNldEludGVydmFsKHRoaXMudXBkYXRlQ29pblByaWNlcywgMTAwMCAqIDYwICogNjApO1xuXG5cdFx0YXdhaXQgdGhpcy51cGRhdGVDb2luUHJpY2VzKCk7XG5cdH1cblxuXHRwcml2YXRlIHVwZGF0ZUNvaW5QcmljZXMgPSBhc3luYygpID0+IHtcblx0XHRmb3IgKGxldCBzeW1ib2wgb2YgdGhpcy5zeW1ib2xzKSB7XG5cdFx0XHRzeW1ib2wgPSBzeW1ib2wuc3BsaXQoJ18nKVswXTtcblxuXHRcdFx0Y29uc3QgcHJpY2UgPSBhd2FpdCBmZXRjaFByaWNlT2ZDb2luKHN5bWJvbCk7XG5cblx0XHRcdGlmIChwcmljZSAhPSB1bmRlZmluZWQpIHtcblx0XHRcdFx0dGhpcy5wcmljZXNbc3ltYm9sXSA9IHByaWNlO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXG5cdGFzeW5jIGdldFByaWNlSW5VU0Qoc3ltYm9sOiBzdHJpbmcpOiBQcm9taXNlPG51bWJlcj4ge1xuXHRcdHN5bWJvbCA9IHN5bWJvbC5zcGxpdCgnXycpWzBdO1xuXG5cblx0XHRpZiAoc3ltYm9sID09ICdGVU4nKSB7XG5cdFx0XHRyZXR1cm4gMDtcblx0XHR9XG5cblx0XHRpZiAoc3ltYm9sID09ICdVU0QnKSB7XG5cdFx0XHRyZXR1cm4gMTtcblx0XHR9XG5cblxuXHRcdGNvbnN0IHByaWNlID0gdGhpcy5wcmljZXNbIHN5bWJvbC50b1VwcGVyQ2FzZSgpIF07XG5cblx0XHRpZiAocHJpY2UgPT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRjb25zb2xlLmxvZyh0aGlzLnByaWNlcyk7XG5cblx0XHRcdHRocm93IG5ldyBFcnJvcihzeW1ib2wgKyAnIFByaWNlIGlzIFVOREVGSU5FRCEnKTtcblx0XHR9XG5cblx0XHRyZXR1cm4gcHJpY2U7XG5cdH1cbn1cblxuXG5sZXQgY29pblByaWNlU2VydmljZTogQ29pblByaWNlU2VydmljZTtcblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldENvaW5QcmljZVNlcnZpY2UoZGF0YWJhc2U6IElDb2luRGF0YWJhc2VTZXJ2aWNlKTogUHJvbWlzZTxJQ3VycmVuY3lQcmljZVNlcnZpY2U+IHtcblx0aWYgKGNvaW5QcmljZVNlcnZpY2UgPT0gdW5kZWZpbmVkKSB7XG5cdFx0Y29pblByaWNlU2VydmljZSA9IG5ldyBDb2luUHJpY2VTZXJ2aWNlKCk7XG5cblx0XHRhd2FpdCBjb2luUHJpY2VTZXJ2aWNlLmluaXQoZGF0YWJhc2UpO1xuXHR9XG5cblx0cmV0dXJuIGNvaW5QcmljZVNlcnZpY2U7XG59XG5cblxuaW50ZXJmYWNlIElDb2luUHJpY2VEYXRhIHtcblx0cHJpY2U6IG51bWJlcjtcbn1cblxuYXN5bmMgZnVuY3Rpb24gZmV0Y2hQcmljZU9mQ29pbihzeW1ib2w6IHN0cmluZyk6IFByb21pc2U8bnVtYmVyPiB7XG5cdGNvbnN0IHVybCA9XG5cdFx0J2ludGVybmFsLmVvc2JldC5pby90b2tlbl9wcmljZS8nICtcblx0XHRzeW1ib2wudG9VcHBlckNhc2UoKSA7XG5cblxuXHR0cnkge1xuXHRcdGNvbnN0IGRhdGEgPSBhd2FpdCBodHRwR2V0SnNvbjxJQ29pblByaWNlRGF0YT4odXJsKTtcblx0XHQvLyBjb25zb2xlLmxvZyhkYXRhKTtcblxuXHRcdHJldHVybiBOdW1iZXIoZGF0YS5wcmljZSk7XG5cdH0gY2F0Y2ggKGVycm9yKSB7XG5cdFx0Y29uc29sZS5lcnJvcihlcnJvcik7XG5cblx0XHRyZXR1cm4gdW5kZWZpbmVkO1xuXHR9XG59XG4iXX0=