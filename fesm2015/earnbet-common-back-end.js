import { __awaiter } from 'tslib';
import { createConnection } from 'mysql';
import { logWithTime, sleep, debugMessage, getUniqueTableRow, getAllTableRows, getTableRows, getTokenBalance } from 'earnbet-common';
import { encode } from 'bs58';
import { mnemonicToSeed, generateMnemonic } from 'bip39';
import { fromSeed } from 'bip32';
import { networks } from 'bitcoinjs-lib';
import Eos from 'eosjs-revised';
import eos_ecc from 'eosjs-ecc';
import { Big } from 'big.js';
import { get } from 'https';
import { get as get$1 } from 'http';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IMySQLConfig() { }
if (false) {
    /** @type {?} */
    IMySQLConfig.prototype.host;
    /** @type {?|undefined} */
    IMySQLConfig.prototype.port;
    /** @type {?} */
    IMySQLConfig.prototype.user;
    /** @type {?} */
    IMySQLConfig.prototype.password;
    /** @type {?} */
    IMySQLConfig.prototype.database;
    /** @type {?} */
    IMySQLConfig.prototype.charset;
}
/**
 * @record
 */
function IMySQLRepository() { }
if (false) {
    /** @type {?} */
    IMySQLRepository.prototype.tableName;
    /** @type {?} */
    IMySQLRepository.prototype.primaryKey;
    /** @type {?} */
    IMySQLRepository.prototype.isPrimaryKeyAString;
}
/**
 * @record
 */
function IMySQLQueryResult() { }
if (false) {
    /** @type {?} */
    IMySQLQueryResult.prototype.fieldCount;
    /** @type {?} */
    IMySQLQueryResult.prototype.affectedRows;
    /** @type {?} */
    IMySQLQueryResult.prototype.insertId;
    /** @type {?} */
    IMySQLQueryResult.prototype.serverStatus;
    /** @type {?} */
    IMySQLQueryResult.prototype.warningCount;
    /** @type {?} */
    IMySQLQueryResult.prototype.message;
    /** @type {?} */
    IMySQLQueryResult.prototype.changedRows;
}
/**
 * @record
 */
function IMySQLDatabase() { }
if (false) {
    /** @type {?} */
    IMySQLDatabase.prototype.builder;
    /** @type {?} */
    IMySQLDatabase.prototype.transactionManager;
}
/**
 * @record
 * @template T
 */
function IQueryParams() { }
if (false) {
    /** @type {?|undefined} */
    IQueryParams.prototype.whereMap;
    /** @type {?|undefined} */
    IQueryParams.prototype.whereAndMap;
    /** @type {?|undefined} */
    IQueryParams.prototype.whereOrMap;
    /** @type {?|undefined} */
    IQueryParams.prototype.whereClause;
    /** @type {?|undefined} */
    IQueryParams.prototype.wherePrimaryKeys;
    /** @type {?|undefined} */
    IQueryParams.prototype.whereGroup;
    /** @type {?|undefined} */
    IQueryParams.prototype.orderBy;
    /** @type {?|undefined} */
    IQueryParams.prototype.limit;
}
/**
 * @record
 * @template T
 */
function ISelectQueryParams() { }
if (false) {
    /** @type {?|undefined} */
    ISelectQueryParams.prototype.fields;
}
/**
 * @record
 * @template T
 */
function IQueryBuilderParams() { }
if (false) {
    /** @type {?} */
    IQueryBuilderParams.prototype.repository;
}
/**
 * @record
 * @template T
 */
function ISelectQueryBuilderParams() { }
if (false) {
    /** @type {?} */
    ISelectQueryBuilderParams.prototype.fields;
}
/**
 * @record
 * @template T
 */
function IUpdateQueryParams() { }
if (false) {
    /** @type {?|undefined} */
    IUpdateQueryParams.prototype.singleStatement;
    /** @type {?|undefined} */
    IUpdateQueryParams.prototype.statements;
}
/**
 * @record
 * @template T
 */
function IUpdateQueryBuilderParams() { }
/**
 * @record
 */
function IQueryData() { }
if (false) {
    /** @type {?} */
    IQueryData.prototype.sql;
    /** @type {?} */
    IQueryData.prototype.argumentsToPrepare;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-connection-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const logSqlQueries = process.env.LOG_SQL_QUERIES === 'true';
class MySQLConnectionManager {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.config = config;
        this.isConnecting = false;
        this.connection = undefined;
        this.connect();
    }
    /**
     * @return {?}
     */
    getConnection() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.reconnect();
            return this.connection;
        });
    }
    /**
     * @private
     * @return {?}
     */
    reconnect() {
        return __awaiter(this, void 0, void 0, function* () {
            while (!this.isConnected) {
                logWithTime('NOT CONNECTED:', this.connection.threadId);
                this.printConnectionState();
                /** @type {?} */
                const isConnected = yield this.connect();
                if (isConnected) {
                    return;
                }
                yield sleep(1000);
            }
        });
    }
    /**
     * @private
     * @return {?}
     */
    connect() {
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        (resolve) => {
            if (this.isConnected) {
                resolve(true);
                return;
            }
            if (this.isConnecting) {
                resolve(false);
                return;
            }
            this.isConnecting = true;
            // end existing connection
            if (this.connection) {
                this.connection.end();
            }
            // connection parameters
            this.connection = createConnection(this.config);
            // logWithTime('MySQLConnection State:',this.connection.state);
            // establish connection
            this.connection.connect((/**
             * @param {?} err
             * @return {?}
             */
            (err) => {
                logWithTime(this.config);
                this.isConnecting = false;
                if (err) {
                    console.error('error connecting: ' + err.stack);
                    resolve(false);
                }
                else {
                    logWithTime('connected as id ' + this.connection.threadId);
                    logWithTime('MySQLConnection State:', this.connection.state);
                    // set query format
                    this.connection.config.queryFormat = (/**
                     * @param {?} query
                     * @param {?} values
                     * @return {?}
                     */
                    function (query, values) {
                        if (!values) {
                            return query;
                        }
                        /** @type {?} */
                        const escapedQuery = query.replace(/\:(\w+)/g, (/**
                         * @param {?} txt
                         * @param {?} key
                         * @return {?}
                         */
                        function (txt, key) {
                            if (values.hasOwnProperty(key)) {
                                return this.escape(values[key]);
                            }
                            return txt;
                        }).bind(this));
                        if (logSqlQueries) {
                            console.log({ escapedQuery });
                        }
                        return escapedQuery;
                    });
                    resolve(true);
                }
            }));
            this.connection.on('error', (/**
             * @param {?} err
             * @return {?}
             */
            (err) => {
                logWithTime('******************* MySQL Connection onError EVENT: *******************');
                console.error(err);
                this.printConnectionState();
            }));
        }));
    }
    /**
     * @private
     * @return {?}
     */
    printConnectionState() {
        logWithTime('MySQLConnection State:', this.connection.state);
    }
    /*
        private get isDisconnected():boolean {
            return this.connection == undefined ||
                    this.connection.state === 'disconnected';
        }
        */
    /**
     * @private
     * @return {?}
     */
    get isConnected() {
        return this.connection !== undefined && (this.connection.state === 'connected' ||
            this.connection.state === 'authenticated');
    }
    /**
     * @return {?}
     */
    endConnection() {
        this.connection.end();
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.isConnecting;
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.connection;
    /**
     * @type {?}
     * @private
     */
    MySQLConnectionManager.prototype.config;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-statements.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const MySQLValueType = {
    EXPRESSION: 0,
    STRING: 1,
};
MySQLValueType[MySQLValueType.EXPRESSION] = 'EXPRESSION';
MySQLValueType[MySQLValueType.STRING] = 'STRING';
/** @enum {string} */
const ExpressionOperator = {
    EQUAL: "=",
    NOT_EQUAL: "!=",
    LESS_THAN: "<",
    LESS_THAN_OR_EQUAL: "<=",
    GREATER_THAN: ">",
    GREATER_THAN_OR_EQUAL: ">=",
    IS: "IS",
    IS_NOT: "IS NOT",
};
/** @enum {string} */
const ExpressionGroupJoiner = {
    AND: "AND",
    OR: "OR",
    COMMA: ",",
};
/**
 * @record
 */
function IMySQLValue() { }
if (false) {
    /** @type {?} */
    IMySQLValue.prototype.data;
    /** @type {?} */
    IMySQLValue.prototype.isString;
}
class MySQLValue {
    /**
     * @param {?} data
     * @param {?} type
     */
    constructor(data, type) {
        this.data = data;
        this.type = type;
        this.isString =
            type == MySQLValueType.STRING;
    }
}
if (false) {
    /** @type {?} */
    MySQLValue.prototype.isString;
    /** @type {?} */
    MySQLValue.prototype.data;
    /** @type {?} */
    MySQLValue.prototype.type;
}
class MySQLNumberValue extends MySQLValue {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data, MySQLValueType.EXPRESSION);
    }
}
class MySQLStringValue extends MySQLValue {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data, MySQLValueType.STRING);
    }
}
class MySQLStringExpressionValue extends MySQLValue {
    /**
     * @param {?} data
     */
    constructor(data) {
        super(data, MySQLValueType.EXPRESSION);
    }
}
/** @enum {string} */
const MySQLExpression = {
    NOW: "NOW()",
    NULL: "NULL",
};
/** @type {?} */
const nowValue = new MySQLStringExpressionValue(MySQLExpression.NOW);
/** @type {?} */
const nullValue = new MySQLStringExpressionValue(MySQLExpression.NULL);
/*
class MySQLValue
{
    static booleanAsInt(bool:boolean):number
    {
        return bool ? 1 : 0;
    }
}
*/
/**
 * @abstract
 */
class Expression {
    /**
     * @param {?} leftSide
     * @param {?} operator
     * @param {?} rightSide
     */
    constructor(leftSide, operator, rightSide) {
        this.leftSide = leftSide;
        this.operator = operator;
        this.rightSide = rightSide;
    }
}
if (false) {
    /** @type {?} */
    Expression.prototype.leftSide;
    /** @type {?} */
    Expression.prototype.operator;
    /** @type {?} */
    Expression.prototype.rightSide;
}
class ExpressionGroup {
    /**
     * @param {?} expressions
     * @param {?} joiner
     * @param {?=} surroundExpressionWithBrackets
     */
    constructor(expressions, joiner, surroundExpressionWithBrackets = true) {
        this.expressions = expressions;
        this.joiner = joiner;
        this.surroundExpressionWithBrackets = surroundExpressionWithBrackets;
    }
}
if (false) {
    /** @type {?} */
    ExpressionGroup.prototype.expressions;
    /** @type {?} */
    ExpressionGroup.prototype.joiner;
    /** @type {?} */
    ExpressionGroup.prototype.surroundExpressionWithBrackets;
}
/**
 * @param {?} map
 * @param {?} joiner
 * @return {?}
 */
function createExpressionGroup(map, joiner) {
    /** @type {?} */
    const expressions = [];
    // should we use Object.keys()?
    for (const fieldName in map) {
        /** @type {?} */
        const fieldValue = map[fieldName];
        expressions.push(typeof fieldValue == 'string' ?
            new StringStatement(fieldName, (/** @type {?} */ (fieldValue))) :
            new NumberStatement(fieldName, fieldValue));
    }
    return new ExpressionGroup(expressions, joiner);
}
class SingleExpression extends ExpressionGroup {
    /**
     * @param {?} expression
     */
    constructor(expression) {
        super([expression], undefined);
    }
}
class AndExpressionGroup extends ExpressionGroup {
    /**
     * @param {?} expressions
     */
    constructor(expressions) {
        super(expressions, ExpressionGroupJoiner.AND);
    }
}
class OrExpressionGroup extends ExpressionGroup {
    /**
     * @param {?} expressions
     */
    constructor(expressions) {
        super(expressions, ExpressionGroupJoiner.OR);
    }
}
class UpdateStatements extends ExpressionGroup {
    /**
     * @param {?} expressions
     */
    constructor(expressions) {
        super(expressions, ExpressionGroupJoiner.COMMA, false);
    }
}
/**
 * @abstract
 */
class FieldStatement extends Expression {
    /**
     * @param {?} fieldName
     * @param {?} value
     * @param {?} operator
     */
    constructor(fieldName, value, operator) {
        super(fieldName, operator, value);
    }
}
class NumberStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     * @param {?} data
     * @param {?=} operator
     */
    constructor(fieldName, data, operator = ExpressionOperator.EQUAL) {
        super(fieldName, new MySQLNumberValue(data), operator);
    }
}
class StringStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     * @param {?} data
     * @param {?=} operator
     */
    constructor(fieldName, data, operator = ExpressionOperator.EQUAL) {
        super(fieldName, new MySQLStringValue(data), operator);
    }
}
class StringExpressionStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     * @param {?} data
     * @param {?=} operator
     */
    constructor(fieldName, data, operator = ExpressionOperator.EQUAL) {
        super(fieldName, new MySQLStringExpressionValue(data), operator);
    }
}
class NullUpdateStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nullValue, ExpressionOperator.EQUAL);
    }
}
class NowUpdateStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nowValue, ExpressionOperator.EQUAL);
    }
}
class FieldIsNullStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nullValue, ExpressionOperator.IS);
    }
}
class FieldIsNotNullStatement extends FieldStatement {
    /**
     * @param {?} fieldName
     */
    constructor(fieldName) {
        super(fieldName, nullValue, ExpressionOperator.IS_NOT);
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-query.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const logSqlQueries$1 = process.env.LOG_SQL_QUERIES === 'true';
/**
 * @template T
 */
class MySQLQuery {
    /**
     * @param {?} sql
     * @param {?} connectionManager
     * @param {?=} argumentsToPrepare
     */
    constructor(sql, connectionManager, argumentsToPrepare) {
        this.sql = sql;
        this.connectionManager = connectionManager;
        this.argumentsToPrepare = argumentsToPrepare;
    }
    /**
     * @return {?}
     */
    execute() {
        return __awaiter(this, void 0, void 0, function* () {
            // When execute is called directly,
            // by default, it will use the connectionManager used to construct the query...
            /** @type {?} */
            const connection = yield this.connectionManager.getConnection();
            return executeQuery(this.sql, connection, this.argumentsToPrepare);
        });
    }
}
if (false) {
    /** @type {?} */
    MySQLQuery.prototype.sql;
    /**
     * @type {?}
     * @private
     */
    MySQLQuery.prototype.connectionManager;
    /** @type {?} */
    MySQLQuery.prototype.argumentsToPrepare;
}
/**
 * @template T
 * @param {?} sql
 * @param {?} connection
 * @param {?} argumentsToPrepare
 * @return {?}
 */
function executeQuery(sql, connection, argumentsToPrepare) {
    return new Promise((/**
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    (resolve, reject) => {
        if (logSqlQueries$1) {
            logWithTime(sql, argumentsToPrepare);
        }
        connection.query(sql, argumentsToPrepare, (/**
         * @param {?} error
         * @param {?} result
         * @param {?} fields
         * @return {?}
         */
        (error, result, fields) => {
            if (error) {
                logWithTime('*** MySQL Error while executing Query: ', sql, argumentsToPrepare);
                console.error(error);
                reject(error);
            }
            else {
                resolve(result);
            }
        }));
    }));
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-query-builder.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MySQLQueryBuilder {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.connectionManager = new MySQLConnectionManager(config);
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    insert(repository, entities) {
        return __awaiter(this, void 0, void 0, function* () {
            // construct query
            /** @type {?} */
            const query = yield this.constructInsertQuery(repository, entities);
            return new MySQLQuery(query, this.connectionManager);
        });
    }
    /**
     * @private
     * @param {?} repository
     * @param {?} entities
     * @return {?}
     */
    constructInsertQuery(repository, entities) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const fields = [];
            for (const fieldName in entities[0]) {
                fields.push(fieldName);
            }
            /** @type {?} */
            const rows = [];
            for (const entity of entities) {
                rows.push(yield this.constructRowForInsert(entity));
            }
            return 'INSERT INTO ' + repository.tableName +
                ' (' + this.prepareFieldList(fields) + ')' +
                ' VALUES ' + rows.join(', ') + ';';
        });
    }
    /**
     * @private
     * @param {?} entity
     * @return {?}
     */
    constructRowForInsert(entity) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const values = [];
            for (const propertyName in entity) {
                /** @type {?} */
                const rawValue = entity[propertyName];
                /** @type {?} */
                const preparedValue = yield this.prepareValueForInsert(rawValue);
                values.push(preparedValue);
            }
            return '(' + values.join(',') + ')';
        });
    }
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    prepareValueForInsert(value) {
        return __awaiter(this, void 0, void 0, function* () {
            switch (typeof value) {
                case 'string':
                    switch (value) {
                        case MySQLExpression.NULL:
                        case MySQLExpression.NOW:
                            break;
                        default:
                            value = yield this.escape(value);
                            break;
                    }
                    break;
                default:
                    value = yield this.escape(value);
                    break;
            }
            return '' + value;
        });
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @param {?} primaryKeyValues
     * @return {?}
     */
    selectByPrimaryKeys(repository, fields, primaryKeyValues) {
        /** @type {?} */
        const query = this.prepareFieldsForSelect(fields, repository.tableName) +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    }
    /**
     * @template T
     * @param {?} repository
     * @param {?} fields
     * @return {?}
     */
    selectAll(repository, fields) {
        /** @type {?} */
        const query = this.prepareFieldsForSelect(fields, repository.tableName) + ';';
        return new MySQLQuery(query, this.connectionManager);
    }
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    select(params) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.prepareSelectStatement(params);
            return new MySQLQuery(`${query};`, this.connectionManager);
        });
    }
    /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    prepareSelectStatement(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const { fields, repository } = params;
            /** @type {?} */
            let query = this.prepareFieldsForSelect(fields, repository.tableName);
            query += yield this.prepareWhereOrderAndLimit(params);
            return query;
        });
    }
    /**
     * @private
     * @param {?} fields
     * @param {?} tableName
     * @return {?}
     */
    prepareFieldsForSelect(fields, tableName) {
        /** @type {?} */
        const fieldsList = this.prepareFieldList(fields);
        return `SELECT ${fieldsList} FROM ${tableName}`;
    }
    /**
     * @private
     * @param {?} fields
     * @return {?}
     */
    prepareFieldList(fields) {
        /** @type {?} */
        const prepared = [];
        for (const field of fields) {
            /** @type {?} */
            const isAlias = field.indexOf(' AS ') > -1;
            prepared.push(isAlias ?
                field :
                '`' + field + '`');
        }
        return prepared.join(',');
    }
    /**
     * @template T
     * @param {?} params
     * @return {?}
     */
    update(params) {
        return __awaiter(this, void 0, void 0, function* () {
            if (params.singleStatement) {
                params.statements = [
                    params.singleStatement
                ];
            }
            const { statements, repository } = params;
            if (!statements ||
                statements.length < 1) {
                throw new Error('there must be at least 1 update statement!');
            }
            /** @type {?} */
            const updateStatements = yield this.prepareExpressionGroup(new UpdateStatements(statements));
            /** @type {?} */
            const remainingQuery = yield this.prepareWhereOrderAndLimit(params);
            /** @type {?} */
            const query = 'UPDATE ' + repository.tableName +
                ' SET ' + updateStatements +
                remainingQuery + ';';
            return new MySQLQuery(query, this.connectionManager);
        });
    }
    /**
     * @private
     * @template T
     * @param {?} params
     * @return {?}
     */
    prepareWhereOrderAndLimit(params) {
        return __awaiter(this, void 0, void 0, function* () {
            if (params.whereMap) {
                params.whereAndMap = params.whereMap;
            }
            if (params.whereAndMap ||
                params.whereOrMap) {
                params.whereGroup = createExpressionGroup(params.whereAndMap ?
                    params.whereAndMap :
                    params.whereOrMap, params.whereAndMap ?
                    ExpressionGroupJoiner.AND :
                    ExpressionGroupJoiner.OR);
            }
            else if (params.wherePrimaryKeys) {
                /** @type {?} */
                const expressions = params.wherePrimaryKeys.map((/**
                 * @param {?} primaryKeyValue
                 * @return {?}
                 */
                (primaryKeyValue) => new StringStatement(params.repository.primaryKey, '' + primaryKeyValue)));
                params.whereGroup = new OrExpressionGroup(expressions);
            }
            else if (params.whereClause) {
                params.whereGroup = new SingleExpression(params.whereClause);
            }
            const { whereGroup, orderBy, limit } = params;
            /** @type {?} */
            let query = '';
            /** @type {?} */
            const whereClause = whereGroup &&
                (yield this.prepareExpressionGroup(whereGroup));
            if (whereClause) {
                query += `
                WHERE ${whereClause}`;
            }
            if (orderBy) {
                query += `
                ORDER BY ${orderBy}`;
            }
            if (limit) {
                query += `
                LIMIT ${limit}`;
            }
            return query;
        });
    }
    /**
     * @private
     * @param {?} group
     * @return {?}
     */
    prepareExpressionGroup(group) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const statements = [];
            /** @type {?} */
            const useBrackets = group.surroundExpressionWithBrackets;
            for (const expression of group.expressions) {
                const { leftSide, operator, rightSide } = expression;
                /** @type {?} */
                const rightSideValue = rightSide.isString ?
                    `${yield this.escape((/** @type {?} */ (rightSide.data)))}` :
                    rightSide.data;
                statements.push(`${useBrackets ? '(' : ''} \`${leftSide}\` ${operator} ${rightSideValue} ${useBrackets ? ')' : ''}`);
            }
            return statements.join(` ${group.joiner} `);
        });
    }
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    escape(value) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const connection = yield this.connectionManager.getConnection();
            return connection.escape(value);
        });
    }
    /**
     * @param {?} repository
     * @param {?} primaryKeyValues
     * @return {?}
     */
    delete(repository, primaryKeyValues) {
        if (primaryKeyValues.length < 1) {
            throw new Error('Please specify which records to delete!');
        }
        /** @type {?} */
        const query = 'DELETE FROM ' + repository.tableName +
            constructWhereWithPrimaryKeys(repository, primaryKeyValues) + ';';
        return new MySQLQuery(query, this.connectionManager);
    }
    /**
     * @template T
     * @param {?} sql
     * @param {?=} argumentsToPrepare
     * @return {?}
     */
    rawQuery(sql, argumentsToPrepare) {
        return new MySQLQuery(sql, this.connectionManager, argumentsToPrepare);
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLQueryBuilder.prototype.connectionManager;
}
/**
 * @param {?} repository
 * @param {?} primaryKeyValues
 * @return {?}
 */
function constructWhereWithPrimaryKeys(repository, primaryKeyValues) {
    /** @type {?} */
    const conditions = [];
    for (let primaryKeyValue of primaryKeyValues) {
        if (repository.isPrimaryKeyAString) {
            primaryKeyValue = '\'' + primaryKeyValue + '\'';
        }
        /** @type {?} */
        const condition = repository.primaryKey + ' = ' + primaryKeyValue;
        conditions.push(condition);
    }
    return ' WHERE ' + conditions.join(' OR ');
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-transaction-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MySQLTransactionManager {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.queue = [];
        this.isExecutingTransaction = false;
        /**
         * we need to manage a queue of transactions
         * to be executed in order
         * once the transaction either is successful or fails
         * then move on to the next
         */
        this.performNextTransaction = (/**
         * @return {?}
         */
        () => {
            if (this.isExecutingTransaction ||
                this.queue.length < 1) {
                return;
            }
            logWithTime('perform next transaction!');
            this.isExecutingTransaction = true;
            // remove first element from queue and return it
            /** @type {?} */
            const executor = this.queue.shift();
            executor.execute();
        });
        this.onTransactionCompleted = (/**
         * @return {?}
         */
        () => {
            logWithTime('onTransactionCompleted');
            this.isExecutingTransaction = false;
            this.performNextTransaction();
        });
        this.connectionManager = new MySQLConnectionManager(config);
    }
    /**
     * @param {?} queries
     * @return {?}
     */
    performTransaction(queries) {
        return this.performPreparedTransaction(queries.map((/**
         * @param {?} sql
         * @return {?}
         */
        sql => ({ sql, argumentsToPrepare: undefined }))));
    }
    /**
     * @param {?} data
     * @return {?}
     */
    performPreparedTransaction(data) {
        /*
                    we need to promise the result of each query executed
                    so that they can be inspected by consumer for Ids, etc
                    */
        /** @type {?} */
        const executor = new MySQLTransactionExecuter(data, this.performNextTransaction, this.onTransactionCompleted, this.connectionManager);
        // add executor to queue
        this.queue.push(executor);
        return new Promise(executor.init);
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.queue;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.isExecutingTransaction;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.connectionManager;
    /**
     * we need to manage a queue of transactions
     * to be executed in order
     * once the transaction either is successful or fails
     * then move on to the next
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.performNextTransaction;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionManager.prototype.onTransactionCompleted;
}
class MySQLTransactionExecuter {
    /**
     * @param {?} queries
     * @param {?} onInitHandler
     * @param {?} onCompleteHandler
     * @param {?} connectionManager
     */
    constructor(queries, onInitHandler, onCompleteHandler, connectionManager) {
        this.queries = queries;
        this.onInitHandler = onInitHandler;
        this.onCompleteHandler = onCompleteHandler;
        this.connectionManager = connectionManager;
        this.init = (/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.resolve = resolve;
            this.reject = reject;
            this.onInitHandler();
        });
    }
    /**
     * @return {?}
     */
    execute() {
        return __awaiter(this, void 0, void 0, function* () {
            // only need to get connection once?
            this.connection = yield this.connectionManager.getConnection();
            this.connection.beginTransaction((/**
             * @return {?}
             */
            () => __awaiter(this, void 0, void 0, function* () {
                try {
                    /** @type {?} */
                    const results = [];
                    for (const { sql, argumentsToPrepare } of this.queries) {
                        /** @type {?} */
                        const result = yield executeQuery(sql, this.connection, argumentsToPrepare);
                        results.push(result);
                    }
                    yield this.commit();
                    this.resolve(results);
                }
                catch (e) {
                    yield this.rollback();
                    this.reject(e);
                }
                this.onCompleteHandler();
            })));
        });
    }
    /**
     * @private
     * @return {?}
     */
    commit() {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.connection.commit((/**
             * @param {?} error
             * @return {?}
             */
            (error) => __awaiter(this, void 0, void 0, function* () {
                if (error) {
                    logWithTime('ERROR in committing queries', this.queries);
                    return reject(error);
                }
                resolve();
            })));
        }));
    }
    /**
     * @private
     * @return {?}
     */
    rollback() {
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        (resolve) => {
            this.connection.rollback(resolve);
        }));
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.resolve;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.reject;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.connection;
    /** @type {?} */
    MySQLTransactionExecuter.prototype.init;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.queries;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.onInitHandler;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.onCompleteHandler;
    /**
     * @type {?}
     * @private
     */
    MySQLTransactionExecuter.prototype.connectionManager;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-database.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MySQLDatabase {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.builder = new MySQLQueryBuilder(config);
        this.transactionManager = new MySQLTransactionManager(config);
    }
}
if (false) {
    /** @type {?} */
    MySQLDatabase.prototype.builder;
    /** @type {?} */
    MySQLDatabase.prototype.transactionManager;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/database/engine/mysql/mysql-repository.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template TypeForSelect
 */
class MySQLRepository {
    /**
     * @param {?} db
     * @param {?} _tableName
     * @param {?=} defaultFieldsToSelect
     * @param {?=} _primaryKey
     * @param {?=} isPrimaryKeyAString
     */
    constructor(db, _tableName, defaultFieldsToSelect = [], _primaryKey = 'id', isPrimaryKeyAString = false) {
        this.db = db;
        this._tableName = _tableName;
        this.defaultFieldsToSelect = defaultFieldsToSelect;
        this._primaryKey = _primaryKey;
        this.isPrimaryKeyAString = isPrimaryKeyAString;
    }
    /**
     * @template T
     * @param {?} entity
     * @return {?}
     */
    insert(entity) {
        return this.insertMany([entity]);
    }
    /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    insertMany(entities) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.buildInsert(entities);
            return yield query.execute();
        });
    }
    /**
     * @template T
     * @param {?} entities
     * @return {?}
     */
    buildInsert(entities) {
        return this.db.builder.insert(this, entities);
    }
    /**
     * @param {?} primaryKeyValue
     * @param {?=} fields
     * @return {?}
     */
    selectOneByPrimaryKey(primaryKeyValue, fields = this.defaultFieldsToSelect) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const rows = yield this.selectManyByPrimaryKeys([primaryKeyValue], fields);
            return rows.length > 0 ?
                rows[0] :
                null;
        });
    }
    /**
     * @param {?} primaryKeyValues
     * @param {?=} fields
     * @return {?}
     */
    selectManyByPrimaryKeys(primaryKeyValues, fields = this.defaultFieldsToSelect) {
        return this.db.builder.selectByPrimaryKeys(this, fields, primaryKeyValues).execute();
    }
    /**
     * @param {?=} fields
     * @return {?}
     */
    selectAll(fields = this.defaultFieldsToSelect) {
        return this.db.builder.selectAll(this, fields).execute();
    }
    /**
     * @param {?} params
     * @return {?}
     */
    selectOne(params) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const rows = yield this.select(Object.assign({}, params, { limit: 1 }));
            return rows.length > 0 ?
                rows[0] :
                null;
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    select(params) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.buildSelect(params);
            return query.execute();
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    buildSelect(params) {
        /** @type {?} */
        const fields = params.fields ?
            params.fields :
            this.defaultFieldsToSelect;
        /** @type {?} */
        const newParams = Object.assign({}, params, { fields, repository: this });
        return this.db.builder.select(newParams);
    }
    /**
     * @param {?} params
     * @return {?}
     */
    updateOne(params) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.buildUpdateOne(params);
            return query.execute();
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    buildUpdateOne(params) {
        return this.buildUpdate(Object.assign({}, params, { limit: 1 }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    update(params) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const query = yield this.buildUpdate(params);
            return query.execute();
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    buildUpdate(params) {
        /** @type {?} */
        const builderParams = Object.assign({}, params, { repository: this });
        return this.db.builder.update(builderParams);
    }
    /**
     * @param {?} primaryKeyValues
     * @return {?}
     */
    delete(primaryKeyValues) {
        return this.db.builder.delete(this, primaryKeyValues).execute();
    }
    /**
     * @return {?}
     */
    get tableName() {
        return this._tableName;
    }
    /**
     * @return {?}
     */
    get primaryKey() {
        return this._primaryKey;
    }
    /**
     * @private
     * @return {?}
     */
    get primaryKeyFieldList() {
        return [this.primaryKey];
    }
}
if (false) {
    /**
     * @type {?}
     * @protected
     */
    MySQLRepository.prototype.db;
    /**
     * @type {?}
     * @private
     */
    MySQLRepository.prototype._tableName;
    /**
     * @type {?}
     * @protected
     */
    MySQLRepository.prototype.defaultFieldsToSelect;
    /**
     * @type {?}
     * @private
     */
    MySQLRepository.prototype._primaryKey;
    /** @type {?} */
    MySQLRepository.prototype.isPrimaryKeyAString;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/coins.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const CoinId = {
    BTC: 0,
    ETH: 1,
    EOS: 2,
    BET_BINANCE: 3,
    LTC: 4,
    XRP: 5,
    BCH: 6,
    BNB: 7,
    WAX: 8,
    TRX: 9,
    LINK: 10,
    BET_ETHEREUM: 11,
    DAI: 12,
    USDC: 13,
    USDT: 14,
    STACK: 15,
};
CoinId[CoinId.BTC] = 'BTC';
CoinId[CoinId.ETH] = 'ETH';
CoinId[CoinId.EOS] = 'EOS';
CoinId[CoinId.BET_BINANCE] = 'BET_BINANCE';
CoinId[CoinId.LTC] = 'LTC';
CoinId[CoinId.XRP] = 'XRP';
CoinId[CoinId.BCH] = 'BCH';
CoinId[CoinId.BNB] = 'BNB';
CoinId[CoinId.WAX] = 'WAX';
CoinId[CoinId.TRX] = 'TRX';
CoinId[CoinId.LINK] = 'LINK';
CoinId[CoinId.BET_ETHEREUM] = 'BET_ETHEREUM';
CoinId[CoinId.DAI] = 'DAI';
CoinId[CoinId.USDC] = 'USDC';
CoinId[CoinId.USDT] = 'USDT';
CoinId[CoinId.STACK] = 'STACK';
/** @enum {string} */
const CoinSymbol = {
    BTC: "BTC",
    ETH: "ETH",
    EOS: "EOS",
    BET: "BET",
    LTC: "LTC",
    XRP: "XRP",
    BCH: "BCH",
    BNB: "BNB",
    WAX: "WAX",
    TRX: "TRX",
    LINK: "LINK",
    DAI: "DAI",
    USDC: "USDC",
    USDT: "USDT",
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/mnemonic/mnemonic-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MnemonicUtility {
    /**
     * @param {?} _words
     * @param {?} network
     */
    constructor(_words, network) {
        this._words = _words;
        this.network = network;
        this.isInit = false;
        this.root = undefined;
    }
    /**
     * @return {?}
     */
    get words() {
        return this._words;
    }
    /**
     * @return {?}
     */
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isInit) {
                return;
            }
            this.isInit = true;
            /** @type {?} */
            const seed = yield mnemonicToSeed(this.words);
            this.root = fromSeed(seed, this.network);
        });
    }
    /**
     * @param {?} coinId
     * @return {?}
     */
    getNodeForChangeAddress(coinId) {
        /** @type {?} */
        let basePath = getBasePath(coinId);
        // console.log(basePath);
        switch (coinId) {
            case CoinId.BTC:
            case CoinId.LTC:
            case CoinId.BCH:
                basePath = basePath.substr(0, basePath.length - 2) + '1/';
                break;
        }
        /** @type {?} */
        const path = basePath + '0';
        // console.log(path);
        /** @type {?} */
        const node = this.root.derivePath(path);
        return node;
    }
    /**
     * @param {?} coinId
     * @param {?} userId
     * @return {?}
     */
    getNodeForDepositAddress(coinId, userId) {
        /** @type {?} */
        const path = getBasePath(coinId) + userId;
        return this.root.derivePath(path);
    }
    /**
     * @param {?} userId
     * @param {?} coinId
     * @param {?} prefix
     * @return {?}
     */
    getDepositMemo(userId, coinId, prefix) {
        /** @type {?} */
        const node = this.getNodeForDepositAddress(coinId, userId);
        /** @type {?} */
        const memo = encode(node.publicKey.slice(0, 12)).toLowerCase();
        return `${prefix}${memo}`;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MnemonicUtility.prototype.isInit;
    /**
     * @type {?}
     * @private
     */
    MnemonicUtility.prototype.root;
    /**
     * @type {?}
     * @private
     */
    MnemonicUtility.prototype._words;
    /**
     * @type {?}
     * @private
     */
    MnemonicUtility.prototype.network;
}
/**
 * the MnemonicUtility class should be a singleton: **
 * @type {?}
 */
let mnemonicUtility;
/** @type {?} */
const allowedNetworkNames = ['mainnet', 'testnet', 'regtest'];
/**
 * @param {?} config
 * @return {?}
 * @this {*}
 */
function getMnemonicUtil(config) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!mnemonicUtility) {
            /** @type {?} */
            const words = config.mnemonicPhrase;
            /** @type {?} */
            const networkName = config.blockchainNetwork;
            if (allowedNetworkNames.indexOf(networkName) < 0) {
                throw new Error(`${networkName} network not supported.`);
            }
            mnemonicUtility = new MnemonicUtility(words, networks[networkName]);
            yield mnemonicUtility.init();
        }
        return mnemonicUtility;
    });
}
/**
 *
 * @param {?} coinId
 * @return {?}
 */
function getBasePath(coinId) {
    switch (coinId) {
        case CoinId.BTC:
            return 'm/49\'/0\'/0\'/0/';
        case CoinId.LTC:
            return 'm/49\'/2\'/0\'/0/';
        case CoinId.BCH:
            return 'm/44\'/145\'/0\'/0/';
        case CoinId.ETH:
            return 'm/44\'/60\'/0\'/0/';
        case CoinId.BNB:
        case CoinId.BET_BINANCE:
            // https://docs.binance.org/blockchain.html
            // derive the private key using BIP32 / BIP44 with HD prefix as "44'/714'/", which is reserved at SLIP 44.
            // 714 comes from Binance's birthday, July 14th
            return `m/44'/714'/0'/0/`;
        case CoinId.XRP:
            return `m/44'/144'/0'/0/`;
        case CoinId.TRX:
            return `m/44'/195'/0'/0/`;
        case CoinId.EOS:
            return `m/44'/194'/0'/0/`;
        default:
            throw new Error('CoinId NOT SUPPORTED: ' + coinId);
    }
}
/**
 * @return {?}
 */
function generateMnemonicPhrase() {
    return generateMnemonic(256);
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/mnemonic/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const MemoPrefix = {
    EOSBET: "EOSBET_",
    EARNBET: "EARNBET_",
    XRP: "EARNBET_XRP_",
};
/**
 * @record
 */
function IMnemonicUtility() { }
if (false) {
    /** @type {?} */
    IMnemonicUtility.prototype.words;
    /**
     * @param {?} userId
     * @param {?} coinId
     * @param {?} prefix
     * @return {?}
     */
    IMnemonicUtility.prototype.getDepositMemo = function (userId, coinId, prefix) { };
    /**
     * @param {?} coinId
     * @return {?}
     */
    IMnemonicUtility.prototype.getNodeForChangeAddress = function (coinId) { };
    /**
     * @param {?} coinId
     * @param {?} userId
     * @return {?}
     */
    IMnemonicUtility.prototype.getNodeForDepositAddress = function (coinId, userId) { };
}
/**
 * @record
 */
function IMnemonicUtilityConfig() { }
if (false) {
    /** @type {?} */
    IMnemonicUtilityConfig.prototype.mnemonicPhrase;
    /** @type {?} */
    IMnemonicUtilityConfig.prototype.blockchainNetwork;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-settings.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IEosConfig() { }
if (false) {
    /** @type {?} */
    IEosConfig.prototype.eosjs;
    /** @type {?} */
    IEosConfig.prototype.endpoints;
    /** @type {?|undefined} */
    IEosConfig.prototype.messageSigningPrivateKey;
}
/**
 * @record
 */
function IEosAccountNamesBase() { }
if (false) {
    /** @type {?} */
    IEosAccountNamesBase.prototype.easyAccount;
    /** @type {?} */
    IEosAccountNamesBase.prototype.withdrawal;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-txn-executor.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const _1HoursInSeconds = 60 * 60;
class EosTransactionExecutorBase {
    /**
     * @param {?} actions
     * @param {?} eos
     * @param {?} txnChecker
     * @param {?=} retryOnError
     * @param {?=} secondsBeforeRetry
     * @param {?=} expireInSeconds
     */
    constructor(actions, eos, txnChecker, retryOnError = true, secondsBeforeRetry = 10, expireInSeconds = _1HoursInSeconds) {
        this.actions = actions;
        this.eos = eos;
        this.txnChecker = txnChecker;
        this.retryOnError = retryOnError;
        this.secondsBeforeRetry = secondsBeforeRetry;
        this.expireInSeconds = expireInSeconds;
        this.numOfRetries = 0;
        this.signedTransaction = undefined;
        // include first action for now
        // include other actions as well?
        const { account, name } = this.actions[0];
        this.actionId = account + ' - ' + name;
    }
    /**
     * @return {?}
     */
    execute() {
        return __awaiter(this, void 0, void 0, function* () {
            debugMessage('Attempting to Execute Transaction: ' + this.actionId);
            console.log(this.actions);
            try {
                yield this.sign();
                // attempt to broadcast
                this.broadcastedTxnId = yield broadcastSignedTransaction(this.eos, this.signedTransaction.transaction);
                this.broadcastedTime = Date.now();
                debugMessage(this.actionId + ' Broadcasted: ' + this.broadcastedTxnId);
                return this.broadcastedTxnId;
            }
            catch (error) {
                return this.onBroadcastError(error);
            }
        });
    }
    /**
     * @return {?}
     */
    sign() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // sign transaction if not already signed
                if (this.signedTransaction == undefined) {
                    this.signedTransaction = yield signTransaction(this.eos, this.actions, this.expireInSeconds);
                }
                debugMessage('Transaction Signed: ' + this.signedTransaction.transaction_id);
                return this.signedTransaction;
            }
            catch (error) {
                console.error(error);
                yield sleep(1000);
                return this.sign();
            }
        });
    }
    /**
     * @protected
     * @param {?} error
     * @return {?}
     */
    onBroadcastError(error) {
        return __awaiter(this, void 0, void 0, function* () {
            console.error(error);
            /** @type {?} */
            const isDuplicateTxn = isDuplicateTxnError(error);
            // if duplicate error, then stop broadcasting process
            // return originally broadcasted txnId
            if (isDuplicateTxn) {
                return this.broadcastedTxnId;
            }
            if (this.retryOnError) {
                /** @type {?} */
                const maxNumOfRetries = (_1HoursInSeconds / this.secondsBeforeRetry) - 1;
                if (this.numOfRetries++ < maxNumOfRetries) {
                    // wait and then retry
                    yield sleep(1000 * this.secondsBeforeRetry);
                    return this.execute();
                }
            }
            throw error;
        });
    }
    /**
     * @return {?}
     */
    confirm() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.broadcastedTxnId) {
                throw new Error('Transaction has not yet been succesfully broadcasted!');
            }
            if (!this.txnChecker) {
                throw new Error('no Transaction Checker function defined!');
            }
            // wait 5 minutes before checking irreversibility
            /** @type {?} */
            const timeToWait = 5 * 60 * 1000;
            /** @type {?} */
            const remainingTimeToWait = timeToWait -
                (Date.now() - this.broadcastedTime);
            console.log({ remainingTimeToWait });
            if (remainingTimeToWait > 0) {
                yield sleep(remainingTimeToWait);
            }
            /** @type {?} */
            const txnId = this.broadcastedTxnId;
            // confirm irreversibility
            try {
                /** @type {?} */
                const isConfirmed = yield this.txnChecker(txnId);
                if (!isConfirmed) {
                    debugMessage(this.actionId + ' Txn: ' + txnId + ' is NOT CONFIRMED!');
                    return this.execute();
                }
                debugMessage(this.actionId + ' Txn: ' + txnId + ' is IRREVERSIBLE!');
                return txnId;
            }
            catch (error) {
                debugMessage('Cound Not Confirm Txn: ' + txnId);
                console.error(error);
                // wait
                yield sleep(5 * 1000);
                return this.execute();
            }
        });
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.numOfRetries;
    /**
     * @type {?}
     * @protected
     */
    EosTransactionExecutorBase.prototype.actionId;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.signedTransaction;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.broadcastedTxnId;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.broadcastedTime;
    /** @type {?} */
    EosTransactionExecutorBase.prototype.actions;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.eos;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.txnChecker;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.retryOnError;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.secondsBeforeRetry;
    /**
     * @type {?}
     * @private
     */
    EosTransactionExecutorBase.prototype.expireInSeconds;
}
/**
 * @template T
 */
class EosTransactionExecutor extends EosTransactionExecutorBase {
    /**
     * @param {?} actions
     * @param {?} eosUtility
     * @param {?} txnChecker
     * @param {?=} retryOnError
     * @param {?=} secondsBeforeRetry
     */
    constructor(actions, eosUtility, txnChecker, retryOnError = true, secondsBeforeRetry = 10) {
        super(actions.map((/**
         * @param {?} action
         * @return {?}
         */
        (action) => (Object.assign({}, action, { authorization: eosUtility.authorization })))), eosUtility.transactor, txnChecker, retryOnError, secondsBeforeRetry);
    }
}
/*
Error: {"code":409,"message":"Conflict","error":{"code":3040008,"name":"tx_duplicate","what":"Duplicate transaction","details":[{"message":"duplicate transaction 7be2845618fa965d89ff8f3b368ed1c768dab63303c38c7d8a5e9f031d7a23c6","file":"producer_plugin.cpp","line_number":527,"method":"process_incoming_transaction_async"}]}}
*/
/**
 * @param {?} error
 * @return {?}
 */
function isDuplicateTxnError(error) {
    /** @type {?} */
    const searchStrings = [
        'tx_duplicate',
        'Duplicate transaction',
        'duplicate transaction'
    ];
    /** @type {?} */
    const errorString = String(error);
    for (const searchString of searchStrings) {
        if (errorString.indexOf(searchString) > -1) {
            return true;
        }
    }
    return false;
}
/**
 * @record
 */
function ISignedTransaction() { }
if (false) {
    /** @type {?} */
    ISignedTransaction.prototype.compression;
    /** @type {?} */
    ISignedTransaction.prototype.transaction;
    /** @type {?} */
    ISignedTransaction.prototype.signatures;
}
/**
 * @record
 */
function ISignedTransactionResult() { }
if (false) {
    /** @type {?} */
    ISignedTransactionResult.prototype.transaction_id;
    /** @type {?} */
    ISignedTransactionResult.prototype.transaction;
}
/**
 * @record
 */
function IBroadcastedTransactionResult() { }
if (false) {
    /** @type {?} */
    IBroadcastedTransactionResult.prototype.transaction_id;
}
/**
 * @param {?} transactor
 * @param {?} actions
 * @param {?} expireInSeconds
 * @return {?}
 * @this {*}
 */
function signTransaction(transactor, actions, expireInSeconds) {
    return __awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const result = yield transactor.transaction({
            actions
        }, {
            broadcast: false,
            sign: true,
            expireInSeconds
        });
        return result;
    });
}
/**
 * @param {?} transactor
 * @param {?} transaction
 * @return {?}
 * @this {*}
 */
function broadcastSignedTransaction(transactor, transaction) {
    return __awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const result = yield transactor.pushTransaction(transaction);
        return result.transaction_id;
    });
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IEOSInfo() { }
if (false) {
    /** @type {?} */
    IEOSInfo.prototype.server_version;
    /** @type {?} */
    IEOSInfo.prototype.chain_id;
    /** @type {?} */
    IEOSInfo.prototype.head_block_num;
    /** @type {?} */
    IEOSInfo.prototype.last_irreversible_block_num;
    /** @type {?} */
    IEOSInfo.prototype.last_irreversible_block_id;
    /** @type {?} */
    IEOSInfo.prototype.head_block_id;
    /** @type {?} */
    IEOSInfo.prototype.head_block_time;
    /** @type {?} */
    IEOSInfo.prototype.head_block_producer;
    /** @type {?} */
    IEOSInfo.prototype.virtual_block_cpu_limit;
    /** @type {?} */
    IEOSInfo.prototype.virtual_block_net_limit;
    /** @type {?} */
    IEOSInfo.prototype.block_cpu_limit;
    /** @type {?} */
    IEOSInfo.prototype.block_net_limit;
    /** @type {?} */
    IEOSInfo.prototype.server_version_string;
}
/**
 * @record
 * @template T
 */
function IEosUtilityParams() { }
if (false) {
    /** @type {?} */
    IEosUtilityParams.prototype.chainId;
    /** @type {?} */
    IEosUtilityParams.prototype.config;
    /** @type {?} */
    IEosUtilityParams.prototype.contracts;
    /** @type {?} */
    IEosUtilityParams.prototype.authorization;
}
/**
 * @record
 * @template T
 */
function IEosUtility() { }
if (false) {
    /** @type {?} */
    IEosUtility.prototype.chainId;
    /** @type {?} */
    IEosUtility.prototype.config;
    /** @type {?} */
    IEosUtility.prototype.contracts;
    /** @type {?} */
    IEosUtility.prototype.authorization;
    /** @type {?} */
    IEosUtility.prototype.transactor;
    /**
     * @return {?}
     */
    IEosUtility.prototype.getInfo = function () { };
    /**
     * @param {?} contract
     * @param {?} account
     * @param {?} symbol
     * @param {?=} table
     * @return {?}
     */
    IEosUtility.prototype.getTokenBalance = function (contract, account, symbol, table) { };
    /**
     * @template T
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} rowId
     * @return {?}
     */
    IEosUtility.prototype.getUniqueTableRow = function (code, scope, table, rowId) { };
    /**
     * @template T
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} key
     * @return {?}
     */
    IEosUtility.prototype.getAllTableRows = function (code, scope, table, key) { };
    /**
     * @template T
     * @param {?} parameters
     * @return {?}
     */
    IEosUtility.prototype.getTableRows = function (parameters) { };
    /**
     * @param {?} actions
     * @return {?}
     */
    IEosUtility.prototype.executeTransaction = function (actions) { };
    /**
     * @param {?} eosAccountName
     * @return {?}
     */
    IEosUtility.prototype.isAccountSafe = function (eosAccountName) { };
    /**
     * @param {?} name
     * @return {?}
     */
    IEosUtility.prototype.isEasyAccountContract = function (name) { };
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/irreversible.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
let waxDatabase;
/** @type {?} */
let eosDatabase;
/**
 * @param {?} txnId
 * @return {?}
 * @this {*}
 */
function isWaxTxnIrreversible(txnId) {
    return __awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const waxDatabase = getWaxDatabase();
        return isTxnIrreversible(waxDatabase, txnId);
    });
}
/**
 * @param {?} txnId
 * @return {?}
 * @this {*}
 */
function isEosTxnIrreversible(txnId) {
    return __awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const eosDatabase = getEosDatabase();
        return isTxnIrreversible(eosDatabase, txnId);
    });
}
/**
 * @return {?}
 */
function getWaxDatabase() {
    if (waxDatabase == undefined) {
        if (process.env.WAX_IRREV_DB_HOSTNAME == undefined ||
            process.env.WAX_IRREV_DB_PORT == undefined ||
            process.env.WAX_IRREV_DB_USERNAME == undefined ||
            process.env.WAX_IRREV_DB_PASSWORD == undefined ||
            process.env.WAX_IRREV_DB_NAME == undefined) {
            throw new Error('must define ENV VARS for WAX_IRREV_DB!');
        }
        waxDatabase = new MySQLDatabase({
            host: process.env.WAX_IRREV_DB_HOSTNAME,
            port: Number(process.env.WAX_IRREV_DB_PORT),
            user: process.env.WAX_IRREV_DB_USERNAME,
            password: process.env.WAX_IRREV_DB_PASSWORD,
            database: process.env.WAX_IRREV_DB_NAME,
            charset: 'utf8',
        });
    }
    return waxDatabase;
}
/**
 * @return {?}
 */
function getEosDatabase() {
    if (eosDatabase == undefined) {
        if (process.env.EOS_IRREV_DB_HOSTNAME == undefined ||
            process.env.EOS_IRREV_DB_PORT == undefined ||
            process.env.EOS_IRREV_DB_USERNAME == undefined ||
            process.env.EOS_IRREV_DB_PASSWORD == undefined ||
            process.env.EOS_IRREV_DB_NAME == undefined) {
            throw new Error('must define ENV VARS for EOS_IRREV_DB!');
        }
        eosDatabase = new MySQLDatabase({
            host: process.env.EOS_IRREV_DB_HOSTNAME,
            port: Number(process.env.EOS_IRREV_DB_PORT),
            user: process.env.EOS_IRREV_DB_USERNAME,
            password: process.env.EOS_IRREV_DB_PASSWORD,
            database: process.env.EOS_IRREV_DB_NAME,
            charset: 'utf8',
        });
    }
    return eosDatabase;
}
/**
 * @param {?} database
 * @param {?} txnId
 * @return {?}
 * @this {*}
 */
function isTxnIrreversible(database, txnId) {
    return __awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const rows = yield database.builder.rawQuery(`select block_num from IRREVTX where trx_id = '${txnId.toLowerCase()}' LIMIT 1`).execute();
        console.log(rows);
        return rows.length > 0;
    });
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} eosConfig
 * @param {?=} endpoint
 * @return {?}
 */
function getEosJs(eosConfig, endpoint = undefined) {
    /** @type {?} */
    const cfg = Object.assign({}, eosConfig.eosjs);
    if (endpoint != undefined) {
        cfg.httpEndpoint = endpoint;
    }
    return Eos(cfg);
}
/**
 * @param {?} signedData
 * @param {?} info
 * @return {?}
 */
function recoverPublicKey(signedData, info) {
    /** @type {?} */
    let recoveredPublicKey;
    try {
        if (!info.isSignedDataHashed) {
            recoveredPublicKey = eos_ecc.recover(info.signature, signedData);
        }
        else {
            /** @type {?} */
            const signedHash = eos_ecc.sha256(eos_ecc.sha256(signedData) +
                eos_ecc.sha256(info.nonce));
            recoveredPublicKey = eos_ecc.recoverHash(info.signature, signedHash);
        }
    }
    catch (error) {
        console.error(error);
    }
    return recoveredPublicKey;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-util-class.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 */
class EosUtility {
    /**
     * @param {?} params
     */
    constructor(params) {
        const { config } = params;
        /** @type {?} */
        const endpoints = config.endpoints;
        this.poller = getEosJs(config, endpoints.poller);
        this.transactor = getEosJs(config, endpoints.transactor);
        this.chainId = params.chainId;
        this.config = config;
        this.contracts = params.contracts;
        this.authorization = params.authorization;
    }
    /**
     * @return {?}
     */
    getInfo() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.poller.getInfo({});
        });
    }
    /**
     * @template T
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} rowId
     * @return {?}
     */
    getUniqueTableRow(code, scope, table, rowId) {
        return getUniqueTableRow(this.poller, code, scope, table, rowId);
    }
    /**
     * @template T
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} key
     * @return {?}
     */
    getAllTableRows(code, scope, table, key) {
        return getAllTableRows(this.poller, code, scope, table, key);
    }
    /**
     * @template T
     * @param {?} parameters
     * @return {?}
     */
    getTableRows(parameters) {
        return getTableRows(this.poller, parameters);
    }
    /**
     * @param {?} contract
     * @param {?} account
     * @param {?} symbol
     * @param {?=} table
     * @return {?}
     */
    getTokenBalance(contract, account, symbol, table = 'accounts') {
        return getTokenBalance(this.poller, contract, account, symbol, table);
    }
    /**
     * @param {?} actions
     * @return {?}
     */
    executeTransaction(actions) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                /** @type {?} */
                const result = yield this.transactor.transaction({
                    actions: actions.map((/**
                     * @param {?} action
                     * @return {?}
                     */
                    (action) => (Object.assign({}, action, { authorization: this.authorization }))))
                });
                return result.transaction_id;
            }
            catch (error) {
                throw error;
            }
        });
    }
    /**
     * @param {?} eosAccountName
     * @return {?}
     */
    isAccountSafe(eosAccountName) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isEasyAccountContract(eosAccountName)) {
                return true;
            }
            try {
                /** @type {?} */
                const result = yield this.poller.getCodeHash(eosAccountName);
                return result.code_hash == '0000000000000000000000000000000000000000000000000000000000000000';
            }
            catch (error) {
                return false;
            }
        });
    }
    /**
     * @param {?} name
     * @return {?}
     */
    isEasyAccountContract(name) {
        return this.contracts.easyAccount != undefined &&
            name == this.contracts.easyAccount;
    }
}
if (false) {
    /** @type {?} */
    EosUtility.prototype.poller;
    /** @type {?} */
    EosUtility.prototype.transactor;
    /** @type {?} */
    EosUtility.prototype.chainId;
    /** @type {?} */
    EosUtility.prototype.config;
    /** @type {?} */
    EosUtility.prototype.contracts;
    /** @type {?} */
    EosUtility.prototype.authorization;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/models/withdrawal-request.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function INewWithdrawalRequestRow() { }
if (false) {
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.id;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.memo;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.coin_id;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.tx_hash;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.is_processed;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.easy_account_id;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.eos_account_name;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.withdraw_address;
    /** @type {?} */
    INewWithdrawalRequestRow.prototype.coin_decimal_amount;
}
/**
 * @record
 */
function IFailedWithdrawalRequestRow() { }
if (false) {
    /** @type {?} */
    IFailedWithdrawalRequestRow.prototype.error_message;
}
/**
 * @record
 */
function ISucceededWithdrawalRequest() { }
if (false) {
    /** @type {?} */
    ISucceededWithdrawalRequest.prototype.processed_at;
    /** @type {?} */
    ISucceededWithdrawalRequest.prototype.withdraw_transaction_id;
}
/**
 * @record
 */
function IWithdrawalRequestInsufficientFunds() { }
if (false) {
    /** @type {?} */
    IWithdrawalRequestInsufficientFunds.prototype.insufficient_funds;
}
/**
 * @record
 */
function ISavedWithdrawalRequestRow() { }
if (false) {
    /** @type {?} */
    ISavedWithdrawalRequestRow.prototype.requested_at;
}
/**
 * @record
 */
function IWithdrawalRequest() { }
if (false) {
    /** @type {?} */
    IWithdrawalRequest.prototype.address;
    /** @type {?} */
    IWithdrawalRequest.prototype.memo;
    /**
     * @return {?}
     */
    IWithdrawalRequest.prototype.getAmount = function () { };
}
/**
 * @record
 */
function ISavedWithdrawalRequest() { }
if (false) {
    /**
     * @return {?}
     */
    ISavedWithdrawalRequest.prototype.getRefundParams = function () { };
}
class SavedWithdrawalRequest {
    /**
     * @param {?} data
     * @param {?} account
     * @param {?} amountFactory
     */
    constructor(data, account, amountFactory) {
        this.account = account;
        this.amountFactory = amountFactory;
        this.userId = undefined;
        this.amount = undefined;
        this.id = data.id;
        this.coinId = data.coin_id;
        this.decimalAmount = data.coin_decimal_amount;
        this.transactionHash = data.tx_hash;
        this.address = data.withdraw_address;
        this.memo = data.memo ? data.memo : '';
        this.eosAccountName = data.eos_account_name;
        this.easyAccountId = data.easy_account_id;
        this.isEosAccount = Number(this.easyAccountId) == 0;
        this.errorMessage = data.error_message;
    }
    /**
     * @return {?}
     */
    getDataForApi() {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const amount = yield this.getAmount();
            return {
                id: this.id,
                memo: this.memo,
                address: this.address,
                errorMessage: this.errorMessage,
                coin: amount.currency.symbol,
                amount: amount.decimal,
            };
        });
    }
    /**
     * @return {?}
     */
    getAmount() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.amount == undefined) {
                this.amount = yield this.amountFactory.newAmountFromDecimalAndCoinId(this.decimalAmount, this.coinId);
            }
            return this.amount;
        });
    }
    /**
     * @return {?}
     */
    getRefundParams() {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const userId = yield this.getUserId();
            /** @type {?} */
            const info = yield this.account.getAccountInfo(userId);
            return {
                requestId: this.id,
                easyAccountPublicKey: info.easy_account_public_key ?
                    info.easy_account_public_key :
                    '',
                reason: this.errorMessage
            };
        });
    }
    /**
     * @return {?}
     */
    getUserId() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.userId == undefined) {
                this.userId =
                    this.isEosAccount ?
                        yield this.account.getUserIdForEosAccount(this.eosAccountName) :
                        yield this.account.getUserIdForEasyAccount(this.easyAccountId);
            }
            return this.userId;
        });
    }
}
if (false) {
    /** @type {?} */
    SavedWithdrawalRequest.prototype.address;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.id;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.transactionHash;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.memo;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.coinId;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.decimalAmount;
    /** @type {?} */
    SavedWithdrawalRequest.prototype.errorMessage;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.isEosAccount;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.eosAccountName;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.easyAccountId;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.userId;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.amount;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.account;
    /**
     * @type {?}
     * @private
     */
    SavedWithdrawalRequest.prototype.amountFactory;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/currency-amount/precise-math.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 */
class PreciseMath {
    /**
     * @param {?} startingValue
     * @param {?} factory
     */
    constructor(startingValue, factory) {
        this.startingValue = startingValue;
        this.factory = factory;
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    addDecimal(decimal) {
        /** @type {?} */
        const other = this.factory.newAmountFromDecimal(decimal, this.precision);
        return this.add(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    add(other) {
        this.validator.isMatchingType(other);
        /** @type {?} */
        const newInteger = this.startingValue.bigInteger.plus(other.integer);
        /** @type {?} */
        const newNumber = this.factory.newAmountFromInteger(newInteger, this.precision);
        return new PreciseMath(newNumber, this.factory);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    subtractDecimal(decimal) {
        /** @type {?} */
        const other = this.factory.newAmountFromDecimal(decimal, this.precision);
        return this.subtract(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    subtract(other) {
        this.validator.isMatchingType(other);
        /** @type {?} */
        const newInteger = this.startingValue.bigInteger.minus(other.integer);
        /** @type {?} */
        const newNumber = this.factory.newAmountFromInteger(newInteger, this.precision);
        return new PreciseMath(newNumber, this.factory);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    divideDecimal(decimal) {
        return this.multiplyDecimal(new Big(1).div(decimal));
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    multiplyDecimal(decimal) {
        /** @type {?} */
        const product = new Big(this.startingValue.decimal).times(decimal);
        /** @type {?} */
        const newInteger = product
            .times(this.factor)
            .round(0, 0 /* RoundDown */);
        /** @type {?} */
        const newNumber = this.factory.newAmountFromInteger(newInteger, this.precision);
        return new PreciseMath(newNumber, this.factory);
    }
    /**
     * @return {?}
     */
    absoluteValue() {
        /** @type {?} */
        const newNumber = this.factory.newAmountFromInteger(new Big(this.bigInteger).abs(), this.precision);
        return new PreciseMath(newNumber, this.factory);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isLessThan(other) {
        return this.number.isLessThan(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isLessThanDecimal(other) {
        return this.number.isLessThanDecimal(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isGreaterThan(other) {
        return this.number.isGreaterThan(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isGreaterThanDecimal(other) {
        return this.number.isGreaterThanDecimal(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isEqualTo(other) {
        return this.number.isEqualTo(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isEqualToDecimal(other) {
        return this.number.isEqualToDecimal(other);
    }
    /**
     * @return {?}
     */
    get decimal() {
        return this.number.decimal;
    }
    /**
     * @return {?}
     */
    get integer() {
        return this.number.integer;
    }
    /**
     * @return {?}
     */
    get isZero() {
        return this.number.isZero;
    }
    /**
     * @return {?}
     */
    get isPositive() {
        return this.number.isPositive;
    }
    /**
     * @return {?}
     */
    get isNegative() {
        return this.number.isNegative;
    }
    /**
     * @return {?}
     */
    get bigInteger() {
        return this.number.bigInteger;
    }
    /**
     * @return {?}
     */
    get precision() {
        return this.number.precision;
    }
    /**
     * @return {?}
     */
    get factor() {
        return this.number.factor;
    }
    /**
     * @return {?}
     */
    get number() {
        return this.startingValue;
    }
    /**
     * @private
     * @return {?}
     */
    get validator() {
        return this.factory.validator;
    }
}
if (false) {
    /** @type {?} */
    PreciseMath.prototype.startingValue;
    /** @type {?} */
    PreciseMath.prototype.factory;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/currency-amount/precise-math-numbers.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PreciseNumber {
    /**
     * @param {?} precision
     * @param {?=} integerValue
     */
    constructor(precision, integerValue = 0) {
        this.precision = precision;
        this.factor = new Big(Math.pow(10, precision));
        this.bigInteger = new Big(integerValue);
        Big.RM = 0 /* RoundDown */;
        this.decimal = this.bigInteger.div(this.factor)
            .toFixed(precision);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isLessThan(other) {
        this.validateMatchingCurrency(other);
        return this.bigInteger.lt(other.integer);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isLessThanDecimal(other) {
        return new Big(this.decimal).lt(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isGreaterThan(other) {
        this.validateMatchingCurrency(other);
        return this.bigInteger.gt(other.integer);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isGreaterThanDecimal(other) {
        return new Big(this.decimal).gt(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isEqualTo(other) {
        this.validateMatchingCurrency(other);
        return this.bigInteger.eq(other.integer);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isEqualToDecimal(other) {
        return new Big(this.decimal).eq(other);
    }
    /**
     * @protected
     * @param {?} other
     * @return {?}
     */
    validateMatchingCurrency(other) {
        if (this.precision != other.precision) {
            throw new Error('both amounts must be the same precision!');
        }
    }
    /**
     * @return {?}
     */
    get integer() {
        return this.bigInteger.toFixed(0);
    }
    /**
     * @return {?}
     */
    get isZero() {
        return this.bigInteger.eq(0);
    }
    /**
     * @return {?}
     */
    get isPositive() {
        return this.bigInteger.gt(0);
    }
    /**
     * @return {?}
     */
    get isNegative() {
        return this.bigInteger.lt(0);
    }
}
if (false) {
    /** @type {?} */
    PreciseNumber.prototype.factor;
    /** @type {?} */
    PreciseNumber.prototype.bigInteger;
    /** @type {?} */
    PreciseNumber.prototype.decimal;
    /** @type {?} */
    PreciseNumber.prototype.precision;
}
class PreciseDecimal extends PreciseNumber {
    /**
     * @param {?} decimalValue
     * @param {?} precision
     */
    constructor(decimalValue, precision) {
        /** @type {?} */
        const factor = new Big(Math.pow(10, precision));
        /** @type {?} */
        const decimal = new Big(decimalValue);
        super(precision, decimal
            .times(factor)
            .round(0, 0 /* RoundDown */));
    }
}
class MatchingPrecisionValidator {
    /**
     * @param {?} precision
     */
    constructor(precision) {
        this.precision = precision;
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isMatchingType(other) {
        if (this.precision != other.precision) {
            throw new Error('both amounts must be the same precision!');
        }
        else {
            return true;
        }
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MatchingPrecisionValidator.prototype.precision;
}
class PreciseNumberFactory {
    /**
     * @param {?} precision
     */
    constructor(precision) {
        this.validator = new MatchingPrecisionValidator(precision);
    }
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    newAmountFromInteger(integer, precision) {
        /** @type {?} */
        const number = new PreciseNumber(precision, integer);
        this.validator.isMatchingType(number);
        return number;
    }
    /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    newAmountFromDecimal(decimal, precision) {
        /** @type {?} */
        const number = new PreciseDecimal(decimal, precision);
        this.validator.isMatchingType(number);
        return number;
    }
}
if (false) {
    /** @type {?} */
    PreciseNumberFactory.prototype.validator;
}
/**
 * @template T
 */
class NumberForPreciseMathBase {
    /**
     * @param {?} precision
     * @param {?} integerValue
     * @param {?} factory
     */
    constructor(precision, integerValue, factory) {
        this.factory = factory;
        /** @type {?} */
        const number = factory.newAmountFromInteger(integerValue, precision);
        this.math = new PreciseMath(number, factory);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    addDecimal(decimal) {
        return this.math.addDecimal(decimal);
    }
    /**
     * @param {?} amount
     * @return {?}
     */
    add(amount) {
        return this.math.add(amount);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    subtractDecimal(decimal) {
        return this.math.subtractDecimal(decimal);
    }
    /**
     * @param {?} amount
     * @return {?}
     */
    subtract(amount) {
        return this.math.subtract(amount);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    multiplyDecimal(decimal) {
        return this.math.multiplyDecimal(decimal);
    }
    /**
     * @param {?} decimal
     * @return {?}
     */
    divideDecimal(decimal) {
        return this.math.divideDecimal(decimal);
    }
    /**
     * @return {?}
     */
    absoluteValue() {
        return this.math.absoluteValue();
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isLessThan(other) {
        return this.number.isLessThan(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isLessThanDecimal(other) {
        return this.number.isLessThanDecimal(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isGreaterThan(other) {
        return this.number.isGreaterThan(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isGreaterThanDecimal(other) {
        return this.number.isGreaterThanDecimal(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isEqualTo(other) {
        return this.number.isEqualTo(other);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    isEqualToDecimal(other) {
        return this.number.isEqualToDecimal(other);
    }
    /**
     * @return {?}
     */
    get decimal() {
        return this.number.decimal;
    }
    /**
     * @return {?}
     */
    get integer() {
        return this.number.integer;
    }
    /**
     * @return {?}
     */
    get isZero() {
        return this.number.isZero;
    }
    /**
     * @return {?}
     */
    get isPositive() {
        return this.number.isPositive;
    }
    /**
     * @return {?}
     */
    get isNegative() {
        return this.number.isNegative;
    }
    /**
     * @return {?}
     */
    get bigInteger() {
        return this.number.bigInteger;
    }
    /**
     * @return {?}
     */
    get precision() {
        return this.number.precision;
    }
    /**
     * @return {?}
     */
    get factor() {
        return this.number.factor;
    }
    /**
     * @return {?}
     */
    get number() {
        return this.startingValue;
    }
    /**
     * @return {?}
     */
    get startingValue() {
        return this.math.startingValue;
    }
}
if (false) {
    /** @type {?} */
    NumberForPreciseMathBase.prototype.math;
    /** @type {?} */
    NumberForPreciseMathBase.prototype.factory;
}
class NumberForPreciseMath extends NumberForPreciseMathBase {
    /**
     * @param {?} precision
     * @param {?=} integerValue
     */
    constructor(precision, integerValue = 0) {
        super(precision, integerValue, new PreciseNumberFactory(precision));
    }
}
class DecimalForPreciseMath extends NumberForPreciseMath {
    /**
     * @param {?} decimalValue
     * @param {?} precision
     */
    constructor(decimalValue, precision) {
        /** @type {?} */
        const factor = new Big(Math.pow(10, precision));
        /** @type {?} */
        const decimal = new Big(decimalValue);
        super(precision, decimal
            .times(factor)
            .round(0, 0 /* RoundDown */));
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/models/coin.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function ISavedCoinRow() { }
if (false) {
    /** @type {?} */
    ISavedCoinRow.prototype.id;
    /** @type {?} */
    ISavedCoinRow.prototype.symbol;
    /** @type {?} */
    ISavedCoinRow.prototype.precision;
    /** @type {?} */
    ISavedCoinRow.prototype.uses_memo_for_deposits;
    /** @type {?} */
    ISavedCoinRow.prototype.minimum_withdrawal_amount;
}
class Coin {
    /**
     * @param {?} data
     */
    constructor(data) {
        this.id = data.id;
        this.symbol = data.symbol;
        this.precision = data.precision;
        this.usesMemoForDeposits = data.uses_memo_for_deposits == 1;
        this.minimumWithdrawalAmount = new PreciseDecimal(data.minimum_withdrawal_amount, this.precision);
    }
    /**
     * @return {?}
     */
    get data() {
        return {
            id: this.id,
            symbol: this.symbol,
            precision: this.precision,
            usesMemoForDeposits: this.usesMemoForDeposits,
            minimumWithdrawalAmount: this.minimumWithdrawalAmount.decimal
        };
    }
}
if (false) {
    /** @type {?} */
    Coin.prototype.id;
    /** @type {?} */
    Coin.prototype.symbol;
    /** @type {?} */
    Coin.prototype.precision;
    /** @type {?} */
    Coin.prototype.usesMemoForDeposits;
    /** @type {?} */
    Coin.prototype.minimumWithdrawalAmount;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/models/account.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function ISavedAccountRow() { }
if (false) {
    /** @type {?} */
    ISavedAccountRow.prototype.id;
    /** @type {?} */
    ISavedAccountRow.prototype.eos_account;
    /** @type {?} */
    ISavedAccountRow.prototype.ezeos_id;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/repositories/account-repository.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const FieldNames = {
    ID: 'id',
    EOS_ACCOUNT: 'eos_account',
    EZEOS_ID: 'ezeos_id',
};
class AccountRepository extends MySQLRepository {
    /**
     * @param {?} db
     */
    constructor(db) {
        super(db, 'account', [
            FieldNames.ID,
            FieldNames.EOS_ACCOUNT,
            FieldNames.EZEOS_ID
        ]);
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IUserAccountRow() { }
if (false) {
    /** @type {?} */
    IUserAccountRow.prototype.eos_account_name;
    /** @type {?} */
    IUserAccountRow.prototype.easy_account_public_key;
}
/**
 * @record
 */
function IUserAccountDatabaseService() { }
if (false) {
    /**
     * @param {?} userId
     * @return {?}
     */
    IUserAccountDatabaseService.prototype.getAccountInfo = function (userId) { };
    /**
     * @param {?} eos_account
     * @return {?}
     */
    IUserAccountDatabaseService.prototype.getUserIdForEosAccount = function (eos_account) { };
    /**
     * @param {?} ezeos_id
     * @return {?}
     */
    IUserAccountDatabaseService.prototype.getUserIdForEasyAccount = function (ezeos_id) { };
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/user-account-database-service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UserAccountDatabaseService {
    /**
     * @param {?} database
     */
    constructor(database) {
        this.database = database;
        this.accountRepository = new AccountRepository(this.database);
    }
    /**
     * @param {?} userId
     * @return {?}
     */
    getAccountInfo(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const rows = yield this.database.builder.rawQuery(`
            SELECT
                eos_account AS eos_account_name,
                public_key AS easy_account_public_key

            FROM
                account
                LEFT JOIN account_ezeos ON account.id = account_id

            WHERE
                account.id = ${userId};
        `).execute();
            return rows[0];
        });
    }
    /**
     * @param {?} eos_account
     * @return {?}
     */
    getUserIdForEosAccount(eos_account) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const row = yield this.accountRepository.selectOne({
                whereMap: { eos_account },
            });
            if (row) {
                return row.id;
            }
            else {
                const { insertId } = yield this.accountRepository.insert({ eos_account });
                return insertId;
            }
        });
    }
    /**
     * @param {?} ezeos_id
     * @return {?}
     */
    getUserIdForEasyAccount(ezeos_id) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const rows = yield this.accountRepository.select({
                whereMap: { ezeos_id },
            });
            if (rows.length == 0) {
                throw new Error('User ID NOT FOUND for Easy Account ID: ' + ezeos_id);
            }
            return rows[0].id;
        });
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    UserAccountDatabaseService.prototype.accountRepository;
    /**
     * @type {?}
     * @protected
     */
    UserAccountDatabaseService.prototype.database;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/coin-data-provider.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CoinDataProvider {
    /**
     * @param {?} database
     */
    constructor(database) {
        this.database = database;
        this.isInit = false;
        this.ids = {};
        this.data = {};
        this.allCoins = [];
    }
    /**
     * @return {?}
     */
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            this.allCoins = (yield this.database.getAllCoins()).map((/**
             * @param {?} row
             * @return {?}
             */
            row => new Coin(row)));
            for (const row of this.allCoins) {
                // map symbol to ID
                this.ids[row.symbol] = row.id;
                // map ID to data
                this.data[row.id] = row;
            }
            this.isInit = true;
        });
    }
    /**
     * @param {?} symbol
     * @return {?}
     */
    getCoinDataBySymbol(symbol) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const id = yield this.getCoinId(symbol);
            return this.getCoinData(id);
        });
    }
    /**
     * @param {?} symbol
     * @return {?}
     */
    getCoinId(symbol) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.isInit) {
                throw new Error('CoinDataProvider is NOT Initialized!');
            }
            symbol = symbol.toUpperCase();
            /** @type {?} */
            const id = this.ids[symbol];
            if (id === undefined) {
                throw new Error('CoinId NOT FOUND: ' + symbol);
            }
            return id;
        });
    }
    /**
     * @param {?} coinId
     * @return {?}
     */
    getCoinSymbol(coinId) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.getCoinData(coinId);
            return data.symbol;
        });
    }
    /**
     * @param {?} coinId
     * @return {?}
     */
    getCoinData(coinId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.isInit) {
                throw new Error('CoinDataProvider is NOT Initialized!');
            }
            /** @type {?} */
            const data = this.data[coinId];
            if (!data) {
                throw new Error('Coin NOT FOUND: ' + coinId);
            }
            return data;
        });
    }
    /**
     * @return {?}
     */
    getAllCoins() {
        return this.allCoins.slice();
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.isInit;
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.ids;
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.data;
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.allCoins;
    /**
     * @type {?}
     * @private
     */
    CoinDataProvider.prototype.database;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/models/common.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function ISavedEntity() { }
if (false) {
    /** @type {?} */
    ISavedEntity.prototype.id;
}
/** @type {?} */
const CommonFieldNames = {
    ID: 'id',
    CREATED_AT: 'created_at',
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/repositories/coin-repository.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const FieldNames$1 = {
    SYMBOL: 'symbol',
    PRECISION: 'precision',
    USES_MEMO_FOR_DEPOSITS: 'uses_memo_for_deposits',
    MINIMUM_WITHDRAWAL_AMOUNT: 'minimum_withdrawal_amount'
};
class CoinRepository extends MySQLRepository {
    /**
     * @param {?} database
     */
    constructor(database) {
        super(database, 'coin', [
            CommonFieldNames.ID,
            FieldNames$1.SYMBOL,
            FieldNames$1.PRECISION,
            FieldNames$1.USES_MEMO_FOR_DEPOSITS,
            FieldNames$1.MINIMUM_WITHDRAWAL_AMOUNT
        ]);
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/database/coin-database-service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CoinDatabaseService {
    /**
     * @param {?} database
     */
    constructor(database) {
        this.coins = new CoinRepository(database);
    }
    /**
     * @return {?}
     */
    getAllCoins() {
        return this.coins.select({
            orderBy: 'id'
        });
    }
    /**
     * @param {?} coinId
     * @return {?}
     */
    getCoinData(coinId) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const result = yield this.coins.selectOneByPrimaryKey(coinId);
            return result;
        });
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    CoinDatabaseService.prototype.coins;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/services/withdrawal-contract-service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 */
class WithdrawalContractService {
    /**
     * @param {?} eosUtility
     */
    constructor(eosUtility) {
        this.eosUtility = eosUtility;
        this.contractName = this.contracts.withdrawal;
    }
    /**
     * @return {?}
     */
    getPendingRequests() {
        return this.eosUtility.getTableRows({
            code: this.contractName,
            scope: this.contractName,
            table: 'withdraws',
            limit: 1000
        });
    }
    /**
     * @param {?} requestId
     * @param {?} txnId
     * @return {?}
     */
    processRequest(requestId, txnId) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const executor = new EosTransactionExecutor([{
                    account: this.contractName,
                    name: 'process',
                    data: {
                        withdraw_id: requestId,
                        memo: txnId
                    }
                }], this.eosUtility, isWaxTxnIrreversible);
            /**
             * save this in DB? **
             * @type {?}
             */
            const broadcastedTxnId = yield executor.execute();
            /** @type {?} */
            const confirmedTxnId = yield executor.confirm();
            /*** save this in DB? ***/
            return confirmedTxnId;
        });
    }
    /**
     * @param {?} request
     * @return {?}
     */
    refundRequest(request) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const params = yield request.getRefundParams();
            const { requestId, easyAccountPublicKey, reason } = params;
            /** @type {?} */
            const data = {
                withdraw_id: requestId,
                ezacct_deposit_memo: easyAccountPublicKey,
                memo: reason
            };
            /** @type {?} */
            const executor = new EosTransactionExecutor([{
                    account: this.contractName,
                    name: 'refund',
                    data
                }], this.eosUtility, isWaxTxnIrreversible);
            /**
             * save this in DB? **
             * @type {?}
             */
            const broadcastedTxnId = yield executor.execute();
            /** @type {?} */
            const confirmedTxnId = yield executor.confirm();
            /*** save this in DB? ***/
            return confirmedTxnId;
        });
    }
    /**
     * @private
     * @return {?}
     */
    get contracts() {
        return this.eosUtility.contracts;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    WithdrawalContractService.prototype.contractName;
    /**
     * @type {?}
     * @private
     */
    WithdrawalContractService.prototype.eosUtility;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/services/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IWithdrawalRequestData() { }
if (false) {
    /** @type {?} */
    IWithdrawalRequestData.prototype.user;
    /** @type {?} */
    IWithdrawalRequestData.prototype.tx_hash;
    /** @type {?} */
    IWithdrawalRequestData.prototype.extra_data;
    /** @type {?} */
    IWithdrawalRequestData.prototype.withdraw_id;
    /** @type {?} */
    IWithdrawalRequestData.prototype.withdraw_memo;
    /** @type {?} */
    IWithdrawalRequestData.prototype.withdraw_address;
    /** @type {?} */
    IWithdrawalRequestData.prototype.withdraw_quantity;
}
/**
 * @record
 */
function IRefundWithdrawalRequestParams() { }
if (false) {
    /** @type {?} */
    IRefundWithdrawalRequestParams.prototype.requestId;
    /** @type {?} */
    IRefundWithdrawalRequestParams.prototype.easyAccountPublicKey;
    /** @type {?} */
    IRefundWithdrawalRequestParams.prototype.reason;
}
/**
 * @record
 */
function IWithdrawalContractService() { }
if (false) {
    /**
     * @return {?}
     */
    IWithdrawalContractService.prototype.getPendingRequests = function () { };
    /**
     * @param {?} requestId
     * @param {?} txnId
     * @return {?}
     */
    IWithdrawalContractService.prototype.processRequest = function (requestId, txnId) { };
    /**
     * @param {?} request
     * @return {?}
     */
    IWithdrawalContractService.prototype.refundRequest = function (request) { };
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/currency-amount/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function ICurrency() { }
if (false) {
    /** @type {?} */
    ICurrency.prototype.id;
    /** @type {?} */
    ICurrency.prototype.symbol;
    /** @type {?} */
    ICurrency.prototype.precision;
}
/**
 * @record
 */
function IPreciseNumber() { }
if (false) {
    /** @type {?} */
    IPreciseNumber.prototype.precision;
    /** @type {?} */
    IPreciseNumber.prototype.decimal;
    /** @type {?} */
    IPreciseNumber.prototype.integer;
    /** @type {?} */
    IPreciseNumber.prototype.isZero;
    /** @type {?} */
    IPreciseNumber.prototype.isPositive;
    /** @type {?} */
    IPreciseNumber.prototype.isNegative;
    /** @type {?} */
    IPreciseNumber.prototype.bigInteger;
    /** @type {?} */
    IPreciseNumber.prototype.factor;
    /**
     * @param {?} other
     * @return {?}
     */
    IPreciseNumber.prototype.isLessThan = function (other) { };
    /**
     * @param {?} other
     * @return {?}
     */
    IPreciseNumber.prototype.isLessThanDecimal = function (other) { };
    /**
     * @param {?} other
     * @return {?}
     */
    IPreciseNumber.prototype.isGreaterThan = function (other) { };
    /**
     * @param {?} other
     * @return {?}
     */
    IPreciseNumber.prototype.isGreaterThanDecimal = function (other) { };
    /**
     * @param {?} other
     * @return {?}
     */
    IPreciseNumber.prototype.isEqualTo = function (other) { };
    /**
     * @param {?} other
     * @return {?}
     */
    IPreciseNumber.prototype.isEqualToDecimal = function (other) { };
}
/**
 * @record
 * @template T
 */
function IMatchingNumberTypeValidator() { }
if (false) {
    /**
     * @param {?} other
     * @return {?}
     */
    IMatchingNumberTypeValidator.prototype.isMatchingType = function (other) { };
}
/**
 * @record
 * @template T
 */
function IPreciseNumberFactory() { }
if (false) {
    /** @type {?} */
    IPreciseNumberFactory.prototype.validator;
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    IPreciseNumberFactory.prototype.newAmountFromInteger = function (integer, precision) { };
    /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    IPreciseNumberFactory.prototype.newAmountFromDecimal = function (decimal, precision) { };
}
/**
 * @record
 * @template T
 */
function IPreciseMath() { }
if (false) {
    /** @type {?} */
    IPreciseMath.prototype.startingValue;
    /** @type {?} */
    IPreciseMath.prototype.factory;
    /**
     * @param {?} decimal
     * @return {?}
     */
    IPreciseMath.prototype.addDecimal = function (decimal) { };
    /**
     * @param {?} amount
     * @return {?}
     */
    IPreciseMath.prototype.add = function (amount) { };
    /**
     * @param {?} decimal
     * @return {?}
     */
    IPreciseMath.prototype.subtractDecimal = function (decimal) { };
    /**
     * @param {?} amount
     * @return {?}
     */
    IPreciseMath.prototype.subtract = function (amount) { };
    /**
     * @param {?} decimal
     * @return {?}
     */
    IPreciseMath.prototype.multiplyDecimal = function (decimal) { };
    /**
     * @param {?} decimal
     * @return {?}
     */
    IPreciseMath.prototype.divideDecimal = function (decimal) { };
    /**
     * @return {?}
     */
    IPreciseMath.prototype.absoluteValue = function () { };
}
/**
 * @record
 */
function IPreciseCurrencyAmount() { }
if (false) {
    /** @type {?} */
    IPreciseCurrencyAmount.prototype.currency;
    /** @type {?} */
    IPreciseCurrencyAmount.prototype.quantity;
}
/**
 * @record
 */
function IPreciseCurrencyAmountWithPrice() { }
if (false) {
    /** @type {?} */
    IPreciseCurrencyAmountWithPrice.prototype.priceInUSD;
    /** @type {?} */
    IPreciseCurrencyAmountWithPrice.prototype.amountInUSD;
}
/**
 * @record
 */
function ICurrencyAmount() { }
if (false) {
    /** @type {?} */
    ICurrencyAmount.prototype.math;
}
/**
 * @record
 */
function ICurrencyAmountWithPrice() { }
if (false) {
    /** @type {?} */
    ICurrencyAmountWithPrice.prototype.math;
}
/**
 * @record
 */
function ICurrencyAmountFactory() { }
if (false) {
    /** @type {?} */
    ICurrencyAmountFactory.prototype.coinDataProvider;
    /**
     * @param {?} quantity
     * @return {?}
     */
    ICurrencyAmountFactory.prototype.newAmountFromQuantity = function (quantity) { };
    /**
     * @param {?} decimalAmount
     * @param {?} tokenSymbol
     * @return {?}
     */
    ICurrencyAmountFactory.prototype.newAmountFromDecimal = function (decimalAmount, tokenSymbol) { };
    /**
     * @param {?} decimalAmount
     * @param {?} coinId
     * @return {?}
     */
    ICurrencyAmountFactory.prototype.newAmountFromDecimalAndCoinId = function (decimalAmount, coinId) { };
    /**
     * @param {?} integerSubunits
     * @param {?} tokenSymbol
     * @return {?}
     */
    ICurrencyAmountFactory.prototype.newAmountFromInteger = function (integerSubunits, tokenSymbol) { };
}
/**
 * @record
 */
function ICurrencyAmountWithPriceFactory() { }
if (false) {
    /** @type {?} */
    ICurrencyAmountWithPriceFactory.prototype.priceService;
    /**
     * @param {?} quantity
     * @return {?}
     */
    ICurrencyAmountWithPriceFactory.prototype.newAmountFromQuantity = function (quantity) { };
    /**
     * @param {?} decimalAmount
     * @param {?} tokenSymbol
     * @return {?}
     */
    ICurrencyAmountWithPriceFactory.prototype.newAmountFromDecimal = function (decimalAmount, tokenSymbol) { };
    /**
     * @param {?} decimalAmount
     * @param {?} coinId
     * @return {?}
     */
    ICurrencyAmountWithPriceFactory.prototype.newAmountFromDecimalAndCoinId = function (decimalAmount, coinId) { };
    /**
     * @param {?} integerSubunits
     * @param {?} tokenSymbol
     * @return {?}
     */
    ICurrencyAmountWithPriceFactory.prototype.newAmountFromInteger = function (integerSubunits, tokenSymbol) { };
}
/**
 * @record
 */
function ICurrencyPriceService() { }
if (false) {
    /**
     * @param {?} currencySymbol
     * @return {?}
     */
    ICurrencyPriceService.prototype.getPriceInUSD = function (currencySymbol) { };
}
/**
 * @record
 */
function ICoinDatabaseService() { }
if (false) {
    /**
     * @return {?}
     */
    ICoinDatabaseService.prototype.getAllCoins = function () { };
}
/**
 * @record
 */
function ICoinDataProvider() { }
if (false) {
    /**
     * @param {?} symbol
     * @return {?}
     */
    ICoinDataProvider.prototype.getCoinId = function (symbol) { };
    /**
     * @param {?} coinId
     * @return {?}
     */
    ICoinDataProvider.prototype.getCoinSymbol = function (coinId) { };
    /**
     * @param {?} coinId
     * @return {?}
     */
    ICoinDataProvider.prototype.getCoinData = function (coinId) { };
    /**
     * @param {?} symbol
     * @return {?}
     */
    ICoinDataProvider.prototype.getCoinDataBySymbol = function (symbol) { };
    /**
     * @return {?}
     */
    ICoinDataProvider.prototype.getAllCoins = function () { };
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/http-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 * @param {?} url
 * @return {?}
 */
function httpsGetJson(url) {
    return new Promise((/**
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    (resolve, reject) => {
        get('https://' + url, (/**
         * @param {?} resp
         * @return {?}
         */
        (resp) => {
            /** @type {?} */
            let data = '';
            // A chunk of data has been recieved.
            resp.on('data', (/**
             * @param {?} chunk
             * @return {?}
             */
            (chunk) => {
                data += chunk;
            }));
            // The whole response has been received. Print out the result.
            resp.on('end', (/**
             * @return {?}
             */
            () => {
                try {
                    /** @type {?} */
                    const parsedData = JSON.parse(data);
                    resolve(parsedData);
                }
                catch (error) {
                    console.log(data);
                    reject(error);
                }
            }));
        })).on('error', (/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
            // console.log("Error: " + err.message);
            // throw err;
            reject(err);
        }));
    }));
}
/**
 * @template T
 * @param {?} url
 * @return {?}
 */
function httpGetJson(url) {
    return new Promise((/**
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    (resolve, reject) => {
        get$1('http://' + url, (/**
         * @param {?} resp
         * @return {?}
         */
        (resp) => {
            /** @type {?} */
            let data = '';
            // A chunk of data has been recieved.
            resp.on('data', (/**
             * @param {?} chunk
             * @return {?}
             */
            (chunk) => {
                data += chunk;
            }));
            // The whole response has been received. Print out the result.
            resp.on('end', (/**
             * @return {?}
             */
            () => {
                try {
                    /** @type {?} */
                    const parsedData = JSON.parse(data);
                    resolve(parsedData);
                }
                catch (error) {
                    console.log(data);
                    reject(error);
                }
            }));
        })).on('error', (/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
            // console.log("Error: " + err.message);
            // throw err;
            reject(err);
        }));
    }));
}
/**
 * @param {?} url
 * @return {?}
 */
function httpGet(url) {
    return new Promise((/**
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    (resolve, reject) => {
        get$1('http://' + url, (/**
         * @param {?} resp
         * @return {?}
         */
        (resp) => {
            /** @type {?} */
            let data = '';
            // A chunk of data has been recieved.
            resp.on('data', (/**
             * @param {?} chunk
             * @return {?}
             */
            (chunk) => {
                data += chunk;
            }));
            // The whole response has been received. Print out the result.
            resp.on('end', (/**
             * @return {?}
             */
            () => {
                // console.log(data);
                resolve(data);
            }));
        })).on('error', (/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
            // console.log("Error: " + err.message);
            // throw err;
            reject(err);
        }));
    }));
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/coin-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CoinPriceService {
    constructor() {
        this.symbols = [];
        this.prices = {};
        this.updateCoinPrices = (/**
         * @return {?}
         */
        () => __awaiter(this, void 0, void 0, function* () {
            for (let symbol of this.symbols) {
                symbol = symbol.split('_')[0];
                /** @type {?} */
                const price = yield fetchPriceOfCoin(symbol);
                if (price != undefined) {
                    this.prices[symbol] = price;
                }
            }
        }));
    }
    /**
     * @param {?} database
     * @return {?}
     */
    init(database) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const coins = yield database.getAllCoins();
            this.symbols = coins.map((/**
             * @param {?} row
             * @return {?}
             */
            row => row.symbol));
            setInterval(this.updateCoinPrices, 1000 * 60 * 60);
            yield this.updateCoinPrices();
        });
    }
    /**
     * @param {?} symbol
     * @return {?}
     */
    getPriceInUSD(symbol) {
        return __awaiter(this, void 0, void 0, function* () {
            symbol = symbol.split('_')[0];
            if (symbol == 'FUN') {
                return 0;
            }
            if (symbol == 'USD') {
                return 1;
            }
            /** @type {?} */
            const price = this.prices[symbol.toUpperCase()];
            if (price == undefined) {
                console.log(this.prices);
                throw new Error(symbol + ' Price is UNDEFINED!');
            }
            return price;
        });
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    CoinPriceService.prototype.symbols;
    /**
     * @type {?}
     * @private
     */
    CoinPriceService.prototype.prices;
    /**
     * @type {?}
     * @private
     */
    CoinPriceService.prototype.updateCoinPrices;
}
/** @type {?} */
let coinPriceService;
/**
 * @param {?} database
 * @return {?}
 * @this {*}
 */
function getCoinPriceService(database) {
    return __awaiter(this, void 0, void 0, function* () {
        if (coinPriceService == undefined) {
            coinPriceService = new CoinPriceService();
            yield coinPriceService.init(database);
        }
        return coinPriceService;
    });
}
/**
 * @record
 */
function ICoinPriceData() { }
if (false) {
    /** @type {?} */
    ICoinPriceData.prototype.price;
}
/**
 * @param {?} symbol
 * @return {?}
 * @this {*}
 */
function fetchPriceOfCoin(symbol) {
    return __awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const url = 'internal.eosbet.io/token_price/' +
            symbol.toUpperCase();
        try {
            /** @type {?} */
            const data = yield httpGetJson(url);
            // console.log(data);
            return Number(data.price);
        }
        catch (error) {
            console.error(error);
            return undefined;
        }
    });
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/currency/currency-amount/currency-amount-factory.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PreciseCurrencyAmount extends PreciseDecimal {
    /**
     * @param {?} decimalValue
     * @param {?} currency
     */
    constructor(decimalValue, currency) {
        super(decimalValue, currency.precision);
        this.currency = currency;
    }
    /**
     * @return {?}
     */
    get quantity() {
        return this.decimal + ' ' + this.currency.symbol;
    }
}
if (false) {
    /** @type {?} */
    PreciseCurrencyAmount.prototype.currency;
}
class PreciseCurrencyAmountWithPrice extends PreciseCurrencyAmount {
    /**
     * @param {?} decimalValue
     * @param {?} currency
     * @param {?} priceInUSD
     */
    constructor(decimalValue, currency, priceInUSD) {
        super(decimalValue, currency);
        this.priceInUSD = priceInUSD;
    }
    /**
     * @return {?}
     */
    get amountInUSD() {
        this.checkPrice();
        return (new Big(this.decimal))
            .times(this.priceInUSD)
            .round(6, 0 /* RoundDown */)
            .toString();
    }
    /**
     * @private
     * @return {?}
     */
    checkPrice() {
        if (this.priceInUSD == undefined) {
            throw new Error('Price for Currency IS NOT DEFINED!');
        }
    }
}
if (false) {
    /** @type {?} */
    PreciseCurrencyAmountWithPrice.prototype.priceInUSD;
}
class MatchingCurrencyValidator {
    /**
     * @param {?} currency
     */
    constructor(currency) {
        this.currency = currency;
    }
    /**
     * @param {?} __0
     * @return {?}
     */
    isMatchingType({ currency }) {
        if (this.currency.symbol != currency.symbol ||
            this.currency.precision != currency.precision) {
            throw new Error('both amounts must be the same currency!');
        }
        else {
            return true;
        }
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MatchingCurrencyValidator.prototype.currency;
}
class PreciseCurrencyAmountFactory {
    /**
     * @param {?} currency
     */
    constructor(currency) {
        this.currency = currency;
        this.validator = new MatchingCurrencyValidator(currency);
    }
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    newAmountFromInteger(integer, precision) {
        /** @type {?} */
        const number = new PreciseCurrencyAmount(new Big(integer).div(Math.pow(10, precision)), this.currency);
        this.validator.isMatchingType(number);
        return number;
    }
    /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    newAmountFromDecimal(decimal, precision) {
        /** @type {?} */
        const number = new PreciseCurrencyAmount(decimal, this.currency);
        this.validator.isMatchingType(number);
        return number;
    }
}
if (false) {
    /** @type {?} */
    PreciseCurrencyAmountFactory.prototype.validator;
    /**
     * @type {?}
     * @private
     */
    PreciseCurrencyAmountFactory.prototype.currency;
}
class PreciseCurrencyAmountWithPriceFactory {
    /**
     * @param {?} currency
     * @param {?} priceInUSD
     */
    constructor(currency, priceInUSD) {
        this.currency = currency;
        this.priceInUSD = priceInUSD;
        this.validator = new MatchingCurrencyValidator(currency);
    }
    /**
     * @param {?} integer
     * @param {?} precision
     * @return {?}
     */
    newAmountFromInteger(integer, precision) {
        /** @type {?} */
        const number = new PreciseCurrencyAmountWithPrice(new Big(integer).div(Math.pow(10, precision)), this.currency, this.priceInUSD);
        this.validator.isMatchingType(number);
        return number;
    }
    /**
     * @param {?} decimal
     * @param {?} precision
     * @return {?}
     */
    newAmountFromDecimal(decimal, precision) {
        /** @type {?} */
        const number = new PreciseCurrencyAmountWithPrice(decimal, this.currency, this.priceInUSD);
        this.validator.isMatchingType(number);
        return number;
    }
}
if (false) {
    /** @type {?} */
    PreciseCurrencyAmountWithPriceFactory.prototype.validator;
    /**
     * @type {?}
     * @private
     */
    PreciseCurrencyAmountWithPriceFactory.prototype.currency;
    /**
     * @type {?}
     * @private
     */
    PreciseCurrencyAmountWithPriceFactory.prototype.priceInUSD;
}
class CurrencyAmount extends NumberForPreciseMathBase {
    /**
     * @param {?} decimalValue
     * @param {?} currency
     */
    constructor(decimalValue, currency) {
        super(currency.precision, new Big(decimalValue).times(Math.pow(10, currency.precision)).round(0, 0 /* RoundDown */), new PreciseCurrencyAmountFactory(currency));
        this.currency = currency;
    }
    /**
     * @return {?}
     */
    get quantity() {
        return this.number.quantity;
    }
}
if (false) {
    /** @type {?} */
    CurrencyAmount.prototype.currency;
}
class CurrencyAmountWithPrice extends NumberForPreciseMathBase {
    /**
     * @param {?} decimalValue
     * @param {?} currency
     * @param {?} priceInUSD
     */
    constructor(decimalValue, currency, priceInUSD) {
        super(currency.precision, new Big(decimalValue).times(Math.pow(10, currency.precision)).round(0, 0 /* RoundDown */), new PreciseCurrencyAmountWithPriceFactory(currency, priceInUSD));
        this.currency = currency;
    }
    /**
     * @return {?}
     */
    get amountInUSD() {
        return this.number.amountInUSD;
    }
    /**
     * @return {?}
     */
    get priceInUSD() {
        return this.number.priceInUSD;
    }
    /**
     * @return {?}
     */
    get quantity() {
        return this.number.quantity;
    }
}
if (false) {
    /** @type {?} */
    CurrencyAmountWithPrice.prototype.currency;
}
class CurrencyAmountFactory {
    /**
     * @param {?} coinDataProvider
     */
    constructor(coinDataProvider) {
        this.coinDataProvider = coinDataProvider;
    }
    /**
     * @param {?} quantity
     * @return {?}
     */
    newAmountFromQuantity(quantity) {
        return __awaiter(this, void 0, void 0, function* () {
            const [amount, tokenSymbol] = quantity.split(' ');
            return this.newAmountFromDecimal(amount, tokenSymbol);
        });
    }
    /**
     * @param {?} decimalAmount
     * @param {?} tokenSymbol
     * @return {?}
     */
    newAmountFromDecimal(decimalAmount, tokenSymbol) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.coinDataProvider.getCoinDataBySymbol(tokenSymbol);
            return new CurrencyAmount(decimalAmount, data);
        });
    }
    /**
     * @param {?} decimalAmount
     * @param {?} coinId
     * @return {?}
     */
    newAmountFromDecimalAndCoinId(decimalAmount, coinId) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.coinDataProvider.getCoinData(coinId);
            return new CurrencyAmount(decimalAmount, data);
        });
    }
    /**
     * @param {?} integerSubunits
     * @param {?} tokenSymbol
     * @return {?}
     */
    newAmountFromInteger(integerSubunits, tokenSymbol) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.coinDataProvider.getCoinDataBySymbol(tokenSymbol);
            /** @type {?} */
            const decimalAmount = new Big(integerSubunits).div(Math.pow(10, data.precision));
            return new CurrencyAmount(decimalAmount, data);
        });
    }
}
if (false) {
    /** @type {?} */
    CurrencyAmountFactory.prototype.coinDataProvider;
}
/** @type {?} */
let currencyAmountFactory;
/**
 * @param {?} database
 * @return {?}
 * @this {*}
 */
function getCurrencyAmountFactory(database) {
    return __awaiter(this, void 0, void 0, function* () {
        if (currencyAmountFactory == undefined) {
            /** @type {?} */
            const coinDataProvider = new CoinDataProvider(database);
            yield coinDataProvider.init();
            currencyAmountFactory = new CurrencyAmountFactory(coinDataProvider);
        }
        return currencyAmountFactory;
    });
}
class CurrencyAmountWithPriceFactory {
    /**
     * @param {?} coinDataProvider
     * @param {?} priceService
     */
    constructor(coinDataProvider, priceService) {
        this.coinDataProvider = coinDataProvider;
        this.priceService = priceService;
    }
    /**
     * @param {?} quantity
     * @return {?}
     */
    newAmountFromQuantity(quantity) {
        return __awaiter(this, void 0, void 0, function* () {
            const [amount, tokenSymbol] = quantity.split(' ');
            return this.newAmountFromDecimal(amount, tokenSymbol);
        });
    }
    /**
     * @param {?} decimalAmount
     * @param {?} tokenSymbol
     * @return {?}
     */
    newAmountFromDecimal(decimalAmount, tokenSymbol) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.coinDataProvider.getCoinDataBySymbol(tokenSymbol);
            return new CurrencyAmountWithPrice(decimalAmount, data, yield this.priceService.getPriceInUSD(data.symbol));
        });
    }
    /**
     * @param {?} decimalAmount
     * @param {?} coinId
     * @return {?}
     */
    newAmountFromDecimalAndCoinId(decimalAmount, coinId) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.coinDataProvider.getCoinData(coinId);
            return new CurrencyAmountWithPrice(decimalAmount, data, yield this.priceService.getPriceInUSD(data.symbol));
        });
    }
    /**
     * @param {?} integerSubunits
     * @param {?} tokenSymbol
     * @return {?}
     */
    newAmountFromInteger(integerSubunits, tokenSymbol) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const data = yield this.coinDataProvider.getCoinDataBySymbol(tokenSymbol);
            /** @type {?} */
            const decimalAmount = new Big(integerSubunits).div(Math.pow(10, data.precision));
            return new CurrencyAmountWithPrice(decimalAmount, data, yield this.priceService.getPriceInUSD(data.symbol));
        });
    }
}
if (false) {
    /** @type {?} */
    CurrencyAmountWithPriceFactory.prototype.coinDataProvider;
    /** @type {?} */
    CurrencyAmountWithPriceFactory.prototype.priceService;
}
/** @type {?} */
let currencyAmountWithPriceFactory;
/**
 * @param {?} database
 * @param {?=} priceService
 * @return {?}
 * @this {*}
 */
function getCurrencyAmountWithPriceFactory(database, priceService = undefined) {
    return __awaiter(this, void 0, void 0, function* () {
        if (currencyAmountWithPriceFactory == undefined) {
            /** @type {?} */
            const coinDataProvider = new CoinDataProvider(database);
            yield coinDataProvider.init();
            if (priceService == undefined) {
                priceService = yield getCoinPriceService(database);
            }
            currencyAmountWithPriceFactory = new CurrencyAmountWithPriceFactory(coinDataProvider, priceService);
        }
        return currencyAmountWithPriceFactory;
    });
}

/**
 * @fileoverview added by tsickle
 * Generated from: node-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: earnbet-common-back-end.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { AccountRepository, AndExpressionGroup, Coin, CoinDataProvider, CoinDatabaseService, CoinId, CoinSymbol, DecimalForPreciseMath, EosTransactionExecutor, EosTransactionExecutorBase, EosUtility, Expression, ExpressionGroup, ExpressionGroupJoiner, ExpressionOperator, FieldIsNotNullStatement, FieldIsNullStatement, FieldStatement, MemoPrefix, MySQLConnectionManager, MySQLDatabase, MySQLExpression, MySQLQuery, MySQLQueryBuilder, MySQLRepository, MySQLTransactionManager, MySQLValue, MySQLValueType, NowUpdateStatement, NullUpdateStatement, NumberForPreciseMath, NumberForPreciseMathBase, NumberStatement, OrExpressionGroup, PreciseDecimal, PreciseMath, PreciseNumber, SavedWithdrawalRequest, SingleExpression, StringExpressionStatement, StringStatement, UpdateStatements, UserAccountDatabaseService, WithdrawalContractService, createExpressionGroup, executeQuery, generateMnemonicPhrase, getCoinPriceService, getCurrencyAmountFactory, getCurrencyAmountWithPriceFactory, getEosJs, getMnemonicUtil, httpGet, httpGetJson, httpsGetJson, isEosTxnIrreversible, isWaxTxnIrreversible, recoverPublicKey };
//# sourceMappingURL=earnbet-common-back-end.js.map
