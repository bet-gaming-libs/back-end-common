/*** IMPORTANT: KEEP CoinIds in THIS ORDER! ***/
export declare enum CoinId {
    BTC = 0,
    ETH = 1,
    EOS = 2,
    BET = 3,
    LTC = 4,
    XRP = 5,
    BCH = 6,
    BNB = 7,
    WAX = 8,
    TRX = 9,
    LINK = 10
}
/***/ 
