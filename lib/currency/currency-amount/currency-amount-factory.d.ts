import { BigSource } from 'big.js';
import { ICurrencyPriceService, ICurrencyAmount, ICurrency, ICoinDataProvider, ICurrencyAmountFactory, ICurrencyAmountWithPrice, ICurrencyAmountWithPriceFactory, IPreciseCurrencyAmount, IPreciseCurrencyAmountWithPrice } from './interfaces';
import { CoinId } from '../database/coins';
import { ICoinDatabaseService } from './interfaces';
import { NumberForPreciseMathBase } from './precise-math-numbers';
declare class CurrencyAmount extends NumberForPreciseMathBase<IPreciseCurrencyAmount> implements ICurrencyAmount {
    readonly currency: ICurrency;
    constructor(decimalValue: BigSource, currency: ICurrency);
    readonly quantity: string;
}
declare class CurrencyAmountWithPrice extends NumberForPreciseMathBase<IPreciseCurrencyAmountWithPrice> implements ICurrencyAmountWithPrice {
    readonly currency: ICurrency;
    constructor(decimalValue: BigSource, currency: ICurrency, priceInUSD: number);
    readonly amountInUSD: string;
    readonly priceInUSD: number;
    readonly quantity: string;
}
declare class CurrencyAmountFactory implements ICurrencyAmountFactory {
    readonly coinDataProvider: ICoinDataProvider;
    constructor(coinDataProvider: ICoinDataProvider);
    newAmountFromQuantity(quantity: string): Promise<CurrencyAmount>;
    newAmountFromDecimal(decimalAmount: BigSource, tokenSymbol: string): Promise<CurrencyAmount>;
    newAmountFromDecimalAndCoinId(decimalAmount: BigSource, coinId: CoinId): Promise<CurrencyAmount>;
    newAmountFromInteger(integerSubunits: BigSource, tokenSymbol: string): Promise<CurrencyAmount>;
}
export declare function getCurrencyAmountFactory(database: ICoinDatabaseService): Promise<CurrencyAmountFactory>;
declare class CurrencyAmountWithPriceFactory implements ICurrencyAmountWithPriceFactory {
    readonly coinDataProvider: ICoinDataProvider;
    readonly priceService: ICurrencyPriceService;
    constructor(coinDataProvider: ICoinDataProvider, priceService: ICurrencyPriceService);
    newAmountFromQuantity(quantity: string): Promise<CurrencyAmountWithPrice>;
    newAmountFromDecimal(decimalAmount: BigSource, tokenSymbol: string): Promise<CurrencyAmountWithPrice>;
    newAmountFromDecimalAndCoinId(decimalAmount: BigSource, coinId: CoinId): Promise<CurrencyAmountWithPrice>;
    newAmountFromInteger(integerSubunits: BigSource, tokenSymbol: string): Promise<CurrencyAmountWithPrice>;
}
export declare function getCurrencyAmountWithPriceFactory(database: ICoinDatabaseService, priceService?: ICurrencyPriceService): Promise<CurrencyAmountWithPriceFactory>;
export {};
