import Big, { BigSource } from 'big.js';
import { CoinId } from '../database/coins';
import { ISavedCoinRow, Coin } from '../database/models/coin';
import { PreciseMath } from './precise-math';
export interface ICurrency {
    id: number;
    symbol: string;
    precision: number;
}
export interface IPreciseNumber {
    readonly precision: number;
    readonly decimal: string;
    readonly integer: string;
    readonly isZero: boolean;
    readonly isPositive: boolean;
    readonly isNegative: boolean;
    readonly bigInteger: Big;
    readonly factor: Big;
    isLessThan(other: IPreciseNumber): boolean;
    isLessThanDecimal(other: BigSource): boolean;
    isGreaterThan(other: IPreciseNumber): boolean;
    isGreaterThanDecimal(other: BigSource): boolean;
    isEqualTo(other: IPreciseNumber): boolean;
    isEqualToDecimal(other: BigSource): boolean;
}
export interface IMatchingNumberTypeValidator<T extends IPreciseNumber> {
    isMatchingType(other: T): boolean;
}
export interface IPreciseNumberFactory<T extends IPreciseNumber> {
    readonly validator: IMatchingNumberTypeValidator<T>;
    newAmountFromInteger(integer: BigSource, precision: number): T;
    newAmountFromDecimal(decimal: BigSource, precision: number): T;
}
export interface IPreciseMath<T extends IPreciseNumber> extends IPreciseNumber {
    readonly startingValue: T;
    readonly factory: IPreciseNumberFactory<T>;
    addDecimal(decimal: BigSource): PreciseMath<T>;
    add(amount: T): PreciseMath<T>;
    subtractDecimal(decimal: BigSource): PreciseMath<T>;
    subtract(amount: T): PreciseMath<T>;
    multiplyDecimal(decimal: BigSource): PreciseMath<T>;
    divideDecimal(decimal: BigSource): PreciseMath<T>;
    absoluteValue(): PreciseMath<T>;
}
export interface IPreciseCurrencyAmount extends IPreciseNumber {
    readonly currency: ICurrency;
    readonly quantity: string;
}
export interface IPreciseCurrencyAmountWithPrice extends IPreciseCurrencyAmount {
    readonly priceInUSD: number;
    readonly amountInUSD: string;
}
export declare type PreciseNumberForMath = PreciseMath<IPreciseNumber>;
export declare type CurrencyAmountForMath = PreciseMath<IPreciseCurrencyAmount>;
export declare type CurrencyAmountWithPriceForMath = PreciseMath<IPreciseCurrencyAmountWithPrice>;
export interface ICurrencyAmount extends IPreciseCurrencyAmount, IPreciseMath<IPreciseCurrencyAmount> {
    readonly math: CurrencyAmountForMath;
}
export interface ICurrencyAmountWithPrice extends IPreciseCurrencyAmountWithPrice, IPreciseMath<IPreciseCurrencyAmountWithPrice> {
    readonly math: CurrencyAmountWithPriceForMath;
}
export interface ICurrencyAmountFactory {
    readonly coinDataProvider: ICoinDataProvider;
    newAmountFromQuantity(quantity: string): Promise<ICurrencyAmount>;
    newAmountFromDecimal(decimalAmount: BigSource, tokenSymbol: string): Promise<ICurrencyAmount>;
    newAmountFromDecimalAndCoinId(decimalAmount: BigSource, coinId: CoinId): Promise<ICurrencyAmount>;
    newAmountFromInteger(integerSubunits: BigSource, tokenSymbol: string): Promise<ICurrencyAmount>;
}
export interface ICurrencyAmountWithPriceFactory extends ICurrencyAmountFactory {
    readonly priceService: ICurrencyPriceService;
    newAmountFromQuantity(quantity: string): Promise<ICurrencyAmountWithPrice>;
    newAmountFromDecimal(decimalAmount: BigSource, tokenSymbol: string): Promise<ICurrencyAmountWithPrice>;
    newAmountFromDecimalAndCoinId(decimalAmount: BigSource, coinId: CoinId): Promise<ICurrencyAmountWithPrice>;
    newAmountFromInteger(integerSubunits: BigSource, tokenSymbol: string): Promise<ICurrencyAmountWithPrice>;
}
export interface ICurrencyPriceService {
    getPriceInUSD(currencySymbol: string): Promise<number>;
}
export interface ICoinDatabaseService {
    getAllCoins(): Promise<ISavedCoinRow[]>;
}
export interface ICoinDataProvider {
    getCoinId(symbol: string): Promise<CoinId>;
    getCoinSymbol(coinId: CoinId): Promise<string>;
    getCoinData(coinId: CoinId): Promise<Coin>;
    getCoinDataBySymbol(symbol: string): Promise<Coin>;
    getAllCoins(): Coin[];
}
