import { ICoinDataProvider, ICoinDatabaseService } from '../currency-amount/interfaces';
import { CoinId } from './coins';
import { Coin } from './models/coin';
export declare class CoinDataProvider implements ICoinDataProvider {
    private readonly database;
    private isInit;
    private ids;
    private data;
    private allCoins;
    constructor(database: ICoinDatabaseService);
    init(): Promise<void>;
    getCoinDataBySymbol(symbol: string): Promise<Coin>;
    getCoinId(symbol: string): Promise<CoinId>;
    getCoinSymbol(coinId: CoinId): Promise<string>;
    getCoinData(coinId: CoinId): Promise<Coin>;
    getAllCoins(): Coin[];
}
