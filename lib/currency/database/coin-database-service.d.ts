import { ICoinDatabaseService } from '../currency-amount/interfaces';
import { IMySQLDatabase } from '../../database/engine/mysql/interfaces';
import { ISavedCoinRow } from './models/coin';
export declare class CoinDatabaseService implements ICoinDatabaseService {
    private readonly coins;
    constructor(database: IMySQLDatabase);
    getAllCoins(): Promise<ISavedCoinRow[]>;
    getCoinData(coinId: number): Promise<ISavedCoinRow>;
}
