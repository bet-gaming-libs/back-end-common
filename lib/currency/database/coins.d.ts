/*** IMPORTANT: KEEP CoinIds in THIS ORDER! ***/
export declare enum CoinId {
    BTC = 0,
    ETH = 1,
    EOS = 2,
    BET_BINANCE = 3,
    LTC = 4,
    XRP = 5,
    BCH = 6,
    BNB = 7,
    WAX = 8,
    TRX = 9,
    LINK = 10,
    BET_ETHEREUM = 11,
    DAI = 12,
    USDC = 13,
    USDT = 14,
    STACK = 15
}
/***/
export declare enum CoinSymbol {
    BTC = "BTC",
    ETH = "ETH",
    EOS = "EOS",
    BET = "BET",
    LTC = "LTC",
    XRP = "XRP",
    BCH = "BCH",
    BNB = "BNB",
    WAX = "WAX",
    TRX = "TRX",
    LINK = "LINK",
    DAI = "DAI",
    USDC = "USDC",
    USDT = "USDT"
}
