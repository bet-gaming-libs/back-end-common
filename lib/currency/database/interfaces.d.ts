export interface IUserAccountRow {
    eos_account_name: string;
    easy_account_public_key: string;
}
export interface IUserAccountDatabaseService {
    getAccountInfo(userId: number): Promise<IUserAccountRow>;
    getUserIdForEosAccount(eos_account: string): Promise<number>;
    getUserIdForEasyAccount(ezeos_id: string): Promise<number>;
}
