import { IRefundWithdrawalRequestParams } from "../services/interfaces";
export interface ISavedWithdrawalRequest {
    getRefundParams(): Promise<IRefundWithdrawalRequestParams>;
}
