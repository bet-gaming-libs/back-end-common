export interface ISavedAccountRow {
    id: number;
    eos_account: string;
    ezeos_id: string;
}
