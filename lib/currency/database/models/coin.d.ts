import { PreciseDecimal } from '../../currency-amount/precise-math-numbers';
import { CoinId } from '../coins';
import { ICurrency } from '../../currency-amount/interfaces';
export interface ISavedCoinRow {
    id: number;
    symbol: string;
    precision: number;
    uses_memo_for_deposits: number;
    minimum_withdrawal_amount: string;
}
export declare class Coin implements ICurrency {
    readonly id: CoinId;
    readonly symbol: string;
    readonly precision: number;
    readonly usesMemoForDeposits: boolean;
    readonly minimumWithdrawalAmount: PreciseDecimal;
    constructor(data: ISavedCoinRow);
    readonly data: {
        id: CoinId;
        symbol: string;
        precision: number;
        usesMemoForDeposits: boolean;
        minimumWithdrawalAmount: string;
    };
}
