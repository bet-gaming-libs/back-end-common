export interface ISavedEntity {
    id: number;
}
export declare const CommonFieldNames: {
    ID: string;
    CREATED_AT: string;
};
