import { IRefundWithdrawalRequestParams } from '../../services/interfaces';
import { IUserAccountDatabaseService } from '../interfaces';
import { ICurrencyAmount, ICurrencyAmountFactory } from '../../currency-amount/interfaces';
export interface INewWithdrawalRequestRow {
    id: string;
    memo: string;
    coin_id: number;
    tx_hash: string;
    is_processed: number;
    easy_account_id: string;
    eos_account_name: string;
    withdraw_address: string;
    coin_decimal_amount: string;
}
export interface IFailedWithdrawalRequestRow extends INewWithdrawalRequestRow {
    error_message: string;
}
export interface ISucceededWithdrawalRequest extends INewWithdrawalRequestRow {
    processed_at: Date;
    withdraw_transaction_id: number;
}
export interface IWithdrawalRequestInsufficientFunds extends INewWithdrawalRequestRow {
    insufficient_funds: number;
}
export interface ISavedWithdrawalRequestRow extends IFailedWithdrawalRequestRow, ISucceededWithdrawalRequest, IWithdrawalRequestInsufficientFunds {
    requested_at: Date;
}
export interface IWithdrawalRequest {
    address: string;
    memo: string;
    getAmount(): Promise<ICurrencyAmount>;
}
export interface ISavedWithdrawalRequest extends IWithdrawalRequest {
    getRefundParams(): Promise<IRefundWithdrawalRequestParams>;
}
export declare class SavedWithdrawalRequest implements ISavedWithdrawalRequest {
    private readonly account;
    private readonly amountFactory;
    readonly address: string;
    readonly id: string;
    readonly transactionHash: string;
    readonly memo: string;
    readonly coinId: number;
    readonly decimalAmount: string;
    readonly errorMessage: string;
    private readonly isEosAccount;
    private readonly eosAccountName;
    private readonly easyAccountId;
    private userId;
    private amount;
    constructor(data: ISavedWithdrawalRequestRow, account: IUserAccountDatabaseService, amountFactory: ICurrencyAmountFactory);
    getDataForApi(): Promise<{
        id: string;
        memo: string;
        address: string;
        errorMessage: string;
        coin: string;
        amount: string;
    }>;
    getAmount(): Promise<ICurrencyAmount>;
    getRefundParams(): Promise<IRefundWithdrawalRequestParams>;
    getUserId(): Promise<number>;
}
