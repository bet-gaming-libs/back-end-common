import { MySQLRepository } from '../../../database/engine/mysql/mysql-repository';
import { IMySQLDatabase } from '../../../database/engine/mysql/interfaces';
import { ISavedAccountRow } from '../models/account';
export declare class AccountRepository extends MySQLRepository<ISavedAccountRow> {
    constructor(db: IMySQLDatabase);
}
