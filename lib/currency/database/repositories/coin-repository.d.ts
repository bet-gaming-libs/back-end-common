import { MySQLRepository } from '../../../database/engine/mysql/mysql-repository';
import { IMySQLDatabase } from '../../../database/engine/mysql/interfaces';
import { ISavedCoinRow } from '../models/coin';
export declare class CoinRepository extends MySQLRepository<ISavedCoinRow> {
    constructor(database: IMySQLDatabase);
}
