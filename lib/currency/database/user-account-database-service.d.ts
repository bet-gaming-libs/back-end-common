import { MySQLDatabase } from '../../database/engine/mysql/mysql-database';
import { IUserAccountDatabaseService, IUserAccountRow } from './interfaces';
export declare class UserAccountDatabaseService implements IUserAccountDatabaseService {
    protected readonly database: MySQLDatabase;
    private readonly accountRepository;
    constructor(database: MySQLDatabase);
    getAccountInfo(userId: number): Promise<IUserAccountRow>;
    getUserIdForEosAccount(eos_account: string): Promise<number>;
    getUserIdForEasyAccount(ezeos_id: string): Promise<number>;
}
