import { bip32 } from 'bitcoinjs-lib';
import { CoinId } from '../database/coins';
export declare enum MemoPrefix {
    EOSBET = "EOSBET_",
    EARNBET = "EARNBET_",
    XRP = "EARNBET_XRP_"
}
export interface IMnemonicUtility {
    words: string;
    getDepositMemo(userId: number, coinId: CoinId, prefix: MemoPrefix): string;
    getNodeForChangeAddress(coinId: CoinId): bip32.BIP32Interface;
    getNodeForDepositAddress(coinId: CoinId, userId: number): bip32.BIP32Interface;
}
export interface IMnemonicUtilityConfig {
    mnemonicPhrase: string;
    blockchainNetwork: string;
}
