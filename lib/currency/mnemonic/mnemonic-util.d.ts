import { IMnemonicUtility, IMnemonicUtilityConfig } from './interfaces';
export declare function getMnemonicUtil(config: IMnemonicUtilityConfig): Promise<IMnemonicUtility>;
export declare function generateMnemonicPhrase(): string;
