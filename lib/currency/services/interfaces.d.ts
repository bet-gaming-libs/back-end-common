import { ISavedWithdrawalRequest } from '../database/models/withdrawal-request';
export interface IWithdrawalRequestData {
    user: string;
    tx_hash: string;
    extra_data: string;
    withdraw_id: string;
    withdraw_memo: string;
    withdraw_address: string;
    withdraw_quantity: string;
}
export interface IRefundWithdrawalRequestParams {
    requestId: string;
    easyAccountPublicKey: string;
    reason: string;
}
export interface IWithdrawalContractService {
    getPendingRequests(): Promise<IWithdrawalRequestData[]>;
    processRequest(requestId: string, txnId: string): Promise<string>;
    refundRequest(request: ISavedWithdrawalRequest): Promise<string>;
}
