import { IWithdrawalContractService, IWithdrawalRequestData } from './interfaces';
import { IEosUtility } from '../../eos/interfaces';
import { IEosAccountNamesBase } from '../../eos/eos-settings';
import { ISavedWithdrawalRequest } from '../database/models/withdrawal-request';
export declare class WithdrawalContractService<T extends IEosAccountNamesBase> implements IWithdrawalContractService {
    private readonly eosUtility;
    private readonly contractName;
    constructor(eosUtility: IEosUtility<T>);
    getPendingRequests(): Promise<IWithdrawalRequestData[]>;
    processRequest(requestId: string, txnId: string): Promise<string>;
    refundRequest(request: ISavedWithdrawalRequest): Promise<string>;
    private readonly contracts;
}
