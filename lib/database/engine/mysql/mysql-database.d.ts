import { IMySQLDatabase, IMySQLConfig } from './interfaces';
import { MySQLQueryBuilder } from './mysql-query-builder';
import { MySQLTransactionManager } from './mysql-transaction-manager';
export declare class MySQLDatabase implements IMySQLDatabase {
    readonly builder: MySQLQueryBuilder;
    readonly transactionManager: MySQLTransactionManager;
    constructor(config: IMySQLConfig);
}
