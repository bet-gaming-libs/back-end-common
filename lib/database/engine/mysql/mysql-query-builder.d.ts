import { IMySQLRepository, PrimaryKeyValue, IMySQLQueryResult, IMySQLConfig, ISelectQueryBuilderParams, IUpdateQueryBuilderParams } from './interfaces';
import { MySQLQuery } from './mysql-query';
export declare class MySQLQueryBuilder {
    private connectionManager;
    constructor(config: IMySQLConfig);
    insert<T>(repository: IMySQLRepository, entities: T[]): Promise<MySQLQuery<IMySQLQueryResult>>;
    private constructInsertQuery;
    private constructRowForInsert;
    private prepareValueForInsert;
    selectByPrimaryKeys<T>(repository: IMySQLRepository, fields: string[], primaryKeyValues: PrimaryKeyValue[]): MySQLQuery<T[]>;
    selectAll<T>(repository: IMySQLRepository, fields: string[]): MySQLQuery<T[]>;
    select<T>(params: ISelectQueryBuilderParams<T>): Promise<MySQLQuery<T[]>>;
    private prepareSelectStatement;
    private prepareFieldsForSelect;
    private prepareFieldList;
    update<T>(params: IUpdateQueryBuilderParams<T>): Promise<MySQLQuery<IMySQLQueryResult>>;
    private prepareWhereOrderAndLimit;
    private prepareExpressionGroup;
    private escape;
    delete(repository: IMySQLRepository, primaryKeyValues: any[]): MySQLQuery<IMySQLQueryResult>;
    rawQuery<T>(sql: string, argumentsToPrepare?: any): MySQLQuery<T>;
}
