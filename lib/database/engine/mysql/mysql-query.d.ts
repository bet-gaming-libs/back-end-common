import { Connection } from 'mysql';
import { MySQLConnectionManager } from './mysql-connection-manager';
export declare class MySQLQuery<T> {
    readonly sql: string;
    private connectionManager;
    readonly argumentsToPrepare?: any;
    constructor(sql: string, connectionManager: MySQLConnectionManager, argumentsToPrepare?: any);
    execute(): Promise<T>;
}
export declare function executeQuery<T>(sql: string, connection: Connection, argumentsToPrepare: any): Promise<T>;
