import { IMySQLDatabase, IMySQLRepository, PrimaryKeyValue, IMySQLQueryResult, ISelectQueryParams, IUpdateQueryParams } from './interfaces';
import { MySQLQuery } from './mysql-query';
export declare type SelectOneHandler<T> = (rows: T[]) => void;
export declare class MySQLRepository<TypeForSelect> implements IMySQLRepository {
    protected db: IMySQLDatabase;
    private _tableName;
    protected defaultFieldsToSelect: string[];
    private _primaryKey;
    readonly isPrimaryKeyAString: boolean;
    constructor(db: IMySQLDatabase, _tableName: string, defaultFieldsToSelect?: string[], _primaryKey?: string, isPrimaryKeyAString?: boolean);
    insert<T>(entity: T): Promise<IMySQLQueryResult>;
    insertMany<T>(entities: T[]): Promise<IMySQLQueryResult>;
    buildInsert<T>(entities: T[]): Promise<MySQLQuery<IMySQLQueryResult>>;
    selectOneByPrimaryKey(primaryKeyValue: PrimaryKeyValue, fields?: string[]): Promise<TypeForSelect>;
    selectManyByPrimaryKeys(primaryKeyValues: PrimaryKeyValue[], fields?: string[]): Promise<TypeForSelect[]>;
    selectAll(fields?: string[]): Promise<TypeForSelect[]>;
    selectOne(params: ISelectQueryParams<TypeForSelect>): Promise<TypeForSelect>;
    select(params: ISelectQueryParams<TypeForSelect>): Promise<TypeForSelect[]>;
    buildSelect(params: ISelectQueryParams<TypeForSelect>): Promise<MySQLQuery<TypeForSelect[]>>;
    updateOne(params: IUpdateQueryParams<TypeForSelect>): Promise<IMySQLQueryResult>;
    buildUpdateOne(params: IUpdateQueryParams<TypeForSelect>): Promise<MySQLQuery<IMySQLQueryResult>>;
    update(params: IUpdateQueryParams<TypeForSelect>): Promise<IMySQLQueryResult>;
    buildUpdate(params: IUpdateQueryParams<TypeForSelect>): Promise<MySQLQuery<IMySQLQueryResult>>;
    delete(primaryKeyValues: PrimaryKeyValue[]): Promise<IMySQLQueryResult>;
    readonly tableName: string;
    readonly primaryKey: string;
    private readonly primaryKeyFieldList;
}
