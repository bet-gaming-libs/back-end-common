import { MySQLConnectionManager } from "./mysql-connection-manager";
import { IMySQLQueryResult } from "./interfaces";
export declare class MySQLQuery<T> {
    readonly sql: string;
    private connectionManager;
    constructor(sql: string, connectionManager: MySQLConnectionManager);
    execute(): Promise<T>;
}
export declare class MySQLTransactionQuery extends MySQLQuery<IMySQLQueryResult> {
}
