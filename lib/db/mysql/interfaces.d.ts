import { MySQLQueryBuilder } from './mysql-query-builder';
import { MySQLTransactionManager } from './mysql-transaction-manager';
import { ExpressionGroup, Expression } from './mysql-statements';
export interface IMySQLConfig {
    host: string;
    user: string;
    password: string;
    database: string;
    charset: string;
}
export interface IMySQLRepository {
    tableName: string;
    primaryKey: string;
    isPrimaryKeyAString: boolean;
}
export interface IMySQLQueryResult {
    fieldCount: number;
    affectedRows: number;
    insertId: number;
    serverStatus: number;
    warningCount: number;
    message: string;
    changedRows: number;
}
export declare type PrimaryKeyValue = number | string;
export declare type QueryExecuter<T> = () => Promise<T>;
export interface IMySQLDatabase {
    readonly builder: MySQLQueryBuilder;
    readonly transactionManager: MySQLTransactionManager;
}
export interface IQueryParams<T> {
    whereMap?: Partial<T>;
    whereAndMap?: Partial<T>;
    whereOrMap?: Partial<T>;
    whereClause?: Expression;
    wherePrimaryKeys?: (number | string)[];
    whereGroup?: ExpressionGroup;
    orderBy?: string;
    limit?: number | string;
}
export interface ISelectQueryParams<T> extends IQueryParams<T> {
    fields?: string[];
}
export interface IQueryBuilderParams<T> extends IQueryParams<T> {
    repository: IMySQLRepository;
}
export interface ISelectQueryBuilderParams<T> extends ISelectQueryParams<T>, IQueryBuilderParams<T> {
    fields: string[];
}
export interface IUpdateQueryParams<T> extends IQueryParams<T> {
    singleStatement?: Expression;
    statements?: Expression[];
}
export interface IUpdateQueryBuilderParams<T> extends IUpdateQueryParams<T>, IQueryBuilderParams<T> {
}
