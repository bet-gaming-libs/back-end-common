import * as mysql from "mysql";
import { IMySQLConfig } from './interfaces';
export declare class MySQLConnectionManager {
    private config;
    private isConnecting;
    private connection;
    constructor(config: IMySQLConfig);
    getConnection(): Promise<mysql.Connection>;
    private reconnect();
    private connect();
    private readonly isDisconnected;
    endConnection(): void;
}
