import { IMySQLRepository, PrimaryKeyValue, IMySQLQueryResult, IMySQLConfig, ISelectQueryBuilderParams, IUpdateQueryBuilderParams } from "./interfaces";
import { MySQLQuery } from './mysql-query';
export declare class MySQLQueryBuilder {
    private connectionManager;
    constructor(config: IMySQLConfig);
    insert<T>(repository: IMySQLRepository, entities: T[]): Promise<MySQLQuery<IMySQLQueryResult>>;
    private constructInsertQuery(repository, entities);
    private constructRowForInsert(entity);
    private prepareValueForInsert(value);
    selectByPrimaryKeys<T>(repository: IMySQLRepository, fields: string[], primaryKeyValues: PrimaryKeyValue[]): MySQLQuery<T[]>;
    selectAll<T>(repository: IMySQLRepository, fields: string[]): MySQLQuery<T[]>;
    select<T>(params: ISelectQueryBuilderParams<T>): Promise<MySQLQuery<T[]>>;
    private prepareSelectStatement<T>(params);
    private prepareFieldsForSelect(fields, tableName);
    private prepareFieldList(fields);
    update<T>(params: IUpdateQueryBuilderParams<T>): Promise<MySQLQuery<IMySQLQueryResult>>;
    private prepareWhereOrderAndLimit<T>(params);
    private prepareExpressionGroup(group);
    private escape(value);
    delete(repository: IMySQLRepository, primaryKeyValues: any[]): MySQLQuery<IMySQLQueryResult>;
    rawQuery<T>(sql: string): MySQLQuery<T>;
}
