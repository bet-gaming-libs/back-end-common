export declare enum MySQLValueType {
    EXPRESSION = 0,
    STRING = 1,
}
export declare enum ExpressionOperator {
    EQUAL = "=",
    LESS_THAN = "<",
    LESS_THAN_OR_EQUAL = "<=",
    GREATER_THAN = ">",
    GREATER_THAN_OR_EQUAL = ">=",
    IS = "IS",
    IS_NOT = "IS NOT",
}
export declare enum ExpressionGroupJoiner {
    AND = "AND",
    OR = "OR",
    COMMA = ",",
}
export declare type ExpressionValueType = string | number;
export interface IMySQLValue {
    data: ExpressionValueType;
    isString: boolean;
}
export declare class MySQLValue implements IMySQLValue {
    readonly data: ExpressionValueType;
    readonly type: MySQLValueType;
    readonly isString: boolean;
    constructor(data: ExpressionValueType, type: MySQLValueType);
}
export declare enum MySQLExpression {
    NOW = "NOW()",
    NULL = "NULL",
}
export declare abstract class Expression {
    readonly leftSide: string;
    readonly operator: string;
    readonly rightSide: MySQLValue;
    constructor(leftSide: string, operator: string, rightSide: MySQLValue);
}
export declare class ExpressionGroup {
    readonly expressions: Expression[];
    readonly joiner: ExpressionGroupJoiner;
    readonly surroundExpressionWithBrackets: boolean;
    constructor(expressions: Expression[], joiner: ExpressionGroupJoiner, surroundExpressionWithBrackets?: boolean);
}
export declare function createExpressionGroup(map: any, joiner: ExpressionGroupJoiner): ExpressionGroup;
export declare class SingleExpression extends ExpressionGroup {
    constructor(expression: Expression);
}
export declare class AndExpressionGroup extends ExpressionGroup {
    constructor(expressions: Expression[]);
}
export declare class OrExpressionGroup extends ExpressionGroup {
    constructor(expressions: Expression[]);
}
export declare class UpdateStatements extends ExpressionGroup {
    constructor(expressions: Expression[]);
}
export declare abstract class FieldStatement extends Expression {
    constructor(fieldName: string, value: MySQLValue, operator: ExpressionOperator);
}
export declare class NumberStatement extends FieldStatement {
    constructor(fieldName: string, data: number, operator?: ExpressionOperator);
}
export declare class StringStatement extends FieldStatement {
    constructor(fieldName: string, data: string, operator?: ExpressionOperator);
}
export declare class StringExpressionStatement extends FieldStatement {
    constructor(fieldName: string, data: string, operator?: ExpressionOperator);
}
export declare class NullUpdateStatement extends FieldStatement {
    constructor(fieldName: string);
}
export declare class NowUpdateStatement extends FieldStatement {
    constructor(fieldName: string);
}
export declare class FieldIsNullStatement extends FieldStatement {
    constructor(fieldName: string);
}
export declare class FieldIsNotNullStatement extends FieldStatement {
    constructor(fieldName: string);
}
