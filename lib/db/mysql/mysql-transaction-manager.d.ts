import { IMySQLConfig, IMySQLQueryResult } from './interfaces';
export declare class MySQLTransactionManager {
    private queue;
    private isExecutingTransaction;
    private readonly connectionManager;
    constructor(config: IMySQLConfig);
    performTransaction(queries: string[]): Promise<IMySQLQueryResult[]>;
    /*** we need to manage a queue of transactions
     * to be executed in order
     * once the transaction either is successful or fails
     * then move on to the next
     */
    private performNextTransaction;
    private onTransactionCompleted;
}
