import { IUpdateStatementData } from './interfaces';
export declare class MySQLExpression {
    static NOW: string;
    static NULL: string;
    static booleanAsInt(bool: boolean): number;
}
export declare class MySQLValueType {
    static EXPRESSION: number;
    static STRING: number;
}
export declare class UpdateStatement<T extends string | number> implements IUpdateStatementData {
    readonly fieldName: string;
    private type;
    private data;
    constructor(fieldName: string, type: number, data: T);
    readonly value: {
        type: number;
        data: T;
    };
}
export declare class StringUpdateStatement extends UpdateStatement<string> {
    constructor(fieldName: string, fieldValue: string);
}
export declare class ExpressionUpdateStatement<T extends string | number> extends UpdateStatement<T> {
    constructor(fieldName: string, fieldValue: T);
}
export declare class NumberUpdateStatement extends ExpressionUpdateStatement<number> {
}
export declare class CalculationUpdateStatement extends ExpressionUpdateStatement<string> {
}
export declare class NowUpdateStatement extends CalculationUpdateStatement {
    constructor(fieldName: string);
}
