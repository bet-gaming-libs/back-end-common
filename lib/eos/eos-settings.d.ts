export interface IEosConfig {
    eosjs: any;
    endpoints: {
        poller: string;
        transactor: string;
    };
    messageSigningPrivateKey?: string;
}
export interface IEosAccountNamesBase {
    easyAccount: string;
    withdrawal: string;
}
