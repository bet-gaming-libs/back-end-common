import { TxnConfirmedChecker, IEosActionData, IEosTransactionAction } from 'earnbet-common';
import { IEosUtility } from './interfaces';
import { IEosAccountNamesBase } from './eos-settings';
export declare class EosTransactionExecutorBase {
    readonly actions: IEosTransactionAction[];
    private readonly eos;
    private readonly txnChecker;
    private readonly retryOnError;
    private readonly secondsBeforeRetry;
    private readonly expireInSeconds;
    private numOfRetries;
    protected readonly actionId: string;
    private signedTransaction;
    private broadcastedTxnId;
    private broadcastedTime;
    constructor(actions: IEosTransactionAction[], eos: any, txnChecker: TxnConfirmedChecker, retryOnError?: boolean, secondsBeforeRetry?: number, expireInSeconds?: number);
    execute(): Promise<string>;
    sign(): Promise<ISignedTransactionResult>;
    protected onBroadcastError(error: any): Promise<string>;
    confirm(): Promise<string>;
}
export declare class EosTransactionExecutor<T extends IEosAccountNamesBase> extends EosTransactionExecutorBase {
    constructor(actions: IEosActionData[], eosUtility: IEosUtility<T>, txnChecker: TxnConfirmedChecker, retryOnError?: boolean, secondsBeforeRetry?: number);
}
interface ISignedTransaction {
    compression: string;
    transaction: {};
    signatures: string[];
}
interface ISignedTransactionResult {
    transaction_id: string;
    transaction: ISignedTransaction;
}
export {};
