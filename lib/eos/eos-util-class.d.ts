import { IActionAuthorization, ITableRowQueryParameters, IEosActionData } from 'earnbet-common';
import { IEosUtility, IEosUtilityParams, IEOSInfo } from './interfaces';
import { IEosConfig, IEosAccountNamesBase } from './eos-settings';
export declare class EosUtility<T extends IEosAccountNamesBase> implements IEosUtility<T> {
    readonly poller: any;
    readonly transactor: any;
    readonly chainId: number;
    readonly config: IEosConfig;
    readonly contracts: T;
    readonly authorization: IActionAuthorization[];
    constructor(params: IEosUtilityParams<T>);
    getInfo(): Promise<IEOSInfo>;
    getUniqueTableRow<T>(code: string, scope: string, table: string, rowId: string): Promise<T[]>;
    getAllTableRows<T>(code: string, scope: string, table: string, key: string): Promise<T[]>;
    getTableRows<T>(parameters: ITableRowQueryParameters): Promise<T[]>;
    getTokenBalance(contract: string, account: string, symbol: string, table?: string): Promise<string>;
    executeTransaction(actions: IEosActionData[]): Promise<string>;
    isAccountSafe(eosAccountName: string): Promise<boolean>;
    isEasyAccountContract(name: string): boolean;
}
