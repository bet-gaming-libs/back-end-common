import { ISignatureInfo } from 'earnbet-common';
import { IEosConfig } from './eos-settings';
export declare function getEosJs(eosConfig: IEosConfig, endpoint?: string): any;
export declare function recoverPublicKey(signedData: string, info: ISignatureInfo): string;
