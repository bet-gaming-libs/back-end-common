import { ITableRowQueryParameters, IEosActionData, IActionAuthorization } from 'earnbet-common';
import { IEosConfig, IEosAccountNamesBase } from './eos-settings';
export interface IEOSInfo {
    server_version: string;
    chain_id: string;
    head_block_num: number;
    last_irreversible_block_num: number;
    last_irreversible_block_id: string;
    head_block_id: string;
    head_block_time: string;
    head_block_producer: string;
    virtual_block_cpu_limit: number;
    virtual_block_net_limit: number;
    block_cpu_limit: number;
    block_net_limit: number;
    server_version_string: string;
}
export interface IEosUtilityParams<T extends IEosAccountNamesBase> {
    readonly chainId: number;
    readonly config: IEosConfig;
    readonly contracts: T;
    readonly authorization: IActionAuthorization[];
}
export interface IEosUtility<T extends IEosAccountNamesBase> {
    readonly chainId: number;
    readonly config: IEosConfig;
    readonly contracts: T;
    readonly authorization: IActionAuthorization[];
    readonly transactor: any;
    getInfo(): Promise<IEOSInfo>;
    getTokenBalance(contract: string, account: string, symbol: string, table?: string): Promise<string>;
    getUniqueTableRow<T>(code: string, scope: string, table: string, rowId: string): Promise<T[]>;
    getAllTableRows<T>(code: string, scope: string, table: string, key: string): Promise<T[]>;
    getTableRows<T>(parameters: ITableRowQueryParameters): Promise<T[]>;
    executeTransaction(actions: IEosActionData[]): Promise<string>;
    isAccountSafe(eosAccountName: string): Promise<boolean>;
    isEasyAccountContract(name: string): boolean;
}
