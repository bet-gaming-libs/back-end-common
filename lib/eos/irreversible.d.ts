export declare function isWaxTxnIrreversible(txnId: string): Promise<boolean>;
export declare function isEosTxnIrreversible(txnId: string): Promise<boolean>;
