import { ICurrencyPriceService, ICoinDatabaseService } from '../currency/currency-amount/interfaces';
export declare function getCoinPriceService(database: ICoinDatabaseService): Promise<ICurrencyPriceService>;
